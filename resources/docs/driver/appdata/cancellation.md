# Cancellation reasons

---
Driver app reason for cancellation

### Details

| Method | Uri   | Authorization |
| : |   :-   |  :  |
| GET | `api/driver/cancellation-reason` | Yes|

### Response

```json
{
    "success": true,
    "data": [
        {
            "id": 2,
            "app_type": [
                {
                    "id": 2,
                    "pivot": {
                        "cancellation_id": 2,
                        "app_id": 2
                    }
                }
            ],
            "lang": {
                "en": {
                    "id": 3,
                    "cancellation_id": 2,
                    "name": "No more required",
                    "language": "en"
                },
                "ar": {
                    "id": 4,
                    "cancellation_id": 2,
                    "name": "No more required-ar",
                    "language": "ar"
                }
            }
        }
    ]
}
```
