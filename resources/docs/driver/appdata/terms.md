# Terms and conditions

---
Driver app terms and conditions

### Details

| Method | Uri   | Authorization |
| : |   :-   |  :  |
| GET | `api/driver/terms-conditions` | No |

### Response

```json
{
    "success": true,
    "data": {
        "id": 2,
        "lang": {
            "en": {
                "page_id": 2,
                "title": "Terms and condition",
                "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. \r\n                Lorem Ipsum has been the industry standard dummy text ever since the 1500s, \r\n                when an unknown printer took",
                "language": "en"
            },
            "ar": {
                "page_id": 2,
                "title": "النوع وتدافعت عليه لصنع",
                "content": " يستخدم في صناعة الطباعة والتنضيد. عندما أخذت طابعة غير معروفة لوحًا من النوع وتدافعت عليه لصنع نوع\r\n                كتاب العينة. لقد نجت ليس فقط خمسة قرون ، ولكن أيضًا القفزة الإلكترونية",
                "language": "ar"
            }
        }
    }
}
```
