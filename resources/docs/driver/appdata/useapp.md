# How to use app

---
How to use app

### Details

| Method | Uri   | Authorization |
| : |   :-   |  :  |
| GET | `api/driver/useapp` | No |

### Response

```json
{
    "success": true,
    "data": [
    {
        "id": 1,
        "created_at": "2020-11-20T19:29:15.000000Z",
        "updated_at": "2020-11-20T19:29:15.000000Z",
        "deleted_at": null,
        "lang": {
            "en": {
                "id": 1,
                "how_to_id": 1,
                "title": "Step 1: Login using phone",
                "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s,",
                "language": "en",
                "image": "http://easify.test/uploads/screenshot/Cs9ZlNbdFdWXsgWP3RHc4k8IBdtLkUu2bdCl6mtm.png"
            },
            "ar": {
                "id": 2,
                "how_to_id": 1,
                "title": "الخطوة 1: تسجيل الدخول باستخدام الهاتف",
                "content": "يستخدم في صناعة الطباعة والتنضيد. عندما أخذت طابعة غير معروفة لوحًا من النوع وتدافعت عليه لصنع نوع كتاب العينة. لقد نجت ليس فقط خمسة قرون ، ولكن أيضًا القفزة الإلكترونية",
                "language": "ar",
                "image": "http://easify.test/uploads/screenshot/RnnqDueWx6SaieeUxVfEwOsttpvMZYj3FeEHtH7m.jpeg"
            }
        }
    }
    ]
}
```
