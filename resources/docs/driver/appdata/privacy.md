# Privacy policies

---
Driver app privacy policies

### Details

| Method | Uri   | Authorization |
| : |   :-   |  :  |
| GET | `api/driver/privacy-policies` | No |

### Response

```json
{
    "success": true,
    "data": {
        "id": 1,
        "lang": {
            "en": {
                "page_id": 1,
                "title": "Privacy policies",
                "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. \r\n                            Lorem Ipsum has been the industry standard dummy text ever since the 1500s, \r\n                            when an unknown printer took a galley of type and scrambled it to make a type \r\n                            specimen book. It has survived not only five centuries, but also the leap into electronic \r\n                            typesetting, remaining essentially unchanged",
                "language": "en"
            },
            "ar": {
                "page_id": 1,
                "title": "النوع وتدافعت عليه لصنع",
                "content": " يستخدم في صناعة الطباعة والتنضيد. عندما أخذت طابعة غير معروفة لوحًا من النوع وتدافعت عليه لصنع نوع\r\n                كتاب العينة. لقد نجت ليس فقط خمسة قرون ، ولكن أيضًا القفزة الإلكترونية",
                "language": "ar"
            }
        }
    }
}
```
