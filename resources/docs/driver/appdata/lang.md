# Lang switch

---

 

### Details

| Method | Uri                  | Authorization |
| :----- | :------------------- | :------------ |
| POST   | `api/driver/lang` | Yes          |

### Request Params

```json
{
    "lang":"ar"
}
```

### Response

```json
{
    "success": true,
    "msg": "Language updated successfully"
}
```
