# FAQ

---
Driver app FAQ

### Details

| Method | Uri   | Authorization |
| : |   :-   |  :  |
| GET | `api/driver/faq` | No |

### Response

```json
{
    "success": true,
    "data": [
        {
            "id": 17,
            "app_type": [
                {
                    "id": 1,
                    "pivot": {
                        "faq_id": 17,
                        "app_id": 1
                    }
                },
                {
                    "id": 2,
                    "pivot": {
                        "faq_id": 17,
                        "app_id": 2
                    }
                }
            ],
            "lang": {
                "en": {
                    "id": 33,
                    "faq_id": 17,
                    "question": "Question (EN)",
                    "answer": "<p>Answer (EN)</p>",
                    "language": "en"
                },
                "ar": {
                    "id": 34,
                    "faq_id": 17,
                    "question": "Question (AR)",
                    "answer": "<p>Answer (AR)</p>",
                    "language": "ar"
                }
            }
        },
        
    ]
}
```
