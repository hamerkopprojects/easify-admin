#Order details
---
### Details

| Method | Uri                  | Authorization |
| :----- | :------------------- | :------------ |
| GET | `api/driver/order-delivery/details/{id}` | Yes            |

### Response

```json
{
    "success": true,
    "data": {
        "id": 2,
        "users_id": 1,     
        "billing_phone": "9747714167",
        "product": [
            {
                "id": 12,
                "category_id": 17,
                "brand_id": 3,
                "sku": "SKU-00007",
                "slug": "fresho-brown-bread-safe-preservative-free-200-g",
                "supplier_id": 22,
                "sub_category": 17,
                "main_category": 3,
                "product_type": "simple",
                "status": "active",
                "cover_image": "http://easify.test/uploads/products/cover/nn.png",
                "pivot": {
                    "order_id": 2,
                    "product_id": 12
                },
                "lang": {
                    "en": {
                        "id": 23,
                        "name": "Fresho Brown Bread - Safe, Preservative ",
                        "language": "en",
                        "product_id": 12
                    },
                    "ar": {
                        "id": 24,
                        "name": "Fresho Brown Bread - Safe, Preservative",
                        "language": "ar",
                        "product_id": 12
                    }
                }
            }
        ],
        "customer": {
            "cust_name": "Reshma",
            "id": 11,
            "phone": "9995256535"
        }
    }
}
```
