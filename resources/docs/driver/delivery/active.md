# Active list

---
active  and todays list

### Details

| Method | Uri                  | Authorization |
| :----- | :------------------- | :------------ |
| GET  | `api/driver/order-delivery/active` | Yes            |

### Request Params

```json
{
    "date": "2020/1/26"//for todays delivery
}
```
### Response

```json
{
    "success": true,
    "data": {
        "current_page": 1,
        "data": [
            {
                "id": 2,
                "customer_id": 11,
                "order_id": "EAS-ORD-00002",
                "driver_id": 5,
                "delivery_loc_coordinates": "{\"latitude\":21.307239955021142,\"longitude\":40.42918906250001}",
                "delivery_loc": "Taif 26526, Saudi Arabia",
                "delivery_schedule_date": "27-January-2021",
                "time_slot": "9 AM to 1 PM",
                "order_status": "Schedule for delivery",
                "customer": {
                    "id": 11,
                    "cust_name": "Reshma",
                    "phone": "9995256535"
                }
            }
        ],
        "first_page_url": "http://easify.test/api/driver/order-delivery/active?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http://easify.test/api/driver/order-delivery/active?page=1",
        "next_page_url": null,
        "path": "http://easify.test/api/driver/order-delivery/active",
        "per_page": 100,
        "prev_page_url": null,
        "to": 1,
        "total": 1
    }
}
```
