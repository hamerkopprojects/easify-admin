# Delivered list

---
### Details

| Method | Uri                  | Authorization |
| :----- | :------------------- | :------------ |
| GET  | `api/driver/order-delivery/delivered` | Yes            |

### Response

```json
{
    "success": true,
    "data": {
        "current_page": 1,
        "data": [
            {
                "id": 1,
                "customer_id": 11,
                "order_id": "EAS-ORD-00001",
                "driver_id": 5,
                "delivery_loc_coordinates": "{\"latitude\":21.307239955021142,\"longitude\":40.42918906250001}",
                "delivery_loc": "Taif 26526, Saudi Arabia",
                "delivery_schedule_date": "24-January-2021",
                "time_slot":"2 PM 5 PM",
                "order_status": "Delivered"
            }
        ],
        "first_page_url": "http://easify.test/api/driver/order-delivery/delivered?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http://easify.test/api/driver/order-delivery/delivered?page=1",
        "next_page_url": null,
        "path": "http://easify.test/api/driver/order-delivery/delivered",
        "per_page": 100,
        "prev_page_url": null,
        "to": 1,
        "total": 1
    }
}
```
