# Mark as delivered

---
### Details

| Method | Uri                  | Authorization |
| :----- | :------------------- | :------------ |
| GET  | `driver/order-delivery/mark-as-delivered/{id}` | Yes |

### Response

```json
{
    "success": true,
    "msg": "Order delivered successfully"
}
```
