# Cancel order

---
Order Cancellation request

### Details

| Method | Uri                  | Authorization |
| :----- | :------------------- | :------------ |
| POST | `api/driver/order-delivery/cancel` | Yes            |

### Request Params

```json
{
    "order_id":2,
    "reason":2,
    "message":"no customer"

}
```
### Response

```json
{
    "success": true,
    "msg": "Order cancellation request send successfully"
}
```
