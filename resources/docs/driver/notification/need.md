# Need Notifications

---

Need notification check

### Details

| Method | Uri                 | Authorization |
| :----- | :------------------ | :------------ |
| GET    | `api/driver/check/need-notification` | Yes           |


### Response

```json
{
    "success": true,
    "data": {
        "id": 5,
        "name": "Chandru",
        "need_notification": 1
    }
}
```
