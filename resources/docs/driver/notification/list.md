# Notification list
---
### Details
| Method | Uri   | Authorization |
| : |   :-   |  :  |
| GET | `api/driver/notification/list` | yes |

### Response
```json
{
    "success": true,
    "data": {
        "current_page": 1,
        "data": [
            {
                "id": 5,
                "content": "{\"en\":\"Easify support team replied on your query hloooo\",\"ar\":\"Easify support team replied on your query hloooo\"}",
                "is_read": 0,
                "details": "{\"type\":\"support\",\"request_id\":\"12\"}",
                "created_at": "2021-01-30T08:55:33.000000Z"
            },          
           
        ],
        "first_page_url": "http://easify.test/api/driver/notification/list?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http://easify.test/api/driver/notification/list?page=1",
        "next_page_url": null,
        "path": "http://easify.test/api/driver/notification/list",
        "per_page": 15,
        "prev_page_url": null,
        "to": 5,
        "total": 5
    }
}
```
