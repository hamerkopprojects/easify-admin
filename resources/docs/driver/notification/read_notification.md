# Notification mark as read
---
### Details
| Method | Uri   | Authorization |
| : |   :-   |  :  |
| GET | `api/driver/notification/read/{id}` | yes |

### Response
```json
{
    "success": true,
    "msg": "Notification changed to read status"
}
```
