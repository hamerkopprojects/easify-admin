# Turn On / Off Notifications

---

Turn notification on / off

### Details

| Method | Uri                 | Authorization |
| :----- | :------------------ | :------------ |
| PUT    | `api/driver/notifications` | Yes           |

### Request params

```json
{
    "enable": 1 // or 0

```

### Response

```json
{
    "success": true,
    "msg": "Notification setting enabled successfully"
}
```
