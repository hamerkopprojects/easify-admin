# Support Request

---
We are here to help

### Details

| Method | Uri   | Authorization |
| : |   :-   |  :  |
| POST | `api/driver/support` | Yes |

### Request Params

```json
{
   "message":"support request driver app",
    "subject":"new testt"
}
```
### Response

```json
{
    "success": true,
    "msg": "Support request send successfully"
}
```
