# Support Chat

---
We are here to help

### Details

| Method | Uri   | Authorization |
| : |   :-   |  :  |
| GET| `api/driver/support/{request_id}` | Yes |


### Response

```json
{
    "success": true,
    "data": [
        {
            "id": 1,
            "message": "support request driver app",
            "user_type": "driver"
        },
        {
            "id": 2,
            "message": "testtt",
            "user_type": "admin"
        }
    ]
}
```
