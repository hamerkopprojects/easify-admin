# Edit Driver

---

### Details

| Method | Uri                  | Authorization |
| :----- | :------------------- | :------------ |
| GET   | `api/driver/my-details` | YES         |



### Response

```json
{
    "success": true,
    "data": {
        "name": "chandhu",
        "phone": "9747714167",
        "email": "example@gmail.com"
    }
}
```
