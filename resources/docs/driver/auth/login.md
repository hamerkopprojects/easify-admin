# Get OTP

---

 Login using OTP

### Details

| Method | Uri                  | Authorization |
| :----- | :------------------- | :------------ |
| POST   | `api/driver/login` | No            |

### Request Params

```json
{
    "phone": "9747714167"
}
```

### Response

```json
{
    "success": true,
    "msg": "Otp send successfully"
}
```
