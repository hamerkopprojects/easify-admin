# Update  Driver

---

### Details

| Method | Uri                  | Authorization |
| :----- | :------------------- | :------------ |
| GET   | `api/driver/update-me` | YES         |


### Request Params

Form Data

| param | value                          |
| :---- | :----------------------------- |
| photo | image                          |
| name  | name                           |
|email  |  email                         |


### Response

```json
{
    "success": true,
    "msg": "Profile updated succesfully"
}
```
