# Verify OTP

---

Verify OTP

### Details

| Method | Uri                         | Authorization |
| :----- | :-------------------------- | :------------ |
| POST   | `api/driver/verify`         | No            |

### Request Params

```json
{
    "phone": "9747714167",
    "otp": "1234",
    "fcm_token": "fdgrghajdhaskfldhgkagfagwifhwe"
}
```

### Response

```json
{
    "success": true,
    "data": {
        "user": {
            "id": 5,
            "name": "chandhu",
            "email": "example@gmail.com",
            "email_verified_at": null,
            "fcm_token": "hghCGhxvbzncbnzcvnbjhcjchhfj",
            "verify_token": null,
            "ip_address": null,
            "last_login_date": null,
            "last_activity_date": null,
            "role_id": 3,
            "phone": "9747714167",
            "country_code": null,
            "otp_generated_at": "2021-01-25 08:44:26",
            "otp": "1234",
            "user_id": "EAS-USER-00000",
            "status": "active",
            "language": "en",
            "national_id": "yu9875",
            "need_notification": 1
        },
        "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMzI4ZDEwNWY0MTVlNjk4YWEzYTQ0YmQ1MjMzNDI5OWE1ZmE4Njc0OTdmYjdlODMzYzE5OWFhMWFjYzliMDg5NjBjYzZiOGM0ODRjOTI0OWYiLCJpYXQiOjE2MTE1NjQyOTAsIm5iZiI6MTYxMTU2NDI5MCwiZXhwIjoxNjQzMTAwMjkwLCJzdWIiOiI1Iiwic2NvcGVzIjpbInVzZXIiXX0.Hf605ROZYQpn1qPIvxr6E7JQhp2xtY4UKgcXrILxrRaf7YrbF5i5KlRRZ6P_NHeXa8lwT_S3ngdZyTy8e8BXpxXFh7yIQJf8OVTjkNm4bW85RhNWTpgMAoRJgp4FYRzwfFt-rw6XJcCGjVQDyWpqm6xqJJTICPZNSTZ4CxNyiEi028MybFSRUebPfz_H3LUnI2znLI5845g2NMIBI2WqvbkvDFeg_9c-bpT8JvskogwM2i1Xw14Y7Xkw3le1QRHxqoKsxL7p4zGH-IOnFhTUxTkI-ScbBWlkLtv_y6ehWyfkgif5C3QGCuRh2r7fCNn4O1_67JhOqhU3lYfCfrGI49qN7q4bNMbLXwORQUQpcwJkMIUvJvys000W4tLSn8i2FbaK03cB8DaUekx5NhoOQFPMqQGPyt3utEEFxZlHifUIsuwm0Kxzh5ljXUUH0GpDAH_-MHWk13qg1SMDZPaB5AACIVS0qswXO8HUALsRlMVeLXdG0j_98Z2teErtI_ypLA5KQJEsme51g_NRGITSzYoL0mIniBYT7Ekzm-0lXXR1ELutJsOMABr6EcpJoP2rzXOOjoGadFmdZrVPcNsRkniBHCi4Tp_GO2CInf8Y5pPsfO80p07_agc6oGutDv0h9stHJnIymUXAhsFyX8xgqnbh_83YKJPYUuxnUYLsdEI"
    }
}
```
