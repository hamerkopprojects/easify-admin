- ## Get Started
    - [Overview](/{{route}}/{{version}}/overview)

- ## Lang switch 
    - [Lang swich](/{{route}}/{{version}}/appdata/lang)
- ## App data 
    - [Privacy policies](/{{route}}/{{version}}/appdata/privacy)
    - [Terms and conditions ](/{{route}}/{{version}}/appdata/terms)
    - [FAQ ](/{{route}}/{{version}}/appdata/faq)
    - [How to use app ](/{{route}}/{{version}}/appdata/useapp)
    - [Cancellation reasons](/{{route}}/{{version}}/appdata/cancellation)

- ## Login
    - [Get Otp](/{{route}}/{{version}}/auth/login)
    - [Verify Otp](/{{route}}/{{version}}/auth/verify)
    - [Logout](/{{route}}/{{version}}/auth/logout)
    - [resend](/{{route}}/{{version}}/auth/resend)
- ## Collection 
    - [Todays and all collection](/{{route}}/{{version}}/collection/collection)
    - [Order Details](/{{route}}/{{version}}/collection/details)
    - [Mark as collected](/{{route}}/{{version}}/collection/mark-as-collected)
    - [At warehoue](/{{route}}/{{version}}/collection/atwarehouse)
    - [Shelf list](/{{route}}/{{version}}/collection/shelflist)
- ## Delivery 
    - [Active and todays order](/{{route}}/{{version}}/delivery/active)
    - [Delivered](/{{route}}/{{version}}/delivery/delivered)
    - [Order Details](/{{route}}/{{version}}/delivery/details)
- ## Profile 
    - [Edit](/{{route}}/{{version}}/auth/edit-user)
    - [Update](/{{route}}/{{version}}/auth/update-user)
- ## Support 
    - [Request a support](/{{route}}/{{version}}/support/request)
    - [Support chat](/{{route}}/{{version}}/support/support-chat)
    - [Send new message](/{{route}}/{{version}}/support/new-chat)
    

    
    
