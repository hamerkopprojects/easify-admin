#Collection order details

---


### Details

| Method | Uri                  | Authorization |
| :----- | :------------------- | :------------ |
| GET | `api/driver/order-collection/details/{id}` | Yes            |

### Response

```json
{
    "success": true,
    "data": {
        "id": 4,
        "users_id": 1,
        "customer_id": 11,
        "order_id": "EAS-ORD-00004",
        "items": [
            {
                "id": 5,
                "order_id": 4,
                "product_id": 11,
                "variant_id": null,
                "product": {
                    "id": 11,
                    "sku": "SKU-00006",
                    "product_type": "simple",
                    "status": "active",
                    "lang": [
                        {
                            "id": 21,
                            "product_id": 11,
                            "name": "Fresho Whole Wheat Bread - Safe, Preservative Free, 400 g",
                            "description": "Freshly Baked bread is one of life's"
                            
                        },
                        {
                            "id": 22,
                            "product_id": 11,
                            "name": "Fresho Whole Wheat Bread - Safe, Preservative Free, 400 g",
                            "description": "Freshly Baked bread is one of life's ",
                            
                        }
                    ]
                },
                "branch": {
                    "id": 22,
                    "code": "EAS-SUPP-00008",
                    "name": "vellayil retail",
                    "phone": "859656865"
                }
            }
        ]
    }
}
```
