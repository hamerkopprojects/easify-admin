# Shelf list

---


### Details

| Method | Uri                  | Authorization |
| :----- | :------------------- | :------------ |
| GET | `api/driver/order-collection/shelflist` | Yes            |


### Response

```json
{
    "success": true,
    "data": {
        "1": "Shelf-1",
        "2": "Shelf-2",
        "3": "Shelf-3",
        "4": "Shelf-4",
        "5": "Shelf-5"
    }
}
```
