# Product at warehouse

---

### Details

| Method | Uri                  | Authorization |
| :----- | :------------------- | :------------ |
| POST | `api/driver/order-collection/at-warehouse` | Yes            |

### Request Params

```json
{
    "order_id":5,
    "id":6,
   "shelf_id":2
}
```
### Response

```json
{
    "success": true,
    "msg": "Status Updated Succesfully"
}
```
