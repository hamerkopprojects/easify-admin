#Collection list

---
Collection  and todays collection list

### Details

| Method | Uri                  | Authorization |
| :----- | :------------------- | :------------ |
| GET | `api/driver/order-collection` | Yes            |

### Request Params

```json
{
    "date": "2020/1/26"//for todays collection
}
```
### Response

```json
{
    "success": true,
    "data": {
        "current_page": 1,
        "data": [
            {
                "order_status": "Collection pending",
                "collection_date": "26-January-2021",
                "collection_time_slot": "9 AM to 1 PM"
            },
            {
                "order_status": "Collection pending",
                "collection_date": "26-January-2021",
                "collection_time_slot": "9 AM to 1 PM"
            }
        ],
        "first_page_url": "http://easify.test/api/driver/order-collection?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http://easify.test/api/driver/order-collection?page=1",
        "next_page_url": null,
        "path": "http://easify.test/api/driver/order-collection",
        "per_page": 100,
        "prev_page_url": null,
        "to": 3,
        "total": 3
    }
}
```
