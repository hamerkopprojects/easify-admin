# Mark as collected

---
Product mark as collected

### Details

| Method | Uri                  | Authorization |
| :----- | :------------------- | :------------ |
| POST | `api/driver/order-collection/status-update` | Yes            |

### Request Params

```json
{
    "order_id":5,// order id
    "id":6, // item id
    "status":"collected"
}
```
### Response

```json
{
    "success": true,
    "msg": "Status Updated Succesfully"
}
```
