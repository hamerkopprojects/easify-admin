# Overview

---

-   [Base Url](/{{route}}/{{version}}/overview#base-url)
-   [Authorization](/{{route}}/{{version}}/overview#authorization)
-   [Response Structure](/{{route}}/{{version}}/overview#response)

<a name="base-url"></a>

## Base Url

<larecipe-badge type="primary" rounded>Dev</larecipe-badge>

```text
https://esy.ourdemo.online/api/
```

<larecipe-badge type="primary" rounded>Live</larecipe-badge>

```text
https://easify.ourdemo.online/api/
```


<a name="authorization"></a>

## Authorization

> {primary} Authorization is done through headers

| Header        | Value        |
| :------------ | :----------- |
| Authorization | Bearer token |

<a name="response"></a>

## Response Structure

<larecipe-badge type="success" rounded>Success</larecipe-badge>

```json
{
    "success": true,
    "msg": "Message content"
}
```

```json
{
	"success": true,
	"data": {
		...
	}
}
```

<larecipe-badge type="danger" rounded>Error</larecipe-badge>

```json
{
    "success": false,
    "error": {
        "msg": "invalid token"
    }
}
```
