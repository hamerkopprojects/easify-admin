# QR Code scan

---

Scan QR code

### Details

| Method | Uri                 | Authorization |
| :----- | :------------------ | :------------ |
| POST    | `api/driver/scan-qr` | Yes           |

### Request params

```json
{
   "code":"21-14-22"

```

### Response

```json
{
    "success": true,
    "data": {
        "order_id": "14",
        "status": "Collection completed"//case of collection
    }
}
```
