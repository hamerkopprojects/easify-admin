@extends('frontend.frontend_layouts.frontend_app')
@section('content')
<div id="content" class="bg-light page-cart">
    
    @include('frontend.cart.cart_content')

</div>

@endsection