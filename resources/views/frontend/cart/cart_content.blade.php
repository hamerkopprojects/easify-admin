<div class="container">

    <div class="modal" id="CartModal">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">

            </div>
        </div>
    </div>

</div>
<div id="content" class="bg-light page-cart">
    <div class="container">
        <div id="breadcrumbs">
            <p><a href="{{ route('frontend') }}">{{__('frontend.home')}}</a> <i class="fa fa-long-arrow-right" aria-hidden="true"></i> <span>{{__('frontend.cart')}}</span></p>
        </div>
        <div class="row">
            <div class="col-lg-8">

                <div class="box1">
                    @php
                    $count = isset($cartItems->parent->items) ? count($cartItems->parent->items) : 0;
                    $customer_id = \Session::get('customer_id');
                    @endphp
                    <h1 class="">{{__('frontend.cart_items')}} @if($count > 0)({{ $count }})@endif </h1>
                    <a href="javascript:void(0);" class="button1b @if( ! $customer_id) hidden @endif" onclick="event.preventDefault(); getSaveLater();"><i class="fa fa-bookmark-o" aria-hidden="true"></i> &nbsp; &nbsp; {{__('frontend.save_for_later')}}</a>
                    <a href="javascript:void(0);" class="button1b @if( ! $customer_id) hidden @endif" onclick="event.preventDefault(); getFromWishlist();"><i class="fa fa-heart-o" aria-hidden="true"></i>&nbsp; {{__('frontend.add_from_wishlist')}}</a>
                    <div class="clearfix"></div>
                    <table class="cart">
                        <tbody>
                            @if(isset($cartItems->parent->items))
                            @foreach($cartItems->parent->items as $item)
                            @php
                            $name = $item->product->lang[0]->name ?? '';
                            @endphp
                            <tr>
                                <td class="select-product">
                                    <input class="custom-checkbox" type="checkbox" id="product2" />
                                    <label for="product2" aria-describedby="label">&nbsp;</label>
                                </td>
                                <td class="product-image">
                                    <figure>
                                        @if(is_file( public_path() . '/uploads/' . $item->product->cover_image))
                                        <img src="{{ url('uploads/'.$item->product->cover_image) }}" alt="Product">
                                        @else
                                        <img src="{{ asset('assets/frontend/custom/images/no-image.png') }}" alt="Product">
                                        @endif
                                    </figure>
                                </td>
                                <td class="product-details">
                                    <div class="actions">
                                        <a href="javascript:void(0);" onclick="removeProduct(this, {{ $item->product->id }}, '{{ $item->product->product_type }}', {{ $item->variant_id }})" class=" remove"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                        <a href="javascript:void(0);" class="button1" onclick="event.preventDefault(); saveLater(this, {{ $item->product->id }}, '{{ $item->product->product_type }}', {{ $item->variant_id }})"><i class="fa fa-bookmark-o" aria-hidden="true"></i></a>
                                        <div class="clearfix"></div>
                                        @php
                                        $d_price = NULL;
                                        $price = NULL;
                                        foreach($item->product->variant_price as $variants) {
                                        if($variants->variant_lang_id == $item->variant_id) {

                                        $discount_price = $variants->discount_price ?? NULL;
                                        $act_price = $variants->price ?? NULL;

                                        $commission = isset($item->product->commission) ? $item->product->commission->value : NULL;
                                        $mark_up = isset($variants->easify_markup) ? $variants->easify_markup : NULL;
                                        $dis_commission = ($discount_price * $commission / 100);
                                        $price_commission = ($act_price * $commission / 100);
                                        $dis_markup = ($discount_price * $mark_up / 100);
                                        $price_markup = ($act_price * $mark_up / 100);
                                        $d_price = $discount_price + $dis_commission + $dis_markup;
                                        $price = $act_price + $price_commission + $price_markup;
                                        }
                                        }
                                        @endphp
                                        <p class="pprice">
                                            @if($d_price)
                                            <del class="actual-price">{{$currency[$lang]}} {{ number_format($price, 2, ".", "") }} </del>
                                            <strong class="discount-price"> {{$currency[$lang]}} {{ number_format($d_price, 2, ".", "") }}</strong>
                                            @else
                                            <strong class="discount-price"> {{$currency[$lang]}} {{ number_format($price, 2, ".", "") }}</strong>
                                            @endif
                                        </p>
                                    </div>
                                    <h2><a href="javascript:void(0);">{{ $name }}</a></h2>
                                    @if($item->product->product_type == 'complex' && $item->product->variant_price)

                                    <span class="select-box fleft7">
                                        <select class="form-control" onchange="changeDisplyaPrice(this)" disabled="disabled">
                                            @foreach($item->product->variant_price as $variants)
                                            @if($lang == 'ar')
                                            @php
                                            $variant_ar = \App\Models\VariantLang::where('en_matching_id', $variants->variant_lang->id)->pluck('name')->first();
                                            $variant_name = $variant_ar;  @endphp
                                            @else
                                            @php $variant_name = $variants->variant_lang->name ?? ''; @endphp
                                            @endif
                                            @php
                                            $discount_price = $variants->discount_price ?? NULL;
                                            $act_price = $variants->price ?? NULL;
                                            $commission = isset($item->product->commission) ? $item->product->commission->value : NULL;
                                            $mark_up = isset($variants->easify_markup) ? $variants->easify_markup : NULL;
                                            $dis_commission = ($discount_price * $commission / 100);
                                            $price_commission = ($act_price * $commission / 100);
                                            $dis_markup = ($discount_price * $mark_up / 100);
                                            $price_markup = ($act_price * $mark_up / 100);
                                            $d_price = $discount_price + $dis_commission + $dis_markup;
                                            $price = $act_price + $price_commission + $price_markup;

                                            @endphp
                                            <option
                                                @if($item->variant_id == $variants->variant_lang_id) selected="selected" @endif
                                                price="{{ $price }}"
                                                d_price="{{ $d_price }}"
                                                value="{{ $variants->variant_lang_id }}">{{ $variant_name }}</option>
                                            @endforeach
                                        </select>

                                    </span>
                                    @endif
                                    <div class="quantity">
                                        <input type="button" value="-" class="minus" onclick="update_cart(this, {{ $item->product_id }}, '{{ $item->product->product_type }}', '{{ $item->variant_id }}', - 1)">
                                        <input type="number" class="qty product-qty" step="1" min="0" max="" value="{{ $item->item_count }}">
                                        <input type="button" value="+" class="plus" onclick="update_cart(this, {{ $item->product_id }}, '{{ $item->product->product_type }}', '{{ $item->variant_id }}', 1)">
                                    </div>
                                    <div class="clearfix"></div>
                                    <!--<p class="Error">Min quantity is 10</p>-->


                                </td>
                            </tr>
                            @endforeach
                            @endif
                            @if( !isset($cartItems->parent->items) || count($cartItems->parent->items) ==0 )
                            <tr class="prdt">
                                <td class="select-product">
                                    <input class="custom-checkbox" type="checkbox" id="product2" />
                                    <label for="product2" aria-describedby="label">&nbsp;</label>
                                </td>
                                <td class="product-details">
                                    <h2>{{__('frontend.no_tems_add_cart')}}</h2>
                                </td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            @if($count > 0)
            <div class="col-lg-4">
                <div class="box1 mt-0 p-0">
                    <h2 class="mt-44">{{__('frontend.order_details')}}</h2>


                    <table>
                        <tbody>
                            <tr>
                                <th><b>{{__('frontend.sub_total')}} </b></th>
                                @php
                                $sub_total = $cartItems->parent->sub_total ?? 0;
                                $final_amount = $sub_total;
                                if(isset($cartItems->parent->coupon_code)){
                                $final_amount = $final_amount - $cartItems->parent->coupon_value;
                                $coupon_value = number_format($cartItems->parent->coupon_value, 2, ".", "");
                                }
                                $delivary_charge = $delivary_charge ?? 0;
                                $vat_percent = isset($vat) ? $final_amount * $vat / 100 : 0;
                                $final_amount = $final_amount + $delivary_charge + $vat_percent;
                                @endphp
                                <td data-title="Sub total">{{$currency[$lang]}} {{ $sub_total }}</td>
                            </tr>
                            @if(!empty($delivary_charge))
                            <tr>
                                <th>{{__('frontend.delivery_charges')}}</th>
                                <td data-title="Delivery Charge">{{$currency[$lang]}} {{ number_format($delivary_charge, 2, ".", "") }}</td>
                            </tr>
                            @endif
                            @if(!empty($vat))
                            <tr>
                                <th>{{__('frontend.vat')}}</th>
                                <td data-title="Delivery Charge">{{ $vat }} %</td>
                            </tr>
                            @endif
                            <tr class="cart-location">
                                <td class="pt-3" colspan="2">
                                    <p>{{__('frontend.hav_coupen')}} </p>
                                    <input placeholder="{{__('frontend.cpn_code')}}" class="form-control" name="coupon_code" id="coupon_code" value="{{$cartItems->parent->coupon_code ?? ''}}"> <a href="javascript:void(0);" class="Apply" onclick="event.preventDefault(); CouponApply(this)">{{__('frontend.apply')}}</a>
                                </td>
                            </tr>
                            @if(isset($cartItems->parent->coupon_code))
                            <tr>
                                <th>{{__('frontend.discount')}}</th>
                                <td data-title="Delivery Charge">{{$currency[$lang]}} {{ $coupon_value ?? '' }}</td>
                            </tr>
                            @endif
                            <tr class="order-total baboth">
                                <th>{{__('frontend.total_pay_amnt')}}<span>({{__('frontend.inclusive_vat')}})</span>
                                </th>
                                <td data-title="Total">{{$currency[$lang]}} {{ number_format($final_amount, 2, ".", "") }}
                                </td>
                            </tr>
                            <tr class="cart-notes">
                                <td colspan="2">
                                    <textarea placeholder="{{__('frontend.order_note')}}" name='order_note' id="order_note">{{$cartItems->parent->note ?? ''}}</textarea>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <a href="#" onclick="checkLogin()" class="checkout-button button1">{{__('frontend.review_chk')}}</a>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
<!-- Common script -->
@include('frontend.common.common_script')

<script>
    function checkLogin() {
    const name = $('.cust-name').text();
    if (name == 'Hello, Sign in') {
    $.toaster({
    priority : 'danger',
    title: @json(__('frontend.error') ),
    message  : @json(__('frontend.pls_cntiue')),
    timeout  : 3000,
    });
    $('#myModal').modal('show');
    } else {
    var order_note = $('#order_note').val();
    $('.ajax-loading-buz').show();
    $.ajax({
    type: "GET",
            url: "{{ route('cart.order_note') }}",
            data: {
            order_note: order_note,
            },
            success: function (data) {
            if (data.success == 1) {
            location.href = "{{ route('cart.checkout') }}";
            } else {
            $.toaster({
            priority : 'danger',
            title    : @json(__('frontend.error')),
            message  : data.message,
            timeout  : 3000,
            });
            }
            },
            complete: function() {
            $('.ajax-loading-buz').hide();
            }
    });
    }
    }

    function changeDisplyaPrice(item) {
    const price = $(item).find("option:selected").attr('price');
    const d_price = $(item).find("option:selected").attr('d_price');
    $(item).closest('tr.prdt').find('.actual-price').text('{{$currency[$lang]}} ' + price);
    $(item).closest('tr.prdt').find('.discount-price').text(' {{$currency[$lang]}} ' + d_price);
    }
</script>