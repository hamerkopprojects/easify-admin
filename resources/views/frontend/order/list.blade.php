@extends('frontend.frontend_layouts.frontend_app')
@section('content')
<div id="content" class="bg-light page-cart pagevart">
    <div class="container" id="ord_div">
        @if($from == 'details')
        @include('frontend.order.details')
        @else
        @include('frontend.order.filter')
        @endif
    </div>
</div>

@include('frontend.common.order_script')
@include('frontend.common.common_script')
@endsection
@push('css')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.css">
<style>
    .nav-pills li {
        width: 33.33% !important
    }
</style>
@endpush