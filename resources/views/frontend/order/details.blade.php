@php
if(in_array($order->order_status,[1,2]))
{
$status= $ord_web_status[1];
$cls='text-danger';
}else if($order->order_status == 7)
{
$status= $ord_web_status[3];
$cls="text-success";
}else if(in_array($order->order_status,[3,4,5])){
$status= $ord_web_status[2];
$cls="text-success";
}
else if($order->order_status == 6){
$cls = "text-danger";
$status =$ord_status['6'];
}else if($order->order_status == 9){
$cls = "text-success";
$status = $ord_status['9'];
}
@endphp
<div class="row">
    <div class="col-lg-12">
        <div id="breadcrumbs">
            <p><a href="{{ route('frontend') }}">{{__('frontend.home')}}</a> <i class="fa fa-long-arrow-right" aria-hidden="true"></i> <span>{{__('frontend.my_orders')}}
                </span></p>
        </div>
    </div>
    <div class="col-lg-12 mt-4 mb-4 mres15">
        <div class="row">
            <div class="col-xs-3 col-md-3  col">
                <h4 class="mb-2">{{__('frontend.single_order')}}</h4>
            </div>
            <div class="col-xs-9 col-md-9 col">
                <div class="quleft ">
                    <span> <a href="{{route('order.list')}}"><img src="{{asset('img/back.png')}}"></a> </span>
                </div>
            </div>
        </div>
    </div>
    @include('frontend.myaccount.sidebar')
    <div class="col-lg-9">
        <div class="cardfull">
            <div class="col-md-12 mt-2">
                <div class="well well-sm mb-3">
                    <div class="row">
                        <div class="col-xs-4 col-md-4 col">
                            <h4>{{$order->order_id}}</h4>
                        </div>
                        <div class="col-xs-8 col-md-8 col">
                            <div class="quleft ">
                                <p><b class="{{$cls}} fright mb-3"><i class="fa fa-circle" aria-hidden="true"></i>  {{ $status }}</b></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cardfull">
            <h5>{{$order->item_count}} {{__('frontend.items')}}</h5>
        </div>
        <div class="well well-sm">
            <div>
                <table>
                    @if(isset($order->product))
                    @foreach ($order->product as $item)
                    <tr class="cardorder">
                        <td>
                            {{-- <figure> --}}
                            @if(is_file( public_path() . '/uploads/' . $item->cover_image))
                            <img src="{{ url('uploads/'.$item->cover_image) }}" alt="Product">
                            @else
                            <img src="{{ asset('assets/frontend/custom/images/no-image.png') }}" alt="Product">
                            @endif
                            {{-- </figure> --}}
                        </td>
                        <td>{{$item->lang[0]->name}}</td>
                        <td>
                            @php
                            $single =$item->pivot->grant_total /$item->pivot->item_count ;
                            @endphp
                            <span>{{ ($lang == 'ar') ? json_decode($settings->currency)->currency_ar : json_decode($settings->currency)->currency_en }}
                                {{number_format($single, 2, ".", "")}} X {{$item->pivot->item_count}}</span>
                        </td>
                        <td><b>{{ ($lang == 'ar') ? json_decode($settings->currency)->currency_ar : json_decode($settings->currency)->currency_en }}
                                {{number_format($item->pivot->grant_total, 2, ".", "")}} </b>
                        </td>
                        <td>
                            @if($order->order_status == 9 && $settings->rating == 'enable')
                            <button type="button" class="btn btn-success text-white fright"
                                    onclick="reviewProduct({{$item->id}})">{{__('frontend.review_pdt')}}
                            </button>
                            @endif
                        </td>
                    </tr>
                    {{-- <br><br> --}}
                    @endforeach
                    @endif
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 p-0 padres">
                <div class="text-left box1 ">
                    <div class="halfadd">
                        <h1>{{__('frontend.del_add')}}</h1>
                        <p>{{$order->delivery_name}}</p>
                        <p>{{$order->delivery_address}}</p>
                        <p>{{$order->delivery_city}}</p>
                        <p>P O Box: {{$order->delivery_zip}}</p>
                        <p>{{$order->delivery_country}}</p>
                        <p>{{$order->delivery_phone}}</p>
                    </div>
                    <hr>
                    <div class="halfadd ">
                        <h1>{{__('frontend.bill_add')}}</h1>
                        <p>{{$order->billing_name}}</p>
                        <p>{{$order->billing_address}}</p>
                        <p>{{$order->billing_city}}</p>
                        {{-- <p>{{$order->delivery_name}}</p> --}}
                        <p>P O Box: {{$order->billing_zip}}</p>
                        <p>{{$order->billing_country}}</p>
                        <p>{{$order->billing_phone}}</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 p-0res">
                <div class="text-left box1 halfres">
                    <h2>{{__('frontend.order_sum')}}</h2>
                    <hr />
                    <table>
                        <tbody>
                            @php
                            $order_total = $order->sub_total + $order->delivery_charge;
                            $final_amount = $order->sub_total;
                            if(isset($order->coupon_code)){
                            if($order->coupon_type == 'amount'){
                            $coupon_value = $currency[$lang]. ' '.number_format($order->coupon_value, 2, ".", "");
                            }
                            else {
                            $coupon_value = $final_amount * $order->coupon_value / 100;
                            $coupon_value =$currency[$lang]. ' '.number_format($coupon_value, 2, ".", "");
                            }
                            }
                            @endphp
                            <tr>
                                <th>{{__('frontend.sub_total')}}</th>
                                <td data-title="Subtotal">
                                    {{ ($lang == 'ar') ? json_decode($settings->currency)->currency_ar : json_decode($settings->currency)->currency_en }}
                                    {{$order->sub_total}}
                                </td>
                            </tr>
                            @if(isset($order->delivery_charge))
                            <tr>
                                <th>{{__('frontend.del_charge')}}</th>
                                <td data-title="Delivery Charge">
                                    {{ ($lang == 'ar') ? json_decode($settings->currency)->currency_ar : json_decode($settings->currency)->currency_en }}
                                    {{$order->delivery_charge}}
                                </td>
                            </tr>
                            @endif
                            @if(isset($order->vat))
                            <tr>
                                <th>{{__('frontend.vat')}}</th>
                                <td data-title="Vat">
                                    {{$order->vat}} %
                                </td>
                            </tr>
                            @endif
                            @if($order->payment_method =='cod')
                            <tr>
                                <th>{{__('frontend.cod_fee')}}</th>
                                <td data-title="cod">
                                    {{ ($lang == 'ar') ? json_decode($settings->currency)->currency_ar : json_decode($settings->currency)->currency_en }}
                                    {{$order->cod_fee}}
                                </td>
                            </tr>
                            @endif
                            @if(isset($order->coupon_code))
                             <tr>
                                <th>{{__('frontend.discount') }} </th>
                                <td data-title="Discount">{{ $coupon_value ?? '' }}</td>
                            </tr>
                            @endif
                            <tr class="order-total baboth">
                                <th>{{__('frontend.tot_amo')}} <span>({{__('frontend.inclusive_vat')}})</span>
                                </th>
                                <td data-title="Total">
                                    {{ ($lang == 'ar') ? json_decode($settings->currency)->currency_ar : json_decode($settings->currency)->currency_en }}
                                    {{$order->grant_total}}
                                </td>
                            </tr>
                            <tr>
                                <th>{{__('frontend.pay_method')}}</th>
                                <td data-title="COD Fee">{{strtoupper($order->payment_method)}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 p-0 mt-4">
                @if($order->order_note)
                <div class="well well-sm mb-3 cardfull bg-secondary text-white">
                    <div class="row">
                        <div class="col-lg-12 ">
                            <h4>{{__('frontend.ord_note')}}</h4>
                            <p>{{$order->order_note}}</p>
                        </div>
                    </div>
                </div>
                @endif
                <div class="pb-5 text-right">
                    <button type="button" class="btn btn-success btnnormal" data-toggle="modal" data-target="#CONTACT">
                        {{__('frontend.contact_supp_caps')}}
                    </button>
                    @if($order->order_status ==9 && $settings->rating == 'enable')
                    <button type="button" class="btn btn-dark btnnormal text-white" data-toggle="modal"
                            data-target="#REVIEW">{{__('frontend.review_order')}}</button>
                    @endif
                    @php $cancel_time = json_decode($settings->driver_loc_fetch_interval)->cancel_time;@endphp
                    @if($order->order_status == 1 && strtotime($order->created_at) > strtotime("-{$cancel_time} minutes"))
                    <button type="button" class="btn btn-dark btnnormal text-white" data-toggle="modal"
                            data-target="#CANCEL">{{__('frontend.canc_ord')}}</button>
                    @endif
                </div>
                <div class="modal " id="CONTACT">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="row helpwe">
                                    <div class="col-lg-12 "><a href="" class="close" data-dismiss="modal"><img
                                                src="{{asset('assets/images/close.png')}}" class="imgclose"></a>
                                        <h4>{{__('frontend.help_here')}}</h4>
                                    </div>
                                    <div class="col-lg-6 ">
                                        <p class="textright"> {{__('frontend.contact_us_on')}} :</p>
                                    </div>
                                    <div class="col-lg-6">
                                        <p><i class="fa fa-phone" aria-hidden="true"></i> 002549090</p>
                                        <p><i class="fa fa-envelope-o" aria-hidden="true"></i> suppot.easify.com</p>
                                    </div>
                                    <div class="col-lg-12">
                                        <form class="contact_support_form" id="contact_support_form">
                                            <div class="col-lg-12">

                                                <div class="form-group form-group1">
                                                    <input type="text" class="form-control formd"
                                                           value="{{__('frontend.complaint_on_order')}} : {{$order->order_id}}"
                                                           placeholder="{{__('frontend.complaint_on_order')}} :123456" name="subject">
                                                </div>
                                                <textarea class="form-control" col="5" placeholder="{{__('frontend.ent_com')}}"
                                                          name="comment"></textarea>
                                            </div>
                                            <div class="col-lg-12">
                                                <input type="hidden" name="order_id" value="{{$order->id}}">
                                                <button type="submit" class="btn btn-success w100"
                                                        id="send_contact_support">{{__('frontend.send')}}</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal " id="REVIEW">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <form id="frm_order_rating">
                                    <div class="mb-3"><a href="" class="close" data-dismiss="modal"><img
                                                src="{{asset('assets/images/close.png')}}" class="imgclose"></a>
                                        <h4>{{__('frontend.add_rivw')}}</h4>
                                    </div>
                                    @foreach ($segments as $item)
                                    <div class="row">
                                        <div class="col-lg-12">

                                            <label class="control-label mt-2">{{$item->lang[0]->name}}</label>

                                            <div style="display:inline-block;" class="order_rateYo" id="order_rateYo"
                                                 data-name={{$item->id}}></div>


                                        </div>
                                    </div>
                                    @endforeach

                                    <div class="row helpwe">
                                        <div class="col-lg-12 mt-4">
                                            <label class="control-label " for="email">{{__('frontend.your_rev')}}</label>
                                            <textarea class="form-control" col="5" placeholder="Type here"
                                                      name="order_review"></textarea>
                                        </div>
                                        <div class="col-lg-12">
                                            <input type="hidden" name="order_id" id="order_id" value="{{$order->id}}" />
                                            <input type="hidden" name="order_rating_array" id="order_rating_array">
                                            <button type="submit" class="btn btn-success w100"
                                                    id="order_form_submit">{{__('frontend.submit')}}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal " id="CANCEL">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <form id="frm_cancel_reason">
                                    <div class="row helpwe">
                                        <div class="col-lg-12 "><a href="" class="close" data-dismiss="modal"><img
                                                    src="{{asset('assets/images/close.png')}}" class="imgclose"></a>
                                            <h4>{{__('frontend.rsn_fr_cncl')}}</h4>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <select class="form-control" id="cancel_reason_id" name="cancel_reason_id">
                                                    <option value="">{{__('frontend.slt_cncl_rsn')}}</option>
                                                    @foreach ($cancellationReason as $cancel)
                                                    <option value="{{$cancel->id}}">{{$cancel->lang[0]->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <textarea class="form-control" col="5" placeholder="Type here" name="comment"
                                                      id="comment"></textarea>
                                        </div>

                                        <div class="col-lg-12" style="margin-top:15px;">
                                            <input type="hidden" name="order_id" id="order_id" value="{{ $order->id }}">
                                            <button type="submit" class="btn btn-success w100"
                                                    id="order_cancel_add">{{__('frontend.done')}}</button>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal" id="REVIEW_PRODUCT">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <form id="frm_product_rating">
                                    <div class="mb-3"><a href="" class="close" data-dismiss="modal">
                                            <img src="{{asset('assets/images/close.png')}}" class="imgclose"></a>
                                        <h4>{{__('frontend.add_rivw')}}</h4>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div style="display:inline-block;" id="rateYo"></div>
                                            <label class="control-label"></label>
                                            <input type="hidden" name="rating_input" id="rating_input" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="row helpwe">
                                        <div class="col-lg-12 mt-4">
                                            <label class="control-label " for="email">{{__('frontend.your_rev')}}</label>
                                            <textarea class="form-control" col="5" placeholder="Type here"
                                                      name="review"></textarea>
                                        </div>
                                        <div class="col-lg-12">
                                            <input type="hidden" name="product_id" id="product_id" />
                                            <input type="hidden" name="order_id" id="order_id" value="{{$order->id}}" />
                                            <button type="submit" class="btn btn-success w100"
                                                    id="save_product_rating">{{__('frontend.submit')}}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $("#rateYo").rateYo({
    halfStar: true,
            rating: 0,
            minValue: 0,
            maxValue: 5,
            onSet: function (rating, rateYoInstance) {
            $('#rating_input').val(rating);
            }

    });
    var itemArray = [];
    $('.order_rateYo').rateYo({
    rating: 0,
            fullStar: true,
            minValue: 0,
            maxValue: 5,
            starWidth: "20px",
            onSet: function (rating, rateYoInstance) {
            let item = $(this).data('name');
            obj = {};
            obj.segment_id = item;
            obj.rating = rating;
            itemArray.push(obj);
            }

    })
</script>