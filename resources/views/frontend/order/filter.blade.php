<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div id="breadcrumbs">
                <p><a href="{{ route('frontend') }}">{{__('frontend.home')}}</a> <i class="fa fa-long-arrow-right" aria-hidden="true"></i> <span>{{__('frontend.my_orders_bread_crump')}}</span></p>
            </div>
        </div>
        <div class="col-lg-12 mt-4 ">
            <div class="row">
                <div class="col-xs-3 col-md-3  col">
                    <h4 class="mb-2"> {{__('frontend.my_orders_head')}}</h4>
                </div>
                @if($customer_branchs->is_branch == 'N')
                <div class="col-xs-9 col-md-9 col">
                    <div id="filter_branch">
                        <span class="select-box alignsel">
                            <select class="form-control branchss"  onchange="branchFilter(this.value)">
                                <option select="" value="{{$customer_branchs->id}}"> <img src="assets/img/filter.png">{{$customer_branchs->cust_name}}</option>
                                @foreach ($customer_branchs->branch as $item)
                                @if($item->customer_id !== $customer_branchs->id )
                                <option value="{{$item->customer_id}}" {{$customer_id == $item->customer_id ? 'selected' : ''}}>{{$item->contact_name}}</option>
                                @endif
                                @endforeach
                            </select>
                        </span>
                    </div>
                </div>
                @endif
            </div>
        </div>
        @include('frontend.myaccount.sidebar')
        <div class="col-lg-9 mt-4">
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home"
                       role="tab" aria-controls="pills-home" aria-selected="true">{{__('frontend.active_order')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile"
                       role="tab" aria-controls="pills-profile" aria-selected="false">{{__('frontend.completed_order')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-cancel-tab" data-toggle="pill" href="#pills-cancel" role="tab"
                       aria-controls="pills-cancel" aria-selected="false">{{__('frontend.canc_order')}}</a>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                     aria-labelledby="pills-home-tab">
                    @if(count($upcoming_orders) >0 )
                    @php
                    $total_purchase = $upcoming_orders->sum('grant_total');
                    @endphp
                    <h4 class="pt-3 pb-3">{{__('frontend.total_pur')}} {{ number_format($total_purchase, 2, ".", "")}} {{$currency[$lang]}}</h4>
                    @foreach ($upcoming_orders as $orders)
                    @php

                    if(in_array($orders->order_status,[1,2]))
                    {
                    $status= $ord_web_status[1];
                    $cls='text-danger';
                    }else if($orders->order_status == 7)
                    {
                    $status= $ord_web_status[3];
                    $cls="text-success";
                    }else if(in_array($orders->order_status,[3,4,5])){
                        $status= $ord_web_status[2];
                        $cls="text-success";
                    }
                    @endphp
                    <div class="col-md-12 cardf" id="">
                        <a href="javascript:void(0);" onclick="getOrderDetails({{ $orders->id }})">
                            <div class="well well-sm">
                                <div class="row">
                                    <div class="col-xs-4 col-md-4 col">
                                        <b> {{$orders->order_id}}</b>
                                    </div>
                                    <div class="col-xs-4 col-md-4 col ml10">
                                    </div>
                                    <div class="col-xs-4 col-md-4 col">
                                        <p class="{{$cls}} fright mb-3"><i class="fa fa-circle"
                                                                           aria-hidden="true"></i> {{$status}}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4 col-md-4 col">
                                        <p>{{__('frontend.dlv_dt')}}</p>
                                    </div>
                                    <div class="col-xs-4 col-md-4 col ml10">
                                        <p>{{__('frontend.ord_amt')}}</p>
                                    </div>
                                    <div class="col-xs-4 col-md-4 col">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4 col-md-4 col">
                                        @if(strlen($orders->delivery_schedule_date))
                                        <strong>{{ date('j F Y', strtotime($orders->delivery_schedule_date)) }}</strong>
                                        @endif
                                    </div>
                                    <div class="col-xs-4 col-md-4 col ml10">
                                        <b>{{$currency[$lang]}} {{ number_format($orders->grant_total, 2, ".", "") }}</b>
                                    </div>
                                    <div class="col-xs-4 col-md-4 col">
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                    @else
                    <div class="col-md-12 cardf">
                        <div class="well well-sm">
                            <div class="row">
                                <div class="col-md-12">
                                    <p style="text-align: center"><b>{{__('frontend.no_ord')}}</b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                <div class="tab-pane fade" id="pills-profile" role="tabpanel"
                     aria-labelledby="pills-profile-tab">
                    @if(count($completed_orders) > 0)
                    <h4 class="pt-3 pb-3">{{__('frontend.total_pur')}}  {{$completed_orders->sum('grant_total')}} {{$currency[$lang]}}</h4>
                    @foreach ($completed_orders as $completed)
                    <div class="col-md-12 cardf card3">
                        <a href="javascript:void(0);" onclick="getOrderDetails({{ $completed->id }})">
                            <div class="well well-sm">
                                <div class="row">
                                    <div class="col-xs-4 col-md-4 col">
                                        <b>{{$completed->order_id}} </b>
                                    </div>
                                    <div class="col-xs-4 col-md-4 col ml10">
                                    </div>
                                    <div class="col-xs-4 col-md-4 col">
                                        <p class="text-success fright mb-3"><i class="fa fa-circle"
                                                                               aria-hidden="true"></i> Delivered</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4 col-md-4 col">
                                        <p>{{__('frontend.dlv_dt')}}</p>
                                    </div>
                                    <div class="col-xs-4 col-md-4 col ml10">
                                        <p>{{__('frontend.ord_amt')}}</p>
                                    </div>
                                    <div class="col-xs-4 col-md-4 col">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4 col-md-4 col">
                                        @if(strlen($completed->delivery_schedule_date))
                                        <strong>{{ date('j F Y', strtotime($completed->delivery_schedule_date)) }}</strong>
                                        @endif
                                    </div>
                                    <div class="col-xs-4 col-md-4 col ml10">
                                        <b>{{$currency[$lang]}} {{ number_format($completed->grant_total, 2, ".", "") }}</b>
                                    </div>
                                    <div class="col-xs-4 col-md-4 col">
                                        <button type="button" class="btn btn-success text-white fright" onclick="reOrder(this);" ord_id="{{ $completed->id }}">{{__('frontend.reorder')}}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                    @else
                    <div class="col-md-12 cardf">
                        <div class="well well-sm">
                            <div class="row">
                                <div class="col-md-12">
                                    <p style="text-align: center"><b>{{__('frontend.no_ord')}}</b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                <div class="tab-pane fade" id="pills-cancel" role="tabpanel" role="tabpanel"
                     aria-labelledby="pills-cancel-tab">
                    @if(count($cacelled_orders) > 0)
                    <h4 class="pt-3 pb-3">{{__('frontend.total_pur')}}  {{$cacelled_orders->sum('grant_total')}} {{$currency[$lang]}}</h4>
                    @foreach ($cacelled_orders as $cancelled)
                    <div class="col-md-12 cardf card3">
                        <a href="javascript:void(0);" onclick="getOrderDetails({{ $cancelled->id }})">

                            <div class="well well-sm">
                                <div class="row">
                                    <div class="col-xs-4 col-md-4 col">
                                        <b>{{$cancelled->order_id}} </b>
                                    </div>
                                    <div class="col-xs-4 col-md-4 col ml10">
                                    </div>
                                    <div class="col-xs-4 col-md-4 col">
                                        <p class="text-success fright mb-3"><i class="fa fa-circle"
                                                                               aria-hidden="true"></i> Cancelled</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4 col-md-4 col">
                                        <p>{{__('frontend.dlv_dt')}}</p>
                                    </div>
                                    <div class="col-xs-4 col-md-4 col ml10">
                                        <p>{{__('frontend.ord_amt')}}</p>
                                    </div>
                                    <div class="col-xs-4 col-md-4 col">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4 col-md-4 col">
                                        @if(strlen($cancelled->delivery_schedule_date))
                                        <strong>{{ date('j F Y', strtotime($cancelled->delivery_schedule_date)) }}</strong>
                                        @endif
                                    </div>
                                    <div class="col-xs-4 col-md-4 col ml10">
                                        <b>{{$currency[$lang]}} {{ number_format($cancelled->grant_total, 2, ".", "") }}</b>
                                    </div>
                                    <div class="col-xs-4 col-md-4 col">
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                    @else
                    <div class="col-md-12 cardf">
                        <div class="well well-sm">
                            <div class="row">
                                <div class="col-md-12">
                                    <p style="text-align: center"><b>{{__('frontend.no_ord')}}</b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>