<div class="container">
    <div class="modal" id="myModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content  pb-4 pd20">
                <div class=" col-md-12  col-sm-12">
                    <a class="close pt-2" data-dismiss="modal"><img src="{{ asset('assets/frontend/img/cancel.png') }}" class="imgclose"></a>
                    </button>
                    <div class="form-container">
                        <div class="divhide">
                        <h3 class="title pt-2">{{__('frontend.user_login')}}</h3>
                        <ul class="nav nav-tabs navinline  pb-1 row" >
                            <li class="col active">
                                <a href="#menu1" id="a-password">
                                    <label class="container1">{{__('frontend.using_password')}}
                                        <input id="password-radio" type="radio" name="radio" checked="checked"> <span class="checkmark"></span>
                                    </label>
                                </a>
                                <!-- <p class="divhide">If you click on the "Hide" button, I will disappear.</p> -->
                            </li>
                            <li class="col">
                                <a id="a-home" href="#home">
                                    <label class="container1">{{__('frontend.using_verfctn_code')}}
                                        <input id="home-radio" type="radio" name="radio"> <span class="checkmark"></span>
                                    </label>
                                </a>
                            </li>
                            <li class="active " style="display: none;"><a id="a-Forgot" href="#Forgot"></a></li>
                            <li class="active " style="display: none;"><a id="a-Createaccount" href="#Createaccount"></a></li>
                            <li class="active " style="display: none;"><a id="a-Reset" href="#Reset"></a></li>
                            <li class="active " style="display: none;"><a id="a-verify" href="#verify"></a></li>
                            <li class="active " style="display: none;"><a id="a-Signup" href="#verifySignup"></a></li>
                        </ul>
                    </div>
                        <div class="tab-content">
                            <div id="home" class="tab-pane">
                                <form class="form-horizontal" id="frm_login">
                                    <div class="col-lg-12">
                                        <div class="form-group loglabel">
                                            <label>{{__('frontend.phone_number')}}</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="cntry-code">
                                        <div class="input-group ">
                                            <div class="input-group-prepend"> <span class="input-group-text"> <i class="fa fa-flag" aria-hidden="true"></i> </span>
                                            </div>
                                            <input type="text" name="country_code" disabled="disabled" value="+966" class="form-control bleft" placeholder="+966">
                                        </div>
                                    </div>
                                    <div class="cntry-phnm">
                                        <div class="form-group form-group1">
                                            <input type="text" name="phone" class="form-control" placeholder="{{__('frontend.phone_number1')}}">
                                        </div>
                                    </div>
                                    </div>
                                    <button class="btn signin">{{__('frontend.login')}}</button>
                                    <div class="social-links mt-3 mb-2"> <span>{{__('frontend.or_login')}}</span>
                                        <a href="{{route('google.redirect')}}"><i class="fa fa-google-plus" aria-hidden="true"></i>{{__('frontend.google')}}</a>
                                    </div>
                                    <!--div class="social-links social-links1 ormargin"> <span>OR </span>
                                </div-->
                                    <div class="row  nav nav-tabs textcc">
                                        <div class="alred ">{{__('frontend.do_hav_accnt')}} <a href="javascript:void(0)" onclick="$('#a-Createaccount').trigger('click');">{{__('frontend.sign_up')}}</a></div>
                                    </div>
                                </form>
                            </div>
                            <div id="menu1" class="tab-pane active">
                                <form class="formpass sign-in" id="frm_email_login">
                                    <div class="row mt-3">
                                        <div class="col-lg-12">
                                            <div class="form-group form-group1">
                                                <label>{{__('frontend.email')}}</label>
                                                <input id="user_name" type="email" class="form-control" placeholder="{{__('frontend.entr_email')}}" name="user_name">
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group form-group1 main">
                                                <label>{{__('frontend.password')}}</label>
                                                <input type="password" id="password" class="form-control" placeholder="{{__('frontend.password1')}}" name="password"/>
                                                <span id="show" class="fa fa-eye-slash"> </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-2 mb-3 nav nav-tabs">
                                        <div class="col-lg-6 col">
                                            <button type="button" class="btn btn-link  text-success"><a href="javascript:void(0);" id="hide" onclick="$('#a-Forgot').trigger('click');">{{__('frontend.forgot_password')}}?</a></button>
                                        </div>
                                        <div class="col-lg-6 col">
                                            <button type="button" class="btn btn-link fright text-success" onclick="$('#a-Createaccount').trigger('click');">{{__('frontend.create_new_account')}}</button>
                                        </div>
                                    </div>
                                    <button class="btn signin" type="submit">{{__('frontend.login')}}</button>
                                    <div class="social-links mt-3 mb-2"> <span>{{__('frontend.or_login')}}</span>
                                        <a href="{{route('google.redirect')}}"><i class="fa fa-google-plus" aria-hidden="true"></i> {{__('frontend.google')}}</a>
                                    </div>
                                </form>
                            </div>
                            <div id="Forgot" class="tab-pane ">
                                <h3 class="title pt-2">{{__('frontend.forgot_password')}}</h3>
                                <p>{{__('frontend.enter_email_to_reset_password')}}</p>
                                <form class="formpass" id="frm_forgot_password">
                                    <div class="row mt-3">
                                        <div class="col-lg-12">
                                            <div class="form-group form-group1">
                                                <label>{{__('frontend.email')}}</label>
                                                <input type="Email" class="form-control" placeholder="{{__('frontend.entr_email')}}" name="email">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <button type="button" class="btn btn-success  text-white h60 w100 back-home">
                                                <a onclick="$('#a-password').trigger('click');">{{__('frontend.back_to_login')}}</a></button>
                                        </div>
                                        <div class="col-lg-6 nav nav-tabs">
                                            <button type="submit" class="btn btn-ligtsucc fright text-white h60 w100">{{__('frontend.submit')}}
                                            </button>
                                        </div>
                                    </div>
                                    <div class="row mt-2">

                                    </div>
                                </form>
                            </div>
                            <div id="Reset" class="tab-pane">
                                <h3 class="title pt-2">{{__('frontend.reset_password')}}</h3>
                                <p>{{__('frontend.new_password')}}</p>
                            <form class="formpass" id="frm_reset_password">
                                    <div class="row mt-3">
                                        <div class="col-lg-12">
                                            <div class="form-group form-group1 main">
                                                <label>{{__('frontend.enter_new_password')}}
                                                </label>
                                                <input type="password" id="password3" class="form-control" name="password"/>
                                                <span id="show" class="show3 fa fa-eye-slash"> </span>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group form-group1 main">
                                                <label>{{__('frontend.confirm_new_password')}}</label>
                                                <input type="password" id="password4" class="form-control" name="c_password"/>
                                                <span id="show" class="show4 fa fa-eye-slash"> </span>
                                            </div>
                                        </div>
                                    </div>
                                <button type="submit" class="btn signin">{{__('frontend.reset_password1')}}</button>
                                </form>
                            </div>
                            <div id="Createaccount" class="tab-pane ">
                                <h3 class="title pt-2">{{__('frontend.create_account')}}</h3>
                                <form class="formpass" id="frm_create_account">
                                    <div class="row mt-3">
                                        <div class="col-lg-12">
                                            <div class="form-group form-group1">
                                                <label>{{__('frontend.busines_type')}}</label>
                                                <span class="select-box helpwesel">
                                                    @php  
                                                    $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
                                                    $business_type = \App\Models\BusinessType::with(['lang' => function ($query) use ($lang) {
                                                    $query->where('language', $lang);
                                                    }])->get(); @endphp
                                                    <select class="form-control" name="type">
                                                        @foreach($business_type as $bus_val)
                                                        <option value="{{$bus_val->id}}">{{$bus_val->lang[0]->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group form-group1">
                                                <label>{{__('frontend.name')}}</label>
                                                <input type="text" name="name" class="form-control" placeholder="{{__('frontend.enter_your_name')}}">
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group form-group1">
                                                <label>{{__('frontend.busnes_name')}}</label>
                                                <input type="text" name="business_name" class="form-control" placeholder="{{__('frontend.entr_busnes_name')}}">
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group form-group1">
                                                <label>{{__('frontend.email')}}</label>
                                                <input name="email" type="Email" class="form-control" placeholder="{{__('frontend.entr_email')}}">
                                            </div>
                                        </div>
                                    <div class="col-lg-12">
                                        <div class="form-group loglabel">
                                            <label>{{__('frontend.phone_number')}}</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="cntry-code">
                                        <div class="input-group ">
                                            <div class="input-group-prepend"> <span class="input-group-text"> <i class="fa fa-flag" aria-hidden="true"></i> </span>
                                            </div>
                                            <input type="text" name="country_code" disabled="disabled" value="+966" class="form-control bleft" placeholder="+966">
                                        </div>
                                    </div>
                                    <div class="cntry-phnm">
                                        <div class="form-group form-group1">
                                            <input type="text" name="phone" class="form-control" placeholder="{{__('frontend.phone_number1')}}">
                                        </div>
                                    </div>
                                    </div>
                                        <div class="col-lg-12">
                                            <div class="form-group form-group1 main">
                                                <label>{{__('frontend.password')}}</label>
                                                <input type="password" id="password1" placeholder="{{__('frontend.password')}}" class="form-control" name="password" />
                                                <span id="show" class="show1 fa fa-eye-slash"> </span>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn signin">{{__('frontend.agree_join')}}</button>
                                    <div class="social-links mt-3 mb-2"> <span>{{__('frontend.or_login')}}</span>
                                        <a href="{{route('google.redirect')}}"><i class="fa fa-google-plus" aria-hidden="true"></i> {{__('frontend.google')}}</a>
                                    </div>
                                    <div class="row nav nav-tabs textcc">
                                        <div class="alred ">{{__('frontend.already_in')}}? <a onclick="$('#a-password').trigger('click');">{{__('frontend.signin')}}</a></div>
                                    </div>
                                </form>
                            </div>

                            <div id="verify" class="tab-pane verifycl">
                                <h3 class="title pt-2">{{__('frontend.verf_account')}}</h3>
                                <p id="email_send">{{__('frontend.email_send')}}
                                </p>
                                <form class="formpass ">
                                    <div class="row mt-3">
                                        <div class="col-lg-3 col-sm-3 col-3">
                                            <input style="text-align: center;" id="otp-1" type="text" class="form-control" placeholder="">
                                        </div>
                                        <div class="col-lg-3 col-sm-3 col-3">
                                            <input style="text-align: center;" id="otp-2" type="text" class="form-control" placeholder="">
                                        </div>
                                        <div class="col-lg-3 col-sm-3 col-3">
                                            <input style="text-align: center;" id="otp-3" type="text" class="form-control" placeholder="">
                                        </div>
                                        <div class="col-lg-3 col-sm-3 col-3">
                                            <input style="text-align: center;" id="otp-4" type="text" class="form-control" placeholder="">
                                        </div>
                                        <div class="col-lg-12 nav nav-tabs verbut">
                                            <a href="#">
                                                <input type="hidden" id="cust_id" value="">
                                                <button type="submit" id="verify-acc" class="btn btn-ligtsucc fright text-white h60 w100">{{__('frontend.verify')}}</button>
                                            </a>
                                        </div>
                                        <div class="col-lg-12 receive">
                                            <p>{{__('frontend.dont_receive_code')}}<span id="Resendshow">{{__('frontend.resend_otp')}}</span></p>
                                        </div>
                                        <div class="col-lg-12 resendshow" style="display:none;">
                                            <form class="form-horizontal">
                                                <div class="row">
                                                    <div class="col-lg-4   w30">
                                                        <div class="input-group ">
                                                            <div class="input-group-prepend"> <span class="input-group-text"> <i class="fa fa-flag" aria-hidden="true"></i> </span>
                                                            </div>
                                                            <input type="text" class="form-control bleft" placeholder="Code">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-5   w600">
                                                        <div class="form-group form-group1">
                                                            <input type="text" class="form-control" placeholder="Phone Number">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3   w160">
                                                        <button class="btn signin">{{__('frontend.resend')}}</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="location-modal">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">{{__('frontend.choose_location')}}</h4>
                @php
                $region_id = \Session::get('region_id');
                @endphp
                @if($region_id)
                <button type="button" class="close" data-dismiss="modal"><img src="{{ asset('assets/frontend/img/cancel.png') }}" class="alcancel"></button>
                @endif
            </div>

            <!-- Modal body -->
            <div class="">
                <div id="map_component" style="width: 100%; height: 450px;"></div>
                <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3624.937001957752!2d46.69382571447422!3d24.69469225796431!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e2f082322acbe6f%3A0xc55d58ec5b24f815!2sEmtyaz%20Catering%20Company!5e0!3m2!1sen!2sin!4v1603134117112!5m2!1sen!2sin" width="100%" height="250" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe> -->
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <h5 class=" mr-auto" style="width: 370px;"><img src="{{ asset('assets/frontend/img/location.png') }}" class="alname"><span class="location-to-deliver location-to-deliver-temp"></span></h5>
                <button id="location_submit" type="button" class="btn btn-success">{{__('frontend.choose')}} &nbsp; <img src="{{ asset('assets/frontend/img/send.png') }}" class="alsend"></button>
            </div>

        </div>
    </div>
</div>


<div class="modal" id="branch" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content  pb-4 pd20">
            <div class=" col-md-12  col-sm-12">
                <a href="" data-dismiss="modal" class="close pt-2"><img src="{{ asset('assets/frontend/img/cancel.png') }}" class="imgclose"></a>
                </button>
                <div class="form-container">
                    <h3 class="title pt-2">{{__('frontend.add_new_branch')}}
                    </h3>
                    <div class="tab-content">
                        <form class="form-horizontal" id="branch-form">
                            <div class="row mt-3 mb-3">
                                <div class="col-lg-12">
                                    <div class="form-group form-group1">
                                        <div class="form-group loglabel">
                                            <label>{{__('frontend.branch_name')}}</label>
                                        </div>
                                        <input type="text" name="branch_name" class="form-control" placeholder="{{__('frontend.enter_branch_name')}}">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group form-group1">
                                        <div class="form-group loglabel">
                                            <label>{{__('frontend.contact_name')}}</label>
                                        </div>
                                        <input type="text" name="contact_name" class="form-control" placeholder="{{__('frontend.enter_contact_name')}}">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group form-group1">
                                        <div class="form-group loglabel">
                                            <label>{{__('frontend.email')}}</label>
                                        </div>
                                        <input type="email" name="email" class="form-control" placeholder="{{__('frontend.entr_email')}}">
                                    </div>
                                </div>
                                    <div class="col-lg-12">
                                        <div class="form-group loglabel">
                                            <label>{{__('frontend.phone_number')}}</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="cntry-code">
                                        <div class="input-group ">
                                            <div class="input-group-prepend"> <span class="input-group-text"> <i class="fa fa-flag" aria-hidden="true"></i> </span>
                                            </div>
                                            <input type="text" name="country_code" disabled="disabled" value="+966" class="form-control bleft" placeholder="+966">
                                        </div>
                                    </div>
                                    <div class="cntry-phnm">
                                        <div class="form-group form-group1">
                                            <input type="text" name="phone" class="form-control" placeholder="{{__('frontend.phone_number1')}}">
                                        </div>
                                    </div>
                                    </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 ">
                                </div>
                                <div class="col-lg-6 ">
                                    <button type="button" onclick="$('#branch').modal('hide');" class="btn btn-dark h60 ">{{__('frontend.cancel')}}</button>
                                    <button type="submit" class="btn btn-success h60 fright">{{__('frontend.save')}}</button>
                                </div>
                            </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@include('frontend.supportchat.support')