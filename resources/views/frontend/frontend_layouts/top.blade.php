<section class="headsection">
    <div class="container">
        <div class="row">
            <div class="col-lg-3  col-xs-3 col-xl-3 col">
                <ul class="sociablestop mb-lg-0">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                </ul>
            </div>
            <div class="col-lg-9  col-xs-9 col-xl-9 col mywh  dcontent">
                <a href="{{route('customer.wishlist')}}">{{ __('frontend.my_wishlist') }}</a>
                <div class="lang custom-select">
                    @php
                    $lang1= app()->getLocale() == 'en' ? 'ar' : 'en';
                    $display = $lang1 == 'en' ? 'English' : 'العربية';
                    @endphp
                    <a href="{{ url('setlocale', $lang1) }}"><span>{{ $display }}</span></a>
                </div>
            </div>
        </div>
    </div>
</section>