<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<!-- head -->
@include('frontend.frontend_layouts.frontend_head')

<body class="home">

    <div class="text-center ajax-loading-buz" style="display: none;">
        <div class="loader-bg" style="width: 100%;height: 100%;background-color:darkgrey;opacity:.5;position: fixed;z-index: 1052;"></div>
        <div style="position: fixed; left: 47%; top: 50%; z-index: 1055;">
            <img src="{{ asset('assets/frontend/custom/img/loader.gif') }}"/>
        </div>
    </div>

    <!-- top -->
    @include('frontend.frontend_layouts.frontend_modals')

    <div id="container">
        <!-- top -->
        @include('frontend.frontend_layouts.top')

        <!-- search section -->
        @include('frontend.frontend_layouts.search_bar_section')

        <!-- nav -->
        @include('frontend.frontend_layouts.nav')
        
        <!-- contend -->
        @yield('content')

        <!-- footer -->
        @include('frontend.frontend_layouts.frontend_footer')

        @stack('scripts')
    </div>
</body>

</html>