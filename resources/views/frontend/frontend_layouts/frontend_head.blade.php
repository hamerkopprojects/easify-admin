<head>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width" />
      <meta name="description" content="" />

      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">

      <title>{{ __('frontend.easfy') }}</title>

      <link rel="icon" href="{{ asset('assets/frontend/custom/img/logo_title.png') }}" type="image/icon type">

      @php
         $rtl_ext = app()->getLocale() == 'en' ? '' : '-rtl';
      @endphp

      <link rel="stylesheet" href="{{ asset('assets/frontend/css/bootstrap' . $rtl_ext . '.min.css') }}" />
      <link rel="stylesheet" href="{{ asset('assets/frontend/css/font-awesome.min.css') }}" />
      <link rel="stylesheet" href="{{ asset('assets/frontend/css/font-awesome4.min.css') }}" />
      <link rel="stylesheet" href="{{ asset('assets/frontend/css/vladimir.css') }}" />
      <link rel="stylesheet" href="{{ asset('assets/frontend/css/main' . $rtl_ext . '.css') }}" />
      <link rel="stylesheet" href="{{ asset('assets/frontend/css/slick-theme.css') }}" />
      <link rel="stylesheet" href="{{ asset('assets/frontend/css/style' . $rtl_ext . '.css') }}" />
      <link rel="stylesheet" href="{{ asset('assets/frontend/css/owl.carousel.min.css') }}">
      <link rel="stylesheet" href="{{ asset('assets/frontend/css/owl.theme.default.min.css') }}">
      <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
      <link rel="stylesheet" href="{{ asset('assets/frontend/css/animate.min.css') }}">

      <link href="{{asset('assets/frontend/custom/css/custom.css')}}" rel="stylesheet">
      <link rel="stylesheet" href="{{ asset('assets/frontend/custom/css/jquery-confirm.min.css') }}" />

      <link rel="stylesheet" href="{{ asset('assets/frontend/css/xzoom.css') }}" />
     <link
         href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,400;0,500;0,600;0,700;0,900;1,400;1,500;1,600;1,700&display=swap"
         rel="stylesheet">
     
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.css">
      <!-- Javascript -->

      <!-- Javascript -->
      <script src="{{ asset('assets/frontend/js/vendor/head.core.js') }}"></script>
      @stack('css')
   </head>