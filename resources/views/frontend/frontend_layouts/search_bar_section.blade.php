<header id="header">
    <div class="container">
        <div class="row justify-content-between align-items-center">
            <div class="col-xl-2 col-lg-2 ">
                <div class="logo">
                    <a href="{{ route('frontend') }}">
                        <img src="{{ asset('assets/frontend/img/logo.png') }}" alt="logo">
                    </a>
                </div>
                <a class="open-menu-btn d-lg-none"><i class="fa fa-bars" aria-hidden="true"></i></a>
            </div>
            <div class="col-xl-4 col-lg-4">
                <div class="search-form">
                    <form action="#">
                        <div class="form-group mb-0">
                            <input type="search" id="autosuggest" name="autosuggest" autocomplete="off" onsearch="OnClear()" placeholder="{{__('frontend.search_for_pdts')}}" class="form-control">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </div>
                        
                    </form>
                </div>
                <ul class="list-group" id="result" style="position: absolute ! important;z-index: 1001 ! important;"></ul>
            </div>
            <div class="col-xl-5 col-lg-5  ">
                <div class="user-block mb-lg-0">

                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            @php
                            $customer_id = \Session::get('customer_id');
                            $customer_name = \Session::get('customer_name');
                            $profile = \App\Models\Customer::where('id', $customer_id)->select('profile_image', 'business_name', 'is_branch')->first();
                            @endphp
                            @if($customer_id != '')
                            <a class="login" href="#" data-target="dropdown" class="dropdown-toggle" data-toggle="dropdown">
                                @if(isset($profile->profile_image) && is_file( public_path() . '/uploads/' . $profile->profile_image))
                                <img src="{{ url('uploads/'.$profile->profile_image) }}" class="avt">
                                @else
                                <img src="{{ asset('assets/frontend/img/man.png') }}" class="avt">
                                @endif
                                <span class="cont cust-name">{{__('frontend.hello')}}, {{ $customer_name }}</span><br>{{__('frontend.my_account1')}}
                            </a>
                            @else
                            <a class="login" href="#" data-toggle="modal" data-target="#myModal" class="dropdown-toggle">
                                <img src="{{ asset('assets/frontend/img/man.png') }}" class="avt">
                                <span class="cont cust-name">{{__('frontend.hello')}}, {{__('frontend.signin')}}</span><br>{{__('frontend.my_account1')}}
                            </a>
                            @endif
                            <ul class="dropdown-menu">
                                <span class="conDiv">
                                    <span class="icon">
                                        @if(isset($profile->profile_image) && is_file( public_path() . '/uploads/' . $profile->profile_image))
                                        <img src="{{ url('uploads/'.$profile->profile_image) }}" class="avtsub">
                                        @else
                                        <img src="{{ asset('assets/frontend/img/man.png') }}" class="avtsub">
                                        @endif
                                    </span>
                                    <span class="conDiv-s">{{ $customer_name }}</span>
                                    <p>{{ $profile->business_name ?? ''}}</p>
                                </span>
                                <li><a href="{{route('order.list')}}">{{__('frontend.my_orders')}} </a></li>
                                <li><a href="{{ route('customer.adddresses') }}">{{__('frontend.my_address')}} </a></li>
                                <li><a href="{{ route('customer.wishlist') }}">{{__('frontend.my_wishlist')}}</a></li>
                                @if(isset($profile->is_branch) && $profile->is_branch == 'N')
                                <li><a href="{{ route('customer.branch') }}">{{__('frontend.cust_bran')}} </a></li>
                                @endif
                                <li><a href="{{ route('profile.edit') }}">{{__('frontend.edt_pro')}} </a></li>
                                <li><a href="{{ route('supprot.frndend.list') }}">{{__('frontend.cont_sup')}}</a></li>
                                <li><a href="{{ route('customer.logout') }}">{{__('frontend.logout')}} </a></li>
                            </ul>
                        </li>
                    </ul>

                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            @php
                            $cartKey = \Session::get('cartKey');
                            $cartKey = isset($cartKey->cartKey) ? $cartKey->cartKey : '';
                            $cart = \App\Models\CartKey::where('key', $cartKey)->with('parent')->first();
                            $count = $cart->parent->item_count ?? 0;
                            $total = $cart->parent->grant_total ?? '0.00';
                            @endphp
                            <a href="{{ route('cart.show') }}">
                                <img src="{{ asset('assets/frontend/img/shopping.png') }}" class="avt">
                                <span class="addnumber cart-count">{{ $count }}</span><span class="cont">{{__('frontend.my_cart')}}</span><span class="total-amount" style="font-size: 14px;position: absolute;left: 50px;width: 100px;"><br>{{$currency[$lang]}} {{ $total }}</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
@push('css')
<style type="text/css">
    #result {
        position: absolute;
        width: 100%;
        max-width:870px;
        cursor: pointer;
        overflow-y: auto;
        max-height: 400px;
        box-sizing: border-box;
        z-index: 1001;
  }
  .link-class:hover{
   background-color:#f1f1f1;
  }
</style>
@endpush
