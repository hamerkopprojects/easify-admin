<nav id="main-navigation">
    <a class="close-menu-btn d-lg-none"><i class="fa fa-times" aria-hidden="true"></i></a>
    <div class="container">
        <div class="row ">
            <div class="col-lg-9">
                <ul class="menu">
                    <li>
                        <a href="{{ route('frontend') }}">{{__('frontend.home')}}</a>
                    </li>
                    @php $lang = app()->getLocale() == 'en' ? 'en' : 'ar';@endphp
                    @if (count($menu) > 0)
                    @php $i = 1; @endphp
                    @foreach ($menu as $menu_val)
                    @php $i++;
                    $cl_act = "";
                    if (isset($menu_id) && ($menu_id == $menu_val->id)) :
                    $cl_act = "current";
                    endif;
                    @endphp
                    <li class="parent">
                        <a href="{{route('category.show', $menu_val->slug)}}">{{$menu_val->lang[0]->name}} </a>
                        @php
                        $sub_menu = \App\Models\Category::with(['lang' => function ($query) use($lang) {
                        $query->where('language', $lang);
                        }])->where(['parent_id' => $menu_val->id, 'status' => 'active'])->get();
                        @endphp
                        @if(count($sub_menu) > 0)
                        <div class="content">
                            <div class="container">
                                <div class="inner">
                                    <div class="row three-col justify-content-between">
                                        @foreach ($sub_menu as $sub_menu_val)
                                        <div class="col-lg-4 col-md-6 column">
                                            <a href="{{route('category.show', $sub_menu_val->slug)}}"><h2>{{$sub_menu_val->lang[0]->name}}</h2></a>
                                            @php
                                            $sub_sub_menu = \App\Models\Category::with(['lang' => function ($query) use($lang) {
                                            $query->where('language', $lang);
                                            }])->where(['parent_id' => $sub_menu_val->id, 'status' => 'active'])->get();
                                            @endphp
                                            @if(count($sub_sub_menu) > 0)
                                            <ul class="list">
                                                @foreach ($sub_sub_menu as $sub_sub_menu_val)
                                                <li><a href="{{route('category.show', $sub_sub_menu_val->slug)}}">{{$sub_sub_menu_val->lang[0]->name}}</a></li>
                                                @endforeach
                                            </ul>
                                            @endif
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    </li>
                    @endforeach
                    @endif
                </ul>
            </div>
            <div class="col-lg-3 p-0 locres">
                <div class="allright">

                    @php
                        $poly = \App\Models\OtherSettings::select('operational_area')->first();
                        $region_id = \Session::get('region_id');
                        if($poly) {
                        $poly = $poly->operational_area;
                        }
                    @endphp

                    <a href="#" region_id="{{ $region_id }}" data-toggle="modal" data-target="#location-modal" area-of-op="{{ $poly }}" id="area_of_op">
                        <span class="iconimg"><img src="{{ asset('assets/frontend/img/location.png') }}" class="alname"></span>
                        <span><span class="textc">{{__('frontend.deliver_to')}} </span>: <b class="location-to-deliver"></b></span>
                        <p class="textunder">{{__('frontend.change_loc')}}</p>
                    </a>
                </div>

            </div>
        </div>
    </div>
</nav>