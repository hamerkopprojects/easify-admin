@php
$customer_id = \Session::get('customer_id');
$customer_name = \Session::get('customer_name');
@endphp

{{-- Model Pop up --}}
<div class="container">
<div class="row">

    <div class="col-lg-12 p-0 mt-4">
        <div class="modal fade" id="request-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-l" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{__('frontend.sup_chat')}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="chat-history" data-no-padding="no-padding">
                        <div class="container">
                            <div class="messaging">
                                <div class="inbox_msg">
                                    <div class="mesgs">
                                        <div class="msg_history">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="type_msg">
                            <div class="input_msg_write">
                                <textarea type="text" id="chat-msg" class="write_msg" placeholder="{{__('frontend.typ_comm')}}" rows="1" cols="100" style="padding-top: 10px" ></textarea>
                                <button id="send-msg" class="msg_send_btn" type="button"><span class="material-icons">send</span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
{{-- end  --}}



<footer id="footer">
    <div class="dashboard">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-12 column textlef">
                    <h5>{{ __('frontend.about') }}</h5>
                    <ul class="footer-links  mb-0">
                        <li><a href="{{route('pages.show', 'about-us')}}">{{ __('frontend.about_easfy') }}</a></li>
                        <li><a href="{{route('pages.show', 'faqs')}}">{{ __('frontend.faqs') }}</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-4 column">
                    <h5>{{ __('frontend.account') }}</h5>
                    <ul class="footer-links  mb-0">
                        @if($customer_id == '')
                        <li><a href="javascript:void(0);" data-toggle="modal" data-target="#myModal" >{{ __('frontend.login_reg') }}</a></li>
                        <li><a href="{{ route('cart.show') }}">{{ __('frontend.my_shopping_car') }}</a></li>
                        {{-- <li><a href="javascript:void(0);">{{ __('frontend.track_orders') }}</a></li> --}}
                        @else
                        <li><a href="{{ route('profile.edit') }}">{{ __('frontend.my_account') }}</a></li>
                        <li><a href="{{ route('cart.show') }}">{{ __('frontend.my_shopping_car') }}</a></li>
                        <li><a href="{{ route('order.list') }}">{{ __('frontend.my_orders') }}</a></li>
                         <li><a href="{{ route('customer.wishlist') }}">{{ __('frontend.my_wishlist') }}</a>
                        {{-- <li><a href="javascript:void(0);">{{ __('frontend.track_orders') }}</a></li> --}}
                        @endif
                    </ul>
                </div>
                <div class="col-lg-3 col-md-4 column">
                    <h5>{{ __('frontend.help') }}</h5>
                    <ul class="footer-links  mb-0">
                        <li><a href="{{route('pages.show', 'terms-and-conditions')}}">{{ __('frontend.terms_conditions') }}</a></li>
                        <li><a href="{{route('pages.show', 'privacy-policy')}}">{{ __('frontend.privacy_policy') }}</a></li>
                        <li><a href="{{route('pages.show', 'return-policy')}}">{{ __('frontend.return_policy') }}</a></li>
                        <li><a href="{{route('pages.show', 'warranty-policy')}}">{{ __('frontend.warranty_policy') }}</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-4 column">
                    <h5>{{ __('frontend.contact_us') }}</h5>
                    <ul class="contact-options mb-0">
                        @if($customer_id != '')
                        <li><a href="#" data-toggle="modal" data-target="#SUPPORT">{{ __('frontend.contact_sup') }}</a></li>
                        @endif
                        <li>{{ __('frontend.cust_serv') }}: <a href="tel:+966 5000 000">+ 966 5000 000</a></li>
                        <li>{{ __('frontend.email_us') }}:<a href="mailto:support@easify.com">support@easify.com</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-footer">
        <div class="container">
            <div class="row ">
                <div class="col-xl-3 col-lg-2 col-md-3 order-lg-3">
                    <img class="d-inline-block mb-3 mb-lg-0 foimg mt-2"  src="{{ asset('assets/frontend/images/payment-option.png') }}" alt="payment-options">
                </div>
                <div class="col-xl-2 col-lg-2 col-md-3 order-lg-4">
                    <ul class="sociables mb-lg-0 mt-2">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    </ul>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 order-lg-1">
                    <ul class="our-apps mb-lg-0">
                        <li><a href="{{ route('frontend') }}"><img src="{{ asset('assets/frontend/img/fo-logo.png') }}" alt="img" ></a></li>
                    </ul>
                </div>
                <div class="col-xl-4 col-lg-5 col-md-12 order-lg-2">
                    <p class="copyright mb-lg-0 mt-2">{{ __('frontend.copyright') }}
                        <a href="#">{{ __('frontend.policy') }}</a> <span class="separator">|</span> <a href="#">{{ __('frontend.disclaimer') }}</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>

@php
$rtl_ext = app()->getLocale() == 'en' ? '' : '-rtl';
@endphp

<script src="{{ asset('assets/frontend/js/vendor/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('assets/frontend/js/vendor/jquery.matchHeight-min.js') }}"></script>
<script src="{{ asset('assets/frontend/js/vendor/popper.min.js') }}"></script>
<script src="{{ asset('assets/frontend/js/vendor/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/frontend/js/vendor/ofi.browser.js') }}"></script>
<script src="{{ asset('assets/frontend/js/owl.carousel.js') }}"></script>
<script src="{{ asset('assets/frontend/js/vendor/owl-carousel.js')}}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('assets/frontend/js/main' . $rtl_ext . '.js') }}"></script>
<script src="{{ asset('assets/frontend/js/vendor/slick.min.js')}}"></script>
<script src="{{ asset('assets/frontend/js/xzoom.min.js')}}"></script>
<script src="{{ asset('assets/frontend/js/setup.js')}}"></script>

<script src="{{asset('assets/js/jquery.validate.js')}}"></script>
<script src="{{ asset('assets/frontend/custom/js/jquery.toaster.js') }}"></script>

<script type="text/javascript" src='https://maps.google.com/maps/api/js?key=AIzaSyDEvpyS_qklkSCHdUd4413z1QUchN4DBRM&libraries=places'></script>
<script src="{{ asset('assets/frontend/custom/js/locationpicker.min.js') }}"></script>
<script src="{{ asset('assets/frontend/custom/js/jquery-confirm.min.js') }}"></script>
<script>
$("#show").click(function () {
    $(".divhide").show();
});
$(function () {
  $('[data-toggle="tooltip"]').tooltip();   
})
</script>