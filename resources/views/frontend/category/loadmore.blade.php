@if (count($pdt_info) > 0)
@php $wishListAll = Helper::wish_listed_products();@endphp
@foreach ($pdt_info as $pdt_info_val)
@php $inFav = in_array($pdt_info_val->id, $wishListAll) ? 'active' : ''; @endphp
<li>
    <div class="product">
        <header class="d-flex flex-wrap justify-content-between align-items-center">
            <span class="total-discount">
            </span>
            <a onclick="event.preventDefault(); addToFavourites(this, {{ $pdt_info_val->id }});" class="favourite {{$inFav}}" href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
        </header>
        <div class="product-image">
            <a href="{{route('products.detail', $pdt_info_val->slug)}}">
                @if(is_file( public_path() . '/uploads/' . $pdt_info_val->cover_image))
                <img src="{{ url('uploads/'.$pdt_info_val->cover_image) }}" alt="img">
                @else
                <img src="{{ asset('assets/frontend/custom/images/no-image.png') }}" alt="img">
                @endif
            </a>
        </div>
        <span class="price-block">
            @php
            $discount_price = $pdt_info_val->variant_price[0]->discount_price ?? NULL;
            $act_price = $pdt_info_val->variant_price[0]->price ?? NULL;
            $commission = isset($pdt_info_val->commission) ? $pdt_info_val->commission->value : NULL;
            $mark_up = isset($pdt_info_val->variant_price) ? $pdt_info_val->variant_price[0]->easify_markup : NULL;
            $dis_commission = ($discount_price * $commission / 100);
            $price_commission = ($act_price * $commission / 100);
            $dis_markup = ($discount_price * $mark_up / 100);
            $price_markup = ($act_price * $mark_up / 100);
            $d_price = $discount_price + $dis_commission + $dis_markup;
            $price = $act_price + $price_commission + $price_markup;
            @endphp
            @if(!empty($d_price))
            <span id="actual-price" class="old-price">{{$currency[$lang]}} {{ number_format($price, 2, ".", "") }}</span>
            <span id="discount-price" class="offer-price">{{$currency[$lang]}} {{ number_format($d_price, 2, ".", "") }} </span>
            @else
            <span id="actual-price" class="old-price"></span>
            <span id="discount-price" class="offer-price">{{$currency[$lang]}} {{ number_format($price, 2, ".", "") }} </span>
            @endif
        </span>
        <div class="product-desc">
            <p>
                <a href="{{route('products.detail', $pdt_info_val->slug)}}"><span>{{ $pdt_info_val->lang[0]->name }}</span></a>
            </p>
        </div>
        <ul class="rating">
            @php $rating = $pdt_info_val->reviews->sum('rating') ? $pdt_info_val->reviews->sum('rating') / Count($pdt_info_val->reviews) : 0; @endphp
            @for($i = 1; $i <= 5; $i++ )
            @if(round( $rating - .25 ) >= $i )
            <li class="fa fa-star"></li>
            @elseif(round( $rating + .25 ) >= $i )
            <li class="fa fa-star-half-o"></li>
            @else
            <li class="fa fa-star-o"></li>
            @endif
            @endfor
        </ul>
        <footer class="d-flex flex-wrap justify-content-between align-items-center ">
            <div class="left-column">
                @if($pdt_info_val->product_type == 'complex' && $pdt_info_val->variant_price)
                <span class="select-box ">
                    <select class="form-control" id="variant-select" onchange="changeDisplyaPrice(this);">
                        @php $i = 0;@endphp
                        @foreach($pdt_info_val->variant_price as $variants)
                        @if($lang == 'ar')
                        @php
                        $variant_ar = \App\Models\VariantLang::where('en_matching_id', $variants->variant_lang->id)->pluck('name')->first();
                        $variant_name = $variant_ar;  @endphp
                        @else
                        @php $variant_name = $variants->variant_lang->name ?? ''; @endphp
                        @endif
                        @php
                        $discount_price = $variants->discount_price ?? NULL;
                        $act_price = $variants->price ?? NULL;
                        $commission = isset($pdt_info_val->commission) ? $pdt_info_val->commission->value : NULL;
                        $mark_up = isset($variants->easify_markup) ? $variants->easify_markup : NULL;
                        $dis_commission = ($discount_price * $commission / 100);
                        $price_commission = ($act_price * $commission / 100);
                        $dis_markup = ($discount_price * $mark_up / 100);
                        $price_markup = ($act_price * $mark_up / 100);
                        $d_price = $discount_price + $dis_commission + $dis_markup;
                        $price = $act_price + $price_commission + $price_markup;
                        $d_price = ($d_price > 0) ? $d_price : '';
                        $price = $price ?? '';
                        @endphp
                        <option price="{{ $variants->$price }}" d_price="{{ $d_price }}" min_stock="{{$pdt_info_val->variant[$i]->pdt_branch->min_stock ?? ''}}" max_stock="{{$pdt_info_val->variant[$i]->pdt_branch->max_stock ?? ''}}"
                                value="{{ $variants->variant_lang->id }}">{{ $variant_name ?? ''}}</option>
                        @php $i++;@endphp
                        @endforeach
                    </select>
                </span>
                @endif
                @if(isset($pdt_info_val->variant[0]->pdt_branch))
                <div class="quantity">
                    <input type="button" value="-" class="minus" min_stock="{{$pdt_info_val->variant[0]->pdt_branch->min_stock}}" onclick="updateCount(this)" id="min_amount">
                    <input type="number" class="qty" step="1" min="1" max="" value="{{$pdt_info_val->variant[0]->pdt_branch->min_stock}}" id="pdt_qty">
                    <input type="button" value="+" class="plus" max_stock="{{$pdt_info_val->variant[0]->pdt_branch->max_stock}}" onclick="updateCount(this)" id="max_amount">
                </div>
                @endif
            </div>
            <div class="right-column">
                <a href="#" class="add-btn align-middle" onclick="addToCart(this, {{ $pdt_info_val->id }}, '{{ $pdt_info_val->product_type }}', 'list')">{{__('frontend.add')}}</a>
            </div>
        </footer>
    </div>
</li>
@endforeach
@endif
<script>
    $(document).ready(function () {
    $(window).scroll(function () {
    if ($(window).scrollTop() + $(window).height() > $(document).height() - 200) {
    var maxPage = $('#maxPage').val();
    var page = $('#page').val();
    if (maxPage > page) {
    var page_new = parseInt(page) + 1;
    $('#page').val(page_new);
    $('.ajax-loading').hide();
    var minval = $('#minval').val();
    var maxval = $('#maxval').val();
    var list_type = $('#list_type').val();
    var sort = $('#sort').val();
    var parent_cat_id = $('#parent_cat_id').val();
    var cat_id = $('#cat_id').val();
    var brand_id = $('#brand_id').val();
    var supplier_id = $('#supplier_id').val();
    loadMoreData(page_new, maxPage, parent_cat_id, cat_id, brand_id, supplier_id, list_type, sort, minval, maxval);
    }
    }
    });
    });
</script>