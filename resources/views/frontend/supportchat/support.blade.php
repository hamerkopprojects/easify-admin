<div class="modal " id="SUPPORT">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <form id="frm_support_request_common">
                    <div class="row helpwe">
                        <div class="col-lg-12 "><a href="" class="close" data-dismiss="modal"><img src="{{asset('assets/images/close.png')}}" class="imgclose"></a>
                            <h4>{{__('frontend.contact_supp_caps')}}</h4>

                        </div>

                        <div class="col-lg-12">

                            <div class="form-group form-group1">
                                <input type="text" class="form-control formd" placeholder="{{__('frontend.ent_sub')}}" name="subject">
                            </div>

                            <textarea class="form-control" col="5" placeholder="{{__('frontend.ent_com')}}" name="comment"></textarea>

                        </div>

                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-success w100" id="support_request_common">{{__('frontend.send')}}</button>
                        </div>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>