@extends('frontend.frontend_layouts.frontend_app')
@section('content')
    <div id="content" class="bg-light page-cart pagevart">
        <div class="container">
            <div id="breadcrumbs">
            <p><a href="{{ route('frontend') }}">{{__('frontend.home')}}</a> <i class="fa fa-long-arrow-right" aria-hidden="true"></i> <span>{{__('frontend.sup_chat')}}</span></p>
           </div>
            <div class="row">
                <div class="col-lg-12 mt-4 mb-4 mres15">
                <div class="row">
                <div class="col-xs-6 col-md-6   p-0">
                    <h4 class="mb-2">{{__('frontend.contact_supp_caps')}}</h4>
                </div>
                <div class="col-xs-6 col-md-6">
                    <button type="button" class="btn btn-success btnnormal fright fribut" data-toggle="modal" data-target="#SUPPORT">{{__('frontend.new_cont_sup')}}</button>
                </div>
                </div>
            </div>
                @include('frontend.myaccount.sidebar')
                <div class="col-lg-9">
                    <div class="row">
                        @if(count($support_list) > 0)
                        @foreach ($support_list as $list)
    
                        <div class="col-md-12 cardf ml-3">
                            <div class="well well-sm">
                                <div class="row">
                                    <div class="col-xs-8 col-md-8 col">
                                        <p><strong>{{ucfirst($list->subject)}} </strong></p>
                                    </div>
                                    <div class="col-xs-3 col-md-3 col">
                                        <p>{{\Carbon\Carbon::parse($list->created_at)->format('j F Y')}}</p>
                                    </div>
                                    <div class="col-xs-1 col-md-1 col">
                                        <a href="javascript:void(0);" class="text-success" data-toggle="tooltip" title="{{__('frontend.view_chats')}}" onclick="getSupportDetails({{$list->id }})"><i class="fa fa-eye"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @else
                        <div class="col-md-12 cardf ml-3">
                            <div class="well well-sm">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p><b>{{__('frontend.no_chats')}}</b></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@include('frontend.common.common_script')

@endsection
@push('css')
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('assets\frontend\custom\css\supportchat_style.css') }}" />
@endpush
@push('scripts')
    <script>
         function getSupportDetails(support_id) {

$('.ajax-loading-buz').show();

    $('#send-msg').data('req-id', support_id);
    $.ajax({
        url: `{{ url('/support-chat') }}/${support_id}`,
        type: 'GET',
        success: function(data) {
            $('.ajax-loading-buz').hide();
            let chat = data.data

            $('#chat-history .msg_history').empty()
            // console.log("msg",chat);
            chat.forEach(msg => {
                $('#chat-history .msg_history').append(
                    msg.user_type == 'website' ?
                    `
                    <div class="outgoing_msg">
                        <div class="sent_msg">
                            <p>${msg.message}</p>
                            <span class="time_date">${msg.time}</span>
                        </div>
                    </div>
                    `
                    :
                    `
                    <div class="incoming_msg">
                        <div class="received_msg">
                            <div class="received_withd_msg">
                                <p>${msg.message}</p>
                                <span class="time_date">${msg.time}</span>
                            </div>
                        </div>
                    </div>
                    `
                )
            })
             $('#request-modal').modal({
                show: true
             });
            setTimeout(() => {
                let objDiv = document.getElementById("chat-history");
                objDiv.scrollTop = objDiv.scrollHeight;
            }, 200)
        }
    })
}
$('#send-msg').click(function(e) {
let msg = $('#chat-msg').val();
let requestId = $(this).data('req-id');
if(msg)
{
    $('#chat-history .msg_history').append(`
        <div class="outgoing_msg">
            <div class="sent_msg">
                <p>${msg.replace(/\n/g, "<br>")}</p>
                <span class="time_date">now</span>
            </div>
        </div>
    `)

    let objDiv = document.getElementById("chat-history");
    objDiv.scrollTop = objDiv.scrollHeight;
    $.ajax({
        url: `{{ url('frondend/support-request/chat') }}/${requestId}`,
        type: 'POST',
        data: {
         message: msg
        },
        success: function(error) {
            $('#chat-msg').val('');
        }
    })
}



})

    </script>
@endpush