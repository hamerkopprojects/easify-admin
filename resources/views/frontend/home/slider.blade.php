@if (count($slider) > 0)
<header>
    <div class="owl-carousel owl-theme ">
        @foreach ($slider as $row_data)
        @php
        if($row_data->web_flag == 'U')
        $slider_link = $row_data->url;
        elseif($row_data->web_flag == 'C')
        $slider_link = isset($row_data->slider_category->slug) ? 'category/'.$row_data->slider_category->slug : '';
        elseif($row_data->web_flag == 'P')
        $slider_link = isset($row_data->slider_product->slug) ? 'products/'.$row_data->slider_product->slug : '';
        if($row_data->ar_flag == 'U')
        $slider_link_ar = $row_data->url_ar;
        elseif($row_data->ar_flag == 'C')
        $slider_link_ar = isset($row_data->slider_category1->slug) ? 'category/'.$row_data->slider_category1->slug : '';
        elseif($row_data->ar_flag == 'P')
        $slider_link_ar = isset($row_data->slider_product1->slug) ? 'products/'.$row_data->slider_product1->slug : '';
        @endphp
        @if(app()->getLocale() == 'en')
        @if(!empty($row_data->web_image))
        <div class="item">
            <div class="our-team">
                <a href="{{ (isset($slider_link) && !empty($slider_link)) ? $slider_link : 'javascript:void(0)' }}" {{$row_data->web_flag == 'U' ? 'target=_blank' : ''}}>
                    @if(is_file( public_path() .'/uploads/'. $row_data->web_image))
                    <img src="{{ url('uploads/'. $row_data->web_image) }}" alt="">
                    @endif
                </a>
                @if(!empty($row_data->title)|| !empty($row_data->content))
                <div class="cover">
                    <div class="container">
                        <div class="header-content">
                            <a href="{{ (isset($slider_link) && !empty($slider_link)) ? $slider_link : 'javascript:void(0)' }}" {{$row_data->web_flag == 'U' ? 'target=_blank' : ''}}>
                                @if(!empty($row_data->title))<h2>{{ $row_data->title }}</h2>@endif
                                @if(!empty($row_data->content))<h1>{{ $row_data->content }}</h1>@endif
                            </a>
                            @if(!empty($slider_link))<a href="{{ (isset($slider_link) && !empty($slider_link)) ? $slider_link : 'javascript:void(0)' }}" {{$row_data->web_flag == 'U' ? 'target=_blank' : ''}} class="btn button2">Shop Now</a>@endif
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
        @endif
        @else
        @if(!empty($row_data->web_image_ar))
        <div class="item">
            <div class="our-team">
                <a href="{{ (isset($slider_link_ar) && !empty($slider_link_ar)) ? $slider_link_ar : 'javascript:void(0)' }}" {{$row_data->ar_flag == 'U' ? 'target=_blank' : ''}}>
                    @if(is_file( public_path() .'/uploads/'. $row_data->web_image_ar))
                    <img src="{{ url('uploads/'. $row_data->web_image_ar) }}" alt="">
                    @endif
                </a>
                @if(!empty($row_data->title_ar)|| !empty($row_data->content_ar))
                <div class="cover">
                    <div class="container">
                        <div class="header-content">
                            <a href="{{ (isset($slider_link_ar) && !empty($slider_link_ar)) ? $slider_link_ar : 'javascript:void(0)' }}" {{$row_data->ar_flag == 'U' ? 'target=_blank' : ''}}>
                                @if(!empty($row_data->title_ar))<h2>{{ $row_data->title_ar }}</h2>@endif
                                @if(!empty($row_data->content_ar))<h1>{{ $row_data->content_ar }}</h1>@endif
                            </a>
                            @if(!empty($slider_link_ar))<a href="{{ (isset($slider_link_ar) && !empty($slider_link_ar)) ? $slider_link_ar : 'javascript:void(0)' }}" {{$row_data->ar_flag == 'U' ? 'target=_blank' : ''}} class="btn button2">Shop Now</a>@endif
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
        @endif
        @endif
        @endforeach
    </div>
</header>
@endif