@if(!empty($promotionalProducts))
<div class="promotional-items">
    <div class="container">
        <header>
            <div class="row">
                <div class="col-md-8">
                    <h2>{{__('frontend.promotional_items')}}</h2>
                </div>
                @if($promo_count > 8)
                <div class="col-md-4 showlef">
                    <a href="{{route('promotional.all')}}" class="link d-none d-md-inline-block viewmore">{{__('frontend.view_more')}}</a>
                </div>
                @endif
            </div>
        </header>
        <ul class="product-list">
            @php 
            $wishListAll = Helper::wish_listed_products();@endphp
            @foreach($promotionalProducts as $promo_items)
            @php
            $name = $promo_items->lang[0]->name ?? '';
            $inFav = in_array($promo_items->id, $wishListAll) ? 'active' : '';
            @endphp
            <li>
                <div class="product">
                    <header class="d-flex flex-wrap justify-content-between align-items-center">
                        <span class="total-discount">
                        </span>
                        <a onclick="event.preventDefault(); addToFavourites(this, {{ $promo_items->id }});" class="favourite {{$inFav}}" href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                    </header>
                    <div class="product-image">
                        <a href="{{route('products.detail', $promo_items->slug)}}">
                            @if(is_file( public_path() . '/uploads/' . $promo_items->cover_image))
                            <img src="{{ url('uploads/'.$promo_items->cover_image) }}" alt="img">
                            @else
                            <img src="{{ asset('assets/frontend/custom/images/no-image.png') }}" alt="img">
                            @endif
                        </a>
                    </div>
                    <span class="price-block">
                        @php
                        $discount_price = $promo_items->variant_price[0]->discount_price ?? NULL;
                        $act_price = $promo_items->variant_price[0]->price ?? NULL;
                        $commission = isset($promo_items->commission) ? $promo_items->commission->value : NULL;
                        $mark_up = isset($promo_items->variant_price) ? $promo_items->variant_price[0]->easify_markup : NULL;
                        $dis_commission = ($discount_price * $commission / 100);
                        $price_commission = ($act_price * $commission / 100);
                        $dis_markup = ($discount_price * $mark_up / 100);
                        $price_markup = ($act_price * $mark_up / 100);
                        $d_price = $discount_price + $dis_commission + $dis_markup;
                        $price = $act_price + $price_commission + $price_markup;
                        @endphp
                        @if(!empty($d_price))
                        <span id="actual-price" class="old-price">{{$currency[$lang]}} {{ $price }}</span>
                        <span id="discount-price" class="offer-price">{{$currency[$lang]}} {{ $d_price }} </span>
                        @else
                        <span id="actual-price" class="old-price"></span>
                        <span id="discount-price" class="offer-price">{{$currency[$lang]}} {{ $price }} </span>
                        @endif
                    </span>
                    <div class="product-desc">
                         <p>
                            <a href="{{route('products.detail', $promo_items->slug)}}"><span>{{ $name }}</span></a>
                        </p>
                    </div>
                     <ul class="rating">
                        @php $rating = $promo_items->reviews->sum('rating') ? $promo_items->reviews->sum('rating') / Count($promo_items->reviews) : 0; @endphp
                        @for($i = 1; $i <= 5; $i++ )
                        @if(round( $rating - .25 ) >= $i )
                        <li class="fa fa-star"></li>
                        @elseif(round( $rating + .25 ) >= $i )
                        <li class="fa fa-star-half-o"></li>
                        @else
                        <li class="fa fa-star-o"></li>
                        @endif
                        @endfor
                    </ul>
                    <footer class="d-flex flex-wrap justify-content-between align-items-center ">
                        <div class="left-column">
                            @if($promo_items->product_type == 'complex' && $promo_items->variant_price)
                            <span class="select-box ">
                                <select class="form-control" id="variant-select" onchange="changeDisplyaPrice(this);">
                                    @php $i = 0;@endphp
                                    @foreach($promo_items->variant_price as $variants)
                                    @php
                                    $v_name = $variants->variant_lang->name ?? '';
                                    
                                    $mark_up = isset($variants) ? $variants->easify_markup : NULL;
                                    $dis_commission = ($variants->discount_price * $commission / 100);
                                    $price_commission = ($variants->price * $commission / 100);
                                    $dis_markup = ($variants->discount_price * $mark_up / 100);
                                    $price_markup = ($variants->price * $mark_up / 100);
                                    
                                    $d_price = $variants->discount_price + $dis_commission + $dis_markup;
                                    $price = $variants->price + $price_commission + $price_markup;
                                    $d_price = ($d_price > 0) ? $d_price : '';
                                    $price = $price ?? '';
                                    @endphp
                                    <option
                                        price="{{ $price }}"
                                        d_price="{{ $d_price }}" min_stock="{{$promo_items->variant[$i]->pdt_branch->min_stock}}" max_stock="{{$promo_items->variant[$i]->pdt_branch->max_stock}}"
                                        value="{{ $variants->variant_lang_id }}">{{ $v_name }}</option>
                                    @php $i++;@endphp
                                    @endforeach
                                </select>
                            </span>
                            @endif
                            @if(isset($promo_items->variant[0]->pdt_branch))
                            <div class="quantity">
                                <input type="button" value="-" class="minus" min_stock="{{$promo_items->variant[0]->pdt_branch->min_stock ?? ''}}" onclick="updateCount(this)" id="min_amount">
                                <input type="number" class="qty" step="1" min="1" max="" value="{{$promo_items->variant[0]->pdt_branch->min_stock}}" id="pdt_qty">
                                <input type="button" value="+" class="plus" max_stock="{{$promo_items->variant[0]->pdt_branch->max_stock ?? ''}}" onclick="updateCount(this)" id="max_amount">
                            </div>
                            @endif
                        </div>
                        <div class="right-column">
                            <a style="cursor:pointer; color:#fff;" class="add-btn align-middle" onclick="addToCart(this, {{ $promo_items->id }}, '{{ $promo_items->product_type }}', 'promo')">Add</a>
                        </div>
                    </footer>
                </div>
            </li>
            @endforeach
        </ul>
    </div>
</div>
@endif