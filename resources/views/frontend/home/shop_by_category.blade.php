<div class="section category">
    @if (count($shop_by_cat) > 0)
    <div class="container">
        <header>
            <div class="row">
                <div class="col-md-8">
                    <h2>{{__('frontend.shop_by_cat')}}</h2>
                </div>
                @if ($shop_by_cat_count > 8)
                <div class="col-md-4 showlef load_more_category">
                    <a href="#" class="link d-none d-md-inline-block">{{__('frontend.show_all')}}</a>
                </div>
                @endif
            </div>
        </header>
        <ul class="category-list">
            @foreach ($shop_by_cat as $cat_val)
            @php 
            $query = \App\Models\Product::query();
            if ($region_id) {
                $query->where('supplier_branch_region', 'like', "%" . '{{' . $region_id . '}}' . "%");
            }
            $product_count= $query->where('status', 'active')->where('supplier_status', 'publish')->where('category_ids', 'like', "%" . '{{' . $cat_val->id . '}}' . "%")->count();
            @endphp
            @php $pos = app()->getLocale() == 'en' ? 0 : 1; @endphp
            <li>
                <a href="{{route('category.show', $cat_val->slug)}}">
                    <div class="image">
                        @if(isset($cat_val->lang[$pos]->image_path) && is_file( public_path() . '/uploads/' . $cat_val->lang[$pos]->image_path))
                        <img src="{{ url('uploads/'. $cat_val->lang[$pos]->image_path) }}" alt="img">
                        @elseif(isset($cat_val->lang[0]->image_path) && is_file( public_path() . '/uploads/' . $cat_val->lang[0]->image_path))
                        <img src="{{ url('uploads/'. $cat_val->lang[0]->image_path) }}" alt="img">
                        @else
                        <img src="{{ asset('assets/frontend/custom/images/no-cat-image.png') }}" alt="img">
                        @endif
                    </div>
                    <p>{{$cat_val->lang[$pos]->name}}</p>
                    <p class="subpro">{{$product_count}} {{ $product_count > 1 ? 'Products' : 'Product'}}</p>
                </a>
            </li>
            @endforeach
        </ul>
    </div>
    @endif
</div>