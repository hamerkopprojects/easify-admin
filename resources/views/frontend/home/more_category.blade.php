@foreach ($shop_by_cat as $cat_val)
@php $pos = app()->getLocale() == 'en' ? 0 : 1; @endphp
@php 
$query = \App\Models\Product::query();
if ($region_id) {
    $query->where('supplier_branch_region', 'like', "%" . '{{' . $region_id . '}}' . "%");
}
$product_count= $query->where('status', 'active')->where('supplier_status', 'publish')->where('category_ids', 'like', "%" . '{{' . $cat_val->id . '}}' . "%")->count();
@endphp
<li class="more_list_div">
    <a href="{{route('category.show', $cat_val->slug)}}">
        <div class="image">
            @if(isset($cat_val->lang[$pos]->image_path) && is_file( public_path() . '/uploads/' . $cat_val->lang[$pos]->image_path))
            <img src="{{ url('uploads/'. $cat_val->lang[$pos]->image_path) }}" alt="img">
            @elseif(isset($cat_val->lang[0]->image_path) && is_file( public_path() . '/uploads/' . $cat_val->lang[0]->image_path))
            <img src="{{ url('uploads/'. $cat_val->lang[0]->image_path) }}" alt="img">
            @else
            <img src="{{ asset('assets/frontend/custom/images/no-image.png') }}" alt="img">
            @endif
        </div>
        <p>{{$cat_val->lang[$pos]->name}}</p>
        <p class="subpro">{{$product_count}} {{ $product_count > 1 ? 'Products' : 'Product'}}</p>
    </a>
</li>
@endforeach
<div class="row show_less_div" style="width: 100%">
    <div class="col-md-12 text-md-right">
        <a href="#" class="link d-none d-md-inline-block">Show Less</a>
    </div>
</div>
<script>
    $('.show_less_div').on('click', function (e) {
        e.preventDefault();
        $('.ajax-loading-buz').show();
        $(this).css('display', 'none');
        $('.more_list_div').css('display', 'none');
        $('.load_more_category').css('display', 'block');
        $('.ajax-loading-buz').hide();
    });
</script>
