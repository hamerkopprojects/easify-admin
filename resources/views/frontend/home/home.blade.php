@extends('frontend.frontend_layouts.frontend_app')
@section('content')

<!-- banner -->
@include('frontend.home.slider')

<div id="content">

    <!-- top slide -->
    @include('frontend.home.top_banner')

    <!-- category -->
    @include('frontend.home.shop_by_category')

    <!-- promotional items -->
    @include('frontend.home.promotional_items')

    <!-- btm slide -->
    @include('frontend.home.bottom_banner')

    <!-- Common script -->
    @include('frontend.common.common_script')

</div>

@endsection
@push('scripts')
@if(app()->getLocale() == 'en')
<script>
    $('.owl-carousel').owlCarousel({
        loop: true,
        items: 2,
        margin: 0,
        padding: 0,
        autoplay: true,<!-- autoplayTimeout:9000 , -->
        dots: false,<!-- animateOut: 'slideOutUp', -->
        nav: true,
        navText: [
            '<i class="fa fa-angle-left"></i>',
            '<i class="fa fa-angle-right"></i>'
        ],
        autoplay:true,
                responsiveClass: true,
        responsiveRefreshRate: true,
        responsive: {
            0: {
                items: 1,
                nav: false,
                dots: false,
            },
            600: {
                items: 1,
                nav: false,
            },
            1000: {
                items: 1,
                nav: false,
            }
        }
    });
        $('.load_more_category').on('click', function (e) {
            $('.ajax-loading-buz').show();
            e.preventDefault();
            loadMoreData();
        });
        function loadMoreData() {
            $.ajax(
                    {
                        url: "{{route('frontend.loadMore')}}",
                        type: "POST",
                    })
                    .done(function (data)
                    {
                        $(".category-list").append(data);
                        $('.load_more_category').css('display', 'none');
                        $('.ajax-loading-buz').hide();
                    })
        }
</script>
@else
<script>
            $('.owl-carousel').owlCarousel({
            loop:true,
            items:2,
            margin:0,
	    rtl: true,
            padding:0,
            autoplay:true,
            <!-- autoplayTimeout:9000 , -->
            dots: false,
            <!-- animateOut: 'slideOutUp', -->
             nav: true,
            	navText: [
            	'<i class="fa fa-angle-left"></i>',
            	'<i class="fa fa-angle-right"></i>'
            	],
              autoplay:true,
            	responsiveClass: true,
            	responsiveRefreshRate: true,
             responsive:{
                 0:{
                     items:1,
            nav:false,
            dots: false,
                 },
                 600:{
                     items:1,
            nav:false,
                 },
                 1000:{
                     items:1,
            nav:false,
                 }
             }
            })
         </script>
@endif
@endpush