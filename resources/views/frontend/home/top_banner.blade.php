@if (count($top_banners) > 0)
<div class=" section1 pt-0 mt-5">
    <div class="container">
        <div class="row">
        @foreach ($top_banners as $row_data)
        @php if($row_data->web_flag == 'U')
        $banner_link = $row_data->url;
        elseif($row_data->web_flag == 'C')
        $banner_link = isset($row_data->webcategory->slug) ? 'category/'.$row_data->webcategory->slug : '';
        elseif($row_data->web_flag == 'P')
        $banner_link = isset($row_data->webproduct->slug) ? 'products/'.$row_data->webproduct->slug : '';
        @endphp
        <div class="col-lg-6">
            <div class="banner">
                <a href="{{ (isset($banner_link) && !empty($banner_link)) ? $banner_link : 'javascript:void(0)' }}"> 
                    @if(is_file( public_path() .'/uploads/'. $row_data->web_image))
                    <img src="{{ url('uploads/'. $row_data->web_image) }}" alt="">
                    @endif
                </a>
            </div>
        </div>
        @endforeach
        </div>
    </div>
</div>
@endif