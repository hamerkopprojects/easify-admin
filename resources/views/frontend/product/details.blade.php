<div class="product-details">
    <div class="row">
        <div class="col-lg-6 mt-5">
            <section id="fancy">
                <div class="row">
                    <div class="container">
                        <div class="large-5 column">
                            <div class="xzoom-container">
                                @if(count($product_data->photos) > 0 && is_file( public_path() .'/uploads/'. $product_data->photos[0]->path))
                                <img class="xzoom4" id="xzoom-fancy" src="{{ url('uploads/'. $product_data->photos[0]->path) }}" xoriginal="{{ url('uploads/'. $product_data->photos[0]->path) }}" />
                                @else
                                <img class="xzoom4" id="xzoom-fancy" src="{{ asset('assets/frontend/custom/images/no-image-large.png') }}" alt="Product">
                                @endif
                                @if(isset($product_data->photos) && count($product_data->photos) > 0)
                                <div class="xzoom-thumbs">
                                    @foreach ($product_data->photos as $row_data)
                                    @if(isset($row_data->path) && is_file( public_path() .'/uploads/'. $row_data->path))
                                    <a href="{{ url('uploads/'. $row_data->path) }}">
                                        <img class="xzoom-gallery4" width="80" src="{{ url('uploads/'. $row_data->path) }}"  xpreview="{{ url('uploads/'. $row_data->path) }}" title="{{ $product_data->lang[0]->name }}">
                                    </a>
                                    @endif
                                    @endforeach
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <div class="col-lg-6 mt-5">
            <div class="info pdt_detail">
                <h2>{{ $product_data->lang[0]->name }}</h2>
                <p class="price">
                    @php
                    $discount_price = $product_data->variant_price[0]->discount_price ?? NULL;
                    $act_price = $product_data->variant_price[0]->price ?? NULL;
                    $commission = isset($product_data->commission) ? $product_data->commission->value : NULL;
                    $mark_up = isset($product_data->variant_price) ? $product_data->variant_price[0]->easify_markup : NULL;
                    $dis_commission = ($discount_price * $commission / 100);
                    $price_commission = ($act_price * $commission / 100);
                    $dis_markup = ($discount_price * $mark_up / 100);
                    $price_markup = ($act_price * $mark_up / 100);
                    $d_price = $discount_price + $dis_commission + $dis_markup;
                    $price = $act_price + $price_commission + $price_markup;
                    @endphp
                <p class="price">
                    @if(!empty($d_price))
                    <del id="actual-price">{{$currency[$lang]}} {{ number_format($price, 2, ".", "") }} </del>
                    <strong id="discount-price"> {{$currency[$lang]}} {{ number_format($d_price, 2, ".", "") }}</strong>
                    @else
                    <strong id="actual-price"> {{$currency[$lang]}} {{ number_format($price, 2, ".", "") }}</strong>
                    <strong id="discount-price"></strong>
                    @endif
                    &nbsp; &nbsp; &nbsp;
                    <span class="badge badge-secondary">{{ $product_data->variant_price[0]->discount ? $product_data->variant_price[0]->discount.' % OFF': '' }}</span>
                </p>
                <div class="buttons">
                    @if($product_data->product_type == 'complex' && $product_data->variant_price)
                    <span class="select-box align-middle">
                        <select class="form-control" id="variant-select" onchange="changeDisplyaPrice(this, 'details');">
                            @php $i = 0;@endphp
                            @foreach($product_data->variant_price as $variants)
                            @if($lang == 'ar')
                            @php
                            $variant_ar = \App\Models\VariantLang::where('en_matching_id', $variants->variant_lang->id)->pluck('name')->first();
                            $variant_name = $variant_ar;  @endphp
                            @else
                            @php $variant_name = $variants->variant_lang->name; @endphp
                            @endif
                            @php
                            $discount_price = $variants->discount_price ?? NULL;
                            $act_price = $variants->price ?? NULL;
                            $commission = isset($product_data->commission) ? $product_data->commission->value : NULL;
                            $mark_up = isset($variants->easify_markup) ? $variants->easify_markup : NULL;
                            $dis_commission = ($discount_price * $commission / 100);
                            $price_commission = ($act_price * $commission / 100);
                            $dis_markup = ($discount_price * $mark_up / 100);
                            $price_markup = ($act_price * $mark_up / 100);
                            $d_price = $discount_price + $dis_commission + $dis_markup;
                            $price = $act_price + $price_commission + $price_markup;
                            $d_price = ($d_price > 0) ? $d_price : '';
                            $price = $price ?? '';
                            @endphp
                            <option price="{{ $price }}" d_price="{{ $d_price }}" min_stock="{{$product_data->variant[$i]->pdt_branch->min_stock}}" max_stock="{{$product_data->variant[$i]->pdt_branch->max_stock}}"
                                    value="{{ $variants->variant_lang->id }}">{{ $variant_name ?? ''}}</option>
                            @php $i++;@endphp
                            @endforeach
                        </select>
                    </span>
                    @endif
                    <span class="select-box align-middle">
                        <div class="quantity qyanch">
                            <input type="button" value="-" class="minus" min_stock="{{$product_data->variant[0]->pdt_branch->min_stock}}" onclick="updateCount(this)" id="min_amount">
                            <input type="number" class="qty" step="1" min="1" max="" value="{{$product_data->variant[0]->pdt_branch->min_stock}}" id="pdt_qty">
                            <input type="button" value="+" class="plus" max_stock="{{$product_data->variant[0]->pdt_branch->max_stock}}" onclick="updateCount(this)" id="max_amount">
                        </div>
                    </span>
                    <a href="#" class="add-btn align-middle" onclick="addToCart(this, {{ $product_data->id }}, '{{ $product_data->product_type }}', 'detail')"><img src="{{ asset('assets/frontend/img/shopping1.png') }}" class="avtadd">{{__('frontend.add_to_cart')}}</a>
                </div>
                <div class="clearfix"></div>
                <p>{{__('frontend.minq')}}: <strong>{{$product_data->variant[0]->pdt_branch->min_stock}}</strong> &nbsp;&nbsp;&nbsp; {{__('frontend.maxq')}}: <strong>{{$product_data->variant[0]->pdt_branch->max_stock}} </strong></p>
                <p>{{__('frontend.del_loc')}}: <strong class="location-to-deliver"></strong></p>
                <p>{{__('frontend.supplier_code')}}: <strong>{{ $product_data->supplier->code ?? '' }}</strong></p>
                <footer>
                    @php $wishListAll = Helper::wish_listed_products();
                    $inFav = in_array($product_data->id, $wishListAll) ? 'fa-heart' : 'fa-heart-o'; @endphp
                    <a href="javascript:void(0);" onclick="event.preventDefault(); addToFavourites(this, {{ $product_data->id }}, 'details');"><i class="fa {{$inFav}}" aria-hidden="true"></i> {{__('frontend.add_wish')}}</a>
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#shareModal"><i class="fa fa-share-alt" aria-hidden="true"></i> {{__('frontend.share')}}</a>
                </footer>
            </div>
        </div>
    </div>
    <div class="box1">
        <h3>{{__('frontend.desc')}}</h3>
        <p>{{ $product_data->lang[0]->description ?? '' }}</p>
        @if(count($product_data->product_attribute) > 0)
        <hr />
        <h3>{{__('frontend.specific')}}</h3>
        @foreach ($product_data->product_attribute as $key => $val)
        @if($val->attribute_type == 'dropdown')
        @if(isset($val->variant->name))
        <p class="lead">{{$val->attribute->lang[0]->name ?? ''}}: <strong>{{$val->variant->name ?? ''}}</strong></p>
        @endif
        @else
        @if(isset($val->lang[0]->name))
        <p class="lead">{{$val->attribute->lang[0]->name ?? ''}}: <strong>{{$val->lang[0]->name ?? ''}}</strong></p>
        @endif
        @endif
        @endforeach
        @endif
    </div>
</div>
<div class="modal fade none-border" id="shareModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Share</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Share::currentPage()
                ->facebook('Easify')
                ->twitter('Easify')
                ->linkedin('Easify')
                ->whatsapp('Easify') !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=" crossorigin="anonymous"></script>
<script src="{{ asset('js/share.js') }}"></script>
