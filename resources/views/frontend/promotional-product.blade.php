@extends('frontend.frontend_layouts.frontend_app')
@section('content')
<div id="content" class="bg-light ">
    <div class="container">
        <div id="breadcrumbs">
            <p><a href="{{ route('frontend') }}">{{__('frontend.home')}} </a><i class="fa fa-long-arrow-right" aria-hidden="true"></i> <span> {{__('frontend.promo_pdts')}}</span>
            </p>
        </div>
        <div class="row" id="pdt_list">
            @include('frontend.promoproduct.filter')
        </div>
    </div>
</div>
@endsection
@include('frontend.common.common_script')
@push('css')
<style type="text/css">
    .ajax-loading{
        text-align: center;
    }
</style>
@endpush
@push('scripts')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $("#frm_promo_filter").submit(function () {
        $('.ajax-loading-buz').show();
        var form = $(this);
        $.ajax({
            type: "POST",
            url: "{{route('promoproduct.search')}}",
            data: form.serialize(), // serializes the form's elements.
            success: function (data)
            {
                $('#pdt_list').html(data);
                $('.ajax-loading-buz').hide();
            }
        });
    });
    $(document).ready(function () {
        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() > $(document).height() - 200) {
                var maxPage = $('#maxPage').val();
                var page = $('#page').val();
                if (maxPage > page) {
                    var page_new = parseInt(page) + 1;
                    $('#page').val(page_new);
                    $('.ajax-loading').hide();
                    var minval = $('#minval').val();
                    var maxval = $('#maxval').val();
                    var list_type = $('#list_type').val();
                    var sort = $('#sort').val();
                    var cat_id = $('#cat_id').val();
                    var brand_id = $('#brand_id').val();
                    var supplier_id = $('#supplier_id').val();
                    loadMoreData(page_new, maxPage, cat_id, brand_id, supplier_id, list_type, sort, minval, maxval);
                }
            }
        });
    });
    function loadMoreData(page, maxPage, cat_id, brand_id, supplier_id, list_type, sort, minval, maxval) {
        $.ajax(
                {
                    url: "{{route('promoproduct.loadmore')}}",
                    data: {'page': page, 'cat_id': cat_id,
                        'brand_id': brand_id, 'supplier_id': supplier_id, 'list_type': list_type, 'sort': sort, 'minval': minval, 'maxval': maxval},
                    type: "POST",
                    beforeSend: function ()
                    {
                        $('.ajax-loading').show();
                    }
                })
                .done(function (data)
                {
                    if (data.length == 0) {
                        $('.ajax-loading').hide();
                    }
                    $('.ajax-loading').hide();
                    $(".product-list").append(data);
                })
                .fail(function (jqXHR, ajaxOptions, thrownError)
                {
                    $('.ajax-loading').hide();
                });
    }
    function Sortval(sort)
    {
        $('#sort').val(sort.value);
        $("#frm_promo_filter").submit();
    }
    function getList(sel)
    {
        $('#list_type').val(sel);
        $("#frm_promo_filter").submit();
    }
    $("#slider-range").slider({
        range: true,
        min: 0,
        max: 10000,
        values: [0, 10000],
        slide: function (event, ui) {
            $("#minval").val(ui.values[0]);
            $("#maxval").val(ui.values[1]);
            $("#sidebar .widget .min p span").html(ui.values[0]);
            $("#sidebar .widget .max p span").html(ui.values[1]);
        }
    });

    $("#sidebar .widget .min p span").html($("#slider-range").slider("values", 0));
    $("#sidebar .widget .max p span").html($("#slider-range").slider("values", 1));
</script>
@endpush

