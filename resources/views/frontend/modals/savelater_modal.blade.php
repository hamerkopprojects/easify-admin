<div class="modal-body">
    <div class="mt-3 promotional-items">
        <div class="container">
            <header>
                <div class="row">
                    <div class="col-md-8 cartsave">
                        <h2>{{__('frontend.save_ltr')}}</h2><p>{{ count($savelater) }} {{ count($savelater) > 1 ? __('frontend.items') : __('frontend.item')}}</p>
                    </div>
                    <div class="col-md-4 text-md-right">
                        <button type="button" class="close" data-dismiss="modal"><img src="{{ asset('assets/frontend/img/cancel.png') }}" class="alcancel"></button>
                    </div>
                </div>
            </header>
            @if(count($savelater) > 0)
            <ul class="row p-0">
                @php $wishListAll = Helper::wish_listed_products();@endphp
                @foreach ($savelater as $pdt_info_val)
                @php $inFav = in_array($pdt_info_val->product->id, $wishListAll) ? 'active' : ''; @endphp
                <div class="col-lg-4 mb-3">
                    <div class="product">
                        <header class="d-flex flex-wrap justify-content-between align-items-center">
                            <span class="total-discount">
                            </span>
                            <a onclick="event.preventDefault(); addToFavourites(this, {{ $pdt_info_val->product->id }}, '');" class="favourite {{$inFav}}" href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                        </header>
                        <div class="product-image">
                            <a href="{{route('products.detail', $pdt_info_val->product->slug)}}">
                                @if(is_file( public_path() . '/uploads/' . $pdt_info_val->product->cover_image))
                                <img src="{{ url('uploads/'.$pdt_info_val->product->cover_image) }}" alt="img">
                                @else
                                <img src="{{ asset('assets/frontend/custom/images/no-image.png') }}" alt="img">
                                @endif
                            </a>
                        </div>
                        <span class="price-block ">
                            @php
                            $discount_price = $pdt_info_val->product->variant_price[0]->discount_price ?? NULL;
                            $act_price = $pdt_info_val->product->variant_price[0]->price ?? NULL;
                            $commission = isset($pdt_info_val->product->commission) ? $pdt_info_val->product->commission->value : NULL;
                            $mark_up = isset($pdt_info_val->product->variant_price) ? $pdt_info_val->product->variant_price[0]->easify_markup : NULL;
                            $dis_commission = ($discount_price * $commission / 100);
                            $price_commission = ($act_price * $commission / 100);
                            $dis_markup = ($discount_price * $mark_up / 100);
                            $price_markup = ($act_price * $mark_up / 100);
                            $d_price = $discount_price + $dis_commission + $dis_markup;
                            $price = $act_price + $price_commission + $price_markup;
                            @endphp
                            @if(!empty($d_price))
                            <span id="actual-price" class="old-price">{{$currency[$lang]}} {{ $price }}</span>
                            <span id="discount-price" class="offer-price">{{$currency[$lang]}} {{ $d_price }} </span>
                            @else
                            <span id="actual-price" class="old-price"></span>
                            <span id="discount-price" class="offer-price">{{$currency[$lang]}} {{ $price }} </span>
                            @endif
                        </span>

                        <div class="product-desc">
                            <a href="{{route('products.detail', $pdt_info_val->product->slug)}}">
                                <p><span>{{ $pdt_info_val->product->lang[0]->name }}</span></p>
                            </a>
                        </div>
                        <footer class="d-flex flex-wrap justify-content-between align-items-center ">
                            <div class="left-column">
                                @if($pdt_info_val->product->product_type == 'complex' && $pdt_info_val->product->variant_price)
                                <span class="select-box">
                                    <select class="form-control" id="variant-select" onchange="changeDisplyaPrice(this);">
                                        @php $i = 0;@endphp
                                        @foreach($pdt_info_val->product->variant_price as $variants)
                                        @if($lang == 'ar')
                                        @php
                                        $variant_ar = \App\Models\VariantLang::where('en_matching_id', $variants->variant_lang->id)->pluck('name')->first();
                                        $variant_name = $variant_ar;  @endphp
                                        @else
                                        @php $variant_name = $variants->variant_lang->name; @endphp
                                        @endif
                                        @php
                                        $discount_price = $variants->discount_price ?? NULL;
                                        $act_price = $variants->price ?? NULL;
                                        $commission = isset($pdt_info_val->product->commission) ? $pdt_info_val->product->commission->value : NULL;
                                        $mark_up = isset($variants->easify_markup) ? $variants->easify_markup : NULL;
                                        $dis_commission = ($discount_price * $commission / 100);
                                        $price_commission = ($act_price * $commission / 100);
                                        $dis_markup = ($discount_price * $mark_up / 100);
                                        $price_markup = ($act_price * $mark_up / 100);
                                        $d_price = $discount_price + $dis_commission + $dis_markup;
                                        $price = $act_price + $price_commission + $price_markup;
                                        $d_price = ($d_price > 0) ? $d_price : '';
                                        $price = $price ?? '';
                                        @endphp
                                        <option price="{{ $price }}" d_price="{{ $d_price }}" min_stock="{{$pdt_info_val->product->variant[$i]->pdt_branch->min_stock}}" max_stock="{{$pdt_info_val->product->variant[$i]->pdt_branch->max_stock}}"
                                                value="{{ $variants->variant_lang->id }}"  {{ ($pdt_info_val->variant_id == $variants->variant_lang->id) ? 'selected' : '' }}>{{ $variant_name ?? ''}}</option>
                                        @php $i++;@endphp
                                        @endforeach
                                    </select>
                                </span>
                                @endif
                                @if(isset($pdt_info_val->product->variant[0]->pdt_branch))
                                <div class="quantity">
                                    <input type="button" value="-" class="minus" min_stock="{{$pdt_info_val->product->variant[0]->pdt_branch->min_stock}}" onclick="updateCount(this)" id="min_amount">
                                    <input type="number" class="qty" step="1" min="1" max="" value="{{$pdt_info_val->item_count ?? $pdt_info_val->product->variant[0]->pdt_branch->min_stock}}" id="pdt_qty">
                                    <input type="button" value="+" class="plus" max_stock="{{$pdt_info_val->product->variant[0]->pdt_branch->max_stock}}" onclick="updateCount(this)" id="max_amount">
                                </div>
                                @endif
                            </div>
                            <div class="right-column">
                                <a href="#" class="add-btn align-middle" onclick="addToCart(this, {{ $pdt_info_val->product->id }}, '{{ $pdt_info_val->product->product_type }}', 'savelater')">{{__('frontend.add')}}</a>
                            </div>
                        </footer>
                    </div>
                </div>
                @endforeach
            </ul>
            @endif
        </div>
    </div>
</div>
