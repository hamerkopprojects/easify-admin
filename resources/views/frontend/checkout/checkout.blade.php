@extends('frontend.frontend_layouts.frontend_app')
@section('content')
<div id="content" class="bg-light page-cart pagecheck">
    <div class="container ">
        <form id="frm_payment_slip" action="javascript:void(0);" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-8 mt-5">
                    <h1>{{__('frontend.checkout')}}</h1>

                    <div>
                        @php
                        $tot_amount = $cart->sub_total;
                        $coupon_value = $currency[$lang].' '.number_format($cart->coupon_value, 2, ".", "");
                        $tot_amount = $tot_amount - $cart->coupon_value;
                        $vat_percent = $settings->vat_percentage ? $tot_amount * $settings->vat_percentage / 100 : 0;
                        $final_amount = $tot_amount + $cart->delivery_charge + $vat_percent;
                        @endphp
                        <!-- Addresses -->
                        @if($delivery_address)
                        @php
                        $_name = $delivery_address->region->lang[0]->name ?? '';
                        @endphp
                        <div class="text-left boxcheck ">
                            <h2>{{__('frontend.del_add')}}</h2>
                            <a href="{{ route('customer.adddresses') }}/checkout" class="editadd"><img src="{{ asset('assets/frontend//img/edit2.png') }}" /></a>
                            <p> {{ $delivery_address->name }}</p>
                            <p> {{ $delivery_address->apartment_name }}</p>
                            <p> {{ $delivery_address->street_name }}</p>
                            <p> {{ $delivery_address->landmark }}</p>
                            <p> PO Box: {{ $delivery_address->postal_code }}</p>
                            <p> {{ $_name }}</p>
                            <p> {{ $delivery_address->phone }} </p>
                            <!-- schedule -->
                        </div>
                        @else
                        <div class="text-left boxcheck ">
                            <h2>{{__('frontend.del_add')}}</h2>
                            <a href="{{ route('customer.adddresses') }}/checkout" class="editadd"><img src="{{ asset('assets/frontend/img/edit2.png') }}" /></a>
                            <p>{{__('frontend.no_del_add')}}</p>
                        </div>
                        @endif

                        @if($billing_address)
                        @php
                        $_name = $billing_address->region->lang[0]->name ?? '';
                        @endphp
                        <div class="text-left boxcheck ">
                            <h2>{{__('frontend.bill_add')}}</h2>
                            <a href="{{ route('customer.adddresses') }}/checkout" class="editadd"><img src="{{ asset('assets/frontend/img/edit2.png') }}" /></a>
                            <p> {{ $billing_address->name }}</p>
                            <p> {{ $billing_address->apartment_name }}</p>
                            <p> {{ $billing_address->street_name }}</p>
                            <p> {{ $billing_address->landmark }}</p>
                            <p> PO Box: {{ $billing_address->postal_code }}</p>
                            <p> {{ $_name }}</p>
                            <p> {{ $billing_address->phone }} </p>
                            <!-- schedule -->
                        </div>
                        @else
                        <div class="text-left boxcheck ">
                            <h2>{{__('frontend.bill_add')}}</h2>
                            <a href="{{ route('customer.adddresses') }}/checkout" class="editadd"><img src="{{ asset('assets/frontend/img/edit2.png') }}" /></a>
                            <p>{{__('frontend.no_bill_add')}}</p>
                        </div>
                        @endif
                    </div>

                    <div class="text-left boxcheck ">
                        <h2>{{__('frontend.tim_sl')}}</h2>
                        <button class="editadd hidicon"  id ="changedate"><img src="{{ asset('assets/frontend/img/edit2.png') }}" /></button>
                        <p>{{__('frontend.del_loc')}}: <strong class="location-to-deliver"></strong></p>
                        @php $today = date('Y-m-d'); @endphp
                        <p> {{__('frontend.del_dat_ti')}} <strong id="order_del_date">: {{ date('j F Y', strtotime($today. ' + '.$settings->delivery_days.' days')) ?? '' }},</strong> <strong id="del_time_slots">{{ $time_slots[1] }}</strong></p>
                        <div class="" id="datebox">
                            <div class="clearfix"></div>
                            <button type="button" class="close margin26" id="hidedate"><img src="{{ asset('assets/frontend/img/cancel.png') }}" class="imgclose editadd "></button>
                            <div class="clearfix"></div>
                            <div class="time-picker-container">
                                <div class="time-picker">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-12 mt-3 p-0">
                                                @php
                                                $start_date = date('Y-m-d', strtotime($today. ' +' .$settings->delivery_days.' days'));
                                                $begin = new DateTime($start_date);
                                                $end_date = date('Y-m-d', strtotime($start_date. ' + 7 days'));
                                                $end = new DateTime($end_date);

                                                $interval = DateInterval::createFromDateString('1 day');
                                                $period = new DatePeriod($begin, $interval, $end);
                                                $i=0;
                                                @endphp
                                                <input type='hidden' name='delivery_date' id='delivery_date' value="{{$start_date}}">
                                                @foreach ($period as $dt)
                                                @php
                                                $i++;
                                                $full_date = $dt->format("Y-m-d \n");
                                                $day_name = $dt->format("D \n");
                                                $date = $dt->format("j \n");
                                                $converted_date = date('j F Y', strtotime($full_date));
                                                @endphp
                                                <label class="container6 date_change" del_date='{{ $full_date }}' converted_date='{{$converted_date}}'>
                                                    <input type="radio" {{ ($i== 1) ? 'checked="checked"' : '' }} name="radio3">
                                                    <span class="textsh">{{ $date }}</span>
                                                    <span class="textmonth">{{ strtoupper($day_name) }}</span>
                                                    <span class="checkmark6"></span>
                                                </label>
                                                @endforeach
                                            </div>
                                            <div class="col-lg-12 mt-4">
                                                <ul class="time-slot">
                                                    <li class="time-slot-item active" time_slot='{{ $time_slots[1]}}'>{{ $time_slots[1] }}</li>
                                                    <li class="time-slot-item" time_slot='{{ $time_slots[2]}}'>{{ $time_slots[2] }}</li>
                                                    <input type='hidden' name='time_slot' id='time_slot' value="{{ $time_slots[1] }}">
                                                </ul>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="text-left boxcheck ">
                        <h2>{{__('frontend.sel_pay')}}</h2>
                        <div id="accordion" class="myaccordion">
                            @if(json_decode($settings->payment_options)->online== 'Y')
                            <div class="card" onclick="checkOption('online', {{ $final_amount }}, '', '{{$currency[$lang]}}')">
                                <div class="card-header" id="headingOne">
                                    <h2 class="mb-0">
                                        <button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                           <label class="container4">{{__('frontend.cr_dr_mada')}}
                                                <input type="radio" name="payment_method" value="online">
                                                <span class="checkmark4"></span>
                                            </label>
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row ">
                                            <div class="col-lg-12 ">
                                                <div class="form-group">
                                                    <label>{{__('frontend.card_no')}}</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="1234" id="">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="5678" id="">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="3456" id="">
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="2456" id="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4 col">
                                                <div class="form-group cart-dates">
                                                    <label>{{__('frontend.exp_date')}}</label>
                                                    <input type="text" class="form-control" placeholder="10/28/2020" id="datepicker">
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col">
                                                <div class="form-group">
                                                    <label>CVV</label>
                                                    <input type="password" class="form-control" placeholder="*****" id="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <div class="form-group">
                                                    <label>{{__('frontend.card_holder_name')}}</label>
                                                    <input type="text" class="form-control" placeholder="John Doe" id="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @if(json_decode($settings->payment_options)->ofline== 'Y')
                            <div class="card" onclick="checkOption('ofline', {{ $final_amount }}, '', '{{$currency[$lang]}}')">
                                <div class="card-header" id="headingTwo">
                                    <h2 class="mb-0">
                                        <button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            <label class="container4">{{__('frontend.off_pay')}}
                                                <input type="radio" name="payment_method" value="offline">
                                                <span class="checkmark4"></span>
                                            </label>
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="container p-y-1">
                                            <div class="row m-b-1">
                                                <!-- <div class="col-sm-6 offset-sm-3"> -->
                                                <div class="col-sm-12">
                                                    <div class="form-group inputDnD">
                                                    <label class="sr-only" for="inputFile">{{__('frontend.up_pay_slip')}}</label>
                                                    <input type="file" class="form-control-file text-success font-weight-bold" name="payment_slip" id="payment_slip" data-title="{{__('frontend.drag_and_drop_a_file')}}"></div>
                                                    <button type="button" class="btn btn-success btn-block" onclick="$('#payment_slip').click();" id="payment_block">{{__('frontend.upload')}}</button>
                                                    <img id="preview_img" class="cover-photo mt-3" width="200" onclick="$('#payment_slip').click();" src="" />
                                                    <p id="display_image" class="mt-3"></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @if(json_decode($settings->payment_options)->cod== 'Y')
                            <div class="card" onclick="checkOption('cod', {{ $final_amount }}, {{$settings->cod_fee}}, '{{$currency[$lang]}}')">
                                <div class="card-header" id="headingThree">
                                    <h2 class="mb-0">
                                        <button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            <label class="container4">{{__('frontend.cod')}}
                                                <input type="radio" name="payment_method" value="cod">
                                                <span class="checkmark4"></span>
                                            </label>
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body container">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <p>{{__('frontend.cod_fee')}}:<b class="text-success"> {{$currency[$lang]}} {{$settings->cod_fee ?? ''}}</b></p>
                                                <input type='hidden' name='cod_fee' id='cod_fee' value="{{$settings->cod_fee}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 ordersum mt-5 mtres">
                    <div class="well well-sm mb-3 cardfull  mt-5 mtres">
                        <div class="row">
                            <div class="col-lg-12 page-cart1">
                                <div class="">
                                    <h2 class="mt-3">{{__('frontend.order_sum')}}</h2>
                                    <table>
                                        <tbody>
                                            <tr>
                                                <th><b>{{__('frontend.sub_total')}}</b></th>
                                                <td data-title="Subtotal">{{$currency[$lang]}} {{ $cart->sub_total }}</td>
                                            </tr>
                                            @if(isset($cart->delivery_charge))
                                            <tr>
                                                <th>{{__('frontend.delivery_charges')}}</th>
                                                <td data-title="Delivery Charge">{{$currency[$lang]}} {{ $cart->delivery_charge }}</td>
                                            </tr>
                                            @endif
                                            <tr style="display:none" id="cod_fees">
                                                <th>{{__('frontend.cod_fee')}}</th>
                                                <td>{{$currency[$lang]}} {{ $settings->cod_fee }}</td>
                                            </tr>
                                            @if(isset($settings->vat_percentage))
                                            <tr>
                                                <th>{{__('frontend.vat')}} </th>
                                                <td data-title="Delivery Charge">{{ $settings->vat_percentage ?? 0 }} %</td>
                                            </tr>
                                            @endif
                                            @if(isset($cart->coupon_code))
                                             <tr>
                                                <th>{{__('frontend.discount')}}</th>
                                                <td data-title="Discount">{{ $coupon_value ?? '' }}</td>
                                            </tr>
                                            @endif
                                            <tr class="order-total baboth ">
                                                <th>{{__('frontend.total_amt')}}<span>({{__('frontend.inclusive_vat')}})</span></th>
                                                <td data-title="Total" id="total_amt">{{$currency[$lang]}} {{ number_format($final_amount, 2, ".", "") }} </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box1 mt-0 p-0">
                        <a style="cursor: pointer;" class="checkout-button place-order button1">{{__('frontend.place_order')}}</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- Common script -->
@include('frontend.common.common_script')
<script>
function checkOption(type, total, cod_fee='', currency = '') {
    if(type == 'cod' && cod_fee){
        var final = total + cod_fee;
        $('#total_amt').text(currency +' '+ final);  
        $('#cod_fees').show();
    }else{
      $('#cod_fees').hide();
      $('#total_amt').text(currency +' '+ total);  
    }
}
</script>
</div>

@endsection
@push('css')
<style type="text/css">
    .btn-block {
        padding: 2% 0 !important;
    }
</style>
@endpush
