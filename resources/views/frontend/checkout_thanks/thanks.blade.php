@extends('frontend.frontend_layouts.frontend_app')
@section('content')
<div id="content" class="bg-light page-cart page-cart1 pagbor">
    <div class="container">
        <div class="container">

            <div class="row mb-5">
                <div class="col-lg-3"></div>
                <div class="col-lg-6 text-center">
                    <div class=" mt-0 p-0 ">
                        <div class="thankclass pt-5">
                            <img src="{{ asset('assets/frontend/img/correct.png') }}" class="mb-3">
                            <h4>{{__('frontend.thanks') }} {{ $customer_name }}</h4>
                            <p>{{__('frontend.order_has_been_created') }}</p>
                            <p>{{__('frontend.order_has_been_sent_to_your_mail') }}</p>
                            <div class="mt-5 ">
                                <p>{{__('frontend.order_id') }}:<b> {{ $order->order_id }}</b></p>
                                <p> {{__('frontend.order_status') }}: <b>Order Received</b></p>
                                <p> {{__('frontend.deli_time') }}: <b>{{ date('j F Y', strtotime($order->delivery_schedule_date))}}, {{ $order->time_slot }}</b></p>
                                @if($order->payment_method == 'cod')
                                @php $payment_method = 'Cash on delivery';@endphp
                                @elseif($order->payment_method == 'offline')
                                @php $payment_method = 'Offline Payment';@endphp
                                @else
                                @php $payment_method = 'Credit/Debit/MADA';@endphp
                                @endif
                                <p>{{__('frontend.pay_method') }}: <b> {{ $payment_method }}</b></p>

                            </div>
                            <div class="text-left">
                                <h2>{{__('frontend.bill_details') }}</h2>
                                @php
                                $final_amount = $order->sub_total;
                                if(isset($order->coupon_code)){
                                if($order->coupon_type == 'amount'){
                                $final_amount = $final_amount - $order->coupon_value;
                                $coupon_value = $currency[$lang]. ' '.number_format($order->coupon_value, 2, ".", "");
                                }
                                else {
                                $coupon_value = $final_amount * $order->coupon_value / 100;
                                $coupon_value =$currency[$lang]. ' '.number_format($coupon_value, 2, ".", "");
                                $final_amount = $final_amount - ($final_amount * $order->coupon_value / 100);
                                }
                                }
                                @endphp
                                <table>
                                    <tbody>
                                        <tr>
                                            <th>{{__('frontend.sub_total') }}</th>
                                            <td data-title="Subtotal">{{$currency[$lang]}} {{ $order->sub_total }}</td>
                                        </tr>
                                        @if(isset($order->delivery_charge))
                                        <tr>
                                            <th>{{__('frontend.del_charge')}}</th>
                                            <td data-title="Discount">{{$currency[$lang]}} {{ $order->delivery_charge }}</td>
                                        </tr>
                                        @endif
                                        @if(isset($order->vat))
                                        <tr>
                                            <th>{{__('frontend.vat')}}</th>
                                            <td data-title="Vat">{{ $order->vat }} %</td>
                                        </tr>
                                        @endif
                                        @if($order->payment_method == 'cod')
                                        <tr>
                                            <th>{{__('frontend.cod_fee') }}</th>
                                            <td data-title="COD Fee">{{$currency[$lang]}} {{ $order->cod_fee }}
                                            </td>
                                        </tr>
                                        @endif
                                        @if(isset($order->coupon_code))
                                         <tr>
                                            <th>{{__('frontend.discount') }} </th>
                                            <td data-title="Discount">{{ $coupon_value ?? '' }}</td>
                                        </tr>
                                        @endif
                                        <tr class="order-total baboth">
                                            <th>{{__('frontend.total_amt') }} <span>({{__('frontend.inclusive_vat') }}) </span>
                                            </th>
                                            @php 
                                            $vat_percent = $order->vat ? $final_amount * $order->vat / 100 : 0;
                                            $final_amount = $final_amount + $order->delivery_charge + $order->cod_fee + $vat_percent; @endphp
                                            <td data-title="Total">{{$currency[$lang]}} {{ number_format($final_amount, 2, ".", "") }}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="pb-5 ">
                            <a onclick="location.href ='{{route('order.list') }}/{{ $order->id }}'"><button type="button" class="btn btn-dark text-white btnheight w44">{{__('frontend.view_order') }}</button></a>
                            <button onclick="location.href ='{{ route('frontend') }}'" type="button" class="btn btn-success btnheight  w44">{{__('frontend.cont_shopping') }}</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3"></div>
            </div>

        </div>

    </div>

    @include('frontend.common.common_script')

</div>

@endsection