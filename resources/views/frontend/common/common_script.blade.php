@push('scripts')

<script>
    $(document).ready(function() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#myModal, #CartModal').on('hidden.bs.modal', function () {
        location.reload();
        });
        
        $("#show").on("click",function(){
        var x = $("#password");
        if (x.attr('type') === "password") {
                x.attr('type','text');
                $(this).removeClass('fa fa-eye-slash')
                $(this).addClass('fa fa-eye')
            } else {
                x.attr('type','password');
                $(this).removeClass('fa fa-eye')
                $(this).addClass('fa fa-eye-slash')
        } // End of if
        })// End of click event
        
         $(".show1").on("click", function () {
            var x = $("#password1");
            if (x.attr('type') === "password") {
                x.attr('type', 'text');
                $(this).removeClass('fa fa-eye-slash')
                $(this).addClass('fa fa-eye')
            } else {
                x.attr('type', 'password');
                $(this).removeClass('fa fa-eye')
                $(this).addClass('fa fa-eye-slash')
            } // End of if
        })// End of click event
        
        $(".show3").on("click", function () {
            var x = $("#password3");
            if (x.attr('type') === "password") {
                x.attr('type', 'text');
                $(this).removeClass('fa fa-eye-slash')
                $(this).addClass('fa fa-eye')
            } else {
                x.attr('type', 'password');
                $(this).removeClass('fa fa-eye')
                $(this).addClass('fa fa-eye-slash')
            } // End of if
        })// End of click event
        
        $(".show4").on("click", function () {
            var x = $("#password4");
            if (x.attr('type') === "password") {
                x.attr('type', 'text');
                $(this).removeClass('fa fa-eye-slash')
                $(this).addClass('fa fa-eye')
            } else {
                x.attr('type', 'password');
                $(this).removeClass('fa fa-eye')
                $(this).addClass('fa fa-eye-slash')
            } // End of if
        })// End of click event

        $(".nav-tabs a").click(function(){
            if( $(this).attr('id') == 'a-home' || $(this).attr('id') == 'a-password') {
                $('.divhide').show();
                $('#home-radio').removeAttr('checked');
                $('#password-radio').removeAttr('checked');
                if($(this).attr('id') == 'a-home') {
                    $('#home-radio').attr('checked', 'checked');
                } else {
                    $('#password-radio').attr('checked', 'checked');
                }
            } else {
                $('.divhide').hide();
            }
            $(this).tab('show');
        });

        $("#frm_email_login").validate({
            normalizer: function(value) {
                return $.trim(value);
            },
            rules: {
                user_name: {
                    required: true,
                    email: true,
                },
                password: {
                    required: true,
                },
            },
            messages: {
                user_name: {
                    required:@json(__('frontend.email_req') ),
                    email: @json(__('frontend.iv_vd_emil') ),
                },
                password: {
                    required: @json(__('frontend.pass_req') ),
                },
            },
            submitHandler: function(form) {
                $('.ajax-loading-buz').show();
                $.ajax({
                    type: "POST",
                    url: "{{route('customer.login')}}",
                    data: $('#frm_email_login').serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.success == true) {
                            $('#myModal').modal('hide');
                            $('.navbar-nav a.login').attr('data-target', 'dropdown-menu').attr('data-toggle', 'dropdown');
                            $('span.cust-name').text('Hello, ' + data.customer_name);
                            $.toaster({
                                priority: 'success',
                                title: @json(__('frontend.success') ),
                                message: data.message,
                                timeout: 3000,
                            });
//                            $('.not-logged-in').hide();
//                            $('.logged-in').show();
//                            $('.customer_name').text(data.customer_name);
                            $('#add-from-wishlist').removeClass('hidden');
                            $('#add-from-savelater').removeClass('hidden');
                        } else {
                            $.toaster({
                                priority: 'danger',
                                title: @json(__('frontend.error')),
                                message: data.message,
                                timeout: 3000,
                            });
                        }
                    },
                    complete: function() {
                        $('.ajax-loading-buz').hide();
                    }
                });
                return false;
            }
        });
        
        $("#frm_forgot_password").validate({
        normalizer: function(value) {
        return $.trim(value);
        },
        rules: {
                        email: {
                        required: true,
                        email: true,
                        },
                },
        messages: {
                        email: {
                        required: @json(__('frontend.email_req') ),
                        email: @json(__('frontend.iv_vd_emil') ),
                        },
        },
                submitHandler: function(form) {
            $('.ajax-loading-buz').show();
            $.ajax({
                type: "POST",
                url: "{{route('customer.forgot_password')}}",
                data: $('#frm_forgot_password').serialize(),
                 dataType: "json",
                success: function (data) {
                    if(data.success == true) {
                        $.toaster({
                            priority : 'success',
                            title    : @json(__('frontend.success') ),
                            message  : data.message,
                            timeout  : 3000,
                        });
                        $('#cust_id').val(data.customer);
                        $('#email_send').html('We have sent an Email with verification code.\n\
                                               Type the verification code here and verify');
                        $('#a-verify').trigger('click');
                        $('#otp-1').focus();
                    } else {
                        $.toaster({
                            priority : 'danger',
                            title    : @json(__('frontend.error')),
                            message  : data.message,
                            timeout  : 3000,
                        });
                    }
                },
                complete: function() {
                    $('.ajax-loading-buz').hide();
                }
            });
              return false;
              }
});

        $("#frm_reset_password").validate({
        normalizer: function(value) {
        return $.trim(value);
        },
        rules: {
                password: {
                    required: true,
                    minlength: 6,
                },
                c_password: {
                    required: true,
                    equalTo: '#password4'
                }
            },
            messages: {
                password: {
                    required: @json(__('frontend.pass_req') ),
                },
                c_password: {
                    required:  @json(__('frontend.c_pass_req') ),
                    equalTo: @json(__('frontend.pass_c_pass_not_match') ) 
                }
            },
            submitHandler: function(form) {
            $('.ajax-loading-buz').show();
            $.ajax({
                type: "POST",
                url: "{{route('customer.reset_password')}}",
                data: $('#frm_reset_password').serialize(),
                 dataType: "json",
                success: function (data) {
                    if(data.success == true) {
                        $('#myModal').modal('hide');
                        $.toaster({
                            priority : 'success',
                            title    : @json(__('frontend.success') ),
                            message  : data.message,
                            timeout  : 3000,
                        });
                        $('.navbar-nav a.login').attr('data-target', 'dropdown-menu').attr('data-toggle', 'dropdown');
                        $('span.cust-name').text('Hello, ' + data.customer_name);
                    } else {
                        $.toaster({
                            priority : 'danger',
                            title    : @json(__('frontend.error')),
                            message  : data.message,
                            timeout  : 3000,
                        });
                    }
                },
                complete: function() {
                    $('.ajax-loading-buz').hide();
                }
            });

            return false;
            }
        });

        $("#branch-form").validate({
            normalizer: function(value) {
                return $.trim(value);
            },
            rules: {
                branch_name: {
                    required: true,
                },
                contact_name: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true,
                },
                phone_no: {
                    required: true,
                    number: true,
                    minlength: 6, // will count space
                    maxlength: 12
                },
            },
            messages: {
                branch_name: {
                    required: @json(__('frontend.branch_name_req') ),
                },
                contact_name: {
                    required: @json(__('frontend.contact_name_req') ),
                },
                email: {
                    required: @json(__('frontend.email_req') ),
                    email: @json(__('frontend.iv_vd_emil') ),
                },
                phone_no: {
                    required: @json(__('frontend.ph_req_msg') ),
                    number:  @json(__('frontend.invd_ph') )
                },
            },
            submitHandler: function(form) {
                $('.ajax-loading-buz').show();
                $.ajax({
                    type: "POST",
                    url: "{{route('branch.add')}}",
                    data: $('#branch-form').serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.success == true) {
                            $.toaster({
                                priority: 'success',
                                title: @json(__('frontend.success') ),
                                message: data.message,
                                timeout: 5000,
                            });
                            $('#branch').modal('hide');
                            setTimeout(() => {
                                location.reload();
                            }, 2000);
                        } else {
                            $.toaster({
                                priority: 'danger',
                                title: @json(__('frontend.error')),
                                message: data.message,
                                timeout: 5000,
                            });
                        }
                    },
                    complete: function() {
                        $('.ajax-loading-buz').hide();
                    }
                });
                return false;
            }
        });

        $("#frm_create_account").validate({
            normalizer: function(value) {
                return $.trim(value);
            },
            rules: {
                type: {
                    required: true,
                },
                name: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true,
                },
                password: {
                    required: true,
                    minlength: 6,
                },
                phone: {
                    required: true,
                    number: true,
                    minlength: 6, // will count space
                    maxlength: 12
                },
            },
            messages: {
                type: {
                    required: 'Type is required.',
                },
                name: {
                    required:  @json(__('frontend.name_req') ),
                },
                email: {
                    required: @json(__('frontend.email_req') ),
                    email: @json(__('frontend.iv_vd_emil') ),
                },
                password: {
                    required: @json(__('frontend.pass_req') ),
                },
                phone: {
                    required: @json(__('frontend.ph_req_msg') ),
                    number: @json(__('frontend.invd_ph') )
                },
            },
            submitHandler: function(form) {
                $('.ajax-loading-buz').show();
                $.ajax({
                    type: "POST",
                    url: "{{route('customer.create')}}",
                    data: $('#frm_create_account').serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.success == true) {
                            $.toaster({
                                priority: 'success',
                                title: @json(__('frontend.success') ),
                                message: @json(__('frontend.otp_receved_verify') ) ,
                                timeout: 5000,
                            });
                            $('#a-verify').trigger('click');
                            $('#otp-1').focus();
                        } else {
                            $.toaster({
                                priority: 'danger',
                                title: @json(__('frontend.error')),
                                message: data.message,
                                timeout: 5000,
                            });
                        }
                    },
                    complete: function() {
                        $('.ajax-loading-buz').hide();
                    }
                });
                return false;
            }
        });

        $("#frm_login").validate({
            normalizer: function(value) {
                return $.trim(value);
            },
            rules: {
                phone: {
                    required: true,
                    number: true,
                    minlength: 6, // will count space
                    maxlength: 12
                },
            },
            messages: {
                phone: {
                    required: @json(__('frontend.ph_req_msg') ),
                    number: @json(__('frontend.invd_ph') )
                },
            },
            submitHandler: function(form) {
                $('.ajax-loading-buz').show();
                $.ajax({
                    type: "POST",
                    url: "{{route('customer.phonelogin')}}",
                    data: $('#frm_login').serialize(),
                    dataType: "json",
                    success: function(data) {
                        if (data.success == true) {
                            $.toaster({
                                priority: 'success',
                                title: @json(__('frontend.success') ),
                                message: @json(__('frontend.otp_receved_verify') ),
                                timeout: 5000,
                            });
                            $('#a-verify').trigger('click');
                            $('#otp-1').focus();
                        } else {
                            $.toaster({
                                priority: 'danger',
                                title: @json(__('frontend.error')),
                                message: data.message,
                                timeout: 5000,
                            });
                        }
                    },
                    complete: function() {
                        $('.ajax-loading-buz').hide();
                    }
                });
                return false;
            }
        });

        $('#otp-1').on('input', function() {
            $('#otp-2').focus();
        });
        $('#otp-2').on('input', function() {
            $('#otp-3').focus();
        });
        $('#otp-3').on('input', function() {
            $('#otp-4').focus();
        });

        $('#verify-acc').click(function(e) {
            e.preventDefault();
            var otp1 = $('#otp-1').val();
            var otp2 = $('#otp-2').val();
            var otp3 = $('#otp-3').val();
            var otp4 = $('#otp-4').val();
            var otp = `${otp1}${otp2}${otp3}${otp4}`;
            $('.ajax-loading-buz').show();
            $.ajax({
                type: "POST",
                url: "{{route('customer.verify')}}",
                data: {
                    'otp': otp,
                },
                success: function(data) {
                    if (data.success == 1) {
                        $.toaster({
                            priority: 'success',
                            title: @json(__('frontend.success') ),
                            message: data.message,
                            timeout: 5000,
                        });
                        if ($('#cust_id').val() != '') {
                            $('#a-Reset').trigger('click');
                        } else {
                            $('#myModal').modal('hide');
                            $('.navbar-nav a.login').attr('data-target', 'dropdown-menu').attr('data-toggle', 'dropdown');
                            $('span.cust-name').text('Hello, ' + data.customer_name);
                        }
                    } else {
                        $.toaster({
                            priority: 'danger',
                            title: @json(__('frontend.error')),
                            message: data.message,
                            timeout: 5000,
                        });
                    }
                },
                complete: function() {
                    $('.ajax-loading-buz').hide();
                }
            });
        });

        // Start - Location fetch -------------------------------------------------------------------------------------
        var crrnt_location = localStorage.getItem('coordinates');
        if (crrnt_location) {
            crrnt_location = JSON.parse(crrnt_location);
        }
        var crrnt_location_name = localStorage.getItem('location_name');
        var default_location = {
            latitude: 21.3891,
            longitude: 39.8579
        };
        var area_of_op = $('#area_of_op').attr('area-of-op');
        try {
            area_of_op = JSON.parse(area_of_op);
        } catch (err) {
            area_of_op = '';
        }
        if (crrnt_location_name && crrnt_location && crrnt_location.latitude && crrnt_location.longitude) {
            default_location = crrnt_location;
            $('.location-to-deliver')
                .text(' ' + crrnt_location_name.substr(0, 20) + (crrnt_location_name.length > 20 ? '...' : ''))
                .attr('title', crrnt_location_name);
        } else {
            if(area_of_op && area_of_op.length > 0 && area_of_op[0].lat && area_of_op[0].lng) {
                try {
                    var bounds = new google.maps.LatLngBounds();
                    var i;
                    var polygonCoords = [];
                    for(var _inc=0; _inc < area_of_op.length; _inc++) {
                        polygonCoords.push( new google.maps.LatLng(area_of_op[_inc].lat, area_of_op[_inc].lng) );
                    }
                    polygonCoords.push( new google.maps.LatLng(area_of_op[0].lat, area_of_op[0].lng) );
                    for (i = 0; i < polygonCoords.length; i++) {
                        bounds.extend(polygonCoords[i]);
                    }
                    var _center = bounds.getCenter();
                    default_location = {
                        latitude: _center.lat(),
                        longitude: _center.lng()
                    };
                } catch(err) {
                    default_location = {
                        latitude: area_of_op[0].lat,
                        longitude: area_of_op[0].lng
                    };
                }
            }
        }

        var _loc_name = localStorage.getItem('temp-location_name');
        if ((_loc_name == '' || _loc_name == null) && (crrnt_location_name == '' || crrnt_location_name == null)) {
            localStorage.setItem('temp-coordinates', JSON.stringify(default_location));
            var _loc = 'Mecca, SA';
            try {
                var latlng = new google.maps.LatLng(default_location.latitude, default_location.longitude);
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({
                    'latLng': latlng
                }, function(results, status) {

                    if (status !== google.maps.GeocoderStatus.OK) {
                        _loc = 'Mecca, SA';
                    }
                    if (status == google.maps.GeocoderStatus.OK) {
                        var address = results[0].formatted_address;
                        _loc = address;
                        localStorage.setItem('temp-location_name', _loc);
                        $('.location-to-deliver-temp').text(_loc);
                    }
                });
            } catch (err) {
                _loc = 'Mecca, SA';
                localStorage.setItem('temp-location_name', _loc);
                $('.location-to-deliver-temp').text(_loc);
            }
        }

        $('#map_component').locationpicker({
            location: default_location,
            enableAutocomplete: true,
            radius: false,
            zoom: 6,
            diagonal: area_of_op,
            onchanged: function(currentLocation, radius, isMarkerDropped) {
                var addressComponents = $(this).locationpicker('map').location.formattedAddress;
                localStorage.setItem('temp-coordinates', JSON.stringify(currentLocation));
                localStorage.setItem('temp-location_name', addressComponents);
                $('.location-to-deliver-temp').text(addressComponents);
            }
        });

        function ChangeLocation() {
            var loc_coordinates = localStorage.getItem('temp-coordinates');
            var loc_name = localStorage.getItem('temp-location_name');
            if (loc_name) {
                loc_coordinates = JSON.parse(loc_coordinates);
                $('.ajax-loading-buz').show();
                $.ajax({
                    type: "POST",
                    url: "{{route('map.is_inside_reg')}}",
                    data: {
                        'lat': loc_coordinates.latitude,
                        'lng': loc_coordinates.longitude,
                    },
                    success: function(data) {
                        if (data.success == true) {
                            localStorage.setItem('coordinates', JSON.stringify(loc_coordinates));
                            localStorage.setItem('location_name', loc_name);
                            $('.location-to-deliver')
                                .text(' ' + loc_name.substr(0, 20) + (loc_name.length > 20 ? '...' : ''))
                                .attr('title', loc_name);
                            $.toaster({
                                priority: 'success',
                                title    : @json(__('frontend.success') ),
                                message:  @json(__('frontend.loc_upd_success') ),
                                timeout: 5000,
                            });
                            setTimeout(() => {
                                location.reload();
                            }, 1500)
                        } else {
                            $.toaster({
                                priority: 'danger',
                                title    : @json(__('frontend.error') ),
                                message: data.message,
                                timeout: 5000,
                            });
                        }
                    },
                    complete: function() {
                        $('.ajax-loading-buz').hide();
                    }
                });

                return true;
            }

            $.toaster({
                priority: 'danger',
                title: @json(__('frontend.error')),
                message: 'Please choose delivery location',
                timeout: 5000,
            });
        }

        $('#location_submit').click(function() {
            const region_id = $('#area_of_op').attr('region_id');
            if (!region_id) {
                ChangeLocation();
            } else {
                $.confirm({
                    title: @json(__('frontend.loc_update') ),
                    content: @json(__('frontend.loc_chng_alert') ),
                    icon: 'fa fa-check',
                    theme: 'bootstrap',
                    closeIcon: true,
                    animation: 'scale',
                    type: 'green',
                    buttons: {
                        okay: {
                            text: @json(__('frontend.ok') ),
                            btnClass: 'btn btn-success text-white',
                            action: function() {
                                ChangeLocation();
                            }
                        },
                        Cancel: {
                            text: @json(__('frontend.cancel') ),
                            btnClass: 'btn btn-primary text-white',
                        }
                    }
                });
            }
        });

        const region_id = $('#area_of_op').attr('region_id');
        if (!region_id) {
            $('#location-modal').modal('show');
        }
        // End - Location fetch -------------------------------------------------------------------------------------

        // Calling from promo, wishlist, category, details etc...
        addToCart = (element, product_id, type, addedFrom = '', add_or_subtract) => {
            var selected_variant = $(element).closest('div.product').find('select#variant-select').val();
            var selected_qty = $(element).closest('div.product').find('input.qty').val();
            if (addedFrom == 'detail') {
                selected_variant = $(element).closest('div.pdt_detail').find('select#variant-select').val();
                selected_qty = $(element).closest('div.pdt_detail').find('input.qty').val();
            }
            addProdutToCart(element, product_id, type, selected_variant, selected_qty, false, addedFrom);
        }

        // Calling from cart
        update_cart = (element, product_id, type, selected_variant, qty) => {
            var selected_qty = $(element).closest('div.quantity').find('.product-qty').val();
            if(parseInt(selected_qty) + parseInt(qty) == 0) {
                removeProduct(element, product_id, type, selected_variant);

                return;
            }
            addProdutToCart(element, product_id, type, selected_variant, qty, true, 'cart');
        }

        removeProduct = (item, product_id, type, variant_id) => {
            $('.ajax-loading-buz').show();
            $.ajax({
                type: "POST",
                url: "{{ route('product.remove') }}",
                data: {
                    product_id: product_id,
                    type: type,
                    variant_id: variant_id
                },
                success: function(data) {
                    if (data.success == true) {
                        $(item).closest('tr').remove();
                        $.toaster({
                            priority: 'success',
                            title    : @json(__('frontend.success') ),
                            message: data.message,
                            timeout: 3000,
                        });
                        $('.cart-count').text(data.count);
                        $('.total-amount').html('<br>SAR ' + parseFloat(data.grant_total).toFixed(2));
                        refreshCart();
                    } else {
                        $.toaster({
                            priority: 'danger',
                            title    : @json(__('frontend.error') ),
                            message: data.message,
                            timeout: 3000,
                        });
                    }
                },
                complete: function() {
                    $('.ajax-loading-buz').hide();
                }
            });
        }

        refreshCart = () => {
            $('.ajax-loading-buz').show();

            $.ajax({
                url: "{{route('cart.refresh')}}",
                success: function(data) {
                    $('.page-cart').html(data);
                },
                complete: function() {
                    $('.ajax-loading-buz').hide();
                }
            });
        }

        addProdutToCart = (element, product_id, type, selected_variant, qty, is_cart_update, addedFrom) => {
            $('.ajax-loading-buz').show();

            $.ajax({
                type: "POST",
                url: "{{route('cart.add')}}",
                data: {
                    'product_id': product_id,
                    'type': type,
                    'variant_id': selected_variant,
                    'qty': qty,
                    'addedFrom': addedFrom,
                    'is_savelater': addedFrom == 'savelater'
                },
                success: function(data) {
                    if (data.success == true) {
                        $('.cart-count').text(data.count);
                        $('.total-amount').html('<br>SAR ' + parseFloat(data.grant_total).toFixed(2));
                        if (addedFrom == 'cart') {
                            $.toaster({
                                priority: 'success',
                                title: @json(__('frontend.success') ),
                                message: data.message,
                                timeout: 3000,
                            });
                            refreshCart();
                        } else {
                            $.confirm({
                                title: @json(__('frontend.success') ),
                                content: data.message,
                                icon: 'fa fa-check',
                                theme: 'bootstrap',
                                closeIcon: true,
                                animation: 'scale',
                                type: 'green',
                                buttons: {
                                    okay: {
                                        text: @json(__('frontend.view_cart') ),
                                        btnClass: 'btn btn-success text-white',
                                        action: function() {
                                            window.location.href = '{{ route('cart.show') }}';
                                        }
                                    }
                                }
                            });
                        }
                        if (is_cart_update) {
                            $(element).closest('div.quantity').find('.product-qty').val(data.product_count);
                            $(element).closest('td.product-details').find('div.price .product-qty').text(data.product_count);
                        }
                        $('#savelater-count').text(`Showing ${data.savelater_count} items of ${data.savelater_count}`);
                        $('.total').text(data.count).show();
                        if (addedFrom == 'savelater') {
                            $(element).closest('li').remove();
                        }
                    } else {
                        $.toaster({
                            priority: 'danger',
                            title: @json(__('frontend.error')),
                            message: data.message,
                            timeout: 3000,
                        });
                    }
                },
                complete: function() {
                    $('.ajax-loading-buz').hide();
                }
            });
        }

    });

    function changeDisplyaPrice(item, from = '') {
        const price = $(item).find("option:selected").attr('price');
        const d_price = $(item).find("option:selected").attr('d_price');
        const min_stock = $(item).find("option:selected").attr('min_stock');
        const max_stock = $(item).find("option:selected").attr('max_stock');
        if (from == 'details') {
            if (d_price) {
                $(item).closest('div.pdt_detail').find('#actual-price').text('SAR ' + price);
                $(item).closest('div.pdt_detail').find('#discount-price').text('SAR ' + d_price);
            } else {
                $(item).closest('div.pdt_detail').find('#actual-price').text('');
                $(item).closest('div.pdt_detail').find('#discount-price').text('SAR ' + price);
            }
            if(min_stock && max_stock){
                $(item).closest('div.pdt_detail').find('#pdt_qty').val(min_stock);
                $(item).closest('div.pdt_detail').find('#min_amount').attr('min_stock', min_stock);
                $(item).closest('div.pdt_detail').find('#max_amount').attr('max_stock', max_stock);
            }
        } else {
            if (d_price) {
                $(item).closest('div.product').find('#actual-price').text('SAR ' + price);
                $(item).closest('div.product').find('#discount-price').text('SAR ' + d_price);
            } else {
                $(item).closest('div.product').find('#actual-price').text('');
                $(item).closest('div.product').find('#discount-price').text('SAR ' + price);
            }
            if(min_stock && max_stock){
                $(item).closest('div.product').find('#pdt_qty').val(min_stock);
                $(item).closest('div.product').find('#min_amount').attr('min_stock', min_stock);
                $(item).closest('div.product').find('#max_amount').attr('max_stock', max_stock);
            }
        }
    }

    $("#frm-add-address").validate({
        normalizer: function(value) {
            return $.trim(value);
        },
        rules: {
            name: {
                required: true,
            },
            apartment: {
                required: true,
            },
            street: {
                required: true,
            },
            po_box: {
                required: true,
            },
            landmark: {
                required: true,
            },
            region: {
                required: true,
            },
            phone: {
                required: true,
                number: true,
                minlength: 6, // will count space
                maxlength: 12
            },
        },
        messages: {
            name: {
                required: @json(__('frontend.name_req') ),
            },
            apartment: {
                required:  @json(__('frontend.ap_na_req') ),
            },
            street: {
                required: @json(__('frontend.str_na_req') ),
            },
            po_box: {
                required: @json(__('frontend.po_req') ),
            },
            landmark: {
                required: @json(__('frontend.landmark_req') ),
            },
            region: {
                required: @json(__('frontend.region_req') ),
            },
            phone: {
                required: @json(__('frontend.ph_req_msg') ),
                number: @json(__('frontend.invd_ph') )
            },
        },
        submitHandler: function(form) {
            $('.ajax-loading-buz').show();
            $.ajax({
                type: "POST",
                url: "{{route('customer.add_address')}}",
                data: $('#frm-add-address').serialize(),
                dataType: "json",
                success: function(data) {
                    if (data.success == true) {
                        $("#frm-add-address")[0].reset();
                        $.toaster({
                            priority: 'success',
                            title: @json(__('frontend.success') ),
                            message: data.message,
                            timeout: 3000,
                        });
                        $('#ADDRESS').modal('hide');
                        window.location.reload();
                    } else {
                        $.toaster({
                            priority: 'danger',
                            title: @json(__('frontend.error')),
                            message: data.message,
                            timeout: 3000,
                        });
                    }
                },
                complete: function() {
                    $('.ajax-loading-buz').hide();
                }
            });
            return false;
        }
    });

    getAddresses = (is_address_page) => {
        $('.ajax-loading-buz').show();
        var is_address_page = "{{ $is_address_page ?? '' }}";
        $.ajax({
            url: "{{ route('customer.list_address') }}",
            type: 'POST',
            success: function(data) {
                if (data.success == true) {
                    var name = '';
                    var list = {};
                    var string = '';
                    var default_id = '';
                    var scheduled = '';
                    var is_default = '';
                    var def_icon = '';
                    for (var inc = 0; inc < data.addresses.length; inc++) {
                        list = data.addresses[inc];
                        try {
                            name = list.region.lang[0].name;
                        } catch (err) {}
                        def_icon = default_btn = make_billing = default_del_btn = '';
                        if (list.is_default == 'Y') {
                            default_id = list.id;
                            def_icon = '<span class="badge-secondary small text-left ml-3"> @json(__('frontend.default_address1') )</span>';
                        }
                        if (list.is_default == 'N') {
                            default_btn = ``;
                            default_del_btn = `<button onclick="deleteAddress(this, ${list.id}, 'D')" class="btn btn-sm btn-secondary text-white py-0 ml-3">Delete</button>`;
                        }
                        if (data.all_billing_addresses.length == 0) {
                            make_billing = `<button onclick="addasBilling(this, ${list.id}, 'D')" class="btn btn-sm btn-info text-white py-0 ml-3"> Make this as billing</button>`;
                        }

                        string += `<label class="container5 ">
                                    <p> ${list.name} ${def_icon}
                                    ${default_btn}
                                    ${default_del_btn}
                                    <button onclick="chhose_address(this, 'D')" address_id="${list.id}" class="select-addr btn btn-sm btn-secondary text-white ml-3 py-0">Select delivery address</button>
                                    ${make_billing}
                                    </p>
                                    <p> ${list.apartment_name}</p>
                                    <p> ${list.street_name}</p>
                                    <p> ${list.landmark}</p>
                                    <p> PO Box: ${list.postal_code}</p>
                                    <p> ${name}</p>
                                    <p> ${list.phone} </p>
                                    ${scheduled}
                                </label>
                                <hr>`;
                    }
                    var string_buz = '';

                    var bill = {};
                    var def_b_id = '';
                    var def_icon = '';
                    for (var incr = 0; incr < data.all_billing_addresses.length; incr++) {
                        bill = data.all_billing_addresses[incr];
                        var b_name = '';
                        try {
                            b_name = bill.region.lang[0].name;
                        } catch (err) {}
                        def_icon = default_btn = default_del_btn = '';
                        if (bill.is_default == 'Y') {
                            default_id = bill.id;
                            def_icon = '<span class="badge-secondary small text-left ml-3"> @json(__('frontend.default_address1') )</span>';
                        }
                        if (bill.is_default == 'N') {
                            default_btn = ``;
                            default_del_btn = `<button onclick="deleteAddress(this, ${bill.id}, 'B')" class="btn btn-sm btn-secondary text-white py-0 ml-3">Delete</button>`;
                        }
                        string_buz += `<label class="container5 ">
                                <p> ${bill.name} ${def_icon}
                                ${default_btn}
                                ${default_del_btn}
                                <button onclick="chhose_address(this, 'B')" address_id="${bill.id}"class="select-addr btn btn-sm btn-secondary text-white py-0 ml-3">Select billing address</button>
                                </p>
                                <p> ${bill.apartment_name}</p>
                                <p> ${bill.street_name}</p>
                                <p> ${bill.landmark}</p>
                                <p> PO Box: ${bill.postal_code}</p>
                                <p> ${name}</p>
                                <p> ${bill.phone} </p>
                                <hr>
                            </label>`;
                    }
                    $('.billingaddress').empty().append(string_buz);
                    string_buz = '';
                    $('.addresses-list').empty().append(string + string_buz);
                    if (is_address_page == '') {
                        $('.select-addr').remove();
                    }
                } else {
                    $.toaster({
                        priority: 'danger',
                        title    : @json(__('frontend.error') ),
                        message: data.message,
                        timeout: 3000,
                    });
                }
            },
            complete: function() {
                $('.ajax-loading-buz').hide();
            }
        });
    }

    chhose_address = (item, type) => {
            var address_id = $(item).attr('address_id');
            var qtn_id = "{{ $q_id ?? '' }}";
            $('.ajax-loading-buz').show();
            $.ajax({
                type: "POST",
                url: "{{ route('billing.set_address') }}",
                data: {
                    type: type,
                    id: address_id
                },
                success: function (data) {
                    if (data.success == true) {
                        $.toaster({
                            priority : 'success',
                            title    : @json(__('frontend.success') ),
                            message  : data.message,
                            timeout  : 3000,
                        });
                        location.href = "{{ route('cart.checkout') }}";
                    } else {
                        $.toaster({
                            priority : 'danger',
                            title    : @json(__('frontend.error')),
                            message  : data.message,
                            timeout  : 3000,
                        });
                    }
                },
                complete: function() {
                    $('.ajax-loading-buz').hide();
                }
            });
        }

    $('.place-order').click(function() {
        var loc = localStorage.getItem('location_name');
        if( ! loc ) {
            $.toaster({
                priority : 'danger',
                title    : @json(__('frontend.error') ),
                message  : 'Please select delivery location',
                timeout  : 3000,
            });
            $('#location-modal').modal('show');

            return false;
        }

        $('.ajax-loading-buz').show();
        var formData = new FormData($('#frm_payment_slip')[0]);
        formData.append('location', localStorage.getItem('location_name'));
        formData.append('coordinates', localStorage.getItem('coordinates'));
        $.ajax({
            type: "POST",
            url: "{{route('cart.place_order')}}",
             data: formData,
            dataType: 'json',
            async: false,
            type: 'post',
            processData: false,
            contentType: false,
            success: function (data) {
                if(data.success == true) {
                    $.toaster({
                        priority : 'success',
                        title    : @json(__('frontend.success') ),
                        message  : data.message,
                        timeout  : 3000,
                    });
                    setTimeout(() => {
                        location.href = data.route;
                    }, 1500);
                } else {
                    if(typeof data.code != 'undefined' && data.code == 'no.login') {
                        $('#myModal').modal('show');
                    }
                    if(typeof data.code != 'undefined' && data.code == 'out_of_stock') {
                        location.href = "{{ route('cart.show') }}";
                    }
                    $.toaster({
                        priority : 'danger',
                        title    : @json(__('frontend.error')),
                        message  : data.message,
                        timeout  : 3000,
                    });
                }
            },
            complete: function() {
                $('.ajax-loading-buz').hide();
            }
        });
    });


    addToFavourites = (item, product_id, from='') => {
            var isInFav = $(item).hasClass('active');
            if(isInFav) {
                $(item).removeClass('active');
            } else {
                $(item).addClass('active');
            }
            $.ajax({
                type: "POST",
                url: "{{route('product.favourite')}}",
                data: {
                    'product_id': product_id
                },
                success: function (data) {
                    if(data.success == true) {
                        $.toaster({
                            priority : 'success',
                            title    : @json(__('frontend.success') ),
                            message  : data.message,
                            timeout  : 3000,
                        });
                        if(from == 'wishlist') {
                            $(item).closest('div.product').remove();
                            window.location.reload();
                        }
                        if(from == 'wishlistmodal') {
                            $(item).closest('div.product').remove();
                        }
                        if(from == 'details') {
                            window.location.reload();
                        }
                    } else {
                        if(typeof data.code != 'undefined' && data.code == 'no.login') {
                            $('#myModal').modal('show');
                        }
                        // On fail revert.
                        if(isInFav) {
                            $(item).addClass('active');
                        } else {
                            $(item).removeClass('active');
                        }
                        $.toaster({
                            priority : 'danger',
                            title    : @json(__('frontend.error')),
                            message  : data.message,
                            timeout  : 3000,
                        });
                    }
                },
                complete: function() {
                    $('.ajax-loading-buz').hide();
                }
            });
        }

        function updateCount(item) {
            min_stock = $(item).attr('min_stock');
            max_stock = $(item).attr('max_stock');
            const current_qty = $(item).closest('div.quantity').find('.qty').val();
            if(min_stock){
            var qty = -1;
            }
            if(max_stock){
              var qty = 1;  
            }
             if(parseInt(max_stock) == 0) {
                  $.toaster({
                            priority: 'danger',
                            title: @json(__('frontend.error')),
                            message: 'Out of stock',
                            timeout: 3000,
                        });
                return false;  
            }
            if(parseInt(current_qty) <= parseInt(min_stock)){
                $.toaster({
                            priority: 'danger',
                            title: @json(__('frontend.error')),
                            message: @json(__('frontend.min_stock_validation'))+' '+ min_stock,
                            timeout: 3000,
                        });
                return false;
            }
            if(parseInt(current_qty) >= parseInt(max_stock)) {
                $.toaster({
                            priority: 'danger',
                            title: @json(__('frontend.error')),
                            message: @json(__('frontend.max_stock_validation'))+' '+ max_stock,
                            timeout: 3000,
                        });
                return false;
            }
            const updatedQty = parseInt(current_qty) + parseInt(qty);
            $(item).closest('div.quantity').find('.qty').val(updatedQty);
        }
        
        deleteAddress = (item, id, flag) => {
            $('.ajax-loading-buz').show();
            $.ajax({
                type: "POST",
                url: "{{ route('customer.delete_address') }}",
                data: {
                    type: flag,
                    id: id
                },
                success: function (data) {
                    if (data.status == 1) {
                        $.toaster({
                            priority : 'success',
                            title    : @json(__('frontend.success') ),
                            message  : data.message,
                            timeout  : 3000,
                        });
                        window.location.reload();
                    } else {
                        $.toaster({
                            priority : 'danger',
                            title    : @json(__('frontend.error')),
                            message  : data.message,
                            timeout  : 3000,
                        });
                    }
                },
                complete: function() {
                    $('.ajax-loading-buz').hide();
                }
            });
        }
        addasBilling = (item, id, flag, customer_id) => {
            $('.ajax-loading-buz').show();
            $.ajax({
                type: "POST",
                url: "{{ route('customer.make_billing') }}",
                data: {
                    type: flag,
                    id: id,
                    customer_id : customer_id
                },
                success: function (data) {
                    if (data.status == 1) {
                        $.toaster({
                            priority : 'success',
                            title    : @json(__('frontend.success') ),
                            message  : data.message,
                            timeout  : 3000,
                        });
                        window.location.reload();
                    } else {
                        $.toaster({
                            priority : 'danger',
                            title    : @json(__('frontend.error')),
                            message  : data.message,
                            timeout  : 3000,
                        });
                    }
                },
                complete: function() {
                    $('.ajax-loading-buz').hide();
                }
            });
        }
        changeDefaultAddress = (item, id, flag, customer_id) => {
            var default_id = '';
            if(flag == 'D') {
                default_id = $('#default-address-id').val();
            } else {
                default_id = $('#default-billing-address-id').val();
            }
            if(id == default_id) {
                return false;
            }
            $.confirm({
                closeIcon: true,
                type: 'orange',
                title: @json(__('frontend.confirm') ),
                content: @json(__('frontend.set_def_addr') ),
                buttons: {
                    ok: {
                        text: @json(__('frontend.yes') ),
                        btnClass: 'btn btn-success',
                        action: function(){
                            changeAddress(id, customer_id);
                        }
                    },
                    @json(__('frontend.no') ): function() {
                    console.log('cancelled');
                    }
                }
            });
        }
        changeAddress = (id, customer_id) => {
            $('.ajax-loading-buz').show();
            $.ajax({
                url: "{{ route('customer.change_address') }}",
                type: 'POST',
                data: {
                    id: id,
                    customer_id: customer_id
                },
                success: function (data) {
                    if(data.success == true) {
                        $.toaster({
                            priority : 'success',
                            title    : @json(__('frontend.success') ),
                            message  : data.message,
                            timeout  : 3000,
                        });
                        window.location.reload();
                    } else {
                        $.toaster({
                            priority : 'danger',
                            title    : @json(__('frontend.error') ),
                            message  : data.message,
                            timeout  : 3000,
                        });
                    }
                },
                complete: function() {
                    $('.ajax-loading-buz').hide();
                }
            });
        }
        
         getFromWishlist = () => {
            $('.ajax-loading-buz').show();
            $.ajax({
                url: "{{ route('customer.wishlist_items') }}",
                success: function (data) {
                    if(typeof data.success == 'undefined') {
                        $('.modal-content').html(data);
                        $('#CartModal').modal('show');
                    } else {
                        $.toaster({
                            priority : 'danger',
                            title    : @json(__('frontend.error') ),
                            message  : data.message,
                            timeout  : 3000,
                        });
                    }
                },
                complete: function() {
                    $('.ajax-loading-buz').hide();
                }
            });
        }
        saveLater = (element, product_id, type, variant_id) => {
            $('.ajax-loading-buz').show();
            $.ajax({
                type: "POST",
                url: "{{ route('customer.savelater') }}",
                data: {
                    product_id: product_id,
                    type: type,
                    variant_id: variant_id
                },
                success: function (data) {
                    if(data.success == true) {
                        $.toaster({
                            priority : 'success',
                            title    : @json(__('frontend.success') ),
                            message  : data.message,
                            timeout  : 3000,
                        });
                        window.location.reload();
                    } else {
                        $.toaster({
                            priority : 'danger',
                            title    : @json(__('frontend.error')),
                            message  : data.message,
                            timeout  : 3000,
                        });
                    }
                },
                complete: function() {
                    $('.ajax-loading-buz').hide();
                }
            });
        }
        getSaveLater = () => {
            $('.ajax-loading-buz').show();
            $.ajax({
                url: "{{ route('customer.savelater_items') }}",
                success: function (data) {
                    if(typeof data.success == 'undefined') {
                        $('.modal-content').html(data);
                        $('#CartModal').modal('show');
                    } else {
                        $.toaster({
                            priority : 'danger',
                            title    : @json(__('frontend.error')),
                            message  : data.message,
                            timeout  : 3000,
                        });
                    }
                },
                complete: function() {
                    $('.ajax-loading-buz').hide();
                }
            });
        }
    $(function(){
    $("#changedate").css('color', 'white');
    $('#changedate').on('click', function(e){
    $("#datebox").show();
    var color = $("#datebox").is(':hidden') ? '#FF0' : 'white';
    $("#changedate").css('color', color);
    });
    $("#hidedate").click(function(){
    $("#datebox").hide();
    });
    $(".hidicon").click(function(){
    $(".hidicon").hide();
    });
    $("#hidedate").click(function(){
    $(".hidicon").show();
    });
    });
    
     $('.date_change').on('click', function(e){
         $('#delivery_date').val($(this).attr('del_date'));
         $('#order_del_date').text($(this).attr('converted_date')+', ');
    });
    
     $('.time-slot-item').on('click', function(e){
         $('#time_slot').val($(this).attr('time_slot'));
         $(this).siblings('li').removeClass('active');
         $(this).addClass('active');
         $('#del_time_slots').text($(this).attr('time_slot'));
    });
    AddressFilter = (cust_id) =>{
            $('.ajax-loading-buz').show();
            if(cust_id){
            var url = `{{ route('customer.adddresses') }}/${cust_id}`;
        }else{
            var url = `{{ route('customer.adddresses') }}`;
        }
        window.location.href = url;
    }
    deleteBranch = (item, id) => {
            $('.ajax-loading-buz').show();
            $.ajax({
                type: "POST",
                url: "{{ route('frontend.branch.delete') }}",
                data: {
                    id: id
                },
                success: function (data) {
                    if (data.status == 1) {
                        $.toaster({
                            priority : 'success',
                            title    : @json(__('frontend.success') ),
                            message  : data.message,
                            timeout  : 3000,
                        });
                        window.location.reload();
                    } else {
                        $.toaster({
                            priority : 'danger',
                            title    : @json(__('frontend.error')),
                            message  : data.message,
                            timeout  : 3000,
                        });
                    }
                },
                complete: function() {
                    $('.ajax-loading-buz').hide();
                }
            });
        }
        
        BranchLoginPermission = (item, id) => {
            $('.ajax-loading-buz').show();
            $.ajax({
                type: "POST",
                url: "{{ route('frontend.branch.login') }}",
                data: {
                    id: id
                },
                success: function (data) {
                    if (data.status == 1) {
                        $.toaster({
                            priority : 'success',
                            title    : @json(__('frontend.success') ),
                            message  : data.message,
                            timeout  : 3000,
                        });
                    } else {
                        $.toaster({
                            priority : 'danger',
                            title    : @json(__('frontend.error')),
                            message  : data.message,
                            timeout  : 3000,
                        });
                    }
                },
                complete: function() {
                    $('.ajax-loading-buz').hide();
                }
            });
        }

        $(document).on('click', '#support_request_common', function () {
            $("#frm_support_request_common").validate({
                normalizer: function (value) {
                    return $.trim(value);
                },
                rules: {
                    subject: {
                        required: true,
                    },
                    comment: {
                        required: true,
                    },
                },
               messages: {
                    subject: {
                        required: @json(__('frontend.sub_req') ),
                    },
                    comment: {
                        required: @json(__('frontend.com_req') ),
                    },
                },
                submitHandler: function (form) {
                    $('.ajax-loading-buz').show();
                    $.ajax({
                        type: "POST",
                        url: "{{route('order.support_request')}}",
                        data: $('#frm_support_request_common').serialize(),
                        dataType: "json",
                        success: function (data) {
                            if (data.status == 1) {
                                $("#frm_support_request_common")[0].reset();
                                $.toaster({
                                    priority: 'success',
                                    title: @json(__('frontend.success') ),
                                    message: data.message,
                                    timeout: 3000,
                                });
                                $('#SUPPORT').modal('hide');
                            } else {
                                $.toaster({
                                    priority: 'danger',
                                    title: @json(__('frontend.error')),
                                    message: data.message,
                                    timeout: 3000,
                                });
                            }
                        },
                        complete: function () {
                            $('.ajax-loading-buz').hide();
                            window.location.reload();
                        }
                    });
                    return false;
                }
            });
        });
        
        
        $('#payment_slip').on('change', function () {
       var focusSet = false;
        var reader = new FileReader();
        var fileUpload = document.getElementById("payment_slip");
        if (fileUpload != '') {
        $("#payment_block").next(".validation").remove();
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.jpeg|.pdf)$");
        if (regex.test(fileUpload.value.toLowerCase()) == '') {
        $("#payment_block").next(".validation").remove(); // remove it
        $("#payment_slip").val("");
        $('#preview_img').attr('src', '');
        $("#display_image").text('');
        $("#payment_block").after("<div class='validation mt-3' style='color:red;font-size: 12px;  font-weight: 400;'>Upload image must be the format of jpeg, png or pdf </div>");
        }
        else if (fileUpload != ''){
           $("#payment_block").next(".validation").remove(); // remove it
        
            var fileUpload = document.getElementById("payment_slip");
            var imgPath = fileUpload.value;
            var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
            var reader = new FileReader();
            //Read the contents of Image File.
            reader.readAsDataURL(fileUpload.files[0]);
            reader.onload = function (e) {
            //Initiate the JavaScript Image object.
            var image = new Image();
            //Set the Base64 string return from FileReader as source.
            image.src = e.target.result;
            //Validate the File Height and Width.
            image.onload = function () {
            var height = this.height;
            var width = this.width;
            $("#payment_block").next(".validation").remove(); // remove it
            var reader = new FileReader();
            $("#display_image").text('');
            document.getElementById("preview_img").src = e.target.result;
            reader.readAsDataURL(fileUpload.files[0]);
            }
            if(extn == "pdf") {
                $('#preview_img').attr('src', '');
                $("#display_image").text(fileUpload.files[0]['name']);
            }
            $.toaster({
                priority : 'success',
                title    : @json(__('frontend.success') ),
                message  : "File uploaded successfully",
                timeout  : 3000,
    });
            }

            }
        }
    });
    
    CouponApply = (item) => {
       var coupon_code = $('#coupon_code').val();
       if(! coupon_code){
           $.toaster({
            priority: 'danger',
            title: @json(__('frontend.error')),
            message: 'Please enter coupon code',
            timeout: 3000,
    });
    return false;
       }
       $('.ajax-loading-buz').show();
        $.ajax({
            type: "GET",
            url: "{{ route('cart.coupon_code') }}",
            data: {
                coupon_code: coupon_code,
            },
            success: function (data) {
                if (data.success == 1) {
                   $.toaster({
                                priority: 'success',
                                title: @json(__('frontend.success') ),
                                message: data.message,
                                timeout: 3000,
                            });
                 refreshCart();
                } else {
                    if(typeof data.code != 'undefined' && data.code == 'no.login') {
                            $('#myModal').modal('show');
                    }
                    $.toaster({
                        priority : 'danger',
                        title    : @json(__('frontend.error')),
                        message  : data.message,
                        timeout  : 3000,
                    });
                   refreshCart();
                }
            },
            complete: function() {
                $('.ajax-loading-buz').hide();
            }
        });
    }

    $('#autosuggest').on('keyup', function () {
        var value = $(this).val();
        if(value.length >= 1){
            $.ajax( {
                url: "{{route('home.search')}}",
                type: 'GET',
                //dataType: "jsonp",
                data: { term: value },
                contentType: "application/json; charset=utf-8",
                success: function( data ) {
                   $('#result').html('');
                   $('#result').append(data);
                }
            }); 
        }else{
            $('#result').html('');
        }
       

    });

    function OnClear () {
        $('#result').html('');     
    }

    
        
</script>

@endpush