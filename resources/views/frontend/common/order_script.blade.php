@push('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.js"></script>
<script>
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    function getOrderDetails(order_id) {

        $('.ajax-loading-buz').show();
            $.ajax({
                type: "GET",
                url: `{{ url('order-details') }}/${order_id}`,
                success: function (data)
                {
                    $('#ord_div').html(data);
                    $('.ajax-loading-buz').hide();
                    $('#filter_branch').hide();
                }
            });
    }
    $(document).on('click', '#send_contact_support', function () {
            console.log("testt");
            $(".contact_support_form").validate({
                normalizer: function (value) {
                    return $.trim(value);
                },
                rules: {
                    subject: {
                        required: true,
                    },
                    comment: {
                        required: true,
                    },
                },
                messages: {
                    subject: {
                        required: @json(__('frontend.sub_req') ),
                    },
                    comment: {
                        required: @json(__('frontend.com_req') ),
                    },
                },
                submitHandler: function (form) {
                    $('.ajax-loading-buz').show();
                    $.ajax({
                        type: "POST",
                        url: "{{route('order.support_request')}}",
                        data: $('#contact_support_form').serialize(),
                        dataType: "json",
                        success: function (data) {
                            if (data.status == 1) {
                                $.toaster({
                                    priority: 'success',
                                    title: @json(__('frontend.success') ),
                                    message: data.message,
                                    timeout: 3000,
                                });
                                $('#CONTACT').modal('hide');
                                $(".contact_support_form")[0].reset();
                            } else {
                                $.toaster({
                                    priority: 'danger',
                                    title: @json(__('frontend.error') ),
                                    message: data.message,
                                    timeout: 3000,
                                });
                            }
                        },
                        complete: function () {
                            $('.ajax-loading-buz').hide();
                        }
                    });
                    return false;
                }
            });
        });
        $(document).on('click', '#order_cancel_add', function () {
            // console.log("click");
            $("#frm_cancel_reason").validate({
                normalizer: function (value) {
                    return $.trim(value);
                },
                rules: {
                    cancel_reason_id: {
                        required: true,
                    },
                },
                messages: {
                    cancel_reason_id: {
                        required: @json(__('frontend.cancel_rsn_req') ),
                    },
                },
                submitHandler: function (form) {
                    $('.ajax-loading-buz').show();
                    $.ajax({
                        type: "POST",
                        url: "{{route('order.cancel_order')}}",
                        data: $('#frm_cancel_reason').serialize(),
                        dataType: "json",
                        success: function (data) {
                            if (data.status == 1) {
                                $.toaster({
                                    priority: 'success',
                                    title: @json(__('frontend.success') ),
                                    message: data.message,
                                    timeout: 3000,
                                });
                                $('#CANCEL_ORDER').modal('hide');
                                window.location.reload();
                            } else {
                                $.toaster({
                                    priority: 'danger',
                                    title: @json(__('frontend.error') ),
                                    message: data.message,
                                    timeout: 3000,
                                });
                            }
                        },
                        complete: function () {
                            $('.ajax-loading-buz').hide();
                        }
                    });
                    return false;
                }
            });


        });
        reOrder = (item) => {
            var ord_id = $(item).attr('ord_id');
            $('.ajax-loading-buz').show();
            $.ajax({
                type: "GET",
                url: "{{ route('cart.item_check') }}",
                success: function (data) {
                    if (data.success == true) {
                        modifyOrder(ord_id);
                    } else {
                        $.confirm({
                            theme: 'supervan',
                            closeIcon: true,
                            animation: 'scale',
                            type: 'orange',
                            title: @json(__('frontend.confirm') ),
                            content:  @json(__('frontend.reorder_confirm') ),
                            buttons: {
                                ok: {
                                    text: @json(__('frontend.yes') ),
                                    btnClass: 'btn btn-success',
                                    action: function(){
                                        modifyOrder(ord_id);
                                    }
                                },
                                cancel: {
                                    text: @json(__('frontend.no') ),
                                    btnClass: 'btn-warning'
                                },
                            }
                        });
                    }
                },
                complete: function() {
                    $('.ajax-loading-buz').hide();
                }
            });
        }
        modifyOrder = (ord_id) => {
            location.href = "{{ route('order.modify') }}/" + ord_id;
        };
        branchFilter = (cust_id) =>{
            $('.ajax-loading-buz').show();
            $.ajax({
                type: "GET",
                url: `{{ url('order/filter') }}/${cust_id}`,
                success: function (data)
                {
                    $('#ord_div').html(data);
                    $('.ajax-loading-buz').hide();
                }
                    
        });
    }

reviewProduct = (product) =>
{
    // console.log(product);
    $('#product_id').val(product);
    $('#REVIEW_PRODUCT').modal('show');
}
$(document).on('click', '#save_product_rating', function () {
$("#frm_product_rating").validate({
                ignore: [],
                normalizer: function (value) {
                    return $.trim(value);
                },
                rules: {
                    rating_input: {
                        required: true,
                        notEqual: '0',
                    },
                },
                messages: {
                    rating_input: {
                        required: 'Rating is required.',
                        notEqual: 'Rating is required.',
                    },
                },
                submitHandler: function (form) {
                    $('.ajax-loading-buz').show();
                    $.ajax({
                        type: "POST",
                        url: "{{route('order.product_rating')}}",
                        data: $('#frm_product_rating').serialize(),
                        dataType: "json",
                        success: function (data) {
                            if (data.status == 1) {
                                $("#frm_product_rating")[0].reset();
                                $.toaster({
                                    priority: 'success',
                                    title: @json(__('frontend.success') ),
                                    message: data.message,
                                    timeout: 3000,
                                });
                                $('#REVIEW_PRODUCT').modal('hide');
                            } else {
                                $.toaster({
                                    priority: 'danger',
                                    title: @json(__('frontend.error') ),
                                    message: data.message,
                                    timeout: 3000,
                                });
                            }
                        },
                        complete: function () {
                            $('.ajax-loading-buz').hide();
                        }
                    });
                    return false;
                }
            });
 });
 $.validator.addMethod("notEqual", function (value, element, param) { // Adding rules for Amount(Not equal to zero)
    return this.optional(element) || value != '0';
});
$(document).on('click','#order_form_submit',function(){
    console.log(itemArray);
    $('#order_rating_array').val(JSON.stringify(itemArray));
            $("#frm_order_rating").validate({
                submitHandler: function (form) {
                    $('.ajax-loading-buz').show();
                    $.ajax({
                        type: "POST",
                        url: "{{route('order.order_rating')}}",
                        data: $('#frm_order_rating').serialize(),
                        dataType: "json",
                        success: function (data) {
                            if (data.status == 1) {
                                $("#frm_order_rating")[0].reset();
                                $.toaster({
                                    priority: 'success',
                                    title: @json(__('frontend.success') ),
                                    message: data.message,
                                    timeout: 3000,
                                });
                                $('#REVIEW').modal('hide');
                            } else {
                                $.toaster({
                                    priority: 'danger',
                                    title: @json(__('frontend.error') ),
                                    message: data.message,
                                    timeout: 3000,
                                });
                            }
                        },
                        complete: function () {
                            $('.ajax-loading-buz').hide();
                        }
                    });
                    return false;
                }
            });

})
</script>

@endpush