@extends('frontend.frontend_layouts.frontend_app')
@section('content')
<div id="content">
    <div id="content" class="bg-light page-cart">
        <div class="container">

             <div id="breadcrumbs">
            <p><a href="{{ route('frontend') }}">{{__('frontend.home')}} </a>
                <i class="fa fa-long-arrow-right" aria-hidden="true"></i> <span> {{ $pages->lang[0]->title ?? '' }}</span>
            </p>
            <h4 class=" pt-2 mt-3">{{ $pages->lang[0]->title ?? '' }}</h4>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <p>{!! $pages->lang[0]->content ?? '' !!}</p>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
@include('frontend.common.common_script')