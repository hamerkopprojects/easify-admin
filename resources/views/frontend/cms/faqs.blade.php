@extends('frontend.frontend_layouts.frontend_app')
@section('content')
<div id="content" class="bg-light ">
    <div class="container">
         <div id="breadcrumbs">
            <p><a href="{{ route('frontend') }}">{{__('frontend.home')}} </a>
                <i class="fa fa-long-arrow-right" aria-hidden="true"></i> <span> Faqs</span>
            </p>
            <h4 class=" pt-2 mt-3">FAQ'S</h4>
            </div>

            <div class="row mt-3">
                <div class="col-lg-12">
                    <div class="bs-example">
                        <div class="accordion" id="accordionExample">
                            @foreach($faqs as $faq_val)
                            <div class="card mb-3">
                                <div class="card-header" id="headingOne">
                                    <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapse{{$faq_val->id}}"><i class="fa fa-plus"></i>  {{ $faq_val->lang[0]->question ?? ''}}</button>
                                </div>
                                <div id="collapse{{$faq_val->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p>{!! $faq_val->lang[0]->answer ?? '' !!}</p>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    </body>
                </div>

            </div>
        </div>
    </div>
@endsection
@include('frontend.common.common_script')