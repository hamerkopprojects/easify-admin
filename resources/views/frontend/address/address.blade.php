@extends('frontend.frontend_layouts.frontend_app')
@section('content')
<div class="container">
    <div class="modal " id="ADDRESS" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content  pb-4 pd20">
                <div class=" col-md-12  col-sm-12">
                    <a href="" data-dismiss="modal" class="close pt-2"><img src="{{ asset('assets/frontend/img/cancel.png') }}" class="imgclose"></a>
                    </button>
                    <div class="form-container">
                        <h3 class="title pt-2" id="address_title">{{ __('frontend.add_nw_dly_adr') }}</h3>
                        <div class="tab-content">
                            <form class="form-horizontal" id="frm-add-address">

                                <input name="address_flag" value="D" type="hidden" id="address_flag">

                                <div class="row mt-3 mb-3">
                                    <div class="col-lg-12">
                                        <div class="form-group form-group1">
                                            <div class="form-group loglabel">
                                                <label>{{ __('frontend.name') }}</label>
                                            </div>
                                            <input name="name" type="text" class="form-control" placeholder="{{ __('frontend.enter_your_name') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group form-group1">
                                            <div class="form-group loglabel">
                                                <label>{{ __('frontend.Street') }}</label>
                                            </div>
                                            <input name="street" type="text" class="form-control" placeholder="{{ __('frontend.ent_str_na') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group form-group1">
                                            <div class="form-group loglabel">
                                                <label>{{ __('frontend.apartment') }}</label>
                                            </div>
                                            <input name="apartment" type="text" class="form-control" placeholder="{{ __('frontend.ent_ap_na') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group form-group1">
                                            <div class="form-group loglabel">
                                                <label>{{ __('frontend.P_O_Box') }}</label>
                                            </div>
                                            <input name="po_box" type="text" class="form-control" placeholder="{{ __('frontend.po_box') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group form-group1">
                                            <div class="form-group loglabel">
                                                <label>{{ __('frontend.landmark_lbl') }}</label>
                                            </div>
                                            <input name="landmark" type="text" class="form-control" placeholder="{{ __('frontend.land_mark') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group form-group1">
                                            <div class="form-group loglabel">
                                                <label>{{__('frontend.region_head')}}</label>
                                            </div>
                                            <select name="region" class="form-control" id="sel1">
                                                <option>--- {{__('frontend.region')}} ---</option>
                                                @foreach($regions as $region_val)
                                                <option value="{{ $region_val->id }}">{{ $region_val->lang[0]->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group loglabel">
                                            <label>{{__('frontend.phone_number')}}</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="cntry-code">
                                        <div class="input-group ">
                                            <div class="input-group-prepend"> <span class="input-group-text"> <i class="fa fa-flag" aria-hidden="true"></i> </span>
                                            </div>
                                            <input type="text" name="country_code" disabled="disabled" value="+966" class="form-control bleft" placeholder="+966">
                                        </div>
                                    </div>
                                    <div class="cntry-phnm">
                                        <div class="form-group form-group1">
                                            <input type="text" name="phone" class="form-control" placeholder="{{__('frontend.phone_number1')}}">
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 ">
                                    </div>
                                    <div class="col-lg-6 ">
                                        <input type='hidden' name='customer_id' id='customer_id' value="{{ $customer_id }}">
                                        <button class="btn btn-dark h60 ">{{__('frontend.cancel')}}</button>
                                        <button class="btn btn-success h60 fright">{{__('frontend.save')}}
                                        </button>
                                    </div>
                                </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="content" class="bg-light page-cart pagecheck">
    <div class="container">
        <div class="row ">
            <div id="breadcrumbs">
                <p><a href="{{ route('frontend') }}">{{__('frontend.home')}}</a> <i class="fa fa-long-arrow-right" aria-hidden="true"></i>  {{__('frontend.my_account1')}} <span> <i class="fa fa-long-arrow-right" aria-hidden="true"></i> {{__('frontend.my_address')}}
                    </span>
                </p>
            </div>
            <div class="col-lg-12 mt-4 mres15">
                <div class="row">
                    <div class="col-xs-6 col-md-6 p-0">
                        <h4 class="mb-2">{{__('frontend.my_address')}}</h4>
                    </div>
                    @if($is_address != 'checkout' && $customer_branchs->is_branch == 'N')
                    <div class="col-xs-6 col-md-6 col text-right mb-2">
                        <span class="select-box alignsel">
                            <select class="form-control branchss" onchange="AddressFilter(this.value)">
                                <option value="" {{$customer_id == $customer_branchs->id ? 'selected' : ''}}> <img src="assets/img/filter.png">{{$customer_branchs->cust_name}}</option>
                                @foreach ($customer_branchs->branch as $item)
                                @if($item->customer_id !== $customer_branchs->id )
                                <option value="{{$item->customer_id}}" {{$customer_id == $item->customer_id ? 'selected' : ''}}>{{$item->contact_name}}</option>
                                @endif
                                @endforeach
                            </select>
                        </span>
                    </div>
                    @endif
                </div>
            </div>
            @include('frontend.myaccount.sidebar')
            <div class="col-lg-9">
                <div class="text-left boxcheck">
                    <div class="row">
                        <div class="col-lg-4"><h5> <b>{{__('frontend.del_add')}}</b></h5></div>
                        <div class="col-lg-8">
                            <p class=" fright mb-52">  <button type="button" onclick="addAddress('D');" class="btn btn-success btnnormal fright fribut" data-toggle="modal" data-target="#ADDRESS">{{__('frontend.add_nw_dly_adr')}}</button>
                            </p>
                        </div>
                    </div>
                    <input type="hidden" id="address_from" value="{{ $is_address ?? ''}}">
                    @foreach($d_addresses as $addr)
                    @php
                    $name = $addr->region->lang[0]->name ?? '';
                    @endphp
                    <div class="row">
                        <div class="col-lg-6 branchadd brbook">
                            <p>{{ $addr->name }}</p>
                            <p> {{ $addr->apartment_name }}</p>
                            <p> {{ $addr->street_name }}</p>
                            <p> {{ $addr->landmark }}</p>
                            <p> PO Box: {{ $addr->postal_code }}</p>
                            <p> {{ $name }}</p>
                            <p> {{ $addr->phone }} </p>
                        </div>

                        <div>
                            <ul>
                                @if($addr->is_default=='Y')
                                <li>
                                    <i class="fa fa-bookmark bookmark bookm fa-address-icons" data-toggle="tooltip" data-placement="top" aria-hidden="true" title="{{__('frontend.default_address1')}}"></i>
                                </li>
                                @endif
                                @if($addr->is_default=='N')
                                <li>
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" onclick="changeDefaultAddress(this, {{ $addr->id }}, 'D', {{$customer_id}})" title="{{__('frontend.set_default')}}" data-toggle="tooltip"><i class="fa fa-arrows fa-address-icons" aria-hidden="true"></i></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" onclick="deleteAddress(this, {{ $addr->id }}, 'D')" title="{{__('frontend.delete_address')}}" data-toggle="tooltip"><i class="fa fa-trash fa-address-icons" aria-hidden="true"></i></a>
                                </li>
                                @endif
                                @if($is_address == 'checkout')
                                <li>
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" onclick="chhose_address(this, 'D')" address_id="{{ $addr->id }}" title="{{__('frontend.select_delivery')}}" data-toggle="tooltip"><i class="fa fa-id-card-o fa-address-icons" aria-hidden="true"></i></a>
                                </li>
                                @endif
                                @if(count($b_addresses) == 0)
                                <li>
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" onclick="addasBilling(this, {{ $addr->id }}, 'D', {{$customer_id}})" title="{{__('frontend.make_billing')}}" data-toggle="tooltip"><i class="fa fa-user-plus fa-address-icons" aria-hidden="true"></i></a>
                                </li>
                                @endif
                            </ul>
                        </div>
                       
                    </div>
                    <hr>
                    @endforeach
                    <div class="row">
                        <div class="col-lg-4"><h5> <b>{{__('frontend.bill_add')}}</b></h5></div>
                        <div class="col-lg-8">
                            <p class=" fright mb-52">  <button type="button" onclick="addAddress('B');" class="btn btn-success btnnormal fright fribut" data-toggle="modal" data-target="#ADDRESS">{{__('frontend.add_nw_bil_adr')}}</button>
                            </p>
                        </div>
                    </div>
                    @foreach($b_addresses as $addr)
                    @php
                    $name = $addr->region->lang[0]->name ?? '';
                    @endphp
                    <div class="row">
                        <div class="col-lg-6 branchadd brbook">
                            <p>{{ $addr->name }}</p>
                            <p> {{ $addr->apartment_name }}</p>
                            <p> {{ $addr->street_name }}</p>
                            <p> {{ $addr->landmark }}</p>
                            <p> PO Box: {{ $addr->postal_code }}</p>
                            <p> {{ $name }}</p>
                            <p> {{ $addr->phone }} </p>
                        </div>

                        <div>
                            <ul>
                                @if($addr->is_default=='Y')
                                <li>
                                    <i class="fa fa-bookmark bookmark bookm fa-address-icons" data-toggle="tooltip" data-placement="top" title="{{__('frontend.default_address1')}}"></i>
                                </li>
                                @endif
                                @if($addr->is_default=='N')
                                <li>
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" onclick="changeDefaultAddress(this, {{ $addr->id }}, 'B')" title="{{__('frontend.set_default')}}" data-toggle="tooltip"><i class="fa fa-arrows fa-address-icons" aria-hidden="true"></i></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" onclick="deleteAddress(this, {{ $addr->id }}, 'B')" title="{{__('frontend.delete_address')}}" data-toggle="tooltip"><i class="fa fa-trash fa-address-icons" aria-hidden="true"></i></a>
                                </li>
                                 @endif
                                @if($is_address == 'checkout')
                                <li>
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" onclick="chhose_address(this, 'B')" address_id="{{ $addr->id }}" title="{{__('frontend.select_billing')}}" data-toggle="tooltip"><i class="fa fa-id-card-o fa-address-icons" aria-hidden="true"></i></a>
                                </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    <hr>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@include('frontend.common.common_script')
<script>
    function addAddress(type) {
    $('#address_flag').val(type);
    if (type == 'B') {
    $('#address_title').text(@json(__('frontend.add_nw_bil_adr') ));
    } else {
    $('#address_title').text(@json(__('frontend.add_nw_dly_adr')));
    }
    }
</script>

@endsection