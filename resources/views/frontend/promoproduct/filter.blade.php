<div class="col-lg-3 mt-55">
    <a href="#" class="filters"><i class="fa fa-filter" aria-hidden="true"></i>{{__('frontend.fltrs')}} </a>
    <form id="frm_promo_filter" action="javascript:;" method="POST">
        <aside id="sidebar">
            <a href="#" class="close"><i class="fa fa-times-circle-o" aria-hidden="true"></i> </a>
            @if (count($suppliers) > 0)
            <div class="widget">
                <h2>{{__('frontend.shop_by_supplier')}}</h2>
                <ul>
                    @foreach ($suppliers as $supplier_val)
                    @php $checked = (isset($supplier_id) && in_array($supplier_val->id, $supplier_id)) ? 'checked="checked"' : '';@endphp
                    <label class="containerone">{{ $supplier_val->code }}
                        <input type="checkbox" name="supplier_id[]" value="{{ $supplier_val->id }}" {{ $checked }}/>
                        <span class="checkmark"></span>
                    </label>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (count($categories) > 0)
            <div class="widget">
                <h2>{{__('frontend.shop_by_cat')}}</h2>
                <ul>
                    @foreach ($categories as $by_category_val)
                    @php $checked = (isset($cat_id) && in_array($by_category_val->id , $cat_id)) ? 'checked="checked"' : '';@endphp
                    <label class="containerone">{{$by_category_val->lang[0]->name}} <span>({{$by_category_val->product_count}})</span>
                        <input type="checkbox" name=cat_id[]" value="{{$by_category_val->id}}" {{ $checked }}>
                        <span class="checkmark"></span>
                    </label>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (count($brands) > 0)
            <div class="widget">
                <h2>{{__('frontend.shop_by_brand')}}</h2>
                <ul>
                    @foreach ($brands as $brand_val)
                    @php $checked = (isset($brand_id) && in_array($brand_val->id, $brand_id)) ? 'checked="checked"' : '';@endphp
                    <label class="containerone">{{$brand_val->lang[0]->name}} <span>({{$brand_val->product_count}})</span>
                        <input type="checkbox" name="brand_id[]" value="{{ $brand_val->id }}" {{ $checked }}/>
                        <span class="checkmark"></span>
                    </label>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="widget">
                <h2>{{__('frontend.price_range')}}</h2>
                <div class="min">
                    <p>{{__('frontend.min_price')}}
                        <span></span>
                    </p>
                    <input type="hidden" name="minval" value="{{ isset($minval) ? $minval : '' }}" id="minval">
                </div>
                <div class="max">
                    <p>{{__('frontend.max_price')}}
                        <span></span></p>
                    <input type="hidden" name="maxval" value="{{ isset($maxval) ? $maxval : '' }}" id="maxval">
                </div>
                <div id="slider-range"></div>
            </div>
            <footer class="clearfix">
                <input type="hidden" name="sort" value="{{ isset($sort) ? $sort : '' }}" id="sort">
                <input type="hidden" name="list_type" value="{{ isset($list_type) ? $list_type : '' }}" id="list_type">
                <button type="button" onclick="location.href ='{{route('promotional.all')}}'">{{__('frontend.clear')}}</button>
                <button type="submit" class="apply">{{__('frontend.apply')}}</button>
            </footer>
        </aside>
    </form>
</div>
<div class="col-lg-9 mt-55">
    <div id="main">
        <header class="clearfix">
            <div class="title">
                <h2>{{__('frontend.promo_pdts')}}</h2>
                <p class="pagination">{{ $promotional_products->total()}} {{ $promotional_products->total() > 1 ? __('frontend.items') : __('frontend.item')}}</p>
            </div>
            <div class="sort">
                <span class="select-box align-middle">
                    <select class="form-control" name="price_filter" onchange="Sortval(this);">
                        <option select>{{__('frontend.sbp')}}</option>
                        <option value="low-high" {{ (isset($sort) && $sort == 'low-high') ? 'selected' : '' }}>Low to High</option>
                        <option value="high-low" {{ (isset($sort) && $sort == 'high-low') ? 'selected' : '' }}>High to Low</option>
                    </select>
                </span>
                <a href="javascript:void(0);" onclick="getList('list_view')"><i class="fa fa-list " aria-hidden="true"></i></a>
                <a href="javascript:void(0);" onclick="getList('grid_view')"><i class="fa fa-th active" aria-hidden="true"></i></a>
            </div>
        </header>
        @if($promotional_products->total() == 0)
        @php
        $no_mage = $lang == 'ar' ? 'no_iteam_bag-rtl.png' : 'no_iteam_bag.png';
        @endphp
        <div class="text-center">
            <img src="{{ URL::asset("assets/frontend/custom/images/{$no_mage}") }}">
            <p class="text-secondary mt-3">{{__('frontend.no_itm')}}</p>
        </div>
        @endif
        @if (count($promotional_products) > 0)
        <ul class="product-list">
            @php $wishListAll = Helper::wish_listed_products();@endphp
            @foreach ($promotional_products as $pdt_info_val)
            @php $inFav = in_array($pdt_info_val->id, $wishListAll) ? 'active' : ''; @endphp
            <li>
                <div class="product">
                    <header class="d-flex flex-wrap justify-content-between align-items-center">
                        <span class="total-discount">
                        </span>
                        <a onclick="event.preventDefault(); addToFavourites(this, {{ $pdt_info_val->id }});" class="favourite {{$inFav}}" href="#"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                    </header>
                    <div class="product-image">
                        <a href="{{route('products.detail', $pdt_info_val->slug)}}">
                            @if(is_file( public_path() . '/uploads/' . $pdt_info_val->cover_image))
                            <img src="{{ url('uploads/'.$pdt_info_val->cover_image) }}" alt="img">
                            @else
                            <img src="{{ asset('assets/frontend/custom/images/no-image.png') }}" alt="img">
                            @endif
                        </a>
                    </div>
                    <span class="price-block">
                        @php
                        $discount_price = $pdt_info_val->variant_price[0]->discount_price ?? NULL;
                        $act_price = $pdt_info_val->variant_price[0]->price ?? NULL;
                        $commission = isset($pdt_info_val->commission) ? $pdt_info_val->commission->value : NULL;
                        $mark_up = isset($pdt_info_val->variant_price) ? $pdt_info_val->variant_price[0]->easify_markup : NULL;
                        $dis_commission = ($discount_price * $commission / 100);
                        $price_commission = ($act_price * $commission / 100);
                        $dis_markup = ($discount_price * $mark_up / 100);
                        $price_markup = ($act_price * $mark_up / 100);
                        $d_price = $discount_price + $dis_commission + $dis_markup;
                        $price = $act_price + $price_commission + $price_markup;
                        @endphp
                        @if(!empty($d_price))
                        <span id="actual-price" class="old-price">{{$currency[$lang]}} {{ $price }}</span>
                        <span id="discount-price" class="offer-price">{{$currency[$lang]}} {{ $d_price }} </span>
                        @else
                        <span id="actual-price" class="old-price"></span>
                        <span id="discount-price" class="offer-price">{{$currency[$lang]}} {{ $price }} </span>
                        @endif
                    </span>
                    <div class="product-desc">
                        <p>
                            <a href="{{route('products.detail', $pdt_info_val->slug)}}"><span>{{ $pdt_info_val->lang[0]->name }}</span></a>
                        </p>
                    </div>
                    <ul class="rating">
                    @php $rating = $pdt_info_val->reviews->sum('rating') ? $pdt_info_val->reviews->sum('rating') / Count($pdt_info_val->reviews) : 0; @endphp
                    @for($i = 1; $i <= 5; $i++ )
                    @if(round( $rating - .25 ) >= $i )
                    <li class="fa fa-star"></li>
                    @elseif(round( $rating + .25 ) >= $i )
                    <li class="fa fa-star-half-o"></li>
                    @else
                    <li class="fa fa-star-o"></li>
                    @endif
                    @endfor
                    </ul>
                    <footer class="d-flex flex-wrap justify-content-between align-items-center ">
                        <div class="left-column">
                            @if($pdt_info_val->product_type == 'complex' && $pdt_info_val->variant_price)
                            <span class="select-box">
                                <select class="form-control" id="variant-select" onchange="changeDisplyaPrice(this);">
                                    @php $i = 0;@endphp
                                    @foreach($pdt_info_val->variant_price as $variants)
                                    @if($lang == 'ar')
                                    @php
                                    $variant_ar = \App\Models\VariantLang::where('en_matching_id', $variants->variant_lang->id)->pluck('name')->first();
                                    $variant_name = $variant_ar;  @endphp
                                    @else
                                    @php $variant_name = $variants->variant_lang->name ?? ''; @endphp
                                    @endif
                                    @php
                                    $discount_price = $variants->discount_price ?? NULL;
                                    $act_price = $variants->price ?? NULL;
                                    $commission = isset($pdt_info_val->commission) ? $pdt_info_val->commission->value : NULL;
                                    $mark_up = isset($variants->easify_markup) ? $variants->easify_markup : NULL;
                                    $dis_commission = ($discount_price * $commission / 100);
                                    $price_commission = ($act_price * $commission / 100);
                                    $dis_markup = ($discount_price * $mark_up / 100);
                                    $price_markup = ($act_price * $mark_up / 100);
                                    $d_price = $discount_price + $dis_commission + $dis_markup;
                                    $price = $act_price + $price_commission + $price_markup;
                                    $d_price = ($d_price > 0) ? $d_price : '';
                                    $price = $price ?? '';
                                    @endphp
                                    <option price="{{ $price }}" d_price="{{ $d_price }}" min_stock="{{$pdt_info_val->variant[$i]->pdt_branch->min_stock ?? ''}}" max_stock="{{$pdt_info_val->variant[$i]->pdt_branch->max_stock ?? ''}}"
                                            value="{{ $variants->variant_lang->id }}">{{ $variant_name ?? ''}}</option>
                                    @php $i++;@endphp
                                    @endforeach
                                </select>
                            </span>
                            @endif
                            @if(isset($pdt_info_val->variant[0]->pdt_branch))
                            <div class="quantity">
                                <input type="button" value="-" class="minus" min_stock="{{$pdt_info_val->variant[0]->pdt_branch->min_stock}}" onclick="updateCount(this)" id="min_amount">
                                <input type="number" class="qty" step="1" min="1" max="" value="{{$pdt_info_val->variant[0]->pdt_branch->min_stock}}" id="pdt_qty">
                                <input type="button" value="+" class="plus" max_stock="{{$pdt_info_val->variant[0]->pdt_branch->max_stock}}" onclick="updateCount(this)" id="max_amount">
                            </div>
                            @endif
                        </div>
                        <div class="right-column">
                            <a href="#" class="add-btn align-middle" onclick="addToCart(this, {{ $pdt_info_val->id }}, '{{ $pdt_info_val->product_type }}', 'list')">{{__('frontend.add')}}</a>
                        </div>
                    </footer>
                </div>
            </li>
            @endforeach
        </ul>
        @endif
    </div>
    <input type="hidden" name="cat_id" value="{{ isset($filter_cat_ids) ? $filter_cat_ids : '' }}" id="cat_id">
    <input type="hidden" name="brand_id" value="{{ isset($filter_brand_ids) ? $filter_brand_ids : '' }}" id="brand_id">
    <input type="hidden" name="supplier_id" value="{{ isset($filter_supplier_ids) ? $filter_supplier_ids : '' }}" id="supplier_id">
    <input type="hidden" name="maxPage" value="{{ $promotional_products->lastPage() }}" id="maxPage">
    <input type="hidden" name="page" value="{{ isset($page) ? $page : 1 }}" id="page">
    <div class="text-center">
        <div class="ajax-loading" style="display: none"><img src="{{ asset('assets/images/loader.gif') }}"/></div>
    </div>
</div>
@if(isset($search) && $search == 'filter')
<script>
    $("#frm_promo_filter").submit(function () {
    $('.ajax-loading-buz').show();
    var form = $(this);
    $.ajax({
    type: "POST",
            url: "{{route('promoproduct.search')}}",
            data: form.serialize(), // serializes the form's elements.
            success: function (data)
            {
            $('#pdt_list').html(data);
            $('.ajax-loading-buz').hide();
            }
    });
    });
    var minval = $("#minval").val() ? $("#minval").val() : 0;
    var maxval = $("#maxval").val() ? $("#maxval").val() : 10000;
    $("#slider-range").slider({
    range: true,
            min: 0,
            max: 10000,
            values: [minval, maxval],
            slide: function (event, ui) {
            $("#minval").val(ui.values[0]);
            $("#maxval").val(ui.values[1]);
            $("#sidebar .widget .min p span").html(ui.values[0]);
            $("#sidebar .widget .max p span").html(ui.values[1]);
            }
    });
    if ($("#minval").val() && $("#maxval").val()) {
    $("#sidebar .widget .min p span").html($("#minval").val());
    $("#sidebar .widget .max p span").html($("#maxval").val());
    } else {
    $("#sidebar .widget .min p span").html($("#slider-range").slider("values", 0));
    $("#sidebar .widget .max p span").html($("#slider-range").slider("values", 1));
    }
</script>
@endif