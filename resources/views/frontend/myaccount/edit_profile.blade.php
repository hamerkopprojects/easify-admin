@extends('frontend.frontend_layouts.frontend_app')
@section('content')
<div id="content" class="bg-light page-cart">
    <div class="container">
        <div class="row editres">
            <div id="breadcrumbs">
                <p><a href="{{ route('frontend') }}">{{__('frontend.home')}} </a><i class="fa fa-long-arrow-right" aria-hidden="true"></i> <span> {{__('frontend.edt_pro')}}</span>
                </p>
                <h4 class=" pt-2 mt-3">{{__('frontend.edt_pro')}}</h4>
            </div>
            <div class="row mt22 ">
                @include('frontend.myaccount.sidebar')
                <div class="col-lg-9">


                    <div class="form-container boxcheck">
                        @if(is_file( public_path() . '/uploads/' . $profile->profile_image))
                        <img src="{{ url('uploads/'.$profile->profile_image) }}" class="profileimg">
                        @else
                        <img src="{{ asset('assets/frontend/img/2.png') }}" class="profileimg">
                        @endif
                        <div class="control-fileupload" style="display: none;" >
                            <input type="file" id="profile_image" name="profile_image"/>
                        </div>
                        <a href="javascript:void(0);" class="changeph" id='change_photo'>{{__('frontend.chang_photo')}}</a>

                        <div class="tab-content">
                            <form class="form-horizontal" id="edit_profile">

                                <div class="row mt-3 mb-3">
                                    <div class="col-lg-12">
                                        <div class="form-group form-group1 select-box sl100">
                                            <label>{{__('frontend.busines_type')}}</label>
                                            <select class="form-control" id="sel1" name='business_type'>
                                                <option>{{__('frontend.busines_type')}}</option>
                                                @foreach($business_type as $bus_val)
                                                <option value="{{ $bus_val->id }}"  {{ ($profile->business_type == $bus_val->id) ? 'selected' : ''}} >{{ $bus_val->lang[0]->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group form-group1">
                                            <label>{{__('frontend.name')}} *</label>
                                            <input type="text" class="form-control" name="name" value="{{ $profile->cust_name ?? '' }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group form-group1">
                                            <label>{{__('frontend.email')}} *</label>
                                            <input type="text" class="form-control" name="email" value="{{ $profile->email ?? '' }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group form-group1 dateimg">
                                            <label>{{__('frontend.dob')}}</label>
                                            <input type="text" class="form-control" name="dob" placeholder="{{__('frontend.select_date_of_birth')}}" id="datepicker" autocomplete="off" value="{{ $profile->dob ?? '' }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group form-group1 select-box sl100">
                                            <label>{{__('frontend.city')}} *</label>
                                            <select class="form-control" name="city" onchange="getRegion(this);">
                                                <option value="">{{__('frontend.select_city')}}</option>
                                                @foreach($city as $city_val)
                                                <option value="{{ $city_val->id }}" {{ ($city_val->id == $profile->city) ? 'selected' : '' }}>{{ $city_val->lang[0]->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group form-group1 select-box sl100">
                                            <label>{{__('frontend.region')}} *</label>
                                            <select class="form-control" name="region" id="region">
                                                <option value="">{{__('frontend.select_region')}}</option>
                                                @foreach($region as $region_val)
                                                <option value="{{ $region_val->id }}" {{ $region_val->id == $profile->region ? 'selected' : '' }}>{{ $region_val->lang[0]->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>{{__('frontend.phone_number')}} *</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-2  col-xs-2 w30">
                                        <div class="input-group ">
                                            <div class="input-group-prepend"> <span class="input-group-text"> <i class="fa fa-flag" aria-hidden="true"></i> </span>
                                            </div>
                                            <input type="text" class="form-control bleft" value="+966" placeholder="Enter Code" disabled="disabled">
                                            <input type="hidden" value="+966" name="country_code">
                                        </div>
                                    </div>
                                    <div class="col-lg-10  col-xs-8 w60">
                                        <div class="form-group form-group1">
                                            <input type="text" class="form-control" name="phone" placeholder="Enter Number" value="{{ $profile->phone ?? '' }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col">
                                        <button class="btn btn-success h60">{{__('frontend.save')}}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>
</div>
@endsection
@include('frontend.common.common_script')
@push('css')
<style type="text/css">
    .ajax-loading{
        text-align: center;
    }
</style>
@endpush
@push('scripts')
<script>
    $("#datepicker").datepicker({maxDate: 0, changeMonth: true,
        changeYear: true,
        dateFormat: "dd-mm-yy",
        yearRange: "-70:+00"});
    $('#change_photo').click(function () {
        $('.ajax-loading-buz').show();
        $('#profile_image').click();
    });
    $('#profile_image').on('change', function () {
        $('.ajax-loading-buz').show();
        const file = $(this)[0].files[0];
        var formData = new FormData();
        formData.append('profile_image', file);
        $.ajax({
            url: "{{ route('profile_image')}}",
            data: formData,
            dataType: 'json',
            async: false,
            type: 'post',
            processData: false,
            contentType: false,
            success: function (data) {
                if (data.status == 1) {
                    $.toaster({
                        priority: 'success',
                        title: @json(__('frontend.success') ),
                        message: data.message
                    });
                    window.setTimeout(function () {
                        window.location.reload();
                    }, 500);
                } else {
                    $.toaster({
                        priority: 'danger',
                        title: @json(__('frontend.error') ),
                        message: data.message,
                        timeout: 5000,
                    });
                }
            },
            complete: function () {
                $('.ajax-loading-buz').hide();
            }
        });
    });

    $("#edit_profile").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        rules: {
            name: {
                required: true,
            },
            email: {
                required: true,
                email: true,
            },
            city: {
                required: true,
            },
            region: {
                required: true,
            },
            phone: {
                required: true,
                number: true,
                minlength: 6, // will count space
                maxlength: 12
            },
        },
        messages: {
            name: {
                required: @json(__('frontend.name_req') ),
            },
            email: {
                required: @json(__('frontend.email_req') ),
                email: @json(__('frontend.iv_vd_emil') ),
            },
            city: {
                required: @json(__('frontend.city_required') ),
            },
            region: {
                required: @json(__('frontend.region_req') ),
            },
            phone: {
                required:@json(__('frontend.phone_req') ),
                number:@json(__('frontend.invd_ph') )
            },
        },
        submitHandler: function (form) {
            $('.ajax-loading-buz').show();
            var data = $('#edit_profile').serialize();
            $('.ajax-loading-buz').show();
            $.ajax({
                url: "{{ route('edit_profile') }}",
                type: 'POST',
                data: data,
                dataType: "json",
                success: function (data) {
                    if (data.status == 1) {
                        $.toaster({
                            priority: 'success',
                            title: @json(__('frontend.success') ),
                            message: data.message,
                            timeout: 3000,
                        });
                        window.location.reload();
                    } else {
                        $.toaster({
                            priority: 'danger',
                            title: @json(__('frontend.error') ),
                            message: data.message,
                            timeout: 3000,
                        });
                    }
                },
                complete: function () {
                    $('.ajax-loading-buz').hide();
                }
            });
            return false;
        }
    });

    function getRegion(sel)
    {
        var city_id = sel.value;
        $.ajax({
            type: "GET",
            url: "{{route('profile.region')}}",
            data: {'city_id': city_id},
            success: function (data) {
                var options = "";
                options += '<option value="">Select Region</option>';
                for (var i = 0; i < data.length; i++) {
                    options += '<option value=' + data[i].id + '>' + data[i].lang[0].name + "</option>";
                }
                $("#region").html(options);
            }
        });
    }
</script>
@endpush

