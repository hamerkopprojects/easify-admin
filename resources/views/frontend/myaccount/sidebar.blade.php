@php
$customer_id = \Session::get('customer_id');
$profile = \App\Models\Customer::where('id', $customer_id)->select('is_branch')->first();
@endphp
<div class="col-lg-3 p-0 mres15 mb-2">
    <div class="myorderul">
        <ul>
            <li class="{{ request()->is('*order*') ? 'active' : '' }}"><a href="{{route('order.list')}}">{{__('frontend.my_orders')}} </a></li>
            @if(isset($profile->is_branch) && $profile->is_branch == 'N')
            <li class="{{ request()->is('*customer/branch*') ? 'active' : '' }}"><a href="{{ route('customer.branch') }}">{{__('frontend.all_branch')}}</a></li>
            @endif
            <li class="{{ request()->is('*customer/address*') ? 'active' : '' }}"><a href="{{ route('customer.adddresses') }}">{{__('frontend.my_address')}}</a></li>
            <li class="{{ request()->is('*customer/wish_list*') ? 'active' : '' }}"><a href="{{ route('customer.wishlist') }}">{{__('frontend.my_wishlist')}}</a></li>
            <li class="{{ request()->is('*profile/edit*') ? 'active' : '' }}"><a href="{{ route('profile.edit') }}">{{__('frontend.edt_pro')}}</a></li>
            <li class="{{ request()->is('*support-list*') ? 'active' : '' }}"><a href="{{ route('supprot.frndend.list') }}">{{__('frontend.cont_sup')}}</a></li>
        </ul>
    </div>
</div>