@extends('frontend.frontend_layouts.frontend_app')
@section('content')
<div id="content" class="bg-light">
    <div class="container">
        <div id="breadcrumbs">
            <p><a href="{{ route('frontend') }}">{{__('frontend.home')}} </a> <i class="fa fa-long-arrow-right" aria-hidden="true"></i> <span>{{ $product_data->lang[0]->name }}</span></p>
        </div>
        @include('frontend.product.details')
        @include('frontend.product.similar')
        @include('frontend.product.recently')
    </div>
</div>
@include('frontend.common.common_script')

@endsection
@push('css')
<style>
    #social-links ul li {
    list-style: none;
    padding: 10px;
    display: inline-block;
    font-size: 30px;
    text-align:center;
    margin:5px 20px;
}
</style>
@endpush
@push('scripts')
<script>
            $('.owl-carouseltwo, .owl-carouselone').owlCarousel({
            items:4,
             margin:10,
            padding:0,
            autoplay:true,
            <!-- autoplayTimeout:9000 , -->
            dots: true,
            <!-- animateOut: 'slideOutUp', -->
             nav: false,
            	navText: [
            	'<i class="fa fa-angle-left d-none"></i>',
            	'<i class="fa fa-angle-right d-none"></i>'
            	],
              autoplay:true,
            	responsiveClass: true,
            	responsiveRefreshRate: true,
             responsive:{
                 0:{
                     items:1,
            nav:false,
            
                 },
                 600:{
                     items:2,
            nav:false,
                 },
                 1000:{
                     items:4,
            nav:false,
                 }
             }
            })
</script>
@endpush