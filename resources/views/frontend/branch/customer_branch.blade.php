@extends('frontend.frontend_layouts.frontend_app')
@section('content')
<div id="content" class="bg-light page-cart pagecheck">
    <div class="container">
        <div class="row ">
            <div id="breadcrumbs " class="mres15 mt-5 bearcham">
                <p><a href="#">{{__('frontend.home')}}</a> <i class="fa fa-long-arrow-right" aria-hidden="true"></i> {{__('frontend.my_account1')}} <span> <i class="fa fa-long-arrow-right" aria-hidden="true"></i> {{__('frontend.cust_bran')}}
                </span>
                </p>
            </div>
            <div class="col-lg-12 mt-4 mb-4 mres15">
                <div class="row">
                <div class="col-xs-6 col-md-6   p-0">
                    <h4 class="mb-2">{{__('frontend.cust_bran1')}}</h4>
                </div>
                <div class="col-xs-6 col-md-6  ">
                    <button type="button" class="btn btn-success btnnormal fright fribut" data-toggle="modal" data-target="#branch">{{__('frontend.add_bran')}}</button>
                </div>
                </div>
            </div>
            @include('frontend.myaccount.sidebar')
            <div class="col-lg-9">
                <div class="text-left boxcheck">
                    @if(count($branches) == 0)
                    <div class="row">
                    <div class="col-lg-7 branchadd">
                        <p>{{__('frontend.no_branch')}} <b></p>
                    </div>
                    </div>
                    @else
                        <div>
                            @foreach($branches as $branch)
                                <div class="row">
                                    <div class="col-lg-7 branchadd">
                                        <p>{{__('frontend.bran_nam')}} <b> : {{ $branch->branch }}</b></p>
                                        <p>{{__('frontend.cont_per_nam')}}  : {{ $branch->contact_name }}</p>
                                        <p>{{__('frontend.email')}}  :  {{ $branch->email }}</p>
                                        <p>{{__('frontend.phone_number')}}   : {{ $branch->phone }}</p>
                                    </div>
                                    <div class="col-lg-5">
                                        <p class="  fright mb-52"> <span class="tooltip1"><a href="javascript:void(0);" onclick="BranchLoginPermission(this, {{ $branch->customer_id }})" class="addemail"><img src="{{ asset('assets/frontend/img/add-email.png') }}"></a>
                                            <span class="tooltiptext">{{__('frontend.sent_log_branch')}}</span></span>
                                            <a href="javascript:void(0);" onclick="deleteBranch(this, {{ $branch->id }})" class="bookdelete btn btn-danger padrmo" title="Delete Branch"> {{__('frontend.remove')}}</a>
                                        </p>
                                        <div class="clearfix"></div>
                                        <p><a href="{{ route('customer.adddresses') }}/{{$branch->customer_id}}" class="bookdelete btn btn-success fright"> {{__('frontend.view_addr')}}</a></p>
                                    </div>
                                </div>
                                <hr>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <!-- Common script -->
    @include('frontend.common.common_script')

</div>

@endsection