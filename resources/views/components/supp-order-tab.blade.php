<ul class="nav nav-pills" role="tablist">
    @foreach ($tabs as $key => $tab)
    <li class="nav-item" style="margin-right:60px" >
        <a 
            class="nav-link @if($key == $activeTab) active @endif" 
            href="{{ $url[$key] }}" role="tab">
            {!! trans("messages.admin.{$tab}") !!}
        </a>

    </li>
    @endforeach
</ul>
