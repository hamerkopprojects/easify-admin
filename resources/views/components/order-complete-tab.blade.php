<ul class="nav nav-tabs">
    @foreach ($tabs as $key => $tab)
    <li class="nav-item">
        <a 
            class="nav-link @if($key == $activeTab) active @endif" 
            href="{{ $url[$key] }}">
            {{ $tab }}
        </a>

    </li>
    @endforeach
</ul>
