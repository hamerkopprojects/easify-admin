@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>ORDER ACTIVITY</h1>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
            </div>
            <!-- /# row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form id="posts-filter" method="get" action="{{ route('log.order') }}">
                                <div class="row tablenav top text-left">

                                    <div class="col-md-4 ml-0">
                                        <div class='input-group date' id='datepicker'>
                                            <input class="form-control datetimepicker" type="text" name="date" value="{{$date ? \Carbon\Carbon::parse($date)->format('d-m-Y') : ''}}" placeholder="Search by date" autocomplete="off">
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar icon-style"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4 ml-0">
                                        <select class="form-control search_val" name="user_id" id="user_id" autocomplete="off">
                                            <option value="">By Operational User</option>
                                            @foreach ($users as $user)
                                            <option value="{{$user->id}}" {{ $user->id == $usr_id ? "selected" :""}}>{{ucfirst($user->name)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4 ml-0">
                                        
                                    </div>
                                    <div class="col-md-4 ml-0">
                                        <select class="form-control search_val" name="supplier" id="supplier_id">
                                            <option value="">By Supplier</option>
                                            @foreach ($suppliers as $supp)
                                            <option value="{{$supp->id}}" {{ $supp->id == $supp_id ? "selected" :""}}>{{ucfirst($supp->name)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4 ml-0">
                                        <select class="form-control search_val" name="customer" id="customer_id">
                                            <option value="">By Customer</option>
                                            @foreach ($customers as $cust)
                                            <option value="{{$cust->id}}" {{ $cust->id == $cust_id ? "selected" :""}}>{{ucfirst($cust->cust_name)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4 text-left">
                                        <button type="submit" class="btn btn-info">
                                            <font style="vertical-align: inherit;">Search</font>
                                        </button>
                                        <a href="{{route('log.order')}}" class="btn btn-default">Reset</a>
                                    </div>
                                </div>
            
                            </form>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div id="msgDiv"></div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>S. No.</th>
                                            <th class="text-left">Order Activity</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($log) > 0)
                                        @php
                                        $i = 1;
                                        @endphp
                                        @foreach ($log as $row_data)
                                        <tr>
                                            <th>{{ $i++ }}</th>
                                            <td class="text-left">{!! $row_data->log !!}</td>
                                           
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="8" class="text-center">No records found!</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center d-flex justify-content-center mt-3">
                                {{ $log->appends(request()->input())->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style>
    .select2-container--default .select2-selection--single {
        height: 42px !important;border-radius: 0px !important;font-size: 1rem !important;
        padding: .4rem !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow b { top:20px !important}

</style>
@endpush
@push('scripts')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
$('.datetimepicker').daterangepicker({
    "singleDatePicker": true,
    "autoUpdateInput": false,
    "autoApply": true,
    locale: {
        format: 'DD-MM-YYYY'
    }
});
$('.datetimepicker').on('apply.daterangepicker', function (ev, picker) {
    $(this).val(picker.startDate.format('DD-MM-YYYY'));
});

$("#user_id").select2();
$("#customer_id").select2();
$('#supplier_id').select2();
</script>
@endpush