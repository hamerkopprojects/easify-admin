@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>CITIES</h1>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
                <div class="col-lg-3">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12 text-right">
                                <a href="javascript:void(0);" id = "add_new_city" class="btn btn-info create_btn">
                                    <font style="vertical-align: inherit;"><i class="ti-plus"></i> Add City
                                    </font>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
            </div>
            <!-- /# row -->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form id="posts-filter" method="get" action="{{ route('city.get') }}">
                    <div class="row tablenav top text-right">
                        <div class="col-md-5 ml-0">
                            <input class="form-control" type="text" name="search" value="{{$search}}" placeholder="Search City">
                        </div>
                        <div class="col-md-3 text-left">
                            <button type="submit" class="btn btn-info">
                                <font style="vertical-align: inherit;">Search</font>
                            </button>
                            <a href="{{ route('city.get') }}" class="btn btn-default reset_style">Reset</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S. No.</th>
                                <th>City (EN)</th>
                                <th class="right-align">City (AR)</th>
                                <th width="25%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($city) > 0)
                            @php
                            $i = 1;
                            @endphp

                            @foreach ($city ?? '' as $row_data)
                            <tr>
                                <th>{{ $i++ }}</th>
                                <td>{{ $row_data->lang[0]->name ?? '' }}</td>
                                <td class="right-align">{{ $row_data->lang[1]->name }}</td>
                                
                                <td class="text-left">
                                    <button type="button" class="change-status btn btn-sm btn-toggle ml-0 {{$row_data->status}}" data-toggle="button" data-id="{{ $row_data->id }}" data-status="{{ $row_data['status'] == 'active' ? 'Deactivate' :'Activate' }}" aria-pressed="true" autocomplete="off">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                  
                                    <a class="btn btn-sm btn-success text-white edit_btn edit_city" title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>

                                    <a class="btn btn-sm btn-danger text-white" title="Delete City" onclick="deleteCity({{ $row_data->id }})"><i class="fa fa-trash"></i></a>
                                  

                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $city->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
{{-- Pop Up --}}
<div class="modal fade" id="user_popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="name_change"><strong>
                        ADD CITY
                    </strong></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form id="city_form" action="javascript:;" method="POST">
                <div id="brdar_photo_loader" class="loader" style="display: none;"> </div>
                <div class="modal-body">
                    <div class="msg_div"></div>
                    <div class="row">
                        <input type="hidden" name="city_unique" id='city_unique'>
                        <div class="col-md-6">
                            <label class="control-label">City (EN) <span class="text-danger">*</span></label>
                            <input class="form-control form-white" placeholder="Enter name" type="text" name="city_en" id="city_en" />
                        </div>
                        <div class="col-md-6">
                            <label>City (AR)<span class="text-danger">*</span></label>
                            <input class="form-control" name="city_ar" id="city_ar" placeholder="مدينة" style="text-align:right !important" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                 
                    <button type="submit" class="btn btn-info waves-effect waves-light save-city" id="save_data">
                        ADD
                    </button>
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- end popup --}}
@endsection
@push('css')
<style>
    .reset_style {
        margin-left: 15px;
    }

    .loader{
    position: absolute;
    top:0px;
    right:0px;
    width:100%;
    height:100%;
    background-color:#eceaea;
    background-image:url('../assets/images/loader.gif');
    background-repeat:no-repeat;
    background-size: 50px;
    z-index:10000000;
    opacity: 0.4;
    filter: alpha(opacity=40);
}
</style>
@endpush
@push('scripts')

<script>
    

    $('#add_new_city').on('click', function() {
    
        $('#name_change').html('Add City');
        $('#save_data').text('Add').button("refresh");
        $("#city_form")[0].reset();
        $('#user_popup').modal({
            show: true
        });
    })
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.save-city').on('click', function(e) {
        // e.preventDefault();
        

        $("#city_form").validate({
            rules: {
                city_en: {
                    required: true,
                },
                city_ar: {
                    required: true,
                },
                
            },
            messages: {
                city_en: {
                    required: "City (EN) required",
                },
                city_ar: {
                    required: "City (AR) required",
                },
            },
            submitHandler: function(form) {
                $(`#brdar_photo_loader`).show();
                city_unique = $("#city_unique").val();
                if (city_unique) {
                    $.ajax({
                        type: "POST",
                        url: "{{route('city.update')}}",
                        data: {
                            city_en: $('#city_en').val(),
                            city_ar: $('#city_ar').val(),
                            city_unique: city_unique


                        },

                        success: function(data) {
                            $(`brdar_photo_loader`).hide();
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.href = '{{route("city.get")}}';
                                }, 1000);
                                $("#city_form")[0].reset();
                            } else {
                                $(`brdar_photo_loader`).hide();
                                // console.log(data.message);
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });


                } else {
                    $.ajax({
                        type: "POST",
                        url: "{{route('city.store')}}",
                        data: {
                            city_en: $('#city_en').val(),
                            city_ar: $('#city_ar').val(),
                        },

                        success: function(data) {
                            $(`brdar_photo_loader`).hide();
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.href = '{{route("city.get")}}';
                                }, 1000);
                                $("#city_form")[0].reset();
                            } else {
                                $(`brdar_photo_loader`).hide();
                                // console.log(data.message);
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });

                }



            }
        })
    });

    $('.edit_city').on('click', function(e) {
        page = $(this).data('id')
        

        $('#name_change').html('Edit City');
        $('#save_data').text('Save').button("refresh");


        var url = "city/edit/";

        $.get(url + page, function(data) {
         
            $('#city_en').val(data.city.lang[0].name),
            $('#city_ar').val(data.city.lang[1].name),
            $('#city_unique').val(data.city.id)
            $('#user_popup').modal({
                show: true

            });
        });
    });

    $('.change-status').on('click', function(e) {
console.log($(this).data('status'));
        var id = $(this).data('id');
        var act_value = $(this).data('status');
     console.log(id);
        $.confirm({
            title: act_value + ' City',
            content: 'Are you sure to ' + act_value + ' the city?',
            buttons: {
                Yes: function() {
        $.ajax({
            type: "POST",
            url: "{{route('city.status.update')}}",
            data: {
                id: id,
                status: act_value
            },

            success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.href = '{{route("city.get")}}';
                                }, 1000);

                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    window.location.reload();
                }
            }
        });
    });
   
   
    function deleteCity(id) {
        $.confirm({
            title: false,
            content: 'Are you sure to delete this city? <br><br>You wont be able to revert this',
            buttons: {
                Yes: function() {
                    $.ajax({
                        type: "POST",
                        url: "{{route('city.delete')}}",
                        data: {
                            id: id
                        },
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                window.setTimeout(function() {
                                    window.location.href = '{{route("city.get")}}';
                                }, 1000);
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    console.log('cancelled');
                }
            }
        });
    }
</script>
@endpush