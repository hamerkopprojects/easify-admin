<form id="frm_create_stock" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label class="control-label"><strong>STOCK</strong></label>
                </div>
                @if($product_type == 'simple')
                @php $pdt_branch_stock = \App\Models\ProductVariant::with(['pdt_branch' => function ($query) use ($supplier_data){
                        $query->where('branch_id', $supplier_data->id);
                    }])->where('product_id', $pdt_id)->first();
                @endphp
                <div class="col-md-4">
                </div>
                <div class="col-md-4">
                    <label class="control-label"><strong>Min Stock</strong></label>
                    <p><small><i class="fa fa-info-circle"></i> Min. Stock is the minimum number of items, which the customer must order.</small></p>
                </div>
                <div class="col-md-4">
                    <label class="control-label"><strong>Max Stock</strong></label>
                    <p><small><i class="fa fa-info-circle"></i> Max. Stock is the maximum number of items, which the customer can order.</small></p>
                </div>
                <div class="col-md-4">
                    <label class="control-label">{{$supplier_data->name ?? ''}} / {{$supplier_data->code ?? ''}}</label>
                </div>
                <div class="col-md-4"><input class="form-control form-white" placeholder="Enter min stock" value="{{ $pdt_branch_stock->pdt_branch->min_stock ?? '' }}" name="min_stock[]" id="min_stock_{{ $supplier_data->id }}"/></div>
                <div class="col-md-4"><input class="form-control form-white sm" placeholder="Enter max stock" value="{{ $pdt_branch_stock->pdt_branch->max_stock ?? '' }}" name="max_stock[]" id="max_stock_{{ $supplier_data->id }}"/></div>
                <input type="hidden" value="{{ $supplier_data->id }}" name="branch_id[]"/>
                <input type="hidden" value="{{ $pdt_branch_stock->id ?? '' }}" name="product_variant_id"/>
                @foreach($branch_data as $value)
                @php $pdt_branch_stock = \App\Models\ProductVariant::with(['pdt_branch' => function ($query) use ($value){
                        $query->where('branch_id', $value->supplier->id);
                    }])->where('product_id', $pdt_id)->first();
                @endphp
                <div class="col-md-4">
                    <label class="control-label">{{$value->supplier->name ?? ''}} / {{$value->supplier->code ?? ''}}</label>
                </div>
                <div class="col-md-4"><input class="form-control form-white" placeholder="Enter min stock" value="{{ $pdt_branch_stock->pdt_branch->min_stock ?? '' }}" name="min_stock[]" id="min_stock_{{ $value->supplier->id }}"/></div>
                <div class="col-md-4"><input class="form-control form-white sm" placeholder="Enter max stock" value="{{ $pdt_branch_stock->pdt_branch->max_stock ?? '' }}" name="max_stock[]" id="max_stock_{{ $value->supplier->id }}"/></div>
                <input type="hidden" value="{{ $value->supplier->id }}" name="branch_id[]"/>
                @endforeach
                @else
                @if($variants = \App\Models\ProductVariant::with('variant')->where(['product_id' => $pdt_id])->get())
                @php $i=0;@endphp
                @foreach($variants as $variant_val)
                @if(!empty($variant_val->variant->id))
                <div class="col-md-12 mt-3"><h6>{{ $pdt_att_var->lang[0]['name'] ?? '' }} - {{ $variant_val->variant->name ?? '' }}</h6></div>
                <div class="col-md-4">
                </div>
                <div class="col-md-4">
                    <label class="control-label"><strong>Min Stock</strong></label>
                    <p><small><i class="fa fa-info-circle"></i> Min. Stock is the minimum number of items, which the customer must order.</small></p>
                </div>
                <div class="col-md-4">
                    <label class="control-label"><strong>Max Stock</strong></label>
                    <p><small><i class="fa fa-info-circle"></i> Max. Stock is the maximum number of items, which the customer can order.</small></p>
                </div>
                @php $pdt_branch_stock = \App\Models\ProductVariant::with(['pdt_branch' => function ($query) use ($supplier_data){
                        $query->where('branch_id', $supplier_data->id);
                    }])->where('product_id', $pdt_id)->where('variant_lang_id', $variant_val->variant_lang_id)->first();
                @endphp
                <div class="col-md-4">
                    <label class="control-label">{{$supplier_data->name ?? ''}} / {{$supplier_data->code ?? ''}}</label>
                </div>
                <div class="col-md-4"><input class="form-control form-white" placeholder="Enter min stock" value="{{ $pdt_branch_stock->pdt_branch->min_stock ?? '' }}" name="min_stock_var[]" id="min_stock_var_{{ $i++ }}"/></div>
                <div class="col-md-4"><input class="form-control form-white sm" placeholder="Enter max stock" value="{{ $pdt_branch_stock->pdt_branch->max_stock ?? '' }}" name="max_stock_var[]" id="max_stock_var_{{ $i++ }}"/></div>
                <input type="hidden" value="{{ $supplier_data->id }}" name="branch_id[]"/>
                <input type="hidden" value="{{ $variant_val->id }}" name="product_variant_id[]"/>
                @foreach($branch_data as $value)
                @php $pdt_branch_stock1 = \App\Models\ProductVariant::with(['pdt_branch' => function ($query) use ($value){
                        $query->where('branch_id', $value->supplier->id);
                    }])->where('product_id', $pdt_id)->where('variant_lang_id', $variant_val->variant_lang_id)->first();
                @endphp
                <div class="col-md-4">
                    <label class="control-label">{{$value->supplier->name ?? ''}} / {{$value->supplier->code ?? ''}}</label>
                </div>
                <div class="col-md-4"><input class="form-control form-white" placeholder="Enter min stock" value="{{ $pdt_branch_stock1->pdt_branch->min_stock ?? '' }}" name="min_stock_var[]" id="min_stock_var_{{ $i++ }}"/></div>
                <div class="col-md-4"><input class="form-control form-white sm" placeholder="Enter max stock" value="{{ $pdt_branch_stock1->pdt_branch->max_stock ?? '' }}" name="max_stock_var[]" id="max_stock_var_{{ $i++ }}"/></div>
                <input type="hidden" value="{{ $value->supplier->id }}" name="branch_id[]"/>
                <input type="hidden" value="{{ $variant_val->id }}" name="product_variant_id[]"/>
                @endforeach
                @endif
                @endforeach
                @endif
                @endif
            </div>
            <div class="modal-footer">
                <input type="hidden" id="pdt_id" name="pdt_id" value="{{ $pdt_id }}">
                <input type="hidden" id="product_type" name="product_type" value="{{ $product_type }}">
                <input type="hidden" id="submit_action" value="" />
                @if(!empty($pdt_id))
                <button type="submit" class="btn btn-info waves-effect waves-light save-btn">
                    Save
                </button>
                @endif
                <button type="submit" class="btn btn-info waves-effect waves-light save-and-continue">
                    Save & Continue
                </button>
                <a class="btn btn-default waves-effect tab_back" data-id='{{ $pdt_id }}' href="javascript:void(0);">Back</a>
            </div>
        </div>
    </div>
</form>
<script>
    $(".save-and-continue").on('click', function () {
        $("#submit_action").val('continue');
    });
    $(".save-btn").on('click', function () {
        $("#submit_action").val('save');
    });
    $('.tab_back').on('click', function () {
        var pdt_id = $(this).data("id");
        $.ajax({
            type: "GET",
            url: "{{route('product_tabs')}}",
            data: {'activeTab': 'ATTRIBUTES', pdt_id: pdt_id},
            success: function (result) {
                $('#pdt_tab a[href="#attributes"]').tab('show');
                $('.tab-content').html(result);
            }
        });
    });
    $("#frm_create_stock").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        errorElement: "label",
        errorPlacement: function (error, element) {
            if (element.attr("type") == "checkbox") {
                error.insertAfter("#error_chk");
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "{{route('save_product_stock')}}",
                data: $('#frm_create_stock').serialize(),
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data.status == 1) {
                        if ($("#submit_action").val() == 'continue') {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#pdt_tab a[href="#price"]').tab('show');
                            $('.tab-content').html(data.result);
                        } else
                        {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                        }
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                    }
                    $('button:submit').attr('disabled', false);
                },
                error: function (err) {
                    $('button:submit').attr('disabled', false);
                }
            });
            return false;
        }
    });
    $('input[id^="min_stock_"]').each(function () {
        $(this).rules('add', {
            required: true,
            number: true,
            messages: {
                required: "Min stock is required",
                number: "Numbers only"
            }
        });
    });
    $('input[id^="max_stock_"]').each(function () {
        $(this).rules('add', {
            required: true,
            number: true,
            messages: {
                required: "Max stock is required",
                number: "Numbers only"
            }
        });
    });
    
    $('input[id^="min_stock_var_"]').each(function () {
        $(this).rules('add', {
            required: true,
            number: true,
            messages: {
                required: "Min stock is required",
                number: "Numbers only"
            }
        });
    });
    $('input[id^="max_stock_var_"]').each(function () {
        $(this).rules('add', {
            required: true,
            number: true,
            messages: {
                required: "Max stock is required",
                number: "Numbers only"
            }
        });
    });
    
</script>