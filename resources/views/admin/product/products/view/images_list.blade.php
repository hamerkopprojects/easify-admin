<div class="tab-pane active" id="pdt_info" role="tabpanel">
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                @if(!empty($cover[0]->cover_image))
                <div class="col-md-12 ">
                    <label class="control-label"><strong>Cover image</strong></label>
                </div>
                
                <div class="col-md-12">
                    <img class="imgst" src="{{ !empty($cover[0]->cover_image) ? url('/uploads/'.$cover[0]->cover_image) : ''}}" alt="">
                </div>
                @endif
                <br>
                @if(!empty($prdt[0]->path))
                <div class="col-md-12">
                    <label class="control-label"><strong>Product image</strong></label>
                </div>
                <div class="col-md-12">
                
                    @foreach ($prdt as $photo)

                    <img class="imgst" src="{{url('/uploads/'.$photo->path)}}" alt="">
                    @endforeach
                   
                </div>
                @endif
            </div>

        </div>

<style type="text/css">
    .imgst {
        width: 200px;
        margin-right: 18px;
        margin-bottom: 18px;
        height: 125px;
        transition-duration: 0.5s;
        box-shadow: 0 5px 15px rgba(0, 0, 0, 0.2);
        border-radius: 6px;
        object-fit: cover;
    }
       

    label.control-label {
        font-size: 16px;
        color: #373757;
    }
  

</style>