<form id="frm_create_product" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body">
            <div class="row">
                {{-- @php
                    dd($product)
                @endphp --}}
                <div class="col-md-6">
                    <label class="control-label">SKU</label>
                    <br />
                        {{ $product[0]['sku'] }}
                </div>
                <div class="col-md-6">
                </div>
                <div class="col-md-6">
                    <label class="control-label">Product Name (EN)</label><br>
                    {{ $product[0]['lang'][0]['name'] }}
                </div>
                <div class="col-md-6 text-right">
                    <label class="control-label">Product Name (AR) </label><br>
                    {{ $product[0]['lang'][1]['name'] }}
                </div>
                <div class="col-md-6">
                    <label class="control-label">Category </label><br>
                    {{ $product[0]['category']['lang'][0]['name'] }}
                </div>
                <div class="col-md-6">
                    <label class="control-label">Brand </label><br>
                    {{ $product[0]['brand']['lang'][0]['name'] }}
                </div>
                <div class="col-md-6">
                    <label class="control-label">Product Type</label><br/>
                    {{ $product[0]['product_type'] }}
                        
                </div>
                <div class="col-md-6"></div>
                <div class="col-md-6">
                    <label class="control-label">Description (EN) </label><br>
                    {{ $product[0]['lang'][0]['description'] }}
                   
                </div>
                <div class="col-md-6 text-right">
                    <label class="control-label">Description (AR)</label><br>
                    {{ $product[0]['lang'][1]['description'] }}
                    
                </div>
                {{-- <div class="col-md-6">
                    <label class="control-label">Ingredients & Nutrition facts (EN)</label><br>
                    {{ $product[0]['lang'][0]['ingredients'] }}
                </div>
                <div class="col-md-6 text-right">
                    <label class="control-label">Ingredients & Nutrition facts (AR)</label><br>
                    {{ $product[0]['lang'][1]['ingredients'] }}
                </div>
                <div class="col-md-6">
                    <label class="control-label">How to use (EN)</label><br>
                    {{ $product[0]['lang'][0]['how_to_use'] }}
                </div>
                <div class="col-md-6 text-right">
                    <label class="control-label">How to use (AR)</label><br>
                    {{ $product[0]['lang'][1]['how_to_use'] }}
                </div>
                <div class="col-md-6">
                    <label class="control-label">Reasons to buy (EN)</label><br>
                    {{ $product[0]['lang'][0]['reasons_to_buy'] }}
                </div>
                <div class="col-md-6 text-right">
                    <label class="control-label">Reasons to buy (AR)</label><br>
                    {{ $product[0]['lang'][0]['reasons_to_buy'] }}
                </div> --}}
            </div>
        </div>
        <div class="modal-footer">
           
        </div>
    </div>
</form>

<style>

label.control-label {
    font-size: 16px;
    color: #373757;
}
    </style>
