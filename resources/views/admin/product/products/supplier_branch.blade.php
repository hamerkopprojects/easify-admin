@if($type == 'simple')
<div class="col-md-6">
    <label class="control-label ml-2"><strong>Supplier Stock</strong></label>
    <div class="row">
        <div class="col-md-6 ml-0"><label class="control-label">Min</label><input class="form-control form-white" placeholder="Enter min stock" value="" name="min_stock[]"/></div>
        <div class="col-md-6 ml-0"><label class="control-label">Max</label><input class="form-control form-white sm" placeholder="Enter max stock" value="" name="max_stock[]"/></div>
    </div>
    <input type="hidden" value="{{$supplier_id}}" name="branch_id[]"/>
</div>
@php $i = 1;@endphp
@foreach($supplier_data as $value)
<div class="col-md-6">
    <label class="control-label ml-2"><strong>Branch{{ $i++ }} Stock</strong></label>
    <div class="row">
        <div class="col-md-6 ml-0"><label class="control-label">Min</label><input class="form-control form-white" placeholder="Enter min stock" value="" name="min_stock[]"/></div>
        <div class="col-md-6 ml-0"><label class="control-label">Max</label><input class="form-control form-white sm" placeholder="Enter max stock" value="" name="max_stock[]"/></div>
    </div>
    <input type="hidden" value="{{ $value->supplier_id }}" name="branch_id[]"/>
</div>
@endforeach
@else
@if($variants = \App\Models\ProductVariant::with('variant')->where(['product_id' => $pdt_id])->get())
@foreach($variants as $variant_val)
<div class="col-md-12 mt-3"><h6>{{ $pdt_att_var->lang[0]['name'] ?? '' }} - {{ $variant_val->variant->name ?? '' }}</h6></div>
<div class="col-md-4">
</div>
<div class="col-md-4">
    <label class="control-label"><strong>Min Stock</strong></label>
</div>
<div class="col-md-4">
    <label class="control-label"><strong>Max Stock</strong></label>
</div>
<div class="col-md-4">
    <label class="control-label">{{$supplier_data->name ?? ''}} / {{$supplier_data->code ?? ''}}</label>
</div>
<div class="col-md-4"><input class="form-control form-white" placeholder="Enter min stock" value="" name="min_stock[]"/></div>
<div class="col-md-4"><input class="form-control form-white sm" placeholder="Enter max stock" value="" name="max_stock[]"/></div>
<input type="hidden" value="{{$supplier_id}}" name="branch_id[]"/>
@php $i = 1;@endphp
@foreach($branch_data as $value)
<div class="col-md-4">
    <label class="control-label">{{$value->supplier->name ?? ''}} / {{$value->supplier->code ?? ''}}</label>
</div>
<div class="col-md-4"><input class="form-control form-white" placeholder="Enter min stock" value="" name="min_stock[]"/></div>
<div class="col-md-4"><input class="form-control form-white sm" placeholder="Enter max stock" value="" name="max_stock[]"/></div>
<input type="hidden" value="{{ $value->supplier_id }}" name="branch_id[]"/>
@endforeach
@endforeach
@endif
@endif