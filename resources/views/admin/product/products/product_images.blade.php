<div class="tab-pane active" id="pdt_info" role="tabpanel">
    <div class="modal-body">
        <div class="row">
            <div class="col-md-4">
                <div class="mb-2">Add cover photo</div>
                <div class="cover-photo" id="pdt_cover_image">
                    <div id="cover-photo-upload" class="add">+</div>
                    <div id="cover_photo_loader" class="loader" style="display: none;">
                        <img src="{{ asset('assets/images/loader.gif') }}" alt="">
                    </div>
                    <div class="preview-image-container" @if(empty($cover_image)) style="display:none;" @endif id="cover_photo_image_preview">
                         <div class="scrn-link" style="position: relative;top: -20px;">
                            <button type="button" class="scrn-img-close delete-cover" data-type="cover" data-id="{{ $pdt_id }}">
                                <i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i>
                            </button>
                            <img class="scrn-img" style="max-width: 200px" src="{{ !empty($cover_image) ? url('uploads/'.$cover_image) : '' }}" alt="">
                        </div>
                    </div>
                </div>
                <div class="control-fileupload" style="display: none;">
                    <label for="cover_photo" data-nocap="1">Select cover photo:</label>
                    <input type="file" id="cover_photo" name="cover_photo" data-id="{{ $pdt_id }}" data-imgw="265" data-imgh="205"/>
                </div>
                <div class="mt-2 small">
                    <p>Max file size: 1 MB<br />
                        Supported formats: jpeg, png<br />
                        File dimension: 265 x 205 pixels<br />
                    </p>
                    <span style="display: none;" class="error" id="cover_photo-error">Error</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="mb-2">Product Images</div>
                <div>
                    <div id="photos_loader" class="loader" style="display: none;">
                        <img src="{{ asset('assets/images/loader.gif') }}" alt="">
                    </div>
                    <div class="uploaded-images">
                        @foreach ($pdt_images as $photo)
                        <div class="scrn-crd scrn-link" id='pdt_image_val_{{ $photo->id }}'>
                            <button
                                type="button"
                                class="scrn-img-close delete-pdt_image" data-type="pdt_image" data-id="{{ $photo->id }}">
                                <i class="ti-close"></i>
                            </button>
                            <img class="scrn-img" src="{{ !empty($photo->path) ? url('uploads/'.$photo->path): '' }}" alt="">
                        </div>
                        @endforeach
                        <div class="cover-photo" id="photos-upload">
                            <div class="add">+</div>
                        </div>
                    </div>
                </div>

                <input type="file" id="pdt_image" name="pdt_image" style="display: none;" data-id="{{ $pdt_id }}" data-imgw="800" data-imgh="800"/>
                <div class="mt-2 small">
                    <p>Max file size: 1 MB<br />
                        Supported formats: jpeg, png<br />
                        File dimension: 800 x 800 pixels<br />
                    </p>

                    <span style="display: none;" class="error" id="photos-error"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <input type="hidden" id="pdt_id" name="pdt_id" value="{{ $pdt_id }}">
        <a type="submit" class="btn btn-info waves-effect waves-light save-categorys" href="{{ route('products') }}">Done</a>
        <a class="btn btn-default waves-effect tab_back" data-id='{{ $pdt_id }}' href="javascript:void(0);">Back</a>
    </div>
</div>
<!-- For cropping -->
<div class="modal none-border" id="image-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Crop Image</h5>
                <button type="button" class="close" data-dismiss-modal="modal2" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="crop-images">
                <div class="row">
                    <div class="col-md-6">
                        <img id="cropper" src="" alt="" style="max-width: 100%">
                    </div>
                    <div class="col-md-4">
                        <div class="preview"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="crop" type="button" class="btn btn-primary">Crop & Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss-modal="modal2">Close</button>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    img {
        display: block;
        max-width: 100%;
    }
    .preview {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }
    #formModal { overflow-y:auto; }
</style>
<script>
    $('.tab_back').on('click', function () {
    var pdt_id = $(this).data("id");
    $.ajax({
    type: "GET",
            url: "{{route('product_tabs')}}",
            data: {'activeTab': 'PRICE', pdt_id: pdt_id},
            success: function (result) {
            $('#pdt_tab a[href="#price"]').tab('show');
            $('.tab-content').html(result);
            }
    });
    });
    $('#cover-photo-upload').click(function (e) {
    $('#cover_photo').click();
    });
    $('#cover_photo').on('change', function () {
    var id = $(this).data("id");
    var imgw = $(this).data('imgw');
    var imgh = $(this).data('imgh');
    const file = $(this)[0].files[0];
    img = new Image();
    var imgwidth = 0;
    var imgheight = 0;
    var _URL = window.URL || window.webkitURL;
    img.src = _URL.createObjectURL(file);
    img.onload = function() {
    imgwidth = this.width;
    imgheight = this.height;
    if (imgwidth >= imgw && imgheight >= imgh){
    readUrl(file, 'cover_photo', uploadFile, imgw, imgh, id);
    } else{
    $('input[type="file"]').val('');
    Toast.fire({
    icon: 'error',
    title: 'Image size must be greater or equal to ' + imgw + ' X ' + imgh,
    });
    }
    }
    });
    $('#photos-upload').click(function() {
    $('#pdt_image').click();
    });
    $('#pdt_image').on('change', function() {
    var id = $(this).data("id");
    var imgw = $(this).data('imgw');
    var imgh = $(this).data('imgh');
    const file = $(this)[0].files[0];
    img = new Image();
    var imgwidth = 0;
    var imgheight = 0;
    var _URL = window.URL || window.webkitURL;
    img.src = _URL.createObjectURL(file);
    img.onload = function() {
    imgwidth = this.width;
    imgheight = this.height;
    if (imgwidth >= imgw && imgheight >= imgh){

    readUrl(file, '', uploadPhotos, imgw, imgh, id);
    } else{
    $('input[type="file"]').val('');
    Toast.fire({
    icon: 'error',
    title: 'Image size must be greater or equal to ' +imgw+' X '+imgh,
    });
    }

    }
    });
    function uploadFile(file, type, id) {
    var formData = new FormData();
    formData.append('photo', file);
    formData.append('pdt_id', id);
    formData.append('upload_type', 'single');
    $.ajax({
    type: "POST",
            url: "{{route('upload_product_images')}}",
            dataType: "json",
            data: formData,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            beforeSend: function() {
            $(`#cover_photo_loader`).show();
            $('#image-modal').modal('hide');
            },
            success: function(data) {
            if (data.status == 1) {
            Toast.fire({
            icon: 'success',
                    title: data.message
            });
            $('#pdt_cover_image').html('<div id="cover-photo-upload" class="add">+</div>' +
                    '<div id="cover_photo_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                    '<div class="preview-image-container" id="cover_photo_image_preview">' +
                    '<div class="scrn-link" style="position: relative;top: -20px;">' +
                    '<button type="button" class="scrn-img-close delete-cover" data-type="cover" data-id="' + data.pdt_id + '">' +
                    '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                    '<img class="scrn-img" style="max-width: 200px" src="' + data.path + '" alt="">' +
                    '</div></div>' +
                    '<script>' +
                    '$(".delete-cover").click(function(e) {' +
                    'delete_image(' + data.pdt_id + ' , "cover");' +
                    '});<\/script>');
            $('input[type="file"]').val('');
            } else {
            Toast.fire({
            icon: 'error',
                    title: data.message
            });
            $(`cover_photo_loader`).hide();
            }
            }
    });
    }


    function uploadPhotos(photos, type, id) {

    if (!Array.isArray(photos)) {
    photos = [photos]
    }

    var formData = new FormData();
    for (let i = 0; i < photos.length; i++) {
    formData.append('photos[]', photos[i]);
    }

    formData.append('upload_type', 'multiple');
    formData.append('pdt_id', id);
    $.ajax({
    url: "{{route('upload_product_images')}}",
            type : 'POST',
            data : formData,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            beforeSend: function() {
            $(`#photos_loader`).show();
            $('#image-modal').modal('hide');
            },
            success: function(data) {
            if (data.status == 1) {
            Toast.fire({
            icon: 'success',
                    title: data.message
            });
            $('.uploaded-images').prepend('<div class="scrn-crd scrn-link" id="pdt_image_val_' + data.image_id + '">' +
                    '<button type="button" class="scrn-img-close delete-pdt_image" id="del_image_' + data.image_id + '" data-type="pdt_image" data-id="' + data.image_id + '">' +
                    '<i class="ti-close"></i></button>' +
                    '<img class="scrn-img" src="' + data.path + '" alt=""></div>' +
                    '<script>' +
                    '$("#del_image_' + data.image_id + '").click(function(e) {' +
                    'delete_image(' + data.image_id + ', "pdt_image");' +
                    '});<\/script>');
            $('input[type="file"]').val('');
            $('#photos_loader').hide();
            } else {
            Toast.fire({
            icon: 'error',
                    title: data.message
            });
            $(`#photos_loader`).hide();
            }
            }
    });
    }
    $('.delete-pdt_image').click(function(e) {
    let id = $(this).data('id');
    let type = $(this).data('type');
    delete_image(id, type);
    });
    $('.delete-cover').click(function(e) {
    let id = $(this).data('id');
    let type = $(this).data('type');
    delete_image(id, type);
    });
    
    function delete_image(id, type){
        $.confirm({
            title: '<span class="small">Are you sure to delete this image?</span>',
            content: 'You wont be able to revert this',
            buttons: {
            Yes: function () {
            $.ajax({
            type: "POST",
                    url: "{{route('detete_pdt_img')}}",
                    data: {
                    id: id,
                    type: type
                    },
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    if (data.type == 'cover') {
                    $('#pdt_cover_image').html('<div id="cover-photo-upload" class="add">+</div>' +
                            '<div id="cover_photo_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                            '<div class="preview-image-container" style="display: none;" id="cover_photo_image_preview">' +
                            '<div class="scrn-link" style="position: relative;top: -20px;">' +
                            '<button type="button" class="scrn-img-close" data-type="cover" data-id="' + data.id + '">' +
                            '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                            '<img class="scrn-img" style="max-width: 200px" src="" alt="">' +
                            '</div></div>' +
                            '<script>' +
                            '$("#cover-photo-upload").click(function (e) {' +
                            '$("#cover_photo").click();});<\/script>');
                    } else
                    {
                    $("#pdt_image_val_" + data.id).remove();
                    }
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            },
                    No: function () {
                    console.log('cancelled');
                    }
            }
    });
    }
    
    
    
    $("button[data-dismiss-modal=modal2]").click(function () {
    $('#image-modal').modal('hide');
    });
</script>