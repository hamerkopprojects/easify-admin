@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>PROMO CODES</h1>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
                <div class="col-lg-3">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12 text-right">
                                <a href="javascript:void(0);" id = "add_new_promocode" class="btn btn-info create_btn">
                                    <font style="vertical-align: inherit;"><i class="ti-plus"></i> Add New Promo Code
                                    </font>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
            </div>
            <!-- /# row -->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form id="posts-filter" method="get" action="{{ route('promocode') }}">
                    <div class="row tablenav top text-right">
                        <div class="col-md-8 ml-0">
                            <input class="form-control" type="text" name="search" value="{{$search}}" placeholder="Search by promo code">
                        </div>


                        <div class="col-md-3 text-left">
                            <button type="submit" class="btn btn-info">
                                <font style="vertical-align: inherit;">Search</font>
                            </button>
                            <a href="{{ route('promocode') }}" class="btn btn-default reset_style">Reset</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S. No.</th>
                                <th>Promo Code</th>
                                <th>Discount Type</th>
                                <th>Value</th>


                                <th width="25%"></th>
                            </tr>
                        </thead>
                        <tbody>
                        @if (count($promo) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($promo ?? '' as $row_data)
                            <tr>
                            <tr>
                                <th>{{ $i++ }}</th>
                                <td>{{ $row_data->code }}</td>
                                <td>{{ $row_data->discount_type }}</td>
                                <td>{{ $row_data->value }}</td>

                                <td class="text-left">

                                    <a class="btn btn-sm btn-success text-white edit_btn edit_promo" title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                    <button type="button" class="change-status btn btn-sm btn-toggle ml-0 {{ $row_data['status'] == 'active' ? 'active' : ''}}" data-toggle="button" data-id="{{ $row_data->id }}" data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" aria-pressed="true" autocomplete="off">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>

                                    <a class="btn btn-sm btn-danger text-white" title="Delete Promo code" onclick="deletePromocodes({{ $row_data->id }})"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">

                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
{{-- Pop Up --}}
<div class="modal fade" id="user_popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg popup-promo" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="name_change"><strong>
                        ADD PROMO CODE
                    </strong></h4>
                <a href="{{ route('promocode') }}" class="close">x</a>
            </div>
            <form id="promocode_form" action="javascript:;" enctype="multipart/form-data" method="POST">
                <div class="modal-body">
                    <div class="msg_div"></div>
                    <div class="row">
                        <input type="hidden" name="promocode_unique" id='promocode_unique'>
                        <div class="col-md-6">
                            <label class="control-label">Promo Code  <span class="text-danger">*</span></label>
                            <input class="form-control form-white" placeholder="Enter promo code" type="text" name="code" id="code" />
                            <span style="display: none;" class="error" id="error"></span>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Discount Type  <span class="text-danger">*</span></label>
                            <select class="form-control" onchange="yesnoCheck(this);" name="discount_type" id="discount_type">
                                <option value="amount"> Amount </option>
                                <option value="percentage">Percentage</option>
                            </select>
                            <span style="display: none;" class="error" id="error"></span>
                        </div>
                        <div class="col-md-6">
                            <div id="value-amount">
                                <label class="control-label">Value (SAR)<span class="text-danger">*</span></label>
                                <input class="form-control form-white" placeholder="Enter Valid Amount" type="text" name="valuea" id="valuea" />
                                </div>
                                <div id="value-per" style="display: none;">
                                    <label class="control-label">Value<span class="text-danger">*</span></label>
                                    <input class="form-control form-white" placeholder="Enter Valid Percentage" type="text" name="valueb" id="valueb" />
                                </div>
                        </div>
                        <div class="col-md-8 rulpad">
                            <label class="control-label">RULES</label>

                        </div>
                        <div class="col-md-12">
                            <label for="category" class="control-label">Rules based on</label>
                            <label class="radio-inline">
                                <input type="radio" name="rule_based" checked value="category" class="rule_based">Category
                              </label>
                              <label class="radio-inline">
                                  <input type="radio" name="rule_based" value="product" class="rule_based">Product
                              </label>
                        </div>
                        <div class="col-md-12 ml-0" id="category_based">
                        <div class="col-md-12">
                            <label for="category" class="control-label">Category Type</label><br>
                            <label class="radio-inline">
                                <input type="radio" class="cate" name="optradio" id="racin" checked value="include">Include
                              </label>
                              <label class="radio-inline">
                                <input type="radio" class="cate" name="optradio" id="raex" value="exclude">Exclude
                              </label>
                        </div>
                        <div class="col-md-12" id="cin">
                            <label for="category" class="control-label">Category (Include)</label><br>
                            <select class="js-example-basic-multiple-cin" id="tag-cin" name="tag-cin[]" multiple="multiple">
                                <option value="a">please select</option>
                            
                              </select> 
                        </div>
                        <div class="col-md-12" id="cex" style="display: none;">
                            <label class="control-label">Category (Exclude)</label><br>
                            <select class="js-example-basic-multiple-ce" id="tag-ce" name="tag-ce[]" multiple="multiple">
                                <option value="a">please select</option>
                              </select> 
                        </div>
                        </div>
                        <div class="col-md-12 ml-0" id="product_based" style="display:none">
                        <div class="col-md-12">
                            <label for="category" class="control-label">Product Type</label><br>
                            <label class="radio-inline">
                                <input type="radio" name="opt" class="prod" id="rapin" checked value="include">Include
                              </label>
                              <label class="radio-inline">
                                <input type="radio" name="opt" id="rapex" class="prod" value="exclude">Exclude
                              </label>
                        </div>
                        <div class="col-md-12" id="pin">
                            <label class="control-label">Product (Include)</label><br>
                            <select class="js-example-basic-multiple-pin" id="tag-pin" name="tag-pin[]" multiple="multiple">
                                <option value="a">please select</option>
                              </select> 
                        </div>
                        <div class="col-md-12" id="pex" style="display: none;">
                            <label class="control-label">Product (Exclude)</label><br>
                            <select class="js-example-basic-multiple-pe" id="tag-pe" name="tag-pe[]" multiple="multiple">
                                <option value="a">please select</option>
                              </select>
                        </div>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Maximum no of usage</label>
                            <small><i class="fa fa-info-circle"></i> Unlimited usage</small>  
                            <input class="form-control form-white" placeholder="Unlimited usage" type="text" name="max_num_usage" id="max_num_usage" />
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Maximum no of usage per user</label>
                            <small><i class="fa fa-info-circle"></i> Unlimited usage per user</small>
                            <input class="form-control form-white" placeholder="Unlimited usage" type="text" name="max_num_per_user" id="max_num_per_user" />
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info waves-effect waves-light save-promocodes" id="save_data">
                        ADD
                    </button>
                    <a href="{{ route('promocode') }}" class="btn btn-default reset_style">Cancel</a>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- end popup --}}

@endsection
@push('css')
<style>
    
    .reset_style {
        margin-left: 15px;
    }

    .search_wid {
        width: 234px;
    }

    .btn-fea {
        background-color: #87b23e;
    }

    .validation {
        color: red;

    }

    .popup-promo {
        max-height: 85%;
        margin-top: 50px;
        margin-bottom: 50px;
        max-width: 100%;
    }
    .rulpad{
        margin-top: 30px;
    }
    .tm-tag.tm-tag-info {
    color: #ffffff !important;
    background-color: #fca131 !important;
    border-color: #fca131 !important;
    }
    
    .select2-container--default .select2-selection--multiple {
    width: 100%;
    height: 100%;
    min-height: 45px;
    border: 1px solid rgba(0,0,0,.15) !important;
    border-radius: 0px !important;
    }

    select + .select2-container {
        width: 100% !important;
    }

input[type="radio"] {
  margin-right: 5px;
}
label {
   
    padding-right: 20px !important;
}
</style>
@endpush
@push('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        $('#add_new_promocode').on('click', function() {
        $('#name_change').html('Add Promo Code');
        $('#save_data').text('Add').button("refresh");
        $("#promocode_form")[0].reset();
        $('#user_popup').modal({
            show: true
        });
    })
   
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
// category include



    $('.js-example-basic-multiple-cin').select2({
     
        ajax: {
          url: "{{ route('autocomplete_category') }}",
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results:  $.map(data, function (item) {
                 
                    return {
                        text: item.name,
                        id: item.category_id
                    }
                })
            };
          },
          cache: true
        }
      });

// category exclude

      $('.js-example-basic-multiple-ce').select2({
        ajax: {
          url: "{{ route('autocomplete_category') }}",
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results:  $.map(data, function (item) {
                 
                    return {
                        text: item.name,
                        id: item.category_id
                    }
                })
            };
          },
          cache: true
        }
      });

// product include

      $('.js-example-basic-multiple-pin').select2({
        ajax: {
          url: "{{ route('autocomplete') }}",
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results:  $.map(data, function (item) {
                  
                    return {
                        text: item.name,
                        id: item.product_id
                    }
                })
            };
          },
          cache: true
        }
      });

    //   product exclude

      $('.js-example-basic-multiple-pe').select2({
        ajax: {
          url: "{{ route('autocomplete') }}",
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            return {
              results:  $.map(data, function (item) {
                 
                    return {
                        text: item.name,
                        id: item.product_id
                    }
                })
            };
          },
          cache: true
        }
      });

    // add promocode

   

    $('.save-promocodes').on('click', function(e) {
        
        $.validator.addMethod('minStrict', function (value, el, param) {
        return this.optional( el ) || value >= param && !(value % 1);
    });
    
    $("#promocode_form").validate({
    rules: {
        code: {
            required: true,
        },
        discount_type: {
            required: true,
        },
        valuea: {
            required: true,
            minStrict: 0
        },
        valueb: {
            required: true,
            minStrict: 0,
            range: [0, 100]
        },
        max_num_usage:{
         
            number: true,
            minStrict: 0,
            
        },
        max_num_per_user:{
           
            number: true,
            minStrict: 0,
        }
    },
    messages: {
        code: {
            required: "Promo code required.",
        },
        discount_type: {
            required: "Discount type required.",
        },
        valuea: {
            required: "Value required.",
            minStrict:"Enter correct number."
            
        },
        valueb: {
            required: "Value required.",
            minStrict:"Enter correct number.",
            range:"Enter valid percentage.",
        },
        max_num_usage: {
            number: "Number required.",
            minStrict:"Enter correct number."
        },
        max_num_per_user: {
            number: "Number required.",
            minStrict:"Enter correct number."
        },
    },
    submitHandler: function(form) {
         if(($('#valuea').val()) ==''){
                        value = $('#valueb').val();  
                    }else{
                        value = $('#valuea').val();
                    }
        promocode_unique = $("#promocode_unique").val();
      
       
        if (promocode_unique) {
            $.ajax({
                type: 'POST',
                 data: {
                    category_type: $(".cate:checked").val(),
                    product_type: $(".prod:checked").val(),
                    category_ci_array: $('#tag-cin').val(),
                    category_ce_array: $('#tag-ce').val(),
                    product_pi_array: $('#tag-pin').val(),
                    product_pe_array: $('#tag-pe').val(),
                    code: $('#code').val(),
                    discount_type: $('#discount_type').val(),
                    value: value,
                    max_num_usage: $('#max_num_usage').val(),
                    max_num_per_user: $('#max_num_per_user').val(),
                    promocode_unique: $('#promocode_unique').val(),
                    rule_based: $('.rule_based:checked').val(),
                },
               
                url: "{{route('promocode.update')}}",
                success: function(data) {
                    
                    if (data.status == 1) {
                        Toast.fire({
                            icon: 'success',
                            title: data.message
                        });
                        window.setTimeout(function() {
                            window.location.reload();
                        }, 1000);
                        $("#promocode_form")[0].reset();
                    } else {
                        // console.log(data.message);
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                    }
                }
            });
        }else{
       $.ajax({
                type: 'POST',
                 data: {
                    category_type: $(".cate:checked").val(),
                    product_type: $(".prod:checked").val(),
                    category_ci_array: $('#tag-cin').val(),
                    category_ce_array: $('#tag-ce').val(),
                    product_pi_array: $('#tag-pin').val(),
                    product_pe_array: $('#tag-pe').val(),
                    code: $('#code').val(),
                    discount_type: $('#discount_type').val(),
                    value: value,
                    max_num_usage: $('#max_num_usage').val(),
                    max_num_per_user: $('#max_num_per_user').val(),
                    rule_based: $(".rule_based:checked").val(),
                },
               
                url: "{{route('promocode.store')}}",
                success: function(data) {
                    
                    if (data.status == 1) {
                        Toast.fire({
                            icon: 'success',
                            title: data.message
                        });
                        window.setTimeout(function() {
                            window.location.reload();
                        }, 1000);
                        $("#promocode_form")[0].reset();
                    } else {
                        // console.log(data.message);
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                    }
                }
            });

      }
    }
})

});

$('.edit_promo').on('click', function(e) {
       
       page = $(this).data('id')
       $('#name_change').html('Edit Promo code');
       $('#save_data').text('Save').button("refresh");
       var url = "promocode/edit/";
       $.get(url + page, function(data) {
           if((data.promocode.discount_type) =='amount'){
               document.getElementById("value-amount").style.display = "block";
               document.getElementById("value-per").style.display = "none";
               $('#valuea').val(data.promocode.value)
                   }else{
                       document.getElementById("value-amount").style.display = "none";
                       document.getElementById("value-per").style.display = "block";
                       $('#valueb').val(data.promocode.value)
                   }
           if(data.promocode.rule_based == 'product'){
              $("#product_based").css({'display': 'block'});
              $("#category_based").css({'display': 'none'}); 
           }else{
             $("#category_based").css({'display': 'block'});
             $("#product_based").css({'display': 'none'});   
           }
           $('#code').val(data.promocode.code),
           $('#discount_type').val(data.promocode.discount_type),
           $('#max_num_usage').val(data.promocode.max_num_usage),
           $('#max_num_per_user').val(data.promocode.max_num_per_user),
           $('#promocode_unique').val(data.promocode.id),
           $('input[name=rule_based][value=' + data.promocode.rule_based + ']').prop('checked',true)
           $.each( data.promocode.promocategory, function( key, value ) {
              
               if(value.type == 'include'){
                document.getElementById("cin").style.display = "block";
                document.getElementById("cex").style.display = "none";
                $('input[name=optradio][value=' + value.type + ']').prop('checked',true)
                $('.js-example-basic-multiple-cin').append("<option value='"+value.promotionalcategory.category_id+"' selected>"+value.promotionalcategory.name+"</option>");
                $('.js-example-basic-multiple-cin').trigger('change');
          
              
               }else{
                document.getElementById("cex").style.display = "block";
                document.getElementById("cin").style.display = "none";
                $('input[name=optradio][value=' + value.type + ']').prop('checked',true)
                $('.js-example-basic-multiple-ce').append("<option value='"+value.promotionalcategory.category_id+"' selected>"+value.promotionalcategory.name+"</option>");
                $('.js-example-basic-multiple-ce').trigger('change');
                 
               }
           });
         

           $.each( data.promocode.promoproduct, function( key, value ) {
              
              if(value.type == 'include'){
                document.getElementById("pin").style.display = "block";
                document.getElementById("pex").style.display = "none";
                $('input[name=opt][value=' + value.type + ']').prop('checked',true)
                $('.js-example-basic-multiple-pin').append("<option value='"+value.promotionalproduct.product_id+"' selected>"+value.promotionalproduct.name+"</option>");
                $('.js-example-basic-multiple-pin').trigger('change');
             
              }else{
                document.getElementById("pex").style.display = "block";
                document.getElementById("pin").style.display = "none";
                $('input[name=opt][value=' + value.type + ']').prop('checked',true)
                $('.js-example-basic-multiple-pe').append("<option value='"+value.promotionalproduct.product_id+"' selected>"+value.promotionalproduct.name+"</option>");
                $('.js-example-basic-multiple-pe').trigger('change');
                
              }
          });
          
           $('#user_popup').modal({
               show: true

           });
       });
   });
   $('.change-status').on('click', function() {
        var cust_id = $(this).data("id");
        var act_value = $(this).data("activate");
        $.confirm({
            title: act_value + ' Promo code',
            content: 'Are you sure to ' + act_value + ' the promo code?',
            buttons: {
                Yes: function() {
                    $.ajax({
                        type: "POST",
                        url: "{{route('promocode.status.update')}}",
                        data: {
                            id: cust_id
                        },
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.href = '{{route("promocode")}}';
                                }, 1000);

                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    window.location.reload();
                }
            }
        });
    });
});

function deletePromocodes(id) {
       console.log(id);
        $.confirm({
            title: false,
            content: 'Are you sure to delete this promo code? <br><br>You wont be able to revert this',
            buttons: {
                Yes: function() {
                    $.ajax({
                        type: "POST",
                        url: "{{route('promocode.delete')}}",
                        data: {
                            id: id
                        },
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                window.setTimeout(function() {
                                    window.location.href = '{{route("promocode")}}';
                                }, 1000);
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    console.log('cancelled');
                }
            }
        });
    }
function yesnoCheck(that) {
    if (that.value == "amount") {
  
        document.getElementById("value-amount").style.display = "block";
        document.getElementById("value-per").style.display = "none";
    } else {
        document.getElementById("value-amount").style.display = "none";
        document.getElementById("value-per").style.display = "block";
    }
}  
$(function() {
    
        $('#racin').click(function() {
            
        document.getElementById("cin").style.display = "block";
        document.getElementById("cex").style.display = "none";
        });           
        $('#raex').click(function() {
            
        document.getElementById("cex").style.display = "block";
        document.getElementById("cin").style.display = "none";
        });
        $('#rapin').click(function() {
            
            document.getElementById("pin").style.display = "block";
            document.getElementById("pex").style.display = "none";
            });           
            $('#rapex').click(function() {
                
            document.getElementById("pex").style.display = "block";
            document.getElementById("pin").style.display = "none";
            });
    });
     $('input[type=radio][name=rule_based]').change(function() {
    if (this.value == 'category') {
        $("#category_based").css({'display': 'block'});
        $("#product_based").css({'display': 'none'});
    }
    else {
        $("#product_based").css({'display': 'block'});
        $("#category_based").css({'display': 'none'});
    }
});
</script>
@endpush