@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>BRANDS</h1>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
                <div class="col-lg-3">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12 text-right">
                                <a href="javascript:void(0);" id = "add_new_brand" class="btn btn-info create_btn">
                                    <font style="vertical-align: inherit;"><i class="ti-plus"></i> Add Add New Brand
                                    </font>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
            </div>
            <!-- /# row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <form id="posts-filter" method="get" action="{{ route('brand') }}">
                            <div class="row tablenav top text-right">
                                <div class="col-md-9 ml-0">
                                    <input class="form-control" type="text" name="search" value="{{$search}}" placeholder="Search by Brand ID / Brand Name">
                                </div>
                                <div class="col-md-3 text-left">
                                    <button type="submit" class="btn btn-info">
                                        <font style="vertical-align: inherit;">Search</font>
                                    </button>
                                    <a href="{{ route('brand') }}" class="btn btn-default reset_style">Reset</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div id="msgDiv"></div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>S. No.</th>
                                            <th>Brand ID</th>
                                            <th>Brand Name (EN)</th>
                                            <th class="text-right">Brand Name (AR)</th>
                                            <th width="25%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($brand) > 0)
                                        @php
                                        $i = 1;
                                        @endphp
                                        @foreach ($brand ?? '' as $row_data)
                                        <tr>
                                        <tr>
                                            <th>{{ $i++ }}</th>
                                            <th>{{ $row_data->brand_unique_id ?? '' }}</th>
                                            <td>{{ $row_data->lang[0]->name ?? '' }}</td>
                                            <td class="text-right">{{ $row_data->lang[1]->name ?? '' }}</td>
                                            <td class="text-left">

                                                <a class="btn btn-sm btn-success text-white edit_btn upload_image" title="Edit" data-image-en ="{{ $row_data->lang[0]->image_path}}" data-image-ar ="{{ $row_data->lang[1]->image_path}}"  data-banner-en ="{{ $row_data->lang[0]->banner_image}}" data-banner-ar ="{{ $row_data->lang[1]->banner_image}}"  data-id="{{ $row_data->id}}"><i class="fa fa-camera"></i></a>

                                                <a class="btn btn-sm btn-success text-white edit_btn edit_user" title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>

                                                <button type="button" class="change-status btn btn-sm btn-toggle ml-0 {{ $row_data['status'] == 'active' ? 'active' : ''}}" data-toggle="button" data-id="{{ $row_data->id }}" data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" aria-pressed="true" autocomplete="off">
                                                    <div class="handle" data-toggle="tooltip" data-placement="top" title="{{ $row_data['status'] == 'active' ? 'Deactivate Brand' : 'Activate Brand' }}"></div>
                                                </button>

                                                <a class="btn btn-sm btn-danger text-white" title="Delete Brand" onclick="deleteBrands({{ $row_data->id }})"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="8" class="text-center">No records found!</td>
                                        </tr>
                                        @endif

                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center d-flex justify-content-center mt-3">
                                {{ $brand->appends(request()->input())->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- Pop Up --}}
<div class="modal fade" id="user_popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="name_change"><strong>
                        ADD NEW BRAND
                    </strong></h4>
                <a href="{{ route('brand') }}" class="close">x</a>
            </div>
            <form id="user_form" action="javascript:;" enctype="multipart/form-data" method="POST">
                <div class="modal-body">
                    <div class="msg_div"></div>
                    <div class="row">
                        <input type="hidden" name="user_unique" id='user_unique'>
                        <div class="col-md-6">
                            <label class="control-label">Brand name (EN) <span class="text-danger">*</span></label>
                            <input class="form-control form-white" placeholder="Enter brand name" type="text" name="brand_name_en" id="brand_name_en" />
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Brand name (AR) <span class="text-danger">*</span></label>
                            <input class="form-control form-white text-right" placeholder="أدخل اسم العلامة التجارية" type="text" id="brand_name_ar" name="brand_name_ar" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info waves-effect waves-light save-categorys" id="save_data">
                        ADD
                    </button>
                    <a href="{{ route('brand') }}" class="btn btn-default reset_style">Cancel</a>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- end popup --}}

{{-- Image upload popup --}}
<div class="modal fade" id="imageupload_popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg popup-promo" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="name_change"><strong>
                        ADD BRAND IMAGES
                    </strong></h4>
                <a href="{{ route('brand') }}" class="close">x</a>
            </div>
            <div class="modal-body">
                <div class="msg_div"></div>
                <div class="row">

                    <div class="col-md-6">
                        <label class="control-label">Brand Image (EN) <span class="text-danger">*</span></label>
                        <div class="cover-photo" id="pdt_brden_image">
                            <div id="brden-photo-upload" class="add">+</div>
                            <div id="brden_photo_loader" class="loader" style="display: none;">
                                <img src="{{ asset('assets/images/loader.gif') }}" alt="">
                            </div>
                            <div class="preview-image-container" id="brden_photo_image_preview">
                                <div class="scrn-link" style="position: relative;top: -20px;">
                                    <button type="button" class="scrn-img-close delete-brden" data-type="image_en" id="delete-brden" data-id="">
                                        <i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i>
                                    </button>
                                    <img class="scrn-img" id= "scrn-img-en" style="max-width: 200px" src="" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="control-fileupload" style="display: none;">
                            <label for="brden_photo" data-nocap="1">Select cover photo:</label>
                            <input type="file" id="brden_photo" name="brden_photo" data-id="" data-imgw="182" data-imgh="183"/>
                        </div>
                        <div class="mt-2 small">
                            <p>Max file size: 1 MB<br />
                                Supported formats: jpeg, png<br />
                                File dimension: 182 x 183 pixels<br />
                            </p>
                            <span style="display: none;" class="error" id="brden-photo-error">Error</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">Brand Image (AR) <span class="text-danger">*</span></label>
                        <div class="cover-photo" id="pdt_brdar_image">
                            <div id="brdar-photo-upload" class="add">+</div>
                            <div id="brdar_photo_loader" class="loader" style="display: none;">
                                <img src="{{ asset('assets/images/loader.gif') }}" alt="">
                            </div>
                            <div class="preview-image-container" id="brdar_photo_image_preview">
                                <div class="scrn-link" style="position: relative;top: -20px;">
                                    <button type="button" class="scrn-img-close delete-brdar" data-type="image_ar" id="delete-brdar" data-id="">
                                        <i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i>
                                    </button>
                                    <img class="scrn-img" id= "scrn-img-ar" style="max-width: 200px" src="" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="control-fileupload" style="display: none;">
                            <label for="brdar_photo" data-nocap="1">Select cover photo:</label>
                            <input type="file" id="brdar_photo" name="brdar_photo" data-id="" data-imgw="182" data-imgh="183"/>
                        </div>
                        <div class="mt-2 small">
                            <p>Max file size: 1 MB<br />
                                Supported formats: jpeg,png<br />
                                File dimension: 182 x 183 pixels<br />
                            </p>
                            <span style="display: none;" class="error" id="brdar-photo-error">Error</span>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label class="control-label">Banner Image (EN) <span class="text-danger">*</span></label>
                        <div class="cover-photo" id="pdt_bnren_image">
                            <div id="bnren-photo-upload" class="add">+</div>
                            <div id="bnren_photo_loader" class="loader" style="display: none;">
                                <img src="{{ asset('assets/images/loader.gif') }}" alt="">
                            </div>
                            <div class="preview-image-container" id="bnren_photo_image_preview">
                                <div class="scrn-link" style="position: relative;top: -20px;">
                                    <button type="button" class="scrn-img-close delete-bnren" data-type="banner_en" id="delete-bnren" data-id="">
                                        <i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i>
                                    </button>
                                    <img class="scrn-img" id= "scrn-img-bnren" style="max-width: 200px" src="" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="control-fileupload" style="display: none;">
                            <label for="bnren_photo" data-nocap="1">Select cover photo:</label>
                            <input type="file" id="bnren_photo" name="bnren_photo" data-id="" data-imgw="2732" data-imgh="334"/>
                        </div>
                        <div class="mt-2 small">
                            <p>Max file size: 1 MB<br />
                                Supported formats: jpeg,png<br />
                                File dimension: 2732 x 334 pixels<br />
                            </p>
                            <span style="display: none;" class="error" id="bnren-photo-error">Error</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">Banner Image (AR) <span class="text-danger">*</span></label>
                        <div class="cover-photo" id="pdt_bnrar_image">
                            <div id="bnrar-photo-upload" class="add">+</div>
                            <div id="bnrar_photo_loader" class="loader" style="display: none;">
                                <img src="{{ asset('assets/images/loader.gif') }}" alt="">
                            </div>
                            <div class="preview-image-container" id="bnrar_photo_image_preview">
                                <div class="scrn-link" style="position: relative;top: -20px;">
                                    <button type="button" class="scrn-img-close delete-bnrar" data-type="banner_ar" id="delete-bnrar" data-id="">
                                        <i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i>
                                    </button>
                                    <img class="scrn-img" id= "scrn-img-bnrar" style="max-width: 200px" src="" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="control-fileupload" style="display: none;">
                            <label for="bnrar_photo" data-nocap="1">Select cover photo:</label>
                            <input type="file" id="bnrar_photo" name="bnrar_photo" data-id="" data-imgw="2732" data-imgh="334"/>
                        </div>
                        <div class="mt-2 small">
                            <p>Max file size: 1 MB<br />
                                Supported formats: jpeg,png<br />
                                File dimension: 2732 x 334 pixels<br />
                            </p>
                            <span style="display: none;" class="error" id="bnrar-photo-error">Error</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="{{ route('brand') }}" class="btn btn-info waves-effect waves-light save-categorys">Done</a>
                <a href="{{ route('brand') }}" class="btn btn-default reset_style">Cancel</a>
            </div>
        </div>
    </div>
</div>

{{-- end popup --}}
<!-- For cropping -->
<div class="modal none-border" id="image-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Crop Image</h5>
                <button type="button" class="close" data-dismiss-modal="modal2" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="crop-images">
                <div class="row">
                    <div class="col-md-6">
                        <img id="cropper" src="" alt="" style="max-width: 100%">
                    </div>
                    <div class="col-md-4">
                        <div class="preview"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="crop" type="button" class="btn btn-primary">Crop & Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss-modal="modal2">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<style>
    .reset_style {
        margin-left: 15px;
    }
    .imgup{
        padding-top: 20px;
    }
    .popup-promo {
        max-height: 70%;
        margin-top: 50px;
        margin-bottom: 50px;
        max-width: 60%;
    }
    .preview{
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/lib/cropper/cropper.min.css') }}">
@endpush
@push('scripts')
{{-- Sweet alert --}}
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
{{-- select2 --}}
<script src="{{ asset('assets/js/lib/cropper/cropper.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/cropper/jquery-cropper.min.js') }}"></script>
<script src="{{ asset('assets/js/custom_crop.js') }}"></script>
<script>
                                                    $('#add_new_brand').on('click', function() {
                                                    $('#name_change').html('Add Brand');
                                                    $('#save_data').text('Add').button("refresh");
                                                    $("#user_form")[0].reset();
                                                    $('#user_popup').modal({
                                                    show: true
                                                    });
                                                    })

                                                            $('.upload_image').on('click', function() {
                                                    page = $(this).data('id')
                                                            img_en = $(this).data('image-en')
                                                            img_ar = $(this).data('image-ar')
                                                            banner_en = $(this).data('banner-en')
                                                            banner_ar = $(this).data('banner-ar')


                                                            if (img_en) {
                                                    document.getElementById("brden_photo_image_preview").style.display = "block";
                                                    var uploadsUrl = "<?php echo asset('/uploads') ?>";
                                                    var imgurl = uploadsUrl + '/' + img_en;
                                                    $('#scrn-img-en').attr("src", imgurl);
                                                    $('#brden_photo').data('id', page);
                                                    $('#delete-brden').data('id', page);
                                                    } else{
                                                    document.getElementById("brden_photo_image_preview").style.display = "none";
                                                    $('#brden_photo').data('id', page);
                                                    $('#delete-brden').data('id', page);
                                                    }
                                                    if (img_ar) {
                                                    document.getElementById("brdar_photo_image_preview").style.display = "block";
                                                    var uploadsUrl = "<?php echo asset('/uploads') ?>";
                                                    var imgurl = uploadsUrl + '/' + img_ar;
                                                    $('#scrn-img-ar').attr("src", imgurl);
                                                    $('#brdar_photo').data('id', page);
                                                    $('#delete-brdar').data('id', page);
                                                    } else{
                                                    document.getElementById("brdar_photo_image_preview").style.display = "none";
                                                    $('#brdar_photo').data('id', page);
                                                    $('#delete-brdar').data('id', page);
                                                    }

                                                    if (banner_en) {
                                                    document.getElementById("bnren_photo_image_preview").style.display = "block";
                                                    var uploadsUrl = "<?php echo asset('/uploads') ?>";
                                                    var imgurl = uploadsUrl + '/' + banner_en;
                                                    $('#scrn-img-bnren').attr("src", imgurl);
                                                    $('#bnren_photo').data('id', page);
                                                    $('#delete-bnren').data('id', page);
                                                    } else{
                                                    document.getElementById("bnren_photo_image_preview").style.display = "none";
                                                    $('#bnren_photo').data('id', page);
                                                    $('#delete-bnren').data('id', page);
                                                    }

                                                    if (banner_ar) {
                                                    document.getElementById("bnrar_photo_image_preview").style.display = "block";
                                                    var uploadsUrl = "<?php echo asset('/uploads') ?>";
                                                    var imgurl = uploadsUrl + '/' + banner_ar;
                                                    $('#scrn-img-bnrar').attr("src", imgurl);
                                                    $('#bnrar_photo').data('id', page);
                                                    $('#delete-bnrar').data('id', page);
                                                    } else{
                                                    document.getElementById("bnrar_photo_image_preview").style.display = "none";
                                                    $('#bnrar_photo').data('id', page);
                                                    $('#delete-bnrar').data('id', page);
                                                    }


                                                    $('#imageupload_popup').modal({
                                                    show: true
                                                    });
                                                    })


                                                            $('#brden-photo-upload').click(function (e) {
                                                    $('#brden_photo').click();
                                                    });
                                                    $('#brden_photo').on('change', function () {
                                                    var id = $(this).data("id");
                                                    var imgw = $(this).data('imgw');
                                                    var imgh = $(this).data('imgh');
                                                    const file = $(this)[0].files[0];
                                                    img = new Image();
                                                    var imgwidth = 0;
                                                    var imgheight = 0;
                                                    var _URL = window.URL || window.webkitURL;
                                                    img.src = _URL.createObjectURL(file);
                                                    img.onload = function() {
                                                    imgwidth = this.width;
                                                    imgheight = this.height;
                                                    if (imgwidth >= imgw && imgheight >= imgh){

                                                    // readUrl(file, 'web_image', uploadFile, imgw, imgh, ad_id);
                                                    readUrl(file, 'cover_photo', uploadFile, imgw, imgh, id);
                                                    } else{
                                                    $('input[type="file"]').val('');
                                                    Toast.fire({
                                                    icon: 'error',
                                                            title: 'Image size must be greater or equal to ' + imgw + ' X ' + imgh,
                                                    });
                                                    }
                                                    }
                                                    });
                                                    $('#brdar-photo-upload').click(function (e) {
                                                    $('#brdar_photo').click();
                                                    });
                                                    $('#brdar_photo').on('change', function () {
                                                    var id = $(this).data("id");
                                                    console.log(id);
                                                    var imgw = $(this).data('imgw');
                                                    var imgh = $(this).data('imgh');
                                                    const file = $(this)[0].files[0];
                                                    img = new Image();
                                                    var imgwidth = 0;
                                                    var imgheight = 0;
                                                    var _URL = window.URL || window.webkitURL;
                                                    img.src = _URL.createObjectURL(file);
                                                    img.onload = function() {
                                                    imgwidth = this.width;
                                                    imgheight = this.height;
                                                    if (imgwidth >= imgw && imgheight >= imgh){
                                                    readUrl(file, 'cover_photo', uploadFiles, imgw, imgh, id);
                                                    } else{
                                                    $('input[type="file"]').val('');
                                                    Toast.fire({
                                                    icon: 'error',
                                                            title: 'Image size must be greater or equal to ' + imgw + ' X ' + imgh,
                                                    });
                                                    }
                                                    }
                                                    });
                                                    $('#bnren-photo-upload').click(function (e) {
                                                    $('#bnren_photo').click();
                                                    });
                                                    $('#bnren_photo').on('change', function () {
                                                    var id = $(this).data("id");
                                                    var imgw = $(this).data('imgw');
                                                    var imgh = $(this).data('imgh');
                                                    const file = $(this)[0].files[0];
                                                    img = new Image();
                                                    var imgwidth = 0;
                                                    var imgheight = 0;
                                                    var _URL = window.URL || window.webkitURL;
                                                    img.src = _URL.createObjectURL(file);
                                                    img.onload = function() {
                                                    imgwidth = this.width;
                                                    imgheight = this.height;
                                                    if (imgwidth >= imgw && imgheight >= imgh){
                                                    readUrl(file, 'cover_photo', uploadBanner, imgw, imgh, id);
                                                    } else{
                                                    $('input[type="file"]').val('');
                                                    Toast.fire({
                                                    icon: 'error',
                                                            title: 'Image size must be greater or equal to ' + imgw + ' X ' + imgh,
                                                    });
                                                    }
                                                    }

                                                    });
                                                    $('#bnrar-photo-upload').click(function (e) {
                                                    $('#bnrar_photo').click();
                                                    });
                                                    $('#bnrar_photo').on('change', function () {
                                                    var id = $(this).data("id");
                                                    var imgw = $(this).data('imgw');
                                                    var imgh = $(this).data('imgh');
                                                    const file = $(this)[0].files[0];
                                                    img = new Image();
                                                    var imgwidth = 0;
                                                    var imgheight = 0;
                                                    var _URL = window.URL || window.webkitURL;
                                                    img.src = _URL.createObjectURL(file);
                                                    img.onload = function() {
                                                    imgwidth = this.width;
                                                    imgheight = this.height;
                                                    if (imgwidth >= imgw && imgheight >= imgh){
                                                    readUrl(file, 'cover_photo', uploadBanners, imgw, imgh, id);
                                                    } else{
                                                    $('input[type="file"]').val('');
                                                    Toast.fire({
                                                    icon: 'error',
                                                            title: 'Image size must be greater or equal to ' + imgw + ' X ' + imgh,
                                                    });
                                                    }
                                                    }

                                                    });
                                                    $.ajaxSetup({
                                                    headers: {
                                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                    }
                                                    });
                                                    $('.save-categorys').on('click', function(e) {

                                                    $("#user_form").validate({
                                                    rules: {
                                                    brand_name_en: {
                                                    required: true,
                                                    },
                                                            brand_name_ar: {
                                                            required: true,
                                                            },
                                                    },
                                                            messages: {
                                                            brand_name_en: {
                                                            required: "Brand name (En) required",
                                                            },
                                                                    brand_name_ar: {
                                                                    required: "Brand name (Ar) required",
                                                                    },
                                                            },
                                                            submitHandler: function(form) {
                                                            user_unique = $("#user_unique").val();
                                                            if (user_unique) {

                                                            $.ajax({
                                                            type: 'POST',
                                                                    url: "{{route('brand.update')}}",
                                                                    data: new FormData(form),
                                                                    mimeType: "multipart/form-data",
                                                                    dataType: 'JSON',
                                                                    contentType: false,
                                                                    processData: false,
                                                                    success: function(data) {

                                                                    if (data.status == 1) {
                                                                    Toast.fire({
                                                                    icon: 'success',
                                                                            title: data.message
                                                                    });
                                                                    window.setTimeout(function() {
                                                                    window.location.reload();
                                                                    }, 1000);
                                                                    $("#user_form")[0].reset();
                                                                    } else {
                                                                    Toast.fire({
                                                                    icon: 'error',
                                                                            title: data.message
                                                                    });
                                                                    }
                                                                    }
                                                            });
                                                            } else {
                                                            $.ajax({
                                                            type: 'POST',
                                                                    data: new FormData(form),
                                                                    mimeType: "multipart/form-data",
                                                                    dataType: 'JSON',
                                                                    contentType: false,
                                                                    processData: false,
                                                                    url: "{{route('brand.store')}}",
                                                                    success: function(data) {
                                                                    console.log(data);
                                                                    if (data.status == 1) {
                                                                    Toast.fire({
                                                                    icon: 'success',
                                                                            title: data.message
                                                                    });
                                                                    window.setTimeout(function() {
                                                                    window.location.reload();
                                                                    }, 1000);
                                                                    $("#user_form")[0].reset();
                                                                    } else {
                                                                    // console.log(data.message);
                                                                    Toast.fire({
                                                                    icon: 'error',
                                                                            title: data.message
                                                                    });
                                                                    }
                                                                    }
                                                            });
                                                            }
                                                            }
                                                    })
                                                    });
                                                    $('.edit_user').on('click', function(e) {
                                                    page = $(this).data('id')
                                                            $('#name_change').html('Edit Brand');
                                                    $('#save_data').text('Save').button("refresh");
                                                    var url = "brand/edit/";
                                                    $.get(url + page, function(data) {
                                                    console.log(data.brand.lang[0].image_path);
                                                    $('#brand_name_en').val(data.brand.lang[0].name),
                                                            $('#brand_name_ar').val(data.brand.lang[1].name),
                                                            $('#user_unique').val(data.brand.id)

                                                            $('#user_popup').modal({
                                                    show: true
                                                    });
                                                    });
                                                    });
                                                    function deleteBrands(id) {
                                                    $.confirm({
                                                    title: false,
                                                            content: 'Are you sure to delete this brand? <br><br>You wont be able to revert this',
                                                            buttons: {
                                                            Yes: function() {
                                                            $.ajax({
                                                            type: "POST",
                                                                    url: "{{route('brand.delete')}}",
                                                                    data: {
                                                                    id: id
                                                                    },
                                                                    dataType: "json",
                                                                    headers: {
                                                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                                    },
                                                                    success: function(data) {
                                                                    if (data.status == 1) {
                                                                    window.setTimeout(function() {
                                                                    window.location.href = '{{route("brand")}}';
                                                                    }, 1000);
                                                                    Toast.fire({
                                                                    icon: 'success',
                                                                            title: data.message
                                                                    });
                                                                    } else {
                                                                    Toast.fire({
                                                                    icon: 'error',
                                                                            title: data.message
                                                                    });
                                                                    }
                                                                    }
                                                            });
                                                            },
                                                                    No: function() {
                                                                    console.log('cancelled');
                                                                    }
                                                            }
                                                    });
                                                    }

                                                    $('.change-status').on('click', function() {
                                                    var cust_id = $(this).data("id");
                                                    var act_value = $(this).data("activate");
                                                    $.confirm({
                                                    title: act_value + ' Brand',
                                                            content: 'Are you sure to ' + act_value + ' the brand?',
                                                            buttons: {
                                                            Yes: function() {
                                                            $.ajax({
                                                            type: "POST",
                                                                    url: "{{route('brand.status.update')}}",
                                                                    data: {
                                                                    id: cust_id
                                                                    },
                                                                    dataType: "json",
                                                                    headers: {
                                                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                                    },
                                                                    success: function(data) {
                                                                    if (data.status == 1) {
                                                                    Toast.fire({
                                                                    icon: 'success',
                                                                            title: data.message
                                                                    });
                                                                    window.setTimeout(function() {
                                                                    window.location.href = '{{route("brand")}}';
                                                                    }, 1000);
                                                                    } else {
                                                                    Toast.fire({
                                                                    icon: 'error',
                                                                            title: data.message
                                                                    });
                                                                    }
                                                                    }
                                                            });
                                                            },
                                                                    No: function() {
                                                                    window.location.reload();
                                                                    }
                                                            }
                                                    });
                                                    });
                                                    function uploadFile(file, type, id) {
                                                    var formData = new FormData();
                                                    formData.append('photo', file);
                                                    formData.append('brand_id', id);
                                                    formData.append('upload_type', 'image_en');
                                                    $.ajax({
                                                    type: "POST",
                                                            url: "{{route('upload_brand_images')}}",
                                                            dataType: "json",
                                                            data: formData,
                                                            processData: false, // tell jQuery not to process the data
                                                            contentType: false, // tell jQuery not to set contentType
                                                            beforeSend: function() {
                                                            $(`#brden_photo_loader`).show();
                                                            $('#image-modal').modal('hide');
                                                            },
                                                            success: function(data) {
                                                            if (data.status == 1) {
                                                            Toast.fire({
                                                            icon: 'success',
                                                                    title: data.message
                                                            });
                                                            $('#pdt_brden_image').html('<div id="brden-photo-upload" class="add">+</div>' +
                                                                    '<div id="brden_photo_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                                                                    '<div class="preview-image-container" id="brden_photo_image_preview">' +
                                                                    '<div class="scrn-link" style="position: relative;top: -20px;">' +
                                                                    '<button type="button" class="scrn-img-close delete-brden" data-type="image_en" data-id="' + data.brand_id + '">' +
                                                                    '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                                                                    '<img class="scrn-img" style="max-width: 200px" src="' + data.path + '" alt="">' +
                                                                    '</div></div>' +
                                                                    '<script>' +
                                                                    '$(".delete-brden").click(function(e) {' +
                                                                    'delete_image(' + data.brand_id + ' , "image_en");' +
                                                                    '});<\/script>');
                                                            $('input[type="file"]').val('');
                                                            } else {
                                                            Toast.fire({
                                                            icon: 'error',
                                                                    title: data.message
                                                            });
                                                            $(`brden_photo_loader`).hide();
                                                            }
                                                            }
                                                    });
                                                    }


                                                    $("button[data-dismiss-modal=modal2]").click(function () {
                                                    $('#image-modal').modal('hide');
                                                    });
                                                    $('.delete-brden').click(function(e) {
                                                    let id = $(this).data('id');
                                                    let type = $(this).data('type');
                                                    delete_image(id, type);
                                                    });
                                                    function delete_image(id, type){
                                                    $.confirm({
                                                    title: '<span class="small">Are you sure to delete this image?</span>',
                                                            content: 'You wont be able to revert this',
                                                            buttons: {
                                                            Yes: function () {

                                                            /////////////////////////////////////////////////////////////////////////////////////

                                                            $.ajax({
                                                            url: "{{ route('detete_brand_img') }}",
                                                                    data: {
                                                                    id: id,
                                                                            type: type
                                                                    },
                                                                    type: 'POST',
                                                                    success: function(data) {
                                                                    if (data.status == 1) {
                                                                    Toast.fire({
                                                                    icon: 'success',
                                                                            title: data.message
                                                                    });
                                                                    if (data.type == 'image_en') {

                                                                    $('#pdt_brden_image').html('<div id="brden-photo-upload" class="add">+</div>' +
                                                                            '<div id="brden_photo_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                                                                            '<div class="preview-image-container" style="display: none;" id="brden_photo_image_preview">' +
                                                                            '<div class="scrn-link" style="position: relative;top: -20px;">' +
                                                                            '<button type="button" class="scrn-img-close" data-type="image_en" data-id="' + data.id + '">' +
                                                                            '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                                                                            '<img class="scrn-img" style="max-width: 200px" src="" alt="">' +
                                                                            '</div></div>' +
                                                                            '<script>' +
                                                                            '$("#brden-photo-upload").click(function (e) {' +
                                                                            '$("#brden_photo").click();});<\/script>');
                                                                    } else
                                                                    {
                                                                    $("#pdt_image_val_" + data.id).remove();
                                                                    }
                                                                    } else {
                                                                    Toast.fire({
                                                                    icon: 'error',
                                                                            title: data.message
                                                                    });
                                                                    }
                                                                    }
                                                            });
                                                            },
                                                                    No: function () {
                                                                    console.log('cancelled');
                                                                    }
                                                            }
                                                    });
                                                    }
                                                    $("button[data-dismiss-modal=modal2]").click(function () {
                                                    $('#image-modal').modal('hide');
                                                    });
                                                    function uploadFiles(file, type, id) {
                                                    var formData = new FormData();
                                                    formData.append('photo', file);
                                                    formData.append('brand_id', id);
                                                    formData.append('upload_type', 'image_ar');
                                                    $.ajax({
                                                    type: "POST",
                                                            url: "{{route('upload_brand_images')}}",
                                                            dataType: "json",
                                                            data: formData,
                                                            processData: false, // tell jQuery not to process the data
                                                            contentType: false, // tell jQuery not to set contentType
                                                            beforeSend: function() {
                                                            $(`#brdar_photo_loader`).show();
                                                            $('#image-modal').modal('hide');
                                                            },
                                                            success: function(data) {
                                                            if (data.status == 1) {
                                                            Toast.fire({
                                                            icon: 'success',
                                                                    title: data.message
                                                            });
                                                            $('#pdt_brdar_image').html('<div id="brdar-photo-upload" class="add">+</div>' +
                                                                    '<div id="brdar_photo_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                                                                    '<div class="preview-image-container" id="brdar_photo_image_preview">' +
                                                                    '<div class="scrn-link" style="position: relative;top: -20px;">' +
                                                                    '<button type="button" class="scrn-img-close delete-brdar" data-type="image_ar" data-id="' + data.brand_id + '">' +
                                                                    '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                                                                    '<img class="scrn-img" style="max-width: 200px" src="' + data.path + '" alt="">' +
                                                                    '</div></div>' +
                                                                    '<script>' +
                                                                    '$(".delete-brdar").click(function(e) {' +
                                                                    'delete_images(' + data.brand_id + ' , "image_ar");' +
                                                                    '});<\/script>');
                                                            $('input[type="file"]').val('');
                                                            } else {
                                                            Toast.fire({
                                                            icon: 'error',
                                                                    title: data.message
                                                            });
                                                            $(`brdar_photo_loader`).hide();
                                                            }
                                                            }
                                                    });
                                                    }

                                                    $('.delete-brdar').click(function(e) {

                                                    let id = $(this).data('id');
                                                    let type = $(this).data('type');
                                                    delete_images(id, type);
                                                    });
                                                    function delete_images(id, type){
                                                    $.confirm({
                                                    title: '<span class="small">Are you sure to delete this image?</span>',
                                                            content: 'You wont be able to revert this',
                                                            buttons: {
                                                            Yes: function () {
                                                            $.ajax({
                                                            url: "{{ route('detete_brand_img') }}",
                                                                    data: {
                                                                    id: id,
                                                                            type: type
                                                                    },
                                                                    type: 'POST',
                                                                    success: function(data) {
                                                                    if (data.status == 1) {
                                                                    Toast.fire({
                                                                    icon: 'success',
                                                                            title: data.message
                                                                    });
                                                                    if (data.type == 'image_ar') {

                                                                    $('#pdt_brdar_image').html('<div id="brdar-photo-upload" class="add">+</div>' +
                                                                            '<div id="brdar_photo_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                                                                            '<div class="preview-image-container" style="display: none;" id="brdar_photo_image_preview">' +
                                                                            '<div class="scrn-link" style="position: relative;top: -20px;">' +
                                                                            '<button type="button" class="scrn-img-close" data-type="image_en" data-id="' + data.id + '">' +
                                                                            '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                                                                            '<img class="scrn-img" style="max-width: 200px" src="" alt="">' +
                                                                            '</div></div>' +
                                                                            '<script>' +
                                                                            '$("#brdar-photo-upload").click(function (e) {' +
                                                                            '$("#brdar_photo").click();});<\/script>');
                                                                    } else
                                                                    {
                                                                    $("#pdt_image_val_" + data.id).remove();
                                                                    }
                                                                    } else {
                                                                    Toast.fire({
                                                                    icon: 'error',
                                                                            title: data.message
                                                                    });
                                                                    }
                                                                    }
                                                            });
                                                            },
                                                                    No: function () {
                                                                    console.log('cancelled');
                                                                    }
                                                            }
                                                    });
                                                    }
                                                    $("button[data-dismiss-modal=modal2]").click(function () {
                                                    $('#image-modal').modal('hide');
                                                    });
                                                    function uploadBanner(file, type, id) {

                                                    var formData = new FormData();
                                                    formData.append('photo', file);
                                                    formData.append('brand_id', id);
                                                    formData.append('upload_type', 'banner_en');
                                                    $.ajax({
                                                    type: "POST",
                                                            url: "{{route('upload_brand_images')}}",
                                                            dataType: "json",
                                                            data: formData,
                                                            processData: false, // tell jQuery not to process the data
                                                            contentType: false, // tell jQuery not to set contentType
                                                            beforeSend: function() {
                                                            $(`#bnren_photo_loader`).show();
                                                            $('#image-modal').modal('hide');
                                                            },
                                                            success: function(data) {
                                                            if (data.status == 1) {
                                                            Toast.fire({
                                                            icon: 'success',
                                                                    title: data.message
                                                            });
                                                            $('#pdt_bnren_image').html('<div id="bnren-photo-upload" class="add">+</div>' +
                                                                    '<div id="bnren_photo_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                                                                    '<div class="preview-image-container" id="bnren_photo_image_preview">' +
                                                                    '<div class="scrn-link" style="position: relative;top: -20px;">' +
                                                                    '<button type="button" class="scrn-img-close delete-bnren" data-type="banner_en" data-id="' + data.brand_id + '">' +
                                                                    '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                                                                    '<img class="scrn-img" style="max-width: 200px" src="' + data.path + '" alt="">' +
                                                                    '</div></div>' +
                                                                    '<script>' +
                                                                    '$(".delete-bnren").click(function(e) {' +
                                                                    'delete_banner(' + data.brand_id + ' , "banner_en");' +
                                                                    '});<\/script>');
                                                            $('input[type="file"]').val('');
                                                            } else {
                                                            Toast.fire({
                                                            icon: 'error',
                                                                    title: data.message
                                                            });
                                                            $(`bnren_photo_loader`).hide();
                                                            }
                                                            }
                                                    });
                                                    }
                                                    $('.delete-bnren').click(function(e) {
                                                    let id = $(this).data('id');
                                                    let type = $(this).data('type');
                                                    delete_banner(id, type);
                                                    });
                                                    function delete_banner(id, type){
                                                    $.confirm({
                                                    title: '<span class="small">Are you sure to delete this image?</span>',
                                                            content: 'You wont be able to revert this',
                                                            buttons: {
                                                            Yes: function () {
                                                            $.ajax({
                                                            url: "{{ route('detete_brand_img') }}",
                                                                    data: {
                                                                    id: id,
                                                                            type: type
                                                                    },
                                                                    type: 'POST',
                                                                    success: function(data) {
                                                                    if (data.status == 1) {
                                                                    Toast.fire({
                                                                    icon: 'success',
                                                                            title: data.message
                                                                    });
                                                                    if (data.type == 'banner_en') {

                                                                    $('#pdt_bnren_image').html('<div id="bnren-photo-upload" class="add">+</div>' +
                                                                            '<div id="bnren_photo_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                                                                            '<div class="preview-image-container" style="display: none;" id="bnren_photo_image_preview">' +
                                                                            '<div class="scrn-link" style="position: relative;top: -20px;">' +
                                                                            '<button type="button" class="scrn-img-close" data-type="banner_en" data-id="' + data.id + '">' +
                                                                            '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                                                                            '<img class="scrn-img" style="max-width: 200px" src="" alt="">' +
                                                                            '</div></div>' +
                                                                            '<script>' +
                                                                            '$("#bnren-photo-upload").click(function (e) {' +
                                                                            '$("#bnren_photo").click();});<\/script>');
                                                                    } else
                                                                    {
                                                                    $("#pdt_image_val_" + data.id).remove();
                                                                    }
                                                                    } else {
                                                                    Toast.fire({
                                                                    icon: 'error',
                                                                            title: data.message
                                                                    });
                                                                    }
                                                                    }
                                                            });
                                                            },
                                                                    No: function () {
                                                                    console.log('cancelled');
                                                                    }
                                                            }
                                                    });
                                                    }

                                                    function uploadBanners(file, type, id) {

                                                    var formData = new FormData();
                                                    formData.append('photo', file);
                                                    formData.append('brand_id', id);
                                                    formData.append('upload_type', 'banner_ar');
                                                    $.ajax({
                                                    type: "POST",
                                                            url: "{{route('upload_brand_images')}}",
                                                            dataType: "json",
                                                            data: formData,
                                                            processData: false, // tell jQuery not to process the data
                                                            contentType: false, // tell jQuery not to set contentType
                                                            beforeSend: function() {
                                                            $(`#bnrar_photo_loader`).show();
                                                            $('#image-modal').modal('hide');
                                                            },
                                                            success: function(data) {
                                                            if (data.status == 1) {
                                                            Toast.fire({
                                                            icon: 'success',
                                                                    title: data.message
                                                            });
                                                            $('#pdt_bnrar_image').html('<div id="bnrar-photo-upload" class="add">+</div>' +
                                                                    '<div id="bnrar_photo_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                                                                    '<div class="preview-image-container" id="bnrar_photo_image_preview">' +
                                                                    '<div class="scrn-link" style="position: relative;top: -20px;">' +
                                                                    '<button type="button" class="scrn-img-close delete-bnrar" data-type="banner_ar" data-id="' + data.brand_id + '">' +
                                                                    '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                                                                    '<img class="scrn-img" style="max-width: 200px" src="' + data.path + '" alt="">' +
                                                                    '</div></div>' +
                                                                    '<script>' +
                                                                    '$(".delete-bnrar").click(function(e) {' +
                                                                    'delete_banners(' + data.brand_id + ' , "banner_ar");' +
                                                                    '});<\/script>');
                                                            $('input[type="file"]').val('');
                                                            } else {
                                                            Toast.fire({
                                                            icon: 'error',
                                                                    title: data.message
                                                            });
                                                            $(`bnrar_photo_loader`).hide();
                                                            }
                                                            }
                                                    });
                                                    }

                                                    $('.delete-bnrar').click(function(e) {
                                                    let id = $(this).data('id');
                                                    let type = $(this).data('type');
                                                    delete_banners(id, type);
                                                    });
                                                    function delete_banners(id, type){
                                                    $.confirm({
                                                    title: '<span class="small">Are you sure to delete this image?</span>',
                                                            content: 'You wont be able to revert this',
                                                            buttons: {
                                                            Yes: function () {
                                                            $.ajax({
                                                            url: "{{ route('detete_brand_img') }}",
                                                                    data: {
                                                                    id: id,
                                                                            type: type
                                                                    },
                                                                    type: 'POST',
                                                                    success: function(data) {
                                                                    if (data.status == 1) {
                                                                    Toast.fire({
                                                                    icon: 'success',
                                                                            title: data.message
                                                                    });
                                                                    if (data.type == 'banner_ar') {

                                                                    $('#pdt_bnrar_image').html('<div id="bnrar-photo-upload" class="add">+</div>' +
                                                                            '<div id="bnrar_photo_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                                                                            '<div class="preview-image-container" style="display: none;" id="bnrar_photo_image_preview">' +
                                                                            '<div class="scrn-link" style="position: relative;top: -20px;">' +
                                                                            '<button type="button" class="scrn-img-close" data-type="banner_ar" data-id="' + data.id + '">' +
                                                                            '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                                                                            '<img class="scrn-img" style="max-width: 200px" src="" alt="">' +
                                                                            '</div></div>' +
                                                                            '<script>' +
                                                                            '$("#bnrar-photo-upload").click(function (e) {' +
                                                                            '$("#bnrar_photo").click();});<\/script>');
                                                                    } else
                                                                    {
                                                                    $("#pdt_image_val_" + data.id).remove();
                                                                    }
                                                                    } else {
                                                                    Toast.fire({
                                                                    icon: 'error',
                                                                            title: data.message
                                                                    });
                                                                    }
                                                                    }
                                                            });
                                                            },
                                                                    No: function () {
                                                                    console.log('cancelled');
                                                                    }
                                                            }
                                                    });
                                                    }
</script>
@endpush