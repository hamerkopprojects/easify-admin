<div class="modal-header">
    <h4 class="modal-title"><strong>
            {{( isset($row_data['id']) && $row_data['id'] != '' ) ? 'Edit' : 'Add' }}
            {{ $address_type == 'D'  ? 'Delivery' : 'Billing' }}
            Address
        </strong></h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <div class="msg_div"></div>
    <div class="row">
        <div class="col-md-12">
            <label class="control-label">Apartment Name <span class="text-danger">*</span></label>
            <input class="form-control form-white" placeholder="Enter apartment name" type="text" name="apartment_name" value="{{ isset($row_data['id']) ? $row_data['apartment_name'] : ''}}"/>
        </div>
        <div class="col-md-6">
            <label class="control-label">Street Name <span class="text-danger">*</span></label>
            <input class="form-control form-white" placeholder="Enter street name" type="text" name="street_name" value="{{ isset($row_data['id']) ? $row_data['street_name'] : ''}}"/>
        </div>
        <div class="col-md-6">
            <label class="control-label">Postal Code <span class="text-danger">*</span></label>
            <input class="form-control form-white" placeholder="Enter postal code" type="text" name="postal_code" value="{{ isset($row_data['id']) ? $row_data['postal_code'] : ''}}"/>
        </div>
        <div class="col-md-6">
            <label class="control-label">Region <span class="text-danger">*</span></label>
            <select class="form-control" name="region">
                <option value=""> Select Region </option>
                 @foreach($region as $region_val)
                <option value="{{ $region_val->id }}" {{ (isset($row_data['id']) && $region_val->id == $row_data['region']) ? 'selected' : '' }}>{{ $region_val->reg_name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-6"></div>
        @if($address_type == 'D')
        <div class="col-md-6" style="{{ $d_count == 0  ? 'display:none' : 'display:block'}}">
            <input type="checkbox" name="is_default" id="is_default" value="Y"
                   {{ isset($row_data['id']) && $row_data['is_default'] == 'Y' || $d_count == 0 ? 'checked="checked"' : ''}}>
                   <label class="control-label ml-1"> Make this address as default</label>
        </div>
        @else
        <div class="col-md-6" style="{{ $b_count == 0  ? 'display:none' : 'display:block'}}">
            <input type="checkbox" name="is_default" id="is_default" value="Y"
                   {{ isset($row_data['id']) && $row_data['is_default'] == 'Y' || $b_count == 0 ? 'checked="checked"' : ''}}>
                   <label class="control-label ml-1"> Make this address as default</label>
        </div>
        @endif
    </div>
</div>
<div class="modal-footer">
    <input type="hidden" name="_address_id" value="@if(isset($row_data['id'])) {{ $row_data['id'] }}@endif">
    <input type="hidden" name="address_type" value="{{ $address_type }}">
    <input type="hidden" name="cust_id" value="{{ $cust_id }}">
    <button type="submit" class="btn btn-info waves-effect waves-light save-categorys">
        {{( isset($row_data['id']) && $row_data['id'] != '' ) ? 'Update' : 'Create' }}
    </button>
    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
</div>