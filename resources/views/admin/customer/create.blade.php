<div class="modal-header">
    <h4 class="modal-title"><strong>
            {{( isset($row_data['id']) && $row_data['id'] != '' ) ? 'Edit' : 'Add' }}
            Customer
        </strong></h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <div class="msg_div"></div>
    <div class="row">
        <div class="col-md-6">
            <label class="control-label">Name <span class="text-danger">*</span></label>
            <input class="form-control form-white" placeholder="Enter name" type="text" name="cust_name" value="{{ isset($row_data['id']) ? $row_data['cust_name'] : ''}}"/>
        </div>
        <div class="col-md-6">
            <label class="control-label">Business Type</label>
            <select class="form-control" name="business_type">
                <option value=""> Select Business Type</option>
                @foreach($business_type as $business)
                <option value="{{ $business->id }}" {{ (isset($row_data['id']) && $business->id == $row_data['business_type']) ? 'selected' : '' }}>{{ $business->lang[0]->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-6">
            <label class="control-label">Business Name <span class="text-danger">*</span></label>
            <input class="form-control form-white" placeholder="Enter business name" type="text" name="business_name" value="{{ isset($row_data['id']) ? $row_data['cust_name'] : ''}}"/>
        </div>
        <div class="col-md-6">
            <label class="control-label">Email <span class="text-danger">*</span></label>
            <input class="form-control form-white" placeholder="Enter email address" type="email" name="email" value="{{ isset($row_data['id']) ? $row_data['email'] : ''}}"/>
        </div>
        <div class="col-md-6">
            <label class="control-label">Phone <span class="text-danger">*</span></label>
            <input class="form-control form-white" placeholder="Enter phone number" type="text" name="phone" value="{{ isset($row_data['id']) ? $row_data['phone'] : ''}}"/>
        </div>
        <div class="col-md-6">
            <label class="control-label">Date of birth <span class="text-danger">*</span></label>
            <input class="form-control form-white datepicker" placeholder="Select date of birth" type="text" name="dob" value="{{ isset($row_data['id']) ? $row_data['dob'] : ''}}" autocomplete="off"/>
        </div>
        <div class="col-md-6">
            <label class="control-label">City <span class="text-danger">*</span></label>
            <select class="form-control" name="city" onchange="getRegion(this);">
                <option value=""> Select City </option>
                @foreach($city as $city_val)
                <option value="{{ $city_val->id }}" {{ (isset($row_data['id']) && $city_val->id == $row_data['city']) ? 'selected' : '' }}>{{ $city_val->lang[0]->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-6">
            <label class="control-label">Region <span class="text-danger">*</span></label>
            <select class="form-control" name="region" id="region">
                <option value=""> Select Region </option>
                @foreach($region as $region_val)
                <option value="{{ $region_val->id }}" {{ (isset($row_data['id']) && $region_val->id == $row_data['region']) ? 'selected' : '' }}>{{ $region_val->lang[0]->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="modal-footer">
    <input type="hidden" id="_customer_id" name="_customer_id" value="@if(isset($row_data['id'])) {{ $row_data['id'] }}@endif">
    <button type="submit" class="btn btn-info waves-effect waves-light save-categorys">
        {{( isset($row_data['id']) && $row_data['id'] != '' ) ? 'Update' : 'Create' }}
    </button>
    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
</div>
<script>
    $('input[name="dob"]').daterangepicker({
        "singleDatePicker": true,
        "autoUpdateInput": false,
        "showDropdowns": true,
        "autoApply": true,
         "drops": 'up',
        locale: {
            format: 'DD-MM-YYYY'
        }
    });
    $('input[name="dob"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });
    function getRegion(sel)
{ 
    var city_id = sel.value;
    $.ajax({
            type: "GET",
            url: "{{route('region.get_all')}}",
            data: {'city_id': city_id},
            success: function (data) {
                var options = "";
                options += '<option value=""> Select Region </option>';
                for (var i = 0; i < data.length; i++) {
                    options += '<option value='+ data[i].id +'>' + data[i].lang[0].name + "</option>";
                }
                $("#region").html(options);
            }
        });
}
</script>