@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>CUSTOMERS</h1>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
                <div class="col-lg-3">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12 text-right">
                                <a href="javascript:void(0);" class="btn btn-info create_btn">
                                    <font style="vertical-align: inherit;"><i class="ti-plus"></i> Add New Customer
                                    </font>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
            </div>
            <!-- /# row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form id="posts-filter" method="get" action="{{ route('customer') }}">
                                <div class="row tablenav top text-right">
                                    <div class="col-md-5 ml-0">
                                        <input class="form-control" type="text" name="search" value="{{ $search }}" placeholder="Search by Name / Email / Phone / Customer ID">
                                    </div>
                                    <div class="col-md-3 text-left">
                                        <button type="submit" class="btn btn-info">
                                            <font style="vertical-align: inherit;">Search</font>
                                        </button>
                                        <a href="{{ route('customer') }}" class="btn btn-default">Reset</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div id="msgDiv"></div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>S. No.</th>
                                            <th>Name</th>
                                            <th>Customer ID</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th width="25%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($customer) > 0)
                                        @php
                                        $i = 1;
                                        @endphp
                                        @foreach ($customer as $row_data)
                                        <tr>
                                            <th>{{ $i++ }}</th>
                                            <td>{{ $row_data->cust_name }}</td>
                                            <td>{{ $row_data->cust_id }}</td>
                                            <td>{{ $row_data->email }}</td>
                                            <td>{{ $row_data->phone }}</td>
                                            <td class="text-center">
                                                <button type="button" class="change-status btn btn-sm btn-toggle ml-0 {{ $row_data['status'] == 'Y' ? 'active' : ''}}" data-toggle="button" data-id="{{ $row_data->id }}" data-activate="{{ $row_data['status'] == 'Y' ? 'Deactivate' : 'Activate' }}" aria-pressed="true" autocomplete="off">
                                                    <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                                </button>
                                                <a href="{{ route('customer_details', ['id' => $row_data->id]) }}" class="btn btn-sm btn-info text-white view_btn" title="View"><i class="fa fa-eye"></i></a>
                                                <a class="btn btn-sm btn-success text-white edit_btn" title="Edit Customer" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                                <a class="btn btn-sm btn-danger text-white" title="Delete Customer" onclick="deleteCustomer({{ $row_data->id }})"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="8" class="text-center">No records found!</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center d-flex justify-content-center mt-3">
                                {{ $customer->appends(request()->input())->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal pooup -->
<div class="modal fade none-border" id="formModal">
    <div class="modal-dialog modal-lg">
        <form id="frm_create_customer" action="javascript:;" method="POST">
            <div class="modal-content">

            </div>
        </form>
    </div>
</div>
<!-- END MODAL -->
@endsection
@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endpush
@push('scripts')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
    $.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $('.create_btn').on('click', function() {
    $.ajax({
    type: "GET",
    url: "{{route('create_customer')}}",
    success: function(data) {
    $('.modal-content').html(data);
    $('#formModal').modal('show');
    }
    });
    });
    $('.edit_btn').on('click', function() {
    var customer_id = $(this).data("id");
    $.ajax({
    type: "GET",
    url: "{{route('create_customer')}}",
    data: {
    'id': customer_id
    },
    success: function(data) {
    $('.modal-content').html(data);
    $('#formModal').modal('show');
    }
    });
    });
    $("#frm_create_customer").validate({
    normalizer: function(value) {
    return $.trim(value);
    },
    rules: {
    cust_name: {
    required: true,
    },
        business_name: {
        required: true,
        },
        email: {
        required: true,
                email: true,
        },
        phone: {
        required: true,
                number: true,
                minlength: 6, // will count space
                maxlength: 12
        },
        dob: {
        required: true,
                validDate:true,
        },
        region: {
        required: true,
        },
        city: {
        required: true,
        },
    },
    messages: {
    cust_name: {
    required: 'Name is required.'
    },
        business_name: {
        required: 'Business name is required.'
        },
        email: {
        required: 'Email is required.',
                email: 'Invalid email',
        },
        phone: {
        required: 'Phone number is required.',
                number: 'Invalid phone number.'
        },
        dob: {
        required: 'DOB is required.',
        },
        region: {
        required: 'Region is required.',
        },
        city: {
        required: 'City is required.',
        }
    },
    submitHandler: function(form) {
    $.ajax({
    type: "POST",
        url: "{{route('save_customer')}}",
        data: $('#frm_create_customer').serialize(),
        dataType: "json",
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
        if (data.status == 1) {
        Toast.fire({
        icon: 'success',
                title: data.message
        });
        window.setTimeout(function () {
        window.location.href = '{{route("customer")}}';
        }, 1000);
        $("#frm_create_customer")[0].reset();
        } else {
        Toast.fire({
        icon: 'error',
                title: data.message
        });
        }
        $('button:submit').attr('disabled', false);
        },
        error: function(err) {
        $('button:submit').attr('disabled', false);
        }
    });
    return false;
    }
    });
    function deleteCustomer(id) {
    $.confirm({
    title: '<span class="small">Are you sure to delete this customer?</span>',
    content: 'You wont be able to revert this',
    buttons: {
    Yes: function() {
    $.ajax({
    type: "POST",
        url: "{{route('delete_customer')}}",
        data: {
        id: id
        },
        dataType: "json",
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
        if (data.status == 1) {
        window.setTimeout(function() {
        window.location.href = '{{route("customer")}}';
        }, 1000);
        Toast.fire({
        icon: 'success',
                title: data.message
        });
        } else {
        Toast.fire({
        icon: 'error',
                title: data.message
        });
        }
        }
    });
    },
        No: function() {
        console.log('cancelled');
        }
    }
    });
    }
    $('.change-status').on('click', function() {
    var cust_id = $(this).data("id");
    var act_value = $(this).data("activate");
    $.confirm({
    title: act_value + ' Customer',
    content: 'Are you sure to ' + act_value + ' the customer?',
    buttons: {
    Yes: function() {
    $.ajax({
    type: "POST",
        url: "{{route('activate_customer')}}",
        data: {
        id: cust_id
        },
        dataType: "json",
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
        if (data.status == 1) {
        Toast.fire({
        icon: 'success',
                title: data.message
        });
        window.setTimeout(function() {
        window.location.href = '{{route("customer")}}';
        }, 1000);
        //                    window.location.reload();
        } else {
        Toast.fire({
        icon: 'error',
                title: data.message
        });
        }
        }
    });
    },
        No: function() {
        window.location.reload();
        }
    }
    });
    });
    $.validator.addMethod("validDate", function(value, element) {
    return this.optional(element) || moment(value, "DD-MM-YYYY").isValid();
    }, "Please enter a valid date in the format DD-MM-YYYY");
</script>
@endpush