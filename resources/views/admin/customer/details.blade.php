
@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>CUSTOMER DETAILS</h1>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
                <div class="col-lg-3">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12 text-right">
                                <a href="{{ route('customer') }}">
                                    <font style="vertical-align: inherit;">Back
                                    </font></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
            </div>
            <!-- /# row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div id="msgDiv"></div>
                            <div class="row">
                                <div class="col-md-3 ml-0">
                                    <label class="control-label"><strong>Customer Name </strong></label><br />
                                    {{ $cust_data->cust_name }}
                                </div>
                                <div class="col-md-3 ml-0">
                                    <label class="control-label"><strong>Business Type </strong></label><br />
                                    {{ $cust_data->business_type }}
                                </div>
                                <div class="col-md-3 ml-0">
                                    <label class="control-label"><strong>Business Name </strong></label><br />
                                    {{ $cust_data->business_name }}
                                </div>
                                <div class="col-md-3 ml-0">
                                    <label class="control-label"><strong>Email </strong></label><br />
                                    {{ $cust_data->email }}
                                </div>
                                <div class="col-md-3 ml-0">
                                    <label class="control-label"><strong>Phone </strong></label><br />
                                    {{ $cust_data->phone }}
                                </div>
                                <div class="col-md-3 ml-0">
                                    <label class="control-label"><strong>City </strong></label><br />
                                    {{ $cust_data->customer_city->lang[0]->name ?? '' }}
                                </div>
                                <div class="col-md-3 ml-0">
                                    <label class="control-label"><strong>Region </strong></label><br />
                                    {{ $cust_data->customer_region->lang[0]->name ?? '' }}
                                </div>
                                <div class="col-md-3 ml-0">
                                    <label class="control-label"><strong>Date of birth </strong></label><br />
                                    {{ $cust_data->dob }}
                                </div>
                                <div class="col-md-3 ml-0">
                                    <label class="control-label"><strong>Customer ID </strong></label><br />
                                    {{ $cust_data->cust_id }}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-8"><label class="control-label"><strong>Delivery Address </strong></label></div>
                                <div class="col-md-4 text-right">
                                    <button class="btn btn-sm btn-primary buttonxl create_btn" data-type="D">
                                        <i class="ti-plus"></i>{{ __('frontend.add_nw_dly_adr') }}
                                    </button>
                                </div>
                                @if (count($delivery_address) > 0)
                                @foreach ($delivery_address as $d_address)
                                <div class="col-md-4 ml-0">
                                    @if ($d_address->is_default == 'Y')
                                    <div class="text-right">
                                        <span class="badge-secondary small text-left">Default Address</span>
                                        <a class="ml-3 edit_btn" data-type="D" title="Edit Address" data-id="{{ $d_address->id }}"><i class="fa fa-edit"></i></a>
                                    </div>
                                    @else
                                    <div class="text-right">
                                        <a class="deault_btn" title="Make this address as default" data-type="D" data-id="{{ $d_address->id }}"><i class="fa fa-star"></i></a>
                                        <a class="ml-3 edit_btn" data-type="D" title="Edit Address" data-id="{{ $d_address->id }}"><i class="fa fa-edit"></i></a>
                                        <a class="ml-3" title="Delete Customer" onclick="deleteAddress({{ $d_address->id }})"><i class="fa fa-trash"></i></a></div>
                                    @endif
                                    <div class="text-left">{{ $d_address->apartment_name }}</div>
                                    <div class="text-left">{{ $d_address->postal_code }}</div>
                                    <div class="text-left">{{ $d_address->street_name }}</div>
                                    <div class="text-left">{{ $d_address->reg_name }}</div>
                                </div>
                                @endforeach
                                @endif
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-8"><label class="control-label"><strong>Billing Address </strong></label></div>
                                <div class="col-md-4 text-right">
                                    <button class="btn btn-sm btn-primary buttonxl create_btn" data-type="B">
                                        <i class="ti-plus"></i> {{__('frontend.add_nw_bil_adr')}}
                                    </button>
                                </div>
                                @if (count($billing_address) > 0)
                                @foreach ($billing_address as $b_address)
                                <div class="col-md-4 ml-0">
                                    @if ($b_address->is_default == 'Y')
                                    <div class="text-right">
                                        <span class="badge-secondary small text-left">Default Address</span>
                                        <a class="ml-3 edit_btn" data-type="B" title="Edit Address" data-id="{{ $b_address->id }}"><i class="fa fa-edit"></i></a>
                                    </div>
                                    @else
                                    <div class="text-right">
                                        <a class="deault_btn" title="Make this address as default" data-type="B" data-id="{{ $b_address->id }}"><i class="fa fa-star"></i></a>
                                        <a class="ml-3 edit_btn" data-type="B" title="Edit Address" data-id="{{ $b_address->id }}"><i class="fa fa-edit"></i></a>
                                        <a class="ml-3" title="Delete address" onclick="deleteAddress({{ $b_address->id }})"><i class="fa fa-trash"></i></a></div>
                                    @endif
                                    <div class="text-left">{{ $b_address->apartment_name }}</div>
                                    <div class="text-left">{{ $b_address->postal_code }}</div>
                                    <div class="text-left">{{ $b_address->street_name }}</div>
                                    <div class="text-left">{{ $b_address->reg_name }}</div>
                                </div>
                                @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal pooup -->
<div class="modal fade none-border" id="formModal">
    <div class="modal-dialog modal-lg">
        <form id="frm_create_address" action="javascript:;" method="POST">
            <div class="modal-content">

            </div>
        </form>
    </div>
</div>
<!-- END MODAL -->
@endsection
@push('scripts')
<script>
    $.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $('.create_btn').on('click', function() {
    var type = $(this).data("type");
    var cust_id = {{$cust_id}};
    $.ajax({
    type: "GET",
            url: "{{route('create_address')}}",
            data: {
            'type': type,
                    'cust_id': cust_id
            },
            success: function(data) {
            $('.modal-content').html(data);
            $('#formModal').modal('show');
            }
    });
    });
    $('.edit_btn').on('click', function() {
    var id = $(this).data("id");
    var type = $(this).data("type");
    var cust_id = {{ $cust_id }};
    $.ajax({
    type: "GET",
            url: "{{route('create_address')}}",
            data: {
            'id': id,
                    'type': type,
                    'cust_id': cust_id
            },
            success: function(data) {
            $('.modal-content').html(data);
            $('#formModal').modal('show');
            }
    });
    });
    $('.deault_btn').on('click', function() {
    var id = $(this).data("id");
    var type = $(this).data("type");
    var cust_id = {{$cust_id}};
    $.ajax({
    type: "GET",
            url: "{{route('set_default')}}",
            data: {
            'id': id,
                    'type': type,
                    'cust_id': cust_id
            },
            success: function(data) {
            if (data.status == 1) {
            Toast.fire({
            icon: 'success',
                    title: data.message
            });
            window.setTimeout(function() {
            window.location.reload();
            }, 1000);
            } else {
            Toast.fire({
            icon: 'error',
                    title: data.message
            });
            }
            }
    });
    });
    $("#frm_create_address").validate({
    normalizer: function(value) {
    return $.trim(value);
    },
            rules: {
            apartment_name: {
            required: true,
            },
                    street_name: {
                    required: true,
                    },
                    postal_code: {
                    required: true,
                            number: true,
                    },
                    region: {
                    required: true,
                    }
            },
            messages: {
            apartment_name: {
            required: 'Apartment Name is required.'
            },
                    street_name: {
                    required: 'Street Name is required.',
                    },
                    postal_code: {
                    required: 'Postal code is required.',
                            number: 'Invalid postal code.'
                    },
                    region: {
                    required: 'Region code is required.',
                    }
            },
            submitHandler: function(form) {
            $.ajax({
            type: "POST",
                    url: "{{route('save_address')}}",
                    data: $('#frm_create_address').serialize(),
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    window.setTimeout(function() {
                    window.location.reload();
                    }, 1000);
                    $("#frm_create_address")[0].reset();
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    $('button:submit').attr('disabled', false);
                    },
                    error: function(err) {
                    $('button:submit').attr('disabled', false);
                    }
            });
            return false;
            }
    });
    function deleteAddress(id) {
    $.confirm({
    title: false,
            content: 'Are you want to delete this address?',
            buttons: {
            Yes: function() {
            $.ajax({
            type: "POST",
                    url: "{{route('delete_address')}}",
                    data: {
                    id: id
                    },
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    window.setTimeout(function() {
                    window.location.reload();
                    }, 1000);
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            },
                    No: function() {
                    console.log('cancelled');
                    }
            }
    });
    }
</script>
@endpush