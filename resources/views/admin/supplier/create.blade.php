<div class="modal-header">
    <h4 class="modal-title"><strong>
            {{( isset($row_data['id']) && $row_data['id'] != '' ) ? 'Edit' : 'Add' }}
            Supplier
        </strong></h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <div class="msg_div"></div>
    <div class="row">
        <div class="col-md-6">
            <label class="control-label">Supplier Name <span class="text-danger">*</span></label>
            <input class="form-control form-white" placeholder="Enter supplier name" type="text" name="name" value="{{ isset($row_data['id']) ? $row_data['name'] : ''}}"/>
        </div>
        <div class="col-md-6">
            <label class="control-label">Contact Person Name <span class="text-danger">*</span></label>
            <input class="form-control form-white" placeholder="Enter contact person name" type="text" name="contact_person_name" value="{{ isset($row_data['id']) ? $row_data['contact_person_name'] : ''}}"/>
        </div>
        <div class="col-md-6">
            <label class="control-label">Email <span class="text-danger">*</span></label>
            <input class="form-control form-white" placeholder="Enter email address" type="email" name="email" value="{{ isset($row_data['id']) ? $row_data['email'] : ''}}"/>
        </div>
        <div class="col-md-6">
            <label class="control-label">Phone <span class="text-danger">*</span></label>
            <input class="form-control form-white" placeholder="Enter phone number" type="text" name="phone" value="{{ isset($row_data['id']) ? $row_data['phone'] : ''}}"/>
        </div>
        <div class="col-md-6">
            <label class="control-label">City <span class="text-danger">*</span></label>
            <select class="form-control" name="city_id" onchange="getRegion(this);">
                <option value=""> Select City </option>
                @foreach($city as $city_val)
                <option value="{{ $city_val->id }}" {{ (isset($row_data['id']) && $city_val->id == $row_data['city_id']) ? 'selected' : '' }}>{{ $city_val->lang[0]->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-6">
            <label class="control-label">Region <span class="text-danger">*</span></label>
            <select class="form-control" name="region_id" id="region">
                <option value=""> Select Region </option>
                @if(isset($row_data['id']))
                @foreach($region as $region_val)
                <option value="{{ $region_val->id }}" {{ (isset($row_data['id']) && $region_val->id == $row_data['region_id']) ? 'selected' : '' }}>{{ $region_val->lang[0]->name }}</option>
                @endforeach
                @endif
            </select>
        </div>
        <div class="col-md-6">
            <label class="control-label"> Commision Category<span class="text-danger">*</span></label>
            <select class="form-control" name="commision" id="commision">
                <option value=""> Select commision category </option>
            
                @foreach($commision as $singlecommision)
                <option value="{{ $singlecommision->id }}" {{ (isset($row_data->supplier_annex->commision_category_id) && $singlecommision->id == $row_data->supplier_annex->commision_category_id) ? 'selected' : '' }}>{{ $singlecommision->name }} - {{$singlecommision->value}}</option>
                @endforeach
             
            </select>
        </div>
        <div class="col-md-6">
            <label class="control-label">CR Copy </label>
            <div class ="imgup">
                <div class="scrn-link" >
                    @php
                       $crr_cpy = $row_data->supplier_annex->cr_copy ?? ''; 
                    @endphp
                    @if(!empty($crr_cpy))
                        <button type="button" class="scrn-img-close delete-img" data-id="" id="scn" onclick="removesrc_en()" data-type="app_image" style="display: block;">
                            <i class="ti-close"></i>
                        </button>
                    @else
                        <button type="button" class="scrn-img-close delete-img" data-id="" id="scn" onclick="removesrc_en()" data-type="app_image" style="display: none;">
                            <i class="ti-close"></i>
                        </button>
                    @endif
                   
                    <img id="previewimage_en" class="cover-photo" width="200" onclick="$('#uploadFile_en').click();" src="{{ !empty($crr_cpy) ? url('/uploads/'.$row_data->supplier_annex->cr_copy) :asset('assets/images/upload.png') }}" />
                </div>
                <input type="file" id="uploadFile_en" name="product_image_en" style="visibility: hidden;" accept="image/*" value="{{!empty($crr_cpy) ? url('/uploads/'.$row_data->supplier_annex->cr_copy) : ""}} " />
                <input type="hidden" name="hidden_image_en" id="hidden_image_en">
                 <input type="hidden" name="my_file" id="my_file">
            </div>
            <span class="error"></span>
            <div class="catimg">
                <p class="small">Supported formats: jpeg,png,pdf</p>
            </div>
            <p id="display_image" class="mt-3"></p>
        </div>
    </div>
</div>
<div class="modal-footer">
    <input type="hidden" id="_customer_id" name="_supplier_id" value="@if(isset($row_data['id'])) {{ $row_data['id'] }}@endif">
    <button type="submit" class="btn btn-info waves-effect waves-light save-categorys">
        {{( isset($row_data['id']) && $row_data['id'] != '' ) ? 'Update' : 'Create' }}
    </button>
    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
</div>
<script>
    function removesrc_en(){
        document.getElementById("scn").style.display = "none";
        $('#previewimage_en').attr('src', '{{ asset('assets/images/upload.png') }}');
        $('#hidden_image_en').remove();
        $("#uploadFile_en").val("");
    };
    document.getElementById("uploadFile_en").onchange = function(e) {
    var focusSet = false;
    var reader = new FileReader();
    var fileUpload = document.getElementById("uploadFile_en");
    var imgPath = fileUpload.value;
    var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
    if (fileUpload != '') {
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.jpeg|.pdf)$");
    var file_size = $('#uploadFile_en')[0].files[0].size;
    if (regex.test(fileUpload.value.toLowerCase()) == '') {
    $("#uploadFile_en").parent().next(".validation").remove(); // remove it
    $('#previewimage_en').attr('src', "{{ asset('assets/images/upload.png') }}");
    $('#hidden_image_en').remove();
    $("#uploadFile_en").val("");
    $("#uploadFile_en").parent().after("<div class='validation' style='color:red;font-size: 12px;  font-weight: 400;'>The image must be of the format jpeg png or pdf </div>");
    }
    //            else if(file_size > 512000) {
    //                $("#uploadFile_en").parent().next(".validation").remove(); // remove it
    //                $('#previewimage_en').attr('src', '{{ asset('assets/images/upload.png') }}');
    //                $('#hidden_image_en').remove();
    //                $("#uploadFile_en").val("");
    //                $("#uploadFile_en").parent().after("<div class='validation' style='color:red;font-size: 12px;  font-weight: 400;'>The image must be less than 512Kb in size </div>");
    //            }
    else if (fileUpload != ''){
    $("#uploadFile_en").parent().next(".validation").remove(); // remove it

    var fileUpload = document.getElementById("uploadFile_en");
    var reader = new FileReader();
    //Read the contents of Image File.
    reader.readAsDataURL(fileUpload.files[0]);
    reader.onload = function (e) {

    //Initiate the JavaScript Image object.
    var image = new Image();
    //Set the Base64 string return from FileReader as source.
    image.src = e.target.result;
    //Validate the File Height and Width.
    image.onload = function () {
    var height = this.height;
    var width = this.width;
    $("#uploadFile_en").parent().next(".validation").remove(); // remove it
    var reader = new FileReader();
   $('#my_file').val($('#uploadFile_en')[0].files[0]);
    document.getElementById("scn").style.display = "block";
    $("#display_image").text('');
    document.getElementById("previewimage_en").src = e.target.result;
    reader.readAsDataURL(fileUpload.files[0]);
    }
    if (extn == "pdf") {
    $('#preview_img').attr('src', '');
    $("#display_image").text(fileUpload.files[0]['name']);
    }
    // $.toaster({
    // priority : 'success',
    //         title    : 'Success',
    //         message  : "File uploaded successfully",
    //         timeout  : 3000,
    // });
    }

    }
    else{
    $("#uploadFile_en").parent().next(".validation").remove(); // remove it
        var reader = new FileReader();
        reader.onload = function(e) {
        document.getElementById("scn").style.display = "block";
        document.getElementById("previewimage_en").src = e.target.result;
        };
        reader.readAsDataURL(this.files[0]);
    }

    } else{
    $("#uploadFile_en").parent().next(".validation").remove(); // remove it

    }
    };
</script>