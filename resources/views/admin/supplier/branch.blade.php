@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>BRANCH</h1>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 ml-0">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12 text-right">
                                <a href="{{ route('admin.supplier') }}">Back to list</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12 text-right">
                                <a href="javascript:void(0);" class="btn btn-info create_btn" supplier_id="{{$supplier_id}}">
                                    <font style="vertical-align: inherit;"><i class="ti-plus"></i> Add New Branch
                                    </font>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item"> <a class="nav-link" href="{{ route('admin.supplier_details', ['id' => $supplier_id]) }}" role="tab"><span class="hidden-sm-up"><i class="ti-receipt"></i></span> <span class="hidden-xs-down">SUPPLIER DETAILS</span></a> </li>
                        <li class="nav-item disabled"> <a class="nav-link active" href="{{ route('admin.branch', ['id' => $supplier_id]) }}" role="tab"><span class="hidden-sm-up"><i class="ti-receipt"></i></span> <span class="hidden-xs-down">BRANCH</span></a> </li>
                    </ul>

                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div id="msgDiv"></div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>S. No.</th>
                                    <th width="10%">Branch Name</th>
                                    <th>Branch Code</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Region</th>
                                    <th width="20%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($branch_data) > 0)
                                @php
                                $i = 1;
                                @endphp
                                @foreach ($branch_data as $row_data)
                                <tr>
                                    <th>{{ $i++ }}</th>
                                    <td>{{ $row_data->supplier->name }}</td>
                                    <td>{{ $row_data->supplier->code}}</td>
                                    <td>{{ $row_data->supplier->email ?? '' }}</td>
                                    <td>{{$row_data->supplier->phone?? '' }}</td>
                                    <td>{{$row_data->supplier->region->name ?? '' }}</td>
                                    <td class="text-center">
                                        <button type="button" class="change-status btn btn-sm btn-toggle ml-0 {{ $row_data->supplier->status== 'active' ? 'active' : ''}}" data-toggle="button" data-id="{{ $row_data->supplier->id }}" data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" aria-pressed="true" autocomplete="off">
                                            <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                        </button>
                                        <a class="btn btn-sm btn-success text-white edit_btn" title="Edit Branch" data-id="{{ $row_data->supplier->id }}" supplier_id="{{$supplier_id}}"><i class="fa fa-edit"></i></a>
                                        <a class="btn btn-sm btn-danger text-white" title="Delete Branch" onclick="deleteBranch({{ $row_data->supplier->id }})"><i class="fa fa-trash"></i></a>
                                        <a class="btn btn-sm btn-danger text-white password_send" title="Send password" data-id="{{ $row_data->supplier->id }}"><i class="fa fa-envelope"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="8" class="text-center">No records found!</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal pooup -->
<div class="modal fade none-border" id="formModal">
    <div class="modal-dialog modal-lg">
        <form id="frm_create_branch" action="javascript:;" method="POST">
            <div class="modal-content">

            </div>
        </form>
    </div>
</div>
<!-- END MODAL -->
@endsection
@push('scripts')
<script>
    $.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $('.create_btn').on('click', function() {
    var supplier_id = $(this).attr("supplier_id");
    $.ajax({
    type: "GET",
            url: "{{route('admin.create_branch')}}",
            data: {
            'supplier_id': supplier_id
            },
            success: function(data) {
            $('.modal-content').html(data);
            $('#formModal').modal('show');
            }
    });
    });
    $('.edit_btn').on('click', function() {
    var supplier_id = $(this).attr("supplier_id");
    var branch_id = $(this).data("id");
    $.ajax({
    type: "GET",
            url: "{{route('admin.create_branch')}}",
            data: {
            'id': branch_id, 'supplier_id': supplier_id
            },
            success: function(data) {
            $('.modal-content').html(data);
            $('#formModal').modal('show');
            }
    });
    });
    $("#frm_create_branch").validate({
    normalizer: function(value) {
    return $.trim(value);
    },
            rules: {
            name: {
            required: true,
            },
                    contact_person_name: {
                    required: true,
                    },
                    email: {
                    required: true,
                            email: true,
                    },
                    phone: {
                    required: true,
                            number: true,
                            minlength: 6, // will count space
                            maxlength: 12
                    },
                    region_id: {
                    required: true,
                    },
                    city_id: {
                    required: true,
                    },
            },
            messages: {
            name: {
            required: 'Name is required.'
            },
                    contact_person_name: {
                    required: 'Contact person name is required.'
                    },
                    email: {
                    required: 'Email is required.',
                            email: 'Invalid email',
                    },
                    phone: {
                    required: 'Phone number is required.',
                            number: 'Invalid phone number.'
                    },
                    region_id: {
                    required: 'Region is required.',
                    },
                    city_id: {
                    required: 'City is required.',
                    }
            },
            submitHandler: function(form) {
            $.ajax({
            type: "POST",
                    url: "{{route('admin.save_branch')}}",
                    data: $('#frm_create_branch').serialize(),
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    window.setTimeout(function () {
                    window.location.reload();
                    }, 1000);
                    $("#frm_create_branch")[0].reset();
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    $('button:submit').attr('disabled', false);
                    },
                    error: function(err) {
                    $('button:submit').attr('disabled', false);
                    }
            });
            return false;
            }
    });
    function deleteBranch(id) {
    $.confirm({
    title: '<span class="small">Are you sure to delete this branch?</span>',
            content: 'You wont be able to revert this',
            buttons: {
            Yes: function() {
            $.ajax({
            type: "POST",
                    url: "{{route('admin.delete_supplier')}}",
                    data: {
                    id: id
                    },
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                    if (data.status == 1) {
                    window.setTimeout(function() {
                    window.location.reload();
                    }, 1000);
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            },
                    No: function() {
                    console.log('cancelled');
                    }
            }
    });
    }
    $('.change-status').on('click', function() {
    var supplier_id = $(this).data("id");
    var act_value = $(this).data("activate");
    $.confirm({
    title: act_value + ' Branch',
            content: 'Are you sure to ' + act_value + ' the branch?',
            buttons: {
            Yes: function() {
            $.ajax({
            type: "POST",
                    url: "{{route('admin.activate_supplier')}}",
                    data: {
                    id: supplier_id
                    },
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    window.setTimeout(function() {
                    window.location.reload();
                    }, 1000);
                    //                    window.location.reload();
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            },
                    No: function() {
                    window.location.reload();
                    }
            }
    });
    });
    function getRegion(sel)
    {
    var city_id = sel.value;
    $.ajax({
    type: "GET",
            url: "{{route('region.get_all')}}",
            data: {'city_id': city_id},
            success: function (data) {
            var options = "";
            options += '<option value=""> Select Region </option>';
            for (var i = 0; i < data.length; i++) {
            options += '<option value=' + data[i].id + '>' + data[i].lang[0].name + "</option>";
            }
            $("#region").html(options);
            }
    });
    }

    $('.password_send').on('click', function(e) {
    e.preventDefault();
    var id = $(this).data("id");
    $.confirm({
    title: 'Confirmation',
            content: 'Are you sure you want to send password?',
            buttons: {
            Yes: function() {


            $.ajax({
            url: "{{route('supplier.send_password')}}",
                    type: 'POST',
                    data: {
                    id: id, from: 'branch'
                    },
                    success: function(data) {

                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    window.setTimeout(function() {
                    window.location.reload();
                    }, 1000);
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            },
                    No: function() {
                    window.location.reload();
                    }
            }
    });
    });
</script>
@endpush