@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>SUPPLIER DETAILS</h1>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 ml-0">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12 text-right">
                                <a href="{{ route('admin.supplier') }}">Back to list</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item"> <a class="nav-link active" href="{{ route('admin.supplier_details', ['id' => $supplier_id]) }}" role="tab"><span class="hidden-sm-up"><i class="ti-receipt"></i></span> <span class="hidden-xs-down">SUPPLIER DETAILS</span></a> </li>
                        <li class="nav-item disabled"> <a class="nav-link" href="{{ route('admin.branch', ['id' => $supplier_id]) }}" role="tab"><span class="hidden-sm-up"><i class="ti-receipt"></i></span> <span class="hidden-xs-down">BRANCH</span></a> </li>
                    </ul>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="control-label"><strong>Supplier Code</strong></label><br>
                                    <p> {{$supplier_data->code ?? ''}}</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>Supplier Name</strong></label><br>
                                    <p> {{$supplier_data->name ?? ''}}</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>Contact Person Name</strong></label><br>
                                    <p> {{$supplier_data->contact_person_name ?? ''}}</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>Email</strong></label><br>
                                    <p> {{$supplier_data->email ?? ''}}</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>Phone</strong></label><br>
                                    <p> {{$supplier_data->phone ?? ''}}</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>City</strong></label><br>
                                    <p> {{$supplier_data->city->name ?? ''}}</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>Region</strong></label><br>
                                    <p> {{$supplier_data->region->name ?? ''}}</p>
                                </div>
                                <div class="col-md-8"></div>
                                @if(!empty($supplier_data->supplier_annex->cr_copy))
                                @php 
                                $infoPath = pathinfo(public_path('/uploads/'.$supplier_data->supplier_annex->cr_copy));
                                $extension = $infoPath['extension']; 
                                @endphp
                                <div class="col-md-4">
                                <label class="control-label"><strong>CR Copy</strong></label><br>
                                @if($extension == 'pdf')
                                <iframe src="{{ url('/uploads/'.$supplier_data->supplier_annex->cr_copy) }}" frameborder="0" style="width:100%;min-height:400px;"></iframe>
                                @else
                                <img class="imgst cr-copy" data-url="{{ url('/uploads/'.$supplier_data->supplier_annex->cr_copy) }}"  src="{{ !empty($supplier_data->supplier_annex->cr_copy) ? url('/uploads/'.$supplier_data->supplier_annex->cr_copy) : ''}}" alt="">
                                @endif
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal pooup -->
<div class="modal fade none-border" id="formModal">
    <div class="modal-dialog modal-lg">
        <form id="frm_create_supplier" action="javascript:;" method="POST">
            <div class="modal-content">

            </div>
        </form>
    </div>
</div>
<!-- END MODAL -->
@endsection
<style type="text/css">
    .imgst {
        width: 200px;
        margin-right: 18px;
        margin-bottom: 18px;
        height: 125px;
        transition-duration: 0.5s;
        box-shadow: 0 5px 15px rgba(0, 0, 0, 0.2);
        border-radius: 6px;
        object-fit: cover;
    }


    label.control-label {
        font-size: 16px;
        color: #373757;
    }


</style>
@push('scripts')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.cr-copy').click(function (e) {
        let url = $(this).data('url')

        if (url != '') {
            window.open(url);
        }
    });
</script>
@endpush