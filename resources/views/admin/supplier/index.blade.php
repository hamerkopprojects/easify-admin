@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>SUPPLIERS</h1>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 ml-0">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12 text-right">
                                <a href="javascript:void(0);" class="btn btn-info create_btn">
                                    <font style="vertical-align: inherit;"><i class="ti-plus"></i> Add New Supplier
                                    </font>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form id="posts-filter" method="get" action="{{ route('admin.supplier') }}">
                                <div class="row tablenav top text-right">
                                    <div class="col-md-8 ml-0">
                                        <input class="form-control" type="text" name="search" value="{{ $search }}" placeholder="Search by Name / Email / Phone / Supplier Code">
                                    </div>


                                    <div class="col-md-3 text-left">
                                        <button type="submit" class="btn btn-info">
                                            <font style="vertical-align: inherit;">Search</font>
                                        </button>
                                        <a href="{{ route('admin.supplier') }}" class="btn btn-default reset_style">Reset</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div id="msgDiv"></div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>S. No.</th>
                                    <th width="10%">Supplier Name</th>
                                    <th>Supplier Code</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($supplier_list) > 0)
                                @php
                                $i = 1;
                                @endphp
                                @foreach ($supplier_list as $row_data)
                                <tr>
                                    <th>{{ $i++ }}</th>
                                    <td>{{ $row_data->name }}</td>
                                    <td>{{ $row_data->code}}</td>
                                    <td>{{ $row_data->email ?? '' }}</td>
                                    <td>{{$row_data->phone }}</td>
                                    <td class="text-center">
                                        <button type="button" class="change-status btn btn-sm btn-toggle ml-0 {{ $row_data['status'] == 'active' ? 'active' : ''}}" data-toggle="button" data-id="{{ $row_data->id }}" data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" aria-pressed="true" autocomplete="off">
                                            <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                        </button>
                                        <a href="{{ route('admin.supplier_details', ['id' => $row_data->id]) }}" class="btn btn-sm btn-success text-white view_btn" title="Supplier Details"><i class="fa fa-eye"></i></a>
                                        <a class="btn btn-sm btn-success text-white edit_btn" title="Edit Supplier" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                        <a class="btn btn-sm btn-danger text-white" title="Delete Supplier" onclick="deleteSupplier({{ $row_data->id }})"><i class="fa fa-trash"></i></a>
                                        <a class="btn btn-sm btn-danger text-white password_send" title="Send password" data-id="{{ $row_data->id }}"><i class="fa fa-envelope"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="8" class="text-center">No records found!</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal pooup -->
<div class="modal fade none-border" id="formModal">
    <div class="modal-dialog modal-lg">
        <form id="frm_create_supplier" action="javascript:;" method="POST" enctype="multipart/form-data">
            <div class="modal-content">

            </div>
        </form>
    </div>
</div>
<!-- END MODAL -->
@endsection
@push('scripts')
<script>
    $.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $('.create_btn').on('click', function() {
    $.ajax({
    type: "GET",
            url: "{{route('admin.create_supplier')}}",
            success: function(data) {
            $('.modal-content').html(data);
            $('#formModal').modal('show');
            }
    });
    });
    $('.edit_btn').on('click', function() {
    var supplier_id = $(this).data("id");
    $.ajax({
    type: "GET",
            url: "{{route('admin.create_supplier')}}",
            data: {
            'id': supplier_id
            },
            success: function(data) {
            $('.modal-content').html(data);
            $('#formModal').modal('show');
            }
    });
    });
    $("#frm_create_supplier").validate({
    normalizer: function(value) {
    return $.trim(value);
    },
            rules: {
            name: {
            required: true,
            },
                    contact_person_name: {
                    required: true,
                    },
                    email: {
                    required: true,
                            email: true,
                    },
                    phone: {
                    required: true,
                            number: true,
                            minlength: 6, // will count space
                            maxlength: 12
                    },
                    city_id: {
                    required: true,
                    },
                    region_id: {
                    required: true,
                    },
                    commision:{
                        required:true,
                    },
            },
            messages: {
            name: {
            required: 'Supplier name is required.'
            },
                    contact_person_name: {
                    required: 'Contact person name is required.'
                    },
                    email: {
                    required: 'Email is required.',
                    email: 'Invalid email',
                    },
                    phone: {
                    required: 'Phone number is required.',
                    number: 'Invalid phone number.',
                    maxlength: 'Please enter no more than 12 digits'
                    },
                    city_id: {
                    required: 'City is required.',
                    },
                    region_id: {
                    required: 'Region is required.',
                    },
                    commision:{
                        required:"Commision category is required",
                    },
            },
            submitHandler: function(form) {
                var fd = new FormData($('#frm_create_supplier')[0]); 
                 
            $.ajax({
            type: "POST",
                    url: "{{route('admin.save_supplier')}}",
                    data:fd,
                    processData: false,
                    contentType: false,
                    //  $('#frm_create_supplier').serialize() ,
                    // dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    window.setTimeout(function () {
                    window.location.href = '{{route("admin.supplier")}}';
                    }, 1000);
                    $("#frm_create_supplier")[0].reset();
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    $('button:submit').attr('disabled', false);
                    },
                    error: function(err) {
                    $('button:submit').attr('disabled', false);
                    }
            });
            return false;
            }
    });
    function deleteSupplier(id) {
    $.confirm({
    title: '<span class="small">Are you sure to delete this supplier?</span>',
            content: 'You wont be able to revert this',
            buttons: {
            Yes: function() {
            $.ajax({
            type: "POST",
                    url: "{{route('admin.delete_supplier')}}",
                    data: {
                    id: id
                    },
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                    if (data.status == 1) {
                    window.setTimeout(function() {
                    window.location.href = '{{route("admin.supplier")}}';
                    }, 1000);
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            },
                    No: function() {
                    console.log('cancelled');
                    }
            }
    });
    }
    $('.change-status').on('click', function() {
    var supplier_id = $(this).data("id");
    var act_value = $(this).data("activate");
    $.confirm({
    title: act_value + ' Supplier',
            content: 'Are you sure to ' + act_value + ' the supplier?',
            buttons: {
            Yes: function() {
            $.ajax({
            type: "POST",
                    url: "{{route('admin.activate_supplier')}}",
                    data: {
                    id: supplier_id
                    },
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    window.setTimeout(function() {
                    window.location.href = '{{route("admin.supplier")}}';
                    }, 1000);
                    //                    window.location.reload();
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            },
                    No: function() {
                    window.location.reload();
                    }
            }
    });
    });
    
    $('.password_send').on('click', function(e) {
        e.preventDefault();
        var id = $(this).data("id");
        $.confirm({
            title: 'Confirmation',
            content: 'Are you sure you want to send password?',
            buttons: {
                Yes: function() {
           
                    
                    $.ajax({
                        url: "{{route('supplier.send_password')}}",
                        type: 'POST',
                        data: {
                            id: id
                        },
                        success: function(data) {
                           
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.href = '{{route("admin.supplier")}}';
                                }, 1000);

                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                
                },
         
                No: function() {
                    window.location.reload();
                }
            }
        });
    });
    
function getRegion(sel)
{ 
    var city_id = sel.value;
    $.ajax({
            type: "GET",
            url: "{{route('region.get_all')}}",
            data: {'city_id': city_id},
            success: function (data) {
                var options = "";
                options += '<option value=""> Select Region </option>';
                for (var i = 0; i < data.length; i++) {
                    options += '<option value='+ data[i].id +'>' + data[i].lang[0].name + "</option>";
                }
                $("#region").html(options);
            }
        });
}
</script>
@endpush