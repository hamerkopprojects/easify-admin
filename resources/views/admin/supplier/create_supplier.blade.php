<div class="modal-header">
    <h4 class="modal-title"><strong>
            {{( isset($row_data['id']) && $row_data['id'] != '' ) ? 'Edit' : 'Add' }}
            Branch
        </strong></h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <div class="msg_div"></div>
    <div class="row">
        <div class="col-md-6">
            <label class="control-label">Branch Name <span class="text-danger">*</span></label>
            <input class="form-control form-white" placeholder="Enter supplier name" type="text" name="name" value="{{ isset($row_data['id']) ? $row_data['name'] : ''}}"/>
        </div>
        <div class="col-md-6">
            <label class="control-label">Contact Person Name <span class="text-danger">*</span></label>
            <input class="form-control form-white" placeholder="Enter contact person name" type="text" name="contact_person_name" value="{{ isset($row_data['id']) ? $row_data['contact_person_name'] : ''}}"/>
        </div>
        <div class="col-md-6">
            <label class="control-label">Email <span class="text-danger">*</span></label>
            <input class="form-control form-white" placeholder="Enter email address" type="email" name="email" value="{{ isset($row_data['id']) ? $row_data['email'] : ''}}"/>
        </div>
        <div class="col-md-6">
            <label class="control-label">Phone <span class="text-danger">*</span></label>
            <input class="form-control form-white" placeholder="Enter phone number" type="text" name="phone" value="{{ isset($row_data['id']) ? $row_data['phone'] : ''}}"/>
        </div>
        <div class="col-md-6">
            <label class="control-label">City <span class="text-danger">*</span></label>
            <select class="form-control" name="city_id" onchange="getRegion(this);">
                <option value=""> Select City </option>
                @foreach($city as $city_val)
                <option value="{{ $city_val->id }}" {{ (isset($row_data['id']) && $city_val->id == $row_data['city_id']) ? 'selected' : '' }}>{{ $city_val->lang[0]->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-6">
            <label class="control-label">Region <span class="text-danger">*</span></label>
            <select class="form-control" name="region_id" id="region">
                <option value=""> Select Region </option>
                @if(isset($row_data['id']))
                @foreach($region as $region_val)
                <option value="{{ $region_val->id }}" {{ (isset($row_data['id']) && $region_val->id == $row_data['region_id']) ? 'selected' : '' }}>{{ $region_val->lang[0]->name }}</option>
                @endforeach
                @endif
            </select>
        </div>
    </div>
</div>
<div class="modal-footer">
    <input type="hidden" id="_customer_id" name="_branch_id" value="@if(isset($row_data['id'])) {{ $row_data['id'] }}@endif">
    <input type="hidden" id="_customer_id" name="supplier_id" value="{{$supplier_id}}">
    <button type="submit" class="btn btn-info waves-effect waves-light save-categorys">
        {{( isset($row_data['id']) && $row_data['id'] != '' ) ? 'Update' : 'Create' }}
    </button>
    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
</div>