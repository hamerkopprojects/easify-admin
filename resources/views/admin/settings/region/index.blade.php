@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>REGIONS</h1>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
                <div class="col-lg-3">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12 text-right">
                                <a href="javascript:void(0);" class="btn btn-info create_btn" id="add_new_region">
                                    <font style="vertical-align: inherit;"><i class="ti-plus"></i> Add New Region
                                    </font>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
            </div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
            <form action="{{route('region.get')}}" method="get">
                <div class="row tablenav top text-right">
                    <div class="col-md-8 ml-0">
                        {{-- <select name="cancellation_search" id="cancellation_search" placeholder="select cancellation reason"class="form-control search_val">
                                @foreach ($reason as $item)
                                <option value="{{$item->cancellation_id}}">{{$item->name}}</option>
                        @endforeach
                        </select> --}}
                        <input type="text" name="search_region" id="search_region" value="{{$search}}" placeholder="Search region" class="form-control search_val">
                        <input type="hidden" id="search_id" name="search_id">
                    </div>
                    <div class="col-md-4 text-left">
                        <button type="submit" class="btn btn-info">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Search</font>
                            </font>
                        </button>
                        <a href="{{ route('region.get') }}" class="btn btn-default reset_style">Reset</a>
                        

                    </div>
                </div>
            </form>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>Region (EN)</th>
                                <th>Region (AR)</th>
                                {{-- <th>Active order</th>
                                <th >Completed order</th> --}}
                                <th width="20%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($region) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($region as $row_data)
                            <tr>
                                <th>{{ $i++ }}</th>
                                <td>{{ $row_data->lang[0]->name ?? '' }}</td>
                                <td>{{ $row_data->lang[1]->name ?? '' }}</td>
                                <td class="text-center">
                                    <a class="btn btn-sm btn-success text-white edit_btn page_edit" title="Edit Region"
                                        data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-sm btn-danger text-white" title="Delete Region" onclick="deleteRegion({{ $row_data->id }})"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $region->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

</div>
</div>
</div>
{{-- pop up --}}
<div class="modal fade none-border" id="region_pop" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg popup-promo" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="model_label">Add Region</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" data-no-padding="no-padding">
                <form id="add-region">
                    {{-- <input type="hidden" id="cancel_id" name="cancel_id"> --}}
                        <input type="hidden" id="region_id" name='region_id'>
                    <div class="row ">
                        <div class="col-md-6">
                            <label>Region (EN)</label>
                            <input class="form-control" name="region_en" id="region_en" placeholder="Enter Region" />

                        </div>
                        <div class="col-md-6">
                            <label>Region (AR)</label>
                            <input class="form-control" name="region_ar" id="region_ar"
                               placeholder="أدخل المنطقة" style="text-align:right !important" />
                        </div>
                        <div class="col-md-6">
                            <label>Delivery Charge</label>
                            <input class="form-control" name="delivary_charge" id="delivary_charge"placeholder="Enter delivery charge" />

                        </div>
                        <div class="col-md-6">
                            <label>Driver</label>
                            <select class="form-control search_val" id="driver" name="driver">
                                <option value=''> Select Driver</option>
                                @foreach ($drivers as $driver)
                                    <option value="{{$driver->id}}"> {{$driver->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>City</label>
                            <select class="form-control search_val" id="city" name="city">
                                <option value=''> Select City</option>
                                @foreach ($city as $cities)
                                    <option value="{{$cities->id}}"> {{$cities->lang[0]->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label>Location<sup class="star">*</sup></label>
                            <input type="text"
                            id="location" 

                            class="form-control" 
                            name="location" 
                            {{-- value='Riyadh Saudi Arabia' --}}
                            placeholder="Enter your location"
                            />
                                
                            {{-- @error('location')
                            <span class="error">{{ $message }}</span>
                            @enderror --}}
                        </div>

                        <div class="col-md-12">
                            <div id="map"></div>
                        </div>
                        <input id="latitude" type="hidden" value="{{ old('latitude', 24.7136) }}" name="latitude">
                        <input id="longitude" type="hidden" value="{{ old('longitude', 46.6753) }}" name="longitude">
                        <input id="latLang" type="hidden" value=[] name="latLang">
                        {{-- <input id="longitude" type="hidden" value="{{ old('longitude', 46.6753) }}"
                        name="longitude"> --}}
                        <div class="col-lg-12">
                            <div class="row ">
                                <div class="col-md-12 text-md-left">
                                    <button type="submit" id="save_region"
                                        class="btn btn-info waves-effect waves-light">
                                        Save
                                    </button>
                                    <button type="button" class="btn btn-default waves-effect"
                                        data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- end --}}
@endsection
@push('css')
<style>
    .popup-promo {
        max-height: 70%;
        margin-top: 50px;
        margin-bottom: 50px;
        max-width: 60%;
    }

    #map {
        widows: 100%;
        height: 400px;
    }

    #clear-btn {
        top: 3px !important;
    }
    .reset_style{
        margin-left:15px;
    }
    .pac-container {
        z-index: 1051!important;
    }
</style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endpush
@push('scripts')

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    let shape, map;
   
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#add_new_region').on('click',function(){
        $('#add-region').trigger("reset")
        $('#model_label').html('Add Region');
        $('#region_pop').modal({
            show: true

        });
    });
    $('.page_edit').on('click',function(e){
        region_id = $(this).data('id');
     
        var url = "region/edit/";
        // console.log("popup");
        if(shape)
        {
            shape.setMap(null)
        }
       
        $.get(url  + region_id, function (data) {
         
            $('#region_en').val(data.region.lang[0].name);
            $('#region_ar').val(data.region.lang[1].name);
            $('#driver').val(data.region.driver);
            $('#city').val(data.region.city_id);
            $('#delivary_charge').val(data.region.delivary_charge);
            $('#model_label').html('Edit Region');
            $('#region_id').val(region_id);
            // $('#location').val(data.region.location_address);
            $('#region_pop').modal({
                show: true
            });

            let coordinates = JSON.parse(data.region.coordinates)
            let coords = []

            coordinates.forEach(cord => {
                coords.push(new google.maps.LatLng(cord.lat, cord.lng))
            })

            let polygon = new google.maps.Polygon({
                path: coords,
                strokeWeight: 5,
                fillColor: '#F0F8FF',
                fillOpacity: 0.5,
            });

            map.setCenter(coords[0]);

            polygon.setMap(map)

            shape = polygon
        
        }) 
        
    });
    $('#save_region').on('click',function(e){
        // $("#add-region").valid();
        // e.preventDefault();
        // console.log("click");
        $("#add-region").validate({
            rules: {
                region_en: {        
                    required: true,         
                },
                region_ar: {          
                    required: true,
                },
                delivary_charge: {        
                    required: true,  
                    number:true,       
                },
                driver: {        
                    required: true,         
                },
                city: {        
                    required: true,         
                },
                 coordinates: {        
                     required: true,         
                 },
            },
            messages: {               
                region_en: {
                      required:"Region (EN) Required",
                      },
               
                delivary_charge: {
                      required: "Delivery charge Required",
                      number:"Please enter a valid charge"
                      },
                region_ar: {
                      required: "Region(AR) Required",
                      },
                driver: {
                      required: "Select a Driver",
                      },
                city: {
                      required: "Select a City",
                      },
                coordinates: {
                      required: "Location Required",
                      }
            },
            submitHandler: function(form) {
                region_en= $('#region_en').val();
                region_ar= $('#region_ar').val();
                driver =$('#driver').val();
                city =$('#city').val();
                charge =$('#delivary_charge').val();
                id=$('#region_id').val();
                // console.log(coordinates);
            $.ajax({
                    type:"POST",
                    url: "{{route('region.store')}}",
                    data:{ 
                        
                        region_en:region_en,
                        region_ar:region_ar,
                        driver:driver,
                        charge:charge,
                        city:city,
                        coordinates:coordinates,
                        id:id
                   
                     },
                     success: function(result){
                         if(result.msg ==='success'){
                            $('#region_pop').modal('hide')
                            window.location.reload();
                                Toast.fire({
                                icon: 'success',
                                title: result.input
                                });
                         }else{
                            Toast.fire({
                                icon: 'error',
                                title: result.message
                                });
                        }     
                    }
                })
            }

    });
                
    });
       
     


//     let map;
    var coordinates=[];
function initMap() {
    uluru={lat: 21.3891, lng: 39.8579};
    map = new google.maps.Map(document.getElementById('map'), {
        center: uluru,
        zoom: 8
    });
    // autocomplte
    // const input = document.getElementById('location');
    let complete = new google.maps.places.Autocomplete(document.getElementById('location'));
    // console.log('autocom',autocomplete);
     complete.bindTo("bounds", map);

     google.maps.event.addListener(complete,'place_changed', function() {
    //   marker.setVisible(false);
      var place = complete.getPlace();
    //   console.log(place);
      if (!place.geometry) {
        // User entered the name of a Place that was not suggested and
        // pressed the Enter key, or the Place Details request failed.
        window.alert("No details available for input: '" + place.name + "'");
        return;
      }

        let address = place.formatted_address.split(',');
        let formattedAddress = address.length > 1
            ? address[0] + ', ' +address[address.length - 1]
            : address[0];
        // console.log(formattedAddress,'address');
        $('#location').val(formattedAddress);
        $('#latitude').val(place.geometry.location.lat());
        $('#longitude').val(place.geometry.location.lng());

      // If the place has a geometry, then present it on a map.
      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);  // Why 17? Because it looks good.
      }
    //   marker.setPosition(place.geometry.location);
    //   marker.setVisible(true);
    });
    // end 
  
  var drawingManager = new google.maps.drawing.DrawingManager({
    drawingMode: google.maps.drawing.OverlayType.MARKER,
    drawingControl: true,
    drawingControlOptions: {
      position: google.maps.ControlPosition.TOP_CENTER,
      drawingModes: [ 'polygon', 'polyline']
    },
    markerOptions: {icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'},
    circleOptions: {
      fillColor: '#ffff00',
      fillOpacity: 1,
      strokeWeight: 5,
      clickable: false,
      editable: true,
      zIndex: 1
    }
  });
  drawingManager.setMap(map);

    google.maps.event.addListener(drawingManager, 'overlaycomplete', function(polygon) {

        if(coordinates.length)
        {
            coordinates.splice(0, coordinates.length);
        }

        if (shape) {
            shape.setMap(null)

        }
        shape = polygon.overlay
      
     path=polygon.overlay.getPath();
     for (var i = 0 ; i < path.length ; i++) {
      coordinates.push({
        lat: path.getAt(i).lat(),
        lng: path.getAt(i).lng()
      });
      
      drawingManager.setMap('');
    }
    });

    const centerControlDiv = document.createElement("button");
    centerControl(centerControlDiv, map);
    centerControlDiv.index = 1;
    centerControlDiv.style.padding = 0;
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(centerControlDiv);
    centerControlDiv.style.border = 'none';
    centerControlDiv.id = 'clear-btn';
    centerControlDiv.type = 'button';

    centerControlDiv.addEventListener('click', function() {
        shape.setMap(null);
        if(coordinates.length)
        {
            coordinates.splice(0, coordinates.length);
        }
    });
}

function centerControl(controlDiv, map) {
  // Set CSS for the control border.
  const controlUI = document.createElement("div");
  controlUI.style.backgroundColor = "#fff";
  controlUI.style.border = "2px solid #fff";
  controlUI.style.borderRadius = "3px";
  controlUI.style.boxShadow = "0 2px 6px rgba(0,0,0,.3)";
  controlUI.style.cursor = "pointer";
  controlUI.style.textAlign = "center";
  controlUI.title = "Click to clear the shape";
  controlDiv.appendChild(controlUI);
  // Set CSS for the control interior.
  const controlText = document.createElement("div");
  controlText.style.color = "rgb(25,25,25)";
  controlText.style.fontFamily = "Roboto,Arial,sans-serif";
  controlText.style.fontSize = "16px";
  controlText.style.paddingLeft = "5px";
  controlText.style.paddingRight = "5px";
  controlText.innerHTML = "Clear";
  controlUI.appendChild(controlText);
  // Setup the click event listeners: simply set the map to Chicago.
}

function deleteRegion(id) {
        $.confirm({
            title: false,
            content: 'Are you sure to delete this region? <br><br>You wont be able to revert this',
            buttons: {
                Yes: function() {
                    $.ajax({
                        type: "POST",
                        url: "{{route('region.delete')}}" ,
                        data: {
                            id: id
                        },
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                window.setTimeout(function() {
                                    window.location.href = '{{route("region.get")}}';
                                }, 1000);
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    console.log('cancelled');
                }
            }
        });
    }
       
</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?libraries=places,geocoder,drawing&key=AIzaSyCo61GS1Wc7xj7EOv5dnew8BsPascihil8&callback=initMap">
</script>
{{-- </script> --}}
{{-- <script async defer
src="https://maps.googleapis.com/maps/api/js?libraries=places,geocoder,drawing&key=AIzaSyBG_t-XkmJAtPKIpyhOoiXz6QuXphkaBQI&callback=initMap">
</script> --}}
<script>
     $( "#search_region" ).autocomplete({
      source: function( request, response ) {
        $.ajax( {
          url: "{{route('region.auto')}}",
          method:'post',
          data: {
            search: request.term
          },
          success: function( data ) {
            //   console.log(data);
            if(!data.length)
                {
                    var result = [
                 {
                    label: 'No matches found', 
                    value:0
                }
                ];
                response(result);
                }else{
                response( data );
                }
            

          }
        } );
      },
     
      select: function( event, ui ) {
        //   console.log(ui);
        
            $('#search_region').val(ui.item.label); // display the selected text
             $('#search_id').val(ui.item.value); // save selected id to input
        
            return false;
      }
    });
 </script>
@endpush