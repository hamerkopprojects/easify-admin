@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>OTHER SETTINGS</h1>
                        </div>
                    </div>
                </div>
            </div>
<div class="row">
    <div class="col-md-12">

        <div class="card-body">
            <form id="name_reset_form" action="{{route('other.store')}}" method="post">
                @csrf
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="basic-form">
                                    <div class="row">
                                            <div class="col-md-6 bottm">
                                                <label>Currency (EN)</label>
                                            @if(!empty($other->currency))
                                                <input type="text" class="form-control" id="currency_en"
                                                    name="currency_en" value="{{ old('currency_en',json_decode($other->currency)->currency_en) }} " placeholder="Enter Currency">
                                                    @else
                                                    <input type="text" class="form-control" id="currency_en"
                                                    name="currency_en" placeholder="Enter Currency" value="{{ old('currency_en') }}">
                                                    @endif
                                                    @error('currency_en')
                                                    <small class="error">
                                                        <strong>{{ $message }}</strong>
                                                    </small>

                                                @enderror
                                            </div>
                                            <div class="col-md-6">
                                                <label>Currency (AR)</label>
                                                @if(!empty($other->currency))
                                                <input type="text" class="form-control text-right" id="currency_ar"
                                                    name="currency_ar" placeholder="أدخل العملة" value="{{ old('currency_en',json_decode($other->currency)->currency_ar) }}">
                                                    @else
                                                    <input type="text" placeholder="أدخل العملة" class="form-control text-right" id="currency_ar"
                                                    name="currency_ar" value="{{ old('currency_ar') }}">
                                                    @endif
                                                    @error('currency_ar')
                                                    <small class="error">
                                                        <strong>{{ $message }}</strong>
                                                    </small>

                                                @enderror
                                            </div>
                                        <div class="col-md-6 bottm">
                                           
                                            <label>Driving Location Fetching Intervel (in minute) </label>
                                            @if(!empty($other->driver_loc_fetch_interval))
                                            <input type="text" placeholder="Enter Time" class="form-control" id="fech_time" name="fetch_time" value="{{ old('currency_en',json_decode($other->driver_loc_fetch_interval)->fetch_time) }}">
                                            @else
                                            <input type="text" class="form-control" id="fech_time" name="fetch_time" placeholder="Enter Time"  value="{{ old('fetch_time') }}">
                                            @endif
                                            @error('fetch_time')
                                            <small class="error">
                                                <strong>{{ $message }}</strong>
                                            </small>
                                            @enderror
                                        </div>

                                    <div class="col-md-6 bottm">
                                        
                                        <label>Customer Cancellation Time (in minute)</label>
                                        @if(!empty($other->driver_loc_fetch_interval))
                                        <input type="text" placeholder="Enter Time"  class="form-control" id="cancel_time" name="cancel_time" value="{{ old('currency_en',json_decode($other->driver_loc_fetch_interval)->cancel_time) }}">
                                        @else
                                        <input type="text" class="form-control" id="cancel_time" name="cancel_time" placeholder="Enter Time"  value="{{ old('cancel_time') }}">
                                        @endif
                                        @error('cancel_time')
                                        <small class="error">
                                            <strong>{{ $message }}</strong>
                                        </small>
                                        @enderror
                                        <i class="fa fa-info-circle infofa">&nbsp;Customer can cancel the order within X mins after accepting the order</i>
                                    </div>
                                    <div class="col-md-8 ">
                                        <label class="mrgn">Payment Mode</label>
                                    </div>
                                    <div class="col-md-8 ">
                                        <label>COD</label>
                                        @if(!empty($other->payment_options))
                                        <label class="switch">
                                        <input type="checkbox" id = "cod"  value = "Y" name="cod" {{json_decode($other->payment_options)->cod== 'Y' ? 'checked' : ''}}>
                                        <span class="slider round"></span>
                                        </label>
                                        @else
                                        <label class="switch">
                                        <input type="checkbox" id="cod"  value="Y" name="cod" >
                                        <span class="slider round"></span>
                                        </label> 
                                        @endif
                                     <label>Online</label>
                                     @if(!empty($other->payment_options))
                                     <label class="switch">
                                        <input type="checkbox" id="online" value = "Y" name="online" {{json_decode($other->payment_options)->online == "Y" ? 'checked' : ''}}>
                                        <span class="slider round"></span>
                                      </label>
                                      @else
                                        <label class="switch">
                                            <input type="checkbox" id="online" value ="Y" name="online" >
                                        <span class="slider round"></span>
                                        </label> 
                                        @endif
                                        <label>Offline</label>
                                        @if(!empty($other->payment_options))
                                        <label class="switch">
                                            <input type="checkbox" id="ofline" value = "Y" name="ofline" {{json_decode($other->payment_options)->ofline == "Y" ? 'checked' : ''}}>
                                            <span class="slider round"></span>
                                          </label>
                                          @else
                                          <label class="switch">
                                            <input type="checkbox" id="ofline"  value ="Y" name="ofline" >
                                          <span class="slider round"></span>
                                          </label> 
                                          @endif
                                    </div>
                                    <div class="col-md-6 bottm">
                                        <label>COD Fee</label>
                                        @if(!empty($other->auto_approval_for_cod_orders))
                                        <input type="text" class="form-control" id="codfee" name="codfee" value="{{ old('codfee', $other->cod_fee) }}">
                                        @else
                                        <input type="text" class="form-control" id="codfee" name="codfee" value ="{{ old('codfee') }}">
                                        @endif
                                        @error('codfee')
                                                    <small class="error">
                                                        <strong>{{ $message }}</strong>
                                                    </small>
                                                    @enderror
                                    </div>
                                    <div class="col-md-6 bottm">
                                        <label class="mrgn bottm">Driver Auto Assignment</label><br/>
                                        @if(!empty($other->auto_approval_for_cod_orders))
                                        <label class="switch">
                                            <input type="checkbox" placeholder="Enter COD Fee" value ="enable" name="order" {{$other->auto_approval_for_cod_orders== 'enable' ? 'checked' : ''}}>
                                            <span class="slider round"></span>
                                          </label>
                                          @else
                                          <label class="switch">
                                          <input type="checkbox"  value ="enable" name="order" >
                                          <span class="slider round"></span>
                                          </label> 
                                          @endif
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label">VAT(%)</label><br>
                                        @if(!empty($other->vat_percentage))
                                        <input type="text" placeholder="Enter VAT"  class="form-control form-white" id="vat" name="vat" value="{{ old('vat', $other->vat_percentage) }}">
                                        @else
                                        <input type="text" class="form-control form-white" id="vat" name="vat" placeholder="Enter VAT"  value="{{ old('vat', $other->vat_percentage) }}">
                                        @endif
                                        @error('vat')
                                        <small class="error">
                                            <strong>{{ $message }}</strong>
                                        </small>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <label class="mrgn bottm">Rating</label><br/>
                                        <label class="switch">
                                            <input type="checkbox" value="enable" name="rating" {{!empty($other->rating) && $other->rating == 'enable' ? 'checked' : ''}}>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="control-label">&nbsp;</label><br>
                                    <label class="control-label label_inline">Order delivery is in</label>
                                    <input type="text" class="form-control form-control-inline" id="delivery_days" name="delivery_days" value="{{ !empty($other->delivery_days)? $other->delivery_days : "" }}">
                                    <label class="control-label"> days after placing the order</label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="mrgn bottm">SMS</label><br/>
                                        <label class="switch">
                                            <input type="checkbox" value="enable" name="sms" {{!empty($other->sms) && $other->sms == 'enable' ? 'checked' : ''}}>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                    <div class="col-md-10 ">
                                        <label>Warehouse Location</label>
                                       
                                        <input 
                                            id="location" 
                                            class="form-control" 
                                            name="location" 
                                            value="{{json_decode($other->warehouse_loc)->loc_name ?? ''}}"
                                            placeholder="Enter your location" 
                                        />
                                        @error('location')
                                        <span class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-md-10 bottm">
                                        <div id="map"></div>
                                    </div>
                                    
                                    @if(!empty($other->warehouse_loc))
                                    <input id="latitude" type="hidden" value="{{json_decode($other->warehouse_loc)->latitude ?? ''}}" name="latitude">
                                    <input id="longitude" type="hidden" value="{{json_decode($other->warehouse_loc)->longitude ?? ''}}" name="longitude">
                                    @else
                                    <input id="latitude" type="hidden" value="{{ old('latitude') }}" name="latitude">
                                    <input id="longitude" type="hidden" value="{{ old('longitude') }}" name="longitude">
                                    @endif
                                    <div class="col-md-10">
                                        <label>Operation Area</label>
                                        <input 
                                            id="locations" 
                                            class="form-control" 
                                            name="locations" 
                                            value="{{ old('locations') }}"
                                            placeholder="Enter your location" 
                                        />
                                        @error('locations')
                                        <span class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-md-10">
                                        <div id="drawMap"></div>
                                    </div>
                                    <input id="latlng" type="hidden" value="{{$other->operational_area ?? ''}}" name="latlng">
                                </div>

                            </div>
                            <div class="col-lg-12">
                                <div class="row justify-content-end pb-5">
                                    <div class="col-md-12 filter-by-boat-jalbooth-butn01 text-md-left">
                                        <button type="submit" class="btn btn-primary buttonxl mr-md-2">
                                            Save
                                        </button>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
</div>
</div>
@endsection
@push('css')
<style>
    .switch {
  position: relative;
  display: inline-block;
     margin: 0 0.5rem;
    padding: 0;
    position: relative;
    border: none;
    height: 1.5rem;
    width: 3rem;
    border-radius: 1.5rem;
    margin-right: 50px;
}
label {
        font-weight: 500;
        margin-bottom: 16px;
        font-family: 'Roboto', sans-serif;
        font-size: 15px;
        color: #373757;
       
    }
    .bottm
    {
        padding-bottom: 37px;
    }

    .infofa
    {
        padding-top: 12px;
    }
    .switpd
    {
        margin-right: 40px;
    }
.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: -2px;
  right: 0;
  bottom: 0;
  background-color:#bdc1c8;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
 
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;

      position: absolute;
    top: 0.1875rem;
    left: 0.1875rem;
    width: 1.125rem;
    height: 1.125rem;
    border-radius: 1.125rem;
    background: #fff;
}

input:checked + .slider {
  background-color: #87b23e;
}

input:focus + .slider {
  box-shadow: 0 0 1px #87b23e;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
.col-md-12.row.form_first {
    margin-left: -15px;
}
label.mrgn {
    margin-bottom: -10px;
}

    .form-control {
        width: 60%;
    }

    .error {
        color: red;
    }
    #map {
        
      width: 950px;
      height: 400px;
    }
    #drawMap {
        width: 950px;
      height: 400px;
      
    }
    #clear-btn {
        top: 3px !important;
    }
    .reset_style{
        margin-left:15px;
    }
    .pac-container {
        z-index: 1051!important;
    }
    .form-control-inline {
    width:60px !important;
    display: inline;
}
.label_inline {
    margin-right: -15px !important;
}
</style>

@endpush
@push('scripts')
    <script>
         let map;
         let map2;
         let drawingManager;
         let shape;

function initMap() {
    // The location of Uluru
    // 
    var lat =   $('#latitude').val();
    var long =   $('#longitude').val();
   
   if(lat.length === 0){
    var uluru = {lat: {{old('latitude', 24.7136) }} , lng: {{old('longitude', 46.6753)}} };
    }else{
       
        var uluru = {lat: {{ old('latitude', 'JSON.parse(lat)') }}, lng: {{ old('longitude', 'JSON.parse(long)') }} };    
    }
   
    console.log(uluru);
    // The map, centered at Uluru
    map = new google.maps.Map(
        document.getElementById('map'), 
        {zoom: 8, center: uluru,
        gestureHandling: 'greedy'}
    );
    map2 = new google.maps.Map(
        document.getElementById('drawMap'), 
        {zoom: 8, center: uluru,
            gestureHandling: 'greedy'}
    );
    // The marker, positioned at Uluru
    marker = new google.maps.Marker({
        position: uluru, 
        map: map,
        draggable: true,
    });

   
    marker.addListener('dragend', setLatLng);

    const input = document.getElementById('location');
    let autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.bindTo('bounds', map);

    autocomplete.addListener('place_changed', function() {
      marker.setVisible(false);
      var place = autocomplete.getPlace();
      if (!place.geometry) {
        // User entered the name of a Place that was not suggested and
        // pressed the Enter key, or the Place Details request failed.
        window.alert("No details available for input: '" + place.name + "'");
        return;
      }

        let address = place.formatted_address.split(',');
        let formattedAddress = address.length > 1
            ? address[0] + ', ' +address[address.length - 1]
            : address[0];

        $('#location').val(formattedAddress);
        $('#latitude').val(place.geometry.location.lat());
        $('#longitude').val(place.geometry.location.lng());

      // If the place has a geometry, then present it on a map.
      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);  // Why 17? Because it looks good.
      }
      marker.setPosition(place.geometry.location);
      marker.setVisible(true);
    });


    let completes = new google.maps.places.Autocomplete(document.getElementById('locations'));
        completes.bindTo("bounds", map2);
        google.maps.event.addListener(completes,'place_changed', function() {
    //   marker.setVisible(false);
      var place = completes.getPlace();
     
      if (!place.geometry) {
        // User entered the name of a Place that was not suggested and
        // pressed the Enter key, or the Place Details request failed.
        window.alert("No details available for input: '" + place.name + "'");
        return;
      }

        let address = place.formatted_address.split(',');
        let formattedAddress = address.length > 1
            ? address[0] + ', ' +address[address.length - 1]
            : address[0];
        // console.log(formattedAddress,'address');
        $('#location').val(formattedAddress);
        $('#latitude').val(place.geometry.location.lat());
        $('#longitude').val(place.geometry.location.lng());

      // If the place has a geometry, then present it on a map.
      if (place.geometry.viewport) {
        map2.fitBounds(place.geometry.viewport);
      } else {
        map2.setCenter(place.geometry.location);
        map2.setZoom(17);  // Why 17? Because it looks good.
      }
    //   marker.setPosition(place.geometry.location);
    //   marker.setVisible(true);
    });

   
    var coordinates=[];
    var drawingManager = new google.maps.drawing.DrawingManager({
    drawingMode: google.maps.drawing.OverlayType.MARKER,
    drawingControl: true,
    drawingControlOptions: {
      position: google.maps.ControlPosition.TOP_CENTER,
      drawingModes: [ 'polygon', 'polyline']
    },
    markerOptions: {icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'},
    circleOptions: {
      fillColor: '#ffff00',
      fillOpacity: 1,
      strokeWeight: 5,
      clickable: false,
      editable: true,
      zIndex: 1
    }
  });
    drawingManager.setMap(map2);

    google.maps.event.addListener(drawingManager, 'overlaycomplete', function(polygon) {

      if (shape) {
       shape.setMap(null)
       }
       shape = polygon.overlay
       var coordinates = [];
       path=polygon.overlay.getPath();
         for (var i = 0 ; i < path.length ; i++) {
            coordinates.push({
               lat: path.getAt(i).lat(),
               lng: path.getAt(i).lng()
             });
        
         }


$('#latlng').val(JSON.stringify(coordinates)); //store array

var value = $('#latlng').val(); //retrieve array
value = JSON.parse(value);
});

    var region =   $('#latlng').val();
   if(region.length === 0){
 
   }else{ 
    
    let coordinate = JSON.parse(region);
   
            let coords = []
         
            coordinate.forEach(cord => {
                coords.push(new google.maps.LatLng(cord.lat, cord.lng))
            })

            let polygon = new google.maps.Polygon({
                path: coords,
                strokeWeight: 5,
                fillColor: '#F0F8FF',
                fillOpacity: 0.5,
             
            });

            map2.setCenter(coords[0]);

            polygon.setMap(map2)

    shape = polygon

   
   }
   const centerControlDiv = document.createElement("button");
    centerControl(centerControlDiv, map2);
    centerControlDiv.index = 1;
    centerControlDiv.style.padding = 0;
    map2.controls[google.maps.ControlPosition.TOP_CENTER].push(centerControlDiv);
    centerControlDiv.style.border = 'none';
    centerControlDiv.id = 'clear-btn';
    centerControlDiv.type = 'button';
    var latlng = document.getElementById('latlng');
    centerControlDiv.addEventListener('click', function() {
        latlng.value = '';
        shape.setMap(null);
        if(coordinates.length)
        {
            coordinates.splice(0, coordinates.length);
        }
    });

    // kcmkcmk cm
}
function centerControl(controlDiv, map) {
  // Set CSS for the control border.
  const controlUI = document.createElement("div");
  controlUI.style.backgroundColor = "#fff";
  controlUI.style.border = "2px solid #fff";
  controlUI.style.borderRadius = "3px";
  controlUI.style.boxShadow = "0 2px 6px rgba(0,0,0,.3)";
  controlUI.style.cursor = "pointer";
  controlUI.style.textAlign = "center";
  controlUI.title = "Click to clear the shape";
  controlDiv.appendChild(controlUI);
  // Set CSS for the control interior.
  const controlText = document.createElement("div");
  controlText.style.color = "rgb(25,25,25)";
  controlText.style.fontFamily = "Roboto,Arial,sans-serif";
  controlText.style.fontSize = "16px";
  controlText.style.paddingLeft = "5px";
  controlText.style.paddingRight = "5px";
  controlText.innerHTML = "Clear";
  controlUI.appendChild(controlText);
  // Setup the click event listeners: simply set the map to Chicago.
}

function setLatLng(event) {
    const lat = event.latLng.lat();
    const lng = event.latLng.lng();
    $('#latitude').val(lat);
    $('#longitude').val(lng);

    setAddress(lat, lng)
}

function setAddress(lat, lng) {
    const geocoder = new google.maps.Geocoder();
    geocoder.geocode({'location': {lat, lng}}, function(results, status) {
        if (status === 'OK' && results[0]) {
            let address = results[0].formatted_address.split(',');
            let formattedAddress = address.length > 1
                ? address[0] + ', ' +address[address.length - 1]
                : address[0];

            $('#location').val(formattedAddress);
        } 
    });
}

if ($("#name_reset_form").length > 0) {
    
 $.validator.addMethod('minStrict', function (value, el, param) {
        return this.optional( el ) || value >= param && !(value % 1);
    });
        $("#name_reset_form").validate({
 
            rules: {
                currency_en: {
                    required: true,
                   
                },
                currency_ar: {
                    required: true,
                },
                fetch_time: {
                    required: true,
                    number: true
                },
                cancel_time: {
                    required: true,
                    number: true
                },
                codfee:{
                    required: true,
                },
               codfee: {
               required: "#cod:checked"
               },
               vat: {
                required: true,
                minStrict: 0,
                number: true
                },
            },
            messages: {
 
                currency_en: {
                    required: "Currency in EN is required",
                },
                currency_ar: {
                    required: "Currency in AR is required",
                },
                fetch_time: {
                    required: "Driving location fetching intervel is required",
                    number:"Please enter a valid intervel"
                },
                cancel_time: {
                    required: "Customer cancellation Time is required",
                    number:"Please enter a valid time"
                },
                codfee: {
                    required: "COD Fee is required",
                },
                vat: {
                required: "Value required.",
                number: "Number required.",
                minStrict:"Enter correct number."
            },
 
            },
        })
    } 
</script>

    <script async defer
    src="https://maps.googleapis.com/maps/api/js?libraries=places,geocoder,drawing&key=AIzaSyBG_t-XkmJAtPKIpyhOoiXz6QuXphkaBQI&callback=initMap">
</script>
{{-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=drawing"></script> --}}

@endpush