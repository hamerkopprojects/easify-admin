@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>CANCELLATION REASON</h1>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
                <div class="col-lg-3">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12 text-right">
                                <a href="javascript:void(0);" id = "add_new_cancel_button" class="btn btn-info create_btn">
                                    <font style="vertical-align: inherit;"><i class="ti-plus"></i> Add Cancellation Reason
                                    </font>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
            </div>
            <!-- /# row -->
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                {{-- <form id="posts-filter" method="get" action="#"> --}}
                <div class="row tablenav top text-right">

                    <div class="col-md-6 ml-0">
                        <form action="{{route('cancel.get')}}" id="search-cancel-form" method="get">
                            <input type="text" value="{{$search  ?? ''}}" class="form-control search_val" id="search_field" name="search_field" placeholder="Search for cancellation reason">
                            <input type="hidden" id="cancellation_search" name="cancellation_search">
                        </form>
                    </div>
                    <div class="col-md-6 text-left">
                        <button type="submit" onclick="event.preventDefault(); 
                            document.getElementById('search-cancel-form').submit();" class="btn btn-info">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Search</font>
                            </font>
                        </button>
                        <a href="{{ route('cancel.get') }}" class="btn btn-default cancel_style">Reset</a>


                    </div>
                </div>
                {{-- </form> --}}
            </div>
        </div>
        {{-- </div> --}}
        {{-- </div>
</div>
<div class="row">
    <div class="col-md-12"> --}}

        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>Cancellation Reason (EN)</th>
                                <th>Cancellation Reason (AR)</th>
                                <th width="15%">Apps</th>
                                <th width="25%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($cancel) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($cancel as $row_data)
                            <tr>
                                <th>{{ $i++ }}</th>
                                <td>{{ $row_data->lang[0]->name }}</td>
                                <td>{{ $row_data->lang[1]->name }}</td>
                                <td>{{ $row_data->appType->pluck('name')->join(',') }}</td>
                                <td class="text-center">
                                    <a class="btn btn-sm btn-success text-white edit_btn  cancel_edit" title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-sm btn-danger text-white  cust_delete" title="Delete " data-id={{ $row_data->id }}><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">

                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
{{-- <div class="card">
    <div class="card-body">
        <div id="msgDiv"></div>
        <div class="table-responsive">
            {{$dataTable->table(['width' => '100%', 'class' => 'table table-borderd table-hover table-striped table-bordered  '])}}
</div>
</div>
</div> --}}

<div class="modal fade" id="cancel-add-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Add Cancellation Reason</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="brdar_photo_loader" class="loader" style="display: none;"> </div>
            <div class="modal-body" data-no-padding="no-padding">
                <form id="add-cancel" method="POST" action="#">
                    <input type="hidden" id="cancel_id" name="cancel_id">
                    @csrf
                    {{-- @method('PUT') --}}
                    <div class="row">
                        <div class="col-md-12">
                            <label>Cancellation Reason (EN)</label>
                            <input class="form-control" name="cancel_en" id="cancel_en" placeholder="Enter cancellation reason" />

                        </div>
                        <div class="col-md-12">
                            <label>Cancellation Reason (AR)</label>
                            <input class="form-control" name="cancel_ar" id="cancel_ar" placeholder="أدخل سبب الإلغاء" style="text-align:right !important" />
                        </div>
                        <div class="col-md-12 form-group adbot01" id ="apptype">
                           
                            <br/>
                            @foreach ($app_type as $item)
                                <input type="checkbox" name="site"  value="{{$item->id}}">{{$item->name}}
                            @endforeach
                            <label class="site_error"></label>
                        </div>
                        <div class="col-lg-12">
                            <div class="row 5">
                                <div class="col-md-12 text-md-left">
                                    <button type="submit" id="save_cancel" class="btn btn-info waves-effect waves-light">
                                        Save
                                    </button>
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                                    {{-- <a href="#" data-dismiss="modal" class="buttonxl button-secondary">
                                        Cancel
                                    </a> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<link href="{{ asset('assets/css/lib/data-table/dataTables.bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/css/cust-style.css') }}" rel="stylesheet" />
<style>
    /* .mul{   
         width: 200px;
    } */
    .res-style {
        margin-left: 12px;
    }


    table.dataTable tbody td {
        padding: 10px 18px;
    }

    .cancel_style {
        margin-left: 20px;
    }

    input[type=checkbox],input[type=radio]{
        box-sizing: border-box;
        padding: 0;
        margin: 0 10px 0 5px;
    }
    .loader{
    position: absolute;
    top:0px;
    right:0px;
    width:100%;
    height:100%;
    background-color:#eceaea;
    background-image:url('../assets/images/loader.gif');
    background-repeat:no-repeat;
    background-size: 50px;
    z-index:10000000;
    opacity: 0.4;
    filter: alpha(opacity=40);
}
</style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endpush
@push('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
{{-- {{$dataTable->scripts()}} --}}
<!-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script> -->
{{-- Sweet alert --}}
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
{{-- select2 --}}
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    //   $("#cancellation_search").select2({

    //     placeholder : "Search Cancellation Reason",
    //     // allowHtml: true,
    //     allowClear: true,

    // });
    $('#add_new_cancel_button').on('click', function() {
        $('#add-cancel').trigger("reset")
        $('#exampleModalLabel').html('Add Cancellation Reason');
        $('#cancel-add-popup').modal({
            show: true

        });
    })
    $('#save_cancel').on('click', function(e) {

        $("#add-cancel").validate({
            rules: {
                cancel_en: {
                    required: true,
                },
                cancel_ar: {
                    required: true,
                }
            },
            messages: {
                cancel_en: {
                    required: "Cancellation Reason (EN) Required",

                },

                cancel_ar: {
                    required: "Cancellation Reason(AR) Required",
                }
            },
            submitHandler: function(form) {
                $(`#brdar_photo_loader`).show();
                cancel_en = $('#cancel_en').val();
                cancel_ar = $('#cancel_ar').val();
                cancel_id = $('#cancel_id').val();
                var test = new Array();
                $("input[name='site']:checked").each(function() {
                            test.push($(this).val());
                    });
               
                if (cancel_id) {
                    $.ajax({
                        type: "POST",
                        url: "{{route('cancel.update')}}",
                        data: {
                            cancel_en: cancel_en,
                            cancel_ar: cancel_ar,
                            id: cancel_id,
                            app_type:test,

                        },

                        success: function(result) {
                            $(`brdar_photo_loader`).hide();
                            $('#cancel-add-popup').modal('hide')
                            window.location.reload();
                            // $('#cancel-reason-table').DataTable().ajax.reload()
                            Toast.fire({
                                icon: 'success',
                                title: 'Cancellation reason updated  successfully'
                            });
                        }
                    })
                } else {
                   
                    $.ajax({
                        type: "POST",
                        url: "{{route('cancel.store')}}",
                        data: {
                            cancel_en: cancel_en,
                            cancel_ar: cancel_ar,
                            app_type:test,
                        },

                        success: function(result) {
                            $(`brdar_photo_loader`).hide();
                            $('#cancel-add-popup').modal('hide')
                            window.location.href = '{{route("cancel.get")}}';
                            Toast.fire({
                                icon: 'success',
                                title: 'Cancellation reason added  successfully'
                            });
                        }
                    })
                }

            }
        })
    });

    $('.cancel_edit').on('click', function(e) {
        segment = $(this).data('id')
        // console.log('custt', segment);
        var url = "cancel-reasons/edit/";

        $.get(url + segment, function(data) {

            $('#cancel_en').val(data.cancel.lang[0].name);
            $('#cancel_ar').val(data.cancel.lang[1].name);
            $('#cancel_id').val(data.cancel.id);
            $("input[name='site']").prop('checked', false); 
                $.each(data.cancel.app_type, function(i, val){
                    // console.log(val);
                    $("input[name='site'][value='" + val.id + "']").prop('checked', true);

                });
            $('#exampleModalLabel').html('Edit Cancellation Reason');
            $('#cancel-add-popup').modal({
                show: true

            });
        })


    })

    $('.cust_delete').on('click', function(e) {
        cancel = $(this).data('id')
        $.confirm({
            title: false,
            content: 'Are you sure to delete this Cancellation reason? <br><br>You wont be able to revert this',
            buttons: {
                Yes: function() {
                    $.ajax({
                        url: "{{route('cancel.delete')}}",
                        type: 'post',
                        data: {
                            id: cancel,
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                window.setTimeout(function() {
                                    window.location.href = '{{route("cancel.get")}}';
                                }, 1000);
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    console.log('cancelled');
                }
            }
        });
    });

    $('#search_datatable').on('click', function() {
        let table = window.LaravelDataTables["cancel-reason-table"];
        cancel = $("#cancellation_search").val();
        // console.log('c',cancel);
        if (cancel) {
            table.columns(1).search(cancel)
        }
        table.draw();
    })
    $('#reset_cancel_table').on('click', function(e) {
        let table = window.LaravelDataTables["cancel-reason-table"];
        $('#cancellation_search').val('').trigger('change');
        table.search('').columns().search("").draw();

    });
    // $('#search_field').keyup(function(){ 
    //     var query = $('#search_field').val();
    //     // console.log('q',query);
    //     if(query.length >= 3 )
    //     {
    //      $.ajax({
    //       url:"{{route('cancel.auto')}}",
    //       method:"POST",
    //       data:{
    //           search:query
    //       },
    //       success:function(data){
    //           console.log(data ,"data");
    //        $('#search_list').fadeIn();   
    //        for( var i = 0; i<data.length; i++){ 
    //           var id = data[i]['value'];  
    //           var name = data[i]['label'];

    //           $("#search_list").append("<li class='form-control' value='"+id+"'>"+name+"</li>"); 
    //        } 
    //        // binding click event to li 
    //        $("#search_list li").bind("click",function(){
    //            console.log("click")
    //           setText(this); 
    //        }); 

    //       }
    //      });
    //     }
    // });
    //    function setText(val)
    //    {
    //     var value = $(val).text(); 
    //      var id = $(val).val(id); 
    //      $('#cancellation_search').val()
    //    }
    // $('#log').fadeOut();  
    $("#search_field").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: "{{route('cancel.auto')}}",
                method: 'post',
                data: {
                    search: request.term
                },
                success: function(data) {
                    response(data);
                }
            });
        },
        minLength: 1,
        select: function(event, ui) {
            $('#search_field').val(ui.item.label); // display the selected text
            $('#cancellation_search').val(ui.item.value); // save selected id to input
            return false;
        }
    });
</script>
<script>
    // $("#search_datatable").click(function() {
    //                 //Event Handling
    //             });
    //             var events = $._data(document.getElementById('search_datatable'), "events");
    //             var hasEvents = (events != null);
    // var foo = $.data( $('body').get(0), 'events' ).click
    // // you can query $.data( object, 'events' ) and get an object back, then see what events are attached to it.

    // $.each( foo, function(i,o) {
    //     alert(i) // guid of the event
    //     alert(o) // the function definition of the event handler
    // });
</script>
@endpush