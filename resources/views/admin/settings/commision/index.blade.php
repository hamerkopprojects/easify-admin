@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>COMMISION CATEGORY</h1>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
                <div class="col-lg-3">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12 text-right">
                                <a href="javascript:void(0);" id = "add_new_commision" class="btn btn-info create_btn">
                                    <font style="vertical-align: inherit;"><i class="ti-plus"></i> Add Commision Category
                                    </font>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
            </div>
            <!-- /# row -->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form id="posts-filter" method="get" action="{{ route('commision.get') }}">
                    <div class="row tablenav top text-right">
                        <div class="col-md-5 ml-0">
                            <input class="form-control" type="text" name="search" value="{{$search}}" placeholder="Search Commison">
                        </div>
                        <div class="col-md-3 text-left">
                            <button type="submit" class="btn btn-info">
                                <font style="vertical-align: inherit;">Search</font>
                            </button>
                            <a href="{{ route('commision.get') }}" class="btn btn-default reset_style">Reset</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S. No.</th>
                                <th>Commision Category</th>
                                <th>Commission Percentage (%)</th>
                                <th width="25%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($commision) > 0)
                            @php
                            $i = 1;
                            @endphp

                            @foreach ($commision ?? '' as $row_data)
                            <tr>
                                <th>{{ $i++ }}</th>
                                <td>{{ $row_data->name ?? '' }}</td>
                                <td>{{ $row_data->value }}</td>
                                
                                <td class="text-left">
                                   
                                  
                                    <a class="btn btn-sm btn-success text-white edit_btn edit_commision" title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>

                                    <a class="btn btn-sm btn-danger text-white" title="Delete Commision" onclick="deleteCommison({{ $row_data->id }})"><i class="fa fa-trash"></i></a>
                                  

                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $commision->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
{{-- Pop Up --}}
<div class="modal fade" id="user_popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="name_change"><strong>
                        ADD COMMISION CATEGORY
                    </strong></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form id="city_form" action="javascript:;" method="POST">
                <div class="modal-body">
                    <div class="msg_div"></div>
                    <div class="row">
                        <input type="hidden" name="commision_unique" id='commision_unique'>
                        <div class="col-md-6">
                            <label class="control-label">Commision Category <span class="text-danger"> *</span></label>
                            <input class="form-control form-white" placeholder="Enter commision category" type="text" name="commision" id="commision" />
                        </div>
                        <div class="col-md-6">
                            <label>Commission percentage (%)<span class="text-danger"> *</span></label>
                            <input class="form-control" name="value" id="value" placeholder="Value" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                 
                    <button type="submit" class="btn btn-info waves-effect waves-light save-city" id="save_data">
                        ADD
                    </button>
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- end popup --}}
@endsection
@push('css')
<style>
    .reset_style {
        margin-left: 15px;
    }
</style>
@endpush
@push('scripts')

<script>
    

    $('#add_new_commision').on('click', function() {
    
        $('#name_change').html('Add Commision Category');
        $('#save_data').text('Add').button("refresh");
        $("#city_form")[0].reset();
        $('#user_popup').modal({
            show: true
        });
    })
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.save-city').on('click', function(e) {
        // e.preventDefault();
        

        $("#city_form").validate({
            rules: {
                commision: {
                    required: true,
                },
                value: {
                  
                    required: true,
                    number:true,
                },
                
            },
            messages: {
                commision: {
                    required: "Commision required",
                },
                value: {
                    required: "Value required",
                    number: "Number required",
                },
            },
            submitHandler: function(form) {
                commision_unique = $("#commision_unique").val();
                if (commision_unique) {
                    $.ajax({
                        type: "POST",
                        url: "{{route('commision.update')}}",
                        data: {
                            commision: $('#commision').val(),
                            value: $('#value').val(),
                            commision_unique: commision_unique


                        },

                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#city_form")[0].reset();
                            } else {
                                // console.log(data.message);
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });


                } else {
                    $.ajax({
                        type: "POST",
                        url: "{{route('commision.store')}}",
                        data: {
                            commision: $('#commision').val(),
                            value: $('#value').val(),
                        },

                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.href = '{{route("commision.get")}}';
                                }, 1000);
                                $("#city_form")[0].reset();
                            } else {
                                // console.log(data.message);
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });

                }



            }
        })
    });

    $('.edit_commision').on('click', function(e) {
        page = $(this).data('id')
        

        $('#name_change').html('Edit Commision');
        $('#save_data').text('Save').button("refresh");


        var url = "commision-category/edit/";

        $.get(url + page, function(data) {
         
            $('#commision').val(data.commision.name),
            $('#value').val(data.commision.value),
            $('#commision_unique').val(data.commision.id)
            $('#user_popup').modal({
                show: true

            });
        });
    });

   
    function deleteCommison(id) {
        $.confirm({
            title: false,
            content: 'Are you sure to delete this commision? <br><br>You wont be able to revert this',
            buttons: {
                Yes: function() {
                    $.ajax({
                        type: "POST",
                        url: "{{route('commision.delete')}}",
                        data: {
                            id: id
                        },
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                window.setTimeout(function() {
                                    window.location.href = '{{route("commision.get")}}';
                                }, 1000);
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    console.log('cancelled');
                }
            }
        });
    }
</script>
@endpush