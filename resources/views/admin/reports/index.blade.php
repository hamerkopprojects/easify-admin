@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>REPORTS</h1>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
            </div>
            <!-- /# row -->
            <div class="row">
                <div class="col-sm-12">
                    <x-report-tab reportTab="sales" />
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form id="frm_report_filter">
                                <div class="row">
                                    <div class="col-md-4 ml-0">
                                        <label>Category</label>
                                        <select class="form-control select-2-filter_category" name="filter_category[]" multiple="multiple">
                                            @foreach($category as $value)
                                            <option value="{{$value->id}}">{{ $value->lang[0]->name }}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                    <div class="col-md-4 ml-0">
                                        <label>Brand</label>
                                        <select class="form-control select-2-filter_brand" name="filter_brand[]" multiple="multiple">
                                            @foreach($brand as $brand_value)
                                            <option value="{{$brand_value->id}}">{{ $brand_value->lang[0]->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4 ml-0">
                                        <label>Supplier</label>
                                        <select class="form-control" name="filter_supplier">
                                            <option value="">All</option>
                                            @foreach($supplier as $supplier_value)
                                            <option value="{{$supplier_value->id}}">{{ $supplier_value->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4 ml-0">
                                        <div id="daterange-sales" style="background: #fff; cursor: pointer; padding: 8px 10px; border: 1px solid #ccc; width: 100% ">
                                            <i class="fa fa-calendar"></i>&nbsp;
                                            <i class="fa fa-caret-down"></i>
                                            <span></span>
                                        </div>
                                    </div>
                                    <div class="col-md-4 text-left">
                                        <button type="button" class="btn btn-info" id="search_filter">
                                            <font style="vertical-align: inherit;">Search</font>
                                        </button>
                                        <a href="{{route('reports')}}" class="btn btn-default">Reset</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>


                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="card-header card_style_rev">
                        <h6 class="center mt-2">Total Sales</h6>
                        <p></p>
                        <h4 class="center center" id="total_sales"> SAR 0.00</h4>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card-header card_style_rev">
                        <h6 class="center mt-2">Total Orders</h6>
                        <p></p>
                        <h4 class="center center" id="total_orders"> 0 </h4>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card-header card_style_rev">
                        <h6 class="center mt-2">Total Customers</h6>
                        <p></p>
                        <h4 class="center" id="total_customers"> 0 </h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title p-b-25">Category</h4>
                            <div id='category-pie'></div>
                            <div id="category-pie-legend" class="pietopcorner"><p class='mt-3 text-center'>No Data Available</p></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title p-b-25">Brand</h4>
                            <div id='brand-pie'></div>
                            <div id="brand-pie-legend" class="pietopcorner"><p class='mt-3 text-center'>No Data Available</p></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-3">Top Selling Category</h4>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Category Name</th>
                                            <th>Sales Count</th>
                                            <th class="cls_last_child">Sales Value</th>
                                        </tr>
                                    </thead>
                                    <tbody id="top-categories">
                                        <tr><td colspan="8" class="text-center">No records found!</td></tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-3">Top Selling Brands</h4>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Brand Name</th>
                                            <th>Sales Count</th>
                                            <th class="cls_last_child">Sales Value</th>
                                        </tr>
                                    </thead>
                                    <tbody id="top-brands">
                                        <tr><td colspan="8" class="text-center">No records found!</td></tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title mb-3">Top Selling Products</h4>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Product Name</th>
                                            <th>Sales Count</th>
                                            <th class="cls_last_child">Sales Value</th>
                                        </tr>
                                    </thead>
                                    <tbody id="top-products">
                                        <tr><td colspan="8" class="text-center">No records found!</td></tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @push('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="{{ asset('assets/js/lib/Morris/morris.css') }}">
    <style>
        .card_style_rev {
            background-color: #000;
        }
        .card-header:first-child{
            border-radius: 14px !important;
        }
        .center {
            text-align: center;
            color: white;
        }
        .select2-container{ width:100% !important; }
        .select2-container--default .select2-selection--single {
            height: 42px !important;border-radius: 0px !important;font-size: 1rem !important;
            padding: .4rem !important;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow b { top:20px !important}
    </style>
</style>
@endpush
@push('scripts')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
<script src="{{ asset('assets/js/lib/Morris/raphael.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/Morris/morris.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/js/reports/reports.js') }}"></script>
<script>
$(function () {
    $("#search_filter").trigger("click");
});
</script>
@endpush