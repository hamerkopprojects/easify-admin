@if (count($delivered_orders) > 0)
<div class="col-lg-12 text-right">
    <a href="{{ route('reports.excel-download', ['id' => $supplier_id, 'start' => $start, 'end' => $end])}}"><i class="fa fa-download"></i> Download report as XLS</a>
</div>
@endif
<div class="col-lg-6">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 ml-0">
                    <h6>Supplier Name</h6>
                    <p>{{ $supplier->name }}</p>
                </div>
                <div class="col-md-6 ml-0">
                    <h6>Phone</h6>
                    <p>{{ $supplier->phone }}</p>
                </div>
                <div class="col-md-6 ml-0">
                    <h6>Supplier Code</h6>
                    <p>{{ $supplier->code }}</p>
                </div>
                <div class="col-md-6 ml-0">
                    <h6>Email</h6>
                    <p>{{ $supplier->email }}</p>
                </div>
                <div class="col-md-6 ml-0">
                    <h6>Commission Percentage</h6>
                    <p>{{ $c_perecent }} %</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-6">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-8 ml-0">
                    <label>Total number of orders:</label>
                </div>
                <div class="col-md-4 ml-0">
                    <h6>{{ $total_count ?? 0 }}</h6>
                </div>
                <div class="col-md-8 ml-0">
                    <label>Total Sale:</label>
                </div>
                <div class="col-md-4 ml-0">
                    <h6>SAR {{$total_sale}}</h6>
                </div>
                <div class="col-md-8 ml-0">
                    <label>Total Commission amount for Easify:</label>
                </div>
                <div class="col-md-4 ml-0">
                    <h6>SAR {{ number_format($com_percent, 2, ".", "") }}</h6>
                </div>
                <div class="col-md-8 ml-0">
                    <label>Total markup amount for Easify:</label>
                </div>
                <div class="col-md-4 ml-0">
                    <h6>SAR {{ number_format($easify_markup, 2, ".", "") }}</h6>
                </div>
                <div class="col-md-8 ml-0">
                    <label>Total revenue for supplier:</label>
                </div>
                <div class="col-md-4 ml-0">
                    <h6>SAR {{ number_format($total_revenue, 2, ".", "")}}</h6>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-12">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>S. No.</th>
                            <th>Order ID</th>
                            <th>Customer</th>
                            <th>Sale Amount</th>
                            <th>Commission <span class="small">(Based on category)</span></th>
                            <th>Markup</th>
                            <th class="cls_last_child">Supplier Revenue</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($delivered_orders) > 0)
                        @php
                        $i = 1;
                        @endphp
                        @foreach ($delivered_orders as $row_data)
                        @php 
                        $product_price = \App\Models\ProductPrice::where(['product_id' => $row_data->product_id, 'variant_lang_id' => $row_data->variant_id])->first();
                        $item_price = $product_price->discount_price ? $product_price->discount_price : $product_price->price;
                        $markup = $item_price * $row_data->easify_markup / 100;
                        $markup = $markup * $row_data->item_count;
                        $commission = $item_price * $row_data->commision_percentage / 100;
                        $commission = $commission * $row_data->item_count;
                        $final_amount = $row_data->grant_total - $markup - $commission;
                        @endphp
                        <tr>
                        <th>{{ $i++ }}</th>
                        <td>{{ $row_data->ord_id }}</td>
                        <td>{{ $row_data->cust_name }}</td>
                        <td>{{ $row_data->grant_total }}</td>
                        <td>{{ $commission }}</td>
                        <td>{{ $markup }}</td>
                        <td class="cls_last_child">{{ $final_amount }}</td>
                        </tr>
                        @endforeach
                        @else
                        <tr><td colspan="8" class="text-center">No records found!</td></tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>