@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>REPORTS</h1>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
            </div>
            <!-- /# row -->
            <div class="row">
                <div class="col-sm-12">
                    <x-report-tab reportTab="commission" />
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form id="frm_commission_report">
                                <div class="row">
                                    <div class="col-md-4 ml-0">
                                        <label>Supplier</label>
                                        <select class="form-control" name="supplier">
                                            <option value="">All</option>
                                            @foreach($supplier as $supplier_value)
                                            <option value="{{$supplier_value->id}}">{{ $supplier_value->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4 ml-0">
                                        <label>Select Date</label>
                                        <div id="daterange-sales" style="background: #fff; cursor: pointer; padding: 8px 10px; border: 1px solid #ccc; width: 100% ">
                                            <i class="fa fa-calendar"></i>&nbsp;
                                            <i class="fa fa-caret-down"></i>
                                            <span></span>
                                        </div>
                                    </div>
                                    <div class="col-md-4 text-left">
                                        <label>&nbsp;</label><br/>
                                        <input type="hidden" name="start_date" id="start_date">
                                        <input type="hidden" name="end_date" id="end_date">
                                        <button type="submit" class="btn btn-info save-btn">
                                            <font style="vertical-align: inherit;">Search</font>
                                        </button>
                                        <a href="{{route('reports.commission')}}" class="btn btn-default">Reset</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="commission_report">
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" href="{{ asset('assets/js/lib/Morris/morris.css') }}">
<style>
    .card_style_rev {
        background-color: #000;
    }
    .card-header:first-child{
        border-radius: 14px !important;
    }
    .center {
        text-align: center;
        color: white;
    }
    .select2-container{ width:100% !important; }
    .select2-container--default .select2-selection--single {
        height: 42px !important;border-radius: 0px !important;font-size: 1rem !important;
        padding: .4rem !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow b { top:20px !important}
</style>
</style>
@endpush
@push('scripts')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
<script src="{{ asset('assets/js/lib/Morris/raphael.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/Morris/morris.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/js/reports/reports.js') }}"></script>
<script>
 $('.save-btn').on('click',function (){
 $('#start_date').val($('#daterange-sales').data('daterangepicker').startDate.format('YYYY-MM-DD'));
 $('#end_date').val($('#daterange-sales').data('daterangepicker').endDate.format('YYYY-MM-DD'));
 $("#frm_commission_report").validate({
    normalizer: function (value) {
    return $.trim(value);
    },
            rules: {
            supplier: {
            required: true,
            },
            },
            messages: {
            supplier: {
            required: 'Select supplier'
            },
            },
            submitHandler: function (form) {
            $('.loading_box').show();
            $('.loading_box_overlay').show();
            $.ajax({
            type: "POST",
                    url: "{{route('reports.commissiontab')}}",
                    data: $('#frm_commission_report').serialize(),
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                    if (data.status == 1) {
                    $("#commission_report").html(data.result);
                    $('.loading_box').hide();
                    $('.loading_box_overlay').hide();
                    } else {
                    $('button:submit').attr('disabled', false);
                    Toast.fire({
                    icon: 'error',
                    title: data.message
                    });
                    }
                    },
                    error: function (err) {
                    $('button:submit').attr('disabled', false);
                    }
            });
            return false;
            }
    }); 
  }); 
</script>
@endpush