@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>SLIDER</h1>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /# row -->
            <div class="row">
                <div class="col-lg-12">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item disabled"> <a class="nav-link" href="#ads" role="tab" data-toggle="tab"><span class="hidden-sm-up"><i class="ti-id-badge"></i></span> <span class="hidden-xs-down">ADS</span></a> </li>
                        <li class="nav-item"> <a class="nav-link active" href="#slider" role="tab" data-toggle="tab"><span class="hidden-sm-up"><i class="ti-image"></i></span> <span class="hidden-xs-down">SLIDER</span></a> </li>
                    </ul>
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th style="width:6%">S. No.</th>
                                            <th style="width:10%">Slider</th>
                                            <th>Slider Image (EN)
                                                <p class="small">Max file size: 1 MB</p>
                                                <p class="small" style="margin-top:-30px">Recommended file size: 200 KB - 300 KB</p>
                                                <p class="small" style="margin-top:-30px">Supported formats: jpeg, png</p>
                                                <p class="small" style="margin-top:-30px">File dimension: 1440 x 444 pixels</p>
                                            </th>
                                            <th class="cls_last_child">Slider Image (AR)
                                                <p class="small">Max file size: 1 MB</p>
                                                <p class="small" style="margin-top:-30px">Recommended file size: 200 KB - 300 KB</p>
                                                <p class="small" style="margin-top:-30px">Supported formats: jpeg, png</p>
                                                <p class="small" style="margin-top:-30px">File dimension: 1440 x 444 pixels</p>
                                            </th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($slider_data) > 0)
                                        @php
                                        $i = 1;
                                        @endphp
                                        @foreach ($slider_data as $row_data)
                                        @php
                                        if($row_data->web_flag == 'U')
                                        $web_link = $row_data->url;
                                        elseif($row_data->web_flag == 'C')
                                        $web_link = $row_data->slider_category->lang[0]->name ?? '';
                                        else
                                        $web_link = $row_data->slider_product->lang[0]->name ?? '';
                                        
                                        if($row_data->ar_flag == 'U')
                                        $web_link_ar = $row_data->url_ar;
                                        elseif($row_data->ar_flag == 'C')
                                        $web_link_ar = $row_data->slider_category1->lang[0]->name ?? '';
                                        else
                                        $web_link_ar = $row_data->slider_product1->lang[0]->name ?? '';

                                        @endphp
                                        <tr>
                                            <th>{{ $i++ }}</th>
                                            <td>{{ $row_data->slider_type }}</td>
                                            <td>
                                                <div class="cover-photo">
                                                    <div id="slider_image_upload_en{{$row_data->id}}" class="add">+</div>
                                                    <div id="slider_image_en{{$row_data->id}}_loader" class="loader" style="display: none;">
                                                        <img src="{{ asset('assets/images/loader.gif') }}" alt="">
                                                    </div>
                                                    <div class="preview-image-container" @if(empty($row_data->web_image)) style="display:none;" @endif id="slider_image_en{{$row_data->id}}_image_preview">
                                                         <div class="scrn-link" style="position: relative;top: -20px;">
                                                            <button type="button" class="scrn-img-close" onclick="deleteSlider({{ $row_data->id }}, 'web_image')">
                                                                <i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i>
                                                            </button>
                                                            <img class="scrn-img" style="max-width: 200px" src="{{ !empty($row_data->web_image) ? url('uploads/'.$row_data->web_image) : '' }}" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-fileupload" style="display: none;">
                                                    <label for="slider_image_en{{$row_data->id}}" data-nocap="1">Select cover photo:</label>
                                                    <input type="file" id="slider_image_en{{$row_data->id}}" name="slider_image_en" data-id="{{$row_data->id}}" data-imgw="1440" data-imgh="444"/>
                                                </div>
                                                @if(!empty($row_data->web_image))
                                                <div class="mt-2">
                                                    <a class="link_btn" title="Link Image" data-type="en" data-id="{{ $row_data->id }}"><i class="fa fa-link"></i> <span style="color: #00b0e8">Link:</span> {{isset($row_data->id) ? $web_link : '' }}</a>
                                                </div>
                                                @endif
                                                @push('scripts')
                                                <script>
                                                    $('#slider_image_upload_en{{$row_data->id}}').click(function(e) {
                                                    $('#slider_image_en{{$row_data->id}}').click();
                                                    });
                                                    $('#slider_image_en{{$row_data->id}}').on('change', function() {
                                                    const file = $(this)[0].files[0];
                                                    var imgw = $(this).data('imgw');
                                                    var imgh = $(this).data('imgh');
                                                    var slider_id = $(this).data('id');
                                                    img = new Image();
                                                    var imgwidth = 0;
                                                    var imgheight = 0;
                                                    var _URL = window.URL || window.webkitURL;
                                                    img.src = _URL.createObjectURL(file);
                                                    img.onload = function() {
                                                    imgwidth = this.width;
                                                    imgheight = this.height;
                                                    if (imgwidth >= imgw && imgheight >= imgh){
                                                    readUrl(file, 'web_image', uploadFile, imgw, imgh, slider_id);
                                                    } else{
                                                    $('input[type="file"]').val('');
                                                    Toast.fire({
                                                    icon: 'error',
                                                            title: 'Image size must be greater or equal to ' + imgw + ' X ' + imgh,
                                                    });
                                                    }
                                                    }
                                                    });
                                                </script>
                                                @endpush
                                            </td>
                                            <td>
                                                <div class="cover-photo">
                                                    <div id="slider_image_upload_ar{{$row_data->id}}" class="add">+</div>
                                                    <div id="slider_image_ar{{$row_data->id}}_loader" class="loader" style="display: none;">
                                                        <img src="{{ asset('assets/images/loader.gif') }}" alt="">
                                                    </div>
                                                    <div class="preview-image-container" @if(empty($row_data->web_image_ar)) style="display:none;" @endif id="slider_image_ar{{$row_data->id}}_image_preview">
                                                         <div class="scrn-link" style="position: relative;top: -20px;">
                                                            <button type="button" class="scrn-img-close" onclick="deleteSlider({{ $row_data->id }}, 'web_image_ar')">
                                                                <i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i>
                                                            </button>
                                                            <img class="scrn-img" style="max-width: 200px" src="{{ !empty($row_data->web_image_ar) ? url('uploads/'.$row_data->web_image_ar) : '' }}" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="control-fileupload" style="display: none;">
                                                    <label for="slider_image_ar{{$row_data->id}}" data-nocap="1">Select cover photo:</label>
                                                    <input type="file" id="slider_image_ar{{$row_data->id}}" name="slider_image_ar" data-id="{{$row_data->id}}" data-imgw="1440" data-imgh="444"/>
                                                </div>
                                                @if(!empty($row_data->web_image_ar))
                                                <div class="mt-2">
                                                    <a class="link_btn" title="Link Image" data-type="ar" data-id="{{ $row_data->id }}"><i class="fa fa-link"></i> <span style="color: #00b0e8">Link:</span> {{isset($row_data->id) ? $web_link_ar : '' }}</a>
                                                </div>
                                                @endif
                                                @push('scripts')
                                                <script>
                                                    $('#slider_image_upload_ar{{$row_data->id}}').click(function(e) {
                                                    $('#slider_image_ar{{$row_data->id}}').click();
                                                    });
                                                    $('#slider_image_ar{{$row_data->id}}').on('change', function() {
                                                    const file = $(this)[0].files[0];
                                                    var imgw = $(this).data('imgw');
                                                    var imgh = $(this).data('imgh');
                                                    var slider_id = $(this).data('id');
                                                    img = new Image();
                                                    var imgwidth = 0;
                                                    var imgheight = 0;
                                                    var _URL = window.URL || window.webkitURL;
                                                    img.src = _URL.createObjectURL(file);
                                                    img.onload = function() {
                                                    imgwidth = this.width;
                                                    imgheight = this.height;
                                                    if (imgwidth >= imgw && imgheight >= imgh){
                                                    readUrl(file, 'web_image_ar', uploadFile, imgw, imgh, slider_id);
                                                    } else{
                                                    $('input[type="file"]').val('');
                                                    Toast.fire({
                                                    icon: 'error',
                                                            title: 'Image size must be greater or equal to ' + imgw + ' X ' + imgh,
                                                    });
                                                    }
                                                    }
                                                    });
                                                </script>
                                                @endpush
                                            </td>
                                            <td class="cls_last_child">
                                                <form method="POST" action="javascript:void(0);" id="frm_slider_data{{$row_data->id}}">
                                                    <div class="row">
                                                    <div class="col-md-6">
                                                        <input class="form-control form-white" placeholder="Enter Title(EN)" type="text" name="title" value="{{ isset($row_data->id) ? $row_data->title : ''}}"/>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <textarea class="form-control area" name="content" placeholder="Enter content(EN)">{{ isset($row_data['id']) ? $row_data->content : ''}}</textarea>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input class="form-control form-white" placeholder="Enter Title(AR)" type="text" name="title_ar" value="{{ isset($row_data->id) ? $row_data->title_ar : ''}}"/>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <textarea class="form-control area" name="content_ar" placeholder="Enter content(AR)">{{ isset($row_data['id']) ? $row_data->content_ar : ''}}</textarea>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <input type="hidden" name="slider_id" value="{{$row_data->id}}">
                                                        <button type="submit" class="btn btn-info waves-effect waves-light save-btn" id="slider_data{{$row_data->id}}" data-id="{{$row_data->id}}">
                                                            Save
                                                        </button>
                                                    </div>
                                                    </div>
                                                </form>
                                                @push('scripts')
                                                <script>
                                                    $('#slider_data{{$row_data->id}}').click(function(e) {
                                                    var slider_id = $(this).data('id');
                                                    addFunction(slider_id);
                                                    });
                                                </script>
                                                @endpush
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal pooup -->
<div class="modal fade none-border" id="formModal">
    <div class="modal-dialog modal-xl">
        <form id="frm_create_link" action="javascript:;" method="POST">
            <div class="modal-content">

            </div>
        </form>
    </div>
</div>
<!-- END MODAL -->
<div class="modal fade none-border" id="image-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Crop Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="crop-images">
                <div class="row">
                    <div class="col-md-8">
                        <img id="cropper" src="" alt="" style="max-width: 100%">
                    </div>
                    <div class="col-md-4">
                        <div class="preview"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="crop" type="button" class="btn btn-primary">Crop & Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/lib/cropper/cropper.min.css') }}">
<style type="text/css">
    img {
        display: block;
        max-width: 100%;
    }
    .preview {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }
    .link_btn{
        cursor: pointer;
    }
</style>
@endpush
@push('scripts')

{{-- Sweet alert --}}
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
{{-- select2 --}}
<script src="{{ asset('assets/js/lib/cropper/cropper.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/cropper/jquery-cropper.min.js') }}"></script>
<script src="{{ asset('assets/js/custom_crop.js') }}"></script>
<script>
                                                    $('.nav-tabs a').on('click', function (e) {
                                                    var x = $(e.target).text();
                                                    $.ajax({
                                                    type: "GET",
                                                            url: "{{route('ads_tab')}}",
                                                            data: {'activeTab': x},
                                                            success: function (result) {
                                                            if (result.status == 'ads'){
                                                            window.location.href = '{{route("ads")}}';
                                                            } else{
                                                            window.location.href = '{{route("slider")}}';
                                                            }
                                                            }
                                                    });
                                                    });
                                                    $.ajaxSetup({
                                                    headers: {
                                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                    }
                                                    });
                                                    function uploadFile(file, type, id) {
                                                    var formData = new FormData();
                                                    formData.append('photo', file);
                                                    formData.append('type', type);
                                                    formData.append('id', id);
                                                    $.ajax({
                                                    url: "{{ route('slider_image')}}",
                                                            type: 'POST',
                                                            data: formData,
                                                            processData: false, // tell jQuery not to process the data
                                                            contentType: false, // tell jQuery not to set contentType
                                                            beforeSend: function() {
                                                            $(`#${type}${id}_loader`).show();
                                                            $('#image-modal').modal('hide');
                                                            },
                                                            success: function(data) {
                                                            if (data.status == 1) {
                                                            window.setTimeout(function() {
                                                            window.location.href = '{{route("slider")}}';
                                                            }, 1000);
                                                            Toast.fire({
                                                            icon: 'success',
                                                                    title: data.message
                                                            });
                                                            $('input[type="file"]').val('');
                                                            $(`#${type}${id}_loader`).hide();
                                                            } else {
                                                            Toast.fire({
                                                            icon: 'error',
                                                                    title: data.message
                                                            });
                                                            $(`#${type}${id}_loader`).hide();
                                                            }
                                                            }
                                                    });
                                                    }

                                                    function deleteSlider(id, type) {
                                                    $.confirm({
                                                    title: '<span class="small">Are you sure to delete this image?</span>',
                                                            content: 'You wont be able to revert this',
                                                            buttons: {
                                                            Yes: function () {

                                                            $.ajax({
                                                            url: "{{ route('detete_slider_img') }}",
                                                                    data: {id: id, type: type},
                                                                    type: 'POST',
                                                                    success: function(data) {
                                                                    if (data.status == 1) {
                                                                    window.setTimeout(function() {
                                                                    window.location.href = '{{route("slider")}}';
                                                                    }, 1000);
                                                                    Toast.fire({
                                                                    icon: 'success',
                                                                            title: data.message
                                                                    });
                                                                    } else {
                                                                    Toast.fire({
                                                                    icon: 'error',
                                                                            title: data.message
                                                                    });
                                                                    }
                                                                    }
                                                            });
                                                            },
                                                                    No: function () {
                                                                    console.log('cancelled');
                                                                    }
                                                            }
                                                    });
                                                    }
                                                    $('.link_btn').on('click', function() {
                                                    var id = $(this).data("id");
                                                    var type = $(this).data("type");
                                                    $.ajax({
                                                    type: "GET",
                                                            url: "{{route('slider_category')}}",
                                                            data: {
                                                            'id': id,
                                                            'type': type
                                                            },
                                                            success: function(data) {
                                                            $('.modal-content').html(data);
                                                            $('#formModal').modal('show');
                                                            }
                                                    });
                                                    });
                                                    $("#frm_create_link").validate({
                                                    normalizer: function(value) {
                                                    return $.trim(value);
                                                    },
                                                            rules: {
                                                            url: {
                                                            required: '#url:blank',
                                                                    url: true
                                                            },
                                                                    category: {
                                                                    required: '#category:blank'
                                                                    },
                                                                    product: {
                                                                    required: '#product:blank'
                                                                    },
                                                            },
                                                            messages: {
                                                            url: {
                                                            required: 'Url is required.'
                                                            },
                                                                    category: {
                                                                    required: 'Category is required.'
                                                                    },
                                                                    product: {
                                                                    required: 'Product is required.'
                                                                    }
                                                            },
                                                            submitHandler: function(form) {
                                                            $.ajax({
                                                            type: "POST",
                                                                    url: "{{route('add_slider_category')}}",
                                                                    data: $('#frm_create_link').serialize(),
                                                                    dataType: "json",
                                                                    headers: {
                                                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                                    },
                                                                    success: function(data) {
                                                                    if (data.status == 1) {
                                                                    Toast.fire({
                                                                    icon: 'success',
                                                                            title: data.message
                                                                    });
                                                                    window.setTimeout(function() {
                                                                    window.location.href = '{{route("slider")}}';
                                                                    }, 1000);
                                                                    $("#frm_create_link")[0].reset();
                                                                    } else {
                                                                    Toast.fire({
                                                                    icon: 'error',
                                                                            title: data.message
                                                                    });
                                                                    }
                                                                    $('button:submit').attr('disabled', false);
                                                                    },
                                                                    error: function(err) {
                                                                    $('button:submit').attr('disabled', false);
                                                                    }
                                                            });
                                                            return false;
                                                            }
                                                    });
                                                    function addFunction(id) {
                                                    $("#frm_slider_data" + id).validate({
                                                    normalizer: function(value) {
                                                    return $.trim(value);
                                                    },
                                                            submitHandler: function(form) {
                                                            $.ajax({
                                                            type: "POST",
                                                                    url: "{{route('add_slider_data')}}",
                                                                    data: $("#frm_slider_data" + id).serialize(),
                                                                    dataType: "json",
                                                                    headers: {
                                                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                                    },
                                                                    success: function(data) {
                                                                    if (data.status == 1) {
                                                                    Toast.fire({
                                                                    icon: 'success',
                                                                            title: data.message
                                                                    });
                                                                    window.setTimeout(function() {
                                                                    window.location.href = '{{route("slider")}}';
                                                                    }, 1000);
                                                                    } else {
                                                                    Toast.fire({
                                                                    icon: 'error',
                                                                    title: data.message
                                                                    });
                                                                    }
                                                                    $('button:submit').attr('disabled', false);
                                                                    },
                                                                    error: function(err) {
                                                                    $('button:submit').attr('disabled', false);
                                                                    }
                                                            });
                                                            return false;
                                                            }
                                                    });
                                                    }
</script>
@endpush