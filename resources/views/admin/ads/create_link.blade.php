<div class="modal-header">
    <h4 class="modal-title"><strong>
            Link Ad
        </strong></h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <div class="msg_div"></div>
    <div class="row">
        <div class="col-md-12">
            @if($type != 'A')
            <label class="radio-inline">
                <input type="radio" name="link_value" value="U" class="chk_link" {{ $row_data[$flag_type] == 'U' ? 'checked="checked"' : ''}}> URL
            </label>
            @endif
            <label class="radio-inline {{ $type != 'A' ? 'ml-3' : ''}}">
                <input type="radio" name="link_value" value="C" class="chk_link" {{ $row_data[$flag_type] == 'C' ? 'checked="checked"' : ''}}> Category
            </label>
            <label class="radio-inline ml-3">
                <input type="radio" name="link_value" value="P" class="chk_link" {{ $row_data[$flag_type] == 'P' ? 'checked="checked"' : ''}}> Product
            </label>
        </div>
        @if($type != 'A')
        <div class="col-md-12" style="{{ $row_data[$flag_type] == 'U' ? 'display:block' : 'display:none'}}" id="link_url">
            <label class="control-label">Url <span class="text-danger">*</span></label>
            <input class="form-control form-white" placeholder="Enter URL" type="text" name="url" id="url" value="{{ isset($row_data['id']) ? $row_data['url'] : ''}}"/>
        </div>
        @endif
        <div class="col-md-12" style="{{ $row_data[$flag_type] == 'C' ? 'display:block' : 'display:none'}}" id="link_cat">
            <div class="col-md-8">
            <label class="control-label">Category <span class="text-danger">*</span></label>
            <select class="form-control" name="category" id="category">
                <option value=""> Select Category </option>
                @foreach($cat_data as $cat_value)
                @if($type == 'A')
                <option value="{{$cat_value['category_id']}}" {{ ($cat_value['category_id'] == $row_data['app_category']) ? 'selected' : '' }}>{{$cat_value['name']}}</option>
                @else
                <option value="{{$cat_value['category_id']}}" {{ ($cat_value['category_id'] == $row_data['web_category']) ? 'selected' : '' }}>{{$cat_value['name']}}</option>
                @endif
                @endforeach
            </select>
            </div>
        </div>
        <div class="col-md-12" style="{{ $row_data[$flag_type] == 'P' ? 'display:block' : 'display:none'}}" id="link_pdt">
            <div class="col-md-8">
            <label class="control-label">Supplier <span class="text-danger">*</span></label>
            <select class="form-control" name="supplier" id="supplier" onchange="getProducts(this);">
                <option value=""> Select Supplier </option>
                @foreach($supplier as $supp_value)
                <option value="{{$supp_value['id']}}" {{ ($supp_value['id'] == $row_data['supplier_id']) ? 'selected' : '' }}>{{$supp_value['name']}}</option>
                @endforeach
            </select>
            </div>
            <div class="col-md-8">
            <label class="control-label">Product <span class="text-danger">*</span></label>
            <select class="form-control" name="product" id="product">
                <option value=""> Select Product </option>
                @foreach($pdt_data as $pdt_value)
                @if($type == 'A')
                <option value="{{$pdt_value['product_id']}}" {{ ($pdt_value['product_id'] == $row_data['app_product']) ? 'selected' : '' }}>{{$pdt_value['name']}}</option>
                @else
                <option value="{{$pdt_value['product_id']}}" {{ ($pdt_value['product_id'] == $row_data['web_product']) ? 'selected' : '' }}>{{$pdt_value['name']}}</option>
                @endif
                @endforeach
            </select>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <input type="hidden" id="_ad_id" name="_ad_id" value="@if(isset($row_data['id'])) {{ $row_data['id'] }}@endif">
    <input type="hidden" id="type" name="type" value="{{ $type }}">
    <button type="submit" class="btn btn-info waves-effect waves-light save-categorys">
        Save
    </button>
    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
</div>
<script>
    $('#product').select2();
    $('#supplier').select2();
    $(".chk_link").change(function () {
        if ($(this).val() == 'U') {
            $("#link_url").css({'display': 'block'});
            $("#link_cat").css({'display': 'none'});
            $("#link_pdt").css({'display': 'none'});
        } else if ($(this).val() == 'C') {
            $("#link_url").css({'display': 'none'});
            $("#link_cat").css({'display': 'block'});
            $("#link_pdt").css({'display': 'none'});
        } else
        {
            $("#link_url").css({'display': 'none'});
            $("#link_cat").css({'display': 'none'});
            $("#link_pdt").css({'display': 'block'});
        }
    });
</script>