@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>SUPPORT REQUESTS</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form id="search_support" method="get" action="{{route('support.list')}}">
                                <div class="row tablenav top text-left">
                                    <div class="col-md-4 ml-0">
                                        <select class="form-control" name="search_app_type" id="search_app_type" value=test placeholder="By App Type">
                                            <option value="">Select app type</option>
                                            <option value="website" {{'website' == $app_type ? "selected" :""}}>Website</option>
                                            {{-- <option value="driver" {{ 'B2Capp' == $app_type ? "selected" :""}}>B2C APP</option> --}}
                                            <option value="supplier" {{ 'supplier' == $app_type ? "selected" :""}}>Supplier </option>
                                            <option value="branch" {{ 'branch' == $app_type ? "selected" :""}}>Branch</option>
                                            <option value="driver" {{ 'driver' == $app_type ? "selected" :""}}>Driver</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 ml-0">
                                        <select class="form-control" name="search_name" id="search_name" placeholder="By App Type">
                                            <option value="">Select user</option>
                                            @foreach ($users as $user)
                                                <option value="{{$user->id}}" {{ $user->id == $name ? "selected" :""}} >{{$user->cust_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4 text-left">
                                        <button type="submit" class="btn btn-info">
                                            <font style="vertical-align: inherit;">Search</font>
                                        </button>
                                        <a href="{{route('support.list')}}" class="btn btn-default">Reset</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div id="msgDiv"></div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>Subject</th>
                                            <th>Message</th>
                                            <th>App type</th>
                                            <th>Submitted by</th>
                                            <th width="10%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($support) > 0)
                                        @php
                                        $i = 1;
                                        @endphp

                                        @foreach ($support as $list)
                                            {{-- @php dd($list) @endphp --}}
                                            @php
                                            
                                                if($list->app_type =='website'){
                                                    $name=$list->owner->cust_name;
                                                }else{
                                                    $name= $list->owner->name;
                                                }
                                            @endphp
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>{{$list->subject}}</td>
                                                <td>{{$list->message}}</td>
                                                <td>{{ucfirst($list->app_type)}}</td>
                                                <td>{{ucfirst($name)}}</td>
                                                <td class="center">
                                                    <a href="#" class="btn btn-sm btn-success text-white view_btn" title="support Chat Details" data-req-id="{{ $list['id']}}"><i class="fa fa-eye"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="8" class="text-center">No records found!</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center d-flex justify-content-center mt-3">
                                {{ $support->links() }}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

    <div class="modal fade" id="request-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Support Chat</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="chat-history" data-no-padding="no-padding">
                    <div class="container">
                        <div class="basic_details">

                        </div>
                        <div class="messaging">
                            <div class="inbox_msg">
                                <div class="mesgs">
                                    <div class="msg_history">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="type_msg">
                        <div class="input_msg_write">
                            <textarea type="text" id="chat-msg" class="write_msg" placeholder="Type a message" rows="1" cols="100" style="padding-top: 10px" ></textarea>
                            {{-- <input type="text" id="chat-msg" class="write_msg" placeholder="Type a message"> --}}
                            <button id="send-msg" class="msg_send_btn" type="button"><span class="material-icons">send</span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('css')
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<style>
     .modal-xl {
        max-width: 900px;
    }
    .head-style {
        margin: 35px 0 0 5px;
        padding-bottom: 35px;
    }
    .container {
        max-width: 1170px;
        margin: auto;
    }
    .inbox_people {
        background: #f8f8f8 none repeat scroll 0 0;
        float: left;
        overflow: hidden;
        width: 40%;
        border-right: 1px solid #c4c4c4;
    }
    img {
        max-width: 100%;
    }
    .inbox_msg {
        /* border: 1px solid #c4c4c4; */
        clear: both;
        overflow: hidden;
    }
    .top_spac {
        margin: 20px 0 0;
    }


    .recent_heading {
        float: left;
        width: 40%;
    }

    .srch_bar {
        display: inline-block;
        text-align: right;
        width: 60%;
        padding:
    }
    @media (min-width: 1500px){
         .container {
        width: auto;
        }
    }

    .headind_srch {
        padding: 10px 29px 10px 20px;
        overflow: hidden;
        border-bottom: 1px solid #c4c4c4;
    }

    .recent_heading h4 {
        color: #05728f;
        font-size: 21px;
        margin: auto;
    }

    .srch_bar input {
        border: 1px solid #cdcdcd;
        border-width: 0 0 1px 0;
        width: 80%;
        padding: 2px 0 4px 6px;
        background: none;
    }

    .srch_bar .input-group-addon button {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
        border: medium none;
        padding: 0;
        color: #707070;
        font-size: 18px;
    }

    .srch_bar .input-group-addon {
        margin: 0 0 0 -27px;
    }

    .chat_ib h5 {
        font-size: 15px;
        color: #464646;
        margin: 0 0 8px 0;
    }

    .chat_ib h5 span {
        font-size: 13px;
        float: right;
    }

    .chat_ib p {
        font-size: 14px;
        color: #989898;
        margin: auto
    }

    .chat_img {
        float: left;
        width: 11%;
    }

    .chat_ib {
        float: left;
        padding: 0 0 0 15px;
        width: 88%;
    }

    .chat_people {
        overflow: hidden;
        clear: both;
    }

    .chat_list {
        border-bottom: 1px solid #c4c4c4;
        margin: 0;
        padding: 18px 16px 10px;
    }

    .inbox_chat {
        height: 550px;
        overflow-y: scroll;
    }

    .active_chat {
        background: #ebebeb;
    }

    .received_msg {
        display: inline-block;
        padding: 0 0 0 10px;
        vertical-align: top;
        width: 100%;
    }

    .received_withd_msg p {
        background: #ebebeb none repeat scroll 0 0;
        border-radius: 3px;
        color: #646464;
        font-size: 14px;
        margin: 0;
        padding: 5px 10px 5px 12px;
        width: 80%;
    }

    .time_date {
        color: #747474;
        display: block;
        font-size: 12px;
        margin: 8px 0 0;
    }

    .received_withd_msg {
        width: 80%;
    }

    .mesgs {
        float: left;
        padding: 30px 15px 0 25px;
        width: 100%;
    }

    .sent_msg p {
        background: #05728f none repeat scroll 0 0;
        border-radius: 3px;
        font-size: 14px;
        margin: 0;
        color: #fff;
        padding: 5px 10px 5px 12px;
        width: 100%;
    }

    .outgoing_msg {
        overflow: hidden;
        margin: 26px 0 26px;
    }

    .sent_msg {
        float: right;
        width: 80%;
    }

    .input_msg_write {
        height: 48px;
    }

    .input_msg_write textarea {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
        border: medium none;
        color: #4c4c4c;
        font-size: 15px;
        min-height: 48px;
        width: 100%;
        padding-left: 15px;
    }
    textarea{
        resize: none;
        overflow:hidden;
        
    }
    .msg_history{
        height: 250px; 
        overflow: auto; 
    }
    .type_msg {
        border: 1px solid #c4c4c4;
        border-radius: 16px;
        position: relative;
        width: 100%;
    }

    .modal-footer {
        border: none !important;
    }

    .msg_send_btn {
        background: #05728f none repeat scroll 0 0;
        border: medium none;
        border-radius: 50%;
        color: #fff;
        cursor: pointer;
        font-size: 4px;
        height: 40px;
        position: absolute;
        right: 4px;
        top: 4px;
        width: 40px;
    }

</style>
@endpush
@push('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function(){

            $('.view_btn').on('click', function(e) {
            let requestId = $(this).data('req-id');

            $('#send-msg').data('req-id', requestId);
            $.ajax({
                url: `{{ url('admin/support-request/chat') }}/${requestId}`,
                type: 'GET',
                success: function(data) {
                    let chat = data.data.chat;
                    let details = data.data.details;
                    console.log("msg",details);
                    $('#chat-history .basic_details').empty()
                    $('#chat-history .basic_details').append(
                        details.app_type =='website' ?
                        `
                        <div class="row">
                                <div class="col-md-6">
                                    <p>Subject</p>
                                    <span class="time_date">${details.subject}</span>
                                </div>
                                <div class="col-md-6">
                                    <p>Message</p>
                                    <span class="time_date">${details.message}</span>
                                </div>
                                <div class="col-md-3">
                                    <p>App Type</p>
                                    <span class="time_date">${details.app_type}</span>
                                </div>
                                <div class="col-md-3">
                                    <p>Requested By</p>
                                    <span class="time_date">${details.owner.cust_name}</span>
                                </div>
                                <div class="col-md-3">
                                    <p>Phone</p>
                                    <span class="time_date">${details.owner.phone}</span>
                                </div>
                                <div class="col-md-3">
                                    <p>Email</p>
                                    <span class="time_date">${details.owner.email}</span>
                                </div>
                        </div>
                    `
                    :
                    `<div class="row">
                                <div class="col-md-6">
                                    <p>Subject</p>
                                    <span class="time_date">${details.subject}</span>
                                </div>
                                <div class="col-md-6">
                                    <p>Message</p>
                                    <span class="time_date">${details.message}</span>
                                </div>
                                <div class="col-md-3">
                                    <p>App Type</p>
                                    <span class="time_date">${details.app_type}</span>
                                </div>
                                <div class="col-md-3">
                                    <p>Requested By</p>
                                    <span class="time_date">${details.owner.name}</span>
                                </div>
                                <div class="col-md-3">
                                    <p>Phone</p>
                                    <span class="time_date">${details.owner.phone}</span>
                                </div>
                                <div class="col-md-3">
                                    <p>Email</p>
                                    <span class="time_date">${details.owner.email}</span>
                                </div>
                        </div>
                    `
                    )
                    $('#chat-history .msg_history').empty()
                   
                    chat.forEach(msg => {
                        $('#chat-history .msg_history').append(
                            msg.user_type == 'admin' ?
                            `
                            <div class="outgoing_msg">
                                <div class="sent_msg">
                                    <p>${msg.message}</p>
                                    <span class="time_date">${msg.time}</span>
                                </div>
                            </div>
                            `
                            :
                            `
                            <div class="incoming_msg">
                                <div class="received_msg">
                                    <div class="received_withd_msg">
                                        <p>${msg.message}</p>
                                        <span class="time_date">${msg.time}</span>
                                    </div>
                                </div>
                            </div>
                            `
                        )
                    })
                     $('#request-modal').modal({
                        show: true
                     });
                    setTimeout(() => {
                        let objDiv = document.getElementById("chat-history");
                        objDiv.scrollTop = objDiv.scrollHeight;
                    }, 200)
                }
            })
        });
        $('#send-msg').click(function(e) {
        let msg = $('#chat-msg').val();
        let replace_msg = msg.replace(/^\s+|\s+$/gm,'');
        let requestId = $(this).data('req-id');
        if(replace_msg)
        {
            $('#chat-history .msg_history').append(`
                <div class="outgoing_msg">
                    <div class="sent_msg">
                        <p>${msg.replace(/\n/g, "<br>")}</p>
                        <span class="time_date">now</span>
                    </div>
                </div>
            `)

            let objDiv = document.getElementById("chat-history");
            objDiv.scrollTop = objDiv.scrollHeight;
            $.ajax({
                url: `{{ url('admin/support-request/chat') }}/${requestId}`,
                type: 'POST',
                data: {
                 message: msg
                },
                success: function(error) {
                    $('#chat-msg').val('');
                }
            })
        }
       

        
    })
    });
    $('#search_app_type').on('change',function(){
        let app_type = $('#search_app_type').val();
        console.log(app_type);
        $.ajax({
            url: `{{ url('admin/user/list-chat') }}/${app_type}`,
            type: 'GET',
            success: function(data) {
                   if(app_type == 'website') 
                   {
                    $('#search_name').empty(); 
                    $('#search_name').append("<option value=''>Select user</option>");
                    $.each(data, function(key, value) {
                     $('#search_name').append("<option value='" + value.id + "'>" + value.cust_name + "</option>");
                    });
                   }else
                   {
                    $('#search_name').empty();  
                    $('#search_name').append("<option value=''>Select user</option>");
                    $.each(data, function(key, value) {
                     $('#search_name').append("<option value='" + value.id + "'>" + value.name + "</option>");
                    });
                   }
            }
            })

    })
    </script>
@endpush