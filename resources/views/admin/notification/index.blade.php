@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>NOTIFICATIONS</h1>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /# row -->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{route('notification.store')}}">
                    @csrf
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <label>Title (EN)</label>
                            <input class="form-control" value="{{ old('title_en') }}" name="title_en" placeholder="Enter Title" />
                            @error('title_en')
                            <span class="error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-md-6 arabic_label">
                            <label>Title (AR)</label>
                            <input class="form-control" value="{{ old('title_ar') }}" name="title_ar" style="text-align:right !important" placeholder="أدخل الشروط" />
                            @error('title_ar')
                            <span class="error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-md-6 english_label">
                            <label>Notification (EN)</label>
                            <textarea class="form-control area" id="notification_en" name="notification_en">{{ old('notification_en') }}</textarea>
                            @error('notification_en')
                            <span class="error">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="col-md-6 arabic_label">
                            <label>Notification (AR)</label>
                            <textarea class="form-control area"  id="notification_ar" name="notification_ar" style="text-align:right !important">{{ old('notification_ar') }}</textarea>
                            @error('notification_ar')
                            <span class="error">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="row col-md-6">
                            <div class="adbot01">
                                <input type="radio" name="app_value" value="b2c" checked />Website 
                                <input type="radio" name="app_value" value="driver" />Driver App
                            </div>
                        </div>

                        {{-- </div> --}}
                        <div class="col-lg-12">
                            <div class="row justify-content-end pb-5">
                                <div class="col-md-12 filter-by-boat-jalbooth-butn01 text-md-left">
                                    <button type="submit" class="btn btn-info waves-effect waves-light">
                                        Send
                                    </button>
                                    <a href="{{route('notification.get')}}" class="btn btn-default waves-effect">
                                        cancel
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <form id="search_form" action="{{route('notification.get')}}" method="get">
                    <div class="row">

                        <div class="col-md-8" id="select_class">

                            <input type="text" class="form-control" id="search_note" value="{{$search}}" name="search_note" placeholder="Search by title">
                        </div>

                        <div class="col-md-4 add-jalbooth-butn-main text-left">
                            <button class="btn btn-info" id="search_datatable">
                                Search
                            </button>
                            <a href="{{route('notification.get')}}" class="btn btn-default reset_style" id="reset_datatable">
                                Reset
                            </a>
                        </div>

                    </div>
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>

                                <th>Title (EN)</th>
                                <th>Notification (EN)</th>
                                <th class="right-align">Title (AR)</th>
                                <th class="right-align">Notification (AR)</th>
                                <th>Apps</th>
                                <th>Sent at</th>
                                <th width="10%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($notification) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($notification as $row_data)
                            <tr>
                                {{-- <th>{{ $i++ }}</th> --}}
                                <td>{{ $row_data->lang[0]->title ?? '' }}</td>
                                <td>{!! \Illuminate\Support\Str::limit($row_data->lang[0]->content ?? '', 100, '...') !!}</td>
                                <td class="right-align">{{ $row_data->lang[1]->title ?? '' }}</td>
                                <td class="right-align">{!! \Illuminate\Support\Str::limit($row_data->lang[1]->content ?? '', 100, '...') !!}</td>
                                <td>{{ $row_data->app_type }}</td>
                                <td>{{ \Carbon\Carbon::parse($row_data->created_at)->format('d-m-Y h:i a')}}</td>
                                <td class="text-center">
                                    <a class="btn btn-sm btn-danger text-white delete_notification" data-id="{{ $row_data->id }}" title="Delete"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $notification->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection
@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
    input[type=checkbox],
    input[type=radio] {
        /* box-sizing: border-box;
        padding: 0;
        margin: 0 10px 0 5px; */
        margin: 10px;
    }

    .area {
        height: 120px;
    }

    .error {
        color: red;
    }

    .reset_style {
        margin-left: 10px;
    }
</style>
@endpush
@push('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })
    $('.delete_notification').on('click', function(e) {
        var id = $(this).data("id");
            $.confirm({
            title: false,
            content: 'Are you sure to delete this Notification? <br><br>You wont be able to revert this',
            buttons: {
                Yes: function() {
                    $.ajax({
                        url: "{{route('notification.delete')}}",
                        type: 'POST',
                        data: {

                            id: id
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                window.setTimeout(function() {
                                    window.location.href = '{{route("notification.get")}}';
                                }, 1000);
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    console.log('cancelled');
                }
            }
        });
    });

    $("#search_note").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: "{{route('notification.auto')}}",
                method: 'post',
                data: {
                    search: request.term
                },
                success: function(data) {
                    response(data);
                }
            });
        },
        minLength: 1,
        select: function(event, ui) {
            $('#search_note').val(ui.item.label); // display the selected text
            // $('#cancellation_search').val(ui.item.value); // save selected id to input
            return false;
        }
    });
</script>
@endpush