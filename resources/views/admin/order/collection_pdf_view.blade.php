<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   </head>
   <body >
         <table  bgcolor="#ffffff" cellpadding="0"  cellspacing="0">
            <tbody>
               <tr>
                  <td  width="77%" >
                     <h4  face="arial" style="font-size: 20px; color:#716e6e;"> ORDER DERAILS</h4>
                  </td>
               </tr>
            </tbody>
         </table>
         <table width="100%"  bgcolor="#ffffff" cellpadding="0"  cellspacing="0" >
            <tbody>
               <tr>
                  <td  width="33%">
                     <b>Order ID</b> 
                     <p style="margin-top: 2px;
                        margin-bottom: 20px;
                        font-size: 14px;">{{$details['order_id']}}</p>
                  </td>
                  <td  width="33%">
                     <b>Order date</b> 
                     <p style="margin-top: 2px;
                        margin-bottom: 20px;
                        font-size: 14px;">{{Carbon\carbon::parse($details['created_at'])->format('d-m-Y')}}</p>
                  </td>
                  <td  width="33%">
                     <b>Collection status</b> 
                     <p style="margin-top: 2px;
                        margin-bottom: 20px;
                        font-size: 14px;">{{$ord_status[$details->order_status]}}</p>
                  </td>
               </tr>
               <tr>
                  <td width="33%">
                     <b>No of supplier</b> 
                     <p style="margin-top: 2px;
                        margin-bottom: 20px;
                        font-size: 14px;">{{$supplier}}</p>
                  </td>
                  <td width="33%">
                     <b>Customer</b> 
                     <p style="margin-top: 2px;
                        margin-bottom: 20px;
                        font-size: 14px;">{{ucfirst($details->customer->cust_name)}}</p>
                  </td>
               </tr>
            </tbody>
         </table>
         <table width="100%"  bgcolor="#ffffff" cellpadding="0"  cellspacing="0" >
            <tbody>
               <tr>
                  <td height="20 "colspan="4"
                     style="border-top:0;border-right:0;border-bottom:1px solid #aaa;border-left:0;">
                  </td>
               </tr>
            </tbody>
         </table>
         <table  bgcolor="#ffffff" cellpadding="0" cellspacing="0">
            <tbody>
               <tr>
                  <td height="20 "colspan="4"
                  style="border-top:0;border-right:0;border-bottom:1px solid #aaa;border-left:0;">
                  </td>
               </tr>
            </tbody>
         </table>
         <table width="100%" bgcolor="#ffffff" cellpadding="0"  cellspacing="0">
            <thead>
               <tr face="arial">
                  <td width="55%" style="padding-bottom: 10px;font-size: 16px;
                     font-weight: 500; "><strong>Supplier/Branch</strong></td>
                  <td width="15%" face="arial" style="padding-bottom: 10px;font-size: 16px;
                     font-weight: 500;  "><strong>Items</strong></td>
                  <td width="10%" style="padding-bottom: 10px;font-size: 16px;
                     font-weight: 500;  "><strong>Quantity</strong></td>
                  {{-- <td width="20%" style="padding-bottom: 10px;font-size: 16px;
                     font-weight: 500;  "><strong>Amount</strong></td> --}}
               </tr>
            </thead>
            <tbody>
               @php
                    $item_list= $details->items->groupBy('branch_id');
               @endphp
                @foreach($item_list as $new_item)
               <tr>
                @php
                $Supplier_code = $new_item[0]->branch->code;
               @endphp
              
                  <td width="55%" style="padding-bottom: 10px;font-size: 14px;
                     font-weight: 500; ">{{$Supplier_code}}</td>
                 <td width="15%" style="padding-bottom: 10px;font-size: 14px;
                     font-weight: 500; ">
                       @foreach ($new_item as $item)
                       <p> {{$item->product->lang[0]->name}}</p>
                       @endforeach
                     </td>
                  <td width="10%" style="padding-bottom: 10px;font-size: 14px;
                     font-weight: 500; ">
                     @foreach ($new_item as $item)
                        <p> {{$item->item_count}}</p>
                     @endforeach
                  </td>
                  {{-- <td width="20%" style="padding-bottom: 10px;font-size: 14px;
                     font-weight: 500; ">SAR {{$amount}} </td> --}}
               @endforeach
               <tr>
                  <td height="20 "colspan="4"
                     style="border-top:0;border-right:0;border-bottom:1px solid #aaa;border-left:0;">
                  </td>
               </tr>
               <tr>
                  <td colspan="1"></td>
                  <td colspan="2" style="padding-top: 10px;font-size: 14px;
                     font-weight: 500; "><strong> Total Amount:</strong></td>
                  <td style="padding-top: 10px;font-size: 18px;
                     font-weight: 500; "><b>SAR {{$details->grant_total}}</b></td>
               </tr>
               
            </tbody>
         </table>
		 
      {{-- </table> --}}
   </body>
</html>