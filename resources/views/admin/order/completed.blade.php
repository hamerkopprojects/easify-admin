@extends('layouts.master')

@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>ORDER</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <x-order-tab activeTab="complete" />
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class=" row">
                        <div class="card col-lg-8">
                            <div class="card-body">
                                <form id="posts-filter" method="get">
                                    <div class="row tablenav top text-left">
                                        <div class="col-md-6 ml-0">
                                            <input class="form-control" type="text" name="search" value="{{$search ?? ''}}"
                                                placeholder="Search by Order Id">
                                        </div>
                                        <div class="col-md-6 ml-0">
                                            <select class="select2 form-control" id="search_select" name="search_select"
                                                placeholder="by customer">
                                                <option value="">By Customer</option>
                                                @foreach ($customers as $item)
                                                
                                                <option value="{{$item->id}}"
                                                    {{ $item->id == $cust_id ? "selected" :""}}>{{$item->cust_name}}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                    <div class="row tablenav top text-left">
                                        <div class="col-md-6 ml-0">
                                            <input class="form-control datetimepicker" type="text" name="date"
                                                value="{{$date ?? ''}}" placeholder="By scheduled delivery date">
                                        </div>
                                        <div class="col-md-6 ml-0">
                                            
                                            <select class="form-control search_val" name="region">
                                                <option value="">Select region</option>
                                                @foreach ($regions as $single)
                                                    <option value="{{$single->lang[0]->name}}"
                                                {{ $single->lang[0]->name == $search_region ? "selected" :""}}>{{$single->lang[0]->name}}
                                                </option>
                                                @endforeach

                                            </select>
                                        </div>

                                    </div>
                                    <div class="row tablenav top text-left">
                                        <div class="col-md-12 text-right">
                                            <button type="submit" class="btn btn-info">
                                                <font style="vertical-align: inherit;">Search</font>
                                            </button>
                                            <a href="{{route('admin.order-completed')}}" class="btn btn-default">Reset</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="card-header card-header-warning card-header-icon card_style_rev">
                                    <h4 class="card-title">Total revenue from all bookings</h4>
                                    <p></p>
                                    <h3 class="center"><b>SAR {{$grant_sum}}</b></h3>
                                </div>
                           <div class="">
                            @if($grant_sum >0)
                            <a href="{{route('admin.order.excel-download')}}" class="btn btn-info waves-effect waves-light mt-3">Download report as XLS</a>
                            @endif

                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div id="msgDiv"></div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>

                                        <tr>
                                            <th>S.No.</th>
                                            <th>Order ID</th>
                                            <th>Customer</th>
                                            <th>Number of <br>supplier</th>
                                            <th>Delivery Date</th>
                                            <th>Amount</th>
                                            <th width="10%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($active) > 0 )
                                        @php
                                        $i = 1;
                                        @endphp
                                        @foreach ($active as $item)
                                        <tr>
                                            @php
                                            $supplier= DB::table('order_items')
                                            ->where('order_id',$item->id)
                                            ->distinct('supplier_id')
                                            ->count('supplier_id');
                                            @endphp
                                            <td>{{$i++}}</td>
                                            <td>{{$item->order_id}}</td>
                                            <td>{{ucfirst($item->customer->cust_name) }}</td>
                                            <td>{{$supplier}}</td>
                                            <td>@if(isset($item->delivery_schedule_date))
                                                {{ \Carbon\Carbon::parse($item->delivery_schedule_date)->format('d-m-Y')}}
                                                @else
                                                <p></p>
                                                @endif
                                            </td>
                                            <td>SAR {{$item->grant_total}}</td>

                                            <td class="text-center">
                                                <a href="{{ route('admin.order-details', $item->id) }}"
                                                    class="btn btn-sm btn-success text-white view_btn"
                                                    title="Order Details"><i class="fa fa-eye"></i></a>
                                                    <a href="{{route('order-qrcode',$item->id)}}"class="btn btn-sm btn-danger waves-effect" title="print order QR code"  target="_blank"><i class="fa fa-print"></i></a>

                                            </td>
                                        </tr>
                                        @endforeach

                                        @else
                                        <tr>
                                            <td colspan="8" class="text-center">No records found!</td>
                                        </tr>

                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center d-flex justify-content-center mt-3">
                                {{ $active->links() }}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
   <style>
     .card-stats {
        margin-top: 26px;
        margin-left: 20px;
}
   </style>
@endpush
@push('scripts')
<script>
    $('input[name="date"]').daterangepicker({
        "singleDatePicker": true,
        "autoUpdateInput": false,
        "autoApply": true,
        // "minDate": new Date(),
        locale: {
                format: 'DD-MM-YYYY'
            }
    });
    $('input[name="date"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });
    </script>
    
@endpush