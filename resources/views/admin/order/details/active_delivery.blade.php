@extends('layouts.master')

@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>Order Details</h1>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="page-header">
                        <div class="page-title">
                            {{-- <div class="col-md-12"> --}}
                        
                            <a href="{{route('admin.order-delivery')}}" class="cust_stylee btn back_btn">Back</a>
                            {{-- </div> --}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <a href="{{route('admin.order-details.pdf',$details->id)}}"class="cust_styl2 btn btn-info waves-effect waves-light" target="_blank"><i class="fa fa-print"></i> Print</a>
                        <button class="btn btn-info waves-effect waves-light cust_styl2 order_schedule" data-order_id="{{$details->id}}">Schedule delivery</button>
                        <button class="btn btn-info waves-effect waves-light cust_styl2 mark_as_complete" data-order_id="{{$details->id}}">Mark as completed</button>
                        <button class="btn btn-info waves-effect waves-light order_cancel" onclick="cancelOrder({{$details->id}})">Cancel</button>
                    </div>
                </div>
                <form id="frm_create_product" action="javascript:;" method="POST">
                    <div class="tab-pane active" id="pdt_info" role="tabpanel">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label"><b>ORDER INFO</b></label>
                                    <br />

                                </div>
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>Order ID</strong></label><br>
                                    <p>{{$details->order_id}}</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>Order date</strong></label><br>
                                    <p> {{\Carbon\Carbon::parse($details->created_at)->format('d F y')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>Delivery date</strong></label><br>                         
                                    <p>{{\Carbon\Carbon::parse($details->delivery_schedule_date)->format('d F y')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>Customer</strong></label><br>
                                    <p> {{ucfirst($details->customer->cust_name)}}</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>Order status</strong></label><br>
                                    <p> {{$ord_status[$details->order_status]}}</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>Driver info</strong></label><br>
                                    <p> {{ucfirst($details->driver->name)}}</p>
                                </div>

                                <div class="col-md-4">
                                    <label class="control-label"><strong>Delivery location</strong></label><br>
                                        <p>{{$details->delivery_loc}}</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>Payment method</strong></label><br>
                                        <p>{{ucfirst($details->payment_method)}}</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>Promo code</strong></label><br>
                                        <p>{{$details->coupon_code ?? ''}}</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>Distance</strong></label><br>
                                        <p></p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>No of suppliers</strong></label><br>
                                        <p>{{$supplier}}</p>
                                </div>
                                @if($details->order_note)
                                <div class="col-md-4">
                                    <label class="control-label"><strong>Delivery note</strong></label><br>
                                        <p>{{$details->order_note}}</p>
                                </div>
                                @endif
                                @if($details->order_status == 8)
                                <div class="col-md-6">
                                    <label class="control-label"><strong>Cancellation request from driver</strong></label><br>
                                    <button type="submit" class="btn btn-info waves-effect waves-light accept_order_cancel" data-order="{{$details->id}}">
                                        <i class="fa fa-check" aria-hidden="true">Accept</i>
                                    </button>
                                    <button type="button" class="btn btn-default waves-effect reject_order_cancel" data-order="{{$details->id}}">
                                        <i class="fa fa-times" aria-hidden="true" >Reject</i>

                                        </button>
                                </div>
                                @endif
                                <br>
                                <div class="col-md-12">
                                    <hr>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><b>Delivery Address</b></label><br>
                                    <strong>{{$details['delivery_name']}}</strong><br>
                                    <strong>{{$details['delivery_phone']}}</strong><br>
                                    {{$details['delivery_address']}}<br>
                                    {{$details['delivery_city']}}<br>
                                    P O Box:{{$details['delivery_zip']}}<br>
                                    {{$details['delivery_country']}}

                                </div>
                                <div class="col-md-6 ">
                                    <label class="control-label"><b>Billing Address</b></label><br>
                                    <strong>{{$details['billing_name']}}</strong><br>
                                    <strong>{{$details['billing_phone']}}</strong><br>
                                    {{$details['billing_address']}}<br>
                                    {{$details['billing_city']}}<br>
                                    P O Box:{{$details['billing_zip']}}<br>
                                    {{$details['billing_country']}}

                                </div>
                                <br>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Supplier</th>
                                            <th>Items</th>
                                            <th>Price</th>
                                            <th>Quantity</th>
                                            <th>Amount</th>                                          
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                        $item_list= $details->items->where('status','!=',3)
                                        ->groupBy('supplier_id');
                                        @endphp
                                        @foreach($item_list as $new_item)
                                        <tr>
                                            {{-- <input type="hidden" value="{{$new_item[0]->supplier_id}} na"> --}}
                                            <td>{{$new_item[0]->supplier->code}}</td>
                                           
                                            <td>
                                                @foreach ($new_item as $item)
                                                <p> {{$item->product->lang[0]->name}}</p>
                                                @endforeach

                                            </td>
                                            <td>
                                                @foreach ($new_item as $item)
                                                <p> {{$item->item_price}}</p>
                                                @endforeach
                                                
                                            </td>
                                            <td>
                                                @foreach ($new_item as $item)
                                                <p> {{$item->item_count}}</p>
                                                @endforeach
                                            </td>
                                            <td>
                                                @foreach ($new_item as $item)
                                                <p> {{$item->grant_total}}</p>
                                                @endforeach
                                            </td>
                                            
                                        </tr>
                                        @endforeach



                                        <input type="hidden" name="hidden_quotation" id="hidden_quotation">
                                        {{-- @else
                                        <tr>
                                            <td colspan="12" class="text-center">No records found!</td>
                                        </tr>
                                        @endif --}}
                                    </tbody>
                                </table>
                                <br>
                                <div class="col-md-6"></div>
                                <div class="col-md-3">
                                    <label class="control-label">Sub Total</label><br>
                                </div>
                                <input type="hidden" value="{{$details->id}}" id="oreder_hidden">
                                <div class="col-md-3">
                                    <label class="control-label">{{$details->sub_total}} </label><br>
                                </div>
                                <div class="col-md-6"></div>
                                <div class="col-md-3">
                                    <label class="control-label">Delivery charge</label><br>
                                </div>
                                <div class="col-md-3">
                                    <label class="control-label">SAR {{$details->delivery_charge ?? '0.00'}}</label><br>
                                </div>
                                <div class="col-md-6"></div>
                                <div class="col-md-3">
                                    <label class="control-label">COD Fee</label><br>
                                </div>
                                <div class="col-md-3">
                                    <label class="control-label">SAR {{$details->cod_fee ?? '0.00'}}</label><br>
                                </div>
                                <div class="col-md-4">
                                </div>
                                <div class="col-md-8">
                                    <hr>
                                </div>
                                <div class="col-md-6">

                                </div>
                                <div class="col-md-3">
                                    <label class="control-label"><strong> TOTAL AMOUNT</strong></label>
                                </div>
                                <div class="col-md-3">

                                    <label class="control-label"><strong>SAR {{$details->grant_total}}</strong></label>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<!-- Modal pooup -->
<div class="modal none-border popupcontent_model" id="formModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="max-height:85%;  margin-top: 50px; margin-bottom:50px;">
        <div class="modal-content popupcontent">

        </div>
    </div>
</div>
<!-- END MODAL -->
@include('admin.order.details.cust_js_common')

@endsection
@push('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"> 
<style>
  .cust_stylee{
    float: left;
    margin-left: 184px;
  }
  /* .cust_styl{ */
        /* position: absolute;
        top: 10px;
        right: 10px; */
        /* cursor: pointer; */
    
    /* }  */
    .cust_styl2{
        /* position: absolute;
        top: 10px;
        right: 100px; */
        cursor: pointer;
    }
</style>
@endpush
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script> 
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
$('.accept_order_cancel').on('click',function(){
    let order_id = $(this).data("order");
        $.confirm({
            title: 'Confirmation',
            content: 'Are you sure to accept cancellation',
            buttons: {
            Yes: function () {
            $.ajax({
            type: "POST",
                    url: "{{route('admin.cancelsingle-order')}}",
                    data: {
                        order:order_id,
                        type :"accept"
                        },
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    window.setTimeout(function () {
                   window.location.reload();
                    }, 1000);
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            },
                    No: function () {
                    window.location.reload();
                    }
            }
    });
});
$('.reject_order_cancel').on('click',function(){
    let order_id = $(this).data("order");
        $.confirm({
            title: 'Confirmation',
            content: 'Are you sure to reject cancellation',
            buttons: {
            Yes: function () {
            $.ajax({
            type: "POST",
                    url: "{{route('admin.cancelsingle-order')}}",
                    data: {
                        order:order_id,
                        type :"reject"
                        },
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    window.setTimeout(function () {
                   window.location.reload();
                    }, 1000);
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            },
                    No: function () {
                    window.location.reload();
                    }
            }
    });

});
$('.mark_as_complete').on('click',function(){
    let order_id = $(this).data("order_id");
        $.confirm({
            title: 'Confirmation',
            content: 'Are you sure to complete the order',
            buttons: {
            Yes: function () {
            $.ajax({
            type: "POST",
                    url: "{{route('admin.orderComplete')}}",
                    data: {
                        order:order_id,
                        },
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    window.setTimeout(function () {
                   window.location.reload();
                    }, 1000);
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            },
                    No: function () {
                    window.location.reload();
                    }
            }
        });
});
$('.order_schedule').on('click',function(){
        
    let order_id = $(this).data("order_id");
        
        $.ajax({
            type: "POST",
            url: "{{route('admin.schedule-form')}}",
            data:{
                'order_id':order_id,
                'type':"complete_schedule",
                'supplier_id': ''
            },
            success: function(data) {
                $('.modal-content').html(data);
                $('#formModal').modal('show');
            }
        });
    })
</script>
@endpush