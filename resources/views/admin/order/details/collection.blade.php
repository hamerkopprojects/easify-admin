@extends('layouts.master')

@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>Order Details</h1>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="page-header">
                        <div class="page-title">
                            
                            <a href="{{route('admin.order-collection')}}" class="cust_stylee btn back_btn">Back</a>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <a href="{{route('admin.order-details.pdf',$details->id)}}"class=" btn btn-info waves-effect waves-light" target="_blank"><i class="fa fa-print"></i> Print</a>
                        <button class="btn btn-info waves-effect waves-light order_cancel" onclick="cancelOrder({{$details->id}})"> Cancel </button>
                    </div>
                </div>
                <form id="frm_create_product" action="javascript:;" method="POST">
                    <div class="tab-pane active" id="pdt_info" role="tabpanel">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label"><b>ORDER INFO</b></label>
                                    <br />

                                </div>
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>Order ID</strong></label><br>
                                    <p>{{$details->order_id}}</p>
                                    <input type="hidden" id="current_order" value="{{$details->id}}">
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>Order date</strong></label><br>
                                    <p> {{\Carbon\Carbon::parse($details->created_at)->format('d F y')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>Collection Status</strong></label><br>

                                    <p>{{$ord_status[$details->order_status]}}</p>
                                </div>

                                <div class="col-md-4">
                                    <label class="control-label"><strong>No.of Suppliers </strong></label><br>
                                    <p>{{$supplier}}</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>Customer</strong></label><br>
                                <p>@if($details->customer){{ucfirst($details->customer->cust_name)}}@endif</p>
                                </div><br>
                                <div class="col-md-12">
                                    <hr>
                                </div>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Supplier /<br>Branch</th>

                                            <th>Distance</th>
                                            <th>Items</th>
                                            <th>Quantity</th>
                                            <th>Driver</th>
                                            <th>Collection Date</th>
                                            <th>Product Status</th>
                                            @if(in_array($details->order_status,[3,4]))
                                                <th></th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                        // if(isset($details->items->branch_id))
                                        // {
                                        $item_list= $details->items->groupBy('branch_id');
                                        // }
                                        // else{
                                        // $item_list= $details->items->groupBy('supplier_id');
                                        // }
                                        @endphp

                                        @foreach($item_list as $new_item)
                                        <tr>
                                            @php
                                            
                                            $Supplier_code = $new_item[0]->branch->code;
                                            
                                            $supplier =\App\Models\Supplier::where('id',$new_item[0]->branch_id)
                                            ->with(['supplier_region' => function ($q) {
                                            $q->with('lang');
                                            }])
                                            ->first();
                                           $supplier_cod = $supplier->supplier_region->coordinates;
                                           
                                            $other = \App\Models\OtherSettings::first();
                                            // dd($supplier_cod);
                                            if (empty($supplier_cod) || $supplier_cod == "null") {
                                            $km = 0 ;
                                            }else{
                                                $supp_loc = json_decode($supplier->supplier_region->coordinates);

                                            $wherehouse = json_decode($other->warehouse_loc);
                                           
                                            $theta = $wherehouse->longitude - $supp_loc[0]->lng;
                                            $distance = sin(deg2rad($wherehouse->latitude)) *
                                            sin(deg2rad($supp_loc[0]->lat)) + cos(deg2rad($wherehouse->latitude)) *
                                            cos(deg2rad($supp_loc[0]->lat)) * cos(deg2rad($theta));
                                            $distance = acos($distance);
                                            $distance = rad2deg($distance);
                                            $miles = $distance * 60 * 1.1515;
                                            $km = $miles * 1.609344;
                                            }

                                            
                                            @endphp
                                            
                                            {{-- <input type="hidden" value="{{$new_item[0]->supplier_id}} na"> --}}
                                            <td>
                                                
                                                {{$Supplier_code}}
                                            {{-- @foreach ($new_item as $my_item) --}}
                                            @php
                                                // $my_pro_stat = $my_item->status ;
                                                $stat_count = $new_item->where('status',3)->count(); 
                                                $item_ct=$new_item->count()
                                            @endphp
                                                
                                            {{-- @endforeach --}}
                                            @if($stat_count != $item_ct )
                                            {{-- (in_array($my_pro_stat,[2,3,4])) --}}
                                            <p>
                                                <a class="btn btn-sm btn-success text-white change_time_schedule"
                                                title="Change time schedule" data-branch_id={{$new_item[0]->branch_id}}>Schedule collection</a> 
                                            </p>
                                            @endif
                                            </td>
                                            <td>
                                                {{-- KM --}}
                                                @if(isset($km)) {{round($km,2)}} KM @endif
                                            </td>
                                            <td>
                                                @foreach ($new_item as $item)
                                                <p> {{$item->product->lang[0]->name}}</p>
                                                @endforeach

                                            </td>

                                            <td>
                                                @foreach ($new_item as $item)
                                                <p> {{$item->item_count}}</p>
                                                @endforeach
                                            </td>
                                            <td>

                                                @if($new_item[0]->driver_id)
                                                <p>{{$new_item[0]->driver->name}}</p>
                                                @else
                                                <p>Select Driver</p>
                                                @endif
                                            </td>
                                            <td>

                                                @if(isset($new_item[0]->collection_date))
                                                <p> {{\Carbon\Carbon::parse($new_item[0]->collection_date)->format('d F y')}}
                                                    @if (isset($new_item[0]->collection_time_slot))-{{$new_item[0]->collection_time_slot}}@endif</p>
                                                @else
                                                <p>Select collection date</p>
                                                @endif
                                              
                                            </td>

                                            <td>@foreach ($new_item as $item)
                                                <p>
                                        
                                                     {{$pro_status[$item->status]}} 

                                                @endforeach
                                            </td>
                                            @if(in_array($details->order_status,[3,4]))
                                               
                                            <td>
                                                @foreach ($new_item as $item)
                                                @if($item->status != 3)

                                                <p>
                                                    
                                                    <a class="btn btn-sm btn-success text-white change_collection_status"
                                                    title="Change time schedule" data-branch_id="{{$new_item[0]->branch_id}}" data-item_id="{{$item->id}}">mark as collected</a> 
                                                </p>
                                                @endif
                                                @endforeach
                                            
                                            </td>
                                            @endif
                                            {{-- <td>
                                                <a class="btn btn-sm btn-success text-white change_time_schedule"
                                                    title="Change time schedule" data-branch_id={{$new_item[0]->branch_id}}><i
                                                        class="fa fa-clock-o"></i></a> 
                                            </td> --}}
                                        </tr>
                                        @endforeach



                                        <input type="hidden" name="hidden_quotation" id="hidden_quotation">
                                        {{-- @else
                                        <tr>
                                            <td colspan="12" class="text-center">No records found!</td>
                                        </tr>
                                        @endif --}}
                                    </tbody>
                                </table>
                                <br>
                                <div class="col-md-6"></div>
                                <div class="col-md-3">
                                    <label class="control-label">Sub Total</label><br>
                                </div>
                                <div class="col-md-3">
                                    <label class="control-label"> SAR {{$details->sub_total}} </label><br>
                                </div>
                                <div class="col-md-6"></div>
                                <div class="col-md-3">
                                    <label class="control-label">Delivery charge</label><br>
                                </div>
                                <div class="col-md-3">
                                    <label class="control-label">SAR {{$details->delivery_charge ?? '0.00'}}</label><br>
                                </div>
                                <div class="col-md-6"></div>
                                <div class="col-md-3">
                                    <label class="control-label">COD Fee</label><br>
                                </div>
                                <div class="col-md-3">
                                    <label class="control-label">SAR {{$details->cod_fee ?? '0.00'}}</label><br>
                                </div>
                                @php
                               
                                if(isset($details->coupon_code)){
                                if($details->coupon_type == 'amount'){
                                $coupon_value = 'SAR '.number_format($details->coupon_value, 2, ".", "");
                                }
                                else {
                               
                                $coupon_value = $details->coupon_value . '%';
                                }
                                }
                                @endphp
                                @if(isset($details->coupon_code))
                                <div class="col-md-6"></div>
                                <div class="col-md-3">
                                    <label class="control-label">Discount</label><br>
                                </div>
                                <div class="col-md-3">
                                    <label class="control-label">{{$coupon_value}}</label><br>
                                </div>
                                @endif
                                <div class="col-md-4">
                                </div>
                                <div class="col-md-8">
                                    <hr>
                                </div>
                                <div class="col-md-6">

                                </div>
                                <div class="col-md-3">
                                    <label class="control-label"><strong> TOTAL AMOUNT</strong></label>
                                </div>
                                <div class="col-md-3">
                                    
                                    <label class="control-label"><strong>SAR {{$details->grant_total}}</strong></label>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
@include('admin.order.details.cust_js_common')
<!-- Modal pooup -->
<div class="modal none-border popupcontent_model" id="formModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="max-height:85%;  margin-top: 50px; margin-bottom:50px;">
        <div class="modal-content popupcontent">

        </div>
    </div>
</div>
<!-- END MODAL -->
@endsection
@push('css')
<link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
<style>
    .cust_stylee {
        float: left;
        margin-left: 184px;
    }

    .change_time_schedule {
        cursor: pointer;
    }

    .arrow {
        border: solid black;
        border-width: 0 3px 3px 0;
        display: inline-block;
        padding: 3px;
    }

    .down {
        transform: rotate(45deg);
        -webkit-transform: rotate(45deg);
    }
    
    .cust_styl{
        position: absolute;
        top: 10px;
        right: 10px;
    }
    .cust_styl2{
        position: absolute;
        top: 10px;
        right: 100px;
        cursor: pointer;
    }
</style>
@endpush
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
        $('.table').on('click','.change_time_schedule',function(){
        
        let currentorder_id= $('#current_order').val();
        let branch_id =$(this).data('branch_id');
        
        $.ajax({
            type: "POST",
            url: "{{route('admin.schedule-form')}}",
            data:{
                'order_id': currentorder_id,
                'branch_id':branch_id
            },
            success: function(data) {
                $('.modal-content').html(data);
                $('#formModal').modal('show');
            }
        });
    })
    $('.mark_collect').on('click',function(){
        let $status = 3;
        let order = $(this).data("order_id");
        $.confirm({
            title: 'Confirmation',
            content: 'Are you sure to change to collection ?',
            buttons: {
            Yes: function () {
            $.ajax({
            type: "POST",
                    url: "{{route('admin.changeStatus')}}",
                    data: {
                        order:order,
                        status:status
                        },
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    window.setTimeout(function () {
                   window.location.reload();
                    }, 1000);
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            },
                    No: function () {
                    window.location.reload();
                    }
            }
    });
    });
$('.change_collection_status').on('click',function(){
    let currentorder_id= $('#current_order').val();
    let branch_id =$(this).data('branch_id');
    let item_id = $(this).data('item_id');
    $.ajax({
            type: "POST",
            url: "{{route('admin.collection_pending')}}",
            data:{
                'order_id': currentorder_id,
                'branch_id':branch_id,
                "item_id":item_id
            },
            success: function(data) {
                $('.modal-content').html(data);
                $('#formModal').modal('show');
            }
        });
});

</script>
@endpush