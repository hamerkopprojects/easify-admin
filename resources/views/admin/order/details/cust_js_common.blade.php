
<script>
    function cancelOrder(ord_id){

    $.ajax({
            type: "GET",
            url: "{{route('admin.order_cancel')}}",
            data: {
                id: ord_id
            },
            success: function(data) {
                $('.modal-content').html(data);
                $('#formModal').modal('show');
            }
        });
    }
</script>