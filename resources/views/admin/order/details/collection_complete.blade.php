@extends('layouts.master')

@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>Order Details</h1>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="page-header">
                        <div class="page-title">
                            {{-- <div class="col-md-12"> --}}

                            <a href="{{route('admin.order-collection')}}" class="cust_stylee btn back_btn">Back</a>
                            {{-- </div> --}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <a href="{{route('admin.order-details.pdf',$details->id)}}"class="cust_styl2 btn btn-info waves-effect waves-light" target="_blank"><i class="fa fa-print"></i> Print</a>
                        <button class="btn btn-info waves-effect waves-light cust_styl2 order_schedule">Schedule delivery</button>
                        <button class="btn btn-info waves-effect waves-light order_cancel" onclick="cancelOrder({{$details->id}})">Cancel</button>
                    </div>
                </div>
                <form id="frm_create_product" action="javascript:;" method="POST">
                    <div class="tab-pane active" id="pdt_info" role="tabpanel">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label"><b>ORDER INFO</b></label>
                                    <br />

                                </div>
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>Order ID</strong></label><br>
                                    <p>{{$details->order_id}}</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>Order date</strong></label><br>
                                    <p> {{\Carbon\Carbon::parse($details->created_at)->format('d F y')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>Collection Status</strong></label><br>
                                    <p>{{ucfirst($ord_status[$details->order_status])}}</p>
                                </div>

                                {{-- <div class="col-md-4">
                                    <label class="control-label"><strong>No.of Suppliers </strong></label><br>
                                        <p>{{$supplier}}</p>
                            </div> --}}
                            <div class="col-md-4">
                                <label class="control-label"><strong>Customer</strong></label><br>
                                <p> {{ucfirst($details->customer->cust_name)}}</p>
                            </div>
                            {{-- <div class="col-md-4">
                                <label class="control-label"><strong>Delivery date</strong></label><br>
                                <p> </p>
                            </div> --}}

                            {{-- <div class="col-md-4">
                                <label class="control-label"><strong>Schedule</strong></label><br>
                                
                            </div> --}}
                            <br>
                            <div class="col-md-12">
                                <hr>
                            </div>
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Supplier</th>
                                        <th>Distance</th>
                                        <th>Items</th>
                                        <th>Quantity</th>
                                        <th>Collected by</th>
                                        <th>Collection Date</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $item_list= $details->items->where('status','!=',3)
                                    ->groupBy('supplier_id');

                                    @endphp
                                    @foreach($item_list as $new_item)
                                    <tr>
                                        @php
                                            $supplier =\App\Models\Supplier::where('id',$new_item[0]->branch_id)
                                            ->with(['supplier_region' => function ($q) {
                                            $q->with('lang');
                                            }])
                                            ->first();
                                           $supplier_cod = $supplier->supplier_region->coordinates;
                                           
                                            $other = \App\Models\OtherSettings::first();
                                            // dd($supplier_cod);
                                            if (empty($supplier_cod) || $supplier_cod == "null") {
                                            $km = 0 ;
                                            }else{
                                                $supp_loc = json_decode($supplier->supplier_region->coordinates);

                                            $wherehouse = json_decode($other->warehouse_loc);
                                           
                                            $theta = $wherehouse->longitude - $supp_loc[0]->lng;
                                            $distance = sin(deg2rad($wherehouse->latitude)) *
                                            sin(deg2rad($supp_loc[0]->lat)) + cos(deg2rad($wherehouse->latitude)) *
                                            cos(deg2rad($supp_loc[0]->lat)) * cos(deg2rad($theta));
                                            $distance = acos($distance);
                                            $distance = rad2deg($distance);
                                            $miles = $distance * 60 * 1.1515;
                                            $km = $miles * 1.609344;
                                            }
                                        @endphp
                                        <td>{{$new_item[0]->supplier->code}}</td>
                                        <td>{{round($km,2)}} KM</td>
                                        <td>
                                            @foreach ($new_item as $item)
                                            <p> {{$item->product->lang[0]->name}}</p>
                                            @endforeach

                                        </td>

                                        <td>
                                            @foreach ($new_item as $item)
                                            <p> {{$item->item_count}}</p>
                                            @endforeach
                                        </td>
                                        <td class="change_time_schedule" data-supplier-id={{$new_item[0]->supplier_id}}
                                            data-order-id={{$details->id}}>

                                            @if($new_item[0]->driver_id)
                                            <p>{{$new_item[0]->driver->name}}</p>
                                            @else
                                            <p></p>
                                            @endif
                                        </td>
                                        <td class="change_time_schedule" data-supplier-id={{$new_item[0]->supplier_id}}
                                            data-order-id={{$details->id}}>
                                            @if(isset($new_item[0]->collection_date))
                                                    {{\Carbon\Carbon::parse($new_item[0]->collection_date)->format('d F y')}}
                                            {{$new_item[0]->collection_time_slot}}
                                            @endif
                                            
                                        </td>


                                    </tr>
                                    @endforeach



                                    <input type="hidden" name="hidden_quotation" id="hidden_quotation">
                                    {{-- @else
                                        <tr>
                                            <td colspan="12" class="text-center">No records found!</td>
                                        </tr>
                                        @endif --}}
                                </tbody>
                            </table>
                            <br>
                            <div class="col-md-6"></div>
                            <div class="col-md-3">
                                <label class="control-label">Sub Total</label><br>
                            </div>
                            <input type="hidden" value="{{$details->id}}" id="oreder_hidden">
                            <div class="col-md-3">
                                <label class="control-label">{{$details->sub_total}} </label><br>
                            </div>
                            <div class="col-md-6"></div>
                            <div class="col-md-3">
                                <label class="control-label">Delivery charge</label><br>
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">SAR {{$details->delivery_charge ?? '0.00'}}</label><br>
                            </div>
                            <div class="col-md-6"></div>
                            <div class="col-md-3">
                                <label class="control-label">COD Fee</label><br>
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">SAR {{$details->cod_fee ?? '0.00'}}</label><br>
                            </div>
                            @php
                               
                            if(isset($details->coupon_code)){
                            if($details->coupon_type == 'amount'){
                            $coupon_value = 'SAR '.number_format($details->coupon_value, 2, ".", "");
                            }
                            else {
                           
                            $coupon_value = $details->coupon_value . '%';
                            }
                            }
                            @endphp
                            @if(isset($details->coupon_code))
                            <div class="col-md-6"></div>
                            <div class="col-md-3">
                                <label class="control-label">Discount</label><br>
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">{{$coupon_value}}</label><br>
                            </div>
                            @endif
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-8">
                                <hr>
                            </div>
                            <div class="col-md-6">

                            </div>
                            <div class="col-md-3">
                                <label class="control-label"><strong> TOTAL AMOUNT</strong></label>
                            </div>
                            <div class="col-md-3">
                                @php
                                   $total_amt =  $details->cod_fee +$details->delivery_charge+$details->sub_total;
                                @endphp
                                <label class="control-label"><strong>SAR {{$details->grant_total}}</strong></label>
                            </div>
                        </div>
                    </div>

            </div>
            </form>
        </div>

    </div>
</div>
</div>
<!-- Modal pooup -->
<div class="modal none-border popupcontent_model" id="formModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="max-height:85%;  margin-top: 50px; margin-bottom:50px;">
        <div class="modal-content popupcontent">

        </div>
    </div>
</div>
<!-- END MODAL -->
@include('admin.order.details.cust_js_common')

@endsection
@push('css')
<link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
<style>
    .cust_stylee {
        float: left;
        margin-left: 184px;
    }
    /* .cust_styl{ */
        /* position: absolute;
        top: 10px;
        right: 10px; */
        /* cursor: pointer; */
    /* } */
    .cust_styl2{
        /* position: absolute;
        top: 10px;
        right: 100px; */
        cursor: pointer;
    }
</style>
@endpush
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
        $('.order_schedule').on('click',function(){
        
        let order_id= $('#oreder_hidden').val();
        
        $.ajax({
            type: "POST",
            url: "{{route('admin.schedule-form')}}",
            data:{
                'order_id':order_id,
                'type':"complete_schedule",
                'supplier_id': ''
            },
            success: function(data) {
                $('.modal-content').html(data);
                $('#formModal').modal('show');
            }
        });
    })
</script>
@endpush