
<div class="modal-header">
    <h4 class="modal-title"><strong>
      
            Cancel Order
        </strong></h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

</div>
<form id="frm_cancel_order" class="frm_cancel_order" action="javascript:;" method="POST">
    <div class="tab-pane active" id="order_cancel" role="tabpanel">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-8">
                    <label class="control-label">Are you sure want to cancel the order</label>
                </div>
                <div class="col-md-8">
                    
                    <select class="form-control" name="cancel_reason_select" id="cancel_reason_select">
                        <option value=""> Select reason</option>
                        @foreach($cancellationReason as $reason)
                        <option value="{{ $reason->id }}">{{ $reason->lang[0]->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-8">
                    <textarea class="form-control area" name="cancel_reason_comment" id="cancel_reason_comment" placeholder="Enter description (EN)"></textarea>
                </div>
                <input type="hidden" value="{{$order_id}}" name="hidden_order" id="hidden_order">
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-info waves-effect waves-light save-btn">
                Save
            </button>
            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>

        </div>
    </div>
</form>
<script>
    
$(".frm_cancel_order").validate({
        
        rules: {
            cancel_reason_select : {
                required: true,
            },
            
            
        },
        messages: {
            cancel_reason_select: {
                required: 'Cancellation reason required'
            },
            // cancel_reason_comment: {
            //     required: 'Product name(EN) is required.'
            // },
           
        },
        submitHandler: function (form) {
            let reason=$('#cancel_reason_select').val();
            let comment = $('#cancel_reason_comment').val();
            let order_id = $('#hidden_order').val();
            $.ajax({
                type: "POST",
                url: "{{route('admin.cacel.my.order')}}",
                data:{
                    reason:reason,
                    comment:comment,
                    order_id:order_id
                },
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data.status == 1) {
                       
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                           window.location.reload();
                        }
                     else {
                        Toast.fire({
                            icon: 'error',
                            title:"Some error !!!"
                        });
                        
                    }
                    $('button:submit').attr('disabled', false);
                },
                error: function (err) {
                    $('button:submit').attr('disabled', false);
                }
            });
            return false;
        }
    });
</script>

