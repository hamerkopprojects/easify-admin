
<div class="modal-header">
    <h4 class="modal-title"><strong>
      
            Change Schedule
        </strong></h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

</div>
<form id="frm_schedule_order" class="frm_schedule_order" action="javascript:;" method="POST">
    <div class="tab-pane active" id="order_schedule" role="tabpanel">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <label class="control-label">Order Status</label><br>
                    <select class="form-control " id="status_select">
                        <option value="">Select Status</option>
                        <option value="7"{{$order_details['order_status'] == 7 ? "selected" :""}}>Schedule for delivery</option>
                        <option value="6">Cancelled</option>
                        <option value="9">Delivered</option>  
                    </select>
                </div>
                <div class="col-md-6">
                    <label class="control-label">Driver</label><br>
                    <select class="form-control" id="driver_select">
                        <option value="">Select Driver</option>
                        @foreach ($drivers as $driver)
                             <option value={{$driver->id}} {{ $driver->id == $order_details->driver_id ? "selected" :""}}>{{$driver->name ?? ''}}</option>
                        
                         @endforeach
                    </select>
                    
                </div>
                <div class="col-md-6">
                    <label class="control-label">Select time</label><br>
                <input class="datepicker form-control" type="text" name="delivery_schedule"
                value="{{\Carbon\carbon::parse($order_details->delivery_schedule_date)->format('d-m-Y')}}"
                />
                    
                </div>
                <div class="col-md-6" id="slot-list">
                    <label class="control-label">Time slots</label><br>
                    <div class="time-slot @if($order_details->time_slot =='9 AM to 1 PM') selected @endif">9 AM to 1 PM</div>
                    <div class="time-slot @if($order_details->time_slot =='2 PM to 5 PM') selected @endif">2 PM to 5 PM</div>
                    <input type='hidden' name='time_slot' id='time_slot' value="{{ $order_details->time_slot ?? '' }}">
                </div>
                <input type="hidden" value="{{$order_id}}" name="hidden_order" id="hidden_order">
                <input type="hidden" value="{{$branch_id}}" name="hidden_supplier" id="hidden_supplier">
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-info waves-effect waves-light save-btn">
                Save
            </button>
            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>

        </div>
    </div>
</form>
<style>
    div.ui-datepicker {
        font-size: 10px;
}
.time-slot {
        border: 1px solid #bfbebe;
        padding: 6px;
        border-radius: 2px;
        display: inline-block;
        margin-bottom: 6px;
        color: black;
        min-width: 100px;
        height: 50px;
        text-align: center;
        line-height: 34px;
    }
    #slot-list .selected {
        background-color: black;
        color: white;
    }
    
</style>
<script>
    $('input[name="delivery_schedule"]').daterangepicker({
        "singleDatePicker": true,
        "autoUpdateInput": false,
        "autoApply": true,
        "minDate": new Date(),
        locale: {
                format: 'DD-MM-YYYY'
            }
    });
    $('input[name="delivery_schedule"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });
    </script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#slot-list').on('click', '.time-slot', function(e) {
        $('#time_slot').val($(this).attr('time_slot'));
           let el = $(this);
           $('.time-slot').removeClass('selected');
           el.addClass('selected');   
    });
    

   $('.save-btn').on('click',function (){
        $('#frm_schedule_order').validate({ 
        rules: {
            driver_select: {
                required:true
            },
            status_select:{
                required:true 
            }
        },
        messages: {
            driver_select: {
                    required: 'Driver required.'
                },
            status_select:{
                required:'Status required'
            }
        },
        submitHandler: function(form) {
            let date = $('.datepicker').val();
            let status =$('#status_select').val();
            let driver =$('#driver_select').val();
            let slot =$('.selected').text();
            let order =$('#hidden_order').val();
            let supplier = $('#hidden_supplier').val();
                $.ajax({
                type: 'POST',
                url:"{{route('order.complete-collection.schedule')}}" ,
                data: {
                    date:date,
                    status:status,
                    driver:driver,
                    slot:slot,
                    order:order,

                },
                success: function(data) {
                    Toast.fire({
                    icon: 'success',
                    title: "Changes updated successfully"
                    });
                    window.location.reload();
                }
                });
             return false;
            }
        });

    });  
</script>

