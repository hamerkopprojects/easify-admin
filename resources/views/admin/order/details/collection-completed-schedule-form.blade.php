
<div class="modal-header">
    <h4 class="modal-title"><strong>
      
            Change Schedule
        </strong></h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

</div>
<form id="frm_schedule_order" class="frm_schedule_order" action="javascript:;" method="POST">
    <div class="tab-pane active" id="order_schedule" role="tabpanel">
        <div class="modal-body">
            <div class="row">
                <input type="hidden" value="7" id="status_select" name="status_select">
                <div class="col-md-6">
                    <label class="control-label">Driver</label><br>
                    <select class="form-control" id="driver_select" name="driver_select">
                        <option value="">Select Driver</option>
                        @foreach ($drivers as $driver)
                             <option value={{$driver->id}} {{$driver->id == $order_details->driver_id  ? "selected" :" "}}>{{$driver->name ?? ''}}</option>
                        
                         @endforeach
                    </select>
                    
                </div>
                <div class="col-md-6">
                    <label class="control-label">Select time</label><br>
                <input class="datepicker form-control" type="text" name="order_date"
                value="{{\Carbon\carbon::parse($order_details->delivery_schedule_date)->format('d-m-y')}}"
                />
                    
                </div>
                <div class="col-md-6" id="slot-list">
                    <label class="control-label">Time slots</label><br>
                    <div class="time-slot @if($order_details->time_slot =='9 AM to 1 PM') selected @endif">9 AM to 1 PM</div>
                    <div class="time-slot @if($order_details->time_slot =='2 PM to 5 PM') selected @endif">2 PM to 5 PM</div>
                    <input type="hidden" id="time_slot_val" name="time_checking" value="{{$order_details->time_slot}}">
                </div>
                <input type="hidden" value="{{$order_id}}" name="hidden_order" id="hidden_order">
                <input type="hidden" value="{{$branch_id}}" name="hidden_supplier" id="hidden_supplier">
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-info waves-effect waves-light save-btn">
                Save
            </button>
            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>

        </div>
    </div>
</form>
<style>
    div.ui-datepicker {
        font-size: 10px;
}
.time-slot {
        border: 1px solid #bfbebe;
        padding: 6px;
        border-radius: 2px;
        display: inline-block;
        margin-bottom: 6px;
        color: black;
        min-width: 100px;
        height: 50px;
        text-align: center;
        line-height: 34px;
    }
    #slot-list .selected {
        background-color: black;
        color: white;
    }
    
</style>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#slot-list').on('click', '.time-slot', function(e) {
           let el = $(this);
           $('.time-slot').removeClass('selected');
           el.addClass('selected');   
           var select_val =$('.selected').text();
           select_val ?   $('#time_slot_val').val('12') :  $('#time_slot_val').val();
    });
//     $(".datepicker").datepicker({
       
//        format: "yyyy/mm/dd",
//        autoclose: true,
//        defaultDate: new Date(),
//        immediateUpdates: true,
//        todayHighlight: true,
//        clearBtn: true

//    }).on('changeDate', function(ev){
//        $.ajax( {
//        type: 'POST',
//        url:"{{route('order.time-slot')}}" ,
//        data: {
//            date: this.value,
//        },
//        success: function(data) {
//            $('#slot-list').empty();
//            $('#slot-list').append(`
//            <label class="control-label">Time slots</label><br>
//                <div class="time-slot ">${data[1]}</div>
//                <div class="time-slot ">${data[2]}</div>
//            `);
//         }
//        });
       
//    });
$('input[name="order_date"]').daterangepicker({
        "singleDatePicker": true,
        "autoUpdateInput": false,
        "autoApply": true,
        "minDate": new Date(),
        locale: {
            format: 'DD-MM-YYYY'
        }
    });
    $('input[name="order_date"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });
   $('.save-btn').on('click',function (){
        $('#frm_schedule_order').validate({ 
            ignore: [],
        rules: {
            driver_select: {
                required: true
            },
            status_select:{
                required:true 
            },
            order_date:{
                required:true
            },
            time_checking:{
                required:true
            }
        },
        messages: {
            driver_select: {
                    required: 'Driver required.'
                },
            status_select:{
                required:'Status required'
            },
            order_date:{
                required:"Date required"
            },
            time_checking:{
                required:"Select time slot"
            }
        },
        submitHandler: function(form) {
            let date = $('.datepicker').val();
            let status =$('#status_select').val();
            let driver =$('#driver_select').val();
            let slot =$('.selected').text();
            let order =$('#hidden_order').val();
            let supplier = $('#hidden_supplier').val();
                $.ajax({
                type: 'POST',
                url:"{{route('order.complete-collection.schedule')}}" ,
                data: {
                    date:date,
                    status:status,
                    driver:driver,
                    slot:slot,
                    order:order,

                },
                success: function(data) {
                    Toast.fire({
                    icon: 'success',
                    title: "Changes updated successfully"
                    });
                    window.location.reload();
                }
                });
             return false;
            }
        });

    });  
</script>

