@extends('layouts.master')


{{-- @section('add-btn')
<a href="{{route('order.complete.list')}} " class="cust_stylee btn back_btn">Back</a>
@endsection --}}

@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">

                            <h1>Completed Order Details</h1>

                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="page-header">
                        <div class="page-title">

                            <a href="{{route('admin.order-completed')}}" class="cust_stylee btn back_btn">Back</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <x-order-complete-tab activeTab="review" :id="$id" />
                </div>
            </div>
            <div class="card">
                <form id="frm_create_product" action="javascript:;" method="POST">
                    <div class="tab-pane active" id="pdt_info" role="tabpanel">
                        <div class="model-body">
                           
                            @if($orderReview)
                            <div class="row">
                                <div class="col-md-12 cust_style row">
                                    <div class="col-md-2">
                                        <div id="rating">{{$orderReview->rating_total?? ''}}</div>
                                        <br>
                                        <p class="stat">{{$status}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        @for ($i = 0; $i < floor($orderReview->rating_total); $i++)
                                                <div class="star">
                                                    <i class="fa fa-star{{ $orderReview->rating_total <= $i ? '-o' : '' }}"
                                                    aria-hidden="true"></i>
                                                </div>
                                        @endfor
                                        <?php $av2 = $orderReview->rating_total - floor($orderReview->rating_total);?>

                                            @if ($av2 > 0.2 && $av2 < 0.8) 
                                            <div class="star">
                                                <i class="fa fa-star{{ $orderReview->rating_total <= $i ? '-o' : '' }}"
                                                    aria-hidden="true"></i>
                                            </div>
                                            @endif
                                    <p>{{$orderReview->customer->cust_name}}</p>
                                    <p>{{$orderReview->message}}</p>
                                    </div>
                                </div>
                            @foreach ($orderReview->reviewSegments as $segment)
                            <div class="row col-md-8">

                                <div class="col-md-4">
                                    {{$segment->segment->lang->firstWhere('language','en')->name ?? '' }}

                                </div>
                                    @for ($i = 0; $i < floor($segment->rating); $i++)
                                        <div class="star">
                                            <i class="fa fa-star{{ $segment->rating <= $i ? '-o' : '' }}"
                                                aria-hidden="true"></i>
                                        </div>
                                    @endfor
                                    <?php $av3 = $segment->rating- floor($segment->rating);?>
                                    @if ($av3 > 0.2 && $av3 < 0.8)
                                     <div class="star">
                                        <i class="fa fa-star{{ $segment->rating <= $i ? '-o' : '' }}"
                                            aria-hidden="true"></i>
                                        </div>               
                                    @endif
                                </div>
                        @endforeach
                    </div>
            </div>
            
            @endif
            @if(count($review) > 0)
                <div class="row">
                    <div class="col-md-12">
                        <label class="control-label label_col">Review on order items</label><br>
                    </div>
                    @foreach ($review as $item)
                    <div class="row col-md-12">

                        <div class="col-md-4">
                            <p class="list">{{$item->product->lang[0]->name}}</p>
                        </div>
                        <div class="col-md-4">

                            @for ($i = 0; $i < 5; ++$i) <i class="fa fa-star{{ $item->rating <= $i ? '-o' : '' }}"
                                aria-hidden="true"></i>
                                @endfor
                                <p>{{$item->review}}</p>
                        </div>
                    </div>

                    @endforeach
                </div>
            @endif
        </div>

    </div>
    </form>
</div>
</div>
</div>
</div>
@endsection
@push('css')
<style>
    .list {
        list-style: disc outside none;
        display: list-item;
    }

    .star {
        display: inline;
        color: #bc1f52;
    }

    .fa-star {
        color: #09764b;
    }

    .cust_stylee {
        float: left;
        margin-left: 184px;
    }

    .cust_style {
        margin-top: 2%;
        left: 2%;
        bottom: 14px;

    }
    .stat{
        margin-top: 32px;
    }
    #rating {
        display: inline;
        background-color: #09764b;
        padding: 16px 32px;
        border-radius: 6px;
        color: white;
        font-size: 1rem;
    }
    .label_col{
        color: black;
    }
</style>

@endpush