<div class="modal-header">
    <h4 class="modal-title"><strong>

            Change Schedule
        </strong></h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

</div>
<form id="frm_schedule_order" class="frm_schedule_order" action="javascript:;" method="POST">
    <div class="tab-pane active" id="order_schedule" role="tabpanel">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    
                    <label class="control-label">Driver</label><br>
                    <select class="form-control" id="driver_select" name="driver_select">
                        <option value="">Select Driver</option>
                        @foreach ($drivers as $driver)
                        <option value={{$driver->id}}
                            @if($collection_stat->driver_id == $driver->id ) 
                            {{"selected"}}
                            @elseif($select_driver->driver == $driver->id)
                            {{"selected"}} 
                            @endif>
                            {{$driver->name ?? ''}}</option>

                        @endforeach
                    </select>

                </div>
                <div class="col-md-6">
                    <label class="control-label">Collection Date</label><br>
                    <input class="datepicker form-control" type="text" name="order_date"
                        value="{{\Carbon\carbon::parse($order_details->items[0]->collection_date)->format('d-m-Y')}}" />

                </div>
                <div class="col-md-6" id="slot-list">
                    <label class="control-label">Collection time</label><br>
                    <div class="time-slot @if($order_details->items[0]->collection_time_slot == '9 AM to 1 PM') selected @endif">9 AM to 1 PM</div>
                    <div class="time-slot @if($order_details->items[0]->collection_time_slot == '2 PM to 5 PM') selected @endif">2 PM to 5 PM</div>
                        <input type="hidden" id="time_slot_val" name="time_checking" value="{{$order_details->items[0]->collection_time_slot}}">
                </div>
                <input type="hidden" value="{{$order_id}}" name="hidden_order" id="hidden_order">
                <input type="hidden" value="{{$branch_id}}" name="hidden_supplier" id="hidden_supplier">
                <input type="hidden" value="{{$order_details->order_status}}" id="order_status" name="order_status">
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-info waves-effect waves-light save-btn">
                Save
            </button>
            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>

        </div>
    </div>
</form>
<style>
    div.ui-datepicker {
        font-size: 10px;
    }

    .time-slot {
        border: 1px solid #bfbebe;
        padding: 6px;
        border-radius: 2px;
        display: inline-block;
        margin-bottom: 6px;
        color: black;
        min-width: 100px;
        height: 50px;
        text-align: center;
        line-height: 34px;
    }

    #slot-list .selected {
        background-color: black;
        color: white;
    }
    #time_slot_val-error{
        position: fixed;
    bottom: 18px;
    left: 40px;
}

</style>

<script>
    $('input[name="order_date"]').daterangepicker({
        "singleDatePicker": true,
        "autoUpdateInput": false,
        "autoApply": true,
        "minDate": new Date(),
        locale: {
            format: 'DD-MM-YYYY'
        }
    });
    $('input[name="order_date"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#slot-list').on('click', '.time-slot', function(e) {
       
           let el = $(this);
           $('.time-slot').removeClass('selected');
           el.addClass('selected');  
           var select_val =$('.selected').text();
           select_val ?   $('#time_slot_val').val('12') :  $('#time_slot_val').val();
       
        
    });
//     $(".datepicker").datepicker({
       
//        format: "yyyy/mm/dd",
//        autoclose: true,
//        defaultDate: new Date(),
//        immediateUpdates: true,
//        todayHighlight: true,
//        clearBtn: true

//    }).on('changeDate', function(ev){
//        $.ajax( {
//        type: 'POST',
//        url:"{{route('order.time-slot')}}" ,
//        data: {
//            date: this.value,
//        },
//        success: function(data) {
//            $('#slot-list').empty();
//            $('#slot-list').append(`
//            <label class="control-label">Time slots</label><br>
//                <div class="time-slot ">${data[1]}</div>
//                <div class="time-slot ">${data[2]}</div>
//            `);
//         }
//        });

       
//    });

   $('.save-btn').on('click',function (){
       
        $('#frm_schedule_order').validate({ 
            ignore: [],
            rules: {
                driver_select:{
                    required:true
                },
            datepicker:{
                required:true
            },
            time_checking:{
              required:true
            }
           
            },
        messages: {
            
            driver_select: {
                    required: 'Driver required.'
                },
            datepicker:{
                required:"Date is required"
            },
            time_checking:{
                required:"Select time slot"
            }
            
        },
        submitHandler: function(form) {
            let date = $('.datepicker').val();
            let status =$('#status_select').val();
            let driver =$('#driver_select').val();
            let slot = $('.selected').text();
            let order =$('#hidden_order').val();
            let supplier = $('#hidden_supplier').val();
                $.ajax({
                type: 'POST',
                url:"{{route('order.save-schedule')}}" ,
                data: {
                    date:date,
                    status:status,
                    driver:driver,
                    slot:slot,
                    order:order,
                    supplier:supplier

                },
                success: function(data) {
                    Toast.fire({
                    icon: 'success',
                    title: "Changes updated successfully"
                    });
                    $("#frm_schedule_order")[0].reset();
                    window.location.reload();
                }
                });
             return false;
            }
        });

    });  
    $('#status_select').on('click',function(){
        let status= $('#order_status').val();
        var opt_val = '';
        var opt_name = ''
       
        switch(status)
        {
            case '2':
            
            $('#status_select option[value= collected]').attr('disabled', true);
            $('#status_select option[value="at warehouse"]').attr('disabled', true);
            break;
            case '3':
            $('#status_select option[value= accept]').attr('disabled', true);
           
               
            break;
            
        }
      
    })
</script>