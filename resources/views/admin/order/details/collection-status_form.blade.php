
<div class="modal-header">
    <h4 class="modal-title"><strong>
      
            Change Collection status
        </strong></h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

</div>
<form id="frm_schedule_order" class="frm_schedule_order" action="javascript:;" method="POST">
    <div class="tab-pane active" id="order_schedule" role="tabpanel">
        <div class="modal-body">
            <div class="row">
                <input type="hidden"  id="order_id" name="order_id" value="{{$order_id}}">
                <input type="hidden"  id="branch_id" name="branch_id" value="{{$branch_id}}">
                <input type="hidden"  id="item_id" name="item_id" value="{{$item_id}}">
                <div class="col-md-6">
                    <label class="control-label">Collection status</label><br>
                    <select class="form-control" id="collection_stat" name="collection_stat">
                        <option value="">Select status</option>
                        <option value="5">Collected</option>
                        <option value="6">At warehouse</option>
                    </select>
                    
                </div>
                <div class="col-md-6 shelf_list" style="display: none">
                    <label class="control-label">Shelfs</label><br>
                    <select class="form-control" id="shelf_id" name="shelf_id">
                        <option value="">Select shelf</option>
                        @foreach ($shelfs as $shelf =>$key)
                        
                             <option value={{$shelf}}>{{$key}}</option>
                        
                         @endforeach
                    </select>
                </div>
                
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-info waves-effect waves-light save-btn">
                Save
            </button>
            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>

        </div>
    </div>
</form>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
   
   $('.save-btn').on('click',function (){
        $('#frm_schedule_order').validate({ 
        rules: {
            collection_stat:{
                required :true,
            }
        },
        messages: {
            collection_stat: {
                    required: 'Collection status required'
                },
           
        },
        submitHandler: function(form) {
            
                $.ajax({
                type: 'POST',
                url:"{{route('admin.changeStatus')}}" ,
                data: {
                   order_id:$('#order_id').val(),
                   branch_id:$('#branch_id').val(),
                   item_id:$('#item_id').val(),
                   collection_stat:$('#collection_stat').val(),
                   shelf:$('#shelf_id').val()
                },
                success: function(data) {
                    Toast.fire({
                    icon: 'success',
                    title: "Collection status updated successfully"
                    });
                    window.location.reload();
                }
                });
             return false;
            }
        });

    });  
    $('#collection_stat').on('change',function(){
        var stat = $('#collection_stat').val();
       
        if(stat == 6)
        {
            $('.shelf_list').show();

        }else{
            $('.shelf_list').hide();
        }

    })
</script>

