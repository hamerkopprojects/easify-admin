@extends('layouts.master')


{{-- @section('add-btn')
<a href="{{route('order.complete.list')}} " class="cust_stylee btn back_btn">Back</a>
@endsection --}}

@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            @if($details->order_status == 9)
                            <h1>Completed Order Details</h1>
                            @else
                            <h1>Cancelled Order Details</h1>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="page-header">
                        <div class="page-title">
                            @if($details->order_status == 9)
                                <a href="{{route('admin.order-completed')}}" class="cust_stylee btn back_btn">Back</a>
                            @else
                            <a href="{{route('admin.order-cancelled')}}" class="cust_stylee btn back_btn">Back</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            @if($details->order_status == 9)
            <div class="row">
                <div class="col-sm-12">
                    <x-order-complete-tab activeTab="order" :id="$details['id']" />
                </div>
            </div>
            @endif
            <div class="card">
                <form id="frm_create_product" action="javascript:;" method="POST">
                    <div class="tab-pane active" id="pdt_info" role="tabpanel">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="control-label"><strong>Order ID</strong></label><br>
                                    <p>{{$details->order_id}}</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>Order date</strong></label><br>
                                    <p> {{\Carbon\Carbon::parse($details->created_at)->format('d F y')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>Delivery date</strong></label><br>
                                    <p> {{\Carbon\carbon::parse($details->delivery_schedule_date)->format('d F y')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>Customer Name</strong></label><br>
                                    <p> {{ucfirst($details->customer->cust_name)}}</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong> Status</strong></label><br>
                                    <p>{{$ord_status[$details->order_status]}}</p>
                                </div>

                                <div class="col-md-4">
                                    <label class="control-label"><strong>Delivery location </strong></label><br>
                                    <p>{{$details->delivery_loc}}</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>Payment Method </strong></label><br>
                                    <p>{{ucfirst($details->payment_method)}}</p>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><strong>Delivery Note </strong></label><br>
                                    <p>{{$details->order_note}}</p>
                                </div>
                                @if($details->order_status == 6)
                                <div class="col-md-6">
                                    <label class="control-label"><strong>Cancelled By </strong></label><br>
                                    @php
                                if($details['updated_by'] == 'customer')
                                {
                                    $name_user = $order_cancel->owner->cust_name ?? '';
                                }else{
                                    $name_user = $order_cancel->owner->name ?? '';
                                }
                            @endphp
                            {{$name_user}} @if($details['updated_by'])({{$details['updated_by']}})@endif
                                    <p></p>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><strong>Cancellation Reason</strong></label><br>
                                    <p>{{$details->cancellation->lang[0]->name ?? ''}}</p>
                                </div>
                                @endif
                                <div class="col-md-12">
                                    <hr>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><b>Delivery Address</b></label><br>
                                    <strong>{{$details['delivery_name']}}</strong><br>
                                    <strong>{{$details['delivery_phone']}}</strong><br>
                                    {{$details['delivery_address']}}<br>
                                    {{$details['delivery_city']}}<br>
                                    P O Box:{{$details['delivery_zip']}}<br>
                                    {{$details['delivery_country']}}

                                </div>
                                <div class="col-md-6 ">
                                    <label class="control-label"><b>Billing Address</b></label><br>
                                    <strong>{{$details['billing_name']}}</strong><br>
                                    <strong>{{$details['billing_phone']}}</strong><br>
                                    {{$details['billing_address']}}<br>
                                    {{$details['billing_city']}}<br>
                                    P O Box:{{$details['billing_zip']}}<br>
                                    {{$details['billing_country']}}

                                </div>
                                <br>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Supplier</th>
                                            <th>Items</th>
                                            <th>price</th>
                                            <th>Quantity</th>
                                            <th>Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $item_list= $details->items->groupBy('supplier_id');
                                        
                                        @endphp
                                        @foreach($item_list as $new_item)
                                        <tr>
                                           
                                            <td>
                                                @php
                                                 if(isset($new_item[0]->branch))
                                                 {
                                                    $Supplier_code = $new_item[0]->branch->code;
                                                 }else{
                                                    $Supplier_code =$new_item[0]->supplier->code;
                                                 }
                                                
                                                @endphp
                                                {{$Supplier_code}}
                                            </td>
                                            <td>
                                                @foreach ($new_item as $item)
                                                <p> {{$item->product->lang[0]->name}}</p>
                                                @endforeach

                                            </td>
                                            <td>
                                                @foreach ($new_item as $item)
                                                @php
                                                   $item_price = $item->grant_total  /$item->item_count;
                                                @endphp
                                                <p> {{$item_price}}</p>
                                                @endforeach
                                            </td>
                                            <td>
                                                @foreach ($new_item as $item)
                                                <p> {{$item->item_count}}</p>
                                                @endforeach
                                            </td>
                                            <td>
                                                @foreach ($new_item as $item)
                                                
                                                <p> {{$item->grant_total}}</p>
                                                @endforeach
                                            </td>

                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                                <br>
                                <div class="col-md-6"></div>
                                <div class="col-md-3">
                                    <label class="control-label">Sub Total</label><br>
                                </div>
                                <div class="col-md-3">
                                    <label class="control-label">SAR {{$details->sub_total}} </label><br>
                                </div>
                                <div class="col-md-6"></div>
                                <div class="col-md-3">
                                    <label class="control-label">Delivery charge</label><br>
                                </div>
                                <div class="col-md-3">
                                    <label class="control-label">SAR {{$details->delivery_charge ?? '0.00'}}</label><br>
                                </div>
                                <div class="col-md-6"></div>
                                <div class="col-md-3">
                                    <label class="control-label">COD Fee</label><br>
                                </div>
                                <div class="col-md-3">
                                    <label class="control-label">SAR {{$details->cod_fee ?? '0.00'}}</label><br>
                                </div>
                                @php
                               
                                if(isset($details->coupon_code)){
                                if($details->coupon_type == 'amount'){
                                $coupon_value = 'SAR '.number_format($details->coupon_value, 2, ".", "");
                                }
                                else {
                               
                                $coupon_value = $details->coupon_value . '%';
                                }
                                }
                                @endphp
                                @if(isset($details->coupon_code))
                                <div class="col-md-6"></div>
                                <div class="col-md-3">
                                    <label class="control-label">Discount</label><br>
                                </div>
                                <div class="col-md-3">
                                    <label class="control-label">{{$coupon_value}}</label><br>
                                </div>
                                @endif
                                <div class="col-md-4">
                                </div>
                                <div class="col-md-8">
                                    <hr>
                                </div>
                                <div class="col-md-6">

                                </div>
                                <div class="col-md-3">
                                    <label class="control-label"><strong> TOTAL AMOUNT</strong></label>
                                </div>
                                <div class="col-md-3">
                                
                                    <label class="control-label"><strong>SAR {{$details->grant_total}}</strong></label>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
    <style>
        .cust_stylee{
            float: left;
            margin-left: 184px;
  } 
    </style>
@endpush