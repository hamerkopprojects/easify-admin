@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row mt-3">
                <div class="col-lg-8 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>Dashboard</h1>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <label>Select date</label>
                    <div class='input-group' id='datepicker'>
                        <input type="text" class="form-control" name="dates">
                        <span class="input-group-addon">
                            <span class="fa fa-calendar icon-style"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-icon card_custom">
                            <div class="card-icon">
                                <i class="fa fa-files-o"></i>
                            </div><br>
                            <p class="card-category"></p>
                            <h3 class="card-title">Total order for collection</h3>
                            <p class="align-p" id="total_collection">{{ $total_order_for_collection }}</p>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-warning card-header-icon">
                            <div class="card-icon">
                                <i class="ti-shopping-cart"></i>
                            </div><br>
                            <p class="card-category"></p>
                            <h3 class="card-title">Total Items in Warehouse
                            </h3>
                            <p class="align-p" id="total_ware_house">{{ $total_items_in_ware_house }}</p>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-warning card-header-icon">
                            <div class="card-icon">
                                <i class="fa fa-files-o"></i>
                            </div><br>
                            <p class="card-category"></p>
                            <h3 class="card-title">Total Orders for delivery
                            </h3>
                            <p class="align-p" id="total_delivery">{{ $total_orders_delivery }}</p>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-warning card-header-icon">
                            <div class="card-icon">
                                <i class="fa fa-files-o"></i>
                            </div><br>
                            <p class="card-category"></p>
                            <h3 class="card-title">Total Delivered Orders
                            </h3>
                            <p class="align-p" id="total_delivered">{{ $total_orders_delivered }}</p>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-warning card-header-icon">
                            <div class="card-icon">
                                <i class="fa fa-files-o"></i>
                            </div><br>
                            <p class="card-category"></p>
                            <h3 class="card-title" style="font-size:16px;">Total Cancelled Orders
                            </h3>
                            <p class="align-p" id="total_cancelled">{{$total_cancelled_order}}</p>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-warning card-header-icon">
                            <div class="card-icon">
                                <i class="fa fa-usd"></i>
                            </div><br>
                            <p class="card-category"></p>
                            <h3 class="card-title">Total Sale
                            </h3>
                            <p class="align-p" id="total_sale">SAR {{ number_format($total_sale, 2, ".", "") }}</p>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-warning card-header-icon">
                            <div class="card-icon">
                                <i class="fa fa-building"></i>
                            </div><br>
                            <p class="card-category"></p>
                            <h3 class="card-title">Total Suppliers
                            </h3>
                            <p class="align-p" id="total_suppliers">{{ $total_suppliers }}</p>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-warning card-header-icon">
                            <div class="card-icon">
                                <i class="fa fa-users"></i>
                            </div><br>
                            <p class="card-category"></p>
                            <h3 class="card-title">Total Customers

                            </h3>
                            <p class="align-p" id="total_customers">{{ $total_customers }}</p>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.dashboard.todays_collection')
            @include('admin.dashboard.todays_delivery')
            @include('admin.dashboard.best_selling_product')
        </div>
    </div>
</div>
@endsection
@push('css')
@php
$rtl_ext = app()->getLocale() == 'en' ? '' : '-rtl';
@endphp
<link href="{{asset('/assets/css/dashboard.css')}}" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style>
    .right-pos{
        position: absolute;
        right: 35px;
        top: 87px;
    }
    .see-more{
        right: 38px;
        position: absolute;
        bottom: 7px;
    }
    .href-style{
        color:  cornflowerblue;
    }
    .card [class*="card-header-"] .card-icon, .card [class*="card-header-"] .card-text {
        padding: 5px;
    }
    .align-p{
        text-align: center;
    }
</style>
@endpush
@push('scripts')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
$('#datepicker').click(function(){
$('input[name="dates"]').click();
});
$('input[name="dates"]').daterangepicker({maxDate: new Date()});
$('input[name="dates"]').on('apply.daterangepicker', function(ev, picker) {
let start = picker.startDate.format('YYYY/MM/DD');
let end = picker.endDate.format('YYYY/MM/DD');
$.ajax({
type: "POST",
        url: "{{route('dashboard.card_count')}}",
        data:{
        start_date:start,
                end_date:end
        },
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
        $('#total_cancelled').text(data.total_cancelled);
        $('#total_sale').text(data.sales_count);
        $('#total_customers').text(data.total_customers);
        $('#total_suppliers').text(data.total_suppliers);
        $('#total_collection').text(data.total_collection);
        $('#total_ware_house').text(data.total_ware_house);
        $('#total_delivery').text(data.total_delivery);
        $('#total_delivered').text(data.total_delivered);
        }
});
});
$('input[name="dates"]').on('cancel.daterangepicker', function(ev, picker) {
window.location.reload();
});
</script>

@endpush