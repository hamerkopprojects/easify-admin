<div class="row">
    <div class="col-lg-12 ml-0">
        <div class="page-title">
            <h5>Todays Delivery ({{\Carbon\Carbon::now()->format('j F Y')}})</h5>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered color-table">
                        <thead>
                            <tr>
                                <th>SNO.</th>
                                <th>Order ID</th>
                                <th>Customer</th>
                                <th>Scheduled Delivery Date</th>
                                <th>Driver </th>
                                <th>Amount</th>
                                <th> </th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($todays_delivery) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($todays_delivery as $order)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$order->order_id}}</td>
                                <td>{{ucfirst($order->customer->cust_name)}}</td>
                                <td>{{ date('j F Y', strtotime($order->delivery_schedule_date))}} - {{$order->time_slot}}</td>
                                <td>{{ isset($order->driver->name) ? ucfirst($order->driver->name) : ''}}</td>
                                <td>SAR {{$order->grant_total}}</td>
                                <td class="text-center">
                                    <a href="{{ route('admin.order-details', $order->id) }}" class="btn btn-sm btn-info text-white view_btn" title="View"><i class="fa fa-eye"></i></a>

                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="12" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $todays_delivery->appends(request()->input())->links() }}
                </div>
            </div>
        </div>
    </div>
</div>