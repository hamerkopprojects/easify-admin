<div class="row">
    <div class="col-lg-12 ml-0">
        <div class="page-title">
            <h5>Todays Collection ({{\Carbon\Carbon::now()->format('j F Y')}})</h5>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered color-table">
                        <thead>
                            <tr>
                                <th>SNO.</th>
                                <th>Order Id</th>
                                <th>Customer </th>
                                <th>No of Suppliers</th>
                                <th>Amount</th>
                                <th>Status</th>
                                <th width="15%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($todays_collection) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($todays_collection as $order)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{$order->order_id}}</td>
                                <td>{{ucfirst($order->customer->cust_name)}}</td>
                                <td>{{$order->items_count}}</td>
                                <td>SAR {{$order->grant_total}}</td>
                                <td>Collection completed</td>
                                <td class="text-center">
                                    <a href="{{ route('admin.order-details', $order->id) }}" class="btn btn-sm btn-info text-white view_btn" title="View"><i class="fa fa-eye"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="12" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $todays_collection->appends(request()->input())->links() }}
                </div>
            </div>
        </div>
    </div>
</div>