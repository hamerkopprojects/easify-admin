@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>ADD ATTRIBUTES TO CATEGORY   '{{ strtoupper($cat_name['name']) }}'</h1>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
                <div class="col-lg-3">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12 text-right">
                                <a href="{{ route('category') }}">
                                    <font style="vertical-align: inherit;">Back
                                    </font>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
            </div>
            <!-- /# row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">

                            <form id="frm_att_cat" action="javascript:;" method="POST">
                                <div class="row">
                                    <div class= "col-md-12">
                                        <label class="control-label"></label>
                                        <select class="select2_multiple form-control" name="attributes[]" multiple="multiple">
                                            @foreach($attributes as $row_data)
                                            <option value="{{$row_data->id}}">{{$row_data->lang[0]->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class= "col-md-12">
                                        <input type="hidden" name='cat_id' value="{{$cat_id}}">
                                        <button type="submit" class="btn btn-info" id="submit">
                                            SAVE
                                        </button>

                                    </div>

                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div id="msgDiv"></div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>S. No.</th>
                                            <th>Attribute Name</th>
                                            <th>Variant</th>
                                            <th width="10%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($sel_attributes) > 0)
                                        @php
                                        $i = 1;
                                        @endphp
                                        @foreach ($sel_attributes as $row_data)
                                        <tr>
                                            <th>{{ $i++ }}</th>
                                            <td>{{ $row_data->attributes->name }}</td>
                                            <td>{{ ucfirst($row_data->cat_attributes->is_variant) }}</td>
                                            <td class="text-center">
                                                @if($row_data->cat_attributes->is_variant == 'no')
                                                <a class="btn btn-sm btn-danger text-white" title="Delete Attributes" onclick="deleteAttributes({{ $row_data->attribute_id }}, {{$cat_id}})"><i class="fa fa-trash"></i></a>
                                                @else
                                                @php $variant_count = \App\Models\Product::where(['category_id' => $cat_id])->count();@endphp
                                                @if($variant_count > 0)
                                                <button type="button" class="btn btn-sm btn-primary text-white" data-toggle="tooltip" title="Variant attribute cannot be deleted because it is assigned to some of the existing products"><i class="fa fa-info"></i></button>
                                                @else
                                                <a class="btn btn-sm btn-danger text-white" title="Delete Attributes" onclick="deleteAttributes({{ $row_data->attribute_id }}, {{$cat_id}})"><i class="fa fa-trash"></i></a>
                                                @endif
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="8" class="text-center">No records found!</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center d-flex justify-content-center mt-3">
                                {{ $sel_attributes->appends(request()->input())->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $('[data-toggle="tooltip"]').tooltip();
    $(".select2_multiple").select2({
    placeholder: "Select Attributes",
            allowClear: true
    });
    $(".select2-selection--multiple").addClass("form-control");
    $(".select2-selection--multiple").css({"border": "1px solid #ccc", "border-radius": "0", "padding": "3px 9px 20px 0px", "height": "auto"});
    $("#frm_att_cat").validate({
    normalizer: function (value) {
    return $.trim(value);
    },
            rules: {
            "attributes[]": {
            required: true,
            },
            },
            messages: {
            "attributes[]": {
            required: 'Select atleast one attribute'
            },
            },
            submitHandler: function (form) {
            $('button:submit').attr('disabled', true);
            $.ajax({
            type: "POST",
                    url: "{{route('save_cat_attribute')}}",
                    data: $('#frm_att_cat').serialize(),
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                    if (data.status == 1) {
                    $("#frm_att_cat")[0].reset();
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    window.setTimeout(function() {
                    window.location.reload();
                    }, 1000);
                    } else {
                    $('button:submit').attr('disabled', false);
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    },
                    error: function (err) {
                    $('button:submit').attr('disabled', false);
                    }
            });
            return false;
            }
    });
    function deleteAttributes(attribute_id, cat_id) {
    $.confirm({
    title: '<span class="small">Are you sure to delete this attribute?</span>',
            content: 'You wont be able to revert this',
            buttons: {
            Yes: function() {
            $.ajax({
            type: "POST",
                    url: "{{route('delete_cat_attributes')}}",
                    data: {
                    attribute_id: attribute_id, cat_id : cat_id
                    },
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    window.setTimeout(function() {
                    window.location.reload();
                    }, 1000);
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            },
                    No: function() {
                    console.log('cancelled');
                    }
            }
    });
    }
</script>
@endpush