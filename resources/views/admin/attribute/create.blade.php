<div class="modal-header">
    <h4 class="modal-title"><strong>
            {{( isset($row_data['id']) && $row_data['id'] != '' ) ? 'Edit' : 'Add' }}
            Attribute
        </strong></h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <div class="msg_div"></div>
    <div class="row">
        <div class="col-md-6">
            <label class="control-label">Attribute Name (EN) <span class="text-danger">*</span></label>
            <input class="form-control form-white" placeholder="Enter attribute name" type="text" name="name_en" value="{{ isset($row_data['id']) ?  $row_data->lang[0]->name : ''}}"/>
        </div>
        <div class="col-md-6">
            <label class="control-label">Attribute Name (AR) <span class="text-danger">*</span></label>
            <input class="form-control form-white text-right" placeholder="Enter attribute name" type="text" name="name_ar" value="{{ isset($row_data['id']) ? $row_data->lang[1]->name : ''}}"/>
        </div>
        <div class="col-md-12">
            <label class="control-label">Attribute Type</label><br/>
            <label class="radio-inline">
                <input type="radio" name="attribute_type" value="textbox" {{ isset($row_data['id']) && $row_data['attribute_type'] == 'textbox' ? 'checked="checked"' : 'checked=""'}}> Textbox
            </label>
            <label class="radio-inline ml-3">
                <input type="radio" name="attribute_type" value="dropdown" {{ isset($row_data['id']) && $row_data['attribute_type'] == 'dropdown' ? 'checked="checked"' : ''}}> Dropdown
            </label>
             <label class="radio-inline ml-3">
                <input type="radio" name="attribute_type" value="textarea" {{ isset($row_data['id']) && $row_data['attribute_type'] == 'textarea' ? 'checked="checked"' : ''}}> Textarea
            </label>
        </div>
        @php
        $i = 0;
        @endphp
        @if(isset($row_data['id']) && $row_data['attribute_type'] == 'dropdown')
        <div class="cls_variant_div mb-3">
            @foreach ($variant_data as $var_data)
            @php
            $i++;
            @endphp
            <div class="row ml-0 cls_att_val{{$i}}">
                <div class="col-md-5">
                    @if($i == 1) <label class="control-label">Attribute value(EN) <span class="text-danger">*</span></label> @endif
                    <input class="form-control form-white" placeholder="Enter attribute value" type="text" name="att_value_en[]" value="{{ $var_data['en_name'] }}"/>
                    <input type="hidden" name="att_value_en_id[]" value="{{ $var_data['en_variant_id'] }}"/>
                </div>
                <div class="col-md-5">
                    @if($i == 1) <label class="control-label">Attribute value(AR) <span class="text-danger">*</span></label> @endif
                    <input class="form-control form-white text-right" placeholder="Enter attribute value" type="text" name="att_value_ar[]" value="{{ $var_data['ar_name']}}"/>
                    <input type="hidden" name="att_value_ar_id[]" value="{{ $var_data['ar_variant_id'] }}"/>
                </div>
                @if($i == 1)
                <div class="col-md-1 text-left">
                    <label class="control-label"></label><br/>
                    <a class="btn btn-sm btn-success text-white mt-3 edit_att_btn" title="Add Attributes"><i class="fa fa-plus"></i></a>
                </div>
                @else
                <div class="col-md-1 text-left">
                    <a class="btn btn-sm btn-danger text-white" title="Remove Attributes" onclick="getCount(this, {{ $var_data['en_variant_id'] }})"><i class="fa fa-minus"></i></a>
                </div>
                @endif
            </div>
            @endforeach
        </div>
        @else
        <div class="cls_variant_div mb-3" style="display:none">
            <div class="row ml-0 cls_att_val1">
                <div class="col-md-5">
                    <label class="control-label">Attribute value(EN) <span class="text-danger">*</span></label>
                    <input class="form-control form-white" placeholder="Enter attribute value" type="text" name="att_value_en[]" value=""/>
                </div>
                <div class="col-md-5">
                    <label class="control-label">Attribute value(AR) <span class="text-danger">*</span></label>
                    <input class="form-control form-white text-right" placeholder="Enter attribute value" type="text" name="att_value_ar[]" value=""/>
                </div>
                <div class="col-md-1 text-left">
                    <label class="control-label"></label><br/>
                    <a class="btn btn-sm btn-success text-white mt-3 add_att_btn" title="Add Attributes"><i class="fa fa-plus"></i></a>
                </div>
            </div>
        </div>
        @endif
        @if(empty($row_data['id']))
        <div class="col-md-6 is_variant" style="{{ isset($row_data['id']) && $row_data['attribute_type'] == 'dropdown' ? 'display:block' : 'display:none'}}">
            <label class="control-label">Is this attribute is variant?</label><br/>
            <label class="radio-inline">
                @if(!empty($row_data['id']))
                <input type="radio" name="is_variant" value="yes" {{ isset($row_data['id']) && $row_data['is_variant'] == 'yes' ? 'checked="checked"' : ''}}> Yes
                       @else
                       <input type="radio" name="is_variant" value="yes"> Yes
                @endif
            </label>
            <label class="radio-inline ml-3">
                @if(!empty($row_data['id']))
                <input type="radio" name="is_variant" value="no" {{ isset($row_data['id']) && $row_data['is_variant'] == 'no' ? 'checked="checked"' : ''}}> No
                       @else
                       <input type="radio" name="is_variant" value="no" checked="checked"> No
                @endif
            </label>
        </div>
        @endif
        <div class="col-md-6 is_mandatory" style="{{ isset($row_data['id']) && $row_data['is_variant'] == 'yes' ? 'display:none' : ''}}">
            <label class="control-label">Is this attribute is mandatory?</label><br/>
            <label class="radio-inline">
                @if(!empty($row_data['id']))
                <input type="radio" name="is_mandatory" value="yes" {{ isset($row_data['id']) && $row_data['is_mandatory'] == 'yes' ? 'checked="checked"' : ''}}> Yes
                       @else
                       <input type="radio" name="is_mandatory" value="yes"> Yes
                @endif
            </label>
            <label class="radio-inline ml-3">
                @if(!empty($row_data['id']))
                <input type="radio" name="is_mandatory" value="no" {{ isset($row_data['id']) && $row_data['is_mandatory'] == 'no' ? 'checked="checked"' : ''}}> No
                       @else
                       <input type="radio" name="is_mandatory" value="no" checked="checked"> No
                @endif
            </label>
        </div>
    </div>
</div>
<div class="modal-footer">
    <input type="hidden" id="_attribute_id" name="_attribute_id" value="@if(isset($row_data['id'])) {{ $row_data['id'] }}@endif">
    <button type="submit" class="btn btn-info waves-effect waves-light save-categorys">
        {{( isset($row_data['id']) && $row_data['id'] != '' ) ? 'Update' : 'Create' }}
    </button>
    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
</div>
<script>
    $("form input:radio[name=attribute_type]").change(function () {
    if ($(this).val() == 'dropdown') {
    $(".cls_variant_div").css({'display': 'block'});
    $(".is_variant").css({'display': 'block'});
    } else
    {
    $(".cls_variant_div").css({'display': 'none'});
    $(".is_variant").css({'display': 'none'});
    $(".is_mandatory").css({'display': 'block'});
    }
    });
    $("form input:radio[name=is_variant]").change(function () {
    if ($(this).val() == 'yes') {
    $(".is_mandatory").css({'display': 'none'});
    } else
    {
     $(".is_mandatory").css({'display': 'block'});
    }
    });
    var edit_cnt = {{$i}};
    $(".edit_att_btn").click(function () {
    var div_cnt = edit_cnt + 1;
    $(".cls_variant_div").append('<div class="row ml-0 cls_att_val' + div_cnt + '"><div class="col-md-5">' +
            '<input class="form-control form-white" placeholder="Enter attribute value" type="text" name="att_value_en[]" value=""/>' +
            '<input type="hidden" name="att_value_en_id[]" value=""/></div>' +
            '<div class="col-md-5">' +
            '<input class="form-control form-white text-right" placeholder="Enter attribute value" type="text" name="att_value_ar[]" value=""/></div>' +
            '<div class="col-md-1 text-left">' +
            '<a class="btn btn-sm btn-danger text-white btn_remove" title="Remove Attributes"><i class="fa fa-minus"></i></a></div></div>');
    edit_cnt++;
    });
    var cnt = 0;
    $(".add_att_btn").click(function () {
    cnt++;
    var div_cnt = cnt + 1;
    $(".cls_variant_div").append('<div class="row ml-0 cls_att_val' + div_cnt + '"><div class="col-md-5">' +
            '<input class="form-control form-white" placeholder="Enter attribute value" type="text" name="att_value_en[]" value=""/>' +
            '<input type="hidden" name="att_value_ar_id[]" value=""/></div>' +
            '<div class="col-md-5">' +
            '<input class="form-control form-white text-right" placeholder="Enter attribute value" type="text" name="att_value_ar[]" value=""/></div>' +
            '<div class="col-md-1 text-left">' +
            '<a class="btn btn-sm btn-danger text-white btn_remove" title="Remove Attributes"><i class="fa fa-minus"></i></a></div></div>');
    });
    $('body').on('click', '.btn_remove', function () {
    $(this).parent().parent().remove();
    cnt--;
    });

    function getCount(item, var_id){
    $.ajax({
    type: "GET",
            url: "{{route('attribute.getCount')}}",
            data: {'id': var_id },
            success: function (data) {
                 if (data.status == 1) {
                    $(item).parent().parent().remove();
                    cnt--; 
                 }else{
                   Toast.fire({
                    icon: 'error',
                    title: data.message
                    });  
                 }
    }
    });
    }

</script>