@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-2 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>{{ __('messages.admin.products') }}</h1>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 ml-0">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12 text-right">
                                <select class="form-control" name="stock_update" id="stock_update" onchange="location = this.value;">
                                    <option disabled selected hidden>{{ strtoupper(__('messages.admin.stock_update')) }} </option>
                                    <option value="{{ route('branch.stock_import',['type'=>"simple"]) }}">{{ strtoupper(__('messages.admin.stock_update')) }} ({{ __('messages.admin.simple') }})</option>
                                    <option value="{{ route('branch.stock_import',['type'=>"complex"]) }}">{{ strtoupper(__('messages.admin.stock_update')) }} ({{ __('messages.admin.complex') }})</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
            </div>
            <!-- /# row -->

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form id="posts-filter" method="get" action="{{ route('branch.product_list') }}">
                                <div class="row tablenav top text-left">
                                    <div class="col-md-4 ml-0">
                                        <select class="form-control search_val" name="cat_id">
                                            <option value="">{{ __('messages.admin.by_main_category') }}</option>
                                            @foreach($cat_data as $value)
                                            <option value="{{ $value->id }}" {{ $value->id == $category_id ? 'selected' : '' }}>{{$value->lang[0]->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-5 ml-0">
                                        <input class="form-control" type="text" name="search" value="{{ $search }}" placeholder="{{ __('messages.admin.search_by_SKU') }}">
                                    </div>
                                    <div class="col-md-3 text-left">
                                        <button type="submit" class="btn btn-info">
                                            <font style="vertical-align: inherit;">{{ __('messages.admin.search') }}</font>
                                        </button>
                                        <a href="{{ route('branch.product_list') }}" class="btn btn-default">{{ __('messages.admin.reset') }}</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div id="msgDiv"></div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>{{ __('messages.admin.sno') }}</th>
                                            <th width="10%">{{ __('messages.admin.sku') }}</th>
                                            <th>{{ __('messages.admin.product_name') }} (EN)</th>
                                            <th class="text-right">{{ __('messages.admin.product_name') }} (AR)</th>
                                            <th>{{ __('messages.admin.category') }}</th>
                                            <th>{{ __('messages.admin.product_type') }}</th>
                                            <th width="20%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($pdt_data) > 0)
                                        @php
                                        $i = 1;
                                        @endphp
                                        @foreach ($pdt_data as $row_data)
                                        <tr>
                                            <th>{{ $i++ }}</th>
                                            <td>{{ $row_data->sku }}</td>
                                            <td>{{ $row_data->lang[0]->name ?? '' }}</td>
                                            <td class="text-right">{{ $row_data->lang[1]->name ?? '' }}</td>
                                            <td>{{ $row_data->category->lang[0]->name ?? '' }}</td>
                                            <td>{{ ucfirst($row_data->product_type) }}</td>
                                            <td class="text-center" width="20%">
                                                <a class="btn btn-sm btn-success text-white view_btn" title="View Product Details" data-id="{{ $row_data->id }}"><i class="fa fa-eye"></i></a>
                                                <a class="btn btn-sm btn-success text-white pdt_modal_btn stock_update" title="Stock Update" data-product_id="{{ $row_data->id }}"><i class="fa fa-building-o"></i></a>
                                               
                                            </td>
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="8" class="text-center">{{ __('messages.admin.no_records_found') }}</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center d-flex justify-content-center mt-3">
                                {{ $pdt_data->appends(request()->input())->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal pooup -->
    <div class="modal none-border" id="formModal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="max-height:85%;margin-top: 50px;margin-bottom:50px;max-width: 100%">
            <div class="modal-content">

            </div>
        </div>
    </div>
    <!-- END MODAL -->
    @endsection
    @push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
    <link rel="stylesheet" href="{{ asset('assets/css/lib/cropper/cropper.min.css') }}">
    @endpush
    @push('scripts')

    {{-- Sweet alert --}}
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    {{-- select2 --}}
    <script src="{{ asset('assets/js/lib/cropper/cropper.min.js') }}"></script>
    <script src="{{ asset('assets/js/lib/cropper/jquery-cropper.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom_crop.js') }}"></script>
    <script>
    $.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
   
    $('.view_btn').on('click', function() {
    var id = $(this).data("id");
    $.ajax({
    type: "GET",
            url: '{{url("/branch/products/details")}}' + '/' + id,
            success: function(data) {
            $('.modal-content').html(data);
            $('#formModal').modal('show');
            }
    });
    });

    $('.stock_update').on('click',function(){
        let id = $(this).data("product_id");
        $.ajax({
        type: "GET",
            url: '{{url("/branch/products/stock_update")}}' + '/' + id,
            success: function(data) {
            $('.modal-content').html(data);
            $('#formModal').modal('show');
            }
    });
    });
    </script>

    @endpush