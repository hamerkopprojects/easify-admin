<form id="frm_create_price" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body">
            <div class="row">
                @if($product_type == 'complex' && !empty($pdt_att_var))
                <div class="col-md-12 mt-3">
                    <label class="control-label"><strong>PRODUCT VARIANT</strong></label>
                </div>
                <div class="col-md-3"><label class="control-label"><b>{{ $pdt_att_var->lang[0]['name'] ?? '' }} variant</b></label></div>
                <div class="col-md-3"><label class="control-label"><b>{{ __('messages.admin.price1') }}</b> </label></div>
                <div class="col-md-3"><label class="control-label"><b>{{ __('messages.admin.discount') }} (%)</b></label></div>
                <div class="col-md-3"><label class="control-label"><b>{{ __('messages.admin.discount_price') }}</b></label></div>
                @if($variants = \App\Models\ProductVariant::with('variant')->where(['product_id' => $pdt_id])->get())
                @foreach($variants as $variant_val)
                @if(!empty($variant_val->variant->id))
                @php
                $pdt_price = \App\Models\ProductPrice::with('attribute')->with('variant')->where('product_id', $pdt_id)
                ->where('variant_lang_id', $variant_val->variant->id)->first();
                @endphp
                <div class="col-md-3 mt-2">
                    <label class="control-label ml-1">{{ $variant_val->variant->name ?? '' }}</label>
                </div>

                <div class="col-md-3">
                    <label class="control-label">{{ isset($pdt_price['price']) ? $pdt_price['price'] : '' }}</label>
                </div>
                <div class="col-md-3">
                    <label class="control-label">{{ isset($pdt_price['discount']) ? $pdt_price['discount'] : '' }} (%)</label>
                </div>
                <div class="col-md-3">
                    <label class="control-label">{{ isset($pdt_price['discount_price']) ? $pdt_price['discount_price'] : '' }}</label>
                </div>
                @endif
                @endforeach
                @endif
                @else
                <div class="col-md-4"><label class="control-label"><b>{{ __('messages.admin.price1') }}</b> </label></div>
                <div class="col-md-4"><label class="control-label"><b>{{ __('messages.admin.discount') }}</b></label></div>
                <div class="col-md-4"><label class="control-label"><b>{{ __('messages.admin.discount_price') }}</b></label></div>
                <div class="col-md-4">
                    <label class="control-label">{{ isset($product_price)? $product_price['price'] : ''}}</label>
                </div>
                <div class="col-md-4">
                    <label class="control-label">{{ isset($product_price)? $product_price['discount'] : ''}}</label>
                </div>
                <div class="col-md-4">
                    <label class="control-label">{{ isset($product_price)? $product_price['discount_price'] : ''}}</label>
                </div>
                @endif
        </div>
        <div class="modal-footer">
            {{-- <input type="hidden" id="pdt_id" name="pdt_id" value="{{ $pdt_id }}">
            <input type="hidden" id="product_type" name="product_type" value="{{ $product_type }}">
            <input type="hidden" id="submit_action" value="" /> --}}
            
        </div>
    </div>
</form>
<style>

 </style>
    