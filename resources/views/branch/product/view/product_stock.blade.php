<form id="frm_create_stock" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label class="control-label"><strong>{{ __('messages.admin.stock') }}</strong></label>
                </div>
                @if($product_type == 'simple')
                <div class="col-md-4">
                </div>
                <div class="col-md-4">
                    <label class="control-label"><strong>{{ __('messages.admin.min_stock') }}</strong></label>
                </div>
                <div class="col-md-4">
                    <label class="control-label"><strong>{{ __('messages.admin.max_stock') }}</strong></label>
                </div>
                @php $pdt_branch_stock = \App\Models\ProductVariant::with(['pdt_branch' => function ($query) use ($branch){
                        $query->where('branch_id', $branch->id);
                    }])->where('product_id', $pdt_id)->first();
                @endphp
                <div class="col-md-4">
                    <label class="control-label">{{$branch->name ?? ''}} / {{$branch->code ?? ''}}</label>
                </div>
                <div class="col-md-4"><label class="control-label" >{{ $pdt_branch_stock->pdt_branch->min_stock ?? '' }}</label></div>
                <div class="col-md-4"><label class="control-label" >{{ $pdt_branch_stock->pdt_branch->max_stock ?? '' }}</label></div>
                @else
                @if($variants = \App\Models\ProductVariant::with('variant')->where(['product_id' => $pdt_id])->get())
                @php $i=0;@endphp
                @foreach($variants as $variant_val)
                @if(!empty($variant_val->variant->id))
                <div class="col-md-12 mt-3"><h6>{{ $pdt_att_var->lang[0]['name'] ?? '' }} - {{ $variant_val->variant->name ?? '' }}</h6></div>
                <div class="col-md-4">
                </div>
                <div class="col-md-4">
                    <label class="control-label"><strong>{{ __('messages.admin.min_stock') }}</strong></label>
                </div>
                <div class="col-md-4">
                    <label class="control-label"><strong>{{ __('messages.admin.max_stock') }}</strong></label>
                </div>
                @php $pdt_branch_stock1 = \App\Models\ProductVariant::with(['pdt_branch' => function ($query) use ($branch){
                        $query->where('branch_id', $branch->id);
                    }])->where('product_id', $pdt_id)->where('variant_lang_id', $variant_val->variant_lang_id)->first();
                @endphp
                <div class="col-md-4">
                    <label class="control-label">{{$branch->name ?? ''}} / {{$branch->code ?? ''}}</label>
                </div>
                <div class="col-md-4"><label class="control-label">{{ $pdt_branch_stock1->pdt_branch->min_stock ?? '' }}</label></div>
                <div class="col-md-4"><label class="control-label">{{ $pdt_branch_stock1->pdt_branch->max_stock ?? '' }}</label></div>
                @endif
                @endforeach
                @endif
                @endif
            </div>
            <div class="modal-footer">
                <input type="hidden" id="pdt_id" name="pdt_id" value="{{ $pdt_id }}">
                <input type="hidden" id="product_type" name="product_type" value="{{ $product_type }}">
                <input type="hidden" id="submit_action" value="" />
                
            </div>
        </div>
    </div>
</form>