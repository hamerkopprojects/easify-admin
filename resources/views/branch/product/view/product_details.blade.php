<div class="modal-header">
    <h4 class="modal-title"><strong>
      
            Product Details
        </strong></h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="col-sm-12">
    <ul class="nav nav-tabs" role="tablist" id="pdt_tab">
        <li class="nav-item"> <a class="nav-link active" data-id="{{$product[0]['id']}}" href="#pdt_info" role="tab" data-toggle="tab"><span class="hidden-sm-up"><i class="ti-shopping-cart"></i></span> <span class="hidden-xs-down">PRODUCT INFO</span></a> </li>
        <li class="nav-item disabled"> <a class="nav-link" href="#spec_stock" data-id="{{$product[0]['id']}}" role="tab" data-toggle="tab"><span class="hidden-sm-up"><i class="ti-folder"></i></span> <span class="hidden-xs-down">SPEC & STOCK</span></a> </li>
        <li class="nav-item disabled"> <a class="nav-link" href="#b2cprice" data-id="{{$product[0]['id']}}" role="tab" data-toggle="tab"><span class="hidden-sm-up"><i class="ti-folder"></i></span> <span class="hidden-xs-down">PRICE</span></a> </li>
        {{-- <li class="nav-item disabled"> <a class="nav-link" href="#b2bprice" data-id="{{$product[0]['id']}}" role="tab" data-toggle="tab"><span class="hidden-sm-up"><i class="ti-folder"></i></span> <span class="hidden-xs-down">B2B PRICE</span></a> </li> --}}
        <li class="nav-item disabled"> <a class="nav-link" href="#photos"   data-id="{{$product[0]['id']}}" role="tab" data-toggle="tab"><span class="hidden-sm-up"><i class="ti-image"></i></span> <span class="hidden-xs-down">PHOTOS</span></a> </li>
    </ul>
</div>
<div class="tab-content">
    @include('branch.product.view.basic_details')
</div>