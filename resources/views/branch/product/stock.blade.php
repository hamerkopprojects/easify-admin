<form id="frm_create_stock" action="javascript:;" method="POST">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label class="control-label">
                        <b>{{ __('messages.admin.update_stock') }}</b>
                    </label>
                </div>
                @if($product_type == 'simple')
                <div class="col-md-4">
                </div>
                <div class="col-md-4">
                    <label class="control-label"><strong>{{ __('messages.admin.min_stock') }}</strong></label>
                </div>
                <div class="col-md-4">
                    <label class="control-label"><strong>{{ __('messages.admin.max_stock') }}</strong></label>
                </div>
                @php $pdt_branch_stock = \App\Models\ProductVariant::with(['pdt_branch' => function ($query) use ($branch){
                        $query->where('branch_id', $branch->id);
                    }])->where('product_id', $pdt_id)->first();
                @endphp
                <div class="col-md-4">
                    <label class="control-label">{{$branch->name ?? ''}} / {{$branch->code ?? ''}}</label>
                </div>
                <div class="col-md-4"><input class="form-control form-white" placeholder="Enter min stock" value="{{ $pdt_branch_stock->pdt_branch->min_stock ?? '' }}" name="min_stock" id="min_stock_{{ $branch->id }}"/></div>
                <div class="col-md-4"><input class="form-control form-white sm" placeholder="Enter max stock" value="{{ $pdt_branch_stock->pdt_branch->max_stock ?? '' }}" name="max_stock" id="max_stock_{{ $branch->id }}"/></div>
                <input type="hidden" value="{{ $branch->id }}" name="branch_id"/>
                @else
                @if($variants = \App\Models\ProductVariant::with('variant')->where(['product_id' => $pdt_id])->get())
                @php $i=0;@endphp
                @foreach($variants as $variant_val)
                @if(!empty($variant_val->variant->id))
                <div class="col-md-12 mt-3"><h6>{{ $pdt_att_var->lang[0]['name'] ?? '' }} - {{ $variant_val->variant->name ?? '' }}</h6></div>
                <div class="col-md-4">
                </div>
                <div class="col-md-4">
                    <label class="control-label"><strong>{{ __('messages.admin.min_stock') }}</strong></label>
                </div>
                <div class="col-md-4">
                    <label class="control-label"><strong>{{ __('messages.admin.max_stock') }}</strong></label>
                </div>
                @php $pdt_branch_stock1 = \App\Models\ProductVariant::with(['pdt_branch' => function ($query) use ($branch){
                        $query->where('branch_id', $branch->id);
                    }])->where('product_id', $pdt_id)->where('variant_lang_id', $variant_val->variant_lang_id)->first();
                @endphp
                <div class="col-md-4">
                    <label class="control-label">{{$branch->name ?? ''}} / {{ $branch->code ?? ''}}</label>
                </div>
                <div class="col-md-4"><input class="form-control form-white" placeholder="Enter min stock" value="{{ $pdt_branch_stock1->pdt_branch->min_stock ?? '' }}" name="min_stock_var[]" id="min_stock_var_{{ $i++ }}"/></div>
                <div class="col-md-4"><input class="form-control form-white sm" placeholder="Enter max stock" value="{{ $pdt_branch_stock1->pdt_branch->max_stock ?? '' }}" name="max_stock_var[]" id="max_stock_var_{{ $i++ }}"/></div>
                <input type="hidden" value="{{ $branch->id }}" name="branch_id[]"/>
                <input type="hidden" value="{{ $variant_val->id }}" name="product_variant_id[]"/>
                @endif
                @endforeach
                @endif
                @endif
            </div>
            <div class="modal-footer">
                <input type="hidden" id="pdt_id" name="pdt_id" value="{{ $pdt_id }}">
                <input type="hidden" id="product_type" name="product_type" value="{{ $product_type }}">
                <input type="hidden" id="submit_action" value="" />
                @if(!empty($pdt_id))
                <button type="submit" class="btn btn-info waves-effect waves-light save-btn">
                    Save
                </button>
                @endif
                
                <button class="btn btn-default waves-effect tab_back" data-id='{{ $pdt_id }}' data-dismiss="modal">Cancel</a>
            </div>
        </div>
</form>
<script>
    
    $(".save-btn").on('click', function () {
        $("#submit_action").val('save');
    });
   
    $("#frm_create_stock").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        errorElement: "label",
        errorPlacement: function (error, element) {
            if (element.attr("type") == "checkbox") {
                error.insertAfter("#error_chk");
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "{{route('branch.save_product_stock')}}",
                data: $('#frm_create_stock').serialize(),
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data.status == 1) {
                        if ($("#submit_action").val() == 'continue') {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#pdt_tab a[href="#price"]').tab('show');
                            $('.tab-content').html(data.result);
                        } else
                        {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            window.location.reload();
                        }
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                    }
                    $('button:submit').attr('disabled', false);
                },
                error: function (err) {
                    $('button:submit').attr('disabled', false);
                }
            });
            return false;
        }
    });
    $('input[id^="min_stock_"]').each(function () {
        $(this).rules('add', {
            required: true,
            number: true,
            messages: {
                required: "Min stock is required",
                number: "Numbers only"
            }
        });
    });
    $('input[id^="max_stock_"]').each(function () {
        $(this).rules('add', {
            required: true,
            number: true,
            messages: {
                required: "Max stock is required",
                number: "Numbers only"
            }
        });
    });
    
    $('input[id^="min_stock_var_"]').each(function () {
        $(this).rules('add', {
            required: true,
            number: true,
            messages: {
                required: "Min stock is required",
                number: "Numbers only"
            }
        });
    });
    $('input[id^="max_stock_var_"]').each(function () {
        $(this).rules('add', {
            required: true,
            number: true,
            messages: {
                required: "Max stock is required",
                number: "Numbers only"
            }
        });
    });
    
</script>