@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>{{ __('messages.admin.stock_update') }} - {{ __('messages.admin.step1') }}</h1>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
                <div class="col-lg-3">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12 text-right">
                                <a href="{{ route('branch.product_list') }}">
                                    <font style="vertical-align: inherit;">{{ __('messages.admin.back') }}
                                    </font>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
            </div>
            <!-- /# row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form id="frm-excel-import" action="javascript:;" method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p><strong>{{ __('messages.admin.import_product_list') }}</strong></p>
                                        <p class="small"><b>{{ __('messages.admin.insructions') }} </b><br/>
                                            1. {{ __('messages.admin.supported_file_formats') }}: <b>xlsx</b><br>
                                            2. {{ __('messages.admin.max_file_size') }}: <b>2 MB</b><br>
                                            3. {{ __('messages.admin.follow_the_excel_fields') }}
                                        </p>
                                        <hr>
                                        @if($type == 'simple')
                                        <a href="{{ asset('sample/stock/Easify-Stock-Simple.xlsx') }}" class="cl-btn-dwnld float-right" style="color: blue;"><i class="fa fa-download"></i> {{ __('messages.admin.download_sample') }}</a>
                                        @else
                                        <a href="{{ asset('sample/stock/Easify-Stock-Complex.xlsx') }}" class="cl-btn-dwnld float-right" style="color: blue;"><i class="fa fa-download"></i> {{ __('messages.admin.download_sample') }}</a>
                                        @endif
                                        <div class="col-md-8 custmfl">
                                            <div class="custom-file">
                                                <input type="hidden" name="type" id="type" value="{{$type}}">
                                                <input type="file" class="custom-file-input" name="select_file" id="select_file">
                                                <label class="custom-file-label" for="customFile">{{ __('messages.admin.choose_the_excel') }}</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 text-left">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-info" id="btn-import-submit">{{ __('messages.admin.import') }}</button>
                                        <button type="button" class="btn btn-default" onclick="location.href ='{{route('branch.product_list')}}'">{{ __('messages.admin.cancel') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<style>
    .custmfl{
    padding-left: 0px!important;
    }
</style>
@push('scripts')
<script>
    $("#frm-excel-import").submit(function () {
        $('.loading_box').show();
        $('.loading_box_overlay').show();
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $("#btn-import-submit").on('click', function () {
        $('.loading_box').show();
        $('.loading_box_overlay').show();
        var form_data = new FormData($('form')[0]);
        $.ajax({
            type: "POST",
            url: "{{route('branch.stock_import_submit')}}",
            data: form_data,
            dataType: "json",
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function () {
                $('#btn-import-submit').button('loading');
            },
            complete: function () {
                $('#btn-import-submit').button('reset');
            },
            success: function (data) {
                 if (data.status == 1 && data.type == "simple") {
                    window.location.href = '{{route("branch.stock_simple")}}';
                }
                else if (data.status == 1 && data.type == "complex") {
                    window.location.href = '{{route("branch.stock_complex")}}';
                } else {
                    $('.loading_box').hide();
                    $('.loading_box_overlay').hide();
                    Toast.fire({
                        icon: 'error',
                        title: data.message
                    });
                }
            },
            error: function () {}
        });
        return false;
    });
</script>
@endpush