@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>{{ __('messages.admin.stock_update') }} - {{ __('messages.admin.step2') }}</h1>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /# row -->
            <div class="row">
                <div class="col-lg-12">
                    <p><strong>{{ __('messages.admin.toatal_records_to_import') }}: {{ $total_count }}</strong></p>
                    <p>Ready for stock update: {{$duplicate_count}}</p>
                    <p>{{ __('messages.admin.errors') }}: {{$error_count}}</p>
                    <div class="card">
                        <div class="card-body">
                            <form class="frm-search" id="posts-filter" method="get" action="">
                                <div class="row tablenav top text-right">
                                    <div class="col-md-8 ml-0">
                                        <input class="form-control search_txt" type="text" name="search" value="{{ $search }}" placeholder="{{ __('messages.admin.search_by_sku') }}">
                                    </div>
                                    <div class="col-md-3 text-left">
                                        <button type="submit" class="btn btn-info"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{ __('messages.admin.search') }}</font></font></button>
                                        <a href="{{route('branch.stock_simple')}}" class="btn btn-default btn-reset">{{ __('messages.admin.reset') }}</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-12 text-right">
                        <a href="{{route('branch.stock_import')}}?action=simple" class="btn btn-default ml-2"><font style="vertical-align: inherit;">{{ __('messages.admin.fix_the_excel') }}</font></a>
                        <a href="javascript:void(0);" class="btn btn-info ml-2 cls_import_confirm"><font style="vertical-align: inherit;">{{ __('messages.admin.continue') }}</font></a>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>{{ __('messages.admin.sno') }}</th>
                                            <th>{{ __('messages.admin.sku') }}</th>
                                            <th>{{ __('messages.admin.min_stock') }}</th>
                                            <th>{{ __('messages.admin.max_stock') }}</th>
                                            <th class="cls_last_child"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($result_data) > 0)
                                        @php
                                        $i = 1;
                                        @endphp
                                        @foreach ($result_data as $row_data)
                                        <tr>
                                            <th>{{$i++}}</th>
                                            <td>{{$row_data->sku}}</td>
                                            <td>{{$row_data->min_stock}}</td>
                                            <td>{{$row_data->max_stock}}</td>
                                            @if($row_data->error_flag=="Y")
                                            <td class="cls_last_child" style="background:red;color:white;">{{ __('messages.admin.errors') }}
                                                <a href="#" class="btn btn-sm btn-success text-white error-data float-right" title="View Log" data-id="{{$row_data->id}}"><i class="fa fa-eye"></i></a></td>
                                            @else
                                            <td class="cls_last_child" style="background:green;color:white;">Ready for stock update</td>
                                            @endif
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="12" class="text-center">{{ __('messages.admin.no_records_found') }}</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center d-flex justify-content-center mt-3">
                                {{ $temp_data->appends(request()->query())->links() }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 text-right">
                        <a href="{{route('branch.stock_import')}}?action=simple" class="btn btn-default ml-2"><font style="vertical-align: inherit;">{{ __('messages.admin.fix_the_excel') }}</font></a>
                        <a href="javascript:void(0);" class="btn btn-info ml-2 cls_import_confirm"><font style="vertical-align: inherit;">{{ __('messages.admin.continue') }}</font></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal pooup -->
<div class="modal fade none-border" id="formModal">
    <div class="modal-dialog modal-lg" style="max-height:85%;  margin-top: 50px; margin-bottom:50px;max-width: 100%">
        <div class="modal-content">

        </div>
    </div>
</div>
<!-- END MODAL -->
@endsection
@push('scripts')
<script>
    $(window).load(function () {
        $('.loading_box').hide();
        $('.loading_box_overlay').hide();
    });
    $('.error-data').on('click', function () {
        var id = $(this).data("id");
        $.dialog({
            title: 'Error: Data with Errors',
            content: "url:{{route('branch.simple_errors')}}?id=" + id,
            //                            animation: 'scale',
            columnClass: 'large',
            //                            closeAnimation: 'scale',
            backgroundDismiss: true,
        });
    });
    $('.cls_import_confirm').on('click', function () {
        $('.loading_box').show();
        $('.loading_box_overlay').show();
        $.ajax({
            type: "GET",
            url: "{{route('branch.comfirm_simple')}}",
            success: function (data) {
                if (data.status == 1) {
                    $('.loading_box').hide();
                    $('.loading_box_overlay').hide();
                    Toast.fire({
                        icon: 'success',
                        title: data.message
                    });
                    window.setTimeout(function () {
                        window.location.href = '{{route("branch.imported_simple")}}';
                    }, 1000);
                }
            }
        });
    });

</script>
@endpush