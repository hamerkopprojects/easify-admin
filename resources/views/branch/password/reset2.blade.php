@extends('layouts.auth')
@section('flash')
    @include('auth.msg')
@endsection
@section('auth-form')

<div class="login-form">
    <h4>VERIFY ACCOUNT</h4>
    <h6 class="sub-title">We have sent verification code to your email entered, please verify the account</h6>
    <form method="post" action="{{ route('branch.token.verify') }}">
        @csrf
        <input type="hidden" name="email" value="{{$email}}">
        <div class="reset-wrapper">
            <input placeholder="Enter verification code" id="code" name="code" type="text" class="form-control el" required autofocus>
            <button type="submit" class="btn btn-primary btn-flat m-b-15 el">VERIFY</button>
        </div>
        @if($errors->any())
                
                <p class="error">{{$errors->first()}}</p>

            @endif
    </form>
    <div id="resend"style="color: black; text-align: center">Didn't receive code?<strong id="resend_token"><u> Resend</u></strong></div>
</div>

@endsection
@push('css')
<style>
    .sub-title {
        color: #656565;
        margin-bottom: 20px;
    }

    .reset-wrapper {
        display: flex;
        justify-content: space-between;
    }

    .reset-wrapper .el {
        width: 49%;
    }

    .reset-wrapper input.el {
        height: 47px;
    }
    #resend strong{
        cursor: pointer;
    }
</style>
@endpush
@push('scripts')
<script>
$('#resend_token').on('click',function(){
        window.history.back();
  
});
</script>
@endpush