@extends('layouts.auth')
@section('flash')
    @include('auth.msg')
@endsection
@section('auth-form')

{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8"> --}}
            {{-- <div class="card">
                <div class="card-header" style="color: white;">RESET</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div> --}}
            <div class="login-form">
                <h4>RESET PASSWORD</h4>
                {{-- <h6 class="sub-title">We have sent verification code to your email entered, please verify the account</h6> --}}
                <form method="POST" action="{{ route('branch.reset') }}" class="validate_reset_form">
                    @csrf
                    <input type="hidden" name="email" value="{{$email}}">
                    <div class="col-md-12">
                        <label>New Password</label>
                        <input placeholder="Enter password" id="password" name="password" type="password" class="form-control el" required />
                        <span class="input-group-text" >
                            <i id="eye" class="fa fa-eye toggle-password" toggle="#password"></i>
                        </span>
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$message}}</strong>
                            </span>
                        @enderror
                        {{-- <button type="submit" class="btn btn-primary btn-flat m-b-15 el">VERIFY</button> --}}
                    </div>
                    <div class="col-md-12"> 
                        <label>Confirm Password</label>
                        <input placeholder="Enter confirm password" id="confirm_password" name="confirm_password" type="password" class="form-control el" required />
                        {{-- <button type="submit" class="btn btn-primary btn-flat m-b-15 el">VERIFY</button> --}}
                        <span class="input-group-text">
                            <i id="eye-c" class="fa fa-eye toggle-password" toggle="#confirm_password"></i>
                        </span>
                    </div>
                    <div class="col-md-12">
                        {{-- <input placeholder="Enter verification code" id="code" name="code" type="text" class="form-control el" required autofocus> --}}
                        <button type="submit" class="btn btn-primary btn-flat m-b-15 el">RESET PASSWORD</button>
                    </div>
                </form>
            </div>
        {{-- </div>
    </div>
</div> --}}
@endsection
@push('css')
<style>

         
</style>
@endpush
@push('scripts')

<script>
$('.toggle-password').on('click',function(){
    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});

</script>
<script>

$('.validate_reset_form').validate({
    rules : {
                password : {
                    required:true,
                    minlength : 5
                },
                confirm_password : {
                    minlength : 5,
                    equalTo :'[name="password"]',
                }
            },
            messages :{
                password:{
                    required:"Password required",
                },
                confirm_password:{
                    equalTo:" Password and confirmation password do not match",
                },

            }

});  

</script>
@endpush
