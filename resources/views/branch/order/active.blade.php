@extends('layouts.master')

@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>{{ __('messages.admin.orders') }}</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <x-branch-order-tab activeTab="bch-active" />
                </div> 
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form id="posts-filter" method="get">
                                <div class="row tablenav top text-left">
                                    <div class="col-md-5 ml-0">
                                        <input class="form-control" type="text" name="search" value="{{$search}}" placeholder="{{ __('messages.admin.search_by_order_id') }}">
                                    </div>
                                    <div class="col-md-5 ml-0">
                                        <select  class="select2 form-control" id="search_select" name="search_select" placeholder ="by customer">
                                            <option value="">{{ __('messages.admin.by_status') }}</option>
                                           
                                            <option value="1"{{ 1 == $stat ? "selected" :""}}>Pending</option>
                                            <option value="2"{{ 2 == $stat ? "selected" :""}}>Accepted</option>
                                            <option value="3"{{ 3 == $stat ? "selected" :""}}>Collection pending</option>
                                            <option value="4"{{ 4 == $stat ? "selected" :""}}>Collection completed</option>

                                           
                                        </select>
                                    </div>
                                    
                                </div>
                                <div class="row tablenav top text-left">
                                    <div class="col-md-5 ml-0">
                                        <input class="form-control datetimepicker" type="text" name="date" value="{{$date ?? ''}}" placeholder="{{ __('messages.admin.by_order_date') }}">
                                    </div>
                                    <div class="col-md-5 text-left">
                                        <button type="submit" class="btn btn-info">
                                            <font style="vertical-align: inherit;">{{ __('messages.admin.search') }}</font>
                                        </button>
                                        <a href="{{route('branch.active-order')}}" class="btn btn-default">{{ __('messages.admin.reset') }}</a>
                                    </div>
                                </div>
                                <div class="row tablenav top text-left">
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div id="msgDiv"></div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    
                                        <tr>
                                            <th>{{ __('messages.admin.sno') }}</th>
                                            <th>{{ __('messages.admin.order_id') }}</th>
                                            <th>{{ __('messages.admin.order_date') }}</th>
                                            <th>{{ __('messages.admin.amount') }}</th>
                                            <th>{{ __('messages.admin.driver_info') }}</th>
                                            <th>{{ __('messages.admin.status') }}</th>
                                            <th width="15%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($active) > 0 )
                                        @php
                                        $i = 1;
                                        // dd($active);
                                        @endphp
                                        @foreach ($active as $item)
                                            {{-- @php
                                                dd($item);
                                            @endphp --}}
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$item->order_id}}</td>
                                            <td>{{ \Carbon\Carbon::parse($item->order_date)->format('d-m-Y')}}</td>
                                            <td>SAR {{$item->item_sum}}</td>
                                            <td>
                                                {{ucfirst($item->driver)}} 
                                            </td>
                                            <td>
                                                {{$supp_status[$item->supplier_status]}} 
                                            </td>
                                            <td class="text-center">
                                                <a href="{{ route('branch.order-details', $item->id) }}" class="btn btn-sm btn-success text-white view_btn" title="Order Details"><i class="fa fa-eye"></i></a>
                                                <a href="/branch-qr?order={{$item->id}} & supplier={{auth()->user()->id}}"class="btn btn-sm btn-danger waves-effect"  target="_blank" title="print Supplier QR code"><i class="fa fa-print"></i></a>

                                                {{-- <a href="#"class="btn btn-sm btn-danger text-white " title="Accept"><i class="fa fa-check"></i></a>
                                                <a href="#"class="btn btn-sm btn-danger text-white" title="reject"><i class="fa fa-close"></i></a> --}}
                                            </td>
                                        </tr>
                                        @endforeach
                                            
                                        @else
                                        <tr>
                                            <td colspan="8" class="text-center">{{ __('messages.admin.no_records_found') }}</td>
                                        </tr>

                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center d-flex justify-content-center mt-3">
                                {{ $active->links() }}
                            </div>
                        </div>
                    </div>
            
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
 $('input[name="date"]').daterangepicker({
        "singleDatePicker": true,
        "autoUpdateInput": false,
        "autoApply": true,
        // "minDate": new Date(),
        locale: {
                format: 'DD-MM-YYYY'
            }
    });
    $('input[name="date"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });
</script>
    
@endpush