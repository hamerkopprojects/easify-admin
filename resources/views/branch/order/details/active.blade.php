@extends('layouts.master')

@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>{{ __('messages.admin.order_info') }}</h1>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="page-header">
                        <div class="page-title">
                            {{-- <div class="col-md-12"> --}}
                            <a href="{{route('branch.active-order')}}" class="cust_stylee btn back_btn">{{ __('messages.admin.back') }}</a>
                            {{-- </div> --}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="row">
                    <a href="{{route('branch.order.details.pdf',$details->id)}}"class="cust_styl btn btn-info waves-effect waves-light" target="_blank"><i class="fa fa-print"> {{ __('messages.admin.print') }}</i></a>
                </div>
                <form id="frm_create_product" action="javascript:;" method="POST">
                    <div class="tab-pane active" id="pdt_info" role="tabpanel">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label"><b>{{ __('messages.admin.order_info1') }}</b></label>
                                    <br />

                                </div>
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>{{ __('messages.admin.order_id') }}</strong></label><br>
                                    <p>{{$details->order_id}}</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>{{ __('messages.admin.order_date') }}</strong></label><br>
                                    <p> {{\Carbon\Carbon::parse($details->created_at)->format('d-m-Y')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>{{ __('messages.admin.collection_date') }}</strong></label><br>
                                    @if(isset($details->items[0]->collection_date))
                                        {{Carbon\Carbon::parse($details->items[0]->collection_date)->format('d-m-y')}} - {{$details->items[0]->collection_time_slot}}
                                    @endif
                                </div>

                                <div class="col-md-4">
                                    <label class="control-label"><strong>{{ __('messages.admin.driver_info') }} </strong></label><br>
                                         @if(isset($details->items[0]->driver_id)) 
                                            {{$details->items[0]->driver->name}}
                                        @endif
                                </div>
                                @if($details->items[0]->status == 1)
                                <div class="col-md-4">
                                    <label class="control-label"><strong>{{ __('messages.admin.driver_phone') }} </strong></label><br>
                                         @if(isset($details->items[0]->driver_id)) 
                                            {{$details->items[0]->driver->phone}}
                                        @endif
                                </div>
                                @endif
                                <div class="col-md-4">
                                    <label class="control-label"><strong>{{ __('messages.admin.delivery_location') }}</strong></label><br>
                                    <p>{{$supplier->supplier_region->lang[0]->name}} </p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>{{ __('messages.admin.distance') }}</strong></label><br>
                                    <p> {{round($km,2)}} KM</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>{{ __('messages.admin.delivery_note') }}</strong></label><br>
                                    <p>{{$details->order_note}}</p>
                                </div>
                                <input type="hidden" id="order_id" name="order_id" value="{{$details->id}}">
                                <br>
                                <div class="col-md-12">
                                    <hr>
                                </div>
                                <table class="table table-bordered ">
                                    <thead>
                                        <tr>
                                            <th width="40%">{{ __('messages.admin.items') }}</th>
                                            <th>{{ __('messages.admin.price1') }}</th>
                                            <th>{{ __('messages.admin.quantity') }}</th>
                                            <th>{{ __('messages.admin.amount') }}</th>
                                            <th width="15%"> </th>
                                            <th width="5%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       
                                        @foreach($details->items as $new_item)
                                        <tr>
                                            
                                            <td>{{$new_item->product->lang[0]->name}}</td>
                                            <td>{{$new_item->item_price}}</td>
                                            <td>{{$new_item->item_count}}</td>
                                            <td>
                                                @php
                                                  $amount =$new_item->item_price *  $new_item->item_count  ;
                                                @endphp
                                                {{$amount}}
                                            </td>

                                            <td class="text-center">
                                                @if($new_item->status == 1)
                                                <a href="#"class="btn btn-sm btn-info waves-effect waves-light accept_product" title="Accept"
                                                     data-branch = "{{$new_item->branch_id}}"
                                                    data-order_id = "{{$new_item->order_id}}"
                                                     data-product = "{{$new_item->product_id}}"
                                                     data-row_id = "{{$new_item->id}}"><i class="fa fa-check"></i></a>
                                                <a href="#"class="btn btn-sm btn-danger text-white reject_product" title="reject" data-branch = "{{$new_item->branch_id}}"
                                                    data-order_id ="{{$new_item->order_id}}"
                                                     data-product = "{{$new_item->product_id}}"
                                                     data-row_id = "{{$new_item->id}}" ><i class="fa fa-close"></i></a>
                                                @else
                                                <p>{{$pro_status[$new_item->status]}}</p>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="/product-qr? product={{$new_item->id}}  & order={{$details->id}} & supplier={{$new_item->branch_id}}"class="btn btn-sm btn-danger waves-effect"  target="_blank" title="print product QR code"><i class="fa fa-print"></i></a>
                                                {{-- <a href="{{route('product.QRCode',$new_item->id)}}"class="btn btn-sm btn-danger waves-effect" target="_blank" title="print product QR code"><i class="fa fa-print"></i></a> --}}
                                            </td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                                <br>
                                <div class="col-md-12"></div>
                                <div class="col-md-4">
                                    <button class="btn btn-info waves-effect waves-light support_chat">Support</button>
                                </div>
                                @php
                                $grant =0;
                                foreach($details->items as $new_item)
                                {
                                    $grant += $new_item->item_price *  $new_item->item_count  ;
                                }
                            @endphp
                                <div class="col-md-4">
                                    <label class="control-label"><strong> TOTAL AMOUNT</strong></label>
                                </div>
                                <div class="col-md-4">

                                    <label class="control-label"><strong>SAR {{$grant}}</strong></label>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<!-- Modal pooup -->
<div class="modal none-border popupcontent_model" id="formModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="max-height:85%;max-width:50%;  margin-top: 50px; margin-bottom:50px;">
        <div class="modal-content popupcontent">

        </div>
    </div>
</div>
<!-- END MODAL -->
@endsection
@push('css')
<style>
    .cust_stylee{
      float: left;
      margin-left: 184px;
    }  
    .cust_styl{
        position: absolute;
        top: 10px;
        right: 10px;
        
    
    }
  </style>
 @endpush
@push('scripts')
  <script>
      $('.accept_product').on('click', function () {
        let product = $(this).data("product");
        let branch = $(this).data("branch");
        let order = $(this).data("order_id");
        let row_id = $(this).data("row_id");
        $.confirm({
            title: @json(__('messages.admin.confirmation')),
            content: 'Are you sure to accept the product?',
            buttons: {
            Yes: function () {
            $.ajax({
            type: "POST",
                    url: "{{route('branch.accept-product')}}",
                    data: {
                        product:product,
                        branch:branch,
                        order:order,
                        id:row_id
                        },
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    window.setTimeout(function () {
                   window.location.reload();
                    }, 1000);
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            },
                    No: function () {
                    window.location.reload();
                    }
            }
    });
    });
    // Cancel 

    $('.reject_product').on('click', function () {
        let product = $(this).data("product");
        let branch = $(this).data("branch");
        let order = $(this).data("order_id");
        let row_id = $(this).data("row_id");
        $.confirm({
            title: @json(__('messages.admin.confirmation')),
            content: 'Are you sure want to cancel the product?',
            buttons: {
            Yes: function () {
            $.ajax({
            type: "POST",
                    url: "{{route('branch.cancel-product')}}",
                    data: {
                        product:product,
                        branch:branch,
                        order:order,
                        id:row_id
                        },
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    window.setTimeout(function () {
                   window.location.reload();
                    }, 1000);
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            },
                    No: function () {
                    window.location.reload();
                    }
            }
    });
    });
    $.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
   
    $('.support_chat').on('click',function(){
        $.ajax({
            type: "POST",
            url: "{{route('branch.support_chat')}}",
            data:{
              order_id :$('#order_id').val()  
            },
            success: function(data) {
                $('.modal-content').html(data);
                $('#formModal').modal('show');
            }
        });
    })
  </script>  
@endpush
