
<div class="modal-header">
    <h4 class="modal-title"><strong>
      
           {{ __('messages.admin.support_chat') }}
        </strong></h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

</div>
<form id="frm_support_chat" class="frm_support_chat" action="javascript:;" method="POST">
    <div class="tab-pane active" id="order_schedule" role="tabpanel">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label class="control-label">{{ __('messages.admin.subject') }}</label><br>
                    <input type="text" class="form-control" name="subject" id="subject">
                </div>
                <div class="col-md-12">
                    <label class="control-label">{{ __('messages.admin.message') }}</label><br>
                    <textarea class="form-control messagearea" name="message" id="message"></textarea>
                    
                </div>
               <input type="hidden" name="order_id" id="order_id" value="{{$order_id}}">
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-info waves-effect waves-light save-btn">
                {{ __('messages.admin.send') }}
            </button>
            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">{{ __('messages.admin.cancel') }}</button>

        </div>
    </div>
</form>
<style>
    .messagearea{
        height: 100px;
    }
</style>
<script>
$('.save-btn').on('click',function (){
    $('#frm_support_chat').validate({ 
    rules: {
        subject: {
          required:true
        },
        
    },
    messages: {
        subject: {
                required: @json(__('frontend.support_validation1')
            },
        
    },
    submitHandler: function(form) {
        
            $.ajax({
            type: 'POST',
            url:"{{route('branch.support.store')}}" ,
            data: $('#frm_support_chat').serialize(),
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            success: function(data) {
                Toast.fire({
                icon: 'success',
                title: @json(__('frontend.support_successs')
                });
                window.location.reload();
            }
            });
         return false;
        }
    });

});
</script>