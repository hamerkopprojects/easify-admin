@extends('layouts.master')

@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>{{ __('messages.admin.order_details') }}</h1>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="page-header">
                        <div class="page-title">
                            @if($details->status == "delivered")
                                <a href="{{route('branch.complete-order')}}" class="cust_stylee btn back_btn">{{ __('messages.admin.back') }}</a>
                            @else 
                                <a href="{{route('branch.cancel-order')}}" class="cust_stylee btn back_btn">{{ __('messages.admin.back') }}</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <form id="frm_create_product" action="javascript:;" method="POST">
                    <div class="tab-pane active" id="pdt_info" role="tabpanel">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label"><b>{{ __('messages.admin.order_info1') }}</b></label>
                                    <br />

                                </div>
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>{{ __('messages.admin.order_id') }}</strong></label><br>
                                    <p>{{$details->order_id}}</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>{{ __('messages.admin.order_date') }}</strong></label><br>
                                    <p> {{\Carbon\Carbon::parse($details->created_at)->format('d-m-Y')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>{{ __('messages.admin.collection_date') }}</strong></label><br>
                                    @if(isset($details->items[0]->collection_date))
                                        {{Carbon\Carbon::parse($details->items[0]->collection_date)->format('d-m-y')}} - {{$details->items[0]->collection_time_slot}}
                                    @endif
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>{{ __('messages.admin.order_status') }}</strong></label><br>
                                    <p>{{ucfirst($details->status)}}</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>{{ __('messages.admin.delivery_location') }}</strong></label><br>
                                    <p>{{$details->delivery_loc}} </p>
                                </div>                               
                                <div class="col-md-6">
                                    <label class="control-label"><strong>{{ __('messages.admin.delivery_note') }}</strong></label><br>
                                    <p>{{$details->order_note}}</p>
                                </div>
                                @if($details->status == 'cancelled')
                                <div class="col-md-6">
                                    <label class="control-label"><strong>{{ __('messages.admin.cancelled_by') }} </strong></label><br>
                                        @php
                                            if($details->updated_by == 'customer')
                                            {
                                                $owner = $details->owner->cust_name;
                                            }else{
                                                $owner =$details->owner->name;
                                            }
                                        @endphp
                                            <p>{{$owner}}</p>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><strong>{{ __('messages.admin.cancellation_reason') }}</strong></label><br>
                                        {{$details->cancellation->lang[0]->name}}
                                </div>
                                @endif
                                <br>
                                <div class="col-md-12">
                                    <hr>
                                </div>
                                <table class="table table-bordered ">
                                    <thead>
                                        <tr>
                                            <th width="40%">{{ __('messages.admin.items') }}</th>
                                            <th>{{ __('messages.admin.price1') }}</th>
                                            <th>{{ __('messages.admin.quantity') }}</th>
                                            <th>{{ __('messages.admin.amount') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       
                                        @foreach($details->items as $new_item)
                                        <tr>
                                            
                                            <td>{{$new_item->product->lang[0]->name}}</td>
                                            <td>{{$new_item->item_price}}</td>
                                            <td>{{$new_item->item_count}}</td>
                                            <td>
                                                @php
                                                  $amount =$new_item->item_price *  $new_item->item_count  ;
                                                @endphp
                                                {{$amount}}
                                            </td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                                <br>
                                <div class="col-md-12"></div>
                                <div class="col-md-4">
                                    <button class="btn btn-info waves-effect waves-light support_chat">{{ __('messages.admin.support') }}</button>
                                </div>
                                @php
                                $grant =0;
                                foreach($details->items as $new_item)
                                {
                                    $grant += $new_item->item_price *  $new_item->item_count  ;
                                }
                            @endphp
                                <div class="col-md-4">
                                    <label class="control-label"><strong> {{ __('messages.admin.total_amount') }}</strong></label>
                                </div>
                                <div class="col-md-4">

                                    <label class="control-label"><strong>SAR {{$grant}}</strong></label>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<!-- Modal pooup -->
<div class="modal none-border popupcontent_model" id="formModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="max-height:85%;  margin-top: 50px; margin-bottom:50px;">
        <div class="modal-content popupcontent">

        </div>
    </div>
</div>
<!-- END MODAL -->
@endsection
@push('css')
<style>
    .cust_stylee{
      float: left;
      margin-left: 184px;
    }  
  </style>
 @endpush
@push('scripts')
  <script>
      $('.accept_product').on('click', function () {
        let product = $(this).data("product")
        let branch = $(this).data("branch")
        let order = $(this).data("order_id")
        $.confirm({
            title: @json(__('messages.admin.confirmation')),
            content: @json(__('messages.admin.cancel_pdt_msg')),
            buttons: {
            Yes: function () {
            $.ajax({
            type: "POST",
                    url: "{{route('branch.accept-product')}}",
                    data: {
                        product:product,
                        branch:branch,
                        order:order
                        },
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    window.setTimeout(function () {
                   window.location.reload();
                    }, 1000);
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            },
                    No: function () {
                    window.location.reload();
                    }
            }
    });
    });

    
  </script>  
  
@endpush
