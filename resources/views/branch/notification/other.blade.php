@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>NOTIFICATIONS</h1>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /# row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form id="posts-filter" method="get" action={{route('notification.other')}}>
                                <div class="row tablenav top text-left">

                                    <div class="col-md-4 ml-0">
                                        <div class='input-group date' id='datepicker'>
                                            <input class="form-control datetimepicker" type="text" name="date" value="{{$date ? \Carbon\Carbon::parse($date)->format('d-m-Y') : ''}}" placeholder="{{ __('messages.admin.search_by_date') }}" autocomplete="off">
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar icon-style"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4 ml-0">
                                        <select class="form-control search_val" name="customer_id" id="customer_id">
                                            <option value="">By Customer</option>
                                            @foreach ($customer as $cust)
                                            <option value="{{$cust->id}}" {{ $cust->id == $customer_id ? "selected" :""}}>{{ucfirst($cust->cust_name)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4 text-left">
                                        <button type="submit" class="btn btn-info">
                                            <font style="vertical-align: inherit;">{{ __('messages.admin.search') }}</font>
                                        </button>
                                        <a href="{{route('notification.other')}}" class="btn btn-default">{{ __('messages.admin.reset') }}</a>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div id="msgDiv"></div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>{{ __('messages.admin.sno') }}</th>
                                            <th class="text-left">{{ __('messages.admin.notification') }}</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($notifications) > 0)
                                        @php
                                        $i = 1;
                                        @endphp
                                        @foreach ($notifications as $list)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td class="text-left">{!! json_decode($list->content)->en !!}</td>
                                            <td class="text-center">
                                                @if(json_decode($list->details)->type == 'order')
                                                <a href="{{ route('branch.order-details', json_decode($list->details)->order_id)}}" class="btn btn-sm btn-success text-white view_btn" title="View order"><i class="fa fa-eye"></i></a>
                                                @else
                                                <a href="" class="btn btn-sm btn-success text-white view_btn" title="View"><i class="fa fa-eye"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="8" class="text-center">{{ __('messages.admin.no_records_found') }}</td>
                                        </tr>
                                        @endif

                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center d-flex justify-content-center mt-3">
                                {{ $notifications->appends(request()->input())->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style>
    .select2-container--default .select2-selection--single {
        height: 42px !important;border-radius: 0px !important;font-size: 1rem !important;
        padding: .4rem !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow b { top:20px !important}

</style>
@endpush
@push('scripts')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
$('.datetimepicker').daterangepicker({
    "singleDatePicker": true,
    "autoUpdateInput": false,
    "autoApply": true,
    locale: {
        format: 'DD-MM-YYYY'
    }
});
$('.datetimepicker').on('apply.daterangepicker', function (ev, picker) {
    $(this).val(picker.startDate.format('DD-MM-YYYY'));
});

$("#user_id").select2();
$("#customer_id").select2();
</script>
@endpush