<div class="row">
    <div class="col-lg-12 ml-0">
        <div class="page-title">
            <h5>{{ __('messages.admin.best_selling_products') }}</h5>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered color-table">
                        <thead>
                            <tr>
                                <th>{{ __('messages.admin.sno') }}</th>
                                <th>{{ __('messages.admin.sku') }}</th>
                                <th>{{ __('messages.admin.product_name') }} </th>
                                <th class="cls_last_child">{{ __('messages.admin.total_sold') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($best_selling_pdts) > 0)
                            @php $i = 1; @endphp;
                            @foreach ($best_selling_pdts as $products)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$products->product->sku}}</td>
                                <td>{{$products->product->lang[0]->name}}</td>
                                <td class="cls_last_child">{{$products->product_count}}</td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">{{ __('messages.admin.no_records_found') }}</td>
                            </tr>
                            @endif

                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $best_selling_pdts->appends(request()->input())->links() }}
                </div>
            </div>
        </div>
    </div>
</div>