@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row mt-3">
                <div class="col-lg-8 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>{{ __('messages.admin.dashboard') }}</h1>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                   <label>{{ __('messages.admin.select_date') }}</label>
                   <div class='input-group' id='datepicker'>
                        <input type="text" class="form-control" name="dates">
                        <span class="input-group-addon">
                            <span class="fa fa-calendar icon-style"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-warning card-header-icon">
                            <div class="card-icon">
                                <i class="fa fa-files-o"></i>
                            </div>
                            <p class="card-category"></p>
                            <h3 class="card-title">{{ __('messages.admin.total_items_for_collection') }}
                            </h3>
                            <p id="total_collection">{{$total_items_for_collection->total}}</p>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-danger card-header-icon">
                            <div class="card-icon">
                                <i class="fa fa-sign-in"></i>
                            </div>
                            <p class="card-category"></p>
                            <h3 class="card-title">{{ __('messages.admin.total_items_collected') }}</h3>
                            <p id="total_collected"> {{$total_items_collected->total}}</p>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-success card-header-icon">
                            <div class="card-icon">
                                <i class="fa fa-usd"></i>
                            </div>
                            <p class="card-category"></p>
                            <h3 class="card-title">{{ __('messages.admin.total_sale') }}</h3>
                            <p id="total_sale">SAR {{ number_format($total_sale->sum, 2, ".", "") }}</p>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
            @include('supplier.dashboard.todays_bookings')
            @include('supplier.dashboard.best_selling_product')
        </div>
    </div>
</div>
@endsection
@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style>

</style>
@endpush
@push('scripts')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
$('#datepicker').click(function(){
$('input[name="dates"]').click();
}); 
$('input[name="dates"]').daterangepicker({maxDate: new Date()});
$('input[name="dates"]').on('apply.daterangepicker', function(ev, picker) {
    let start =picker.startDate.format('YYYY/MM/DD');
    let end = picker.endDate.format('YYYY/MM/DD');
    $.ajax({
            type: "POST",
            url: "{{route('branch.card_count')}}",
            data:{
                start_date:start,
                end_date:end
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            success: function(data) {
                 $('#total_sale').text(data.sales_count);
                 $('#total_collection').text(data.total_collection);
                 $('#total_collected').text(data.total_collected);
            }
        }); 

  });
  $('input[name="dates"]').on('cancel.daterangepicker', function(ev, picker) {
        window.location.reload();
  });
</script>

@endpush