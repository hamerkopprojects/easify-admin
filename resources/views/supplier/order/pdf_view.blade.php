<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   </head>
   <body >
         <table  bgcolor="#ffffff" cellpadding="0"  cellspacing="0">
            <tbody>
               <tr>
                  <td  width="77%" >
                     <h4  face="arial" style="font-size: 20px; color:#716e6e;"> ORDER DERAILS</h4>
                  </td>
               </tr>
            </tbody>
         </table>
         <table width="100%"  bgcolor="#ffffff" cellpadding="0"  cellspacing="0" >
            <tbody>
               <tr>
                  <td  width="33%">
                     <b>Order ID</b> 
                     <p style="margin-top: 2px;
                        margin-bottom: 20px;
                        font-size: 14px;">{{$details['order_id']}}</p>
                  </td>
                  <td  width="33%">
                     <b>Order date</b> 
                     <p style="margin-top: 2px;
                        margin-bottom: 20px;
                        font-size: 14px;">{{Carbon\carbon::parse($details['created_at'])->format('d-m-Y')}}</p>
                  </td>
                  <td  width="33%">
                     <b>Collection date</b> 
                     <p style="margin-top: 2px;
                        margin-bottom: 20px;
                        font-size: 14px;">{{Carbon\Carbon::parse($details->items[0]->collection_date)->format('d-m-Y')}} - {{$details->items[0]->collection_time_slot}}</p>
                  </td>
               </tr>
               <tr>
                  <td width="33%">
                     <b>Distance</b> 
                     <p style="margin-top: 2px;
                        margin-bottom: 20px;
                        font-size: 14px;">{{round($km,2)}} KM</p>
                  </td>
                  <td width="33%">
                     <b>Driver info</b> 
                     <p style="margin-top: 2px;
                        margin-bottom: 20px;
                        font-size: 14px;">
                          @if(isset($details->items[0]->driver_id)) 
                          {{$details->items[0]->driver->name}}
                      @endif</p>
                  </td>
                  <td width="33%">
                     <b>Delivery  Location</b> 
                     <p style="margin-top: 2px;
                        margin-bottom: 20px;
                        font-size: 14px;">{{$supplier->supplier_region->lang[0]->name}}</p>
                  </td>
               </tr>
              
               <tr>
                  @if($details['order_status'] == 6)
                  <td width="33%">
                     <b>Cancellation Reason</b> 
                     <p style="margin-top: 2px;
                        margin-bottom: 20px;
                        font-size: 14px;"></p>
                  </td>
                  @if($details['cancel_note'])
                  <td width="33%">
                     <b>Comment</b> 
                     <p style="margin-top: 2px;
                        margin-bottom: 20px;
                        font-size: 14px;">{{$details['cancel_note']}}</p>
                  </td>
                  @endif
                  @endif
               </tr>
            </tbody>
         </table>
         <table width="100%"  bgcolor="#ffffff" cellpadding="0"  cellspacing="0" >
            <tbody>
               <tr>
                  <td height="20 "colspan="4"
                     style="border-top:0;border-right:0;border-bottom:1px solid #aaa;border-left:0;">
                  </td>
               </tr>
            </tbody>
         </table>
         <table  bgcolor="#ffffff" cellpadding="0" cellspacing="0">
            <tbody>
               <tr>
                  <td height="20 "colspan="4"
                  style="border-top:0;border-right:0;border-bottom:1px solid #aaa;border-left:0;">
                  </td>
               </tr>
            </tbody>
         </table>
         <table width="100%" bgcolor="#ffffff" cellpadding="0"  cellspacing="0">
            <thead>
               <tr face="arial">
                  <td width="55%" style="padding-bottom: 10px;font-size: 16px;
                     font-weight: 500; "><strong>ITEMS</strong></td>
                  <td width="15%" face="arial" style="padding-bottom: 10px;font-size: 16px;
                     font-weight: 500;  "><strong>Price</strong></td>
                  <td width="10%" style="padding-bottom: 10px;font-size: 16px;
                     font-weight: 500;  "><strong>Quantity</strong></td>
                  <td width="20%" style="padding-bottom: 10px;font-size: 16px;
                     font-weight: 500;  "><strong>Amount</strong></td>
               </tr>
            </thead>
            <tbody>
                @foreach($details->items as $new_item)
               <tr>
                @php
                $amount =$new_item->item_price *  $new_item->item_count  ;
              @endphp
              
                  <td width="55%" style="padding-bottom: 10px;font-size: 14px;
                     font-weight: 500; ">{{$new_item->product->lang[0]->name}}</td>
                  <td width="15%" style="padding-bottom: 10px;font-size: 14px;
                     font-weight: 500; ">SAR {{$new_item->item_price}}</td>
                  <td width="10%" style="padding-bottom: 10px;font-size: 14px;
                     font-weight: 500; ">{{$new_item->item_count}}</td>
                  <td width="20%" style="padding-bottom: 10px;font-size: 14px;
                     font-weight: 500; ">SAR {{$amount}} </td>
               @endforeach
               <tr>
                  <td height="20 "colspan="4"
                     style="border-top:0;border-right:0;border-bottom:1px solid #aaa;border-left:0;">
                  </td>
               </tr>
               <tr>
                  <td colspan="1"></td>
                  @php
                                $grant =0;
                                foreach($details->items as $new_item)
                                {
                                    $grant += $new_item->item_price *  $new_item->item_count  ;
                                }
                            @endphp
                  <td colspan="2" style="padding-top: 10px;font-size: 14px;
                     font-weight: 500; "><strong> Total:</strong></td>
                  <td style="padding-top: 10px;font-size: 18px;
                     font-weight: 500; "><b>SAR {{$grant}}</b></td>
               </tr>
               
            </tbody>
         </table>
		 
      {{-- </table> --}}
   </body>
</html>