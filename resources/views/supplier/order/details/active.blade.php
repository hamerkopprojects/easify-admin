@extends('layouts.master')

@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>{{ __('messages.admin.order_info') }}</h1>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="page-header">
                        <div class="page-title">
                            {{-- <div class="col-md-12"> --}}
                            <a href="{{route('supplier.active-order')}}" class="cust_stylee btn back_btn">{{ __('messages.admin.back') }}</a>
                            {{-- </div> --}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <a href="{{route('supplier.order.details.pdf',$details->id)}}"class="cust_styl1 btn btn-info waves-effect waves-light" target="_blank"><i class="fa fa-print"> {{ __('messages.admin.print') }}</i></a>
                    </div>
                </div>
                <form id="frm_create_product" action="javascript:;" method="POST">
                    <div class="tab-pane active" id="pdt_info" role="tabpanel">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label"><b>{{ __('messages.admin.order_info1') }}</b></label>
                                    <br />

                                </div>
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>{{ __('messages.admin.order_id') }}</strong></label><br>
                                    <p>{{$details->order_id}}</p>
                                </div>
                                <input type="hidden" id="order_id" name="order_id" value="{{$details->id}}">
                                <div class="col-md-4">
                                    <label class="control-label"><strong>{{ __('messages.admin.order_date') }}</strong></label><br>
                                    <p> {{\Carbon\Carbon::parse($details->created_at)->format('d-m-Y')}}</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>{{ __('messages.admin.collection_date') }}</strong></label><br>
                                        @if(isset($details->items[0]->collection_date)){{\Carbon\Carbon::parse($details->items[0]->collection_date)->format('d-m-Y')}}
                                        @endif
                                    </div>

                                <div class="col-md-4">
                                    <label class="control-label"><strong>{{ __('messages.admin.driver_info') }} </strong></label><br>
                                        <p>@if(isset($details->items[0]->driver_id)){{$details->items[0]->driver->name}}@endif</p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>{{ __('messages.admin.delivery_location') }}</strong></label><br>
                                    <p>{{$supplier->supplier_region->lang[0]->name}} </p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>{{ __('messages.admin.distance') }}</strong></label><br>
                                    <p>{{round($km,2)}} KM</p>
                                </div>
                                @if($details->order_status == 6)
                                <div class="col-md-4">
                                    <label class="control-label"><strong>{{ __('messages.admin.cancelled_by') }}</strong></label><br>
                                    <p></p>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label"><strong>{{ __('messages.admin.cancellation_reason') }}</strong></label><br>
                                    <p></p>
                                </div>
                                @endif
                                <div class="col-md-4">
                                    <label class="control-label"><strong>{{ __('messages.admin.delivery_note') }}</strong></label><br>
                                    <p>{{$details->order_note}}</p>
                                </div>
                                <br>
                                <div class="col-md-12">
                                    <hr>
                                </div>
                                <table class="table table-bordered ">
                                    <thead>
                                        <tr>
                                            <th width="40%">{{ __('messages.admin.items') }}</th>
                                            <th>{{ __('messages.admin.price1') }}</th>
                                            <th>{{ __('messages.admin.quantity') }}</th>
                                            <th>{{ __('messages.admin.amount') }}</th>
                                            <th width="15%"> </th>
                                            <th width="5%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach($details->items as $new_item)
                                        <tr>

                                            <td>{{$new_item->product->lang[0]->name}}</td>
                                            <td>{{$new_item->item_price}}</td>
                                            <td>{{$new_item->item_count}}</td>
                                            <td>
                                                @php
                                                $amount =$new_item->item_price * $new_item->item_count ;
                                                @endphp
                                                {{$amount}}
                                            </td>

                                            <td class="text-center">
                                                @if($new_item->status == 1)
                                                <a href="#"
                                                    class="btn btn-sm btn-info waves-effect waves-light accept_product"
                                                    title="Accept product" data-supplier="{{$new_item->branch_id}}"
                                                    data-order_id="{{$new_item->order_id}}"
                                                    data-product="{{$new_item->product_id}}"
                                                    data-row ="{{$new_item->id}}"><i
                                                        class="fa fa-check"></i></a>
                                                <a href="#" class="btn btn-sm btn-danger text-white reject_product"
                                                    title="Cancel product" data-supplier="{{$new_item->branch_id}}"
                                                    data-order_id="{{$new_item->order_id}}"
                                                    data-row ="{{$new_item->id}}"
                                                    data-product="{{$new_item->product_id}}"><i class="fa fa-close"></i></a>
                                                @else
                                                <p>{{$pro_status[$new_item->status]}}</p>
                                                @endif
                                            </td>
                                            
                                            <td>
                                                @if(!in_array($new_item->status,[1,3]))
                                                <a href="/product-qr? product={{$new_item->id}}  & order={{$details->id}} & supplier={{$new_item->branch_id}}"class="btn btn-sm btn-danger waves-effect"  target="_blank" title="print product QR code"><i class="fa fa-print"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                                <br>
                                <div class="col-md-12"></div>
                                <div class="col-md-4">
                                    <button class="btn btn-info waves-effect waves-light support_chat"
                                        >Support</button>
                                </div>
                                @php
                                $grant =0;
                                foreach($details->items as $new_item)
                                {
                                 if($new_item->status != 'cancelled')
                                    {
                                        $grant += $new_item->item_price * $new_item->item_count ;
                                    }
                                }
                                @endphp
                                <div class="col-md-4">
                                    <label class="control-label"><strong> {{ __('messages.admin.total_amount') }}</strong></label>
                                </div>
                                <div class="col-md-4">

                                    <label class="control-label"><strong>SAR {{$grant}}</strong></label>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<!-- Modal pooup -->
<div class="modal none-border popupcontent_model" id="formModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="max-height:70%; max-width:50%; margin-top: 50px; margin-bottom:50px;">
        <div class="modal-content popupcontent">

        </div>
    </div>
</div>
<!-- END MODAL -->
@endsection
@push('css')
<style>
  .cust_styl1{
      cursor: pointer;
  }
</style>
@endpush
@push('scripts')
{{-- <script src="{{ asset('assets/js/order_support.js') }}"></script> --}}
<script>
    $('.accept_product').on('click', function () {
        let product = $(this).data("product");
        let supplier = $(this).data("supplier");
        let order = $(this).data("order_id");
        let variant = $(this).data("row");
        $.confirm({
            title: @json(__('messages.admin.confirmation')),
            content: @json(__('messages.admin.accept_pdt_msg')),
            buttons: {
            Yes: function () {
            $.ajax({
            type: "POST",
                    url: "{{route('supplier.accept-order')}}",
                    data: {
                        product:product,
                        supplier:supplier,
                        order:order,
                        variant:variant
                        },
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    window.setTimeout(function () {
                   window.location.reload();
                    }, 1000);
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            },
                    No: function () {
                    window.location.reload();
                    }
            }
    });
    });
    // Cancel

    $('.reject_product').on('click', function () {
        let product = $(this).data("product");
        let supplier = $(this).data("supplier");
        let order = $(this).data("order_id");
        let variant = $(this).data("row");
        $.confirm({
            title: @json(__('messages.admin.confirmation')),
            content: @json(__('messages.admin.cancel_pdt_msg')),
            buttons: {
            Yes: function () {
            $.ajax({
            type: "POST",
                    url: "{{route('supplier.cancel-product')}}",
                    data: {
                        product:product,
                        supplier:supplier,
                        order:order,
                        variant:variant
                        },
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    window.setTimeout(function () {
                   window.location.reload();
                    }, 1000);
//                    window.location.reload();
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            },
                    No: function () {
                    window.location.reload();
                    }
            }
    });
    });

    $('.support_chat').on('click',function(){
        $.ajax({
            type: "POST",
            url: "{{route('supplier.support_chat')}}",
            data:{
              order_id :$('#order_id').val()  
            },
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
            success: function(data) {
                $('.modal-content').html(data);
                $('#formModal').modal('show');
            }
        });
    })
</script>

@endpush