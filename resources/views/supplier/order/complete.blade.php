@extends('layouts.master')

@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-5 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>{{ __('messages.admin.orders') }}</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <x-supp-order-tab activeTab="supp-complete" />
                </div> 
                <div class="col-sm-4">
                <div class="card-header card-header-warning card-header-icon card_style_rev">
                    <h4 class="card-title">{{ __('messages.admin.total_revenue') }}</h4>
                    <p></p>
                    <h3 class="center"><b>SAR {{$detail}} </b></h3>
                </div>
                @if($detail != 0)
                <a href="{{route('supplier.order.excel-download')}}" class="btn btn-info mt-3"><i class="ti-download"></i> Download report as XLS </a>
                @endif
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form id="posts-filter" method="get">
                                <div class="row tablenav top text-left">
                                    <div class="col-md-4 ml-0">
                                        <input class="form-control" type="text" name="search" value="{{$search ??''}}" placeholder="{{ __('messages.admin.search_by_order_id') }}">
                                    </div>
                                    <div class="col-md-4 ml-0">
                                        <input class="form-control datetimepicker" type="text" name="date" value="{{$date ?? ''}}" placeholder="{{ __('messages.admin.by_scheduled_delivery_date') }}">
                                    </div>
                                    <div class="col-md-4 text-left">
                                        <button type="submit" class="btn btn-info">
                                            <font style="vertical-align: inherit;">{{ __('messages.admin.search') }}</font>
                                        </button>
                                        <a href="{{route('supplier.complete-order')}}" class="btn btn-default">{{ __('messages.admin.reset') }}</a>
                                    </div>
                                </div>
                                
                            </form>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div id="msgDiv"></div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    
                                        <tr>
                                            <th>{{ __('messages.admin.sno') }}</th>
                                            <th>{{ __('messages.admin.order_id') }}</th>
                                            <th>{{ __('messages.admin.order_date') }}</th>
                                            <th>{{ __('messages.admin.collection_date') }}</th>
                                            <th>{{ __('messages.admin.amount') }}</th>
                                            <th width="15%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($active) > 0 )
                                        @php
                                        $i = 1;
                                        // dd($active);
                                        @endphp
                                        @foreach ($active as $item)
                                            
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$item->order_id}}</td>
                                            <td>{{ \Carbon\Carbon::parse($item->order_date)->format('d-m-Y')}}</td>
                                            <td>{{\Carbon\Carbon::parse($item->collection_date)->format('d-m-Y')}} {{$item->collection_time_slot}}</td>
                                            <td>SAR {{$item->value}}</td>
                                            <td class="text-center">
                                                <a href="{{ route('supplier.order-details', $item->id) }}" class="btn btn-sm btn-success text-white view_btn" title="Order Details"><i class="fa fa-eye"></i></a>
                                                {{-- <a href="#"class="btn btn-sm btn-danger text-white " title="Accept"><i class="fa fa-check"></i></a>
                                                <a href="#"class="btn btn-sm btn-danger text-white" title="reject"><i class="fa fa-close"></i></a> --}}
                                            </td>
                                        </tr>
                                        @endforeach
                                            
                                        @else
                                        <tr>
                                            <td colspan="8" class="text-center">{{ __('messages.admin.no_records_found') }}</td>
                                        </tr>

                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center d-flex justify-content-center mt-3">
                                {{ $active->links() }}
                            </div>
                        </div>
                    </div>
            
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<style>
    
</style>
@endpush
@push('scripts')
    <script>
         $('input[name="date"]').daterangepicker({
        "singleDatePicker": true,
        "autoUpdateInput": false,
        "autoApply": true,
        // "minDate": new Date(),
        locale: {
                format: 'DD-MM-YYYY'
            }
    });
    $('input[name="date"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
    });
    </script>
@endpush