@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>{{__('messages.admin.repo_head')}}</h1>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
            </div>
            <!-- /# row -->
<div class="row">
    <div class="col-sm-12">
        <x-supp-report-tab reportTab="hourlysales" />
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form id="frm_hourly_chart_filter">
                    <div class="row">
                        <div class="col-md-4 ml-0">
                            <div id="daterange-sales" style="background: #fff; cursor: pointer; padding: 8px 10px; border: 1px solid #ccc; width: 100% ">
                                <i class="fa fa-calendar"></i>&nbsp;
                                <i class="fa fa-caret-down"></i>
                                <span></span>
                            </div>
                        </div>
                        <div class="col-md-4 text-left">
                            <button type="button" class="btn btn-info" id="hourly_filter">
                                <font style="vertical-align: inherit;">{{ __('messages.admin.search') }}</font>
                            </button>
                            <a href="{{route('supplier.sales.hourly')}}" class="btn btn-default">{{ __('messages.admin.reset') }}</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>


    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="card-header card_style_rev">
            <h6 class="center mt-2">{{ __('messages.admin.total_sales') }}</h6>
            <p></p>
            <h4 class="center center" id="total_sales"> SAR 0.00</h4>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card-header card_style_rev">
            <h6 class="center mt-2">{{ __('messages.admin.total_orders') }}</h6>
            <p></p>
            <h4 class="center center" id="total_orders"> 0 </h4>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card-header card_style_rev">
            <h6 class="center mt-2">{{ __('messages.admin.total_customers') }}</h6>
            <p></p>
            <h4 class="center" id="total_customers"> 0 </h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
                <h4 class="card-title mb-3">{{ __('messages.admin.hrly_sale_grph') }}</h4>
                <div id='hourlysaleslinegraph' style="height:450px"></div>

                </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection
@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
<style>
    .card_style_rev {
        background-color: #000;
    }
    .card-header:first-child{
        border-radius: 14px !important;
    }
    .center {
        text-align: center;
        color: white;
    }
    .select2-container{ width:100% !important; }
    .select2-container--default .select2-selection--single {
        height: 42px !important;border-radius: 0px !important;font-size: 1rem !important;
        padding: .4rem !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__arrow b { top:20px !important}
</style>

@endpush
@push('scripts')
<script type="text/javascript">
    var base_url = "{{ URL::to('/') }}";
</script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
<script src="{{ asset('assets/js/reports/supp-reports.js') }}"></script>
<script src="{{ asset('assets/js/lib/Morris/raphael.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/Morris/morris.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/select2/select2.full.min.js') }}"></script>
{{-- <script src="{{ asset('assets/js/lib/spinner/spin.js') }}"></script> --}}
<script>
$(function () {
    $("#hourly_filter").trigger("click");
});
</script>
@endpush