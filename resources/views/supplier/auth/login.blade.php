@extends('layouts.auth')

@section('flash')
@include('user.flash-message')
@endsection

@section('auth-form')

<div class="login-form">
    <h4>SUPPLIER LOGIN</h4>
    <form method="POST" action="{{ route('supplier.supplierlogin') }}" id="supplier_login_page">
        @csrf
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" name="email" id="email" class="form-control @error('email') is-invalid @enderror"
                   placeholder="Email">
            @error('email')
            <span class="invalid-feedback" role="alert">
                <p class="error">{{ $message }}</p>
            </span>
            @enderror
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input id="password" placeholder="Password" type="password"
                   class="form-control @error('password') is-invalid @enderror" name="password" required
                   autocomplete="current-password">
            <span class="input-group-text" toggle="#password">
                <i id="eye" class="fa fa-eye-slash toggle-password" style="color:black;"></i>
            </span>
            @error('password')
            <span class="invalid-feedback" role="alert">
                <p class="error">{{ $message }}</p>
            </span>
            @enderror
        </div>
        <div class="checkbox">
            {{-- <label>
                <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
            </label> --}}
            <label class="pull-right">
                <a href="{{ route('supplier.password.request') }}">Forgot Password?</a>
            </label>

        </div>
        <button type="submit" class="btn btn-primary btn-flat m-b-30 m-t-30">Login</button>
        <div class="register-link m-t-15 text-center">
            <a href="{{ route('supplier.register') }}">Create New Account</a>
        </div>
    </form>
</div>
@endsection
@push('css')
<style>
    #eye{
        float: right;
        margin-right: 14px;
        margin-top: -25px;
        position: relative;
        z-index: 2;
    }
    .abot{
        color: #ffffff !important;
    }
    .error{
        color: red;
    }
    .login-form label{
        text-transform: none !important;
    }
</style>
@endpush
@push('scripts')
<script src="{{asset('assets/js/jquery.validate.js')}}"></script>
<script>
$("#supplier_login_page").validate({
     rules: {
        email: {
        required: true,
        email: true,
        },
        password: {
            required: true,
        },
    },
    messages: {
        email: {
            required: 'Email is required.',
            email: 'Invalid email',
        },
        password: {
            required: 'Password is required'
        },
    }

});
</script>
<script>
    $(".toggle-password").click(function () {

        $(this).toggleClass("fa fa-eye fa fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if ($('#password').attr('type') === 'password') {
            $('#password').attr("type", "text");
        } else {
            $('#password').attr("type", "password");
        }
    });
</script>
@endpush