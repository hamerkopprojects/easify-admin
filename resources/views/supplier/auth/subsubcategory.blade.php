<div style="padding-left:20px;">
    @foreach($subsubcategories as $subcategory)
    <div class="row" role="tab" id="headingTwo{{$subcategory->id}}" style="background: #cdf5e5;">
          <div class="col-md-4">
                <input type="checkbox" id="category" name="category[]" value="{{$subcategory->id}}">
                       <label> {{$subcategory->category_unique_id}}</label>
            </div>
            <div class="col-md-4">
                <label> {{$subcategory->lang[0]->name}}</label>
            </div>
            <div class="col-md-4">
                <label> {{$subcategory->lang[1]->name}}</label>
            </div>
        </div>
    @endforeach
</div>