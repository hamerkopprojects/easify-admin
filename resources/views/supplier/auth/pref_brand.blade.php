@extends('layouts.auth')

@section('flash')
    @include('user.flash-message')
@endsection

@section('auth-form')

<div class="login-form register">
    <h4>PREFERRD BRAND</h4>
    <form method="POST" action="{{ route('supplier.pref_brand.store') }}">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <input type="checkbox" id="checkAll">
                    <label> Check All</label>
                <ul>
                    <input type="hidden" id="supplier_id" name="supplier_id" value="{{$id}}">
                    @foreach($brand as $brands)
                     <li style="background: #94d9be;">
                        <p>
                            <div class="row"> 
                                <div class="col-md-3">
                        <input type="checkbox" id="brand" name="brand[]" value="{{$brands->id}}">
                        <label> {{$brands->brand_unique_id ?? ''}}</label>
                    </div> 
                        <div class="col-md-3">
                        <label> {{$brands->lang[0]->name}}</label>
                    </div> 
                        <div class="col-md-3">
                        <label> {{$brands->lang[1]->name}}</label>
                    </div>        
                    </div>
                        </p>
                    </li>
                    @endforeach
                    </ul>
          </div>
        </div>
      
        <div class="modal-footer">
          
            <a href="{{ route('supplier.login') }}" type="button" class="btn btn-primary btn-sm btn-flat"> SKIP</a>
            <button type="submit" class="btn btn-default btn-sm btn-flat" data-dismiss="modal">NEXT</button>
        </div>
    </form>
</div>
@endsection
@push('css')
<style>
    #eye{
        float: right;
        margin-right: 14px;
        margin-top: -25px;
        position: relative;
        z-index: 2;
    }
    #register{
        text-transform: lowercase !important;
    }
    .login-form .btn {
        float: left !important;
    width: 20% !important;
    }
</style>
@endpush
@push('scripts')
<script>
    $(".toggle-password").click(function() {
     
        $(this).toggleClass("fa fa-eye fa fa-eye-slash");
        var input = $($(this).attr("toggle"));
            if ($('#password').attr('type') === 'password') {
                $('#password').attr("type", "text");
            } else {
                $('#password').attr("type", "password");
            }
    });
    $("#checkAll").change(function () {

        $('input:checkbox').not(this).prop('checked', this.checked);
    });
</script>
@endpush