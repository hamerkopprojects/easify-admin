@extends('layouts.auth')
@section('flash')
    @include('auth.msg')
@endsection
@section('auth-form')

<div class="login-form">
    <h4>VERIFY ACCOUNT</h4>
    <h6 class="sub-title text-center">Thank you for registering with us!</h6>
    <label class="sub-title">A verification code has been sent to your email/ phone number. Type the verification code and complete the registration</label>>
    <form method="POST" action="{{ route('supplier.verify') }}" id="frm_supplier_verify">
        @csrf
        <input type="hidden" name="email" value="{{$email}}">
        <div class="reset-wrapper">
            <input placeholder="Enter verification code" id="code" name="code" type="text" class="form-control el" required autofocus>
            <button type="submit" class="btn btn-primary btn-flat m-b-15 el">VERIFY</button>
        </div>
        <label class="errorTxt"></label>
        @if($errors->any())<p class="error">{{$errors->first()}}</p> @endif
    </form>
    <div id="resend"style="color: black; text-align: center">Didn't receive code?<strong id="resend_token"><u> Resend</u></strong></div>
</div>
</div>

@endsection

@push('css')
<style>
    .sub-title {
        color: #656565;
        margin-bottom: 20px;
    }

    .reset-wrapper {
        display: flex;
        justify-content: space-between;
    }

    .reset-wrapper .el {
        width: 49%;
    }

    .reset-wrapper input.el {
        height: 47px;
    }
    #resend strong{
        cursor: pointer;
    }
    .error{
        color: red;
    }
    .login-form label{
        text-transform: none !important;
    }
</style>
@endpush
@push('scripts')
<script src="{{asset('assets/js/jquery.validate.js')}}"></script>
<script>
$("#frm_supplier_verify").validate({
     rules: {
        code: {
        required: true,
        number: true,
        },
    },
    messages: {
        code: {
            required: 'Verification code is required.',
            number: 'Numbers only',
        },
    },
    errorElement : 'label',
    errorLabelContainer: '.errorTxt'

});
</script>
<script>
$('#resend_token').on('click',function(){
        window.history.back();
  
});
</script>
@endpush