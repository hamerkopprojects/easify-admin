<div style="padding-left:20px;">
    @foreach($subcategories as $subcategory)
    <div class="row" role="tab" id="headingTwo{{$subcategory->id}}" style="background: #94d9be;" class="mb-1">
            <div class="col-md-4">
                <input type="checkbox" id="category" name="category[]" value="{{$subcategory->id}}">
                       <label> {{$subcategory->category_unique_id}}</label>
            </div>
            <div class="col-md-3">
                <label>{{$subcategory->lang[0]->name}}</label>
            </div>
            <div class="col-md-3">
                <label>{{$subcategory->lang[1]->name}}</label>
            </div>
            <div class="col-md-2">
                <a class="collapsed" data-toggle="collapse"
                   href="#collapseTwo{{$subcategory->id}}" aria-expanded="false"
                   aria-controls="collapseTwo{{$subcategory->id}}"
                   style="float: right">
                    @if(count($subcategory->subcategory))
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                    @endif
                </a>
            </div>
        </div>
    @if(count($subcategory->subcategory))
    <div id="collapseTwo{{$subcategory->id}}" class="collapse show" role="tabpanel"
         aria-labelledby="headingTwo{{$subcategory->id}}">
        <div class="card-body" style="padding: 2px 0px;">
           @include('supplier.auth.subsubcategory',['subsubcategories' =>$subcategory->subcategory])
        </div>
    </div>
    @endif
    @endforeach
</div>