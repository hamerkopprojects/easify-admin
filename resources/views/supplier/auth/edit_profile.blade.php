@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>{{ __('messages.admin.edit_profile') }}</h1>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /# row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form id="frm_edit_profile" action="javascript:;" method="POST">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>{{ __('messages.admin.name') }} <span class="text-danger"> *</span></label>
                                        <input class="form-control" name="auth_name" autocomplete="off" value="{{ $supplier->name ?? ''}}">
                                    </div>
                                    <div class="col-md-6">
                                        <label>{{ __('messages.admin.email') }}<span class="text-danger"> *</span></label>
                                        <input type="text" class="form-control" id="auth_email" name="auth_email" value="{{ $supplier->email ?? ''}}">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="control-label">{{ __('messages.admin.phone') }} <span class="text-danger">*</span></label>
                                        <input class="form-control form-white" type="text" name="country_code" value="+966" id="country_code" readonly/>
                                    </div>
                                    <div class="col-md-4">
                                        <label>&nbsp;</label>
                                        <input type="text" class="form-control" id="phone" name="phone" value="{{ $supplier->phone ?? ''}}" autocomplete="off">
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-info waves-effect waves-light">{{ __('messages.admin.save') }}</button>
                                        <button type="button" class="btn btn-default waves-effect" onclick="window.location.reload();">{{ __('messages.admin.cancel') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>{{ __('messages.admin.change_password') }}</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form id="frm_change_password" action="javascript:;" method="POST">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>{{ __('messages.admin.password') }} <span class="text-danger"> *</span></label>
                                        <input type="password" class="form-control" name="password" id="password" autocomplete="off" placeholder="{{ __('messages.admin.new_pwd') }}">
                                        <span class="input-group-text" toggle="#password">
                                         <i id="eye" class="fa fa-eye-slash toggle-password" style="color:black;"></i>
                                        </span>
                                    </div>
                                    <div class="col-md-6">
                                        <label>{{ __('messages.admin.confirm_password') }} <span class="text-danger"> *</span></label>
                                        <input type="password" placeholder="{{ __('messages.admin.confirm_password') }}" class="form-control" id="confirm_password" name="confirm_password">
                                        <span class="input-group-text" toggle="#confirm_password">
                                         <i id="eye" class="fa fa-eye-slash toggle-confirm-password" style="color:black;"></i>
                                        </span>
                                    </div>
                                    <div class="col-lg-12">
                                        <button type="submit" class="btn btn-info waves-effect waves-light save-categorys">
                                            {{ __('messages.admin.save') }}
                                        </button>
                                        <button type="button" class="btn btn-default waves-effect" onclick="window.location.reload();">{{ __('messages.admin.cancel') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $("#frm_edit_profile").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        rules: {
            auth_name: {
                required: true,
            },
            auth_email: {
                required: true,
                email: true,
            },
            phone: {
                required: true,
                number: true,
                minlength: 6, // will count space
                maxlength: 12
            },
        },
        messages: {
            auth_name: {
                required: @json(__('messages.admin.profile_validation1') )
            },
            auth_email: {
                required: @json(__('messages.admin.profile_validation2') ),
                email: @json(__('messages.admin.profile_validation3') ),
            },
            phone: {
                required: @json(__('messages.admin.profile_validation4') ),
                number: @json(__('messages.admin.profile_validation5') )
            },
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "{{route('supplier.profile')}}",
                data: $('#frm_edit_profile').serialize(),
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data.status == 1) {
                        Toast.fire({
                            icon: 'success',
                            title: data.message
                        });
                        window.setTimeout(function () {
                            window.location.href = '{{route("supplier.edit")}}';
                        }, 1000);
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                    }
                }
            });
            return false;
        }
    });
    $("#frm_change_password").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        rules: {
            password: {
                required: true,
                minlength: 6,
            },
            confirm_password: {
                required: true,
                minlength: 6,
                equalTo: '#password'
            }
        },
        messages: {
            password: {
                required: @json(__('messages.admin.profile_validation6') ),
            },
            confirm_password: {
                required: @json(__('messages.admin.profile_validation7') ),
                equalTo: @json(__('messages.admin.profile_validation8') )
            }
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "{{route('supplier.password_new')}}",
                data: $('#frm_change_password').serialize(),
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data.status == 1) {
                        Toast.fire({
                            icon: 'success',
                            title: data.message
                        });
                        window.setTimeout(function () {
                            window.location.href = '{{route("supplier.edit")}}';
                        }, 1000);
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                    }
                }
            });
            return false;
        }
    });
    $(".toggle-password").click(function() {
     
        $(this).toggleClass("fa fa-eye fa fa-eye-slash");
        var input = $($(this).attr("toggle"));
            if ($('#password').attr('type') === 'password') {
                $('#password').attr("type", "text");
            } else {
                $('#password').attr("type", "password");
            }
    });
    $(".toggle-confirm-password").click(function() {
     
        $(this).toggleClass("fa fa-eye fa fa-eye-slash");
            if ($('#confirm_password').attr('type') === 'password') {
                $('#confirm_password').attr("type", "text");
            } else {
                $('#confirm_password').attr("type", "password");
            }
    });
</script>
@endpush