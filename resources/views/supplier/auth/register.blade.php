@extends('layouts.auth')

@section('flash')
@include('user.flash-message')
@endsection

@section('auth-form')

<div class="login-form register">
    <h4>SUPPLIER REGISTER</h4>
    <form method="POST" id ="create_new_supplier" action="{{ route('supplier.store') }}" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <label for="supplier_name">Supplier Name</label> <span class="text-danger">*</span>
                        <input type="text" name="supplier_name" id="supplier_name" value="{{ old('supplier_name') }}" class="form-control"
                               placeholder="Enter supplier name">
                    </div>
                    <div class="col-md-6">
                        <label for="contact_name">Contact Person Name</label> <span class="text-danger">*</span>
                        <input type="text" name="contact_name" id="contact_name" value="{{ old('contact_name') }}" class="form-control"
                               placeholder="Enter contact person name">
                    </div>

                    <div class="col-md-6">
                        <label for="phone">Contact Person Phone</label> <span class="text-danger">*</span>
                        <input type="text" name="phone" value="{{ old('phone') }}" id="phone" class="form-control @error('phone') is-invalid @enderror"
                               placeholder="Enter contact person phone">
                        @error('phone')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-md-6">
                        <label for="email">Contact Person Email</label> <span class="text-danger">*</span>
                        <input type="email" name="email" id="email"  value="{{ old('email') }}" class="form-control @error('email') is-invalid @enderror"
                               placeholder="Enter email">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="col-md-6">
                        <label class="control-label">City <span class="text-danger">*</span></label>
                        <select class="form-control" name="city" onchange="getRegion(this);">
                            <option value=""> Select City </option>
                            @foreach($city as $city_val)
                            @if (old('city') == $city_val->id)
                            <option value="{{ $city_val->id }}" selected>{{ $city_val->lang[0]->name }}</option>
                            @else
                            <option value="{{ $city_val->id }}">{{ $city_val->lang[0]->name }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">Region <span class="text-danger">*</span></label>
                        <select  class="form-control" name="region" id="region">
                            <option value=""> Select Region </option>
                            @foreach($region as $region_val)
                            @if (old('region') == $region_val->id)
                            <option value="{{ $region_val->id }} " selected>{{ $region_val->lang[0]->name }}</option>
                            @else
                            <option value="{{ $region_val->id }}">{{ $region_val->lang[0]->name }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label">CR Copy </label>
                        <div class ="imgup">
                            <div class="scrn-link" >
                                <button type="button" class="scrn-img-close delete-img" data-id="" id="scn" onclick="removesrc_en()" data-type="app_image" style="display: none;">
                                    <i class="ti-close"></i>
                                </button>
                                <img id="previewimage_en" class="cover-photo" width="200" onclick="$('#uploadFile_en').click();" src="{{ asset('assets/images/upload.png') }}" />
                            </div>
                            <input type="file" id="uploadFile_en" name="product_image_en" style="visibility: hidden;" accept="image/*" value="" />
                            <input type="hidden" name="hidden_image_en" id="hidden_image_en">
                             
                        </div>
                        <span class="error"></span>
                        <div class="catimg">
                            <p class="small">Supported formats: jpeg,png,pdf</p>
                        </div>
                        <p id="display_image" class="mt-3"></p>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" id="backbutton" class="btn btn-primary btn-sm btn-flat" id="save_data">
                        BACK
                    </button>
                    <button type="submit" class="btn btn-default btn-sm btn-flat" data-dismiss="modal">SUBMIT</button>
                </div>
                </form>
            </div>
        </div>
@endsection
@push('css')
<style>
    #eye{
        float: right;
        margin-right: 14px;
        margin-top: -25px;
        position: relative;
        z-index: 2;
    }
    .login-form .btn {
        float: left !important;
        width: 20% !important;
    }

    .modal-footer {

        display: inherit !important;
    }
    .login-form label{
        text-transform: none !important;
    }
</style>
    @endpush
    @push('scripts')
    <script src="{{asset('assets/js/jquery.validate.js')}}"></script>
    <script>


        $('#backbutton').on('click', function(){
        window.history.back();
        });
        function removesrc_en(){
        document.getElementById("scn").style.display = "none";
        $('#previewimage_en').attr('src', '{{ asset('assets/images/upload.png') }}');
        $('#hidden_image_en').remove();
        $("#uploadFile_en").val("");
        };
    document.getElementById("uploadFile_en").onchange = function(e) {
    var focusSet = false;
    var reader = new FileReader();
    var fileUpload = document.getElementById("uploadFile_en");
    var imgPath = fileUpload.value;
    var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
    if (fileUpload != '') {
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.jpeg|.pdf)$");
    var file_size = $('#uploadFile_en')[0].files[0].size;
    if (regex.test(fileUpload.value.toLowerCase()) == '') {
    $("#uploadFile_en").parent().next(".validation").remove(); // remove it
    $('#previewimage_en').attr('src', '{{ asset('assets/images/upload.png') }}');
    $('#hidden_image_en').remove();
    $("#uploadFile_en").val("");
    $("#uploadFile_en").parent().after("<div class='validation' style='color:red;font-size: 12px;  font-weight: 400;'>The image must be of the format jpeg png or pdf </div>");
    }
    //            else if(file_size > 512000) {
    //                $("#uploadFile_en").parent().next(".validation").remove(); // remove it
    //                $('#previewimage_en').attr('src', '{{ asset('assets/images/upload.png') }}');
    //                $('#hidden_image_en').remove();
    //                $("#uploadFile_en").val("");
    //                $("#uploadFile_en").parent().after("<div class='validation' style='color:red;font-size: 12px;  font-weight: 400;'>The image must be less than 512Kb in size </div>");
    //            }
    else if (fileUpload != ''){
    $("#uploadFile_en").parent().next(".validation").remove(); // remove it

    var fileUpload = document.getElementById("uploadFile_en");
    var reader = new FileReader();
    //Read the contents of Image File.
    reader.readAsDataURL(fileUpload.files[0]);
    reader.onload = function (e) {

    //Initiate the JavaScript Image object.
    var image = new Image();
    //Set the Base64 string return from FileReader as source.
    image.src = e.target.result;
    //Validate the File Height and Width.
    image.onload = function () {
    var height = this.height;
    var width = this.width;
    $("#uploadFile_en").parent().next(".validation").remove(); // remove it
    var reader = new FileReader();
    document.getElementById("scn").style.display = "block";
    $("#display_image").text('');
    document.getElementById("previewimage_en").src = e.target.result;
    reader.readAsDataURL(fileUpload.files[0]);
    }
    if (extn == "pdf") {
    $('#preview_img').attr('src', '');
    $("#display_image").text(fileUpload.files[0]['name']);
    }
    $.toaster({
    priority : 'success',
            title    : 'Success',
            message  : "File uploaded successfully",
            timeout  : 3000,
    });
    }

    }
    else{
    $("#uploadFile_en").parent().next(".validation").remove(); // remove it
    var reader = new FileReader();
    reader.onload = function(e) {
    document.getElementById("scn").style.display = "block";
    document.getElementById("previewimage_en").src = e.target.result;
    };
    reader.readAsDataURL(this.files[0]);
    }

    } else{
    $("#uploadFile_en").parent().next(".validation").remove(); // remove it

    }
    };
    $(".toggle-password").click(function() {

    $(this).toggleClass("fa fa-eye fa fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if ($('#password').attr('type') === 'password') {
    $('#password').attr("type", "text");
    } else {
    $('#password').attr("type", "password");
    }
    });
    $("#create_new_supplier").validate({
    normalizer: function (value) {
    return $.trim(value);
    },
            rules: {
            supplier_name: {
            required: true,
            },
                    contact_name: {
                    required: true,
                    },
                    phone: {
                    required: true,
                    },
                    city: {
                    required: true,
                    },
                    email: {
                    required: true,
                    },
                    region: {
                    required: true,
                    },
            },
            messages: {
            supplier_name: {
            required: 'Name is required.'
            },
                    contact_name: {
                    required: 'Contact name is required.'
                    },
                    phone: {
                    required: 'Phone is required.'
                    },
                    email: {
                    required: 'Email is required.'
                    },
                    city: {
                    required: 'City is required.'
                    },
                    region: {
                    required: 'Region is required.'
                    },
            },
            errorElement: "label",
            errorPlacement: function (error, element) {
            if (element.attr("id") == "sec_category") {
            error.insertAfter("#error_chk");
            } else {
            error.insertAfter(element);
            }
            },
    });
    function getRegion(sel)
    {
    var city_id = sel.value;
    $.ajax({
    type: "GET",
            url: "{{route('supplier.get_region')}}",
            data: {'city_id': city_id},
            success: function (data) {
            var options = "";
            options += '<option value=""> Select Region </option>';
            for (var i = 0; i < data.length; i++) {
            options += '<option value=' + data[i].id + '>' + data[i].lang[0].name + "</option>";
            }
            $("#region").html(options);
            }
    });
    }
        </script>
        @endpush