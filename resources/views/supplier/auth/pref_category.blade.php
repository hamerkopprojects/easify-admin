@extends('layouts.auth')

@section('flash')
@include('user.flash-message')
@endsection

@section('auth-form')

<div class="login-form register">
    <h4>PREFERED CATEGORY</h4>
    <form method="POST" action="{{ route('supplier.preferred_category') }}">
        @csrf
        <div class="row">
            <div class="col-md-12">
                    <input type="checkbox" id="checkAll">
                    <label> Check All</label>
                <!--Accordion wrapper-->
                <div class="accordion md-accordion" id="accordionEx1" role="tablist" aria-multiselectable="true">
                    <!-- Accordion card -->
                    <input type="hidden" id="supplier_id" name="supplier_id" value="{{$id}}">
                    @foreach($main_cat as $categorylist)
                    <div class="card">
                        <!-- Card header -->

                        <div class="row" role="tab" id="headingTwo{{$categorylist->id}}" style="background: #5de4af;">
                               <div class="col-md-4">
                                    <input type="checkbox" id="category" name="category[]" class="@error('category') is-invalid @enderror" value="{{$categorylist->id}}">
                                    <label> {{$categorylist->category_unique_id}}</label>
                                </div>
                                <div class="col-md-3">
                                    <label> {{$categorylist->lang[0]->name}}</label>
                                </div>
                                <div class="col-md-3">
                                    <label> {{$categorylist->lang[1]->name}}</label>
                                </div>
                                <div class="col-md-2">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseTwo{{$categorylist->id}}"
                                       aria-expanded="false" aria-controls="collapseTwo{{$categorylist->id}}" style="float: right">
                                        @if(count($categorylist->subcategory))
                                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                                        @endif
                                    </a>
                                </div>
                        </div>

                        <!-- Card body -->
                        @if(count($categorylist->subcategory))
                        <div id="collapseTwo{{$categorylist->id}}" class="collapse show" role="tabpanel" aria-labelledby="headingTwo{{$categorylist->id}}"
                             data-parent="#accordionEx1">
                            <div class="card-body" style="padding: 2px 0px;">
                                @include('supplier.auth.subCategoryList',['subcategories' => $categorylist->subcategory])

                            </div>
                        </div>
                        @endif
                    </div>
                    <!-- Accordion card -->
                    @endforeach
                </div>
                @if ($error = $errors->first('password'))
                <div class="alert alert-danger">
                    {{ $error }}
                </div>
                @endif
                <!-- Accordion wrapper -->
            </div>
        </div>
        <div class="modal-footer">
            <a href="{{ route('supplier.pref-brand',['supplier_id'=>$id]) }}" type="button" class="btn btn-primary btn-sm btn-flat"> SKIP</a>
            <button type="submit" class="btn btn-default btn-sm btn-flat">NEXT</button>
        </div>
    </form>
</div>
@endsection
@push('css')
<style>
    #eye{
        float: right;
        margin-right: 14px;
        margin-top: -25px;
        position: relative;
        z-index: 2;
    }
    #register{
        text-transform: lowercase !important;
    }
    .crdhead {
        padding: -1.25rem 1.25rem !important;
    }
    .login-form .btn {
        float: left !important;
        width: 20% !important;
    }

</style>
@endpush
@push('scripts')
<script>
    $(".toggle-password").click(function () {

        $(this).toggleClass("fa fa-eye fa fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if ($('#password').attr('type') === 'password') {
            $('#password').attr("type", "text");
        } else {
            $('#password').attr("type", "password");
        }
    });
    $("#checkAll").change(function () {

        $('input:checkbox').not(this).prop('checked', this.checked);
    });
</script>
@endpush