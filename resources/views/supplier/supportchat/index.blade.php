@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>{{ __('messages.admin.support_request') }}</h1>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 ml-0">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12 text-right">
                                <a href="javascript:void(0);" class="btn btn-info pdt_modal_btn">
                                    <font style="vertical-align: inherit;"><i class="ti-plus"></i> {{ __('messages.admin.add_new') }}
                                    </font>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div id="msgDiv"></div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>{{ __('messages.admin.sno') }}</th>
                                            <th>{{ __('messages.admin.subject') }}</th>
                                            <th>{{ __('messages.admin.message') }}</th>
                                            <th width="10%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($support) > 0)
                                        @php
                                        $i = 1;
                                        @endphp

                                        @foreach ($support as $list)
                                            
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>{{$list->subject}}</td>
                                                <td>{{$list->message}}</td>
                                                <td class="center">
                                                    <a href="#" class="btn btn-sm btn-success text-white view_btn" title="support Chat Details" data-req-id="{{ $list['id']}}"><i class="fa fa-eye"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="8" class="text-center">{{ __('messages.admin.no_records_found') }}</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center d-flex justify-content-center mt-3">
                                {{ $support->links() }}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

    <div class="modal fade" id="request-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ __('messages.admin.support_chat') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="chat-history" data-no-padding="no-padding">
                    <div class="container">
                        <div class="basic_details">

                        </div>
                        <div class="messaging">
                            <div class="inbox_msg">
                                <div class="mesgs">
                                    <div class="msg_history">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="type_msg">
                        <div class="input_msg_write">
                            <textarea type="text" id="chat-msg" class="write_msg" placeholder="{{ __('messages.admin.type_message') }}" rows="1" cols="100" style="padding-top: 10px" ></textarea>
                            {{-- <input type="text" id="chat-msg" class="write_msg" placeholder="Type a message"> --}}
                            <button id="send-msg" class="msg_send_btn" type="button"><span class="material-icons">send</span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal none-border" id="formModal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" style="max-height:70%; max-width:50%; margin-top: 50px; margin-bottom:50px;">
            <div class="modal-content-support modal-content">

            </div>
        </div>
    </div>
@endsection
@push('css')
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<style>
     
</style>
@endpush
@push('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function(){

            $('.view_btn').on('click', function(e) {
            let requestId = $(this).data('req-id');

            $('#send-msg').data('req-id', requestId);
            $.ajax({
                url: `{{ url('supplier/support-request/chat') }}/${requestId}`,
                type: 'GET',
                success: function(data) {
                    let chat = data.data.chat;
                    let details = data.data.details;
                    console.log("msg",details);
                    $('#chat-history .basic_details').empty()
                    $('#chat-history .basic_details').append(
                       
                    `<div class="row">
                                <div class="col-md-6">
                                    <p>{{ __('messages.admin.subject') }}</p>
                                    <span class="time_date">${details.subject}</span>
                                </div>
                                <div class="col-md-6">
                                    <p>{{ __('messages.admin.message') }}</p>
                                    <span class="time_date">${details.message}</span>
                                </div>
                        </div>
                    `
                    )
                    $('#chat-history .msg_history').empty()
                   
                    chat.forEach(msg => {
                        $('#chat-history .msg_history').append(
                            msg.user_type == 'supplier' ?
                            `
                            <div class="outgoing_msg">
                                <div class="sent_msg">
                                    <p>${msg.message}</p>
                                    <span class="time_date">${msg.time}</span>
                                </div>
                            </div>
                            `
                            :
                            `
                            <div class="incoming_msg">
                                <div class="received_msg">
                                    <div class="received_withd_msg">
                                        <p>${msg.message}</p>
                                        <span class="time_date">${msg.time}</span>
                                    </div>
                                </div>
                            </div>
                            `
                        )
                    })
                     $('#request-modal').modal({
                        show: true
                     });
                    setTimeout(() => {
                        let objDiv = document.getElementById("chat-history");
                        objDiv.scrollTop = objDiv.scrollHeight;
                    }, 200)
                }
            })
        });
        $('#send-msg').click(function(e) {
        let msg = $('#chat-msg').val();
        let replace_msg = msg.replace(/^\s+|\s+$/gm,'');
        let requestId = $(this).data('req-id');
        if(replace_msg)
        {
            $('#chat-history .msg_history').append(`
                <div class="outgoing_msg">
                    <div class="sent_msg">
                        <p>${msg.replace(/\n/g, "<br>")}</p>
                        <span class="time_date">now</span>
                    </div>
                </div>
            `)

            let objDiv = document.getElementById("chat-history");
            objDiv.scrollTop = objDiv.scrollHeight;
            $.ajax({
                url: `{{ url('supplier/support-request/chat') }}/${requestId}`,
                type: 'POST',
                data: {
                 message: msg
                },
                success: function(error) {
                    $('#chat-msg').val('');
                }
            })
        }
       

        
    })
    });
    
$('.pdt_modal_btn').on('click', function() {

$.ajax({
type: "GET",
        url: "{{route('supplier.create-support')}}",
        success: function(data) {
        $('.modal-content-support').html(data);
        $('#formModal').modal('show');
        }
});
});
    </script>
@endpush