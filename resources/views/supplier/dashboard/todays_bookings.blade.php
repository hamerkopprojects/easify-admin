<div class="row">
    <div class="col-lg-12 ml-0">
        <div class="page-title">
            <h5>{{ __('messages.admin.todays_bookings') }} ({{\Carbon\Carbon::now()->format('j F Y')}})</h5>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered color-table">
                        <thead>
                            <tr>
                                <th>{{ __('messages.admin.sno') }}</th>
                                <th>{{ __('messages.admin.order_id') }}</th>
                                <th>{{ __('messages.admin.customer') }} </th>
                                <th>{{ __('messages.admin.scheduled_delivery_date') }}</th>
                                <th>{{ __('messages.admin.amount') }}</th>
                                <th>{{ __('messages.admin.status') }}</th>
                                <th width="15%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($todays_bookings) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($todays_bookings as $order)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{$order->order_id}}</td>
                                <td>{{ucfirst($order->cust_name)}}</td>
                                <td>{{ date('j F Y', strtotime($order->delivery_schedule_date))}} - {{$order->time_slot}}</td>
                                <td>SAR {{$order->item_sum}}</td>
                                <td>Pending</td>
                                <td class="text-center">
                                    <a href="{{ route('supplier.order-details', $order->id) }}" class="btn btn-sm btn-info text-white view_btn" title="View"><i class="fa fa-eye"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="12" class="text-center">{{ __('messages.admin.no_records_found') }}</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $todays_bookings->appends(request()->input())->links() }}
                </div>
            </div>
        </div>
    </div>
</div>