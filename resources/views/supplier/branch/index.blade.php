@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>{{ __('messages.admin.branches') }}</h1>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
                <div class="col-lg-3">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12 text-right">
                                <a href="javascript:void(0);" id = "add_new_branch" class="btn btn-info create_btn">
                                    <font style="vertical-align: inherit;"><i class="ti-plus"></i> {{ __('messages.admin.add_branch') }}
                                    </font>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
            </div>
            <!-- /# row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form id="posts-filter" method="get" action="{{ route('supplier.branch') }}">
                                <div class="row tablenav top text-right">
                                    <div class="col-md-5 ml-0">
                                        <input class="form-control" type="text" name="search" value="{{$search}}" placeholder="{{ __('messages.admin.search_email_phone') }}">
                                    </div>
                                    <div class="col-md-3 text-left">
                                        <button type="submit" class="btn btn-info">
                                            <font style="vertical-align: inherit;">{{ __('messages.admin.search') }}</font>
                                        </button>
                                        <a href="{{ route('supplier.branch') }}" class="btn btn-default reset_style">{{ __('messages.admin.reset') }}</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div id="msgDiv"></div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>{{ __('messages.admin.sno') }}</th>
                                            <th>{{ __('messages.admin.branch_code') }}</th>
                                            <th>{{ __('messages.admin.branch_name') }}</th>
                                            <th>{{ __('messages.admin.email') }}</th>
                                            <th>{{ __('messages.admin.phone') }}</th>
                                            <th>{{ __('messages.admin.region') }}</th>
                                            <th width="25%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($branch) > 0)
                                        @php
                                        $i = 1;
                                        @endphp

                                        @foreach ($branch as $key => $row_data)
                                        @if(!empty($row_data->supplier))
                                        <tr>
                                            <th>{{ $i++ }}</th>
                                            <td>{{ $row_data->supplier->code ?? '' }}</td>
                                            <td>{{ $row_data->supplier->name ?? '' }}</td>
                                            <td>{{ $row_data->supplier->email ?? '' }}</td>
                                            <td>{{ $row_data->supplier->phone ?? '' }}</td>
                                            <td>{{ $row_data->supplier->region->name ?? '' }}</td>

                                            <td class="text-left">
                                                <a class="btn btn-sm btn-danger text-white password_send" title="Send password" data-id="{{ $row_data->supplier->id }}"><i class="fa fa-envelope"></i></a>

                                                <a class="btn btn-sm btn-success text-white edit_btn edit_branch" title="Edit" data-id="{{ $row_data->supplier->id }}"><i class="fa fa-edit"></i></a>

                                                <button type="button" class="change-status btn btn-sm btn-toggle ml-0 {{$row_data->supplier->status}}" data-toggle="button" data-id="{{ $row_data->supplier->id }}" data-status="{{ $row_data['status'] == 'active' ? 'Deactivate' :'Activate' }}" aria-pressed="true" autocomplete="off">
                                                    <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                                </button>


                                                <a class="btn btn-sm btn-danger text-white" title="Delete Branch" onclick="deleteBranch({{ $row_data->supplier->id }})"><i class="fa fa-trash"></i></a>


                                            </td>
                                        </tr>
                                        @else
                                        @if($key == 0)
                                        <tr>
                                            <td colspan="8" class="text-center">{{ __('messages.admin.no_records_found') }}</td>
                                        </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="8" class="text-center">{{ __('messages.admin.no_records_found') }}</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center d-flex justify-content-center mt-3">
                                {{ $branch->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- Pop Up --}}
<div class="modal fade" id="user_popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="name_change"><strong>
                        ADD BRANCH
                    </strong></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form id="branch_form" action="javascript:;" method="POST">
                <div class="modal-body">
                    <div class="msg_div"></div>
                    <div class="row">
                        <input type="hidden" name="branch_unique" id='branch_unique'>
                        <div class="col-md-6">
                            <label class="control-label">{{ __('messages.admin.branch_name') }} <span class="text-danger"> *</span></label>
                            <input class="form-control form-white" placeholder="{{ __('messages.admin.enter_branch_name') }}" type="text" name="name" id="name" />
                        </div>
                        <div class="col-md-6">
                            <label>{{ __('messages.admin.contact_person_name') }}<span class="text-danger"> *</span></label>
                            <input class="form-control" name="contact_name" id="contact_name" placeholder="{{ __('messages.admin.enter_name') }}" />
                        </div>
                        <div class="col-md-6">
                            <label>{{ __('messages.admin.email') }}</label><span class="text-danger"> *</span>
                            <input class="form-control" name="email" id="email" placeholder="{{ __('messages.admin.enter_email') }}" />
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">{{ __('messages.admin.phone') }} <span class="text-danger">*</span></label>
                            <input class="form-control form-white" placeholder="{{ __('messages.admin.enter_phone') }}" type="text" name="phone" id="phone" />
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">{{ __('messages.admin.city') }} <span class="text-danger"> *</span></label>
                            <select class="form-control" name="city" id="city" onchange="getRegion(this);">
                                <option value=""> {{ __('messages.admin.select_city') }} </option>
                                @foreach($city as $city_val)
                                <option value="{{ $city_val->id }}">{{ $city_val->lang[0]->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">{{ __('messages.admin.region') }}<span class="text-danger">*</span></label>
                            <select  class="form-control" name="region" id="region">
                                <option value=""> {{ __('messages.admin.select_region') }} </option>
                                @foreach($region as $region_val)
                                <option value="{{ $region_val->id }}">{{ $region_val->lang[0]->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="submit" class="btn btn-info waves-effect waves-light save-branch" id="save_data">
                        {{ __('messages.admin.save') }}
                    </button>
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">{{ __('messages.admin.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- end popup --}}
@endsection
@push('css')
<style>

</style>
@endpush
@push('scripts')

<script>


    $('#add_new_branch').on('click', function() {
    $('#name_change').html(@json(__('messages.admin.add_branch')));
    $('#save_data').text(@json(__('messages.admin.save'))).button("refresh");
    $("#branch_form")[0].reset();
    $('#user_popup').modal({
    show: true
    });
    })
            $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });
    $('.save-branch').on('click', function(e) {
    // e.preventDefault();


    $("#branch_form").validate({
    rules: {
    name: {
    required: true,
    },
            contact_name: {
            required: true,
            },
            email: {
            required: true,
            email: true,
            },
            phone: {
            required: true,
                    number: true,
                    minlength: 6, // will count space
                    maxlength: 12
            },
            city: {
            required: true,
            },
            region: {
            required: true,
            },
    },
            messages: {
            name: {
            required: @json(__('messages.admin.branch_validation_1')),
            },
                    contact_name: {
                    required: @json(__('messages.admin.branch_validation_2')),
                    },
                    email: {
                    required: @json(__('messages.admin.branch_validation_3')),
                    email: @json(__('messages.admin.branch_validation_9')),
                    },
                    phone: {
                    required: @json(__('messages.admin.branch_validation_4')),
                    number: @json(__('messages.admin.branch_validation_5')),
                    maxlength: @json(__('messages.admin.branch_validation_6')),
                    },
                    city: {
                    required: @json(__('messages.admin.branch_validation_7')),
                    },
                    region: {
                    required: @json(__('messages.admin.branch_validation_8')),
                    },
            },
            submitHandler: function(form) {
            branch_unique = $("#branch_unique").val();
            if (branch_unique) {
            $.ajax({
            type: "POST",
                    url: "{{route('branch.update')}}",
                    data: {
                    name: $('#name').val(),
                            contact_name: $('#contact_name').val(),
                            email: $('#email').val(),
                            phone: $('#phone').val(),
                            city: $('#city').val(),
                            region: $('#region').val(),
                            branch_unique: branch_unique


                    },
                    success: function(data) {
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    window.setTimeout(function() {
                    window.location.reload();
                    }, 1000);
                    $("#branch_form")[0].reset();
                    } else {
                    // console.log(data.message);
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            } else {
            $.ajax({
            type: "POST",
                    url: "{{route('branch.store')}}",
                    data: {
                    name: $('#name').val(),
                            contact_name: $('#contact_name').val(),
                            email: $('#email').val(),
                            phone: $('#phone').val(),
                            city: $('#city').val(),
                            region: $('#region').val(),
                    },
                    success: function(data) {
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    window.setTimeout(function() {
                    window.location.reload();
                    }, 1000);
                    $("#branch_form")[0].reset();
                    } else {
                    // console.log(data.message);
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            }



            }
    })
    });
    $('.edit_branch').on('click', function(e) {
    page = $(this).data('id')


            $('#name_change').html(@json(__('messages.admin.edit_branch')));
    $('#save_data').text(@json(__('messages.admin.save'))).button("refresh");
    var url = "branch/edit/";
    $.get(url + page, function(data) {

    $('#name').val(data.branch.name),
            $('#contact_name').val(data.branch.name),
            $('#phone').val(data.branch.phone),
            $('#email').val(data.branch.email),
            $('#city').val(data.branch.city_id),
            $('#region').val(data.branch.region_id),
            $('#branch_unique').val(data.branch.id)
            $('#user_popup').modal({
    show: true

    });
    });
    });
    $('.change-status').on('click', function(e) {
    console.log($(this).data('status'));
    var id = $(this).data('id');
    var act_value = $(this).data('status');
    console.log(id);
    $.confirm({
    title: act_value + ' Branch',
            content: 'Are you sure to ' + act_value + ' the branch?',
            buttons: {
            Yes: function() {
            $.ajax({
            type: "POST",
                    url: "{{route('branch.status.update')}}",
                    data: {
                    id: id,
                            status: act_value
                    },
                    success: function(data) {
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    window.setTimeout(function() {
                    window.location.href = '{{route("supplier.branch")}}';
                    }, 1000);
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            },
                    No: function() {
                    window.location.reload();
                    }
            }
    });
    });
    function deleteBranch(id) {
    $.confirm({
    title: '<span class="small">'+@json(__('messages.admin.branch_delete_1'))+'</span>',
    content: @json(__('messages.admin.delete_1')),
            buttons: {
            @json(__('messages.admin.yes')): function() {
            $.ajax({
            type: "POST",
                    url: "{{route('branch.delete')}}",
                    data: {
                    id: id
                    },
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                    if (data.status == 1) {
                    window.setTimeout(function() {
                    window.location.href = '{{route("supplier.branch")}}';
                    }, 1000);
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            },
                    @json(__('messages.admin.no')): function() {
                    console.log('cancelled');
                    }
            }
    });
    }

    $('.password_send').on('click', function(e) {
    e.preventDefault();
    var id = $(this).data("id");
    $.confirm({
    title: @json(__('messages.admin.confirmation')),
            content: @json(__('messages.admin.send_pwd_msg')),
            buttons: {
            Yes: function() {


            $.ajax({
            url: "{{route('branch.setPassword')}}",
                    type: 'POST',
                    data: {
                    id: id
                    },
                    success: function(data) {

                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    window.setTimeout(function() {
                    window.location.href = '{{route("supplier.branch")}}';
                    }, 1000);
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            },
                    No: function() {
                    window.location.reload();
                    }
            }
    });
    });
    function getRegion(sel)
            {
            var city_id = sel.value;
            $.ajax({
            type: "GET",
                    url: "{{route('branch.get_region')}}",
                    data: {'city_id': city_id},
                    success: function (data) {
                    var options = "";
                    options += '<option value=""> Select Region </option>';
                    for (var i = 0; i < data.length; i++) {
                    options += '<option value=' + data[i].id + '>' + data[i].lang[0].name + "</option>";
                    }
                    $("#region").html(options);
                    }
            });
            }
</script>
@endpush