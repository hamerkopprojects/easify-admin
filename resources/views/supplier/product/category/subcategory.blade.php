<div style="padding-left:20px;">
    @foreach($subcategories as $subcategorylist)
    <div role="tab" id="headingTwo{{$subcategorylist->id}}" style="background: #94d9be;" class="mb-1">
        <div class="row">
            <div class="col-md-4 mt-2">
                <input type="checkbox" id="category" name="category[]"
                       class="@error('category') is-invalid @enderror"
                       @if($selected_category->contains('category_id', $subcategorylist->id)) checked @endif
                       value="{{$subcategorylist->id}}">
                       <label> {{$subcategorylist->category_unique_id}}</label>
            </div>
            <div class="col-md-3 mt-2">
                <label>{{$subcategorylist->lang[0]->name}}</label>
            </div>
            <div class="col-md-3 mt-2">
                <label>{{$subcategorylist->lang[1]->name}}</label>
            </div>
            <div class="col-md-2 mt-2">
                <a class="collapsed" data-toggle="collapse"
                   href="#collapseTwo{{$subcategorylist->id}}" aria-expanded="false"
                   aria-controls="collapseTwo{{$subcategorylist->id}}"
                   style="float: right">
                    @if(count($subcategorylist->subcategory))
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                    @endif
                </a>
            </div>
        </div>
    </div>
    @if(count($subcategorylist->subcategory))
    <div id="collapseTwo{{$subcategorylist->id}}" class="collapse show" role="tabpanel"
         aria-labelledby="headingTwo{{$subcategorylist->id}}">
        <div class="card-body" style="padding: 2px 0px;">
            @include('supplier.product.category.subsubcategory',['subsubcategories' =>
            $subcategorylist->subcategory])

        </div>
    </div>
    @endif
    @endforeach
</div>