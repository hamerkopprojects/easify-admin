@extends('layouts.master')
@section('content')
@include('supplier.msg.flash_message')

<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="col-lg-12 p-r-0 title-margin-right">
                <div class="page-header">
                    <div class="page-title">
                        <h1>{{ __('messages.admin.preferred_category') }}</h1>
                    </div>
                </div>
            </div>
            <form method="POST" action="{{ route('supplier.category.store') }}">
                @csrf
                <div class="row">
                    <div class="col-md-10">
                        <div class="card">
                            <b><input type="checkbox" id="checkAll">
                         <label> {{ __('messages.admin.check_all') }}</label></b>
                            <div class="accordion md-accordion" id="accordionEx1" role="tablist"
                                aria-multiselectable="true">
                                @foreach($main_cat as $categorylist)
                                <div class="card">
                                    <div role="tab" id="headingTwo{{$categorylist->id}}" style="background: #5de4af;">
                                        <div class="row">
                                            <div class="col-md-4 mt-2">
                                                <input type="checkbox" id="category" name="category[]"
                                                    class="@error('category') is-invalid @enderror"
                                                    @if($selected_category->contains('category_id', $categorylist->id)) checked @endif
                                                    value="{{$categorylist->id}}">
                                                <label> {{$categorylist->category_unique_id}}</label>
                                            </div>
                                            <div class="col-md-3 mt-2">
                                                <label> {{$categorylist->lang[0]->name}}</label>
                                            </div>
                                            <div class="col-md-3 mt-2">
                                                <label> {{$categorylist->lang[1]->name}}</label>
                                            </div>
                                            <div class="col-md-2 mt-2">
                                                <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1"
                                                    href="#collapseTwo{{$categorylist->id}}" aria-expanded="false"
                                                    aria-controls="collapseTwo{{$categorylist->id}}"
                                                    style="float: right">
                                                    @if(count($categorylist->subcategory))
                                                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                                                    @endif
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    @if(count($categorylist->subcategory))
                                    <div id="collapseTwo{{$categorylist->id}}" class="collapse show" role="tabpanel"
                                        aria-labelledby="headingTwo{{$categorylist->id}}" data-parent="#accordionEx1">
                                        <div class="card-body" style="padding: 2px 0px;">
                                            @include('supplier.product.category.subcategory',['subcategories' =>
                                            $categorylist->subcategory])

                                        </div>
                                    </div>
                                    @endif
                                </div>
                                <!-- Accordion card -->
                                @endforeach
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-info waves-effect waves-light" data-dismiss="modal">{{ __('messages.admin.save') }}</button>
                                <a href="#" type="button" class="btn btn-default waves-effect"> {{ __('messages.admin.cancel') }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
     $("input[type='checkbox']").change(function () {
         console.log($(this).children(':checkbox').attr('checked', this.checked));
         
  });
  
</script>
    
@endpush