<div style="padding-left:50px;">
    @foreach($subsubcategories as $subcategory)
    <div style="background: #cdf5e5;" role="tab" id="headingTwo{{$subcategory->id}}" class="mb-1">
        <div class="row">
            <div class="col-md-4 mt-2">
                <input type="checkbox" id="category" name="category[]"
                       class="@error('category') is-invalid @enderror"
                       @if($selected_category->contains('category_id', $subcategory->id)) checked @endif
                       value="{{$subcategory->id}}">
                       <label> {{$subcategory->category_unique_id}}</label>
            </div>
            <div class="col-md-3 mt-2">
                <label> {{$subcategory->lang[0]->name}}</label>
            </div>
            <div class="col-md-3 mt-2">
                <label> {{$subcategory->lang[1]->name}}</label>
            </div>
        </div>
    </div>
    @endforeach
</div>