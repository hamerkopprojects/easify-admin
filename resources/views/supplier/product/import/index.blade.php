@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>{{ __('messages.admin.import_simple_product') }} - {{ __('messages.admin.step1') }}</h1>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
                <div class="col-lg-3">
                    <div class="page-header">
                        <div class="page-title">
                            <div class="col-md-12 text-right">
                                <a href="{{ route('supplier.product_list') }}">
                                    <font style="vertical-align: inherit;">{{ __('messages.admin.back') }}
                                    </font>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
            </div>
            <!-- /# row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form id="frm-excel-import" action="javascript:;" method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p><strong>{{ __('messages.admin.import_product_list') }}</strong></p>
                                        <p class="small"><b>{{ __('messages.admin.insructions') }} </b><br/>
                                            1. {{ __('messages.admin.supported_file_formats') }}: <b>xlsx</b><br>
                                            2. {{ __('messages.admin.max_file_size') }}: <b>2 MB</b><br>
                                            3. {{ __('messages.admin.follow_the_excel_fields') }}
                                        </p>
                                        <hr>
                                        <a href="{{ asset('sample/products/Easify-Supplier-Product-Sample.xlsx') }}" class="cl-btn-dwnld float-right" style="color: blue;"><i class="fa fa-download"></i> {{ __('messages.admin.download_sample') }}</a>
                                        <div class="form-group upload_btn">
                                            <div class="tower-file">
                                                <input type="file" name="select_file" id="select_file" style="display:none">
                                                <label for="select_file" class="btn btn-primary" data-default-text="Upload Photo">
                                                    <span class="mdi mdi-upload"></span>{{ __('messages.admin.choose_the_excel') }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 text-left">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-info" id="btn-import-submit">{{ __('messages.admin.import') }}</button>
                                        <button type="button" class="btn btn-default" onclick="location.href ='{{route('supplier.product_list')}}'">{{ __('messages.admin.cancel') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $("#frm-excel-import").submit(function () {
        $('.loading_box').show();
        $('.loading_box_overlay').show();
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $("#btn-import-submit").on('click', function () {
        $('.loading_box').show();
        $('.loading_box_overlay').show();
        var form_data = new FormData($('form')[0]);
        $.ajax({
            type: "POST",
            url: "{{route('import_submit')}}",
            data: form_data,
            dataType: "json",
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function () {
                $('#btn-import-submit').button('loading');
            },
            complete: function () {
                $('#btn-import-submit').button('reset');
            },
            success: function (data) {
                if (data.status == 1) {
                    window.location.href = '{{route("product_excel_view")}}';
                } else {
                    $('.loading_box').hide();
                    $('.loading_box_overlay').hide();
                    Toast.fire({
                        icon: 'error',
                        title: data.message
                    });
                }
            },
            error: function () {}
        });
        return false;
    });
</script>
@endpush