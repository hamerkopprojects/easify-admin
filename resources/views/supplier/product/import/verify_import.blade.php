@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>{{ __('messages.admin.import_simple_product') }} - {{ __('messages.admin.step2') }}</h1>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /# row -->
            <div class="row">
                <div class="col-lg-12">
                    <p><strong>{{ __('messages.admin.toatal_records_to_import') }}: {{ $total_count }}</strong></p>
                    <p>{{ __('messages.admin.ready_to_import') }}: {{ $success_count }} </p>
                    <p>{{ __('messages.admin.duplicate') }} - {{ __('messages.admin.ready_to_update') }}: {{$duplicate_count}}</p>
                    <p>{{ __('messages.admin.errors') }}: {{$error_count}}</p>
                    <div class="card">
                        <div class="card-body">
                            <form class="frm-search" id="posts-filter" method="get" action="">
                                <div class="row tablenav top text-right">
                                    <div class="col-md-8 ml-0">
                                        <input class="form-control search_txt" type="text" name="search" value="{{ $search }}" placeholder="{{ __('messages.admin.search_by_sku_or_product_name') }}">
                                    </div>
                                    <div class="col-md-3 text-left">
                                        <button type="submit" class="btn btn-info"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{ __('messages.admin.search') }}</font></font></button>
                                        <a href="{{route('product_excel_view')}}" class="btn btn-default btn-reset">{{ __('messages.admin.reset') }}</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-12 text-right">
                        <a href="{{route('import_products')}}?action=76GH34RT65" class="btn btn-default ml-2"><font style="vertical-align: inherit;">{{ __('messages.admin.fix_the_excel') }}</font></a>
                        <a href="javascript:void(0);" class="btn btn-info ml-2 cls_import_confirm"><font style="vertical-align: inherit;">{{ __('messages.admin.continue') }}</font></a>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>{{ __('messages.admin.sno') }}</th>
                                            <th>{{ __('messages.admin.sku') }}</th>
                                            <th>{{ __('messages.admin.product_name') }} (EN)</th>
                                            <th class="text-right">{{ __('messages.admin.product_name') }} (AR)</th>
                                            <th>{{ __('messages.admin.category') }}</th>
                                            <th>{{ __('messages.admin.price1') }}</th>
                                            <th></th>
                                            <th class="cls_last_child"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($result_data) > 0)
                                        @php
                                        $i = 1;
                                        @endphp
                                        @foreach ($result_data as $row_data)
                                        <tr>
                                            <th>{{$i++}}</th>
                                            <td>{{$row_data->sku}}</td>
                                            <td>{{$row_data->product_name_en}}</td>
                                            <td class="text-right">{{$row_data->product_name_ar}}</td>
                                            <td>{{$row_data->category}}</td>
                                            <td>{{$row_data->price}}</td>
                                            <td class="text-center"><a class="btn btn-sm btn-success text-white view_btn" title="View Product Details" data-id="{{ $row_data->id }}"><i class="fa fa-eye"></i></a></td>
                                            @if ($row_data->error_flag=="N" && $row_data->duplicate_flag == "N")
                                            <td class="cls_last_child" style="background:green;color:white;">{{ __('messages.admin.ready_to_import') }}</td>
                                            @elseif($row_data->error_flag=="Y")
                                            <td class="cls_last_child" style="background:red;color:white;">{{ __('messages.admin.errors') }}
                                                <a href="#" class="btn btn-sm btn-success text-white error-data float-right" title="View Log" data-id="{{$row_data->id}}"><i class="fa fa-eye"></i></a></td>
                                            @elseif($row_data->duplicate_flag=="Y")
                                            <td class="cls_last_child" style="background:orange;color:white;">{{ __('messages.admin.duplicate') }} - {{ __('messages.admin.ready_to_update') }}</td>
                                            @endif
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="12" class="text-center">{{ __('messages.admin.no_records_found') }}</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center d-flex justify-content-center mt-3">
                                {{ $temp_data->appends(request()->query())->links() }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 text-right">
                        <a href="{{route('import_products')}}?action=76GH34RT65" class="btn btn-default ml-2"><font style="vertical-align: inherit;">{{ __('messages.admin.fix_the_excel') }}</font></a>
                        <a href="javascript:void(0);" class="btn btn-info ml-2 cls_import_confirm"><font style="vertical-align: inherit;">{{ __('messages.admin.continue') }}</font></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal pooup -->
<div class="modal fade none-border" id="formModal">
    <div class="modal-dialog modal-lg" style="max-height:85%;  margin-top: 50px; margin-bottom:50px;max-width: 100%">
        <div class="modal-content">

        </div>
    </div>
</div>
<!-- END MODAL -->
@endsection
@push('scripts')
<script>
    $(window).load(function () {
        $('.loading_box').hide();
        $('.loading_box_overlay').hide();
    });
    $('.error-data').on('click', function () {
        var id = $(this).data("id");
        $.dialog({
            title: 'Error: Data with Errors',
            content: "url:{{route('import_errors')}}?id=" + id,
            //                            animation: 'scale',
            columnClass: 'large',
            //                            closeAnimation: 'scale',
            backgroundDismiss: true,
        });
    });

    $('.view_btn').on('click', function () {
        var temp_id = $(this).data("id");
        $.ajax({
            type: "GET",
            url: "{{route('import_view')}}",
            data: {'id': temp_id},
            success: function (data) {
                $('.modal-content').html(data);
                $('#formModal').modal('show');
            }
        });
    });
    $('.cls_import_confirm').on('click', function () {
        $('.loading_box').show();
        $('.loading_box_overlay').show();
        $.ajax({
            type: "GET",
            url: "{{route('confrim_import')}}",
            success: function (data) {
                if (data.status == 1) {
                    $('.loading_box').hide();
                    $('.loading_box_overlay').hide();
                    Toast.fire({
                        icon: 'success',
                        title: data.message
                    });
                    window.setTimeout(function () {
                        window.location.href = '{{route("imported_data")}}';
                    }, 1000);
                }
            }
        });
    });

</script>
@endpush