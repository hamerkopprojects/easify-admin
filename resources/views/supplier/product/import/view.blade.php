<div class="modal-header">
    <h4 class="modal-title"><strong>{{ __('messages.admin.product_details') }}</strong></h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="modal-body">
    <div class="msg_div"></div>
    <div class="row">
        <div class="col-md-12">
            <label class="control-label"><b>{{ __('messages.admin.sku') }}</b></label>
            <p>{{ $row_data['sku'] }}</p>
        </div>
        <div class="col-md-6">
            <label class="control-label"><b>{{ __('messages.admin.product_name') }} (EN)</b></label>
            <p>{{ $row_data['product_name_en'] }}</p>
        </div>
        <div class="col-md-6 text-right">
            <label class="control-label"><b>{{ __('messages.admin.product_name') }} (AR)</b></label>
            <p>{{ $row_data['product_name_ar'] }}</p>
        </div>
        <div class="col-md-6">
            <label class="control-label"><b>{{ __('messages.admin.main_category') }}</b></label>
            <p>{{ $row_data['main_category'] }}</p>
        </div>
        <div class="col-md-6">
            <label class="control-label"><b>{{ __('messages.admin.level_2_category') }}</b></label>
            <p>{{ $row_data['level2_category'] }}</p>
        </div>
        <div class="col-md-6">
            <label class="control-label"><b>{{ __('messages.admin.level_3_category') }}</b></label>
            <p>{{ $row_data['level3_category'] }}</p>
        </div>
        <div class="col-md-6"></div>
        <div class="col-md-6">
            <label class="control-label"><b>{{ __('messages.admin.attribute_mapping_category') }}</b></label>
            <p>{{ $row_data['category'] }}</p>
        </div>
        <div class="col-md-6">
            <label class="control-label"><b>{{ __('messages.admin.brand') }}</b></label>
            <p>{{ $row_data['brand'] }}</p>
        </div>
        <div class="col-md-6">
            <label class="control-label"><b>{{ __('messages.admin.description') }} (EN)</b></label>
            <p>{{ $row_data['description_en'] }}</p>
        </div>
        <div class="col-md-6 text-right">
            <label class="control-label"><b>{{ __('messages.admin.description') }} (AR)</b></label>
            <p>{{ $row_data['description_ar'] }}</p>
        </div>
        <div class="col-md-6">
            <label class="control-label"><b>{{ __('messages.admin.min_stock') }}</b></label>
            <p>{{ $row_data['min_stock'] }}</p>
        </div>
        <div class="col-md-6">
            <label class="control-label"><b>{{ __('messages.admin.max_stock') }}</b></label>
            <p>{{ $row_data['max_stock'] }}</p>
        </div>
        <div class="col-md-6">
            <label class="control-label"><b>{{ __('messages.admin.price1') }}</b></label>
            <p>{{ $row_data['price'] }}</p>
        </div>
        <div class="col-md-6">
            <label class="control-label"><b>{{ __('messages.admin.discount') }}</b></label>
            <p>{{ $row_data['discount_price'] }}</p>
        </div>
    </div>
</div>
