@extends('layouts.master')

@section('content')
@include('supplier.msg.flash_message')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="col-lg-12 p-r-0 title-margin-right">
                <div class="page-header">
                    <div class="page-title">
                        <h1>{{ __('messages.admin.preferred_brands') }}</h1>
                    </div>
                </div>
            </div>
            <form method="POST" action="{{ route('supplier.brand.store')}}">
                @csrf
                <div class="row">
                    <div class="col-md-10">
                        <div class="card">
                            <div class="row">
                                <div class="col-md-3">
                                    <b><input type="checkbox" id="checkAll"> <label> {{ __('messages.admin.check_all') }}</label></b>
                                </div></div>
                            <!--<div class="card">-->
                            @php
                            // dd($brand)
                            $i=1;
                            @endphp
                            @foreach($brands as $brand)
                            <div class="row" id="headingTwo{{$brand->id}}" style="background: #94d9be;">
                                <div class="col-md-3">
                                    <input type="checkbox" id="category" name="brands[]"
                                           class="@error('brands') is-invalid @enderror"
                                           @if($selected_brand->contains('brand_id', $brand->id)) checked @endif
                                           value="{{$brand->id}}">
                                           <label> {{$brand->brand_unique_id}}</label>
                                </div>
                                <div class="col-md-3">
                                    <label> {{$brand->lang[0]->name}}</label>
                                </div>
                                <div class="col-md-3">
                                    <label> {{$brand->lang[1]->name}}</label>
                                </div>

                            </div>
                            @endforeach
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-info waves-effect waves-light" data-dismiss="modal">{{ __('messages.admin.save') }}</button>
                                <a href="{{route('supplier.brand_list')}}" type="button" class="btn btn-default waves-effect"> {{ __('messages.admin.cancel') }}</a>
                            </div>
                            <!--</div>-->
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection