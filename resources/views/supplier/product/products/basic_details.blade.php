<form id="frm_create_product" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <label class="control-label">{{ __('messages.admin.sku') }} <span class="text-danger">*</span></label>
                    <input class="form-control form-white" placeholder="Enter SKU" type="text" name="sku" value="{{ isset($row_data['id']) ? $row_data['sku'] : $sku}}"/>
                </div>
                <div class="col-md-6">
                </div>
                <div class="col-md-6">
                    <label class="control-label">{{ __('messages.admin.product_name') }} (EN) <span class="text-danger">*</span></label>
                    <input class="form-control form-white" placeholder="Enter product name (EN)" type="text" name="name_en" value="{{ isset($row_data['id']) ? $row_data['lang'][0]['name'] : ''}}"/>
                </div>
                <div class="col-md-6 text-right">
                    <label class="control-label">{{ __('messages.admin.product_name') }} (AR) <span class="text-danger">*</span></label>
                    <input class="form-control form-white text-right" placeholder="Enter product name (AR)" type="text" name="name_ar" value="{{ isset($row_data['id']) ? $row_data['lang'][1]['name'] : ''}}"/>
                </div>
                <div class="col-md-6">
                    <label class="control-label">{{ __('messages.admin.main_category') }} <span class="text-danger">*</span></label><br/>
                    <select class="form-control" name="main_category" id="main_category" onchange="getSubcategory(this);">
                        <option value="">{{ __('messages.admin.select_category') }} </option>
                        @foreach($cat_data as $row_val)
                        <option value="{{ $row_val->id }}" {{ (isset($row_data['id']) &&  $row_val->id == $row_data['main_category']) ? 'selected' : '' }}>{{ $row_val->lang[0]->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6">
                    <label class="control-label">{{ __('messages.admin.level_2_category') }} <span class="text-danger">*</span></label><br/>
                    <select class="form-control" name="sub_category" id="sub_category">
                        <option value="">{{ __('messages.admin.select_category') }} </option>
                        @if(isset($row_data['id']))
                        @foreach($sub_cat as $row_val)
                        <option value="{{ $row_val->id }}" {{ (isset($row_data['id']) &&  $row_val->id == $row_data['sub_category']) ? 'selected' : '' }}>{{ $row_val->lang[0]->name }}</option>
                        @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-md-6">
                    <label class="control-label">{{ __('messages.admin.level_3_category') }} </label><br/>
                    <select class="select2_multiple" name="sub_sub_category[]" multiple="multiple" id="sub_sub_category">
                        @foreach($multiple_cat as $single_val)
                        <option value="{{ $single_val->id }}" {{ !empty($cat_ids) && in_array($single_val->id, $cat_ids) ? 'selected' : '' }}>{{ $single_val->lang[0]->name }}</option>
                        @endforeach
                    </select>
                    <label class="control-label" id="error_chk"></label>
                </div>
                <div class="col-md-6">
                    <label class="control-label">{{ __('messages.admin.attribute_mapping_category') }} <span class="text-danger">*</span></label>
                    <select class="form-control" name="category">
                        <option value=""> {{ __('messages.admin.select_category') }} </option>
                        @foreach($cat_data as $cat_data_val)
                        <option value="{{ $cat_data_val->id }}" {{ (isset($row_data['id']) &&  $cat_data_val->id == $row_data['category_id']) ? 'selected' : '' }}>{{ $cat_data_val->lang[0]->name }}</option>
                        @if(count($cat_data_val->subcategory))
                        @foreach($cat_data_val->subcategory as $sub_cat_val)
                        <option value="{{ $sub_cat_val->id }}" {{ (isset($row_data['id']) &&  $sub_cat_val->id == $row_data['category_id']) ? 'selected' : '' }}>&nbsp;&nbsp;{{ $sub_cat_val->lang[0]->name }}</option>
                        @php
                        $sub_sub_cat = \App\Models\Category::with('lang')->where(['parent_id' => $sub_cat_val->id, 'status' => 'active'])->get();
                        @endphp
                        @if(count($sub_sub_cat))
                        @foreach($sub_sub_cat as $sub_sub_cat_val)
                        <option value="{{ $sub_sub_cat_val->id }}" {{ (isset($row_data['id']) &&  $sub_sub_cat_val->id == $row_data['category_id']) ? 'selected' : '' }}>&nbsp;&nbsp;&nbsp;&nbsp;{{ $sub_sub_cat_val->lang[0]->name }}</option>
                        @endforeach
                        @endif
                        @endforeach
                        @endif
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6">
                    <label class="control-label">{{ __('messages.admin.brand') }} <span class="text-danger">*</span></label>
                    <select class="form-control" name="brand">
                        <option value=""> {{ __('messages.admin.brand') }} </option>
                        @foreach($brand_data as $brand_data_val)
                        <option value="{{ $brand_data_val->brand_id }}" {{ (isset($row_data['category_id']) &&  $brand_data_val->brand_id == $row_data['brand_id']) ? 'selected' : '' }}>{{ $brand_data_val->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6">
                    
                </div>
                <div class="col-md-6 mt-3">
                    <label class="control-label">{{ __('messages.admin.product_type') }}</label><br/>
                    <label class="radio-inline">
                        <input type="radio" name="product_type" value="simple" {{ isset($row_data['id']) && $row_data['product_type'] == 'simple' ? 'checked="checked"' : 'checked=""'}}> {{ __('messages.admin.simple') }}
                    </label>
                    @if($is_variant)
                    <label class="radio-inline ml-3">
                        <input type="radio" name="product_type" value="complex" {{ isset($row_data['id']) && $row_data['product_type'] == 'complex' ? 'checked="checked"' : ''}}> {{ __('messages.admin.complex') }}
                    </label>
                    @endif
                    <p id="pdt_simple" style="{{ (isset($row_data['id']) && $row_data['product_type'] == 'complex') ? 'display:none' : 'display:block' }}"><small><i class="fa fa-info-circle"></i> Lorem ipsum dolor sit amet</small><br/>
                    <small><i class="fa fa-info-circle"></i> Lorem ipsum dolor sit amet, consectetur adipiscing elit.</small></p>
                    <p id="pdt_complex" style="{{ (isset($row_data['id']) && $row_data['product_type'] == 'complex') ? 'display:block' : 'display:none' }}"><small><i class="fa fa-info-circle"></i> Lorem ipsum dolor</small><br/>
                    <small><i class="fa fa-info-circle"></i> Lorem ipsum dolor sit amet, consectetur adipiscing elit.</small></p>
                </div>
                <div class="col-md-6"></div>
                <div class="col-md-6">
                    <label class="control-label">{{ __('messages.admin.description') }} (EN) <span class="text-danger">*</span></label>
                    <textarea class="form-control area" name="description_en" placeholder="Enter description (EN)">{{ isset($row_data['id']) ? $row_data['lang'][0]['description'] : ''}}</textarea>
                </div>
                <div class="col-md-6 text-right">
                    <label class="control-label">{{ __('messages.admin.description') }} (AR) <span class="text-danger">*</span></label>
                    <textarea class="form-control area text-right" name="description_ar" placeholder="Enter description (AR)">{{ isset($row_data['id']) ? $row_data['lang'][1]['description'] : ''}}</textarea>
                </div>
                {{-- <div class="col-md-6">
                    <label class="control-label">Ingredients & Nutrition Facts (EN)</label>
                    <textarea class="form-control area" name="ingredients_en" placeholder="Enter Ingredients & Nutrition facts (EN)">{{ isset($row_data['id']) ? $row_data['lang'][0]['ingredients'] : ''}}</textarea>
                </div>
                <div class="col-md-6 text-right">
                    <label class="control-label">Ingredients & Nutrition Facts (AR)</label>
                    <textarea class="form-control area text-right" name="ingredients_ar" placeholder="Enter Ingredients & Nutrition facts (AR)">{{ isset($row_data['id']) ? $row_data['lang'][1]['ingredients'] : ''}}</textarea>
                </div>
                <div class="col-md-6">
                    <label class="control-label">How To Use (EN)</label>
                    <textarea class="form-control area" name="how_to_use_en" placeholder="Enter how to use (EN)">{{ isset($row_data['id']) ? $row_data['lang'][0]['how_to_use'] : ''}}</textarea>
                </div>
                <div class="col-md-6 text-right">
                    <label class="control-label">How To Use (AR)</label>
                    <textarea class="form-control area text-right" name="how_to_use_ar" placeholder="Enter how to use (AR)">{{ isset($row_data['id']) ? $row_data['lang'][1]['how_to_use'] : ''}}</textarea>
                </div>
                <div class="col-md-6">
                    <label class="control-label">Reasons To Buy (EN)</label>
                    <textarea class="form-control area" name="reasons_to_buy_en" placeholder="Enter reasons to buy (EN)">{{ isset($row_data['id']) ? $row_data['lang'][0]['reasons_to_buy'] : ''}}</textarea>
                </div>
                <div class="col-md-6 text-right">
                    <label class="control-label">Reasons To Buy (AR)</label>
                    <textarea class="form-control area text-right" name="reasons_to_buy_ar" placeholder="Enter reasons to buy (AR)">{{ isset($row_data['id']) ? $row_data['lang'][1]['reasons_to_buy'] : ''}}</textarea>
                </div> --}}
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" id="pdt_id" name="pdt_id" value="{{ isset($row_data['id']) ? $row_data['id'] : '' }}">
            @if(isset($row_data['id']))
            <button type="submit" class="btn btn-info waves-effect waves-light save-btn">
                {{ __('messages.admin.save') }}
            </button>
            @endif
            <button type="submit" class="btn btn-info waves-effect waves-light save-and-continue">
                {{ __('messages.admin.save_continue') }}
            </button>

            <input type="hidden" id="submit_action" value="" />
            <a class="btn btn-default waves-effect" href="{{ route('supplier.product_list') }}">{{ __('messages.admin.back') }}</a>
        </div>
    </div>
</form>
<script>
    $('input[type=radio][name=product_type]').change(function() {
    if (this.value == 'simple') {
        $("#pdt_simple").css({'display': 'block'});
        $("#pdt_complex").css({'display': 'none'});
    }
    else if (this.value == 'complex') {
        $("#pdt_complex").css({'display': 'block'});
        $("#pdt_simple").css({'display': 'none'});
    }
});
    $(".select2_multiple").select2({
        placeholder: "Select Multiple Category",
        allowClear: true
    });
//    $(".select2-selection--multiple").addClass("form-control");
    $(".select2-selection--multiple").css({"border": "1px solid #ccc", "border-radius": "0", "padding": "3px 0px 9px 0px", "height": "auto", "width": "432"});
    $(".save-and-continue").on('click', function (e) {
        $("#submit_action").val('continue');
    });
    $(".save-btn").on('click', function (e) {
        $("#submit_action").val('save');
    });

    $("#frm_create_product").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        rules: {
            sku: {
                required: true,
            },
            name_en: {
                required: true,
            },
            name_ar: {
                required: true,
            },
            main_category: {
                required: true,
            },
            sub_category: {
                required: true,
            },
            category: {
                required: true,
            },
            brand: {
                required: true,
            },
            supplier: {
                required: true,
            },
            description_en: {
                required: true,
            },
            description_ar: {
                required: true,
            }
        },
        messages: {
            sku: {
                required: @json(__('messages.admin.product_validation_1')),
            },
            name_en: {
                required: @json(__('messages.admin.product_validation_2')),
            },
            name_ar: {
                required: @json(__('messages.admin.product_validation_3')),
            },
            main_category: {
                required: @json(__('messages.admin.product_validation_4')),
            },
            sub_category: {
                required: @json(__('messages.admin.product_validation_5')),
            },
            category: {
                required: @json(__('messages.admin.product_validation_7')),
            },
            brand: {
                required: @json(__('messages.admin.product_validation_8')),
            },
            supplier: {
                required: @json(__('messages.admin.product_validation_9')),
            },
            description_en: {
                required: @json(__('messages.admin.product_validation_10')),
            },
            description_ar: {
                required: @json(__('messages.admin.product_validation_11')),
            },
        },
        errorElement: "label",
        errorPlacement: function (error, element) {
            if (element.attr("id") == "sub_sub_category") {
                error.insertAfter("#error_chk");
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "{{route('supplier.save_product_info')}}",
                data: $('#frm_create_product').serialize(),
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data.status == 1) {
                        if ($("#submit_action").val() == 'continue') {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#pdt_tab a[href="#attributes"]').tab('show');
                            $('.tab-content').html(data.result);
                        } else {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                        }
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                        $("#pdt_tab").tabs({cache: true});
                    }
                    $('button:submit').attr('disabled', false);
                },
                error: function (err) {
                    $('button:submit').attr('disabled', false);
                }
            });
            return false;
        }
    });
</script>