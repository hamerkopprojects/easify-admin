<div class="modal-header">
    <h4 class="modal-title"><strong>
            {{( isset($row_data['id']) && $row_data['id'] != '' ) ? __('messages.admin.edit') : __('messages.admin.add') }}
            {{ __('messages.admin.product') }}
        </strong></h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="col-sm-12">
    <ul class="nav nav-tabs" role="tablist" id="pdt_tab">
        <li class="nav-item"> <a class="nav-link active" href="#pdt_info" data-id="{{ isset($row_data['id']) ? $row_data['id']  : '' }}" role="tab"><span class="hidden-sm-up"><i class="ti-shopping-cart"></i></span> <span class="hidden-xs-down">{{ __('messages.admin.product_info') }}</span></a> </li>
        <li class="nav-item disabled"> <a class="nav-link" href="#attributes" data-id="{{ isset($row_data['id']) ? $row_data['id']  : '' }}" role="tab"><span class="hidden-sm-up"><i class="ti-shopping-cart"></i></span> <span class="hidden-xs-down">{{ __('messages.admin.attributes') }}</span></a> </li>
        <li class="nav-item disabled"> <a class="nav-link" href="#stock" data-id="{{ isset($row_data['id']) ? $row_data['id']  : '' }}" role="tab"><span class="hidden-sm-up"><i class="ti-folder"></i></span> <span class="hidden-xs-down">{{ __('messages.admin.stock') }}</span></a> </li>
        <li class="nav-item disabled"> <a class="nav-link" href="#price" data-id="{{ isset($row_data['id']) ? $row_data['id']  : '' }}" role="tab"><span class="hidden-sm-up"><i class="ti-folder"></i></span> <span class="hidden-xs-down">{{ __('messages.admin.price') }}</span></a> </li>
        <li class="nav-item disabled"> <a class="nav-link" href="#photos" role="tab" data-id="{{ isset($row_data['id']) ? $row_data['id']  : '' }}"><span class="hidden-sm-up"><i class="ti-image"></i></span> <span class="hidden-xs-down">{{ __('messages.admin.photos') }}</span></a> </li>
    </ul>
</div>
<div class="tab-content">
    @include('supplier.product.products.basic_details')
</div>

<script>
    $("button[data-dismiss=modal]").click(function () {
        location.reload();
    });
    $('.nav-tabs a').on('click', function (e) {
        var x = $(this).attr("href");
        var id = $(this).data("id");
//        var x = $(e.target).text();
        var location = $(this).attr('href');
        if (id != '') {
            $.ajax({
                type: "GET",
                url: "{{route('supplier.product_tabs')}}",
                data: {'activeTab': x, pdt_id: id},
                success: function (result) {
                $('#pdt_tab a[href="'+location+'"]').tab('show');
                    $('.tab-content').html(result);
                }
            });
        }
    });
//
//    function display_photo_tab()
//    {
//        $('#photos_loader').hide();
//        $.ajax({
//            type: "GET",
//            url: "{{route('product_tabs')}}",
//            data: {'activeTab': 'PHOTOS'},
//            success: function (result) {
//                $('.tab-content').html(result);
//            }
//        });
//    }

</script>