<form id="frm_create_attributes" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body">
            <div class="row">
                @if(!empty($pdt_att_var) && $product_type == 'complex')
                <div class="col-md-12 mt-3">
                    <label class="control-label"><strong>PRODUCT VARIANTS</strong></label>
                </div>
                <div class="col-md-6"><label class="control-label"><b>Variant - {{ $pdt_att_var->lang[0]['name'] ?? '' }}</b></label></div>
                <div class="col-md-6"></div>
                <input type="hidden"  name="var_attribute_id" value="{{ $pdt_att_var['id'] }}"/>
                <input type="hidden" name="var_attribute_value_type" value="dropdown"/>
                @if($variants = \App\Models\Variant::leftJoin('variant_i18n', 'variant_i18n.variant_id', '=', 'variant.id')
                ->select('variant_i18n.name as var_name', 'variant_i18n.id as id', 'variant_i18n.variant_id as var_id')
                ->where('variant_i18n.language', '=', 'en')
                ->where('variant.attribute_id', '=', $pdt_att_var['id'])
                ->get(['id', 'var_name', 'var_id']))
                @php $i=0; @endphp
                @foreach($variants as $variant_val)
                @php $i++; @endphp
                @php $pdt_var_data = \App\Models\ProductVariant::with('attribute')->with('variant')
                ->where('variant_lang_id', $variant_val['id'])
                ->where('attribute_id', $pdt_att_var['id'])
                ->where('product_id', $pdt_id)->first();
                @endphp
                <input type="hidden" name="variant_id" value="{{ $variant_val['var_id'] }}"/>
                <div class="col-md-3 mt-2">
                    <input type="checkbox" class="chk-input" name="variant_lang_id[]" value="{{$variant_val['id']}}" {{ isset($pdt_var_data->variant_lang_id) ? "checked=checked" : ''}}>
                           <label class="control-label ml-1" id="error_chk">{{ $variant_val->var_name ?? '' }}</label>
                </div>
                @endforeach
                @endif
                @endif
            </div>
            <div class="row">
                @if(isset($pdt_att) && count($pdt_att) > 0)
                <div class="col-md-12">
                    <label class="control-label"><strong>{{ __('messages.admin.attributes') }}</strong></label>
                </div>
                @foreach($pdt_att as $key => $pdt_att_val)
                @php
                $pdt_attribute = \App\Models\ProductAttribute::with('lang')->with('attribute')->with('product')
                ->where('attribute_id', $pdt_att_val->attribute_id)
                ->where('product_id', $pdt_id)->first();
                @endphp
                @if($pdt_att_val->cat_attributes && $pdt_att_val->cat_attributes->attribute_type == 'textbox')
                <input type="hidden" name="attribute_id[]" value="{{$pdt_att_val->attribute_id}}"/>
                <input type="hidden" name="attribute_type[]" value="textbox"/>
                <div class="col-md-6">
                    <label class="control-label">{{$pdt_att_val->cat_attributes->lang[0]->name}}<span class="text-danger">{{ ($pdt_att_val->cat_attributes->is_mandatory == 'yes') ? '*' : ''}}</span></label>
                    <input class="form-control form-white" placeholder="Enter {{$pdt_att_val->cat_attributes->lang[0]->name}}" type="text" name="att_name_{{$pdt_att_val->attribute_id}}_en" id="{{ ($pdt_att_val->cat_attributes->is_mandatory == 'yes') ? 'att_name_en_'.$pdt_att_val->attribute_id : ''}}" value="{{ isset($pdt_attribute) ? $pdt_attribute->lang[0]['name'] : '' }}"/>
                </div>
                <div class="col-md-6 text-right">
                    <label class="control-label">{{ $pdt_att_val->cat_attributes->lang[1]->name }}<span class="text-danger">{{ ($pdt_att_val->cat_attributes->is_mandatory == 'yes') ? '*' : ''}}</span></label>
                    <input class="form-control form-white text-right" placeholder="Enter {{$pdt_att_val->cat_attributes->lang[1]->name}}" type="text" name="att_name_{{$pdt_att_val->attribute_id}}_ar" id="{{ ($pdt_att_val->cat_attributes->is_mandatory == 'yes') ? 'att_name_ar_'.$pdt_att_val->attribute_id : ''}}" value="{{ isset($pdt_attribute) ? $pdt_attribute->lang[1]['name'] : '' }}"/>
                </div>
                @elseif($pdt_att_val->cat_attributes && $pdt_att_val->cat_attributes->attribute_type == 'textarea')
                <input type="hidden" name="attribute_id[]" value="{{$pdt_att_val->attribute_id}}"/>
                <input type="hidden" name="attribute_type[]" value="textarea"/>
                <div class="col-md-6">
                    <label class="control-label">{{ $pdt_att_val->cat_attributes->lang[0]->name }} <span class="text-danger">{{ ($pdt_att_val->cat_attributes->is_mandatory == 'yes') ? '*' : ''}}</span></label>
                    <textarea class="form-control area" name="att_name_{{$pdt_att_val->attribute_id}}_en" placeholder="Enter {{ $pdt_att_val->cat_attributes->lang[0]->name }}" id="{{ ($pdt_att_val->cat_attributes->is_mandatory == 'yes') ? 'att_name_en_'.$pdt_att_val->attribute_id : ''}}">{{ isset($pdt_attribute) ? $pdt_attribute->lang[0]['name'] : '' }}</textarea>
                </div>
                <div class="col-md-6 text-right">
                    <label class="control-label">{{ $pdt_att_val->cat_attributes->lang[1]->name }} <span class="text-danger">{{ ($pdt_att_val->cat_attributes->is_mandatory == 'yes') ? '*' : ''}}</span></label>
                    <textarea class="form-control area text-right" name="att_name_{{$pdt_att_val->attribute_id}}_ar" placeholder="Enter {{ $pdt_att_val->cat_attributes->lang[1]->name }}" id="{{ ($pdt_att_val->cat_attributes->is_mandatory == 'yes') ? 'att_name_ar_'.$pdt_att_val->attribute_id : ''}}">{{ isset($pdt_attribute) ? $pdt_attribute->lang[1]['name'] : '' }}</textarea>
                </div>
                @endif
                @endforeach
                @foreach($pdt_att as $key => $pdt_att_val)
                @php
                $pdt_var_attribute = \App\Models\ProductAttribute::with('lang')->with('attribute')->with('variant')
                ->where('attribute_type', 'dropdown')
                ->where('attribute_id', $pdt_att_val->attribute_id)
                ->where('product_id', $pdt_id)->first();
                @endphp
                @if($pdt_att_val->cat_attributes && $pdt_att_val->cat_attributes->attribute_type == 'dropdown')
                <input type="hidden" name="attribute_id[]" value="{{$pdt_att_val->attribute_id}}"/>
                <input type="hidden" name="attribute_type[]" value="dropdown"/>
                <div class="col-md-6">
                    <label class="control-label">{{ $pdt_att_val->cat_attributes->lang[0]->name }} <span class="text-danger">{{ ($pdt_att_val->cat_attributes->is_mandatory == 'yes') ? '*' : ''}}</span></label>
                    @if($variants = \App\Models\Variant::leftJoin('variant_i18n', 'variant_i18n.variant_id', '=', 'variant.id')
                    ->select('variant_i18n.name as var_name', 'variant_i18n.id as id', 'variant.id as variant_id')
                    ->where('variant_i18n.language', '=', 'en')
                    ->where('variant.attribute_id', '=', $pdt_att_val->attribute_id)
                    ->get(['id', 'var_name', 'variant_id']))
                    <input type="hidden"  name="variant_id{{$pdt_att_val->attribute_id}}" value="{{$variants[0]->variant_id}}"/>
                    <select class="form-control" name="varinat_lang_{{$pdt_att_val->attribute_id}}_id" id="{{ ($pdt_att_val->cat_attributes->is_mandatory == 'yes') ? 'varinat_lang_'.$pdt_att_val->attribute_id : ''}}">
                        <option value="">Select {{ $pdt_att_val->cat_attributes->lang[0]->name }}</option>
                        @foreach($variants as $variant_val)
                        <option value="{{ $variant_val->id}}" {{ (isset($pdt_var_attribute['id']) &&  $variant_val->id == $pdt_var_attribute['variant_lang_id']) ? 'selected' : '' }}>{{ $variant_val->var_name}}</option>
                        @endforeach
                    </select>
                    @endif
                </div>
                <div class="col-md-6"></div>
                @endif
                @endforeach
                @endif
            </div>
            <div class="modal-footer">
                <input type="hidden" id="pdt_id" name="pdt_id" value="{{ $pdt_id }}">
                <input type="hidden" id="product_type" name="product_type" value="{{ $product_type }}">
                <input type="hidden" id="submit_action" value="" />
                @if(!empty($pdt_id))
                <button type="submit" class="btn btn-info waves-effect waves-light save-btn">
                    {{ __('messages.admin.save') }}
                </button>
                @endif
                <button type="submit" class="btn btn-info waves-effect waves-light save-and-continue">
                   {{ __('messages.admin.save_continue') }}
                </button>
                <a class="btn btn-default waves-effect tab_back" data-id='{{ $pdt_id }}' href="javascript:void(0);">{{ __('messages.admin.back') }}</a>
            </div>
        </div>
    </div>
</form>
<script>
    $(".save-and-continue").on('click', function () {
        $("#submit_action").val('continue');
    });
    $(".save-btn").on('click', function () {
        $("#submit_action").val('save');
    });
    $('.tab_back').on('click', function () {
        var pdt_id = $(this).data("id");
        $.ajax({
            type: "GET",
            url: "{{route('supplier.product_tabs')}}",
            data: {'activeTab': '#pdt_info', pdt_id: pdt_id},
            success: function (result) {
                $('#pdt_tab a[href="#pdt_info"]').tab('show');
                $('.tab-content').html(result);
            }
        });
    });
    $("#frm_create_attributes").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        rules: {
            "variant_lang_id[]": {
                required: true,
            }
        },
        messages: {
            "variant_lang_id[]": {
                required: 'Variant is required',
            }
        },
        errorElement: "label",
        errorPlacement: function (error, element) {
            if (element.attr("type") == "checkbox") {
                error.insertAfter("#error_chk");
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "{{route('supplier.save_product_attributes')}}",
                data: $('#frm_create_attributes').serialize(),
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data.status == 1) {
                        if ($("#submit_action").val() == 'continue') {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#pdt_tab a[href="#stock"]').tab('show');
                            $('.tab-content').html(data.result);
                        } else
                        {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                        }
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                    }
                    $('button:submit').attr('disabled', false);
                },
                error: function (err) {
                    $('button:submit').attr('disabled', false);
                }
            });
            return false;
        }
    });
    $('input[id^="att_name_en_"]').each(function () {
        $(this).rules('add', {
            required: true,
            messages: {
                required: "Attribute value (EN) is required"
            }
        });
    });
    $('input[id^="att_name_ar_"]').each(function () {
        $(this).rules('add', {
            required: true,
            messages: {
                required: "Attribute value (AR) is required"
            }
        });
    });
    $('textarea[id^="att_name_en_"]').each(function () {
        $(this).rules('add', {
            required: true,
            messages: {
                required: "Attribute value (EN) is required"
            }
        });
    });
    $('textarea[id^="att_name_ar_"]').each(function () {
        $(this).rules('add', {
            required: true,
            messages: {
                required: "Attribute value (AR) is required"
            }
        });
    });
    $('select[id^="varinat_lang_"]').each(function () {
        $(this).rules('add', {
            required: true,
            messages: {
                required: "Select this field"
            }
        });
    });
</script>