<div class="tab-pane active" id="pdt_info" role="tabpanel">
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                @if(!empty($cover[0]->cover_image))
                <div class="col-md-12 ">
                    <label class="control-label"><strong>{{ __('messages.admin.cover_image') }}</strong></label>
                </div>
                
                <div class="col-md-12">
                    <img class="imgst" src="{{ !empty($cover[0]->cover_image) ? url('/uploads/'.$cover[0]->cover_image) : ''}}" alt="">
                </div>
                @endif
                <br>
                @if(!empty($prdt[0]->path))
                <div class="col-md-12">
                    <label class="control-label"><strong>{{ __('messages.admin.product_images') }}</strong></label>
                </div>
                <div class="col-md-12">
                
                    @foreach ($prdt as $photo)

                    <img class="imgst" src="{{url('/uploads/'.$photo->path)}}" alt="">
                    @endforeach
                   
                </div>
                @endif
            </div>

        </div>

<style type="text/css">
   

</style>