<div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body">
            <div class="row">
                @if($product_type == 'complex' && !empty($pdt_att_var))
                <div class="col-md-12 mt-3">
                    <label class="control-label"><strong>{{ __('messages.admin.product_variant') }}</strong></label>
                </div>
                <div class="col-md-3"><label class="control-label"><b>{{ $pdt_att_var->lang[0]['name'] ?? '' }} {{ __('messages.admin.variant') }}</b></label></div>
                <div class="col-md-3"><label class="control-label"><b>{{ __('messages.admin.price1') }}</b> </label></div>
                <div class="col-md-3"><label class="control-label"><b>{{ __('messages.admin.discount') }} (%)</b></label></div>
                <div class="col-md-3"><label class="control-label"><b>{{ __('messages.admin.discount_price') }}</b></label></div>
                <input type="hidden"  name="attribute_id" value="{{ $pdt_att_var['id'] }}"/>
                @if($variants = \App\Models\ProductVariant::with('variant')->where(['product_id' => $pdt_id])->get())
                @foreach($variants as $variant_val)
                @if(!empty($variant_val->variant->id))
                @php
                $pdt_price = \App\Models\ProductPrice::with('attribute')->with('variant')->where('product_id', $pdt_id)
                ->where('variant_lang_id', $variant_val->variant->id)->first();
                @endphp
                <div class="col-md-3 mt-2">
                    <input type="hidden"  name="variant_id" value="{{ $variant_val->variant->variant_id }}"/>
                    <input type="hidden" name="variant_lang_id[]" value="{{$variant_val->variant->id }}">
                    <label class="control-label ml-1">{{ $variant_val->variant->name ?? '' }}</label>
                </div>

                <div class="col-md-3">
                    <label class="control-label">{{ isset($pdt_price['price']) ? $pdt_price['price'] : '' }}</label>
                </div>
                <div class="col-md-3">
                    <label class="control-label">{{ isset($pdt_price['discount']) ? $pdt_price['discount'] : '' }}</label>
                </div>
                <div class="col-md-3">
                    <label class="control-label">{{ isset($pdt_price['discount_price']) ? $pdt_price['discount_price'] : '' }}</label>
                </div>
                @endif
                @endforeach
                @endif
                @else
                <div class="col-md-4"><label class="control-label"><b>{{ __('messages.admin.price1') }}</b> </label></div>
                <div class="col-md-4"><label class="control-label"><b>{{ __('messages.admin.discount') }} (%)</b></label></div>
                <div class="col-md-4"><label class="control-label"><b>{{ __('messages.admin.discount_price') }}</b></label></div>
                <div class="col-md-4">
                    <label class="control-label">{{ isset($product_price)? $product_price['price'] : ''}}</label>
                </div>
                <div class="col-md-4">
                    <label class="control-label">{{ isset($product_price)? $product_price['discount'] : ''}}</label>
                </div>
                <div class="col-md-4">
                    <label class="control-label">{{ isset($product_price)? $product_price['discount_price'] : ''}}</label>
                </div>
                @endif
        </div>
        <div class="modal-footer">
            
        </div>
    </div>
</div>
<style>

</style>
    