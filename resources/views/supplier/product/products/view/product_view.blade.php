<div class="modal-header">
    <h4 class="modal-title"><strong>
      
            {{ __('messages.admin.product_details') }}
        </strong></h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="col-sm-12">
    <ul class="nav nav-tabs" role="tablist" id="pdt_tab">
        <li class="nav-item"> <a class="nav-link active" data-id="{{$product[0]['id']}}" href="#pdt_info" role="tab" data-toggle="tab"><span class="hidden-sm-up"><i class="ti-shopping-cart"></i></span> <span class="hidden-xs-down">{{ __('messages.admin.product_info') }}</span></a> </li>
        <li class="nav-item disabled"> <a class="nav-link" href="#attributes" data-id="{{$product[0]['id']}}" role="tab" data-toggle="tab"><span class="hidden-sm-up"><i class="ti-folder"></i></span> <span class="hidden-xs-down">{{ __('messages.admin.attributes') }}</span></a> </li>
        <li class="nav-item disabled"> <a class="nav-link" href="#stock" data-id="{{$product[0]['id']}}" role="tab" data-toggle="tab"><span class="hidden-sm-up"><i class="ti-folder"></i></span> <span class="hidden-xs-down">{{ __('messages.admin.stock') }}</span></a> </li>
        <li class="nav-item disabled"> <a class="nav-link" href="#price" data-id="{{$product[0]['id']}}" role="tab" data-toggle="tab"><span class="hidden-sm-up"><i class="fa fa-dollar"></i></span> <span class="hidden-xs-down">{{ __('messages.admin.price') }}</span></a> </li>
        <li class="nav-item disabled"> <a class="nav-link" href="#photos"   data-id="{{$product[0]['id']}}" role="tab" data-toggle="tab"><span class="hidden-sm-up"><i class="ti-image"></i></span> <span class="hidden-xs-down">{{ __('messages.admin.photos') }}</span></a> </li>
    </ul>
</div>
<div class="tab-content">
    @include('supplier.product.products.view.basic_details')
</div>

<script>
    $('.nav-tabs a').on('click', function (e) {
        e.preventDefault()
        $(this).tab('show')
    });
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e)
    {    var id = $(this).data("id");
         var x = $(this).attr("href");
        $.ajax({
            type: "GET",
            url: "{{route('supplier.productview_tabs')}}",
            data: {'activeTab': x,
            id:id
                   },
            success: function (result) {
                $('.tab-content').html(result);
            }
        });
    });
 
 </script>