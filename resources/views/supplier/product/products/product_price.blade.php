<form id="frm_create_price" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body">
            <div class="row">
                @if($product_type == 'complex' && !empty($pdt_att_var))
                <div class="col-md-12 mt-3">
                    <label class="control-label"><strong>PRODUCT VARIANT</strong></label>
                </div>
                <div class="col-md-4"><label class="control-label"><b>{{ $pdt_att_var->lang[0]['name'] ?? '' }} variant</b></label></div>
                <div class="col-md-4"><label class="control-label"><b>{{ __('messages.admin.price1') }}</b> <span class="text-danger">*</span></label></div>
                <div class="col-md-4"><label class="control-label"><b>{{ __('messages.admin.discount') }} (%)</b></label></div>
                <input type="hidden"  name="attribute_id" value="{{ $pdt_att_var['id'] }}"/>
                @if($variants = \App\Models\ProductVariant::with('variant')->where(['product_id' => $pdt_id])->get())
                @foreach($variants as $variant_val)
                @if(!empty($variant_val->variant->id))
                @php
                $pdt_price = \App\Models\ProductPrice::with('attribute')->with('variant')->where('product_id', $pdt_id)
                ->where('variant_lang_id', $variant_val->variant->id)->first();
                @endphp
                <div class="col-md-4 mt-2">
                    <input type="hidden"  name="variant_id" value="{{ $variant_val->variant->variant_id }}"/>
                    <input type="hidden" name="variant_lang_id[]" value="{{$variant_val->variant->id }}">
                    <label class="control-label ml-1">{{ $variant_val->variant->name ?? '' }}</label>
                </div>
                <div class="col-md-4">
                    <input class="form-control form-white" placeholder="{{ __('messages.admin.enter_price') }}" type="text" name="price_var[]" id="price_var_{{$variant_val->variant->id}}" value="{{ isset($pdt_price['price']) ? $pdt_price['price'] : '' }}"/>
                </div>
                <div class="col-md-4">
                    <input class="form-control form-white" placeholder="{{ __('messages.admin.enter_discount') }}" type="text" name="discount_var[]" id="discount_{{$variant_val->variant->id}}" value="{{ isset($pdt_price['discount']) ? $pdt_price['discount'] : '' }}"/>
                    <input type="hidden"  name="easify_markup_var[]" value="{{ isset($pdt_price['easify_markup']) ? $pdt_price['easify_markup'] : $easy_markup }}"/>
                </div>
                @endif
                @endforeach
                @endif
                @else
                <div class="col-md-3">
                    <label class="control-label">{{ __('messages.admin.price1') }} <span class="text-danger">*</span></label>
                    <input class="form-control form-white" placeholder="{{ __('messages.admin.enter_price') }}" type="text" name="price" value="{{ isset($product_price)? $product_price['price'] : ''}}" id="simple_price"/>
                </div>
                <div class="col-md-3">
                    <label class="control-label">{{ __('messages.admin.discount') }} (%)</label>
                    <input class="form-control form-white" placeholder="{{ __('messages.admin.enter_discount') }}" type="text" name="discount" value="{{ isset($product_price)? $product_price['discount'] : ''}}"/>
                    <input type="hidden"  name="easify_markup" value="{{ isset($product_price)? $product_price['easify_markup'] : $easy_markup}}"/>
                </div>
                @endif

            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" id="pdt_id" name="pdt_id" value="{{ $pdt_id }}">
            <input type="hidden" id="product_type" name="product_type" value="{{ $product_type }}">
            <input type="hidden" id="submit_action" value="" />
            @if(!empty($pdt_id))
            <button type="submit" class="btn btn-info waves-effect waves-light save-btn">
                {{ __('messages.admin.save') }}
            </button>
            @endif
            <button type="submit" class="btn btn-info waves-effect waves-light save-and-continue">
                {{ __('messages.admin.save_continue') }}
            </button>
            <a class="btn btn-default waves-effect tab_back" data-id='{{ $pdt_id }}' href="javascript:void(0);">{{ __('messages.admin.back') }}</a>
        </div>
    </div>
</form>
<script>
    $(".save-and-continue").on('click', function () {
        $("#submit_action").val('continue');
    });
    $(".save-btn").on('click', function () {
        $("#submit_action").val('save');
    });
    $('.tab_back').on('click', function () {
        var pdt_id = $(this).data("id");
        $.ajax({
            type: "GET",
            url: "{{route('supplier.product_tabs')}}",
            data: {'activeTab': '#stock', pdt_id: pdt_id},
            success: function (result) {
                $('#pdt_tab a[href="#stock"]').tab('show');
                $('.tab-content').html(result);
            }
        });
    });
    $("#frm_create_price").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        rules: {
            price: {
                required: true,
                number: true
            },
            discount: {
                number: true,
//                lessThan: "#simple_price",
            },
            easify_markup: {
                required: true,
                number: true
            },
            "price_var[]": {
                required: true,
                number: true
            },
            "discount_var[]": {
                number: true,
            },
            "easify_markup_var[]": {
                required: true,
                number: true
            },
        },
        messages: {
            price: {
                required: @json(__('messages.admin.price_validation_1')),
                number: @json(__('messages.admin.price_validation_2')),
            },
            discount: {
                number: @json(__('messages.admin.price_validation_2')),
            },
            // easify_markup: {
            //     required: 'Easify markup is required',
            //     number: "Numbers only"
            // },
            "price_var[]": {
                required: @json(__('messages.admin.price_validation_1')),
                number: @json(__('messages.admin.price_validation_2')),
            },
            "discount_var[]": {
                number: @json(__('messages.admin.price_validation_2')),
            },
            // "easify_markup_var[]": {
            //     required: 'Easify markup is required',
            //     number: "Numbers only"
            // },
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "{{route('supplier.save_product_price')}}",
                data: $('#frm_create_price').serialize(),
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data.status == 1) {
                        if ($("#submit_action").val() == 'continue') {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#pdt_tab a[href="#photos"]').tab('show');
                            $('.tab-content').html(data.result);
                        } else {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                        }
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                    }
                    $('button:submit').attr('disabled', false);
                },
                error: function (err) {
                    $('button:submit').attr('disabled', false);
                }
            });
            return false;
        }
    });

    $.validator.addMethod('lessThan', function (value, element, param) {
        if (this.optional(element))
            return true;
        var i = parseInt(value);
        var j = parseInt($(param).val());
        return i < j;
    }, @json(__('messages.admin.price_validation_4')));

</script>