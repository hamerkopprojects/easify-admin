@extends('layouts.master')
@section('content')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 p-r-0 title-margin-right">
                    <div class="page-header">
                        <div class="page-title">
                            <h1>{{ __('messages.admin.stock_update') }} - {{ __('messages.admin.step3') }}</h1>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /# row -->
            <div class="row">
                <div class="col-lg-12">
                    <p><strong>{{ __('messages.admin.toatal_records_to_import') }}: {{ $total_count }}</strong></p>
                    <p>Total records imported: {{ $success_count }} </p>
                    <p>{{ __('messages.admin.errors') }}: {{$error_count}}</p>
                    <div class="card">
                        <div class="card-body">
                            <form class="frm-search" id="posts-filter" method="get" action="">
                                <div class="row tablenav top text-right">
                                    <div class="col-md-8 ml-0">
                                        <input class="form-control search_txt" type="text" name="search" value="{{ $search }}" placeholder="{{ __('messages.admin.search_by_sku') }}">
                                    </div>
                                    <div class="col-md-3 text-left">
                                        <button type="submit" class="btn btn-info"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{ __('messages.admin.search') }}</font></font></button>
                                        <a href="{{route('stock.imported_data_complex')}}" class="btn btn-default btn-reset">{{ __('messages.admin.reset') }}</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-12 text-right">
                        <a href="{{route('stock_update')}}?action=complex" class="btn btn-info ml-2"><font style="vertical-align: inherit;">{{ __('messages.admin.start_new_import') }}</font></a>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>{{ __('messages.admin.sno') }}</th>
                                            <th>{{ __('messages.admin.sku') }}</th>
                                            <th>Variant</th>
                                            <th>{{ __('messages.admin.min_stock') }}</th>
                                            <th>{{ __('messages.admin.max_stock') }}</th>
                                            <th class="cls_last_child"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($result_data) > 0)
                                        @php
                                        $i = 1;
                                        @endphp
                                        @foreach ($result_data as $row_data)
                                        <tr>
                                            <th>{{$i++}}</th>
                                            <td>{{$row_data->sku}}</td>
                                            <th>{{$row_data->variant}}</th>
                                            <td>{{$row_data->min_stock}}</td>
                                            <td>{{$row_data->max_stock}}</td>
                                            @if($row_data->error_flag=="Y")
                                            <td class="cls_last_child" style="background:red;color:white;">{{ __('messages.admin.errors') }}
                                                <a href="#" class="btn btn-sm btn-success text-white error-data float-right" title="View Log" data-id="{{$row_data->id}}"><i class="fa fa-eye"></i></a></td>
                                            @else
                                            <td class="cls_last_child" style="background:green;color:white;">{{ __('messages.admin.imported') }}</td>
                                            @endif
                                        </tr>
                                        @endforeach
                                        @else
                                        <tr>
                                            <td colspan="12" class="text-center">{{ __('messages.admin.no_records_found') }}</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center d-flex justify-content-center mt-3">
                                {{ $result_data->appends(request()->query())->links() }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 text-right">
                        <a href="{{route('stock_update')}}?action=complex" class="btn btn-info ml-2"><font style="vertical-align: inherit;">{{ __('messages.admin.start_new_import') }}</font></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal pooup -->
<div class="modal fade none-border" id="formModal">
    <div class="modal-dialog modal-lg" style="max-height:85%;  margin-top: 50px; margin-bottom:50px;max-width: 100%">
        <div class="modal-content">

        </div>
    </div>
</div>
<!-- END MODAL -->
@endsection
@push('scripts')
<script>
    $(window).load(function () {
        $('.loading_box').hide();
        $('.loading_box_overlay').hide();
    });
    $('.error-data').on('click', function () {
        var id = $(this).data("id");
        $.dialog({
            title: 'Error: Data with Errors',
            content: "url:{{route('stock.import_errors_complex')}}?id=" + id,
            //                            animation: 'scale',
            columnClass: 'large',
            //                            closeAnimation: 'scale',
            backgroundDismiss: true,
        });
    });
</script>
@endpush