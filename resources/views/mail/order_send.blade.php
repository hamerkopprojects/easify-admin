@include('mail.layouts.header')
<repeater>
    <!-- Intro -->
    <layout label='Intro'>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td style="padding-bottom: 10px;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="tbrr p30-15" style="padding: 40px 30px; border-radius:26px 26px 0px 0px;" bgcolor="#ffffff">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="img m-center" style="font-size:0pt; line-height:30pt;padding-bottom:30px; text-align:center;"><img src="{{asset('mail/logo.png')}}" width="142" height="70" editable="true" border="0" alt=""  style="    border-radius: 6px;"></td>
                                    </tr>
                                    <tr>
                                        <td class="h1 pb25" style="color:#2eb680; font-family:'Muli', Arial,sans-serif; font-size:30px; line-height:30px; text-align:center; padding-bottom:5px;">
                                    <multiline>
                                        Congratulations!
                                        </td>
                                        </tr>
                                        <tr>
                                            <td class="h1 pb25" style="color:#000000; font-family:'Muli', Arial,sans-serif; font-size:20px; line-height:30px; text-align:center; padding-bottom:5px;">
                                        <multiline>
                                            {{$name}}
                                            </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center pb25" style="color:#00000; font-family:'Muli', Arial,sans-serif; font-size:16px; line-height:20px; text-align:center; padding-bottom:5px;">
                                            <multiline>Thank you for your order!</multiline>
                                            </td>
                                            </tr>
                                            </table>
                                            </td>
                                            </tr>
                                            </table>

                                            <table width="780" bgcolor="#ffffff" cellpadding="0" align="center" cellspacing="0">
                                                <tbody>
                                                    <tr>
                                                        <td
                                                            style="border-top:0;border-right:0; border-bottom:1px solid #dee1e2; border-left:0;">
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table  width="780" bgcolor="#ffffff" cellpadding="0" align="center" cellspacing="0" style="padding: 30px; padding-top: 30px; font-family:'Muli', Arial,sans-serif; padding-left: 30px;     padding-right: 30px;">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            @if($order_status == 1)
                                                            <img src="{{asset('mail/line4.png')}}"  editable="true" border="0" alt="" width="720" height="110">
                                                            @elseif($order_status == 2)
                                                            <img src="{{asset('mail/line3.png')}}"  editable="true" border="0" alt=""   width="720" height="110">
                                                            @elseif($order_status == 3)
                                                            <img src="{{asset('mail/line2.png')}}"  editable="true" border="0" alt=""   width="720" height="110">
                                                            @else
                                                            <img src="{{asset('mail/line.png')}}"  editable="true" border="0" alt=""   width="720" height="110">
                                                            @endif

                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <!-- Article / Image On The Right - Copy On The Left -->
                                            <layout label='Article / Image On The Right - Copy On The Left'>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-top:10px; font-family:'Muli', Arial,sans-serif; ">
                                                    <tr>
                                                        <td class="p30-15" style="padding: 30px 30px;" bgcolor="#ffffff">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" dir="rtl" style="direction: rtl;">
                                                                <tr>
                                                                    <th class="column-dir-top" dir="ltr" width="280" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr; vertical-align:top;">
                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="    border-left: 1px solid #dee1e2;    padding-left: 40px;">
                                                                            <tr>
                                                                                <td class="fluid-img" style="font-size:0pt; text-align:left;">
                                                                                    <h1 style="font-size: 18px; line-height:20px">DELIVERY ADDRES:</h1>
                                                                                    <h6 style="margin-top: 2px; margin-bottom: 4px; font-size: 14px; line-height:20px"> {{ $order->delivery_address ?? '' }}</h6>
                                                                                    <p style="margin-top: 2px; margin-bottom:4px; font-size: 14px; line-height:20px">Phone: {{ $order->delivery_phone ?? '' }}</p>
                                                                                    <p style="margin-top: 2px; margin-bottom:4px; font-size: 14px; line-height:20px"> {{ $order->delivery_name ?? '' }}</p>
                                                                                    <p style="margin-top: 2px; margin-bottom:4px; font-size: 14px; line-height:20px"> {{ $order->delivery_city ?? '' }}</p>
                                                                                    <p style="margin-top: 2px; margin-bottom:4px; font-size: 14px; line-height:20px"> {{ $order->delivery_loc ?? '' }}</p>
                                                                                    <p style="margin-top: 2px; margin-bottom:4px; font-size: 14px; line-height:20px"> PO Box: {{ $order->delivery_zip ?? '' }}</p>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </th>
                                                                    <th class="column-empty2" width="30" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;"></th>
                                                                    <th class="column-dir-top" dir="ltr" width="280" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr; vertical-align:top; text-align:left">
                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                                                                            <tr>
                                                                                <td>
                                                                                    <b style="margin-top: 2px; margin-bottom: 20px; font-size: 14px; text-align:left; line-height:20px">Order ID</b>
                                                                                    <p style="margin-top: 2px; margin-bottom: 20px; font-size: 14px; text-align:left; line-height:20px">{{ $order->order_id }}</p>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <b style="margin-top: 2px; margin-bottom: 20px; font-size: 14px; text-align:left; line-height:20px">Delivery Date  </b>
                                                                                    <p style="margin-top: 2px; margin-bottom: 20px; font-size: 14px; text-align:left; line-height:20px">{{ date('j F Y', strtotime($order->delivery_schedule_date))}}</p>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <b style="margin-top: 2px; margin-bottom: 20px; font-size: 14px; text-align:left; line-height:20px">Time Slot </b>
                                                                                    <p style="margin-top: 2px; margin-bottom: 20px; font-size: 14px; text-align:left; line-height:20px">{{ $order->time_slot }}</p>
                                                                                </td>
                                                                            </tr>

                                                                            <!-- END Button -->
                                                                        </table>
                                                                    </th>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="img pb10" style="font-size:0pt; line-height:0pt; text-align:left; padding-bottom:10px; "></td>
                                                    </tr>
                                                </table>
                                            </layout>


                                            <table width="780" bgcolor="#ffffff" cellpadding="0" align="center" cellspacing="0" style="font-family:'Muli', Arial,sans-serif;padding-top: 30px;    padding-bottom:30px;">
                                                <thead sty>
                                                    <tr face="arial">
                                                        <td width="15%" style="padding-bottom: 10px;font-size: 16px; padding-left: 30px; font-weight: 500;"><strong>ITEMS</strong></td>
                                                        <td width="40%" style="padding-bottom: 10px;font-size: 16px;font-weight: 500;"></td>
                                                        <td width="15%" style="padding-bottom: 10px;font-size: 16px; font-weight: 500;"><strong>Price</strong></td>
                                                        <td width="10%" style="padding-bottom: 10px;font-size: 16px; font-weight: 500;"><strong>Quantity</strong></td>
                                                        <td width="20%" style="padding-bottom: 10px;font-size: 16px; font-weight: 500;"><strong>Amount</strong></td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($order->items as $item)
                                                    @php
                                                    $name = $item->product->lang[0]->name ?? '';
                                                    @endphp
                                                    <tr>
                                                        <td width="15%" style="padding-bottom: 10px;font-size: 14px;font-weight: 500; padding-left: 30px; ">
                                                            @if(is_file( public_path() . '/uploads/' . $item->product->cover_image))
                                                            <img src="{{ url('uploads/'.$item->product->cover_image) }}" width="60" height="60">
                                                            @else
                                                            <img src="{{ asset('b2b/images/no-image.png') }}" width="60" height="60">
                                                            @endif
                                                        </td>
                                                        <td width="30%" style="padding-bottom: 10px;font-size: 14px; font-weight: 500;">
                                                            {{ $name }}
                                                        </td>
                                                        <td width="15%" style="padding-bottom: 10px;font-size: 14px; font-weight: 500; ">SAR {{$item->item_price}}</td>
                                                        <td width="10%" style="padding-bottom: 10px;font-size: 14px; font-weight: 500; ">{{ $item->item_count }} </td>
                                                        @php $sub_total = $item->sub_total; @endphp
                                                        <td width="30%" style="padding-bottom: 10px;font-size: 14px; font-weight: 500; ">SAR {{ number_format($sub_total, 2, ".", "") }}</td>
                                                    </tr>
                                                    @endforeach
                                                    <tr>
                                                        <td height="20 "colspan="5"
                                                            style="border-top:0;border-right:0;border-bottom:1px solid #dee1e2;border-left:0;">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="1"></td>
                                                        <td colspan="1"></td>
                                                        <td colspan="2" style="padding-top: 10px;font-size: 14px;font-weight: 500; line-height:20px"><strong>Sub Total:</strong></td>
                                                        @php $ord_sub_total = $order->sub_total; @endphp
                                                        <td style="padding-top: 10px;font-size: 18px; font-weight: 500; color: #2eb680; line-height:20px"><b>SAR {{ number_format($ord_sub_total, 2, ".", "") }}</b></td>
                                                    </tr>
                                                    @if(!empty($order->payment_method) && $order->payment_method == 'cod')
                                                    <tr>
                                                        <td colspan="1" ></td>
                                                        <td colspan="1" ></td>
                                                        <td colspan="2" style="padding-top: 10px;font-size: 14px; font-weight: 500; line-height:20px"><strong>COD Fee:</strong></td>
                                                        <td style="padding-top: 10px;font-size: 14px; font-weight: 500; line-height:20px">SAR {{ $order->cod_fee ?? '' }}</td>
                                                    </tr>
                                                    @endif
                                                    @if(!empty($order->delivery_charge) && $order->delivery_charge != 0.00)
                                                    <tr>
                                                        <td colspan="1" ></td>
                                                        <td colspan="1" ></td>
                                                        <td colspan="2" style="padding-top: 10px;font-size: 14px; font-weight: 500; line-height:20px"><strong>Delivery charge:</strong></td>
                                                        <td style="padding-top: 10px;font-size: 14px; font-weight: 500; line-height:20px">SAR {{ $order->delivery_charge ?? '' }}</td>
                                                    </tr>
                                                    @endif
                                                    <tr>
                                                        <td colspan="1" ></td>
                                                        <td colspan="1" ></td>
                                                        <td colspan="2"style="padding-top: 10px;font-size: 14px; font-weight: 500; ">
                                                            <b style="line-height:20px">TOTAL AMOUNT:</b>
                                                            <p style="font-size: 13px; padding: 0px; margin: 0px; color: #a7a0a0; line-height:20px">(Inclusive of VAT)</p>
                                                        </td>
                                                        @php $final_amount = $ord_sub_total + $order->delivery_charge + $order->cod_fee; @endphp
                                                        <td style="padding-top: 10px;font-size: 18px; font-weight: 500; color: 42a736; line-height:20px"><b>SAR {{ number_format($final_amount, 2, ".", "") }}</b></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            @include('mail.layouts.common')
                                            </td>
                                            </tr>
                                            </table>
                                            </layout>
                                            <!-- END Two Columns -->
                                            </repeater>
                                            @include('mail.layouts.footer')