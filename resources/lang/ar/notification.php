<?php

    return[
        'enabled' => 'Notification setting enabled successfully',
        'disabled' => 'Notification setting disabled successfully',
        'support' => [
            'title' => [
                'replied' => 'Hi :user',
            ],
            'content' => [
                'replied' => 'تم رد على استفسارتكم رقم :subject من قبل فريق العمليات ',
            ]
        ],
        'order'=>[
            'title'=>[
                'cancel'=>'Hi ',
                'schedule'=>'تم جدولة الطلب',
                'collection'=>'New order for collection',
            ],
            'content'=>[
                'cancel'=>'تم قبول الغاء الطلب رقم  :order_id من فريق العمليات ',
                'schedule'=>'تم تحديث جدولة تسليم الطلب رقم:order_id لتاريخ  :date :slot ',
                'collection'=>'تم تعينك من قبل فريق العمليات لاستلام طلب رقم :order_id من المورد رقم :supp في تاريخ :date  :slot ',
            ],
        ],
        'success_msg'=>[
            'support_success'=>'شكرا لك على تقديم الطلب. سيقوم فريقنا بالرد عليك'
        ],
        'qr'=>[
            'pdt_collected'=>'Products already collected',
            'supp_collected'=>"Are you sure to collect all products from supplier  ? ",
            'ord_delivered'=>'Order alredy delivered',
            'ord_del_confirm'=>"Are you sure to deliver the order  ? ",
            'pdt_aldy_ware'=>'Product already in warehouse',
            'qr_invd'=>'Invalid QR code',
            'Collect'=>"Are you sure to collect  the product ? ",
            'place_at_ware_house'=>'Are you sure to place the the product at warehouse'
        ],
        'collection_stat'=>[
            'collection_completed' =>'اكتمل التحصيل',
            'Collection_pending' =>'عملية التحصيل معلقة',
            'Collection_started' =>'بدأ التحصيل',
            'cancelled' =>'تم الالغاء',
            'stat_success'=> 'تم تحديث الحالة بنجاح',
        ],
        'order_status' => [
            'Waiting for supplier action' => "في انتظار إجراء المورد",
            'Ready for collection' => "جاهز للتحصيل",  
            'Collection pending'  => 'عملية التحصيل معلقة',
            'Collection started' => "بدأ الإستلام",
            'Collection completed' => "اكتمل التحصيل",
            'Cancelled' => 'تم الالغاء',
            'Ready for delivery' => "جاهز للتوصيل",
            'Cancellation request' => "طلب الإلغاء",

            'Delivered' => "تم التوصيل",

        ],
    ];