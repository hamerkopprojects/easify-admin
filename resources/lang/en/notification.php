<?php

    return[
        'enabled' => 'Notification setting enabled successfully',
        'disabled' => 'Notification setting disabled successfully',
        'support' => [
            'title' => [
                'replied' => 'Hi :user',
            ],
            'content' => [
                'replied' => 'Opertaion team replied on your query :subject',
            ]
        ],
        'order'=>[
            'title'=>[
                'cancel'=>'Hi ',
                'schedule'=>'Order scheduled for you',
                'collection'=>'New order for collection',
                'cancel_collection' =>'Cancel the  collection for order '
            ],
            'content'=>[
                'cancel'=>'The order cancellation request on the order :order_id has been accepted by the operation team',
                'schedule'=>'The delivery schedule of the order :order_id has been updated to :date  :slot',
                'collection'=>'The operation team assigned you for the collection of the order :order_id from the supplier :supp on :date :slot ',
                'cancel_collection' =>'Admin cancelled order :order_id stop the collectins from'
            ],
        ],
        'success_msg'=>[
            'support_success'=>'Thank you for submitting the support request. Our team will get back to you!',
            
        ],
        'qr'=>[
            'pdt_collected'=>'Products already collected',
            'supp_collected'=>"Are you sure to collect all products from supplier  ? ",
            'ord_delivered'=>'Order alredy delivered',
            'ord_del_confirm'=>"Are you sure to deliver the order  ? ",
            'pdt_aldy_ware'=>'Product already in warehouse',
            'qr_invd'=>'Invalid QR code',
            'Collect'=>"Are you sure to collect  the product ? ",
            'place_at_ware_house'=>'Are you sure to place the the product at warehouse'
        ],
        'collection_stat'=>[
            'collection_completed'=>'Collection Completed',
            'Collection_pending' => 'Collection Pending',
            'Collection_started' =>'Collection Started',
            'cancelled' =>'Cancelled',
            'stat_success'=> 'Status updated succesfully'
        ],
        'order_status' => [
            'Waiting for supplier action' => "Waiting for supplier action",
            'Ready for collection' => "Ready for collection",  
            'Collection pending' => "Collection pending",
            'Collection started' => "Collection started",
            'Collection completed' => "Collection completed",
            'Cancelled' => "Cancelled",
            'Ready for delivery' => "Ready for delivery",
            'Cancellation request' => "Cancellation request",
            'Delivered' => "Delivered"
        ],
        
    ];