<?php

return[
    'login'=>[
        'otp_success' =>'OTP send to the phone number entered!',
        'user_exist'=>'Driver with the phone number entered not registered with us!',
        'otp_wrong'=>'OTP entered is wrong',
        'otp_resend'=>'OTP resend to the phone number entered!'
    ]
];
