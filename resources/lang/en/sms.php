<?php

return [
    'otp' => [
        'driver' => 'Hello from Easify ! Your verification number is :otp',
        'web' => 'Hello from Easify web! Your verification number is :otp',
    ],
];