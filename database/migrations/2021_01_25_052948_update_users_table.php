<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->string('fcm_token')->nullable()->after('password');
            $table->string('otp')->nullable()->after('phone');
            $table->enum('language',['en','ar'])->after('status')->default('en');
            $table->dateTime('otp_generated_at')->after('phone')->nullable();
            $table->string('country_code')->after('phone')->nullable();
            $table->boolean('need_notification')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
