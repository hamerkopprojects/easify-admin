<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFieldsToSliderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('slider', function (Blueprint $table) {
            $table->string('web_image_ar', 500)->nullable()->after('content');
            $table->enum('ar_flag', ['U', 'C', 'P'])->comment('U: Url, C: Category, P: Product')->nullable();
            $table->string('url_ar', 60)->nullable();
            $table->integer('category_ar')->nullable();
            $table->integer('product_ar')->nullable();
            $table->string('title_ar', 60)->nullable();
            $table->text('content_ar')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('slider', function (Blueprint $table) {
            //
        });
    }
}
