<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOtherSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('other_settings', function (Blueprint $table) {
            $table->id();
            $table->engine = 'InnoDB';
            $table->text('currency')->nullable();
            $table->text('driver_loc_fetch_interval')->nullable();
            $table->text('payment_options')->nullable();
            $table->decimal('cod_fee')->nullable();
            $table->enum('auto_approval_for_cod_orders',['enable', 'disable'])->default('disable');
            $table->string('warehouse_loc')->nullable();
            $table->text('operational_area')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('other_settings');
    }
}
