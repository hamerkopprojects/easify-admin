<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCancelReasonAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cancellation_apps', function (Blueprint $table) {
            $table->id();
            $table->engine = 'InnoDB';
            $table->unsignedBigInteger('cancellation_id');
            $table->unsignedTinyInteger('app_id');
            $table->timestamps();

            $table->foreign('cancellation_id')
                ->references('id')
                ->on('cancellation_reason')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('app_id')
                ->references('id')
                ->on('app_types')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cancellation_apps');
    }
}
