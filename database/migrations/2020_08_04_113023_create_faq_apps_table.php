<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaqAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faq_apps', function (Blueprint $table) {
            $table->id();
            $table->engine = 'InnoDB';
            $table->unsignedSmallInteger('faq_id');
            $table->unsignedTinyInteger('app_id');
            $table->timestamps();

            $table->foreign('faq_id')
                ->references('id')
                ->on('faq')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('app_id')
                ->references('id')
                ->on('app_types')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faq_apps');
    }
}
