<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBussinessTypeI18nTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bussiness_type_i18n', function (Blueprint $table) {
            $table->id();
            $table->engine = 'InnoDB';
            $table->text('name');
            $table->enum('language', ['en', 'ar']);
            $table->unsignedBigInteger('bussiness_type_id');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('bussiness_type_id')
            ->references('id')
            ->on('bussiness_type')
            ->onUpdate('cascade')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bussiness_type_i18n');
    }
}
