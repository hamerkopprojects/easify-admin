<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvertisementsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('advertisements', function (Blueprint $table) {
            $table->id();
            $table->engine = 'InnoDB';
            $table->string('ad_type', 60)->nullable();
            $table->enum('web_flag', ['U', 'C', 'P'])->comment('U: Url, C: Category, P: Product')->nullable();
            $table->string('url', 60)->nullable();
            $table->integer('web_category')->nullable();
            $table->integer('web_product')->nullable();
            $table->string('web_image', 500);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('advertisements');
    }

}
