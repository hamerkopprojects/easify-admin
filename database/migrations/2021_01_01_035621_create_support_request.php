<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupportRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('support_requests', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->integer('order_id')->nullable();
            $table->string('subject', 1000);
            $table->text('message')->nullable();
            $table->enum('app_type', ['driver', 'customer']);
            $table->unsignedBigInteger('submitted_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('support_requests');
    }
}
