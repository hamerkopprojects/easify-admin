<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSliderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slider', function (Blueprint $table) {
            $table->id();
            $table->engine = 'InnoDB';
            $table->string('slider_type', 60)->nullable();
            $table->string('web_image', 500)->nullable();
            $table->enum('web_flag', ['U', 'C', 'P'])->comment('U: Url, C: Category, P: Product')->nullable();
            $table->string('url', 60)->nullable();
            $table->integer('category')->nullable();
            $table->integer('product')->nullable();
            $table->string('title', 60)->nullable();
            $table->text('content')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slider');
    }
}
