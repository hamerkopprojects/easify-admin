<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->unsignedInteger('category_id')->comment('Mapping Category ID');
            $table->unsignedInteger('brand_id');
            $table->string('sku', 60)->nullable();
            $table->text('slug')->nullable();
            $table->integer('supplier_id')->nullable();
            $table->enum('product_type', ['simple', 'complex'])->default('simple');
            $table->enum('status', ['active', 'deactive'])->default('active');
            $table->text('cover_image')->nullable();
            $table->text('category_ids')->nullable();
            $table->timestamps();
            $table->softDeletes();
            
             $table->foreign('category_id')->references('id')->on('category')->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('brand_id')->references('id')->on('brand')->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
