<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOrderItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_items', function (Blueprint $table) {
            
            $table->dropColumn('status');
        });
        Schema::table('order_items', function (Blueprint $table) {
            
            $table->integer('driver_collection_status')->default(1);
            $table->enum('status',['pending','accept','reject','at warehouse','collected'])
                        ->default('pending')
                        ->after('supplier_status');
            
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
