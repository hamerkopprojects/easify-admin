<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFieldToProductCartOrderTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('supplier_annex', function (Blueprint $table) {
            $table->text('cr_copy')->nullable()->change();
        });
         Schema::table('product', function (Blueprint $table) {
            $table->integer('commision_percentage')->nullable()->after('supplier_id');
        });
         Schema::table('cart_items', function (Blueprint $table) {
            $table->integer('commision_percentage')->nullable()->after('easify_markup');
        });
        Schema::table('order_items', function (Blueprint $table) {
            $table->integer('commision_percentage')->nullable()->after('easify_markup');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('supplier_annex', function (Blueprint $table) {
            //
        });
    }
}
