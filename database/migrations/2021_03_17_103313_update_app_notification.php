<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAppNotification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE app_notifications CHANGE COLUMN from_type from_type ENUM('website', 'driver', 'admin','supplier','branch')");
        DB::statement("ALTER TABLE app_notifications CHANGE COLUMN to_type to_type ENUM('website', 'driver', 'admin','supplier','branch')");
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
