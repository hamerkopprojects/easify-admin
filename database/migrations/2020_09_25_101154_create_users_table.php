<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->engine = 'InnoDB';
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('verify_token')->nullable();
            $table->string('ip_address', 60)->nullable();
            $table->dateTime('last_login_date')->nullable();
            $table->dateTime('last_activity_date')->nullable();
            $table->unsignedBigInteger('role_id')->nullable();
            $table->string('phone', 15)->nullable();
            $table->string('user_id', 20)->nullable();
            $table->enum('status', ['active', 'deactive'])->default('deactive');
            $table->rememberToken();
            $table->timestamps();
            $table->foreign('role_id')
                ->references('id')
                ->on('user_roles')
                ->onUpdate('cascade')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
