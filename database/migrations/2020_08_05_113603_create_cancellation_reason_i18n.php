<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCancellationReasonI18n extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cancellation_reason_i18n', function (Blueprint $table) {
            $table->id();
            $table->engine = 'InnoDB';
            $table->text('name');
            $table->enum('language', ['en', 'ar']);
            $table->unsignedBigInteger('cancellation_id');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('cancellation_id')
            ->references('id')
            ->on('cancellation_reason')
            ->onUpdate('cascade')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cancellation_reason_i18n');
    }
}
