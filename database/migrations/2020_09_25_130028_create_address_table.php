<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('cust_id');
            $table->string('apartment_name', 60)->nullable();
            $table->string('street_name', 60)->nullable();
            $table->string('postal_code', 60)->nullable();
            $table->integer('region')->nullable();
            $table->enum('is_default', ['Y', 'N'])->default('N');
            $table->enum('address_flag', ['D', 'B'])->comment('D: Delivery, B: Billing');
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('cust_id')
                ->references('id')
                ->on('customer')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('address');
    }
}
