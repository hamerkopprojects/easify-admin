<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOrderAndOrderItemsData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->unsignedBigInteger('supplier_id')->after('status')->nullable();
            $table->unsignedBigInteger('driver_id')->nullable();
            $table->decimal('item_price',2)->nullable();
            $table->dateTime('collection_date')->nullable();
            $table->string('collection_time_slot')->nullable();
           

        });
        Schema::table('order', function (Blueprint $table) {
            $table->string('billing_name')->nullable();
            $table->text('billing_address')->nullable();
            $table->string('billing_city')->nullable();
            $table->string('billing_country',150)->nullable();
            $table->string('billing_zip',150)->nullable();
            $table->string('billing_phone')->nullable();
            $table->string('delivery_phone')->nullable()->after('delivery_zip');
            $table->string('time_slot')->nullable();
            $table->enum('supplier_status',['pending','accept','collection pending','collection completed'])->default('pending');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
