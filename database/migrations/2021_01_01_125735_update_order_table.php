<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order', function (Blueprint $table) {
            $table->unsignedBigInteger('cancel_reason_id')->nullable()->after('cancel_note');
            $table->enum('updated_by',['customer','admin','driver'])->nullable('cancel_note');
            $table->unsignedBigInteger('updated_by_id')->nullable()->after('cancel_note');
            // $table->text('cancel_note')->nullable()->after('delivery_zip');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order', function (Blueprint $table) {
           
        });
    }
}
