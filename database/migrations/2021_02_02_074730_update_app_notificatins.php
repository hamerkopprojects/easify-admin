<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAppNotificatins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('app_notifications', function (Blueprint $table) {
            $table->dropColumn('from_type');
            $table->dropColumn('to_type');
            
        });
        Schema::table('app_notifications', function (Blueprint $table) {
            
            $table->enum('from_type', ['website','driver','admin'])
                    ->comment('From where the notification is generated')
                    ->after('details');
            $table->enum('to_type', ['website', 'admin','driver'])
                    ->comment('The recepient type')
                    ->after('to_id');
            
        });
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('app_notifications', function (Blueprint $table) {
            
            
        });
    }
}
