<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFieldsToOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order', function (Blueprint $table) {
            $table->dropColumn('payment_method');
            $table->dropColumn('time_slot');
        });
        Schema::table('order', function (Blueprint $table) {
            $table->decimal('cod_fee',10,2)->nullable()->after('delivery_charge');
            $table->enum('payment_method',['cod','online','offline'])->nullable()->after('cod_fee');
            $table->text('payment_slip')->nullable()->after('payment_method');
            $table->integer('vat')->nullable()->after('payment_slip');
            $table->string('time_slot', 50)->after('delivery_schedule_date')->nullable();
            $table->integer('delivery_days')->after('time_slot')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order', function (Blueprint $table) {
            //
        });
    }
}
