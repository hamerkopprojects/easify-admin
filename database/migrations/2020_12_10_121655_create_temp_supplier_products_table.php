<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempSupplierProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_supplier_products', function (Blueprint $table) {
            $table->id();
            $table->string('sku', 60)->nullable();
            $table->string('product_name_en', 60)->nullable();
            $table->string('product_name_ar', 60)->nullable();
            $table->string('category', 60)->nullable();
            $table->string('brand', 60)->nullable();
            $table->text('description_en')->nullable();
            $table->text('description_ar')->nullable();
            $table->text('ingredients_en')->nullable();
            $table->text('ingredients_ar')->nullable();
            $table->text('how_to_use_en')->nullable();
            $table->text('how_to_use_ar')->nullable();
            $table->text('reasons_to_buy_en')->nullable();
            $table->text('reasons_to_buy_ar')->nullable();
            $table->decimal('price', 8, 2)->nullable();
            $table->decimal('discount_price', 8, 2)->nullable();
            $table->integer('min_stock')->nullable();
            $table->integer('max_stock')->nullable();
            $table->integer('easify_markup')->nullable();
            $table->text('errors')->nullable();
            $table->enum('error_flag', ['Y', 'N'])->default('N');
            $table->enum('duplicate_flag', ['Y', 'N'])->default('N');
            $table->enum('cron_flag', ['I', 'S', 'C'])->default('I')->comment('I: Initiated, S: Started, C: Completed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_supplier_products');
    }
}
