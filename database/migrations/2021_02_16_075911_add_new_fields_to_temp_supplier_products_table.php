<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFieldsToTempSupplierProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('temp_supplier_products', function (Blueprint $table) {
             $table->string('main_category', 60)->nullable()->after('product_name_ar');
             $table->string('level2_category', 60)->nullable()->after('main_category');
             $table->text('level3_category')->nullable()->after('level2_category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('temp_supplier_products', function (Blueprint $table) {
            //
        });
    }
}
