<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateSupportChatAndRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('support_requests', function (Blueprint $table) {
            $table->dropColumn('app_type');
        });
        Schema::table('support_requests', function (Blueprint $table) {
            $table->enum('app_type',['driver','customer','supplier','branch'])->after('message');
            
        });

        Schema::table('support_chat', function (Blueprint $table) {
            $table->dropColumn('user_type');
            
        });
        Schema::table('support_chat', function (Blueprint $table) {
            
            $table->enum('user_type',['driver','customer','supplier','branch','admin'])->after('message');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
