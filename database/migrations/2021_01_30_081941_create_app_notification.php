<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppNotification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_notifications', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->text('content');
            $table->enum('from_type', ['b2b', 'b2c','driver','admin'])->comment('From where the notification is generated');
            $table->enum('to_type', ['b2b', 'b2c', 'admin','driver'])->comment('The recepient type');
            $table->text('details')->nullable();
            $table->unsignedBigInteger('to_id')->nullable();
            $table->boolean('is_read')->default(0);
            $table->boolean('admin_read')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_notifications');
    }
}
