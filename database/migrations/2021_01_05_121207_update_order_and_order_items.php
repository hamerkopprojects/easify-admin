<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOrderAndOrderItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    
        
        Schema::table('order', function (Blueprint $table) {
            $table->enum('status',['init','ready for collection','collection pending','collection started','collection completed','delivered','cancelled','schedule for delivery','cancellation request'])->default('init')->after('cancel_reason_id');

        });
        
        // DB::statement("ALTER TABLE order CHANGE COLUMN status status ENUM('init','ready for collection','collection pending','collection started','collection completed','delivered','cancelled','schedule for delivery','cancellation request')");

        Schema::table('order_items', function (Blueprint $table) {
            $table->enum('status',['pending','accept','collected','at warehouse','cancelled'])->nullable()->default('pending')->after('product_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
