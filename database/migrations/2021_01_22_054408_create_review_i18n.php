<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewI18n extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review_segments', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->unsignedBigInteger('review_id');
            $table->unsignedBigInteger('segment_id');
            $table->string('rating', 50)->nullable();
            $table->timestamps();
            $table->foreign('review_id')
                ->references('id')
                ->on('review')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('segment_id')
                ->references('id')
                ->on('rating_segments')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('review_i18n');
    }
}
