<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeStatusFieldInOrderAndOrderItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order', function (Blueprint $table) {
            //$table->dropColumn('status');
            //$table->dropColumn('supplier_status');
        });
        Schema::table('order', function (Blueprint $table) {
            $table->integer('order_status')->nullable()->after('cancel_reason_id')->default('1');
            
        });
        Schema::table('order_items', function (Blueprint $table) {
           
            //$table->dropColumn('supplier_status');
        });
        Schema::table('order_items', function (Blueprint $table) {
            $table->integer('supplier_status')->nullable()->after('status')->default('1');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_and_order_items', function (Blueprint $table) {
            //
        });
    }
}
