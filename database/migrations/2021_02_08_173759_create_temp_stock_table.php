<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_stock', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->string('sku', 60)->nullable();
            $table->integer('min_stock')->nullable();
            $table->integer('max_stock')->nullable();
            $table->string('variant', 60)->nullable();
            $table->text('errors')->nullable();
            $table->enum('error_flag', ['Y', 'N'])->default('N');
            $table->enum('duplicate_flag', ['Y', 'N'])->default('N');
            $table->enum('cron_flag', ['I', 'S', 'C'])->default('I')->comment('I: Initiated, S: Started, C: Completed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_stock');
    }
}
