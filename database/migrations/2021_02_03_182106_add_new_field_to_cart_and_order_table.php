<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFieldToCartAndOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cart', function (Blueprint $table) {
             $table->string('coupon_code')->nullable()->after('note');
             $table->string('coupon_type')->nullable()->after('coupon_code');
             $table->integer('coupon_value')->nullable()->after('coupon_type');
        });
        Schema::table('order', function (Blueprint $table) {
             $table->string('coupon_code')->nullable()->after('order_note');
             $table->string('coupon_type')->nullable()->after('coupon_code');
             $table->integer('coupon_value')->nullable()->after('coupon_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cart', function (Blueprint $table) {
            //
        });
    }
}
