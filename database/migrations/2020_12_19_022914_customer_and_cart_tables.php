<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CustomerAndCartTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('customer_branch', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('customer_id');
            $table->string('contact_name', 200)->nullable();
            $table->string('branch', 250);
            $table->string('phone', 15)->nullable();
            $table->string('email', 60)->nullable();
            $table->enum('is_fefault_branch', ['Y', 'N'])->default('N');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('customer_id')->references('id')->on('customer')->onUpdate('cascade')->onDelete('cascade');

        });

        Schema::table('address', function (Blueprint $table) {
            $table->unsignedBigInteger('branch_id')->nullable();
            $table->string('name', 200)->nullable();
            $table->string('landmark', 250)->nullable();
            $table->string('phone', 15)->nullable();
        });

        Schema::create('cart', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->bigInteger('customer_id')->nullable();
            $table->integer('item_count')->default(0);
            $table->decimal('grant_total',10,2)->default(0);
            $table->decimal('sub_total',10,2)->default(0);
            $table->decimal('tax_total',10,2)->default(0);
            $table->decimal('delivery_charge',10,2)->default(0);
            $table->enum('is_guest', ['Y', 'N'])->default('Y');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('cart_key', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->unsignedBigInteger('cart_id');
            $table->text('key');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('cart_id')->references('id')->on('cart')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('cart_items', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->unsignedBigInteger('cart_id');
            $table->unsignedBigInteger('product_id');
            $table->bigInteger('variant_id')->nullable();
            $table->integer('item_count')->default(0);
            $table->decimal('grant_total',10,2)->default(0);
            $table->decimal('sub_total',10,2)->default(0);
            $table->decimal('tax_total',10,2)->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('cart_id')->references('id')->on('cart')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('product')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('order', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->unsignedBigInteger('users_id');
            $table->unsignedBigInteger('customer_id');
            $table->string('order_id',200)->nullable();
            $table->integer('item_count')->nullable();
            $table->decimal('grant_total',10,2)->nullable();
            $table->decimal('sub_total',10,2)->nullable();
            $table->decimal('tax_total',10,2)->nullable();
            $table->decimal('delivery_charge',10,2)->nullable();
            $table->enum('payment_method',['credit','debit','cod','mada'])->nullable();
            $table->dateTime('delivery_schedule_date')->nullable();
            $table->text('delivery_loc_coordinates')->nullable();
            $table->text('delivery_loc')->nullable();
            $table->time('schedule_time_from')->nullable();
            $table->time('schedule_time_to')->nullable();
            $table->text('order_note')->nullable();
            $table->text('cancel_note')->nullable();
            $table->enum('status',['init','accept','reject','cancel','delivered'])->nullable();
            $table->string('delivery_name')->nullable();
            $table->text('delivery_address')->nullable();
            $table->string('delivery_city')->nullable();
            $table->string('delivery_country',150)->nullable();
            $table->string('delivery_zip',150)->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('users_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
             $table->foreign('customer_id')
                ->references('id')
                ->on('customer')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        Schema::create('order_items', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->unsignedBigInteger('order_id');
            $table->unsignedBigInteger('product_id');
            $table->integer('item_count')->nullable();
            $table->decimal('grant_total',10,2)->nullable();
            $table->decimal('sub_total',10,2)->nullable();
            $table->decimal('tax_total',10,2)->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('order_id')
                ->references('id')
                ->on('order')
                ->onUpdate('cascade')
                ->onDelete('cascade');
             $table->foreign('product_id')
                ->references('id')
                ->on('product')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
