<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromocodesCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promocodes_category', function (Blueprint $table) {
            $table->id();
            $table->engine = 'InnoDB';
            $table->unsignedInteger('promocodes_id');
            $table->unsignedInteger('category_id');
            $table->enum('type', ['exclude', 'include']);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('category_id')
                ->references('id')
                ->on('category')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('promocodes_id')
                ->references('id')
                ->on('promocodes')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promocodes_category');
    }
}
