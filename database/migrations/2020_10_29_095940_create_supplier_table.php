<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->unsignedBigInteger('region_id');
            $table->unsignedBigInteger('city_id');
            $table->unsignedBigInteger('role_id')->nullable();
            $table->string('name');
            $table->string('contact_person_name')->nullable();
            $table->string('email')->unique();
            $table->string('phone', 15)->nullable();
            $table->string('password');
            $table->string('ip_address', 60)->nullable();
            $table->dateTime('last_login_date')->nullable();
            $table->dateTime('last_activity_date')->nullable();
            $table->string('verification_code')->nullable();
            $table->string('code_generated_at')->nullable();
            $table->enum('status', ['active', 'deactive'])->default('deactive');
            $table->timestamps();
            $table->softDeletes();


            $table->foreign('region_id')
                ->references('id')
                ->on('region')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('city_id')
                ->references('id')
                ->on('city')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('role_id')
                ->references('id')
                ->on('user_roles')
                ->onUpdate('cascade')
                ->onDelete('set null');        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier');
    }
}
