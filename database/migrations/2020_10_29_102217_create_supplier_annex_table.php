<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplierAnnexTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_annex', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->unsignedBigInteger('supplier_id');
            $table->unsignedBigInteger('commision_category_id');
            $table->string('cr_copy', 45)->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('supplier_id')
                    ->references('id')
                    ->on('supplier')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->foreign('commision_category_id')
                    ->references('id')
                    ->on('commision_category')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_annex');
    }
}
