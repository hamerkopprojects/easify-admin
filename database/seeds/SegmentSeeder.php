<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SegmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rating_segments')->insert([
            [
                
                'status'=>'active',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'status'=>'active',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
        ]);
        DB::table('rating_segments_i18n')->insert([
            [
                'rating_segment_id' => '1',
                'name'=>'Product quality',
                 'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'rating_segment_id' => '1',
                'name'=>'جودة المنتج',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'rating_segment_id' => '2',
                'name'=>'On time delivery',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'rating_segment_id' => '2',
                'name'=>'التسليم في الوقت المحدد',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            
        ]);
    }
}
