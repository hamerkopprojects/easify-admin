<?php

use App\Ads;
use App\User;
use App\Slider;
use App\Models\Pages;
use App\Models\AppType;
use App\Models\HowtoUse;
use App\Models\PagesLang;
use App\Models\UserRoles;
use App\Models\BusinessType;
use App\Models\HowtoUseLang;
use App\Models\RatingSegments;
use Illuminate\Database\Seeder;
use App\Models\BusinessTypeLang;
use App\Models\RatingSegmentsLang;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        RatingSegments::truncate();
        RatingSegmentsLang::truncate();
        UserRoles::truncate();
        User::truncate();
        Pages::truncate();
        PagesLang::truncate();
        HowtoUse::truncate();
        HowtoUseLang::truncate();
        Slider::truncate();
        Ads::truncate();
        BusinessType::truncate();
        BusinessTypeLang::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
         $this->call(UserRoleSeeder::class);
         $this->call(UserSeeder::class);
         $this->call(SegmentSeeder::class);
         $this->call(UserAppType::class);
         $this->call(PagesTableSeeder::class);
         $this->call(HowToUseSeeder::class);
         $this->call(SliderTableSeeder::class);
          $this->call(AdvertisementsTableSeeder::class);
         $this->call(BusinessTypeSeeder::class);
    }
}
