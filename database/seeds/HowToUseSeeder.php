<?php

use Illuminate\Database\Seeder;

class HowToUseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     
        DB::table('how_to_use')->insert([
            [
              
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
              
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
              
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
              
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
              
                'created_at' => now(),
                'updated_at' => now()
            
            ],
        ]);
        DB::table('how_to_use_i18n')->insert([
            [
                'how_to_id' => '1',
                'title'=>'Question goes here',
                'content'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                            Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                            when an unknown printer took a galley of type and scrambled it to make a type 
                            specimen book. It has survived not only five centuries, but also the leap into electronic 
                            typesetting, remaining essentially unchanged',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'how_to_id' => '1',
                'title'=>'السؤال يذهب هنا',
                'content'=>' يستخدم في صناعة الطباعة والتنضيد. عندما أخذت طابعة غير معروفة لوحًا من النوع وتدافعت عليه لصنع نوع
                كتاب العينة. لقد نجت ليس فقط خمسة قرون ، ولكن أيضًا القفزة الإلكترونية',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'how_to_id' => '2',
                'title'=>'',
                'content'=>'',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'how_to_id' => '2',
                'title'=>'',
                'content'=>'',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'how_to_id' => '3',
                'title'=>'',
                'content'=>'',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'how_to_id' => '3',
                'title'=>'',
                'content'=>'',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'how_to_id' => '4',
                'title'=>'',
                'content'=>'',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'how_to_id' => '4',
                'title'=>'',
                'content'=>'',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'how_to_id' => '5',
                'title'=>'',
                'content'=>'',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'how_to_id' => '5',
                'title'=>'',
                'content'=>'',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
           
        ]);  
    }
}
