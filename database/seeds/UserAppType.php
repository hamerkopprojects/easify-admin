<?php

use App\Models\AppType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserAppType extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        AppType::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        DB::table('app_types')->insert([
            [
                'name' => 'Website',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Driver App',
                'created_at' => now(),
                'updated_at' => now()

            ],
        ]);
    }
}
