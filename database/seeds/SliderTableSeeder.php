<?php

use Illuminate\Database\Seeder;

class SliderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('slider')->insert([
            [
                'slider_type' => 'Slider 1',
                'created_at'=>now(),
            ],
            [
                'slider_type' => 'Slider 2',
                'created_at'=>now(),
            ],
            [
                'slider_type' => 'Slider 3',
                'created_at'=>now(),
            ],
            [
                'slider_type' => 'Slider 4',
                'created_at'=>now(),
            ],
            [
                'slider_type' => 'Slider 5',
                'created_at'=>now(),
            ],
            [
                'slider_type' => 'Slider 6',
                'created_at'=>now(),
            ],
        ]);
    }
}
