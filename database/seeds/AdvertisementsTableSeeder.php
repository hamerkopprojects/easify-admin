<?php

use Illuminate\Database\Seeder;

class AdvertisementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('advertisements')->truncate();
        DB::table('advertisements')->insert([
            [
                'ad_type' => 'Banner 1',
                'created_at'=>now(),
            ],
            [
                'ad_type' => 'Banner 2',
                'created_at'=>now(),
            ],
            [
                'ad_type' => 'Banner 3',
                'created_at'=>now(),
            ],
            [
                'ad_type' => 'Banner 4',
                'created_at'=>now(),
            ],
        ]);
    }
}
