<?php

use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        // DB::table('user_roles')->truncate();
        DB::table('user_roles')->insert([
            [
                'name' => 'Admin ',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Office staff ',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Driver ',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Warehouse ',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Supplier ',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'Branch ',
                'created_at' => now(),
                'updated_at' => now()
            ],
        ]);
    }

}
