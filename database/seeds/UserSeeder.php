<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        // DB::table('users')->truncate();
            DB::table('users')->insert([
                'name' => 'Admin',
                'email' => 'admin@easify.com',
                'status' =>'active' ,
                'phone'=>'',
                'user_id'=>'',
                'role_id'=>'1',
                'email_verified_at' => now(),
                'password' => Hash::make('123456'),
                'created_at' => now(),
                'updated_at' => now()
            ]);
      
    }
}
