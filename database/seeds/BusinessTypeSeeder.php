<?php

use App\Models\BusinessType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BusinessTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        BusinessType::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        DB::table('bussiness_type')->insert([
            [
                
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'created_at' => now(),
                'updated_at' => now()
            
            ],
        ]);
        DB::table('bussiness_type_i18n')->insert([
            [
                'bussiness_type_id' => '1',
                'name'=>'Restaurant',
                 'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'bussiness_type_id' => '1',
                'name'=>'مطعم',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'bussiness_type_id' => '2',
                'name'=>'Shop',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'bussiness_type_id' => '2',
                'name'=>'الفندق',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ]
        ]);
    }
}
