<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('pages_i18n')->truncate();
        DB::table('pages')->truncate();
        DB::table('pages')->insert([
            [
                'type' => '1',
                'available_for'=>'Website',
                'slug'=>'about-us',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'type' => '1',
                'available_for'=>'Driverapp',
                'slug'=>'about-us',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'type' => '1',
                'available_for'=>'Website',
                'slug'=>'terms-and-conditions',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'type' => '1',
                'available_for'=>'Website',
                'slug'=>'privacy-policy',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'type' => '1',
                'available_for'=>'Website',
                'slug'=>'return-policy',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'type' => '1',
                'available_for'=>'Website',
                'slug'=>'warranty-policy',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
        ]);
        DB::table('pages_i18n')->insert([
            [
                'page_id' => '1',
                'title'=>'About as',
                'content'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                            Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                            when an unknown printer took a galley of type and scrambled it to make a type 
                            specimen book. It has survived not only five centuries, but also the leap into electronic 
                            typesetting, remaining essentially unchanged',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'page_id' => '1',
                'title'=>'النوع وتدافعت عليه لصنع',
                'content'=>' يستخدم في صناعة الطباعة والتنضيد. عندما أخذت طابعة غير معروفة لوحًا من النوع وتدافعت عليه لصنع نوع
                كتاب العينة. لقد نجت ليس فقط خمسة قرون ، ولكن أيضًا القفزة الإلكترونية',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'page_id' => '2',
                'title'=>'Terms and condition',
                'content'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                when an unknown printer took',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'page_id' => '2',
                'title'=>'النوع وتدافعت عليه لصنع',
                'content'=>' يستخدم في صناعة الطباعة والتنضيد. عندما أخذت طابعة غير معروفة لوحًا من النوع وتدافعت عليه لصنع نوع
                كتاب العينة. لقد نجت ليس فقط خمسة قرون ، ولكن أيضًا القفزة الإلكترونية',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'page_id' => '3',
                'title'=>'Terms and condition',
                'content'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                when an unknown printer took',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'page_id' => '3',
                'title'=>'النوع وتدافعت عليه لصنع',
                'content'=>' يستخدم في صناعة الطباعة والتنضيد. عندما أخذت طابعة غير معروفة لوحًا من النوع وتدافعت عليه لصنع نوع
                كتاب العينة. لقد نجت ليس فقط خمسة قرون ، ولكن أيضًا القفزة الإلكترونية',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'page_id' => '4',
                'title'=>'Privacy Policy',
                'content'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                when an unknown printer took',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'page_id' => '4',
                'title'=>'النوع وتدافعت عليه لصنع',
                'content'=>' يستخدم في صناعة الطباعة والتنضيد. عندما أخذت طابعة غير معروفة لوحًا من النوع وتدافعت عليه لصنع نوع
                كتاب العينة. لقد نجت ليس فقط خمسة قرون ، ولكن أيضًا القفزة الإلكترونية',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'page_id' => '5',
                'title'=>'Retun Policy',
                'content'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                when an unknown printer took',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'page_id' => '5',
                'title'=>'النوع وتدافعت عليه لصنع',
                'content'=>' يستخدم في صناعة الطباعة والتنضيد. عندما أخذت طابعة غير معروفة لوحًا من النوع وتدافعت عليه لصنع نوع
                كتاب العينة. لقد نجت ليس فقط خمسة قرون ، ولكن أيضًا القفزة الإلكترونية',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'page_id' => '6',
                'title'=>'Warranty Policy',
                'content'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                Lorem Ipsum has been the industry standard dummy text ever since the 1500s, 
                when an unknown printer took',
                'language'=>'en',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
            [
                'page_id' => '6',
                'title'=>'النوع وتدافعت عليه لصنع',
                'content'=>' يستخدم في صناعة الطباعة والتنضيد. عندما أخذت طابعة غير معروفة لوحًا من النوع وتدافعت عليه لصنع نوع
                كتاب العينة. لقد نجت ليس فقط خمسة قرون ، ولكن أيضًا القفزة الإلكترونية',
                'language'=>'ar',
                'created_at' => now(),
                'updated_at' => now()
            
            ],
        ]);

    }
}
