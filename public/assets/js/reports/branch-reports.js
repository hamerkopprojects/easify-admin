$(document).ready(function () {
  // Start PieChart
  function drawPieChart(pieobject) {
    let donut = Morris.Donut({
      element: pieobject.pie_element,
      data: pieobject.data,
      formatter: function (value, data) {
        var formatter = new Intl.NumberFormat('en-US', {
         style: 'currency',
         currency: 'SAR',
        });
        if(!isNaN(value)){
         value = formatter.format(value);
        }
        return value;
      }
    });
    donut.options.data.forEach(function (label, i) {
     var formatter = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'SAR',
     });
     var tempval = label['value'];
     if(!isNaN(label['value'])){
      label['value'] = formatter.format(label['value']);
     }
      let legendItem = $('<span></span>').text(`${label['label']}`).prepend('<br><span>&nbsp;</span>');
      legendItem.find('span')
        .css('backgroundColor', donut.options.colors[i])
        .css('width', '20px')
        .css('display', 'inline-block')
        .css('margin', '5px');
      $(pieobject.pie_legend_element).append(legendItem);
      label['value'] = tempval;
    });

  }
  // End Piechart


  // Start line graph
  function drawLineGraph(lineObject) {
    let line = Morris.Line({
      // ID of the element in which to draw the chart.
      element: lineObject.element,
      // Chart data records -- each entry in this array corresponds to a point on
      // the chart.
      data: lineObject.data,
      resize: true,
      // The name of the data record attribute that contains x-values.
      xkey: lineObject.xkey,
      // A list of names of data record attributes that contain y-values.
      ykeys: lineObject.ykeys,
      // Labels for the ykeys -- will be displayed when you hover over the
      // chart.
      labels: lineObject.ykeys,
      //lineColors: lineObject.colors
      hideHover: false,
      xLabelAngle: '90',
      padding: 70,
      parseTime: false,
      hoverCallback: function (index, options, content, row) {
        var formatter = new Intl.NumberFormat('en-US', {
          style: 'currency',
          currency: 'SAR',
        });
        let hover_string = '';
        lineObject.ykeys.forEach((key, index) => 
        {
          if(!isNaN(row[key])){
            row[key] = formatter.format(row[key]);
          }
          hover_string += `<div class='morris-hover-point' style='color:${options.lineColors[index]} !important;'>${row[key]}</div>`;
        });
        return hover_string;
      },
    });

    line.options.labels.forEach(function (label, i) {
      var legendlabel = $('<span style="display:inline-block;width:170px">&nbsp;&nbsp;' + label + '</span>')
      var legendItem = $('<div class="mbox"></div>').css('background-color', line.options.lineColors[i]).append(legendlabel)
      $(lineObject.legend_element).append(legendItem)
    })
  }
  // End line graph
  
    // Start bar graph
  function drawBarGraph(barObject) {
    console.log(barObject);
    Morris.Bar({
      element: barObject.element,
      resize: true,
      data: barObject.data,
      xkey: 'y',
      xLabelAngle: '90',
      padding: 70,
      ykeys: barObject.ykeys,
      labels: ['Sales'],
      hideHover: false,
      hoverCallback: function (index, options, content, row) {
        var formatter = new Intl.NumberFormat('en-US', {
          style: 'currency',
          currency: 'AED',
        });
        let hover_string = '';
        barObject.ykeys.forEach((key, index) => 
        {
          if(!isNaN(row[key])){
            row[key] = formatter.format(row[key]);
          }
          hover_string += `<div class='morris-hover-point' style='color:${options.barColors[index]} !important;'>${row[key]}</div>`;
        });
        return hover_string;
      },
    });
  }
  // End bar graph

// Start Select 2 boxes - category and diet
  $(".select-2-filter_category").select2({
    placeholder: "All"
  });
  $(".select-2-filter_brand").select2({
    placeholder: "All"
  });
  
 // End  

 // Start DateRange picker
  function dateRangePicker(params) {
    let start = params.start;

    function callback(start) {
      $(params.elementSpan).html(start.format('MMMM D, YYYY'));
    }
    // single date picker
    if (params.single == true) {
      $(params.element).daterangepicker({
        singleDatePicker: true,
        maxDate: moment(),
        ranges: params.ranges
      }, callback);
      callback(start)

    } else { // date range picker
      let start = params.start;
      let end = params.end;

      function callback(start, end) {
        $(params.elementSpan).html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }

      $(params.element).daterangepicker({
        startDate: start,
        endDate: end,
        maxDate: moment(),
        dateLimit: {
          days: params.dateLimit
        },
        ranges: params.ranges
      }, callback);
      callback(start, end)

    }

  }

// start -  sales date range picker
  dateRangePicker({
    start: moment().subtract(29, 'days'),
    end: moment(),
    elementSpan: '#daterange-sales span',
    element: '#daterange-sales',
    dateLimit: 180,
    single: false,
    ranges: {
      'Today': [moment(), moment()],
      'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Last 7 Days': [moment().subtract(6, 'days'), moment()],
      'Last 30 Days': [moment().subtract(29, 'days'), moment()],
      'This Month': [moment().startOf('month'), moment().endOf('month')],
      'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
      'Last 3 Months': [moment().subtract(3, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
      'Last 6 Months': [moment().subtract(6, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }
  });
  
  $("#search_filter").click(function () {
    $('.loading_box').show();
    $('.loading_box_overlay').show();
    var start_date = $('#daterange-sales').data('daterangepicker').startDate.format('YYYY-MM-DD');
    var end_date = $('#daterange-sales').data('daterangepicker').endDate.format('YYYY-MM-DD');
    var formData = $('#frm_report_filter').serialize();
    $.ajax({
    type: "POST",
    url: "reports/salestab",
    data: formData+ "&start_date=" + start_date+"&end_date=" + end_date,
    dataType: "json",
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(data) {
        promiseThenSales(data);
    }
    })
  });
  
  
  
  function promiseThenSales(data) {
    let top5_category_html = Object.keys(data.top_categories).length === 0 ? '<tr><td colspan="8" class="text-center">No records found!</td></tr>' : '';
    let top5_brands_html = Object.keys(data.top_brands).length === 0 ? '<tr><td colspan="8" class="text-center">No records found!</td></tr>' : '';
    let top5_product_html = Object.keys(data.top_products).length === 0 ? '<tr><td colspan="8" class="text-center">No records found!</td></tr>' : '';
    let total_sales = data.total_sale === 0 ? 'SAR 0.00' : 'SAR ' + data.total_sale;
    let total_orders = data.total_orders === 0 ? '0' : data.total_orders;
    let total_customers = data.total_customers === 0 ? '0' : data.total_customers;

    // start - clearing the pie chart and its legends for regeneration
    $("#category-pie").empty();
    $("#brand-pie").empty();
    $("#category-pie-legend").html('');
    $("#brand-pie-legend").html('');
    // end - clearing the pie chart and its legends for regeneration

    // start - total sales
     $("#total_sales").text(total_sales);
    // end - total sales
    
     $("#total_orders").text(total_orders);
     
     $("#total_customers").text(total_customers);

    // start - populating html for top 5 category
    data.top_categories.forEach((item) => {
      top5_category_html += `<tr><td>${item.label}</td><td>${item.sale_count}</td><td class="cls_last_child"><span class="cl-currency">SAR </span>${item.value}</td></tr>`
    });
    $("#top-categories").html(top5_category_html);
    // end - populating html for top 5 category
     
    // start - populating html for top 5 brands
    data.top_brands.forEach((item) => {
      top5_brands_html += `<tr><td>${item.label}</td><td>${item.sale_count}</td><td class="cls_last_child"><span class="cl-currency">SAR </span>${item.value}</td></tr>`
    });
    $("#top-brands").html(top5_brands_html);
    // end - populating html for top 5 brands
     
    // start - populating html for top 5 products
    data.top_products.forEach((item) => {
      top5_product_html += `<tr><td>${item.label}</td><td>${item.sale_count}</td><td class="cls_last_child"><span class="cl-currency">SAR </span>${item.value}</td></tr>`
    });
    $("#top-products").html(top5_product_html);
    // end - populating html for top 5 products 
     
    // start - creating pie chart
    Object.keys(data.all_categories).length === 0 ? $("#category-pie").html("<p class='mt-3 text-center'>No Data Available</p>") : drawPieChart({
      data: data.all_categories,
      pie_element: 'category-pie',
      pie_legend_element: '#category-pie-legend'
    });
    // end - creating pie chart
    
    // start - creating pie chart
    Object.keys(data.all_brands).length === 0 ? $("#brand-pie").html("<p class='mt-3 text-center'>No Data Available</p>") : drawPieChart({
      data: data.all_brands,
      pie_element: 'brand-pie',
      pie_legend_element: '#brand-pie-legend'
    });
    // end - creating pie chart
    
     $('.loading_box').hide();
     $('.loading_box_overlay').hide();
  }
  
  
  $("#linechart_filter").click(function () {
    $('.loading_box').show();
    $('.loading_box_overlay').show();
    var start_date = $('#daterange-sales').data('daterangepicker').startDate.format('YYYY-MM-DD');
    var end_date = $('#daterange-sales').data('daterangepicker').endDate.format('YYYY-MM-DD');
    var formData = $('#frm_line_chart_filter').serialize();
    $.ajax({
    type: "POST",
    url: `${base_url}/branch/reports/linecharttab`,
    data: formData+ "&start_date=" + start_date+"&end_date=" + end_date,
    dataType: "json",
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(data) {
        promiseThenSalesLine(data);
    }
    })
  });
  
  function promiseThenSalesLine(data) {
    let total_sales = data.total_sale === 0 ? 'SAR 0.00' : 'SAR ' + data.total_sale;
    let total_orders = data.total_orders === 0 ? '0' : data.total_orders;
    let total_customers = data.total_customers === 0 ? '0' : data.total_customers;
    
    // start - clearing the pie chart and its legends for regeneration
    $("#saleslinegraph").empty();
    $("#sales-linegraph-legend").html('');
    // end - clearing the pie chart and its legends for regeneration

    // start - total sales
    $("#total_sales").text(total_sales);
    // end - total sales
    
    $("#total_orders").text(total_orders);
    
    $("#total_customers").text(total_customers);

    // start - creating line graph
    Object.keys(data.linegraph_data).length === 0 ? $("#saleslinegraph").html("No Data Available") : drawLineGraph({
      data: data.linegraph_data,
      element: 'saleslinegraph',
      legend_element: '#sales-linegraph-legend',
      xkey: 'day',
      ykeys: data.linegraph_ykeys,
      labels: data.linegraph_ykeys,
    });
    if($("#saleslinegraph").html() == "No Data Available") {
        $("#saleslinegraph").addClass('p-t-25');
        $("#saleslinegraph").removeClass('p-t-140');
    } else {
        $("#saleslinegraph").addClass('p-t-140');
        $("#saleslinegraph").removeClass('p-t-25');
    }

    // end - creating line graph

     $('.loading_box').hide();
     $('.loading_box_overlay').hide();
  }
  
  $("#hourly_filter").click(function () {
    $('.loading_box').show();
    $('.loading_box_overlay').show();
    var start_date = $('#daterange-sales').data('daterangepicker').startDate.format('YYYY-MM-DD');
    var end_date = $('#daterange-sales').data('daterangepicker').endDate.format('YYYY-MM-DD');
    var formData = $('#frm_line_chart_filter').serialize();
    $.ajax({
    type: "POST",
    url: `${base_url}/branch/reports/hourlytab`,
    data: formData+ "&start_date=" + start_date+"&end_date=" + end_date,
    dataType: "json",
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function(data) {
       promiseThenSalesHourly(data);
    }
    })
  });
  
  // start - promise callback for hourlysales tab
  function promiseThenSalesHourly(data) {
    let total_sales = data.total_sale === 0 ? 'SAR 0.00' : 'SAR ' + data.total_sale;
    let total_orders = data.total_orders === 0 ? '0' : data.total_orders;
    let total_customers = data.total_customers === 0 ? '0' : data.total_customers;

    // start - clearing the pie chart and its legends for regeneration
    $("#hourlysaleslinegraph").empty();
    // end - clearing the pie chart and its legends for regeneration

     // start - total sales
    $("#total_sales").text(total_sales);
    // end - total sales
    
    $("#total_orders").text(total_orders);
    
    $("#total_customers").text(total_customers);

    // start - creating line graph
    Object.keys(data.hourly_data).length === 0 ? $("#hourlysaleslinegraph").html("No Data Available") : drawLineGraph({
      data: data.hourly_data,
      element: 'hourlysaleslinegraph',
      legend_element: '#hourlysaleslinegraph-legend',
      xkey: 'time',
      ykeys: ['sales'],
      labels: 'sales'
    });

    // end - creating line graph

     $('.loading_box').hide();
     $('.loading_box_overlay').hide();
  }

  // end - promise callback for hourlysales tab
});