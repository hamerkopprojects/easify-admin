<?php

return [
    'container_type' => [
        'C' => 'Count',
        'Q' => 'Quantity',
        'N' => 'Nos',
    ],
    'schedule_time' => [
        '1' => '9 AM to 1 PM',
        '2' => '2 PM to 5 PM',
    ],
    'order_status' => [
        1 => "Waiting for supplier action",
        2 => "Ready for collection",  
        3 => "Collection pending",
        4 => "Collection started",
        5 => "Collection completed",
        6 => "Cancelled",
        7 => "Ready for delivery",
        8 => "Cancellation request",
        9 => "Delivered"
    ],
    'supplier_status' => [
        1 => "Pending",
        2 => "Accepted",
        3 => "Collection pending",
        4 => "Collection completed",
        5 => "Cancelled"
    ],
    'web_order_status' => [
        1 => "Order received",
        2 => "Order preparation",
        3 => "Ready for delivery",
        4 => "Delivered"
        
    ],
    'warehouse_shelfs'=>[
        1 => "Shelf-1",
        2 => "Shelf-2",
        3 => "Shelf-3",
        4 => "Shelf-4",
        5 => "Shelf-5"
    ],
    'driver_collection_status'=>[
        1 =>'Collection_pending',
        2 =>'collection_completed',
        3 =>'Collection_started',
        4 =>'cancelled'
    ],
    'product_status'=>[
        1 =>'Pending',
        2 =>'Accepted',
        3 =>'Rejected',
        4 =>'Collection pending',
        5 =>'Collected',
        6 => 'At warehouse'

    ]

];
