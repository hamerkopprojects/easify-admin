<?php

return [
    'driver' => env('FCM_PROTOCOL', 'http'),
    'log_enabled' => false,

    'http' => [
        'server_key' => env('FCM_SERVER_KEY', 'AAAAbPMGjfQ:APA91bFbCvM3NR6DgFWt_U_KfF5cf8YFmW5016eu0Z7QIolt6G3i0uCQ7VjQUBIx8Yk4NiCasgvq3909hwAfH52UUE3IRF3RtJKv7ThAgk-cTzl80VKcDXiwBO69FZxaUUZRx8FzfGeG'),
        'sender_id' => env('FCM_SENDER_ID', '467933761012'),
        'server_send_url' => 'https://fcm.googleapis.com/fcm/send',
        'server_group_url' => 'https://android.googleapis.com/gcm/notification',
        'timeout' => 30.0, // in second
    ],
];
