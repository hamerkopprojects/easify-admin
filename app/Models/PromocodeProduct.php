<?php

namespace App\Models;

use App\Models\ProductLang;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PromocodeProduct extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */
    

    protected $table = "promocodes_product";

    public function promotionalproduct()
    {
        return $this->hasOne(ProductLang::class, 'product_id','product_id');
    }
}
