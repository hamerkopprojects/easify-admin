<?php

namespace App\Models;

use App\Models\Customer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Region extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
        protected $guarded = [];
        /**
         * $table variable
         *
         * @var string
         */
    
        protected $table="region";
       
        public function lang()
        {
            return $this->hasMany(RegionLang::class, 'region_id');
        }
        public function city()
        {
            return $this->hasMany(CityLang::class, 'city_id','city_id');
        }
        public function customer()
        {
            return $this->hasMany(Customer::class,'region');
        }
}
