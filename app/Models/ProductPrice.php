<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductPrice extends Model
{

    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "product_price";


    public function attribute()
    {
        return $this->belongsTo('App\Models\Attribute', 'attribute_id', 'id')->with('lang');
    }
    public function variant()
    {
        return $this->hasOne('App\Models\VariantLang', 'id', 'variant_id');
    }
    public function product()
    {
     return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }
    public function variant_lang()
    {
        return $this->hasOne('App\Models\VariantLang', 'id', 'variant_lang_id');
    }
}
