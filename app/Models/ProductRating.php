<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductRating extends Model
{

    protected $table = 'product_rating';

    protected $guarded = [];
    
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }
}
