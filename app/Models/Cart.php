<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cart extends Model {

    use SoftDeletes;

    /**
     * guarded variable

     * @var array
     */
    protected $guarded = [];

    /**
     * $table variable
     *
     * @var string
     */
    protected $table = "cart";

    public function items() {
        return $this->hasMany(CartItems::class, 'cart_id');
    }
}