<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttributeLang extends Model
{
    /**
    * guarded variable
    *
    * @var array
    */

    protected $guarded = [];
    
    /**
    * $table variable
    *
    * @var string
    */

    protected $table="attribute_i18n";

}
