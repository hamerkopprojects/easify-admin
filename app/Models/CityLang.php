<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CityLang extends Model
{
    use  SoftDeletes;
    /**
    * guarded variable
    *
    * @var array
    */

    protected $guarded = [];
    
    /**
    * $table variable
    *
    * @var string
    */

    protected $table="city_i18n";
}
