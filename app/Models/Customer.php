<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
   

    protected $table = 'customer';

    protected $guarded = [];
    
    public function customer_region()
    {
     return $this->belongsTo('App\Models\Region', 'region')->with('lang')->where('deleted_at', NULL);
    }
    
    public function customer_city()
    {
     return $this->belongsTo('App\Models\City', 'city')->with('lang')->where('deleted_at', NULL);
    }
    public function branch()
    {
        return $this->hasMany(CustomerBranch::class,'created_by');

    }
}
