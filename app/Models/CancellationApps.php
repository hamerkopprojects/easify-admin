<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CancellationApps extends Model
{
    
    /**
     * guarded variable
     *
     * @var array
     */
        protected $guarded = [];
        /**
         * $table variable
         *
         * @var string
         */
    
        protected $table="cancellation_apps";
       
}
