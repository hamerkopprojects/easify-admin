<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;
class CategoryAttributes extends Model
{
    /**
     * guarded variable
     
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */
    

    protected $table = "category_attributes";

    public function attributes()
    {
        return $this->hasOne(AttributeLang::class, 'attribute_id','attribute_id');
    }
    
    public function cat_attributes()
    {
        return $this->belongsTo(Attribute::class, 'attribute_id')->with('lang')->where('deleted_at', NULL);
    }
}
