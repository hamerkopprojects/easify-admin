<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Variant extends Model
{
    use  SoftDeletes;
/**
 * guarded variable
 *
 * @var array
 */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table="variant";
   
    public function lang()
    {
        return $this->hasMany(VariantLang::class, 'variant_id');
    }
    
    public function b2bprice()
    {
        return $this->hasMany(ProductB2BPrice::class, 'variant_id');
    }
}
