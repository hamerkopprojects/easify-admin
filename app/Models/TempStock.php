<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TempStock extends Model
{

    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */
    
    protected $table="temp_stock";
}
