<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OtherSettings extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
        protected $guarded = [];
        /**
         * $table variable
         *
         * @var string
         */
    
        protected $table="other_settings";
}
