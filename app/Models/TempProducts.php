<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TempProducts extends Model
{

    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table="temp_supplier_products";
}
