<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use  SoftDeletes;
/**
 * guarded variable
 *
 * @var array
 */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table="product";
   
    public function lang()
    {
        return $this->hasMany(ProductLang::class, 'product_id');
    }
    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id')->with('lang')->where('deleted_at', NULL);
    }
    public function brand()
    {
        return $this->belongsTo('App\Models\Brand', 'brand_id')->with('lang')->where('deleted_at', NULL);
    }
    public function variant_price()
    {
        return $this->hasMany('App\Models\ProductPrice')->where('deleted_at', NULL);
    }
    public function price()
    {
        return $this->hasMany('App\Models\ProductPrice', 'product_id');
    }
    public function photos()
    {
        return $this->hasMany('App\Models\ProductImages', 'product_id');
    }
     public function product_attribute()
    {
     return $this->hasMany('App\Models\ProductAttribute', 'product_id', 'id');
    }
    public function supplier()
    {
        return $this->belongsTo('App\Models\Supplier', 'supplier_id');
    }
    public function variant()
    {
        return $this->hasMany('App\Models\ProductVariant')->where('deleted_at', NULL);
    }
    
    public function commission()
    {
        return $this->belongsTo('App\Models\CommisionCategory', 'commision_percentage');
    }
    public function reviews()
    {
        return $this->hasMany('App\Models\ProductRating','product_id', 'id');
    }
    
}
