<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RegionLang extends Model
{
    use  SoftDeletes;
    /**
    * guarded variable
    *
    * @var array
    */

    protected $guarded = [];
    
    /**
    * $table variable
    *
    * @var string
    */

    protected $table="region_i18n";
}
