<?php

namespace App\Models;

use App\User;
use App\Address;
use App\Models\Customer;
use App\Models\CancellationReason;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    protected $table = 'order';

    protected $guarded = [];

    public function product()
    {
        return $this->belongsToMany(Product::class,'order_items','order_id','product_id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class,'customer_id');
    }

    public function variant()
    {
        return $this->belongsTo(Variant::class,'variant_id');
    }
    
    public function items() {
        return $this->hasMany(OrderItems::class, 'order_id');
    }
    
    public function customer_address()
    {
        return $this->hasMany(Address::class, 'cust_id', 'customer_id');
    }
    public function driver()
    {
        return $this->belongsTo(User::class,'driver_id');
    }
    public function owner()
    {
        return $this->morphTo(__FUNCTION__, 'updated_by', 'updated_by_id');
    }
    public function cancellation()
    {
        return $this->belongsTo(CancellationReason::class,'cancel_reason_id');
    }
}
