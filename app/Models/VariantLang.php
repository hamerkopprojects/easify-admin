<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VariantLang extends Model
{
    /**
    * guarded variable
    *
    * @var array
    */

    protected $guarded = [];
    
    /**
    * $table variable
    *
    * @var string
    */

    protected $table="variant_i18n";

}
