<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductVariant extends Model
{
    use  SoftDeletes;
/**
 * guarded variable
 *
 * @var array
 */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table="product_variant_stock";

    public function attribute()
    {
     return $this->belongsTo('App\Models\Attribute', 'attribute_id', 'id')->with('lang');
    }
    public function variant()
    {
     return $this->hasOne('App\Models\VariantLang', 'id', 'variant_lang_id');
    }
    public function price()
    {
     return $this->hasOne('App\Models\ProductPrice', 'product_id', 'product_id');
    }
    
     public function pdt_branch()
    {
     return $this->hasOne('App\Models\ProductBranchStock', 'product_variant_stock_id');
    }
   
}
