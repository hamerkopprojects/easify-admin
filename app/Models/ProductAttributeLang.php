<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductAttributeLang extends Model
{
    /**
    * guarded variable
    *
    * @var array
    */

    protected $guarded = [];
    
    /**
    * $table variable
    *
    * @var string
    */

    protected $table="product_attribute_i18n";

}
