<?php

namespace App\Models;

use App\Models\Customer;
use App\Models\ReviewSegments;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table="review";
    
    public function reviewSegments()
    {
        return $this->hasMany(ReviewSegments::class, 'review_id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class,'customer_id');
    }
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }
   
}
