<?php

namespace App\Models;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends  Authenticatable
{
  
    use  SoftDeletes;
    /**
    * guarded variable
    *
    * @var array
    */

    protected $guarded = [];
    
    /**
    * $table variable
    *
    * @var string
    */

    protected $table="supplier";

    public function region()
    {
        return $this->hasOne(RegionLang::class, 'region_id','region_id');
    }
     public function branch()
    {
        return $this->belongsTo(BranchAnnex::class, 'supplier_parent_id');
    }

    public function brand()
    {
        return $this->belongsToMany(Brand::class, 'supplier_brand', 'supplier_id', 'brand_id');
    }
    public function category()
    {
        return $this->belongsToMany(Category::class, 'supplier_category', 'supplier_id', 'category_id');
    }
    public function city()
    {
        return $this->hasOne(CityLang::class, 'city_id','city_id');
    }
    
    public function supplier_annex()
    {
        return $this->hasOne(SupplierAnnex::class, 'supplier_id');
    }

    public function supplier_region()
    {
        return $this->belongsTo(Region::class,'region_id');
    }
    
    public function supplier_branch()
    {
        return $this->hasOne(BranchAnnex::class, 'supplier_id');
    }
}
