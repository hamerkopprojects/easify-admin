<?php

namespace App\Models;

use App\Models\PromocodeProduct;
use App\Models\PromocodeCategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promocode extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */
    

    protected $table = "promocodes";

    public function promocategory()
    {
        return $this->hasMany(PromocodeCategory::class, 'promocodes_id');
    }
    public function promoproduct()
    {
        return $this->hasMany(PromocodeProduct::class, 'promocodes_id');
    }
}
