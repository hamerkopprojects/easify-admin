<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FaqsLang extends Model
{
    use SoftDeletes;
        /*
* guarded variable
 *
 * @var array
 */
protected $guarded = [];
/**
 * $table variable
 *
 * @var string
 */

protected  $table="faq_i18n";
}
