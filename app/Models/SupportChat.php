<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupportChat extends Model
{
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */
    
    protected $table="support_chat";
}
