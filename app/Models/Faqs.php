<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faqs extends Model
{
       
    use  SoftDeletes;
/**
 * guarded variable
 *
 * @var array
 */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table="faq";
   
    public function lang()
    {
        return $this->hasMany(FaqsLang::class, 'faq_id');
    }
    public function appType()
    {
        return $this->belongsToMany (AppType::class, 'faq_apps','faq_id','app_id');
    }
}
