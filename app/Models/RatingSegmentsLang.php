<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RatingSegmentsLang extends Model
{
    /**
    * guarded variable
    *
    * @var array
    */

    protected $guarded = [];
    
    /**
    * $table variable
    *
    * @var string
    */

    protected $table="rating_segments_i18n";

}
