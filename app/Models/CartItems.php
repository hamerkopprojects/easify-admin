<?php

namespace App\Models;

use B2bCart;
use B2bCartKey;
use App\Models\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class CartItems extends Model {

    use SoftDeletes;

    /**
     * guarded variable

     * @var array
     */
    protected $guarded = [];

    /**
     * $table variable
     *
     * @var string
     */
    protected $table = "cart_items";

    public function parent() {
        return $this->belongsTo(Cart::class, 'cart_id');
    }

    public function product() {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    public function variant() {
        return $this->belongsTo('App\Models\VariantLang');
    }

}