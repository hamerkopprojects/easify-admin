<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CartKey extends Model {

    use SoftDeletes;

    /**
     * guarded variable

     * @var array
     */
    protected $guarded = [];

    /**
     * $table variable
     *
     * @var string
     */
    protected $table = "cart_key";

    public function parent() {
        return $this->belongsTo(Cart::class, 'cart_id');
    }
}