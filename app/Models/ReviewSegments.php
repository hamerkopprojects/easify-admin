<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReviewSegments extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'review_segments';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function segment()
    {
        return $this->belongsTo(RatingSegments::class, 'segment_id');
    }
}
