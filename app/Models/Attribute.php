<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attribute extends Model
{
    use  SoftDeletes;
/**
 * guarded variable
 *
 * @var array
 */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table="attribute";
   
    public function lang()
    {
        return $this->hasMany(AttributeLang::class, 'attribute_id');
    }
    
    public function variant(){
        
        return $this->hasOne(Variant::class, 'attribute_id');
    }
    
    public function catattributes()
    {
     return $this->belongsTo('App\Models\CategoryAttributes', 'attribute_id', 'id');
    }
    
    public function cat_variant()
    {
        return $this->hasMany('App\Models\VariantLang', 'attribute_id');
    }
}
