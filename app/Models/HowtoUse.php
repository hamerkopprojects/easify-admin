<?php

namespace App\Models;

use App\Models\HowtoUseLang;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HowtoUse extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
        protected $guarded = [];
        /**
         * $table variable
         *
         * @var string
         */
    
        protected $table="how_to_use";
       
        public function lang()
        {
            return $this->hasMany(HowtoUseLang::class, 'how_to_id');
        }
}
