<?php

namespace App\Models;

use App\Models\CategoryLang;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PromocodeCategory extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */
    

    protected $table = "promocodes_category";

    public function promotionalcategory()
    {
        return $this->hasOne(CategoryLang::class, 'category_id','category_id');
    }
}
