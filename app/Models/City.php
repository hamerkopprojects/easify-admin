<?php

namespace App\Models;

use App\Customer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
        protected $guarded = [];
        /**
         * $table variable
         *
         * @var string
         */
    
        protected $table="city";
       
        public function lang()
        {
            return $this->hasMany(CityLang::class, 'city_id');
        }
}
