<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pages extends Model
{
    
    use  SoftDeletes;
/**
 * guarded variable
 *
 * @var array
 */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table="pages";
   
    public function lang()
    {
        return $this->hasMany(PagesLang::class, 'page_id');
    }
}
