<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductBranchStock extends Model
{
//    use  SoftDeletes;
/**
 * guarded variable
 *
 * @var array
 */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table="product_branch_stock";
    
     public function pdt_variant()
    {
     return $this->belongsTo('App\Models\ProductVariant', 'product_variant_stock_id');
    }
   
}
