<?php

namespace App\Models;

use App\User;
use App\Models\Supplier;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderItems extends Model
{
    use SoftDeletes;

    protected $table = 'order_items';

    protected $guarded = [];
    
    public function product() {
        return $this->belongsTo('App\Models\Product', 'product_id')->with('lang')->where('deleted_at', NULL);
    }

    public function variant() {
        return $this->belongsTo('App\Models\VariantLang');
    }
    public  function supplier()
    {
        return $this->belongsTo(Supplier::class,'supplier_id');
    }
    public function driver()
    {
        return $this->belongsTo(User::class,'driver_id');
    }
    public  function branch()
    {
        return $this->belongsTo(Supplier::class,'branch_id');
    }
}
