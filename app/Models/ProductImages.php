<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductImages extends Model
{
    
    /**
     * guarded variable
     *
     * @var array
     */
        protected $guarded = [];
        /**
         * $table variable
         *
         * @var string
         */
    
        protected $table="product_images";

//        public function getPathAttribute($path)
//        {
//            if(auth()->user()->role_id !== 1 )
//            {
//                return  asset(url('uploads/'.$path));
//            }else{
//                return $path;
//            }
//        }
        
}
