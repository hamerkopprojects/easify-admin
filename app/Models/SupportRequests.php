<?php

namespace App\Models;

use App\Models\SupportChat;
use Illuminate\Database\Eloquent\Model;

class SupportRequests extends Model
{
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */
    
    protected $table="support_requests";

    public function chats()
    {
        return $this->hasMany(SupportChat::class, 'request_id');
    }

    public function owner()
    {
        return $this->morphTo(__FUNCTION__, 'app_type', 'submitted_by');
    }
}
