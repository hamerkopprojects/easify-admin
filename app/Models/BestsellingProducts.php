<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BestsellingProducts extends Model
{
    use SoftDeletes;

    /**
     * guarded variable

     * @var array
     */
    protected $guarded = [];

    /**
     * $table variable
     *
     * @var string
     */
    protected $table = "best_selling_products";

    
        public function product() {
            return $this->belongsTo('App\Models\Product', 'product_id')->with('lang')->where('deleted_at', NULL);
        }
    
}
