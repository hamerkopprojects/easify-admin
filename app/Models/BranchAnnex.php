<?php

namespace App\Models;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BranchAnnex extends  Authenticatable
{
  
//    use  SoftDeletes;
    /**
    * guarded variable
    *
    * @var array
    */

    protected $guarded = [];
    
    /**
    * $table variable
    *
    * @var string
    */

    protected $table="branch_annex";
    
    
     public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }

   
}
