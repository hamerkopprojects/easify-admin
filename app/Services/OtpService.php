<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class OtpService
{
    public function send(string $message, string $number, string $code = '+91')
    {
        
        $response = Http::post(config('sms.url'), [
            'userName' => config('sms.user_name'),
            'apiKey' => config('sms.api_key'),
            'numbers' => $code . $this->replaceNumber($number),
            'userSender' => config('sms.sender_id'),
            'msg' => $message,
        ]);
        
        //info($response->body());
    }

    public function replaceNumber(string $number)
    {
        $en = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        $ar = ['٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩'];
        
        $replaced = str_replace($ar, $en, $number);

        if ($replaced[0] == '0') {
            $replaced = substr($replaced, 1);
        }

        return $replaced;
    }
}