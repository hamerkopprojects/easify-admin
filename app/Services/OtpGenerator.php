<?php

namespace App\Services;

use App\Models\OtherSettings;

class OtpGenerator
{
    public static function generateOtp($phone = '')
    {
        $other_setting = OtherSettings::first();
        if ($other_setting->sms == 'disable') {
            return 1234;
        }
    
        return mt_rand(1000, 9999);
    }
}