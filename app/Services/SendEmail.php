<?php

namespace App\Services;

use App\Mail\OrderSendEmail;
use App\Mail\SupportSendMail;
use App\Mail\OTPGenerateEmail;

use App\Mail\PasswordGenerateEmail;
use Illuminate\Support\Facades\Mail;

class SendEmail {

    public static function generateEmail($to, $otp = '', $name = '', $subject = '', $from = '', $url = '') {
        if ($from == 'branch' || $from == 'supplier' || $from == 'customer' || $from == 'user') {
            $emai_send = Mail::to($to)->send(new PasswordGenerateEmail($otp, $name, $to, $subject, $from, $url));
            return $emai_send;
        } else {
            $emai_send = Mail::to($to)->send(new OTPGenerateEmail($otp, $name, $subject, $from));
            return $emai_send;
        }
    }

    public static function generateOrderEmail($to, $name = '', $subject = '', $o_details = '', $ord_status = '') {
        $emai_send = Mail::to($to)->send(new OrderSendEmail($name, $subject, $o_details, $ord_status));
        return $emai_send;
    }
    public static function generateSupportEmail($to, $name = '', $subject = '',$query = '') {
        $emai_send = Mail::to($to)->send(new SupportSendMail($name, $subject, $query));
        return $emai_send;
    }
   

}
