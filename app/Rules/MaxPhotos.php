<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class MaxPhotos implements Rule
{
    protected $maxCount;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($maxCount)
    {
        $this->maxCount = $maxCount;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $length = count($value);

        /** @var \App\Models\Product */
        $product = request()->route('product');
        $photoCount = $product->photos()->count();
        return ($length + $photoCount) <= $this->maxCount;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "Max photos count of {$this->maxCount} exceeded";
    }
}
