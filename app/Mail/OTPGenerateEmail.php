<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OTPGenerateEmail extends Mailable {

    use Queueable,
        SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $token;

    public function __construct($token, $name, $subject, $mail_from) {
        $this->token = $token;
        $this->name = $name;
        $this->subject = $subject;
        $this->mail_from = $mail_from;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        if ($this->mail_from == 'create') {
            return $this->subject($this->subject)
                            ->markdown('mail.otp_generate')
                            ->with('name', $this->name)
                            ->with('token', $this->token);
        } else {
            return $this->subject($this->subject)
                            ->markdown('mail.forgot_password')
                            ->with('name', $this->name)
                            ->with('token', $this->token);
        }
    }

}
