<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class QuotationSendEmail extends Mailable {

    use Queueable,
        SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $name;
    public $quotation;

    public function __construct($name, $subject, $quotation, $mail_from) {
        $this->name = $name;
        $this->subject = $subject;
        $this->quotation = $quotation;
        $this->mail_from = $mail_from;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        if ($this->mail_from == 'admin') {
        return $this->subject($this->subject)
                        ->markdown('mail.quotation_approved')
                        ->with('name', $this->name)
                        ->with('quotation', $this->quotation);
        } else {
            return $this->subject($this->subject)
                        ->markdown('mail.quotation_send')
                        ->with('name', $this->name)
                        ->with('quotation', $this->quotation);
        }
    }

}
