<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SupportSendMail extends Mailable {

    use Queueable,
        SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $name;
    public $query;
    public $subject;

    public function __construct($name,$subject,$query) {
        $this->name = $name;
        $this->subject = $subject;
        $this->query = $query;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->subject($this->subject)
                        ->markdown('mail.support_reply')
                        ->with('name', $this->name)
                        ->with('query',$this->query);
    }

}
