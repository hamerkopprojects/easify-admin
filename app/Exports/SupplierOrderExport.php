<?php

namespace App\Exports;


use App\Models\Order;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class SupplierOrderExport implements FromCollection,WithHeadings, WithMapping
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    public $order;

    public function collection()
    {
        $order_status =[9];
        $order=Order::leftJoin('order_items as item', 'order.id', '=', 'item.order_id')
        ->where('order.order_status', 9)
        ->where('item.supplier_id', auth()->user()->id)
        ->select('order.id','order.order_id', 'order.created_at', 'item.status','item.collection_date')
        ->groupBy('order.id')
        ->get();
        return $order;
    }

    public function headings(): array {
        return [
           "Order Id",
           "Order Date",
           "Delivered date"

        ];
    }
    public function map($order): array {
        
        return [
            $order->order_id,
            $order->created_at,
            $order->collection_date,

           
        ];
    }
    // public function view(): view
    // {
    //     $order=Order::where('status','delivered')->get();
    //     return view('order.excel', [
    //         'order' => $order
    //     ]);
    // }
}
