<?php

namespace App\Exports;


use App\Models\Order;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class AdminOrderExport implements FromCollection,WithHeadings, WithMapping
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    public $order;

    public function collection()
    {
        $order_status =[9];
        $order=Order::whereIn('order_status',$order_status)->with('customer')->get();
        return $order;
    }

    public function headings(): array {
        return [
           "order Id",
           "Customer",
           "Customer Phone",
           "Amount",
           "Item Count",
           "Delivery Charge",
           "Payment Method",
           "Delivery Date"

        ];
    }
    public function map($order): array {
        
        return [
            $order->order_id,
            $order->customer->cust_name,
            $order->customer->phone,
            $order->grant_total,
            $order->item_count,
            $order->delivery_charge,
            $order->payment_method,
            $order->delivery_schedule_date
           
        ];
    }
    // public function view(): view
    // {
    //     $order=Order::where('status','delivered')->get();
    //     return view('order.excel', [
    //         'order' => $order
    //     ]);
    // }
}
