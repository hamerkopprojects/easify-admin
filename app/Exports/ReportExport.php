<?php

namespace App\Exports;


use App\Models\Order;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\Models\ProductPrice;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;

class ReportExport implements FromCollection,WithHeadings, WithMapping, ShouldAutoSize, WithEvents
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    public $orders;
 function __construct($orders) {
        $this->orders = $orders;
 }

    public function collection()
    {
        foreach ($this->orders as $row_data){
        $product_price = ProductPrice::where(['product_id' => $row_data->product_id, 'variant_lang_id' => $row_data->variant_id])->first();
        $item_price = $product_price->discount_price ? $product_price->discount_price : $product_price->price;
        $commission_percent = $item_price * $row_data->commision_percentage / 100;
        $markup_percent = $item_price * $row_data->easify_markup / 100;
        $comm_count = $commission_percent * $row_data->item_count;
        $mark_count = $markup_percent * $row_data->item_count;
        $all_orders[] = array(
        'ord_id'  => $row_data->ord_id,
        'markup'  => $mark_count,
        'commission'  => $comm_count,
        'cust_name'  => $row_data->cust_name,
        'grant_total'  => $row_data->grant_total,
        'revenue'  => $row_data->grant_total -  $comm_count - $mark_count,
        );
        }
        return collect($all_orders);
    }

    public function headings(): array {
        return [
           "Order ID",
           "Customer",
           "Sale Amount",
           "Commission",
           "Markup",
           "Supplier Revenue",
        ];
    }
    public function map($orders): array {
        return [
            $orders['ord_id'],
            $orders['cust_name'],
            $orders['grant_total'],
            $orders['commission'],
            $orders['markup'],
            $orders['revenue'],

        ];
    }

    public function registerEvents(): array
        {
        $styleArray = [
        'font' => [
        'family' => 'Calibri',
        'bold' => true,
        'size' => 12,
        ],
        'borders' => [
        'outline' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
            'color' => ['argb' => 'FFFF'],
        ],
       ],
     ];
        return [
            AfterSheet::class    => function(AfterSheet $event) use ($styleArray)
            {
             $event->getSheet()->getDelegate()->getStyle('A1:F1')->applyFromArray($styleArray);
            },
      ];
       }
}
