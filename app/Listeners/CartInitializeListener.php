<?php

namespace App\Listeners;

use App\Events\CartInitialize;
use Illuminate\Support\Facades\Session;

class CartInitializeListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\CartInitialize  $event
     * @return void
     */
    public function handle(CartInitialize $event)
    {
        Session::put('cartKey', $event);
        Session::save();
    }
}