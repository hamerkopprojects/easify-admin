<?php

namespace App\Listeners;

use App\Events\CustomerAuthenticated;
use Illuminate\Support\Facades\Session;

use App\Models\CartKey;

class CustomerAuthListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\CustomerAuthenticated  $event
     * @return void
     */
    public function handle(CustomerAuthenticated $event)
    {
        $cartKey = Session::get('cartKey');
        $cartKey = $cartKey->cartKey ?? '';

        $cart_exists = CartKey::with('parent')
        ->where('key', '=', $cartKey)
        ->first();
        if(isset($cart_exists->parent)) {
            $cart_exists->parent->customer_id = $event->customer_id;
            $cart_exists->parent->save();
        }
        Session::put('customer_id', $event->customer_id);
        Session::put('customer_name', $event->customer_name);
        Session::save();
    }
}