<?php

namespace App\Traits;

use App\User;


trait LoginActivityLog
{
    /**
     * 
     *
     * @param string $done
     * @param string $log
     * @return void
     */
    public function loginLog($id)
    {
        User::where('id',$id)
        ->update([
            'last_activity_date'=> now()
        ]);
    }

    
}