<?php

namespace App\Traits;


use App\Services\FcmNotification;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;

trait CanSendNotification
{
    public function sendNotification($fcmId, string $message, array $data,$badge)
    {
        
        $notification = new FcmNotification(
            new PayloadDataBuilder(),
            new PayloadNotificationBuilder(),
            new OptionsBuilder()
        );

        $downstreamResponse = $notification
            ->setTimeToLive(60 * 2)
            ->setTitle($message)
            ->setBody($data['body'])
            ->setData($data)
            ->to($fcmId)
            ->setSound() 
            // ->setChannelId() 
            ->setBadge($badge)
            ->send();

        return $downstreamResponse;
    }
}