<?php

namespace App\Traits;

use App\Models\OrderLog;

trait OrderActivityLog
{
    /**
     * 
     *
     *
     * @param string $done
     * @param string $log
     * @return void
     */
    public function orderActivity($order_id, String $done, $log, $supplier='', $customer = '', $user = '')
    {
        OrderLog::create([
            'order_id' =>$order_id,
            'done_by'=>$done,
            'log'=>$log,
            'supplier_id'=>$supplier,
            'customer_id'=>$customer,
            'user_id'=>$user
        ]);
    }

    
}