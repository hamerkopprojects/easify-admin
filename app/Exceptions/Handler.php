<?php

namespace App\Exceptions;

use Throwable;
use ErrorException;
use Illuminate\Support\Arr;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($request->expectsJson()) {
            $statusCode = 400;
            $msg = $exception->getMessage();

            switch(true) {
                case $exception instanceof ModelNotFoundException : 
                    $msg = 'No results found';
                    break;
                case $exception instanceof AuthorizationException :
                case $exception instanceof AuthenticationException :
                    $msg = 'Unauthorized';
                    $statusCode = 401;
                    break;
                case $exception instanceof NotFoundHttpException :
                    $msg = 'Invalid url';
                    $statusCode = 404;
                    break;
                case $exception instanceof ValidationException:
                    $msg = Arr::collapse($exception->errors())[0];
                    break;
                case $exception instanceof ErrorException:
                    $msg = $exception->getMessage();
                    $statusCode = 400;
                break;
                default :
                    break;
            }

            return response()->json([
                'success' => false,
                'error' => [
                    'msg' => $msg
                ]
            ],$statusCode);
        }
        return parent::render($request, $exception);
    }
}
