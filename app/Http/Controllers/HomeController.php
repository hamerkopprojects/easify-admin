<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\User;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // return view('home');
        // return view('welcome');
        User::where('id', Auth::user()->id)
                    ->update([
                'ip_address' => request()->ip(),
                'last_login_date' => Carbon::now(),
            ]);
        
        return redirect()->route('dashboard.get');
    }
}
