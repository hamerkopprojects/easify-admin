<?php

namespace App\Http\Controllers\Frontend;

use App\User;
use Carbon\Carbon;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Product;
use App\Models\AppNotifications;
use App\Models\Review;
use App\Models\CartKey;
use App\Models\Customer;
use App\Models\CartItems;
use App\Models\OrderItems;
use App\Models\SupportChat;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Models\OtherSettings;
use App\Models\ProductRating;
use App\Events\CartInitialize;
use App\Models\ProductVariant;
use App\Models\RatingSegments;
use App\Models\SupportRequests;
use App\Models\CancellationReason;
use App\Models\ProductBranchStock;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Traits\OrderActivityLog;

class OrderController extends Controller
{
    use OrderActivityLog; 
    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }
    public function orderList(Request $req)
    {
        $customer = Session::get('customer_id');
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
        $cust_id = [$customer];
        $customer_branchs = Customer::with('branch')
            ->where('id', $customer)
            ->first();
        $ord_web_status = (Config::get('constants.web_order_status'));
        if ($req->id) {
            $order_id = $req->id;
            $ord_status = (Config::get('constants.order_status'));
           
            $order = $this->order->with(['product' => function ($query) use ($lang) {

            $query->withPivot(['item_count', 'grant_total', 'sub_total'])
                ->with(['lang' => function ($query) use ($lang) {
                    $query->where('language', $lang);
                }]);
            }])
        
            ->where('id', $order_id)
            ->first();
        $cancellationReason = CancellationReason::with(['lang' => function ($query) use ($lang) {
            $query->where('language', $lang);
        }])
        ->get();
        $segments = RatingSegments::with('lang')->where('status','active')->get();
        $from = "details";
        $settings = OtherSettings::where('deleted_at', null)->first();
        return view('frontend.order.list', compact('lang', 'order', 'cancellationReason', 'settings', 'ord_status','from','segments','ord_web_status'));
        }else{
        $upcoming_orders = $this->order->where('customer_id', $customer)
            ->whereNotIn('order_status', [6, 9, 8])
            ->orderBy('id', 'desc')
            ->get();
        $completed_orders = $this->order->where('customer_id', $customer)
            ->orderBy('id', 'desc')
            ->where('order_status', 9)
            ->get();
        $cacelled_orders =  $this->order->where('customer_id', $customer)
            ->orderBy('id', 'desc')
            ->where('order_status', 6)
            ->get();
        $from = 'list';
        $customer_id = $customer;
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
        return view('frontend.order.list', compact('customer_branchs', 'upcoming_orders', 'customer', 'completed_orders', 'cacelled_orders', 'from', 
                'customer_id','ord_web_status', 'lang'));
    }
    }

    public function orderDetails($id)
    {
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
        $order_id = $id;
        $ord_status = (Config::get('constants.order_status'));
        $ord_web_status = (Config::get('constants.web_order_status'));
        $order = $this->order->with(['product' => function ($query) use ($lang) {

            $query->withPivot(['item_count', 'grant_total', 'sub_total'])
                ->with(['lang' => function ($query) use ($lang) {
                    $query->where('language', $lang);
                }]);
        }])
            //     ->with(['cancel_reason' => function ($query) use ($lang) {
            //         $query->with(['lang' => function ($query) use ($lang) {
            //             $query->where('language', $lang);
            //         }]);
            // }])
            ->where('id', $order_id)
            ->first();
            $segments = RatingSegments::with('lang')->where('status','active')->get();
        $cancellationReason = CancellationReason::with(['lang' => function ($query) use ($lang) {
            $query->where('language', $lang);
        }])
            ->get();
        $settings = OtherSettings::where('deleted_at', null)->first();
        return view('frontend.order.details', compact('lang', 'order', 'cancellationReason', 'settings', 'ord_status','segments','ord_web_status'));
    }
    public function supportChat(Request $req)
    {
        $customer = Session::get('customer_id');
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
        $order_id = $req->order_id;
        $rules = [
            'subject' => 'required',
            'comment' => 'required',
        ];
        $messages = [
            'subject.required' => 'Subject is required.',
            'comment.required' => 'Message is required.',
        ];

        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $support_values = array(
                'subject' => $req->subject,
                'message' => $req->comment,
                'submitted_by' => $customer,
                'app_type' => 'website',
                'created_at' => Carbon::now()
            );
            if ($order_id) {
                $support_values['order_id'] = $order_id;
            }
            $support_request = SupportRequests::create($support_values);

            SupportChat::create([
                'request_id' => $support_request->id,
                'message' => $req->comment,
                'user_type' => 'website',
            ]);

            //send notification
            $details =[
             'type' => 'support',
             'order_id'=>$support_request->id ];
            $notifications = new AppNotifications;
            $notifications->details = json_encode($details);
            $notifications->from_type = 'website';
            $notifications->to_type = 'admin';
            $notifications->to_id = 1;

            $content = json_encode([
                    'en' => 'New contact support has been created by '. Session::get('customer_name') .'(Customer) on ' . Carbon::now()->format('d/m/Y g:i A'),
                    'ar' => 'New contact support has been created by '. Session::get('customer_name') .'(Customer) on ' . Carbon::now()->format('d/m/Y g:i A')
                ]);
            $notifications->content = $content;
            $notifications->save();
            
            return response()->json(['status' => 1, 'message' => __('frontend.send_suc')]);
        }
    }
    public function cancelOrder(Request $req)
    {
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
        $customer = Session::get('customer_id');
        $rules = [
            'cancel_reason_id' => 'required',
        ];
        $messages = [
            'cancel_reason_id.required' => 'Cancellation reason is required.',
        ];

        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {

            $order_id = $req->order_id;
            Order::where('id', $order_id)
                ->update([
                    'updated_by_id' => $customer,
                    'updated_by' => 'customer',
                    'cancel_note' => $req->camment,
                    'cancel_reason_id' => $req->cancel_reason_id,
                    'order_status' => 6
                ]);
         
        //send notification
        $order_data = Order::where('id', '=', $order_id)->first();
        $details =[
         'type' => 'order',
         'order_id'=>$order_id
        ];
        $notifications = new AppNotifications;
        $notifications->details = json_encode($details);
        $notifications->from_type = 'website';
        $notifications->to_type = 'admin';
        $notifications->to_id = Session::get('customer_id');
        
        $content = json_encode([
                'en' => 'Order '.$order_data->order_id.' cancelled by '. Session::get('customer_name') .'(customer) on ' . Carbon::now()->format('d/m/Y g:i A'),
                'ar' => 'Order '.$order_data->order_id.' cancelled by '. Session::get('customer_name') .'(customer) on ' . Carbon::now()->format('d/m/Y g:i A')
            ]);
        $notifications->content = $content;
        $notifications->save();
        
        //Create order log
        $order_data = Order::where('id', '=', $order_id)->first();
        if ($order_data) {
            $order_id = $order_data->id;
            $done_by = 'website';
            $log = 'Order '.$order_data->order_id.' cancelled  by ' . Session::get('customer_name') . '(Customer) on ' . Carbon::now()->format('d/m/Y g:i A');
            $customer = $order_data->customer_id;
            $this->orderActivity($order_id, $done_by, $log, '', $customer, '');
        }
        //End order log
        
        //For stock update
        $region_id = Session::get('region_id');
        $order_item = OrderItems::where('order_id', '=', $order_id)->get();
        foreach ($order_item as $item) {
                $product = Product::where('id', '=', $item->product_id)->first();
                if ($product->product_type == 'complex') {
                $product_stock = ProductVariant::where('variant_lang_id', '=', $item->variant_id)
                ->with(['pdt_branch' => function($query) use($region_id){
                if ($region_id) {
                  $query->where('region_id', $region_id);  
                }
                }])->where('product_id', '=', $item->product_id)
                ->first();
          } 
          else {
                $product_stock = ProductVariant::where('variant_lang_id', '=', NULL)
                ->with(['pdt_branch' => function($query) use($region_id){
                if ($region_id) {
                  $query->where('region_id', $region_id);  
                }
               }])->where('product_id', '=', $item->product_id)
                ->first();
        }
         $max_stock = $product_stock->pdt_branch->max_stock;
         $remaining_stock = $max_stock + $item->item_count;
         ProductBranchStock::where(['product_variant_stock_id' => $product_stock->id, 'region_id' => $region_id])
                    ->update([
                        'max_stock' => $remaining_stock,
                    ]);
       }
                
                // End
       
        }
        return response()->json(['status' => 1, 'message' => __('frontend.canc_ord_msg')]);
    }
    public function cartCount()
    {
        $cartKey = Session::get('cartKey');
        $cartKey = $cartKey->cartKey ?? '';

        $cart_exists = CartKey::with('parent')
            ->where('key', '=', $cartKey)
            ->first();

        if (isset($cart_exists->parent) &&  $cart_exists->parent->item_count > 0) {
            return response()->json(['success' => FALSE, 'message' =>__('frontend.pdt_extst')]);
        }

        return response()->json(['success' => TRUE, 'message' => 'Cart is empty']);
    }
    public function modifyOrder($id)
    {

        if ($id == '') {
            return redirect()->route('frontend');
        }

        $order = Order::with('items')
            ->where('id', $id)
            ->where('order_status', 9)
            ->first();

        if (!$order) {
            return redirect()->route('frontend');
        }

        $cartKey = Session::get('cartKey');
        if (!isset($cartKey->cartKey)) {
            event(new CartInitialize('create'));
        }
        $cartKey = $cartKey->cartKey ? $cartKey->cartKey : '';

        $cart_exists = CartKey::with('parent')
            ->where('key', '=', $cartKey)
            ->first();

        if (isset($cart_exists->parent)) {
            $cart_exists->parent->delete();
        }
        if ($cart_exists) {
            $cart_exists->delete();
        }
        $customer_id = '';
        $customer = Session::get('customer_id');
        if (isset($customer) && $customer) {
            $customer_id = $customer;
        }
        $admin = User::where('role_id', 1)->first();

        $cart = new Cart();
        // $cart->users_id = $admin->id;
        if ($customer_id) {
            $cart->customer_id = $customer_id;
            $cart->is_guest = 'N';
        }
        $cart->item_count = $order->item_count;
        $cart->grant_total = $order->grant_total;
        $cart->sub_total = $order->sub_total;
        $cart->tax_total = $order->tax_total;
        $cart->delivery_charge = $order->delivery_charge;
        $cart->save();
        $easyfy_cart_id = $cart->id;

        $cart_key_entry = new CartKey();
        $cart_key_entry->cart_id = $easyfy_cart_id;
        $cart_key_entry->key = $cartKey;
        $cart_key_entry->save();

        if (isset($order->items)) {
            foreach ($order->items as $item) {
                $product_id = $item->product_id;
                $variant_id = $item->variant_id;
                // $variant_price_id = $item->variant_price_id;
                $item_count = $item->item_count;
                $grant_total = $item->grant_total;
                $sub_total = $item->sub_total;
                $tax_total = $item->tax_total;

                $cartItems = new CartItems();
                $cartItems->cart_id = $easyfy_cart_id;
                $cartItems->product_id = $product_id;
                $cartItems->branch_id = $item->branch_id;
                $cartItems->variant_id = $variant_id;
                $cartItems->item_count = $item_count;
                $cartItems->sub_total = $sub_total;
                $cartItems->grant_total = $grant_total;
                $cartItems->tax_total = $tax_total;
                // $cartItems->variant_price_id = $variant_price_id;
                $cartItems->save();
            }
        }

        return redirect()->route('cart.show');
    }
    public function filterOrder($customer_id)
    {
        $customer = Session::get('customer_id');
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
        $ord_web_status = (Config::get('constants.web_order_status'));
        $customer_branchs = Customer::with('branch')
            ->where('id', $customer)
            ->first();
        $status = Config::get('constants.order_status');
        
        if ($customer_id == 'all') {
            $customer = Session::get('customer_id');
            $cust_id = [$customer];
            $customer_branchs = Customer::with('branch')
                ->where('id', $customer)
                ->first();
            foreach ($customer_branchs->branch as $branch) {
                array_push($cust_id, $branch->id);
            }

            $upcoming_orders = $this->order->whereIn('customer_id', $cust_id)
                ->whereNotIn('order_status', [6, 9, 8])
                ->orderBy('id', 'desc')
                ->get();
            $completed_orders = $this->order->where('customer_id', $cust_id)
                ->orderBy('id', 'desc')
                ->where('order_status', 9)
                ->get();
            $cacelled_orders =  $this->order->where('customer_id', $cust_id)
                ->orderBy('id', 'desc')
                ->where('order_status', 6)
                ->get();
        } else {
            $upcoming_orders = $this->order->where('customer_id', $customer_id)
                ->whereNotIn('order_status', [6, 9, 8])
                ->orderBy('id', 'desc')
                ->get();
            $completed_orders = $this->order->where('customer_id', $customer_id)
                ->orderBy('id', 'desc')
                ->where('order_status', 9)
                ->get();
            $cacelled_orders =  $this->order->where('customer_id', $customer_id)
                ->orderBy('id', 'desc')
                ->where('order_status', 6)
                ->get();
        }
        $from = 'list';
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
        return view('frontend.order.filter', compact('customer_branchs', 'upcoming_orders', 'customer', 'completed_orders', 'cacelled_orders', 'from', 'customer_id','ord_web_status', 'lang'));
    }
    
    public function product_rating(Request $req) {
        $order_id = $req->order_id;
        $rules = [
            'rating_input' => 'required',
        ];
        $messages = [
            'rating_input.required' => 'Rating is required.',
        ];
        
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $rating = ProductRating::where(['order_id' => $req->order_id, 'product_id' => $req->product_id])->count();
            if($rating > 0){
                ProductRating::where(['order_id' => $req->order_id, 'product_id' => $req->product_id])
                ->update([
                    'rating' => $req->rating_input,
                    'review' => $req->review,
                ]);
            }else{
            ProductRating::create([
                'order_id' => $req->order_id,
                'product_id' => $req->product_id,
                'rating' => $req->rating_input,
                'review' => $req->review,
                'created_at' => Carbon::now()
            ]);
            }
            return response()->json(['status' => 1, 'message' =>__('frontend.pdt_rate')]);
        }
    }

    public function orderRating(Request $req)
    {
        
        
        $ratings_val=json_decode($req->order_rating_array,true);
        $ratings = Arr::pluck($ratings_val, 'rating');
        $totalRating = round(array_sum($ratings) / count($ratings), 1);
        $rules = [
            'order_rating_array' => 'required',
        ];
        $messages = [
            'order_rating_array.required' => 'Rating is required.',
        ];

        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            DB::transaction(function () use ($ratings_val, $req,$totalRating) {
                $review = Review::updateOrCreate([
                    'order_id'=>$req->order_id,
                    'customer_id' => Session::get('customer_id'),
                    'message'=>$req->order_review,
                    'rating_total'=>$totalRating
                ]);
                $review->reviewSegments()->forceDelete();
                $review->reviewSegments()->createMany($ratings_val);
            });
            return response()->json(['status' => 1, 'message' =>__('frontend.ord_rate') ]);
        }

    }
}
