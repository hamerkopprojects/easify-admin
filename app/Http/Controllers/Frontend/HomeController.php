<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Slider;
use App\Ads;
use App\Models\Category;
use App\Models\PromotionalProduct;
use App\Models\CustomerBranch;
use App\Models\ProductPrice;
use App\Models\ProductVariant;
use App\Models\Cart;
use App\Models\CartKey;
use App\Models\CartItems;
use App\Models\Product;
use App\Models\Address;
use App\Models\Order;
use App\Models\OrderItems;
use App\Models\ProductBranchStock;
use App\Models\VariantLang;
use App\Models\AppNotifications;
use App\Models\Supplier;
use App\Models\BranchAnnex;
use App\Models\WishList;
use App\Models\SaveLater;
use App\Models\OtherSettings;
use App\Models\Region;
use App\Models\Promocode;
use App\Models\PromocodeCategory;
use App\Models\PromocodeProduct;
use App\Models\CommisionCategory;

use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Events\CartInitialize;

use Illuminate\Support\Carbon;
use App\User;
use Config;
use Illuminate\Support\Facades\Mail;
use App\Services\SendEmail;
use App\Traits\OrderActivityLog;
class HomeController extends Controller {

    use OrderActivityLog;
    
    public function home() {
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
        $slider = Slider::with('slider_category')->with('slider_product')->with('slider_category1')->with('slider_product1')->get();
//for banner
        $top_banners = Ads::with('webcategory')->with('webproduct')->whereIn('id', [1, 2])->where('web_image', '!=', '')->get();
        $bottom_banners = Ads::with('webcategory')->with('webproduct')->whereIn('id', [3, 4])->where('web_image', '!=', '')->get();
//end
// Shop by category
        $region_id = Session::get('region_id');
        $shop_by_cat = Category::with('lang')->where(['is_featured' => 'yes', 'status' => 'active'])->limit(8)->get();
        $shop_by_cat_count = Category::where(['is_featured' => 'yes', 'status' => 'active'])->count();
//end
        $promoproducts = $this->promotional_products($lang);
        $promotionalProducts = $promoproducts['promotional_products'];
        $promo_count = $promoproducts['total_count'];

        $cartData = $this->cartItemData();
        $commission = CommisionCategory::select('value')->where('deleted_at', null)->get();
        return view('frontend.home.home', compact('lang', 'slider', 'promotionalProducts', 'top_banners', 'bottom_banners', 'shop_by_cat', 'shop_by_cat_count', 'cartData', 'promo_count', 'region_id')
        );
    }

    public function more_category(Request $req) {
        $region_id = Session::get('region_id');
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
        $shop_by_cat_count = Category::where(['is_featured' => 'yes', 'status' => 'active'])->count();
        $shop_by_cat = Category::with('lang')->where(['is_featured' => 'yes', 'status' => 'active'])->skip(8)->take($shop_by_cat_count)->get();
        return view('frontend.home.more_category', compact('shop_by_cat', 'region_id'));
    }

    private function cartItemData() {
        $cartKey = Session::get('cartKey');
        $cartKey = isset($cartKey->cartKey) ? $cartKey->cartKey : '';

        $cartItems = CartKey::where('key', '=', $cartKey)
                        ->with(['parent' => function ($query) {
                                $query->with('items');
                            }])->first();
        $productsCountMap = [];
        if ($cartItems && isset($cartItems->parent) && isset($cartItems->parent->items)) {
            foreach ($cartItems->parent->items as $each) {
                $productsCountMap[$each->product_id] = $each->item_count;
            }
        }

        return $productsCountMap;
    }

    private function promotional_products($lang) {
        $region_id = Session::get('region_id');
        $data = array();
        $promo = PromotionalProduct::where('deleted_at', null)->select('product_id')->get()->toArray();
        $query = Product::with(['lang' => function ($query) use($lang) {
                                $query->where('language', $lang);
                            }])
                        ->with(['variant_price' => function($query) use($lang) {
                                $query->with(['variant_lang' => function($query) use($lang) {
                                        $query->where('language', 'en');
                                    }]);
                            }])->with(['variant' => function($query) use($region_id){
                $query->with(['pdt_branch' => function($query) use($region_id){
                  if ($region_id) {
                  $query->where('region_id', $region_id);  
                  }
                }]);
                }]);
                if ($region_id) {
                    $query->where('supplier_branch_region', 'like', "%" . '{{' . $region_id . '}}' . "%");
                }
                $query->with('commission');
                $query = $query->where('status', 'active')
                        ->where('supplier_status', 'publish')
                        ->where('deleted_at', null)
                        ->orderBy('id', 'desc')
                        ->whereIn('id', $promo);
                $total_records = $query->count();
                $promotional_products = $query->take(8)->get();
//                print_r($promotional_products->toArray());exit;
                $data = [
                    'promotional_products' => $promotional_products,
                    'total_count' => $total_records,
                ];

                return $data;
            }

            public function addToCart(Request $req) {
                $product_id = $req->product_id;
                $is_savelater = $req->is_savelater;
                $type = $req->type;
                $region_id = Session::get('region_id');
                if ($region_id) {
                    $pdt_data = Product::with(['supplier' => function($query) use($region_id) {
                                    $query->where('region_id', $region_id);
                                }])->where(['id' => $product_id])->first();

                    if ($pdt_data->supplier) {
                        $supplier_id = $pdt_data->supplier->id;
                    } else {
                        $bra_data = Supplier::with(['supplier_branch' => function($query) use($pdt_data) {
                                        $query->where('supplier_parent_id', $pdt_data->supplier_id);
                                    }])->where(['region_id' => $region_id])->get();
                        foreach ($bra_data as $rowdata) {
                            if (!empty($rowdata->supplier_branch)) {
                                $supplier_id = $rowdata->supplier_branch->supplier_id;
                            }
                        }
                    }
                }
                Session::put('supplier_id', $supplier_id);
                Session::save();
// variant NULL for simple product.
                $variant_id = $req->variant_id ?? NULL;
// Expected values for item_qty are 1 & -1
                $item_qty = $req->qty ?? 1;
                $lang = app()->getLocale() == 'en' ? 'en' : 'ar';

                $cartKey = Session::get('cartKey');
                if (!isset($cartKey->cartKey)) {
                    event(new CartInitialize('create'));
                }
                $cartKey = Session::get('cartKey');
                $cartKey = $cartKey->cartKey;
                if ($type == 'complex') {
                    $varinat_price = ProductPrice::where('product_id', '=', $product_id)
                            ->where('variant_lang_id', '=', $variant_id)
                            ->first();
                } else {
                    $varinat_price = ProductPrice::where('product_id', '=', $product_id)
                            ->where('variant_lang_id', '=', NULL)
                            ->first();
                }
                
                if (!$varinat_price) {
                    return response()->json(['success' => FALSE, 'message' => __('frontend.qty_not_avl')]);
                }
                
                $product = Product::with(['lang' => function ($query) use ($lang) {
                                $query->where('language', $lang);
                            }])
                        ->with('commission')
                        ->where('id', '=', $product_id)
                        ->first();

                if (!$product) {
                    return response()->json(['success' => FALSE, 'message' =>__('frontend.pdt_not_avl')]);
                }
                $easify_markup = $varinat_price->easify_markup ? $varinat_price->easify_markup : NULL;
                $price = $varinat_price->discount_price ? $varinat_price->discount_price : $varinat_price->price;
                $commission = isset($product->commission->value) ? $product->commission->value : NULL;
                $price_commission = ($price * $commission / 100);
                $price_markup = ($price * $easify_markup / 100);
                $price = $price + $price_commission + $price_markup;
                $qty = $item_qty;
// complex product added as bundles.
// $qty = $type == 'complex' ? $qty : $qty*$qty;

                $customer_id = '';
                $customer = Session::get('customer_id');
                if (isset($customer) && $customer) {
                    $customer_id = $customer;
                }

                if ($type == 'complex') {
                    $product_stock = ProductVariant::where('variant_lang_id', '=', $variant_id)
                            ->with(['pdt_branch' => function($query) use($region_id){
                            if ($region_id) {
                              $query->where('region_id', $region_id);  
                              } 
                           }])->where('product_id', '=', $product_id)
                            ->first();
                } else {
                    $product_stock = ProductVariant::where('variant_lang_id', '=', NULL)
                            ->with(['pdt_branch' => function($query) use($region_id){
                            if ($region_id) {
                              $query->where('region_id', $region_id);  
                              } 
                           }])->where('product_id', '=', $product_id)
                            ->first();
                }

                $max_stock = 0;
                if (isset($product_stock->pdt_branch)) {
                    $max_stock = $product_stock->pdt_branch->max_stock;
                    $min_stock = $product_stock->pdt_branch->min_stock;
                }
                if($max_stock == 0 || $min_stock > $max_stock){
                  return response()->json(['success' => FALSE, 'message' => __('frontend.out_stk')]);  
                }
                if ($req->addedFrom != 'cart') {
                    if ($qty > 0 && $max_stock < $qty) {
                        return response()->json(['success' => FALSE, 'message' => __('frontend.max_stock_validation') .' '. $max_stock]);
                    }
                    if ($qty > 0 && $qty < $min_stock) {
                        return response()->json(['success' => FALSE, 'message' => __('frontend.min_stock_validation') .' '. $min_stock]);
                    }
                }
                $sub_total = $qty * $price;
                $tax_total = 0;
                $grant_total = $sub_total + $tax_total;

                $cart_exists = CartKey::with('parent')
                        ->where('key', '=', $cartKey)
                        ->first();

                $cartItemExists = [];
// Check cart exists or not.
                if (isset($cart_exists->parent)) {
                    $cart_id = $cart_exists->parent->id;
// Get product if already added.
                    if ($type == 'complex') {
                        $cartItemExists = CartItems::where('product_id', '=', $product_id)
                                        ->where('cart_id', '=', $cart_id)
                                        ->where('variant_id', '=', $variant_id)->first();
                    } else {
                        $cartItemExists = CartItems::where('product_id', '=', $product_id)
                                        ->where('cart_id', '=', $cart_id)
                                        ->where('variant_id', '=', null)->first();
                    }
                    if ($cartItemExists) {
// Already added item.
                        $main_item_count = $cart_exists->parent->item_count;
                    } else {
// New product added.
                        $main_item_count = $cart_exists->parent->item_count + 1;
                    }
                } else {
// New cart.
                    $main_item_count = 1;
                }
                if ($cartItemExists && isset($cartItemExists->item_count) && $req->addedFrom == 'cart') {
                    $cart_qty = $cartItemExists->item_count + $qty;
                    if ($cart_qty > 0 && $max_stock < $cart_qty) {
                        return response()->json(['success' => FALSE, 'message' => __('frontend.max_stock_validation') .' '. $max_stock]);
                    }
                    if ($cart_qty > 0 && $cart_qty < $min_stock) {
                        return response()->json(['success' => FALSE, 'message' => __('frontend.min_stock_validation') .' '. $min_stock]);
                    }
                }
                $main_subtotal = $sub_total;
                $main_grant_total = $grant_total;
                $main_tax_total = $tax_total;
                $total_item_count = $main_item_count;

                if ($region_id) {
                    $regions = Region::select('delivary_charge')->where('id', $region_id)->first();
                    $delivery_charge = $regions->delivary_charge;
                }
                $delivery_charge = $delivery_charge ? $delivery_charge : 0;

                if ($cart_exists && isset($cart_exists->parent)) {
// Cart existing
                    if ($customer_id) {
                        $cart_exists->parent->customer_id = $customer_id;
                    }
                    $cart_exists->parent->delivery_charge = $delivery_charge;
                    $cart_exists->parent->item_count = $main_item_count;

                    $cart_exists->parent->save();
                } else {
                    $cart = new Cart();
                    if ($customer_id) {
                        $cart->customer_id = $customer_id;
                        $cart->is_guest = 'N';
                    }
                    $cart->item_count = $main_item_count;
                    $cart->grant_total = $main_grant_total;
                    $cart->sub_total = $main_subtotal;
                    $cart->tax_total = $main_tax_total;
                    $cart->delivery_charge = $delivery_charge;
                    $cart->save();

                    $cart_id = $cart->id;
                    if (!$cart_exists) {
// Create cart key entry.
                        $cart_key_entry = new CartKey();
                        $cart_key_entry->cart_id = $cart_id;
                        $cart_key_entry->key = $cartKey;
                        $cart_key_entry->save();
                    }
                }
                if ($cartItemExists) {
// This product already exists.
                    $cartItemExists->item_count += $item_qty;
                    $cartItemExists->sub_total += $sub_total;
                    $cartItemExists->grant_total += $grant_total;
                    $cartItemExists->tax_total = $tax_total;
                    $cartItemExists->save();

                    $product_count = $cartItemExists->item_count;
                } else {
// This product not exists in cart
                    $commission_id = isset($product->commision_percentage) ? $product->commision_percentage : NULL;
                    $cartItems = new CartItems();
                    $cartItems->cart_id = $cart_id;
                    $cartItems->product_id = $product_id;
                    $cartItems->variant_id = $variant_id;
                    $cartItems->item_count = $item_qty;
                    $cartItems->item_price = $price;
                    $cartItems->easify_markup = $easify_markup;
                    $cartItems->commision_percentage = $commission_id;
                    $cartItems->branch_id = $supplier_id;
                    $cartItems->sub_total = $sub_total;
                    $cartItems->grant_total = $grant_total;
                    $cartItems->tax_total = $tax_total;
                    $cartItems->save();

                    $product_count = $cartItems->item_count;
                }

                if ($cart_exists && isset($cart_exists->parent)) {
                    $g_total = CartItems::where('cart_id', '=', $cart_id)
                            ->sum('grant_total');
                    $s_total = CartItems::where('cart_id', '=', $cart_id)
                            ->sum('sub_total');
                    $t_total = CartItems::where('cart_id', '=', $cart_id)
                            ->sum('tax_total');
                    $cart_exists->parent->sub_total = $s_total;
                    $cart_exists->parent->grant_total = $g_total;
                    $cart_exists->parent->tax_total = $t_total;
                    $cart_exists->parent->save();
                    $main_grant_total = $g_total;
                }

                $message = ($req->addedFrom == 'cart') ? __('frontend.item_update_cart') :__('frontend.item_add_cart') ;
                 if ($is_savelater) {
                     SaveLater::where('product_id', $product_id)->where('customer_id', $customer)->delete();
                 }
// $savelater_count = SaveLater::where('customer_id', $customer)->count();

                return response()->json([
                            'success' => TRUE,
                            'message' => $message,
                            'count' => $total_item_count,
                            'product_count' => $product_count,
                            'grant_total' => $main_grant_total
// 'savelater_count' => $savelater_count
                ]);
            }

        public function cart(Request $req) {
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';

        $cartKey = Session::get('cartKey');
        $cartKey = isset($cartKey->cartKey) ? $cartKey->cartKey : '';

        $cartItems = CartKey::where('key', '=', $cartKey)
        ->with(['parent' => function ($query) use ($lang) {
        $query->with(['items' => function ($query) use ($lang) {
        $query->with(['product' => function ($query) use ($lang) {

            $query->with(['lang' => function($query) use($lang) {
                    $query->where('language', $lang);
                }])->with(['variant_price' => function($query) use($lang) {
                    $query->with(['variant_lang' => function($query) use($lang) {
                            $query->where('language', 'en');
                        }]);
                }]);
                $query->with('commission');
                }]);
                }]);
                }])
                    ->first();
            $region_id = Session::get('region_id');
            $regions = Region::select('delivary_charge')->where('id', $region_id)->first();
            $delivary_charge = $regions->delivary_charge;
            $settings = OtherSettings::first();
            $vat = $settings->vat_percentage;
            return view('frontend/cart/cart', compact('lang', 'cartItems', 'delivary_charge','vat'));
        }

        public function cartRefresh(Request $req) {
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
        $cartKey = Session::get('cartKey');
        $cartKey = isset($cartKey->cartKey) ? $cartKey->cartKey : '';

        $cartItems = CartKey::where('key', '=', $cartKey)
        ->with(['parent' => function ($query) use ($lang) {
        $query->with(['items' => function ($query) use ($lang) {
        $query->with(['product' => function ($query) use ($lang) {

            $query->with(['lang' => function($query) use($lang) {
                    $query->where('language', $lang);
                }])->with(['variant_price' => function($query) use($lang) {
                    $query->with(['variant_lang' => function($query){
                            $query->where('language', 'en');
                        }]);
                }]);
                }]);
                }]);
                }])
                    ->first();
            $region_id = Session::get('region_id');
            $regions = Region::select('delivary_charge')->where('id', $region_id)->first();
            $delivary_charge = $regions->delivary_charge;
            $settings = OtherSettings::first();
            $vat = $settings->vat_percentage;
            return view('frontend/cart/cart_content', compact('lang', 'cartItems', 'delivary_charge', 'vat'));
        }

        public function branch(Request $request) {
            $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
            $customer_id = Session::get('customer_id');
            $branches = CustomerBranch::where('created_by', $customer_id)->get();

            return view('frontend/branch/customer_branch', compact('branches', 'lang'));
        }

        function removeFromCart(Request $request) {
        $product_id = $request->product_id;
        $type = $request->type;
        $variant_id = $request->variant_id;
        $variant_id = $type == 'complex' ? $variant_id : NULL;

        $cartKey = Session::get('cartKey');
        $cartKey = $cartKey->cartKey ?? '';

        $cart_exists = CartKey::with('parent')
        ->where('key', '=', $cartKey)
        ->first();

        if (!$cart_exists || !isset($cart_exists->parent)) {
        return response()->json(['success' => FALSE, 'message' => 'Cart is empty']);
        }

        $cart_id = $cart_exists->parent->id;

        if (!$product_id || !$type) {
        return response()->json(['success' => FALSE, 'message' => 'Product not found']);
        }

        $cartItem = CartItems::where('product_id', $product_id)
        ->where('variant_id', $variant_id)
        ->where('cart_id', '=', $cart_id)
        ->first();
        if (!$cartItem) {
        return response()->json(['success' => FALSE, 'message' => 'Product not found']);
        }

        $response = $cartItem->delete();
        if ($response) {
        $grant_total = CartItems::where('cart_id', '=', $cart_id)
        ->sum('grant_total');
        $sub_total = CartItems::where('cart_id', '=', $cart_id)
        ->sum('sub_total');
        $tax_total = CartItems::where('cart_id', '=', $cart_id)
        ->sum('tax_total');
        $total_item_count = CartItems::where('cart_id', '=', $cart_id)
        ->where('item_count', '>', 0)
        ->count();
        $cart_exists->parent->grant_total = $grant_total;
        $cart_exists->parent->sub_total = $sub_total;
        $cart_exists->parent->tax_total = $tax_total;
        $cart_exists->parent->item_count = $total_item_count;
        $cart_exists->parent->save();

        return response()->json([
        'success' => TRUE,
        'message' => __('frontend.pdt_rmv_frm_crt'),
        'count' => $total_item_count,
        'grant_total' => $grant_total
        ]);
        } else {
        return response()->json([
        'success' => FALSE,
        'message' => __('frontend.smtg_wt_wrng')
        ]);
        }
        }

        function order_note(Request $request) {
        $order_note = $request->order_note;
        $customer = Session::get('customer_id');
        if (!isset($customer) || !$customer) {
        return response()->json(['success' => FALSE, 'code' => 'no.login', 'message' => __('frontend.ps_lgn_order')]);
        }
        $cartKey = Session::get('cartKey');
        $cartKey = $cartKey->cartKey ?? '';
        $cart_data = CartKey::with('parent')
        ->where('key', '=', $cartKey)
        ->first();
        $cart_id = $cart_data->cart_id;

        $cart = Cart::where('id', $cart_id)->where('customer_id', $customer)->first();
        if (!$cart) {
        return response()->json(['success' => 0, 'message' => __('frontend.cart_empty')]);
        } else {
        $saved_data = Cart::where('id', $cart->id)
        ->update([
        'note' => $order_note,
        ]);
        return response()->json(['success' => 1]);
        }
        }
        function coupon_code(Request $request) {
        
        $coupon_code = $request->coupon_code;
        $customer = Session::get('customer_id');
        if (!isset($customer) || !$customer) {
        return response()->json(['success' => FALSE, 'code' => 'no.login', 'message' => __('frontend.pls_login_aply_coupon')]);
        }
        
        $cartKey = Session::get('cartKey');
        $cartKey = $cartKey->cartKey ?? '';
        $cart_data = CartKey::with('parent')
        ->where('key', '=', $cartKey)
        ->first();
        $cart_id = $cart_data->cart_id;
        
        if(!empty($coupon_code)){
        $promocodes = Promocode::where('code', $coupon_code)->first();
        if (!isset($promocodes) || !$promocodes) {
        Cart::where('id', $cart_id)
        ->update([
        'coupon_code' => Null,
        'coupon_type' => Null,
        'coupon_value' => Null
        ]);
        return response()->json(['success' => FALSE, 'message' => __('frontend.invd_cpn_cd')]);
        }else {
          $max_num_usage = $promocodes->max_num_usage;
          if($max_num_usage > 0){
          $order_count = Order::where('coupon_code', $coupon_code)->count();
          if($order_count >= $max_num_usage){
          Cart::where('id', $cart_id)
            ->update([
            'coupon_code' => Null,
            'coupon_type' => Null,
            'coupon_value' => Null
            ]);
          return response()->json(['success' => FALSE, 'message' => __('frontend.coupon_usage1')]);    
          }
          }
          $max_num_per_user = $promocodes->max_num_per_user;
          if($max_num_per_user > 0){
          $customer_id = Session::get('customer_id');
          $order_count = Order::where('coupon_code', $coupon_code)->where('customer_id', $customer_id)->count();
          if($order_count >= $max_num_per_user){
          Cart::where('id', $cart_id)
            ->update([
            'coupon_code' => Null,
            'coupon_type' => Null,
            'coupon_value' => Null
          ]);
          return response()->json(['success' => FALSE, 'message' => __('frontend.coupon_usage2')]);    
          }
          }
        }
        }
        $cart = Cart::where('id', $cart_id)->where('customer_id', $customer)->first();
        if (!$cart) {
        return response()->json(['success' => 0, 'message' => __('frontend.cart_empty')]);
        } else {
        $promocodes = Promocode::where('code', $coupon_code)->first();  
        $cartItemExists = CartItems::where('cart_id', '=', $cart_id)->get();
        $count = 0;
        $item_array =[];
        foreach ($cartItemExists as $item) { 
        if($promocodes->rule_based == 'category'){
         $product_cat= Product::where('id', '=', $item->product_id)->first();
         $category_ids = $product_cat->category_ids;
         $category_ids = str_replace('}}{{', ',', $category_ids);
         $category_ids = str_replace('{{', ' ', $category_ids);
         $category_ids = str_replace('}}', ' ', $category_ids);
         $cat_ids = explode(",", $category_ids);
         if($promocodes->type == 'include'){
         foreach($cat_ids as $catid_val){
         $promo_cats = PromocodeCategory::where('promocodes_id', $promocodes->id)->where('category_id', $catid_val)->first();
          if(!empty($promo_cats)){
           $item_array[]  = $item->product_id;     
         } 
         }
         }else{
         foreach($cat_ids as $catid_val){
         $promo_cats = PromocodeCategory::where('promocodes_id', $promocodes->id)->where('category_id', '!=', $catid_val)->first();
          if(empty($promo_cats)){
           $item_array[]  = $item->product_id;     
         }
         }
         }  
        }else{
         $promo_products = PromocodeProduct::where('promocodes_id', $promocodes->id)->where('product_id', $item->product_id)->first(); 
         if($promocodes->type == 'include'){
         if(!empty($promo_products)){
           $item_array[]  = $item->product_id;  
         }    
         }else{
          if(empty($promo_products)){  
           $item_array[]  = $item->product_id;      
          } 
         }
        }
        }
        }
        $offer_items = array_unique($item_array);
        $tot_sum =0;
        foreach ($cartItemExists as $item_val) {
            if (in_array($item_val->product_id, $offer_items)) {
                if($promocodes->discount_type == 'amount'){
                $tot_sum += $promocodes->value;
                }else{
                $tot_sum += $item_val->sub_total * $promocodes->value /100; 
                }
            }
        }
        if($tot_sum > 0){
        $saved_data = Cart::where('id', $cart->id)
        ->update([
        'coupon_code' => $coupon_code,
        'coupon_type' => $promocodes->discount_type,
        'coupon_value' => $tot_sum
        ]);
        return response()->json(['success' => 1, 'message' => __('frontend.cpn_app_succ')]);
        }else{
          Cart::where('id', $cart_id)
            ->update([
            'coupon_code' => Null,
            'coupon_type' => Null,
            'coupon_value' => Null
            ]);
          return response()->json(['success' => 0, 'message' =>  __('frontend.cpn_app_error')]);  
        }
        }

        function checkout(Request $request) {

        $customer = Session::get('customer_id');
        if (!isset($customer) || !$customer) {
        return response()->json(['success' => FALSE, 'code' => 'no.login', 'message' => __('frontend.ps_lgn_order')]);
        }

        $cartKey = Session::get('cartKey');
        $cartKey = $cartKey->cartKey ?? '';
        $cart_data = CartKey::with('parent')
        ->where('key', '=', $cartKey)
        ->first();
        $cart_id = $cart_data->cart_id;

        $cart = Cart::where('id', $cart_id)->where('customer_id', $customer)->first();
        if (!$cart) {
        return response()->json(['success' => FALSE, 'message' => __('frontend.cart_empty')]);
        }
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';

        $selected_delivery_address = Session::get('delivery_address_id');
        if ($selected_delivery_address) {
        $delivery_address = Address::where(['cust_id' => $customer, 'address_flag' => 'D', 'id' => $selected_delivery_address])
        ->first();
        } else {
        $delivery_address = Address::where(['cust_id' => $customer, 'address_flag' => 'D', 'is_default' => 'Y'])
        ->first();
        }
        $selected_billing_address = Session::get('billing_address_id');
        if ($selected_billing_address) {
        $billing_address = Address::where(['cust_id' => $customer, 'address_flag' => 'B', 'id' => $selected_billing_address])
        ->first();
        } else {
        $billing_address = Address::where(['cust_id' => $customer, 'address_flag' => 'B', 'is_default' => 'Y'])
        ->first();
        }
        $time_slots = Config::get('constants.schedule_time');
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
        return view('frontend/checkout/checkout', compact('delivery_address', 'billing_address', 'cart', 'time_slots', 'lang'));
        }

        public function checkout_cart(Request $req) {
        $location = $req->location;
        $coordinates = $req->coordinates;
        $settings = OtherSettings::first();
        $customer = Session::get('customer_id');
        if (!isset($customer) || !$customer) {
        return response()->json(['success' => FALSE, 'code' => 'no.login', 'message' => 'Please login to checkout']);
        }

        $cartKey = Session::get('cartKey');
        $cartKey = $cartKey->cartKey;

        $cart_exists = CartKey::with('parent')
        ->where('key', '=', $cartKey)
        ->first();
        
        if($req->payment_method == ''){
            return response()->json(['success' => FALSE, 'message' => __('frontend.sel_pay')]);
        }
        if ($req->payment_method == 'offline') {
            if (!empty($req->file('payment_slip'))) {
            $current = Carbon::now()->format('YmdHs');
            $file = $req->file('payment_slip');
            $name = $current . $file->getClientOriginalName();
            $path_en = $file->move(public_path() . '/uploads/order/payment_slip/', $name);
            $path = 'order/payment_slip/' . $name;
        } else {
            return response()->json(['success' => FALSE, 'message' => __('frontend.pymt_slip_req')]);
        }
        }

        if (!$cart_exists->parent) {
        return response()->json(['success' => FALSE, 'message' => __('frontend.cart_empty')]);
        }
        $cart_id = $cart_exists->parent->id;
        $branch_id = $cart_exists->parent->branch_id;
        $cartItemExists = CartItems::where('cart_id', '=', $cart_id)->get();

        if (!$cartItemExists) {
        return response()->json(['success' => FALSE, 'message' => __('frontend.cart_empty')]);
        }

        $selected_billing_address = Session::get('billing_address_id');
        $selected_delivery_address = Session::get('delivery_address_id');

        if ($selected_delivery_address) {
        $address = Address::where('cust_id', $customer)
        ->where('address_flag', 'D')
        ->where('id', $selected_delivery_address)->first();
        } else {
        $address = Address::where('cust_id', $customer)
        ->where('address_flag', 'D')
        ->where('is_default', 'Y')->first();
        }

        if (!$address) {
        return response()->json(['success' => FALSE, 'message' => __('frontend.select_del_add')]);
        }

        if ($req->payment_method == '') {
        return response()->json(['success' => FALSE, 'message' => __('frontend.sel_pay')]);
        }

        if ($selected_billing_address) {
        $b_address = Address::where('cust_id', $customer)
        ->where('address_flag', 'B')
        ->where('id', $selected_billing_address)->first();
        } else {
        $b_address = Address::where('cust_id', $customer)
        ->where('address_flag', 'B')
        ->where('is_default', 'Y')->first();
        }

        if (!$b_address) {
        return response()->json(['success' => FALSE, 'message' => __('frontend.select_bill_add')]);
        }
        $order_id = $this->generateOrderId();
        $order_amount = $cart_exists->parent->sub_total;
        if($cart_exists->parent->coupon_code){
        $order_amount = $cart_exists->parent->sub_total - $cart_exists->parent->coupon_value;
        } 
        $order_amount = $order_amount ? $order_amount : $cart_exists->parent->sub_total;
        $vat_percent = $order_amount * $settings->vat_percentage / 100;
        $grand_total = $order_amount + $cart_exists->parent->delivery_charge + $req->cod_fee + $vat_percent;
        $order = new Order();
        $order->order_id = $order_id;
        $user = User::where('role_id', 1)->first();
        $order->users_id = $user->id;
        $order->customer_id = $cart_exists->parent->customer_id;
        $order->item_count = $cart_exists->parent->item_count;
        $order->grant_total = $grand_total;
        $order->sub_total = $cart_exists->parent->sub_total;
        $order->tax_total = $cart_exists->parent->tax_total;
        $order->delivery_charge = $cart_exists->parent->delivery_charge;
        $order->delivery_schedule_date = $req->delivery_date;
        $order->time_slot = $req->time_slot;
        $order->delivery_days = $settings->delivery_days;
        $order->vat = $settings->vat_percentage;
        $order->payment_method = $req->payment_method;
        if ($order->payment_method == 'cod') {
        $order->cod_fee = $req->cod_fee;
        }
        if($req->payment_method == 'offline'){
         $order->payment_slip = $path;
        }
        if ($location) {
        $order->delivery_loc = $location;
        $order->delivery_loc_coordinates = $coordinates;
        }
        if($cart_exists->parent->coupon_code){
        $promocodes = Promocode::where('code', $cart_exists->parent->coupon_code)->first();
        if (!isset($promocodes) || !$promocodes) {
         return response()->json(['success' => FALSE, 'message' => __('frontend.invd_cpn_cd')]);   
        }else{
        $order->coupon_code = $cart_exists->parent->coupon_code;
        $order->coupon_type = $promocodes->discount_type;
        $order->coupon_value = $promocodes->value;
        }
        }
        $order->delivery_name = $address->name;
        $order->delivery_phone = $address->phone;
        $order->delivery_address = $address->apartment_name;
        $order->delivery_city = $address->street_name;
        $order->delivery_country = 'SA';
        $order->delivery_zip = $address->postal_code;

        $order->billing_name = $b_address->name;
        $order->billing_phone = $b_address->phone;
        $order->billing_address = $b_address->apartment_name;
        $order->billing_city = $b_address->street_name;
        $order->billing_country = 'SA';
        $order->billing_zip = $b_address->postal_code;


        $order->deleted_at = NULL;
        $order->save();

        $items = [];
        foreach ($cartItemExists as $item) {
        $variant_name = VariantLang::where(['id' => $item->variant_id, 'language' => 'en'])->pluck('name')->first();
        $variant_name_ar = VariantLang::where(['en_matching_id' => $item->variant_id, 'language' => 'ar'])->pluck('name')->first();
        $branch_count = BranchAnnex::where('supplier_parent_id', $item->branch_id)->count();
        if ($branch_count > 0) {
        $supplier_id = $item->branch_id;
        } else {
        $supp_data = BranchAnnex::where('supplier_id', $item->branch_id)->first();
        if (!empty($supp_data)) {
        $supplier_id = $supp_data->supplier_parent_id;
        } else {
        $supplier_id = $item->branch_id;
        }
        }
        $commision = CommisionCategory::where('id', $item->commision_percentage)->where('deleted_at',null)->pluck('value')->first();
        $items[] = [
        'order_id' => $order->id,
        'product_id' => $item->product_id,
        'supplier_id' => $supplier_id,
        'branch_id' => $item->branch_id,
        'variant_id' => $item->variant_id,
        'product_details' => json_encode([
        'variant_en' => $variant_name,
        'variant_ar' => $variant_name_ar
        ]),
        'item_count' => $item->item_count,
        'item_price' => $item->item_price,
        'easify_markup' => $item->easify_markup,
        'commision_percentage' => $commision,
        'grant_total' => $item->grant_total,
        'sub_total' => $item->sub_total,
        'tax_total' => $item->tax_total
        ];
       
        //For stock update
        $region_id = Session::get('region_id');
        $product = Product::where('id', '=', $item->product_id)->first();
        if ($product->product_type == 'complex') {
        $product_stock = ProductVariant::where('variant_lang_id', '=', $item->variant_id)
                ->with(['pdt_branch' => function($query) use($region_id){
                if ($region_id) {
                  $query->where('region_id', $region_id);  
                }
               }])->where('product_id', '=', $item->product_id)
                ->first();
       } else {
        $product_stock = ProductVariant::where('variant_lang_id', '=', NULL)
                ->with(['pdt_branch' => function($query) use($region_id){
                if ($region_id) {
                  $query->where('region_id', $region_id);  
                } 
               }])->where('product_id', '=', $item->product_id)
                ->first();
        }
        $max_stock = $product_stock->pdt_branch->max_stock;
        $remaining_stock = $max_stock - $item->item_count;
        ProductBranchStock::where(['product_variant_stock_id' => $product_stock->id, 'region_id' => $region_id])
                    ->update([
                        'max_stock' => $remaining_stock
                    ]);
        //end
        
        $branch_ids[] = $item->branch_id;
        }
        if ($items) {
        OrderItems::insert($items);
        }
        //send notification
        $to_type[] = 'admin';
        $to_id[] = 1;
        $branch_id = array_unique($branch_ids);
        foreach($branch_id as $branch_id_val){
            $suppliers = Supplier::where(['id' => $branch_id_val])->first();
            if($suppliers->role_id = 5){
                 $to_type[] = 'supplier';
                 $to_id[] = $branch_id_val;
            }else{
                $to_type[] = 'branch';
                $to_id[] = $branch_id_val;
            }
        }
        foreach($to_id as $key => $to_id_val){
        $order_data = Order::where('id', '=', $order->id)->first();
        $details =[
         'type' => 'order',
         'order_id'=>$order_data->id ];
        $notifications = new AppNotifications;
        $notifications->details = json_encode($details);
        $notifications->from_type = 'website';
        $notifications->to_type = $to_type[$key];
        $notifications->to_id = $to_id_val;
        
        $content = json_encode([
                'en' => 'New Order '.$order_data->order_id.' created by '. Session::get('customer_name') .'(customer) on ' . Carbon::now()->format('d/m/Y g:i A'),
                'ar' => 'New Order '.$order_data->order_id.' created by '. Session::get('customer_name') .'(customer) on ' . Carbon::now()->format('d/m/Y g:i A')
            ]);
        $notifications->content = $content;
        $notifications->save();
        }
        
         //Create order log
        $order_data = Order::where('id', '=', $order->id)->first();
        if($order_data){
        $order_id = $order_data->id;
        $done_by = 'website';
        $log = 'New Order '. $order_data->order_id.' created by ' . Session::get('customer_name') . '(Customer) on ' . Carbon::now()->format('d/m/Y g:i A');
        $customer = $order_data->customer_id;
        $this->orderActivity($order_id, $done_by, $log, '', $customer, '');
        }
       //End order log
        
        $cart_exists->delete();
        $cart_exists->parent->delete();
        CartItems::where('cart_id', '=', $cart_id)->delete();
        event(new CartInitialize('destroy'));
        $route = route('cart.thanks', ['id' => $order->id]);

        $message =  __('frontend.order_create');
        $customer_data = Customer::where(['id' => $customer])->first();
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
        
        $order_data = Order::with(['items' => function ($query) use ($lang) {
        $query->with(['product' => function ($query) use ($lang) {
                $query->with(['lang' => function ($query) use ($lang) {
                        $query->where('language', $lang);
                    }]);
            }]);
            }])->where('id', $order->id)->first();
        $subject = 'Thank you for your order!';
        SendEmail::generateOrderEmail($customer_data->email, $customer_data->cust_name, $subject, $order_data, 1);

        return response()->json(['success' => TRUE, 'message' => $message, 'route' => $route]);
        }

        private function generateOrderId() {
        $lastOrder = Order::select('order_id')
        ->orderBy('id', 'desc')
        ->withTrashed()
        ->first();

        $lastId = 0;

        if ($lastOrder) {
        $lastId = (int) substr($lastOrder->order_id, 8);
        }

        $lastId++;

        return 'EAS-ORD-' . str_pad($lastId, 5, '0', STR_PAD_LEFT);
        }

        function thankyou(Request $request) {
        $orderId = $request->id;
        $customer_name = Session::get('customer_name');
        $order = Order::where('id', $orderId)->first();

        $ord_status = (Config::get('constants.order_status'));
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
        return view('frontend/checkout_thanks/thanks', compact('order', 'customer_name', 'ord_status', 'lang'));
        }

        public function wishlist_items(Request $request) {
        $region_id = Session::get('region_id');
        $customer = Session::get('customer_id');
        if (!$customer) {
        return response()->json(['success' => FALSE, 'message' => __('frontend.login_for_wish')]);
        }
        $lang = app()->getLocale() == 'ar' ? 'ar' : 'en';

        $wishlist = WishList::with(['product' => function ($query) use ($lang, $region_id) {
        $query->with(['lang' => function ($query) use ($lang) {
        $query->where('language', $lang);
        }])->with(['variant_price' => function($query) use($lang) {
        $query->with(['variant_lang' => function($query) use($lang) {
                $query->where('language', 'en');
            }]);
        }])->with(['variant' => function($query) use($region_id){
                $query->with(['pdt_branch' => function($query) use($region_id){
                  if ($region_id) {
                  $query->where('region_id', $region_id);  
                  }  
                }]);
                }])->with('commission');
        }])->where('customer_id', $customer)->where('region_id', $region_id)
        ->get();

        return view('frontend.modals.wishlist_modal', compact('wishlist', 'lang'));
        }

        public function saveLaterItems(Request $request) {
        $region_id = Session::get('region_id');
        $customer = Session::get('customer_id');
        if (!$customer) {
        return response()->json(['success' => FALSE, 'message' => 'Please login']);
        }
        $lang = app()->getLocale() == 'ar' ? 'ar' : 'en';

        $savelater = SaveLater::with(['product' => function ($query) use ($lang, $region_id) {
                $query->with(['lang' => function ($query) use ($lang) {
                        $query->where('language', $lang);
                    }])->with(['variant_price' => function($query) use($lang) {
                        $query->with(['variant_lang' => function($query) use($lang) {
                                $query->where('language', 'en');
                            }]);
                    }])->with(['variant' => function($query) use($region_id){
                $query->with(['pdt_branch' => function($query) use($region_id){
                  if ($region_id) {
                  $query->where('region_id', $region_id);  
                  } 
                }]);
                }])->with('commission');
                    }])->where('customer_id', $customer)->where('region_id', $region_id)
                        ->get();

                return view('frontend.modals.savelater_modal', compact('savelater', 'lang'));
            }

}
                                                                                                        