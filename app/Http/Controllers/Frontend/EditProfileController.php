<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Config;
use DB;
use App\Models\Customer;
use App\Address;
use App\Models\Category;
use App\Models\City;
use App\Models\Region;
use App\Models\BusinessType;
use Exception;
use Facade\FlareClient\Http\Response;
use Illuminate\Support\Facades\Session;
use Validator;

class EditProfileController extends Controller {

    public function __construct() {
        
    }

    public function index(Request $req, $lang = 'en') {
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
        $customer = Session::get('customer_id');
        $profile = Customer::where('id', $customer)->first();
        $city = City::with(['lang' => function ($query) use ($lang) {
                        $query->where('language', $lang);
                    }])->get();
        $region = Region::with(['lang' => function ($query) use ($lang) {
                        $query->where('language', $lang);
                    }])->where('city_id', $profile->city)->get();
        $business_type = BusinessType::with(['lang' => function ($query) use ($lang) {
                        $query->where('language', $lang);
                    }])->get();

        return view('frontend.myaccount.edit_profile', compact('lang', 'profile', 'city', 'business_type', 'region'));
    }

    public function upload_image(Request $req) {
        $customer = Session::get('customer_id');
        $cust = Customer::where('id', $customer)->first();
        $rules = [
            'profile_image' => 'required|image|mimes:png,jpg,jpeg|max:10240'
        ];
        $messages = [
            'profile_image.max' => 'The image must be less than 2Mb in size',
            'profile_image.mimes' => "The image must be of the format jpeg or png",
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $file_path = public_path() . '/uploads/' . $cust->profile_image;
            if (is_file($file_path)) {
                unlink($file_path);
            }
            $file = request()->file('profile_image');
            $path = $file->store("profile", ['disk' => 'public_uploads']);
            Customer::where('id', $customer)
                    ->update([
                        "profile_image" => $path
            ]);
            return response()->json(['status' => 1, 'message' => __('frontend.img_upd_suss')]);
        }
    }

    public function profile_update(Request $req) {
        $customer = Session::get('customer_id');
        if ($customer) {
            $unique = ',' . $customer;
        } else {
            $unique = '';
        }
        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:customer,email' . $unique,
            'phone' => 'required|unique:customer,phone' . $unique,
        ];
        $messages = [
            'name.required' => 'Name is required.',
            'email.required' => 'Email is required.',
            'email.unique' => 'Customer with same email already exists',
            'phone.required' => 'Phone number is required.',
            'phone.unique' => 'Customer with same phone number already exists',
        ];

        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            if (isset($customer)) {
                $data_to_save = [
                    'cust_name' => $req->name,
                    'business_type' => $req->business_type,
                    'email' => $req->email,
                    'region' => $req->region,
                    'city' => $req->city,
                    'phone' => (int) $req->phone,
                    'dob' => $req->dob,
                    'country_code' => $req->country_code,
                ];
                $saved_data = Customer::where('id', $customer)
                        ->update($data_to_save);

                return response()->json(['status' => 1, 'message' =>__('frontend.prof_upd_succ') ]);
            } else {
                return response()->json(['status' => 0, 'message' => __('frontend.smtg_wt_wrng')]);
            }
        }
    }

    public function getRegion(Request $req) {
        $region = Region::with('lang')->where('city_id', $req->city_id)->get();
        return $region;
    }

}
