<?php

namespace App\Http\Controllers\Frontend;

use URL;

use Config;
use Validator;

use App\Models\Region;
use App\Models\Address;
use App\Models\CartKey;
use App\Models\Product;
use App\Models\Customer;
use App\Models\WishList;
use App\Models\CartItems;
use App\Models\SaveLater;
use App\Services\SendEmail;
use App\Services\OtpService;

use Illuminate\Http\Request;
use App\Models\OtherSettings;
use App\Events\CartInitialize;
use App\Models\CustomerBranch;
use App\Services\OtpGenerator;
use Illuminate\Support\Carbon;

use App\Http\Controllers\Controller;
use App\Events\CustomerAuthenticated;
use Illuminate\Support\Facades\Session;

class Common extends Controller {

    public function login(Request $req) {
        $username = $req->user_name;
        $password = $req->password;
        if ($password == NULL || $password == '' || $username == '' || $username == NULL) {

            return response()->json(['success' => FALSE, 'message' => __('frontend.usr_pass_rq')]);
        }

        $customer = Customer::where('email', $username)->where('password', md5($password))->first();
        if ($customer) {
            event(new CustomerAuthenticated($customer->id, $customer->cust_name));

            return response()->json(['success' => TRUE, 'message' => __('frontend.log_succ'), 'customer_name' => $customer->cust_name]);
        } else {

            return response()->json(['success' => FALSE, 'message' => __('frontend.inv_usr_pass') ]);
        }
    }

    public function logout(Request $request) {
        event(new CustomerAuthenticated('', ''));

        return redirect('/');
    }

    public function customer_create(Request $req) {
        if ($req->_customer_id) {
            $unique = ',' . $req->_customer_id;
        } else {
            $unique = '';
        }

        $rules = [
            'type' => 'required',
            'name' => 'required',
            'business_name' => 'required',
            'email' => 'required|email|unique:customer,email' . $unique,
            'password' => 'required|min:6',
            'phone' => 'required|unique:customer,phone' . $unique,
        ];
        $messages = [
            'type.required' => 'Business type is required.',
            'name.required' => 'Name is required.',
            'business_name.required' => 'Business name is required.',
            'email.required' => 'Email is required.',
            'email.unique' => 'Customer with same email already exists',
            'password.required' => 'Password is required.',
            'password.min' => 'Password minimum of 6 characters is required.',
            'phone.required' => 'Phone number is required.',
            'phone.unique' => 'Customer with same phone number already exists',
        ];

        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['success' => FALSE, 'message' => $validator->errors()->first()]);
        } else {
            $otp = $this->generateOtp();
            $data_to_save = [
                'cust_name' => $req->name,
                'business_name' => $req->business_name,
                'business_type' => $req->type,
                'email' => $req->email,
                'password' => md5($req->password),
                'country_code' => $req->country_code,
                'phone' => (int)$req->phone_no,
                'otp' => $otp
            ];
            if ($req->_customer_id == '') {
                $cust_id = $this->generateCustomerId();
                $data_to_save['cust_id'] = $cust_id;
                $data_to_save['created_at'] = Carbon::now();
                $saved_data = Customer::insertGetId($data_to_save);
                $msg = __('frontend.cust_add_succ');
                Session::put('cust_to_verify', $saved_data);

                // When customer created, a default branch also generated with is_fefault_branch 'Y'.
//                $default_branch['customer_id'] = $saved_data;
//                $default_branch['branch'] = 'Default';
//                $default_branch['is_fefault_branch'] = 'Y';
//                $default_branch['created_by'] = $saved_data;
//                CustomerBranch::insert($default_branch);

            } else {
                $saved_data = Customer::where('id', $req->_customer_id)
                        ->update($data_to_save);
                $msg = __('frontend.cust_updt_scss') ;
            }
            if ($saved_data) {
                $subject = 'Welcome to Easify';
                SendEmail::generateEmail($req->email, $otp, $req->name, $subject, 'create');
                return response()->json(['success' => 1, 'message' => $msg, 'd' => $saved_data]);
            } else {
                return response()->json(['success' => 0, 'message' => __('frontend.smtg_wt_wrng')]);
            }
        }
    }

    private function generateCustomerId() {
        $lastcustomer = Customer::select('cust_id')
                ->orderBy('id', 'desc')
                ->first();

        $lastId = 0;

        if ($lastcustomer) {
            $lastId = (int) substr($lastcustomer->cust_id, 9);
        }

        $lastId++;

       return 'EAS-CUST-' . str_pad($lastId, 5, '0', STR_PAD_LEFT);
    }

    private function generateOtp() {
        $otp = mt_rand(1111, 9999);
        return 1234;
    }

    public function customer_verify(Request $request) {
        $otp = $request->otp;
        if (!$otp) {
            return response()->json(['success' => 0, 'message' => __('frontend.otp_req_msg')]);
        }

        $cust_id = Session::get('cust_to_verify');
        $data = Customer::where('id', $cust_id)->first();
        if (!$data) {
            return response()->json(['success' => 0, 'message' => __('frontend.smtg_wt_wrng')]);
        }
        if ($data->otp == $otp) {
            Session::put('cust_to_verify', '');
            event(new CustomerAuthenticated($data->id, $data->cust_name));
            return response()->json(['success' => 1, 'message' => __('frontend.otp_verf_msg'), 'customer_name' => $data->cust_name]);
        }

        return response()->json(['success' => 0, 'message' => __('frontend.inv_otp')]);
    }

    public function login_with_phone(Request $request , OtpService $otpService) {
        $phone = $request->phone;
        if (!$phone) {
            return response()->json(['success' => 0, 'message' =>  __('frontend.phone_required')]);
        }

        $data = Customer::where('phone', $phone)->first();
        if (!$data) {
            return response()->json(['success' => 0, 'message' => __('frontend.cust_wt_phone_not_exists')]);
        }
        $otp = OtpGenerator::generateOtp($request->phone);
        $data->otp = $otp ;
        // $this->generateOtp();
        $data->save();
        $other_setting = OtherSettings::first();
        if($other_setting->sms == 'enable'){
            $message = trans('sms.otp.web', ['otp' => $otp]);
            $country_code = $data->country_code ?? '+91' ;
            $otpService->send($message, $request->phone,$country_code);
        }
        Session::put('cust_to_verify', $data->id);

        return response()->json(['success' => 1, 'message' => __('frontend.otp_receved_verify')]);
    }
    
    public function forgot_password(Request $req) {
        $email = $req->email;
        $password = $req->password;
        if ($email == NULL) {

            return response()->json(['success' => FALSE, 'message' => 'Email required']);
        }
        $customer = Customer::where('email', $email)->first();
        if ($customer) {
                $otp = $this->generateOtp();
                $customer->otp = $otp;
                $customer->save();
                Session::put('cust_to_verify', $customer->id);
                $subject = 'Easify: Welcome back!';
                SendEmail::generateEmail($customer->email, $otp, $customer->cust_name, $subject, 'forgot');
                return response()->json(['success' => TRUE, 'message' => __('frontend.ver_send_to_email') , 'customer' => $customer->id]);
        } else {

            return response()->json(['success' => FALSE, 'message' => __('frontend.emil_not_reg_wit_as')]);
        }
    }

    public function reset_password(Request $req) {
        $rules = [
            'password' => 'required',
            'c_password' => 'required',
        ];
        $messages = [
            'password.required' => __('frontend.pass_req'),
            'c_password.required' => __('frontend.c_pass_req')
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['success' => FALSE, 'message' => $validator->errors()->first()]);
        }
        if ($req->password != $req->c_password) {
            return response()->json(['success' => FALSE, 'message' => __('frontend.pass_cpass_nomatch')]);
        }
        $update_data = [
            'password' => md5($req->password)
        ];
        $customer = Session::get('customer_id');
        $customer_det = Customer::where(['id' => $customer])->first();
        $customer = Customer::where('id', $customer)
                ->update($update_data);
        if ($customer) {
            return response()->json(['success' => TRUE, 'message' => __('frontend.pass_upd_succ'), 'customer_name' => $customer_det->cust_name]);
        } else {
            return response()->json(['success' => FALSE, 'message' => __('frontend.smtg_wt_wrng')]);
        }
    }

    function is_inside_geo_region(Request $req) {
        $lat = $req->lat;
        $lng = $req->lng;
        $polygon = OtherSettings::select('operational_area')->first();
        if ($polygon) {
            $polygon = json_decode($polygon->operational_area);
        } else {
            return response()->json(['success' => 1, 'inside' => TRUE]);
        }

        if ($polygon[0] != $polygon[count($polygon) - 1]) {
            $polygon[count($polygon)] = $polygon[0];
        }
        $j = 0;
        $is_inside = false;
        $x = $lng;
        $y = $lat;
        $n = count($polygon);
        for ($i = 0; $i < $n; $i++) {
            $j++;
            if ($j == $n) {
                $j = 0;
            }
            if ((($polygon[$i]->lat < $y) && ($polygon[$j]->lat >= $y)) || (($polygon[$j]->lat < $y) && ($polygon[$i]->lat >=
                    $y))) {
                if ($polygon[$i]->lng + ($y - $polygon[$i]->lat) / ($polygon[$j]->lat - $polygon[$i]->lat) * ($polygon[$j]->lng -
                        $polygon[$i]->lng) < $x) {
                    $is_inside = !$is_inside;
                }
            }
        }
        $regionId = '';
        if($is_inside) {
            $regions = Region::select('coordinates', 'id')->where('coordinates', '!=', 'null')->get();
            $distance = '';
            foreach($regions as $poly) {
                $polygon = $poly->coordinates;
                if ($polygon) {
                    $polygon = json_decode($polygon);
                }
                $n = count($polygon);
                for ($i = 0; $i < $n; $i++) {
                    // Find distance to each lat,lng of regions.
                    $_d = $this->vincentyGreatCircleDistance($lat, $lng, $polygon[$i]->lat, $polygon[$i]->lng);
                    // get nearest cordinte of regions.
                    if($distance == '' || $_d < $distance) {
                        $distance = $_d;
                        $regionId = $poly->id;
                    }
                }
            }
            if($regionId) {
                Session::put('region_id', $regionId);
                Session::save();
            }
        }
        if($regionId){
           $cartKey = Session::get('cartKey');
           $cartKey = $cartKey->cartKey ?? '';
           $cart_exists = CartKey::with('parent')
            ->where('key', '=', $cartKey)
            ->first();
            if($cart_exists){
            $cart_exists->delete();
            $cart_exists->parent->delete();
            $cart_id = $cart_exists->parent->id;
            CartItems::where('cart_id', '=', $cart_id)->delete();
            event(new CartInitialize('destroy'));
            }
        }

        return response()->json(['success' => $is_inside, 'region_id' => $regionId, 'inside' => $is_inside, 
            'message' => __('frontend.loc_chng_out_region_msg')]
        );
    }

    private function vincentyGreatCircleDistance(
        $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
      {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);
      
        $lonDelta = $lonTo - $lonFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) +
          pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);
      
        $angle = atan2(sqrt($a), $b);
        
        return $angle * $earthRadius;
      }

    public function branch_create(Request $req) {
        $rules = [
            'branch_name' => 'required',
            'contact_name' => 'required',
            'email' => 'required|email|unique:customer,email',
            'phone' => 'required|unique:customer,phone',
        ];
        $messages = [
            'branch_name.required' => 'Branch name is required.',
            'contact_name.required' => 'Contact name is required.',
            'email.required' => 'Email is required.',
            'email.unique' => 'Customer with same email already exists',
            'phone.required' => 'Phone number is required.',
            'phone.unique' => 'Customer with same Phone number already exists',
        ];

        $customer_id = Session::get('customer_id');

        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['success' => FALSE, 'message' => $validator->errors()->first()]);
        } else {
            // When branch created, a branch customer also generated with is_branch 'Y'.
            $data_to_save = [
                'cust_name' => $req->contact_name,
                'business_name' => $req->branch_name,
                'business_type' => 1,
                'email' => $req->email,
                'country_code' => $req->country_code,
                'phone' => (int)$req->phone,
                'is_branch' => 'Y'
            ];
            
            $cust_id = $this->generateCustomerId();
            $data_to_save['cust_id'] = $cust_id;
            $data_to_save['created_at'] = Carbon::now();
            $saved_data = Customer::insertGetId($data_to_save);

            $branch['customer_id'] = $saved_data;
            $branch['branch'] = $req->branch_name;
            $branch['email'] = $req->email;
            $branch['phone'] = (int)$req->phone;
            $branch['contact_name'] = $req->contact_name;
            $branch['is_fefault_branch'] = 'N';
            $branch['created_by'] = $customer_id;
            CustomerBranch::insert($branch);
            $msg = __('frontend.brch_add_succ');
            
            if ($saved_data) {
                return response()->json(['success' => 1, 'message' => $msg, 'd' => $saved_data]);
            } else {
                return response()->json(['success' => 0, 'message' => __('frontend.smtg_wt_wrng')]);
            }
        }
    }
    
    public function wishlist(Request $request)
    {
        $region_id = Session::get('region_id');
        $customer = Session::get('customer_id');
        if (!$customer) {
            return redirect('/');
        }
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';

        $wishlist = WishList::with(['product' => function ($query) use($lang, $region_id) {
        $query->with(['lang' => function($query) use ($lang) {
        $query->where('language', $lang);
        }])->with(['variant_price' => function($query) use($lang) {
        $query->with(['variant_lang' => function($query) use($lang) {
        $query->where('language', 'en');
        }]);
        }])->with(['variant' => function($query) use($region_id){
                $query->with(['pdt_branch' => function($query) use($region_id){
                  if ($region_id) {
                  $query->where('region_id', $region_id);  
                  }
                }]);
                }])->with('commission');
        }])->where('customer_id', $customer)->where('region_id', $region_id)
            ->get();

        return view('frontend.myaccount.wishlist', compact('lang', 'wishlist'));
    }
    
    public function add_to_favourite(Request $request) {
        $product_id = $request->product_id;
        $region_id = Session::get('region_id');
        $customer = Session::get('customer_id');
        if (!$product_id || !$customer) {
            return response()->json(['success' => 0, 'message' => __('frontend.login_for_wish')]);
        }

        $product = Product::where('id', $product_id)->first();
        if (!$product) {
            return response()->json(['success' => 0, 'message' => __('frontend.pdt_nt_fnd')]);
        }

        $wishList = WishList::where('customer_id', $customer)->where('product_id', $product_id)->first();
        if ($wishList) {
            $wishList->delete();
            $message = __('frontend.pdt_removed_wishlist');
        } else {
            $wishList = new WishList();
            $wishList->customer_id = $customer;
            $wishList->product_id = $product_id;
            $wishList->region_id = $region_id;
            $wishList->save();
            $message = __('frontend.pdt_added_wishlist');
        }

        return response()->json(['success' => 1, 'message' => $message]);
    }

    function addresses(Request $req)
    {  
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
        $branch_id = $is_address = $req->id;
        $customer = Session::get('customer_id');
        if(isset($branch_id) && $branch_id != 'checkout'){
        $customer_id = $branch_id;
        }else{
         $customer_id = $customer;   
        }
        if (!$customer) {
            return redirect('/');
        }
        $d_addresses = Address::where(['cust_id' => $customer_id, 'address_flag' => 'D'])
            ->orderBy('id', 'asc')
            ->get();
        $b_addresses = Address::where(['cust_id' => $customer_id, 'address_flag' => 'B'])
            ->orderBy('id', 'asc')
            ->get();
        $selected_billing_address = Session::get('billing_address_id');
        $selected_delivery_address = Session::get('delivery_address_id');
        $is_address_page = TRUE;
        
        $customer_branchs = Customer::with('branch')
            ->where('id', $customer) ->first();
        
        $regions = Region::with(['lang' => function ($query) use ($lang) {
                        $query->where('language', $lang);
                    }])->get();
        
        return view('frontend.address.address',
            compact( 'd_addresses', 'b_addresses', 'selected_billing_address', 'selected_delivery_address', 'is_address_page', 
                    'is_address', 'customer_branchs', 'customer_id', 'regions', 'lang')
        );
    }

    public function save_address(Request $req) {
        $customer = Session::get('customer_id');
        if (!$customer) {
            return response()->json(['success' => 0, 'message' => __('frontend.smtg_wt_wrng')]);
        }

        // B or D
        $address_flag = $req->address_flag;

        if ($req->is_default == 'on') {
            $is_default = 'Y';
        } else {
            $is_default = 'N';
        }
        $rules = [
            'name' => 'required',
            'apartment' => 'required',
            'street' => 'required',
            'landmark' => 'required',
            'phone' => 'required',
            'po_box' => 'required',
            'region' => 'required',
        ];
        $messages = [
            'name.required' => 'Name is required.',
            'apartment.required' => 'Apartment Name is required.',
            'street.required' => 'Street Name is required.',
            'landmark.required' => 'Landmark is required.',
            'po_box.required' => 'Postal Code is required.',
            'region.required' => 'Region is required.',
            'phone.required' => 'Phone is required.',
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if ( ! $validator->passes()) {
            return response()->json(['success' => 0, 'message' => $validator->errors()->first()]);
        } else {
            if($address_flag == 'D') {
                $d_count = Address::where(['cust_id' => $req->customer_id, 'address_flag' => 'D'])->count();
                if ($d_count > 0 && $is_default == 'Y' && $address_flag == 'D') {
                    // Change existing default del address.
                    Address::where(['cust_id' => $req->customer_id, 'address_flag' => 'D'])
                            ->update([
                                'is_default' => 'N'
                    ]);
                }
                if( $d_count == 0 ) {
                    $is_default = 'Y';
                }
            }
            
            if( $address_flag == 'B') {
                $b_count = Address::where(['cust_id' => $req->customer_id, 'address_flag' => 'B'])->count();
                if ($b_count > 0 && $is_default == 'Y' && $address_flag == 'B') {
                    // Change existing default bil address.
                    Address::where(['cust_id' => $req->customer_id, 'address_flag' => 'B'])
                        ->update([
                            'is_default' => 'N'
                        ]);
                }
                if( $b_count == 0 ) {
                    $is_default = 'Y';
                }
            }

            $data_to_save = [
                'address_flag' => $address_flag,
                'name' => $req->name,
                'cust_id' => $req->customer_id,
                'apartment_name' => $req->apartment,
                'street_name' => $req->street,
                'landmark' => $req->landmark,
                'postal_code' => $req->po_box,
                'region' => $req->region,
                'phone' => (int)$req->phone,
                'is_default' => $is_default,
            ];
            if ($req->_address_id == '') {
                $data_to_save['created_at'] = Carbon::now();
                $saved_data = Address::insert($data_to_save);
                $msg = __('frontend.add_sucess_add');
            } else {
                $saved_data = Address::where('id', $req->_address_id)
                        ->update($data_to_save);
                $msg = __('frontend.add_sucess_update');
            }
            if ($saved_data) {
                return response()->json(['success' => 1, 'message' => $msg]);
            } else {
                return response()->json(['success' => 0, 'message' => __('frontend.smtg_wt_wrng')]);
            }
        }
    }

    public function list_address(Request $req) {
        $customer = Session::get('customer_id');
        if (!$customer) {
            return response()->json(['success' => 0, 'message' => __('frontend.smtg_wt_wrng')]);
        }
        $addresses = Address::where(['cust_id' => $customer, 'address_flag' => 'D'])
                // ->orderBy('is_default', 'asc')
                ->orderBy('id', 'asc')
                ->get();

        $selected_billing_address = Session::get('billing_address_id');
        if( $selected_billing_address ) {
            $billing_address = Address::where(['cust_id' => $customer, 'address_flag' => 'B', 'id' => $selected_billing_address])
                ->first();
        } else {
            $billing_address = Address::where(['cust_id' => $customer, 'address_flag' => 'B', 'is_default' => 'Y'])
                ->first();
        }

        $allBilling_addresses = Address::where(['cust_id' => $customer, 'address_flag' => 'B'])
            // ->orderBy('is_default', 'asc')
            ->orderBy('id', 'asc')
            ->get();
        
        $selected_delivery_address = Session::get('delivery_address_id');

        return response()->json([
            'success' => 1, 
            'addresses' => $addresses, 
            'billing_address' => $billing_address,
            'all_billing_addresses' => $allBilling_addresses,
            'selected_delivery_address' => $selected_delivery_address,
            'selected_billing_address' => $selected_billing_address
        ]);
    }

    public function choose_billing_address(Request $req) {
        $type = $req->type;
        $id = $req->id;
        if($type == 'B') {
            Session::put('billing_address_id', $id);
        } else {
            Session::put('delivery_address_id', $id);
        }

        return response()->json(['success' => 1, 'message' => __('frontend.add_select')]);
    }
    
    public function delete_address(Request $req) {
        $address = Address::where('id', '=', $req->id)->firstOrFail();
        if ($address) {
            $address->delete();
            return response()->json(['status' => 1, 'message' => __('frontend.addr_delet')]);
        } else {
            return response()->json(['status' => 0, 'message' => __('frontend.smtg_wt_wrng')]);
        }
    }
    
    // make this address as billing
    public function make_billing(Request $req) {
        $_address_id = $req->id;
        $type = $req->type;
        $d_address = Address::where(['id' => $_address_id, 'address_flag' => $req->type])->first();
        if ($_address_id && !empty($d_address)) {
            $customer = $req->customer_id ?? Session::get('customer_id');
            $data_to_save = [
                'address_flag' => 'B',
                'name' => $d_address->name,
                'cust_id' => $customer,
                'apartment_name' => $d_address->apartment_name,
                'street_name' => $d_address->street_name,
                'landmark' => $d_address->landmark,
                'postal_code' => $d_address->postal_code,
                'region' => $d_address->region,
                'phone' => $d_address->phone,
                'is_default' => 'Y',
                'created_at' => Carbon::now()
            ];
            Address::insert($data_to_save);
            return response()->json(['status' => 1, 'message' => __('frontend.set_bill_succ')]);
        } else {
            return response()->json(['status' => 0, 'message' => __('frontend.smtg_wt_wrng')]);
        }
    }
    
    public function change_default(Request $req) {
        $_address_id = $req->id;
        $customer = $req->customer_id ?? Session::get('customer_id');
        if (!$customer) {
            return response()->json(['error' => 0, 'message' => __('frontend.smtg_wt_wrng')]);
        }
        $addres = Address::where(['id' => $_address_id])->first();
        if (!$addres) {
            return response()->json(['error' => 0, 'message' => __('frontend.sel_address_not_found')]);
        }
        if ($_address_id && $addres) {
            Address::where(['cust_id' => $customer, 'address_flag' => $addres->address_flag])
                    ->update([
                        'is_default' => 'N'
            ]);
            Address::where(['id' => $_address_id])
                    ->update([
                        'is_default' => 'Y'
            ]);

            return response()->json(['success' => 1, 'message' => __('frontend.set_dt_succ') ]);
        } else {
            return response()->json(['error' => 0, 'message' => __('frontend.smtg_wt_wrng')]);
        }
    }
    
    public function add_to_savelater(Request $request) {
        $region_id = Session::get('region_id');
        $customer = Session::get('customer_id');
        $product_id = $request->product_id;
        $type = $request->type;
        $variant_id = $request->variant_id;
        $variant_id = $type == 'complex' ? $variant_id : NULL;

        $cartKey = Session::get('cartKey');
        $cartKey = $cartKey->cartKey ?? '';

        $cart_exists = CartKey::with('parent')
            ->where('key', '=', $cartKey)
            ->first();

        if (!$cart_exists || !isset($cart_exists->parent)) {
            return response()->json(['success' => FALSE, 'message' =>__('frontend.cart_empty')]);
        }

        $cart_id = $cart_exists->parent->id;

        if (!$product_id || !$type) {
            return response()->json(['success' => FALSE, 'message' => __('frontend.pdt_nt_fnd')]);
        }

        $cartItem = CartItems::where('product_id', $product_id)
            ->where('variant_id', $variant_id)
            ->where('cart_id', '=', $cart_id)
            ->first();
        $qty = $cartItem->item_count;
        
        if (!$cartItem) {
            return response()->json(['success' => FALSE, 'message' => __('frontend.pdt_nt_fnd')]);
        }
        
        $saveLater = SaveLater::where('customer_id', $customer)
                ->where('product_id', $product_id)->where('region_id', $region_id) 
                ->first();
        if ($saveLater) {
            $saveLater->variant_id = $variant_id;
            $saveLater->item_count = $qty;
            $saveLater->region_id = $region_id;
            $saveLater->save();
            $message = 'Save later product updated';
        } else {
            $saveLater = new SaveLater();
            $saveLater->customer_id = $customer;
            $saveLater->product_id = $product_id;
            $saveLater->region_id = $region_id;
            $saveLater->variant_id = $variant_id;
            $saveLater->item_count = $qty;
            $saveLater->save();
            $message = 'Product added to save later';
        }

        $response = $cartItem->delete();
        if( $response ) {
            $grant_total = CartItems::where('cart_id', '=', $cart_id)
                ->sum('grant_total');
            $sub_total = CartItems::where('cart_id', '=', $cart_id)
                ->sum('sub_total');
            $total_item_count = CartItems::where('cart_id', '=', $cart_id)
                ->where('item_count', '>', 0)
                ->count();
            $cart_exists->parent->grant_total = $grant_total;
            $cart_exists->parent->sub_total = $sub_total;
            $cart_exists->parent->item_count = $total_item_count;
            $cart_exists->parent->save();
        }
            
            return response()->json(['success' => 1, 'message' => $message]);
    }
    
    public function delete_branch(Request $req) {
        $branch = CustomerBranch::where('id', '=', $req->id)->firstOrFail();
        $customer_id = $branch->customer_id;
        if ($branch) {
            if($customer_id){
               $customer = Customer::where('id', '=', $customer_id)->firstOrFail(); 
               $address = Address::where('cust_id', '=', $customer_id)->get(); 
               if($customer){
                   $customer->delete();
               }
               if($address){
                   $address->each->delete();
               }
            }
            $branch->delete();
            return response()->json(['status' => 1, 'message' =>__('frontend.brch_del_succ') ]);
        } else {
            return response()->json(['status' => 0, 'message' => __('frontend.smtg_wt_wrng')]);
        }
    }
    
    public function branch_login_credentials(Request $request) {
        $cust_id = $request->id;
        if (!$cust_id) {
            return response()->json(['status' => 0, 'message' => __('frontend.brch_nt_fnd')]);
        }

        $data = Customer::where('id', $cust_id)->first();
        if (!$data) {
            return response()->json(['status' => 0, 'message' => __('frontend.branch_not_exist')]);
        }
        $gen_password = $this->generatePassword();
        $data->password = md5($gen_password);
        $data->save();
        $subject = 'Welcome to EASIFY!';
        $url = URL::to('/');
        SendEmail::generateEmail($data->email, $gen_password, $data->cust_name, $subject, 'customer', $url);
        return response()->json(['status' => 1, 'message' => __('frontend.login_details_send')]);
    }
     private function generatePassword() {
        $password = mt_rand(111111, 999999);
        return $password;
    }

}