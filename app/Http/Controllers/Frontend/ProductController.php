<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Product;
use App\Models\Attribute;
use App\Models\CategoryAttributes;
use App\Ads;
use App\Models\Variant;
use App\Models\ProductPrice;
use Config;
use DB;
use Exception;
use Illuminate\Support\Facades\Session;
use App\Events\LangSwitched;
use App\Models\ProductCategories;
use App\Models\RecentlyViewedProducts;
use App\Models\ProductViewCount;
use App\Models\WishList;
use Cookie;

    class ProductController extends Controller {

    // Category
    public function index(Request $req) {
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
        $region_id = Session::get('region_id');
        $slug = request()->segment(2);
        $product_data = Product::with(['lang' => function ($query) use ($lang, $region_id) {
        $query->where('language', $lang);
        }])->with('photos')
        ->with(['product_attribute' => function ($query) use($lang) {
        $query->with(['lang' => function($query) use ($lang) {
        $query->where('language', $lang);
        }])->with(['attribute' => function ($query) use($lang) {
        $query->with(['lang' => function($query) use ($lang) {
        $query->where('language', $lang);
        }]);
        }])->with(['variant' => function ($query) use($lang) {
        $query->where('language', $lang);
        }]);
        }])->with(['variant_price' => function($query) use($lang) {
        $query->with(['variant_lang' => function($query) use($lang) {
        $query->where('language', 'en');
        }]);
        }])->with(['variant' => function($query) use($region_id){
                $query->with(['pdt_branch' => function($query) use($region_id){
                  if ($region_id) {
                  $query->where('region_id', $region_id);  
                  } 
                }]);
                }])->with('supplier')->with('commission')->where('slug', $slug)->first();

        //add products to recently viewed table
        $AddProducts = $this->add_recent_products($lang, $product_data->id);
        //similar product
        $similarProducts = $this->similar_products($lang, $product_data);

        // recently viewed products
        $recently_viewed = $this->recently_viewed($lang, $product_data);

        return view('frontend.product', compact('lang', 'product_data', 'similarProducts', 'recently_viewed'));
    }

    private function add_recent_products($lang, $product_id) {
        $cookie_id = Cookie::get('easify_session');
        $cookie_id = $cookie_id ? $cookie_id : '';
        $most_viewed_products_count = ProductViewCount::select('view_count')->where('product_id', $product_id)->first();
        if (isset($most_viewed_products_count) && $most_viewed_products_count->view_count > 0) {
        $view_count = $most_viewed_products_count->view_count + 1;
        ProductViewCount::where('product_id', $product_id)
        ->update([
        "view_count" => $view_count
        ]);
        } else {
        ProductViewCount::create([
        'product_id' => $product_id,
        'view_count' => 1,
        'created_at' => Carbon::now()
        ]);
        }
        RecentlyViewedProducts::create([
        'browser_cookie_id' => $cookie_id,
        'product_id' => $product_id,
        'created_at' => Carbon::now()
        ]);

        return true;
    }

    private function similar_products($lang, $product_data) {
        $region_id = Session::get('region_id');
        
        $category = explode(",", str_replace(
        array("{{", "}}"), array(" ", " ,"), $product_data->category_ids
        ));

        $list_id = ProductCategories::whereIn('category_id', $category)->select('product_id')->get();
        foreach ($list_id as $item) {
        $tmpArray[] = $item->product_id;
        }
        $query = Product::with(['lang' => function ($query) use ($lang) {
        $query->where('language', $lang);
        }])
        ->with(['variant_price' => function($query) use($lang) {
        $query->with(['variant_lang' => function($query) use($lang) {
        $query->where('language', 'en');
        }]);
        }])->with(['variant' => function($query) use($region_id){
        $query->with(['pdt_branch' => function($query) use($region_id){
          if ($region_id) {
          $query->where('region_id', $region_id);  
          }  
        }]);
        }]);
        $query->with('commission');
        if ($region_id) {
        $query->where('supplier_branch_region', 'like', "%" . '{{' . $region_id . '}}' . "%");
         }
        $query->WhereIn('id', array_unique($tmpArray))
        ->where('id', '!=', $product_data->id)->where('status', 'active')->where('supplier_status', 'publish');
        $similarProducts = $query->inRandomOrder()
        ->limit(12)
        ->get();
        return $similarProducts;
    }

    private function recently_viewed($lang, $product_data) {
        $region_id = Session::get('region_id');
        $cookie_id = Cookie::get('easify_session');
        $cookie_id = $cookie_id ? $cookie_id : '';
        $recently_viewed_products = RecentlyViewedProducts::with(['product' => function ($query) use($lang, $region_id) {
        $query->with(['lang' => function($query) use ($lang) {
        $query->where('language', $lang);
        }])->with(['variant_price' => function($query) use($lang) {
        $query->with(['variant_lang' => function($query) use($lang) {
        $query->where('language', 'en');
        }]);
        }])->with(['variant' => function($query) use($region_id){
                $query->with(['pdt_branch' => function($query) use($region_id){
                  if ($region_id) {
                  $query->where('region_id', $region_id);  
                  }  
                }]);
                }]);
        if ($region_id) {
        $query->where('supplier_branch_region', 'like', "%" . '{{' . $region_id . '}}' . "%");
         }
        $query->with('commission')->where('status', 'active');
        }])->where('product_id', '!=', $product_data->id)->where('browser_cookie_id', '=', $cookie_id)
        ->groupBy('product_id')->orderBy('created_at', 'desc')->limit(12)->get();
        return $recently_viewed_products;
    }

    }
                                                