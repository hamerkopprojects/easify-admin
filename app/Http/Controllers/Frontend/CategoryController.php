<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Product;
use App\Models\Attribute;
use App\Models\CategoryAttributes;
use App\Ads;
use App\Models\Variant;
use App\Models\ProductPrice;
use App\Models\Supplier;
use Config;
use DB;
use Illuminate\Support\Facades\Session;
use App\Helpers\General\CollectionHelper;

class CategoryController extends Controller {

    const pagination = 6;

    // Category
    public function index(Request $req) {
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
        $cat_slug = request()->segment(2);
        $region_id = Session::get('region_id');
        $category_name1 = $category_name2 = $category_name3 = $cat_slug2 = $cat_slug3 = '';
        $cat1 = Category::with(['lang' => function ($query) use ($lang) {
                        $query->where('language', $lang);
                    }])->where(['slug' => $cat_slug, 'status' => 'active'])->first();
        $category_id1 = $cat1->id;
        $parent_cat_id1 = $cat1->parent_id;
        $category_name = $category_name1 = $cat1->lang[0]->name;
        if ($parent_cat_id1 != '') {
            $cat2 = Category::with(['lang' => function ($query) use ($lang) {
                            $query->where('language', $lang);
                        }])->where(['id' => $parent_cat_id1, 'status' => 'active'])->first();
            $category_id2 = $cat2->id;
            $parent_cat_id2 = $cat2->parent_id;
            $category_name2 = $cat2->lang[0]->name;
            $cat_slug2 = $cat2->slug;
            if ($parent_cat_id2 != '') {
                $cat3 = Category::with(['lang' => function ($query) use ($lang) {
                                $query->where('language', $lang);
                            }])->where(['id' => $parent_cat_id2, 'status' => 'active'])->first();
                $category_id3 = $cat3->id;
                $parent_cat_id3 = $cat3->parent_id;
                $category_name3 = $cat3->lang[0]->name;
                $cat_slug3 = $cat3->slug;
                $category_filter_id = $category_id2;
            } else {
                $category_filter_id = $category_id1;
            }
        } else {
            $category_filter_id = $category_id1;
        }

        $brands = Brand::with(['lang' => function ($query) use($lang) {
                                $query->where('language', $lang);
                            }])->withCount(['product' => function($b) use($category_id1, $region_id) {
                                $b->Where('category_ids', 'like', "%" . '{{' . $category_id1 . '}}' . "%");
                                if ($region_id) {
                                    $b->where('supplier_branch_region', 'like', "%" . '{{' . $region_id . '}}' . "%");
                                }
                                $b->where('supplier_status', 'publish');
                                $b->Where('status', 'active');
                            }])
                        ->where(['status' => 'active'])->get();


        $by_category = Category::with(['lang' => function ($query) use($lang) {
                        $query->where('language', $lang);
                    }])->where(['parent_id' => $category_filter_id, 'status' => 'active'])->get();

        $suppliers = Supplier::where(['role_id' => 5, 'status' => 'active', 'region_id' => $region_id])->get();
        $query = Product::with(['lang' => function ($query) use($lang) {
                        $query->where('language', $lang);
                    }])->with(['variant_price' => function($query) use($lang) {
                $query->with(['variant_lang' => function($query) use($lang) {
                        $query->where('language', 'en');
                    }]);
            }])->with(['variant' => function($query) use($region_id){
                $query->with(['pdt_branch' => function($query) use($region_id){
                  if ($region_id) {
                  $query->where('region_id', $region_id);  
                  }
                }]);
                }])->with('commission');
                if ($region_id) {
                    $query->where('supplier_branch_region', 'like', "%" . '{{' . $region_id . '}}' . "%");
                }
                $pdt_info = $query->where('category_ids', 'like', "%" . '{{' . $category_id1 . '}}' . "%")
                        ->where('status', 'active')
                        ->where('supplier_status', 'publish')
                        ->where('deleted_at', null)
                        ->orderBy('id', 'desc')
                        ->paginate(static::pagination);

                $bread_crumb = array(0 => array('bread_crumb' => $category_name3, 'slug' => $cat_slug3),
                    1 => array('bread_crumb' => $category_name2, 'slug' => $cat_slug2),
                );
                $slug_id = $category_filter_id;
                $parent_cat_id = $category_id1;
                return view('frontend.category', compact('parent_cat_id', 'category_name', 'brands', 'by_category', 'pdt_info', 'bread_crumb', 'lang', 'slug_id', 'cat_slug', 'suppliers', 'region_id'));
            }

            public function search(Request $req) {
                $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
                $parent_cat_id = $req->parent_cat_id;
                $slug_id = $req->slug_id;
                $cat_slug = $req->cat_slug;
                $cat_id = $req->cat_id;
                $brand_id = $req->brand_id;
                $supplier_id = $req->supplier_id;
                $minval = $req->minval;
                $maxval = $req->maxval;
                $filter_cat_ids = $filter_brand_ids = $filter_supplier_ids = '';
                $region_id = Session::get('region_id');
                if (!empty($cat_id)) {
                    $filter_cat_ids = implode(',', $cat_id);
                }
                if (!empty($brand_id)) {
                    $filter_brand_ids = implode(',', $brand_id);
                }
                if (!empty($supplier_id)) {
                    $filter_supplier_ids = implode(',', $supplier_id);
                }
                $category_name = $req->category_name;

                $brnd = Brand::with(['lang' => function ($query) use($lang) {
                                $query->where('language', $lang);
                            }]);
                if (!empty($cat_id)) {
                    $brnd->withCount(['product' => function($b) use($cat_id, $region_id) {
                            for ($i = 0; $i < count($cat_id); $i++) {
                                if ($i == 0) {
                                    $b->Where('category_ids', 'like', "%" . '{{' . $cat_id[$i] . '}}' . "%");
                                } else {
                                    $b->orWhere('category_ids', 'like', "%" . '{{' . $cat_id[$i] . '}}' . "%");
                                }
                            }
                            if ($region_id) {
                             $b->where('supplier_branch_region', 'like', "%" . '{{' . $region_id . '}}' . "%");
                            }
                            $b->where('supplier_status', 'publish');
                            $b->Where('status', 'active');
                        }]);
                } else {
                    $brnd->withCount(['product' => function($b) use($parent_cat_id, $region_id) {
                            $b->where('category_ids', 'like', "%" . '{{' . $parent_cat_id . '}}' . "%");
                            if ($region_id) {
                             $b->where('supplier_branch_region', 'like', "%" . '{{' . $region_id . '}}' . "%");
                            }
                            $b->where('supplier_status', 'publish');
                            $b->Where('status', 'active');
                        }]);
                }

                $brands = $brnd->where(['status' => 'active'])->get();

                $by_category = Category::with(['lang' => function ($query) use($lang) {
                                $query->where('language', $lang);
                            }])->where(['parent_id' => $slug_id, 'status' => 'active'])->get();

                $suppliers = Supplier::where(['role_id' => 5, 'status' => 'active', 'region_id' => $region_id])->get();


                $q = Product::with(['lang' => function ($query) use($lang) {
                                $query->where('language', $lang);
                            }]);

                $q->whereHas('variant_price', function ($query) use ($minval, $maxval, $lang) {
                    $query->with(['variant_lang' => function($query) use($lang) {
                            $query->where('language', 'en');
                        }]);
                    if (isset($minval) && isset($maxval)) {
                        $query->whereBetween('price', [$minval, $maxval]);
                    }
                });
                $q->with(['variant_price' => function($query) use ($minval, $maxval, $lang) {
                        $query->with(['variant_lang' => function($query) use($lang) {
                                $query->where('language', 'en');
                            }]);
                        if (!empty($minval) && !empty($maxval)) {
                            $query->whereBetween('price', [$minval, $maxval]);
                        }
                    }]);
                $q->with(['variant' => function($query) use($region_id){
                $query->with(['pdt_branch' => function($query) use($region_id){
                  if ($region_id) {
                  $query->where('region_id', $region_id);  
                  }  
                }]);
                }])->with('commission');
                        if (!empty($cat_id)) {
                            $q->where(function ($v) use ($cat_id) {
                                for ($i = 0; $i < count($cat_id); $i++) {
                                    if ($i == 0) {
                                        $v->Where('category_ids', 'like', "%" . '{{' . $cat_id[$i] . '}}' . "%");
                                    } else {
                                        $v->orWhere('category_ids', 'like', "%" . '{{' . $cat_id[$i] . '}}' . "%");
                                    }
                                }
                            });
                        } else {
                            $q->where('category_ids', 'like', "%" . '{{' . $req->parent_cat_id . '}}' . "%");
                        }
                        if (!empty($brand_id)) {
                            $q->where(function ($b) use ($brand_id) {
                                for ($j = 0; $j < count($brand_id); $j++) {
                                    if ($j == 0) {
                                        $b->Where('brand_id', '=', $brand_id[$j]);
                                    } else {
                                        $b->orWhere('brand_id', '=', $brand_id[$j]);
                                    }
                                }
                            });
                        }
                        if (!empty($supplier_id)) {
                            $q->where(function ($c) use ($supplier_id) {
                                for ($x = 0; $x < count($supplier_id); $x++) {
                                    if ($x == 0) {
                                        $c->Where('supplier_id', '=', $supplier_id[$x]);
                                    } else {
                                        $c->orWhere('supplier_id', '=', $supplier_id[$x]);
                                    }
                                }
                            });
                        }
                        if ($region_id) {
                            $q->where('supplier_branch_region', 'like', "%" . '{{' . $region_id . '}}' . "%");
                        }

                        if (!empty($req->sort)) {
                            $q = $q->get();
                            if ($req->sort == "low-high") {
                                $q = $q->sortBy(function ($q, $key) {
                                    return (int) $q['variant_price'][0]['price'];
                                });
                            }
                            if ($req->sort == "high-low") {
                                $q = $q->sortByDesc(function ($q, $key) {
                                    return (int) $q['variant_price'][0]['price'];
                                });
                            }
                            $q->where('status', 'active')
                                    ->where('deleted_at', null);
                            $pdt_info = CollectionHelper::paginate($q, static::pagination);
                        } else {
                            $pdt_info = $q->where('status', 'active')
                                    ->where('supplier_status', 'publish')
                                    ->where('deleted_at', null)
                                    ->orderBy('id', 'desc')
                                    ->paginate(static::pagination);
                        }
                        $search = 'filter';
                        $list_type = $req->list_type ? $req->list_type : '';
                        $sort = $req->sort ? $req->sort : '';
                        if ($req->list_type == 'list_view') {
                            return view('frontend.category.filter_list', compact('pdt_info', 'category_name', 'filter_cat_ids', 'parent_cat_id', 'filter_brand_ids', 'brands', 'by_category', 'suppliers', 'slug_id', 'search', 'cat_id', 'brand_id', 'cat_slug', 'supplier_id', 'filter_supplier_ids', 'lang', 'list_type', 'sort', 'minval', 'maxval', 'region_id'));
                        } else {
                            return view('frontend.category.filter', compact('pdt_info', 'category_name', 'filter_cat_ids', 'parent_cat_id', 'filter_brand_ids', 'brands', 'by_category', 'suppliers', 'slug_id', 'search', 'cat_id', 'brand_id', 'cat_slug', 'supplier_id', 'filter_supplier_ids', 'lang', 'list_type', 'sort', 'minval', 'maxval', 'region_id'));
                        }
                    }

                    public function loadMore(Request $req) {
                        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
                        $cat_id = $req->cat_id;
                        $brand_id = $req->brand_id;
                        $parent_cat_id = $req->parent_cat_id;
                        $supplier_id = $req->supplier_id;
                        $minval = $req->minval;
                        $maxval = $req->maxval;
                        $region_id = Session::get('region_id');
                        if (!empty($cat_id)) {
                            $cat_id = explode(',', $cat_id);
                        }
                        if (!empty($brand_id)) {
                            $brand_id = explode(',', $brand_id);
                        }
                        if (!empty($supplier_id)) {
                            $supplier_id = explode(',', $supplier_id);
                        }
                        $q = Product::with(['lang' => function ($query) use($lang) {
                                        $query->where('language', $lang);
                                    }]);
                        $q->whereHas('variant_price', function ($query) use ($minval, $maxval, $lang) {
                            $query->with(['variant_lang' => function($query) use($lang) {
                                    $query->where('language', 'en');
                                }]);
                            if (isset($minval) && isset($maxval)) {
                                $query->whereBetween('price', [$minval, $maxval]);
                            }
                        });
                        $q->with(['variant_price' => function($query) use ($minval, $maxval, $lang) {
                                $query->with(['variant_lang' => function($query) use($lang) {
                                        $query->where('language', 'en');
                                    }]);
                                if (!empty($minval) && !empty($maxval)) {
                                    $query->whereBetween('price', [$minval, $maxval]);
                                }
                            }]);
                       $q->with(['variant' => function($query) use($region_id){
                        $query->with(['pdt_branch' => function($query) use($region_id){
                          if ($region_id) {
                          $query->where('region_id', $region_id);  
                          }
                        }]);
                        }])->with('commission');
                        if (!empty($cat_id)) {
                            $q->where(function ($v) use ($cat_id) {
                                for ($i = 0; $i < count($cat_id); $i++) {
                                    if ($i == 0) {
                                        $v->Where('category_ids', 'like', "%" . '{{' . $cat_id[$i] . '}}' . "%");
                                    } else {
                                        $v->orWhere('category_ids', 'like', "%" . '{{' . $cat_id[$i] . '}}' . "%");
                                    }
                                }
                            });
                        } else {
                            $q->where('category_ids', 'like', "%" . '{{' . $req->parent_cat_id . '}}' . "%");
                        }

                                if (!empty($brand_id)) {
                                    $q->where(function ($b) use ($brand_id) {
                                        for ($j = 0; $j < count($brand_id); $j++) {
                                            if ($j == 0) {
                                                $b->Where('brand_id', '=', $brand_id[$j]);
                                            } else {
                                                $b->orWhere('brand_id', '=', $brand_id[$j]);
                                            }
                                        }
                                    });
                                }

                                if (!empty($supplier_id)) {
                                    $q->where(function ($query) use ($supplier_id) {
                                        for ($x = 0; $x < count($supplier_id); $x++) {
                                            if ($x == 0) {
                                                $query->Where('supplier_id', '=', $supplier_id[$x]);
                                            } else {
                                                $query->orWhere('supplier_id', '=', $supplier_id[$x]);
                                            }
                                        }
                                    });
                                }

                                if ($region_id) {
                                    $q->where('supplier_branch_region', 'like', "%" . '{{' . $region_id . '}}' . "%");
                                }

                                if (!empty($req->sort)) {
                                    $q = $q->get();
                                    if ($req->sort == "low-high") {
                                        $q = $q->sortBy(function ($q, $key) {
                                            return (int) $q['variant_price'][0]['price'];
                                        });
                                    }
                                    if ($req->sort == "high-low") {
                                        $q = $q->sortByDesc(function ($q, $key) {
                                            return (int) $q['variant_price'][0]['price'];
                                        });
                                    }
                                    $q->where('status', 'active')
                                            ->where('deleted_at', null);
                                    $pdt_info = CollectionHelper::paginate($q, static::pagination);
                                } else {
                                    $pdt_info = $q->where('status', 'active')
                                            ->where('supplier_status', 'publish')
                                            ->where('deleted_at', null)
                                            ->orderBy('id', 'desc')
                                            ->paginate(static::pagination);
                                }
                                $page = $req->page;
                                $list_type = $req->list_type ? $req->list_type : '';
                                $sort = $req->sort ? $req->sort : '';
                                if ($req->list_type == 'list_view') {
                                    return view('frontend.category.loadmore_list', compact('pdt_info', 'page', 'parent_cat_id', 'list_type', 'sort', 'minval', 'maxval', 'lang', 'region_id'));
                                } else {
                                    return view('frontend.category.loadmore', compact('pdt_info', 'page', 'parent_cat_id', 'list_type', 'sort', 'minval', 'maxval', 'lang', 'region_id'));
                                }
                            }

                        }
                        