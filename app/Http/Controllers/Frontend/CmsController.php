<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Config;
use DB;
use App\Customer;
use App\Address;
use App\Models\Category;
use App\Models\Region;
use App\Models\Pages;
use App\Models\Faqs;
use Exception;
use Facade\FlareClient\Http\Response;
use Illuminate\Support\Facades\Session;
use Validator;

class CmsController extends Controller {

    public function __construct() {
        
    }

    public function index(Request $req) {
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';

        $page_slug = request()->segment(2);
        if ($page_slug == 'faqs') {
            $faqs = Faqs::with(['lang' => function ($query) use ($lang) {
                                    $query->where('language', $lang);
                                }])->where(['status' => 'active'])
                            ->with('appType')->get();
            return view('frontend/cms/faqs', compact('lang', 'faqs'));
        } else {
            $pages = Pages::with(['lang' => function ($query) use ($lang) {
                            $query->where('language', $lang);
                        }])->where(['available_for' => 'Website', 'slug' => $page_slug])->first();

            return view('frontend/cms/cms', compact('lang', 'pages'));
        }
    }

}
