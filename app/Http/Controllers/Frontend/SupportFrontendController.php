<?php

namespace App\Http\Controllers\Frontend;

use Carbon\Carbon;
use App\Models\SupportChat;
use Illuminate\Http\Request;
use App\Models\SupportRequests;
use App\Http\Controllers\Controller;
use App\Http\Responses\SuccessResponse;
use App\Http\Responses\SuccessWithData;
use Illuminate\Support\Facades\Session;

class SupportFrontendController extends Controller
{
    public function getSupportList()
    {
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
        $customer = Session::get('customer_id');
        $support_list  = SupportRequests::where('submitted_by',$customer)->orderBy('created_at', 'desc')->get();
        return view ('frontend.supportchat.list',compact('support_list','lang'));
    }

    public function getDetailsChat($support_id)
    {
        $customer = Session::get('customer_id');
        $chat = SupportChat::where('request_id',$support_id)
            ->orderBy('created_at')
            ->get();

        $now = now();

        $chat->map(function ($msg) use ($now) {
            $msg->message = nl2br($msg->message);
            $msg->time = Carbon::parse($msg->created_at)->diffForHumans($now);
            return $msg;
        });

        return new SuccessWithData($chat);

    }

    public function sendChat(Request $req,$id)
    {
        SupportChat::create([
            'request_id' => $id,
            'message' => $req->message,
            'user_type' => 'website',
        ]);


        return new SuccessResponse(__('frontend.msg_snd'));
    }
}
