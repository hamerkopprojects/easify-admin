<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Elasticsearch\ClientBuilder;

class ElasticSearchController extends Controller
{
    public function index(Request $req)
    {
        $client = ClientBuilder::create()->build();
       
        $result = $client->search([
           "index"=>"suggestion_easify",
            "from" => 0, "size" => 10,
            "body"=>[
                'query' => [
                    'query_string' => [
                        'query' => "*".$req->term."*",
                        'fields' => ['product_name','category_name','brand_name']
                        
                    ]
                ]
                
            ]
        ]);

       $resp = $result['hits']['hits'];
        
        $match =[];
        foreach ($resp as $key => $value) {
            if($value['_source']['brand_name']){
                if (strpos(strtolower($value['_source']['brand_name']), strtolower($req->term)) !== false) {
                    $highlighted = '<b>'.$req->term.'</b>';
                    $content = str_ireplace( $req->term, $highlighted, $value['_source']['brand_name'] );
                    $link ="<a href=#>".strtolower($content)."</a>";
                    $match['brand'][] =  $link;
                }
            }
            if($value['_source']['product_name']){
                if (strpos(strtolower($value['_source']['product_name']), strtolower($req->term)) !== false) {
                    $path = route('products.detail',$value['_source']['product_slug']);
                    $highlighted = '<b>'.$req->term.'</b>';
                    $content = str_ireplace( $req->term, $highlighted, $value['_source']['product_name'] );
                    $link ="<a href=$path>".strtolower($content)."</a>";
                    $match['product'][] =  $link;
                }
            }
            if($value['_source']['category_name']){
                if (strpos(strtolower($value['_source']['category_name']), strtolower($req->term)) !== false) {
                    $path = route('category.show',$value['_source']['category_slug']);
                    $highlighted = '<b>'.$req->term.'</b>';
                    $content = str_ireplace( $req->term, $highlighted, $value['_source']['category_name'] );
                    $link ="<a href=$path>".strtolower($content)."</a>";
                    $match['category'][] = $link;
                }
            }
        }

       // print_r($match);exit;
        
        $html ='';
        if(isset($match)){
            foreach ($match as $key => $value) {
               
                if($key == "brand"){
                    foreach ($value as $key1 => $val1) {
                        $html .= "<li class='list-group-item link-class'>$val1</span></li>";
                    }
                }
                if($key == "product"){
                    foreach ($value as $key2 => $val2) {
                        $html .= "<li class='list-group-item link-class'>$val2</li>";
                    }
                }
                if($key == "category"){
                    foreach ($value as $key3 => $val3) {
                        $html .= "<li class='list-group-item link-class'>$val3</li>";
                    }
                }
            }
            $html .= '<li class="list-group-item viewall-txt">View all search results for "<b>'.$req->term.'</b>"</li>';
            return $html;
        }else{
            $html .= '<li class="list-group-item viewall-txt">View all search results for "<b>'.$req->term.'</b>"</li>';
            return $html;
        }
        
    }

}
