<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Brand;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\ProductPrice;
use App\Models\PromotionalProduct;
use App\Models\Supplier;
use App\Http\Controllers\Controller;
use Config;
use Illuminate\Support\Facades\Session;
use App\Helpers\General\CollectionHelper;

class PromotionalProductsController extends Controller {

    protected $products;
    const pagination = 6;

    public function __construct(PromotionalProduct $products) {
        $this->products = $products;
    }

    public function get(Request $req) {
        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
        $region_id = Session::get('region_id');
        
        $promo = PromotionalProduct::where('deleted_at', null)->select('product_id')->get()->toArray();

        $brands = Brand::with(['lang' => function ($query) use($lang, $promo) {
                                $query->where('language', $lang);
                            }])
                        ->withCount(['product' => function($b) use ($promo, $region_id) {
                                $b->Where('status', 'active')->where('supplier_status', 'publish')
                                ->whereIn('id', $promo);
                                if ($region_id) {
                                    $b->where('supplier_branch_region', 'like', "%" . '{{' . $region_id . '}}' . "%");
                                }
                            }])
                        ->where(['status' => 'active'])->get();

        $categories = Category::with(['lang' => function ($query) use($lang, $promo) {
                                $query->where('language', $lang);
                            }])
                        ->withCount(['product' => function($b) use ($promo, $region_id) {
                                $b->Where('status', 'active')->where('supplier_status', 'publish')
                                ->whereIn('id', $promo);
                                if ($region_id) {
                                    $b->where('supplier_branch_region', 'like', "%" . '{{' . $region_id . '}}' . "%");
                                }
                            }])
                        ->where(['status' => 'active'])->get();

        $query = Product::with(['lang' => function ($query) use($lang) {
                        $query->where('language', $lang);
                    }])
                ->with(['variant_price' => function($query) use($lang) {
                $query->with(['variant_lang' => function($query) use($lang) {
                        $query->where('language', 'en');
                    }]);
            }])->with(['variant' => function($query) use($region_id){
                $query->with(['pdt_branch' => function($query) use($region_id){
                  if ($region_id) {
                  $query->where('region_id', $region_id);  
                  }
                }]);
                }])->with('commission');
                if ($region_id) {
                    $query->where('supplier_branch_region', 'like', "%" . '{{' . $region_id . '}}' . "%");
                }
                $promotional_products = $query->where('status', 'active')->where('supplier_status', 'publish')
                        ->where('deleted_at', null)
                        ->orderBy('id', 'desc')
                        ->whereIn('id', $promo)
                        ->paginate(static::pagination);
                $suppliers = Supplier::where(['role_id' => 5, 'status' => 'active', 'region_id' => $region_id])->get();
                return view('frontend.promotional-product', compact('brands', 'categories', 'lang', 'promotional_products', 'suppliers'));
            }

            public function loadMore(Request $req) {
                $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
                $cat_id = $req->cat_id;
                $brand_id = $req->brand_id;
                $parent_cat_id = $req->parent_cat_id;
                $supplier_id = $req->supplier_id;
                $minval = $req->minval;
                $maxval = $req->maxval;
                $region_id = Session::get('region_id');
                $promo = PromotionalProduct::where('deleted_at', null)->select('product_id')->get()->toArray();
                $q = Product::with(['lang' => function ($query) use($lang) {
                                $query->where('language', $lang);
                            }]);
                $q->with(['variant_price' => function($query) use ($minval, $maxval, $lang) {
                        $query->with(['variant_lang' => function($query) use($lang) {
                                $query->where('language', 'en');
                            }]);
                        if (!empty($minval) && !empty($maxval)) {
                            $query->whereBetween('price', [$minval, $maxval]);
                        }
                    }])->with(['variant' => function($query) use($region_id){
                                $query->with(['pdt_branch' => function($query) use($region_id){
                                  if ($region_id) {
                                  $query->where('region_id', $region_id);  
                                  }  
                                }]);
                                }])->with('commission');
                        $q->whereIn('id', $promo);

                        if (!empty($cat_id)) {
                            $q->where(function ($v) use ($cat_id) {

                                for ($i = 0; $i < count($cat_id); $i++) {
                                    if ($i == 0) {
                                        $v->Where('category_id', '=', $cat_id[$i]);
                                    } else {
                                        $v->orWhere('category_id', '=', $cat_id[$i]);
                                    }
                                }
                            });
                        }
                        if (!empty($brand_id)) {
                            $q->where(function ($b) use ($brand_id) {

                                for ($j = 0; $j < count($brand_id); $j++) {
                                    if ($j == 0) {
                                        $b->Where('brand_id', '=', $brand_id[$j]);
                                    } else {
                                        $b->orWhere('brand_id', '=', $brand_id[$j]);
                                    }
                                }
                            });
                        }
                        if (!empty($supplier_id)) {
                            $q->where(function ($query) use ($supplier_id) {
                                for ($x = 0; $x < count($supplier_id); $x++) {
                                    if ($x == 0) {
                                        $query->Where('supplier_id', '=', $supplier_id[$x]);
                                    } else {
                                        $query->orWhere('supplier_id', '=', $supplier_id[$x]);
                                    }
                                }
                            });
                        }

                        if ($region_id) {
                            $q->where('supplier_branch_region', 'like', "%" . '{{' . $region_id . '}}' . "%");
                        }

                        if (!empty($req->sort)) {
                            $q = $q->get();
                            if ($req->sort == "low-high") {
                                $q = $q->sortBy(function ($q, $key) {
                                    return (int) $q['variant_price'][0]['price'];
                                });
                            }
                            if ($req->sort == "high-low") {
                                $q = $q->sortByDesc(function ($q, $key) {
                                    return (int) $q['variant_price'][0]['price'];
                                });
                            }
                            $q->where('status', 'active')
                                    ->where('deleted_at', null);
                            $promotional_products = CollectionHelper::paginate($q, static::pagination);
                        } else {
                            $promotional_products = $q->where('status', 'active')->where('supplier_status', 'publish')
                                    ->where('deleted_at', null)
                                    ->orderBy('id', 'desc')
                                    ->paginate(static::pagination);
                        }
                        $page = $req->page;
                        $list_type = $req->list_type ? $req->list_type : '';
                        $sort = $req->sort ? $req->sort : '';
                        if ($req->list_type == 'list_view') {
                            return view('frontend.promoproduct.loadmore_list', compact('promotional_products', 'page', 'container_type', 'list_type', 'sort', 'minval', 'maxval', 'lang', 'region_id'));
                        } else {
                            return view('frontend.promoproduct.loadmore', compact('promotional_products', 'page', 'container_type', 'list_type', 'sort', 'minval', 'maxval', 'lang', 'region_id'));
                        }
                    }

                    public function searchProduct(Request $req) {
                        $region_id = Session::get('region_id');
                        $lang = app()->getLocale() == 'en' ? 'en' : 'ar';
                        $cat_id = $req->cat_id;
                        $brand_id = $req->brand_id;
                        $supplier_id = $req->supplier_id;
                        $minval = $req->minval;
                        $maxval = $req->maxval;

                        $promo = PromotionalProduct::where('deleted_at', null)->select('product_id')->get()->toArray();

                        $brands = Brand::with(['lang' => function ($query) use($lang, $promo) {
                                                $query->where('language', $lang);
                                            }])
                                        ->withCount(['product' => function($b) use ($promo, $region_id) {
                                                $b->Where('status', 'active')->where('supplier_status', 'publish')
                                                ->whereIn('id', $promo);
                                                if ($region_id) {
                                                    $b->where('supplier_branch_region', 'like', "%" . '{{' . $region_id . '}}' . "%");
                                                }
                                            }])
                                        ->where(['status' => 'active'])->get();
                        $categories = Category::with(['lang' => function ($query) use($lang, $promo) {
                                                $query->where('language', $lang);
                                            }])
                                        ->withCount(['product' => function($b) use ($promo, $region_id) {
                                                $b->Where('status', 'active')->where('supplier_status', 'publish')
                                                ->whereIn('id', $promo);
                                                if ($region_id) {
                                                    $b->where('supplier_branch_region', 'like', "%" . '{{' . $region_id . '}}' . "%");
                                                }
                                            }])
                                        ->where(['status' => 'active'])->get();

                        $q = Product::with(['lang' => function ($query) use($lang, $region_id) {
                                        $query->where('language', $lang);
                                    }]);
                        $q->whereHas('variant_price', function ($query) use ($minval, $maxval, $lang) {
                            $query->with(['variant_lang' => function($query) use($lang) {
                                    $query->where('language', 'en');
                                }]);
                            if (isset($minval) && isset($maxval)) {
                                $query->whereBetween('price', [$minval, $maxval]);
                            }
                        });
                        $q->with(['variant_price' => function($query) use ($minval, $maxval, $lang) {
                                $query->with(['variant_lang' => function($query) use($lang) {
                                        $query->where('language', 'en');
                                    }]);
                                if (!empty($minval) && !empty($maxval)) {
                                    $query->whereBetween('price', [$minval, $maxval]);
                                }
                            }])->with(['variant' => function($query) use($region_id){
                                $query->with(['pdt_branch' => function($query) use($region_id){
                                  if ($region_id) {
                                  $query->where('region_id', $region_id);  
                                  } 
                                }]);
                                }])->with('commission');
                                if ($region_id) {
                                    $q->where('supplier_branch_region', 'like', "%" . '{{' . $region_id . '}}' . "%");
                                }
                                $q->whereIn('id', $promo);

                                if (!empty($cat_id)) {
                                    $q->where(function ($v) use ($cat_id) {

                                        for ($i = 0; $i < count($cat_id); $i++) {
                                            if ($i == 0) {
                                                $v->Where('category_id', '=', $cat_id[$i]);
                                            } else {
                                                $v->orWhere('category_id', '=', $cat_id[$i]);
                                            }
                                        }
                                    });
                                }
                                if (!empty($brand_id)) {
                                    $q->where(function ($b) use ($brand_id) {

                                        for ($j = 0; $j < count($brand_id); $j++) {
                                            if ($j == 0) {
                                                $b->Where('brand_id', '=', $brand_id[$j]);
                                            } else {
                                                $b->orWhere('brand_id', '=', $brand_id[$j]);
                                            }
                                        }
                                    });
                                }

                                if (!empty($supplier_id)) {
                                    $q->where(function ($c) use ($supplier_id) {
                                        for ($x = 0; $x < count($supplier_id); $x++) {
                                            if ($x == 0) {
                                                $c->Where('supplier_id', '=', $supplier_id[$x]);
                                            } else {
                                                $c->orWhere('supplier_id', '=', $supplier_id[$x]);
                                            }
                                        }
                                    });
                                }

                                if (!empty($req->sort)) {
                                    $q = $q->get();
                                    if ($req->sort == "low-high") {
                                        $q = $q->sortBy(function ($q, $key) {
                                            return (int) $q['variant_price'][0]['price'];
                                        });
                                    }
                                    if ($req->sort == "high-low") {
                                        $q = $q->sortByDesc(function ($q, $key) {
                                            return (int) $q['variant_price'][0]['price'];
                                        });
                                    }
                                    $q->where('status', 'active')
                                            ->where('deleted_at', null);
                                    $promotional_products = CollectionHelper::paginate($q, static::pagination);
                                } else {
                                    $promotional_products = $q->where('status', 'active')->where('supplier_status', 'publish')
                                            ->where('deleted_at', null)
                                            ->orderBy('id', 'desc')
                                            ->paginate(static::pagination);
                                }
                                $search = 'filter';
                                $list_type = $req->list_type ? $req->list_type : '';
                                $sort = $req->sort ? $req->sort : '';
                                $suppliers = Supplier::where(['role_id' => 5, 'status' => 'active', 'region_id' => $region_id])->get();
                                if ($req->list_type == 'list_view') {
                                    return view('frontend.promoproduct.filter_list', compact('brands', 'categories', 'lang', 'promotional_products', 'search', 'cat_id', 'brand_id', 'suppliers', 'supplier_id', 'sort', 'minval', 'maxval', 'region_id', 'list_type'));
                                } else {
                                    return view('frontend.promoproduct.filter', compact('brands', 'categories', 'lang', 'promotional_products', 'search', 'cat_id', 'brand_id', 'suppliers', 'supplier_id', 'sort', 'minval', 'maxval', 'region_id', 'list_type'));
                                }
                            }

                        }
                        