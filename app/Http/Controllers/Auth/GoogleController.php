<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use Socialite;
use Exception;
use App\Models\Customer;
use App\Services\SendEmail;
use Request;
use App\Events\CustomerAuthenticated;
use URL;

class GoogleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }
      
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleGoogleCallback()
    {
        try {
            $user = Socialite::driver('google')->with(['access_type' => 'offline'])->user();
            $finduser = Customer::where('google_id', $user->id)->first();
     
            if($finduser){
                event(new CustomerAuthenticated($finduser->id, $finduser->cust_name));
                return redirect('/');
     
            }else{
                $otp = $this->generateOtp();
                $gen_password = $this->generatePassword();
                $password = md5($gen_password); 
                $newUser = Customer::create([
                    'cust_name' => $user->name,
                    'cust_id' => $this->generateCustomerId(),
                    'business_type' => 1,
                    'email' => $user->email,
                    'google_id'=> $user->id,
                    'password' => $password,
                    'otp' => $otp
                ]);
    
               if ($newUser) {
                 event(new CustomerAuthenticated($newUser->id, $user->name));
                 $subject = 'Welcome to EASIFY!';
                 $url = URL::to('/');
                 SendEmail::generateEmail($user->email, $gen_password, $user->name, $subject, 'customer', $url);
               }
     
                return redirect('/');
            }
    
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }
    
    private function generateCustomerId() {
        $lastcustomer = Customer::select('cust_id')
                ->orderBy('id', 'desc')
                ->first();

        $lastId = 0;

        if ($lastcustomer) {
            $lastId = (int) substr($lastcustomer->cust_id, 10);
        }

        $lastId++;

        return 'EMTY-CUST-' . str_pad($lastId, 5, '0', STR_PAD_LEFT);
    }
    
    private function generateOtp() {
        $otp = mt_rand(1111, 9999);
        return 1234;
    }
    private function generatePassword() {
        $password = mt_rand(111111, 999999);
        return $password;
    }
}
