<?php

namespace App\Http\Controllers\Branch;

use Carbon\Carbon;
use App\Models\SupportChat;
use Illuminate\Http\Request;
use App\Models\SupportRequests;
use App\Http\Controllers\Controller;
use App\Http\Responses\SuccessResponse;
use App\Http\Responses\SuccessWithData;
use Illuminate\Support\Facades\Validator;

class SupportRequestController extends Controller
{
    public function index(Request $req)
    {
            
       $support=SupportRequests::where('app_type','branch')
                ->where('submitted_by',auth()->user()->id)
                ->paginate(20);

        return view('branch.supportchat.index',compact('support'));
    }
    public function create()
    {
        return view('branch.supportchat.create_form');
    }
    public function supportChatStore(Request $req)
    {
             
        $rules = [
            'subject' => 'required',
            'message' => 'required',
        ];
        $messages = [
            'subject.required' => 'Subject is required.',
            'message.required' => 'Message is required.',
        ];

        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $support_values = array(
                'subject' => $req->subject,
                'message' => $req->message,
                'submitted_by' => auth()->user()->id,
                'app_type' => 'branch',
                'created_at' => Carbon::now()
            );
            $support_request = SupportRequests::create($support_values);

            SupportChat::create([
                'request_id' => $support_request->id,
                'message' => $req->message,
                'user_type' => 'branch',
            ]);


            return response()->json(['status' => 1, 'message' => __('messages.admin.support_request_msg')]);
        }
    }
    public function getChat($id)
    {
        $chat = SupportChat::where('request_id', $id)
            ->orderBy('created_at')
            ->get();
        $details=SupportRequests::
            with('owner')
            ->where('id',$id)
            ->first();
        $now = now();

        $chat->map(function ($msg) use ($now) {
            $msg->message = nl2br($msg->message);
            $msg->time = Carbon::parse($msg->created_at)->diffForHumans($now);
            return $msg;
        });

        return new SuccessWithData([
            'chat'=>$chat,
            'details'=>$details
        ]);
    }
    public function send(Request $request, $id)
    {
        
        SupportChat::create([
            'request_id' => $id,
            'message' => $request->message,
            'user_type' => 'branch',
        ]);
        $support = SupportRequests::find($id);
        // if($support->app_type == 'driver')
        // {
        //     $this->notifyDriver($id);
        // }
            

        return new SuccessResponse(__('messages.admin.support_send_msg'));
    }
}
