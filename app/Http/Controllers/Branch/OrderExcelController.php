<?php

namespace App\Http\Controllers\Branch;

use PDF;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\Supplier;
use App\Models\SupportChat;
use Illuminate\Http\Request;
use App\Models\OtherSettings;
use App\Models\SupportRequests;
use App\Exports\BranchOrderExport;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;

class OrderExcelController extends Controller
{
    public function downloadExcel()
    {
        return Excel::download(new BranchOrderExport, 'Orders.xlsx');
    }

    public function supportChat(Request $req)
    {
        $order_id = $req->order_id;

        return view('branch.order.details.support', compact('order_id'));
    }

    public function supportChatStore(Request $req)
    {
        // dd($req->toArray());
        $order_id = $req->order_id;
        $rules = [
            'subject' => 'required',
            'message' => 'required',
        ];
        $messages = [
            'subject.required' => 'Subject is required.',
            'message.required' => 'Message is required.',
        ];

        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $support_values = array(
                'subject' => $req->subject,
                'message' => $req->message,
                'submitted_by' => auth()->user()->id,
                'app_type' => 'branch',
                'created_at' => Carbon::now()
            );
            if ($order_id) {
                $support_values['order_id'] = $order_id;
            }
            $support_request = SupportRequests::create($support_values);

            SupportChat::create([
                'request_id' => $support_request->id,
                'message' => $req->message,
                'user_type' => 'branch',
            ]);


            return response()->json(['status' => 1, 'message' => 'Support request added successfully']);
        }
    }

    public function OrderPdf($id)
    {
        $lang = "en";
        $details = Order::with(['items' => function ($query) use ($lang) {
                $query->with('driver')
                    ->with(['product' => function ($query) use ($lang) { }])
                    ->where('branch_id', auth()->user()->id);
            }])
            ->with('owner')
            ->with(['cancellation' => function ($q) {
                $q->with(['lang' => function ($q) {
                    $q->where('language', 'en');
                }]);
            }])
            ->where('id', $id)
            ->first();
        $supplier = Supplier::where('id', auth()->user()->id)
            ->with(['supplier_region' => function ($q) {
                $q->with('lang');
            }])
            ->first();
        $other = OtherSettings::first();
        if ($supplier->supplier_region) {
            $supp_loc = json_decode($supplier->supplier_region->coordinates);

            $wherehouse = json_decode($other->warehouse_loc);
            //    dd($supp_loc[0]);
            $theta = $wherehouse->longitude - $supp_loc[0]->lng;
            $distance = sin(deg2rad($wherehouse->latitude)) * sin(deg2rad($supp_loc[0]->lat)) +  cos(deg2rad($wherehouse->latitude)) * cos(deg2rad($supp_loc[0]->lat)) * cos(deg2rad($theta));
            $distance = acos($distance);
            $distance = rad2deg($distance);
            $miles = $distance * 60 * 1.1515;
            $km = $miles * 1.609344;
        }
        $pdf = PDF::loadView('branch.order.pdf_view', compact('details', 'km', 'supplier'));
        return $pdf->stream('order.pdf');
    }
}
