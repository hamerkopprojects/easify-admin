<?php

namespace App\Http\Controllers\Branch;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\User;
use Auth;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      
       
            return view('branch.dashboard.index');
    }
}
