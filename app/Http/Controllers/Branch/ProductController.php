<?php

namespace App\Http\Controllers\Branch;

use Carbon\Carbon;
use App\Models\Product;
use App\Models\Category;
use App\Models\Supplier;
use App\Models\Attribute;
use App\Models\BranchAnnex;
use App\Models\ProductPrice;
use Illuminate\Http\Request;
use App\Models\ProductVariant;
use App\Models\ProductBranchStock;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller {

    public function get(Request $req) {
        $search = $req->search;
        $category_id = $req->cat_id;
        $query = Product::with('lang');
        if ($search) {
            $query->whereHas('lang', function ($query) use ($search) {
                $query->where('name', 'like', "%" . $search . "%");
                $query->orWhere('sku', 'like', "%" . $search . "%");
            });
        }
        if ($req->cat_id) {
            $query->where('category_id', '=', $req->cat_id);
        }
        $parent_supplier = BranchAnnex::where('supplier_id', auth()->user()->id)->first();
        $pdt_data = $query->where('deleted_at', null)
                ->where('supplier_id', $parent_supplier->supplier_parent_id)
                ->where('supplier_status','publish')
                ->orderBy('id', 'desc')
                ->paginate(20);
        $supplier = Supplier::where('id', $parent_supplier->supplier_parent_id)->first();
        $cat_data = Category::with(['lang' => function ($query) {
                        $query->where('language', 'en');
                    }])
                ->where('parent_id', NULL)
                ->get();


        return view('branch.product.index', compact('pdt_data', 'cat_data', 'search', 'category_id', 'supplier'));
    }

    public function details(Request $req) {
        // dd($req);
        $product_id = $req->id;

        $product = Product::with('lang')
                ->with('category')
                ->with('brand')
                ->where('id', $product_id)
                ->get();
        return view('branch.product.view.product_details', compact('product'));
    }

    public function stockUpdates(Request $req) {
        $pdt_id = $req->id;
        $pdt_att_var = $pdt_att_row_data = array();
        $pdt_data = Product::where(['id' => $pdt_id])->first();
        $product_type = $pdt_data->product_type ? $pdt_data->product_type : 'simple';
        $pdt_att_var = array();
        if ($product_type == 'complex') {
            $pdt_att_var = Attribute::with('lang')->leftJoin('category_attributes', 'category_attributes.attribute_id', '=', 'attribute.id')
                    ->select('attribute.*')
                    ->where('category_attributes.category_id', '=', $pdt_data->category_id)
                    ->where('attribute.is_variant', 'yes')
                    ->where('attribute.attribute_type', '=', 'dropdown')
                    ->groupBy('category_attributes.attribute_id')
                    ->first();
        }
        $branch = Supplier::where('id', auth()->user()->id)->first();

        return view('branch.product.stock', compact('pdt_id', 'pdt_att_var', 'product_type', 'branch'));
    }

    public function updateStock(Request $req) {
        $pdt_data = Product::where(['id' => $req->pdt_id])->first();
        if ($req->product_type == 'simple') {
            $rules = [
                'max_stock.*' => 'required',
                'min_stock.*' => 'nullable|lt:max_stock.*',
            ];
            $messages = [
                'max_stock.required' => 'Max stock is required.',
                'lt' => 'Min stock must be less than the max stock',
            ];
        } else {
            $rules = [
                'max_stock_var.*' => 'required',
                'min_stock_var.*' => 'nullable|lt:max_stock_var.*',
            ];
            $messages = [
                'max_stock_var.required' => 'Max stock is required.',
                'lt' => 'Min stock must be less than the max stock',
            ];
        }
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            if ($req->pdt_id) {
                $flag = 0;
                $pdt_var_count = ProductVariant::with('attribute')->with('variant')
                        ->where('product_id', $req->pdt_id)
                        ->get();
                if (count($pdt_var_count) > 0) {
                    $flag = 1;
                }
                
                $pdt_var = ProductVariant::where('product_id', $req->pdt_id)->first();
                if ($pdt_data->product_type == 'simple') {
                    if (!empty($pdt_var)) {
                        $pdt_branch = ProductBranchStock::where(['branch_id' => auth()->user()->id, 'product_variant_stock_id' => $pdt_var->id])->get();
                        if (!empty($pdt_branch)) {
                            $pdt_branch->each->delete();
                        }
                    }
                    $pdt_variant = '';
                    if (empty($pdt_var)) {
                        $pdt_variant = ProductVariant::create([
                                    'product_id' => $req->pdt_id,
                                    'supplier_id' => $pdt_data->supplier_id,
                                    'created_at' => Carbon::now()
                        ]);
                    } else {
                        ProductVariant::where('product_id', $req->pdt_id)->where('id', $pdt_var->id)
                                ->update([
                                    'product_id' => $req->pdt_id,
                                    'supplier_id' => $pdt_data->supplier_id,
                        ]);
                    }
                    $product_variant_id = isset($pdt_var->id) ? $pdt_var->id : $pdt_variant->id;
                    $branch_stock = ProductBranchStock::create([
                                'product_variant_stock_id' => $product_variant_id,
                                'branch_id' => $req->branch_id,
                                'min_stock' => $req->min_stock,
                                'max_stock' => $req->max_stock,
                                'created_at' => Carbon::now()
                    ]);
                } else {
                    $variant_ids = $req->product_variant_id;
                    $pdt_branch = ProductBranchStock::whereIn('product_variant_stock_id', $variant_ids)->where('branch_id', auth()->user()->id)->get();
                    if (!empty($pdt_branch)) {
                        $pdt_branch->each->delete();
                    }
                    $pdt_variant = ProductVariant::whereIn('id', $variant_ids)
                            ->update([
                        'supplier_id' => $pdt_data->supplier_id,
                    ]);

                    $branches = $req->branch_id;
                    foreach ($branches as $key => $branch_val) {
                        ProductBranchStock::create([
                            'product_variant_stock_id' => $req->product_variant_id[$key],
                            'branch_id' => $branch_val,
                            'min_stock' => $req->min_stock_var[$key],
                            'max_stock' => $req->max_stock_var[$key],
                            'created_at' => Carbon::now()
                        ]);
                    }
                }
                if ($flag == 1) {
                    $msg = __('messages.admin.branch_stock_updated');
                } else {
                    $msg = __('messages.admin.branch_stock_added');
                }



                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }

}
