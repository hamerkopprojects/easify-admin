<?php

namespace App\Http\Controllers\Branch;


use App\Events\SendToken;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Listeners\GenerateToken;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\SegmentRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use App\Models\Supplier;

class ForgotPasswordController extends Controller
{
    public function passwordRequest()
    {
        return view('branch.password.email');
    }

    public function forgetpassword(Request $request)
    {
        // $email=$request->email;
        $email=encrypt($request->email);
        $user_email=Supplier::where('email',$request->email)->first();
        if($user_email){
            $var=mt_rand(1000,9999);
            // $var = "1234"; 
            $user=Supplier::where('email',$request->email)->update([
                'verification_code'=>$var,
            ]);
    
              event(new SendToken($request->email, $var));
            // if($request->resent != RESEND){
                Session::flash('success','Verification token send successfully.');
                return view('branch.password.reset2',compact('email'));
            }
        else 

        return Redirect::back()->withErrors([ "We can't find a user with that e-mail address"]);
    }

    public function verify(Request $request)
    {
        // dd($request);

        $email1=decrypt($request->email);
        $email=$request->email;
    
        $supplier=Supplier::where('email',$email1)->where('verification_code',$request->code)->first();
        
        if($supplier)
        {
            Session::flash('success','Token Verified Successfully.');
            return view('branch.password.reset',compact('email'));
        
        }
        else{
            Session::flash('error','Enter valid token');
            return view('branch.password.reset2',compact('email'));
            
        }
    }

    public function passwordUpdating(Request $request)
    {
        // dd($request);
        // $validatedData = $request->validate([
        //     'password' => 'required|confirmed|min:6',

        // ]);
            $email= decrypt($request->email);
            $user=Supplier::where('email',$email)->update(
                [
                    'password' => Hash::make($request->password),
                ]);
                Session::flash('success','Password reset successfully');
                return redirect()->route('branch.login');
        
       

    }
}