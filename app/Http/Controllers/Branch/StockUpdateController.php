<?php

namespace App\Http\Controllers\Branch;

use DB;
use Excel;
use Config;
use Validator;
use App\Models\Product;
use App\Models\VariantLang;
use App\Models\TempStock;
use Illuminate\Http\Request;
use App\Imports\ProductImport;
use App\Models\ProductVariant;
use App\Models\BranchAnnex;
use App\Models\ProductBranchStock;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;

class StockUpdateController extends Controller {

    public function index(Request $request) {
        if (!empty($request['type'])) {
            $type = $request['type'];
        }
        if (!empty($request->action)) {
            $type = $request->action;
        }
        TempStock::truncate();
        return view('branch.product.stock_updates.index', compact('type'));
    }

    public function importsubmit(Request $req) {
        $type = $req->type;
        $rules = [
            'select_file' => 'required|mimes:xlsx|max:2048',
        ];
        $messages = [
            'select_file.required' => 'File is required.',
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            TempStock::truncate();
            $file = $req->file('select_file');
            $data = Excel::toArray(new ProductImport, $file);
            if ($type == "simple") {
                if (count($data) > 0) {
                    foreach ($data as $key => $sub) {
                        foreach ($sub as $key1 => $val) {
                            if ($key1 > 0) {
                                $errors = "";
                                $temp_products = new TempStock;

                                $temp_products->sku = trim($val[0]);
                                if ($temp_products->sku == "") {
                                    $errors .= "SKU is required,";
                                }
                                $temp_products->min_stock = $val[1];
                                if ($temp_products->min_stock == "") {
                                    $errors .= "Min stock is required,";
                                }
                                $temp_products->max_stock = $val[2];
                                if ($temp_products->max_stock == "") {
                                    $errors .= "Max stock is required,";
                                }

                                if ($errors) {
                                    $temp_products->error_flag = "Y";
                                    $temp_products->errors = $errors;
                                } else {
                                    $temp_products->error_flag = "N";
                                }
                                if (!empty($temp_products->sku)) {
                                    $pdt_data = TempStock::where('sku', '=', $temp_products->sku)->first();

                                    if (!empty($pdt_data)) {
                                        $temp_products->duplicate_flag = 'Y';
                                    }
                                }
                                $temp_products->created_at = Carbon::now();

                                $temp_products->save();
                            }
                        }
                    }
                    return response()->json(['status' => '1', 'type' => $type]);
                } else {
                    return response()->json(['message' => $validator->errors()->first(), 'status' => '0']);
                }
            } elseif ($type == "complex") {
                TempStock::truncate();
                if (count($data) > 0) {
                    foreach ($data as $key => $sub) {
                        foreach ($sub as $key1 => $val) {
                            if ($key1 > 0) {
                                $errors = "";
                                $temp_products = new TempStock;

                                $temp_products->sku = trim($val[0]);
                                if ($temp_products->sku == "") {
                                    $errors .= "SKU is required,";
                                }
                                $temp_products->variant = $val[1];
                                if ($temp_products->variant == "") {
                                    $errors .= "Variant is required,";
                                }
                                $temp_products->min_stock = $val[2];
                                if ($temp_products->min_stock == "") {
                                    $errors .= "Min stock is required,";
                                }
                                $temp_products->max_stock = $val[3];
                                if ($temp_products->max_stock == "") {
                                    $errors .= "Max stock is required,";
                                }

                                if ($errors) {
                                    $temp_products->error_flag = "Y";
                                    $temp_products->errors = $errors;
                                } else {
                                    $temp_products->error_flag = "N";
                                }
                                if (!empty($temp_products->sku)) {
                                    $pdt_data = TempStock::where('sku', '=', $temp_products->sku)->where('variant', '=', $temp_products->variant)->first();

                                    if (!empty($pdt_data)) {
                                        $temp_products->duplicate_flag = 'Y';
                                    }
                                }
                                $temp_products->created_at = Carbon::now();

                                $temp_products->save();
                            }
                        }
                    }
                    return response()->json(['status' => '1', 'type' => $type]);
                } else {
                    return response()->json(['message' => $validator->errors()->first(), 'status' => '0']);
                }
            }
        }
    }

    public function StockSimple(Request $req) {
        $search = $req->search;
        $error_count = $duplicate_count = $success_count = 0;
        $result_data = array();
        $pagination_count = 10;
        $query = TempStock::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->where('sku', 'like', "%" . $search . "%");
            });
        }
        $temp_data = $query->where('cron_flag', 'I')->paginate($pagination_count)->appends(request()->query());
        if (count($temp_data) > 0) {
            foreach ($temp_data as $row_data) {
                $parent_supplier = BranchAnnex::where('supplier_id', auth()->user()->id)->pluck('supplier_parent_id')->first();
                $sku = Product::select('id', 'product_type')->where('sku', $row_data->sku)->where('supplier_id', '=', $parent_supplier)->first();
                if (empty($sku)) {
                    $row_data->error_flag = 'Y';
                }
                if (!empty($sku)) {
                    if ($sku->product_type != 'simple') {
                        $row_data->error_flag = 'Y';
                    }
                }
                if (!empty($row_data->min_stock) && !empty($row_data->max_stock)) {
                    if ($row_data->max_stock < $row_data->min_stock) {
                        $row_data->error_flag = 'Y';
                    }
                }
                if ($row_data->error_flag == 'Y') {
                    $error_count++;
                }
                if ($row_data->error_flag == 'N') {
                    $pdt_data = Product::where('sku', '=', $row_data->sku)->where('supplier_id', '=', $parent_supplier)->first();
                    if (!empty($pdt_data)) {
                        $row_data->duplicate_flag = 'Y';
                        $duplicate_count++;
                    }
                }
                if ($row_data->error_flag == 'N' && $row_data->duplicate_flag == 'N') {
                    $success_count++;
                }
                $result[] = $row_data;
            }
            $result_data = $result;
        }
        $total_count = $error_count + $duplicate_count + $success_count;
        $data = [
            'temp_data' => $temp_data,
            'result_data' => $result_data,
            'error_count' => $error_count,
            'duplicate_count' => $duplicate_count,
            'success_count' => $success_count,
            'total_count' => $total_count,
            'search' => $search
        ];

        return view('branch.product.stock_updates.stock_simple', $data);
    }

    public function simple_errors(Request $req) {

        $id = $req->id;
        $result = TempStock::where('id', $id)->first();
        $parent_supplier = BranchAnnex::where('supplier_id', auth()->user()->id)->pluck('supplier_parent_id')->first();
        $sku_data = Product::where('sku', '=', $result->sku)->where('supplier_id', '=', $parent_supplier)->first();

        $new_errors = '';
        if (!empty($result->sku)) {
            if (empty($sku_data)) {

                $new_errors .= "SKU not exist ,";
            }
        }
        if (!empty($sku_data)) {
            if ($sku_data->product_type != 'simple') {
                $new_errors .= "Imported product is not simple ,";
            }
        }
        if (!empty($result->min_stock) && !empty($result->max_stock)) {
            if (($result->max_stock) < ($result->min_stock)) {
                $new_errors .= "Min stock is greater than max stock,";
            }
        }
        $all_errors = $result->errors . $new_errors;
        $errors = [];
        $errors = explode(",", $all_errors);

        $data = ['errors' => $errors];

        return view('branch.product.stock_updates.errors', $data);
    }

    public function confirmSimple() {

        $result_data = array();

        $result_data = TempStock::where('cron_flag', 'I')->get();

        if (count($result_data) > 0) {
            foreach ($result_data as $row_data) {
                if ($row_data->error_flag == 'N') {
                    $parent_supplier = BranchAnnex::where('supplier_id', auth()->user()->id)->pluck('supplier_parent_id')->first();
                    $pdt_data = Product::where('sku', '=', $row_data->sku)->where('supplier_id', '=', $parent_supplier)->first();
                    if (empty($pdt_data)) {
                        $row_data->error_flag = 'Y';
                    } else {
                        if ($pdt_data->product_type != 'simple') {
                            $row_data->error_flag = 'Y';
                        }
                    }
                    if (!empty($row_data->min_stock) && !empty($row_data->max_stock)) {
                        if (($row_data->max_stock) < ($row_data->min_stock)) {
                            $row_data->error_flag = 'Y';
                        }
                    }

                    if ($row_data->error_flag == 'N') {
                        $pdt_var = ProductVariant::where('product_id', $pdt_data->id)->where('supplier_id', '=', $parent_supplier)->first();
                        $data_to_save = [
                            'min_stock' => $row_data->min_stock,
                            'max_stock' => $row_data->max_stock
                        ];
                        $saved_data = ProductBranchStock::where(['product_variant_stock_id' => $pdt_var->id, 'branch_id' => auth()->user()->id])
                                ->update($data_to_save);
                    }
                }
                TempStock::where('id', $row_data->id)->update(['cron_flag' => 'S', 'error_flag' => $row_data->error_flag, 'duplicate_flag' => $row_data->duplicate_flag]);
            }
        }
        $check_temp_emp = TempStock::where('cron_flag', 'I')->first();
        if (!$check_temp_emp) {
            TempStock::where('cron_flag', 'S')->update(['cron_flag' => 'C']);
        }

        return response()->json(['message' => 'Thank you ! Successfully updated stock list', 'status' => '1']);
    }

    public function importedSimple(Request $req) {
        $search = $req->search;
        $error_count = $success_count = $duplicate_count = 0;
        $pagination_count = 10;
        $result_data = array();
        $query = TempStock::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->where('sku', 'like', "%" . $search . "%");
            });
        }
        $result_data = $query->where('cron_flag', 'C')
                        ->paginate($pagination_count)->appends(request()->query());

        if (count($result_data) > 0) {
            foreach ($result_data as $row_data) {
                if ($row_data->error_flag == 'Y') {
                    $error_count++;
                } else {
                    $success_count++;
                }
            }
        }
        $total_count = $error_count + $success_count;
        $data = [
            'result_data' => $result_data,
            'error_count' => $error_count,
            'success_count' => $success_count,
            'total_count' => $total_count,
            'search' => $search,
        ];

        return view('branch.product.stock_updates.imported_simple', $data);
    }

    public function StockComplex(Request $req) {
        $search = $req->search;
        $error_count = $duplicate_count = $success_count = 0;
        $result_data = array();
        $pagination_count = 10;
        $query = TempStock::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->where('sku', 'like', "%" . $search . "%");
            });
        }
        $temp_data = $query->where('cron_flag', 'I')->paginate($pagination_count)->appends(request()->query());
        if (count($temp_data) > 0) {
            foreach ($temp_data as $row_data) {
                $parent_supplier = BranchAnnex::where('supplier_id', auth()->user()->id)->pluck('supplier_parent_id')->first();
                $sku = Product::select('id', 'product_type')->where('sku', $row_data->sku)->where('supplier_id', '=', $parent_supplier)->first();
                if (empty($sku)) {
                    $row_data->error_flag = 'Y';
                }
                if (!empty($sku)) {
                    if ($sku->product_type != 'complex') {
                        $row_data->error_flag = 'Y';
                    }
                    if (!empty($row_data->min_stock) && !empty($row_data->max_stock)) {
                        if ($row_data->max_stock < $row_data->min_stock) {
                            $row_data->error_flag = 'Y';
                        }
                    }

                    $varient_da = array();
                    $varient_da = ProductVariant::where('product_id', '=', $sku->id)->where('supplier_id', '=', $parent_supplier)->get();

                    if (!empty($varient_da)) {
                        $val = "";
                        foreach ($varient_da as $varient_data) {
                            $variant = VariantLang::where('id', $varient_data->variant_lang_id)->where('attribute_id', $varient_data->attribute_id)->first();
                            if (!empty($variant)) {
                                if ((strtolower($variant->name) === strtolower($row_data->variant))) {
                                    $val = "exist";
                                    break;
                                }
                            }
                        }
                        if ($val != "exist") {
                            $row_data->error_flag = 'Y';
                        }
                    } else {
                        $row_data->error_flag = 'Y';
                    }
                }
                if ($row_data->error_flag == 'Y') {
                    $error_count++;
                }
                if ($row_data->error_flag == 'N') {
                    $pdt_data = Product::where('sku', '=', $row_data->sku)->where('supplier_id', '=', $parent_supplier)->first();
                    if (!empty($pdt_data)) {
                        $row_data->duplicate_flag = 'Y';
                        $duplicate_count++;
                    }
                }
                if ($row_data->error_flag == 'N' && $row_data->duplicate_flag == 'N') {
                    $success_count++;
                }
                $result[] = $row_data;
            }
            $result_data = $result;
        }
        $total_count = $error_count + $duplicate_count + $success_count;
        $data = [
            'temp_data' => $temp_data,
            'result_data' => $result_data,
            'error_count' => $error_count,
            'duplicate_count' => $duplicate_count,
            'success_count' => $success_count,
            'total_count' => $total_count,
            'search' => $search
        ];

        return view('branch.product.stock_updates.stock_complex', $data);
    }

    public function complex_errors(Request $req) {

        $id = $req->id;
        $result = TempStock::where('id', $id)->first();
        $parent_supplier = BranchAnnex::where('supplier_id', auth()->user()->id)->pluck('supplier_parent_id')->first();
        $sku_data = Product::where('sku', '=', $result->sku)->where('supplier_id', '=', $parent_supplier)->first();

        $new_errors = '';
        if (!empty($result->sku)) {
            if (empty($sku_data)) {
                $new_errors .= "SKU not exist ,";
            }
        }
        if (!empty($result->min_stock) && !empty($result->max_stock)) {
            if (($result->max_stock) < ($result->min_stock)) {
                $new_errors .= "Min stock is greater than max stock,";
            }
        }
        if (!empty($sku_data)) {
            if ($sku_data->product_type != 'complex') {
                $new_errors .= "Imported product is not simple ,";
            }
            $varient_da = array();
            $varient_da = ProductVariant::where('product_id', '=', $sku_data->id)->where('supplier_id', '=', $parent_supplier)->get();
            if (!empty($result->variant) && !empty($varient_da)) {
                $val = "";
                foreach ($varient_da as $varient_data) {
                    $varient = VariantLang::where('id', $varient_data->variant_lang_id)->where('attribute_id', $varient_data->attribute_id)->first();
                    if (!empty($varient)) {
                        if ((strtolower($varient->name) === strtolower($result->variant))) {
                            $val = "exist";
                            break;
                        }
                    }
                }

                if ($val != "exist") {

                    $new_errors .= "SKU and Variant mismatch,";
                }
            } else {
                $new_errors .= "SKU and Variant mismatch,";
            }
        }
        $all_errors = $result->errors . $new_errors;
        $errors = [];
        $errors = explode(",", $all_errors);

        $data = ['errors' => $errors];

        return view('branch.product.stock_updates.errors', $data);
    }

    public function confirmComplex() {

        $result_data = array();

        $result_data = TempStock::where('cron_flag', 'I')->get();

        if (count($result_data) > 0) {
            foreach ($result_data as $row_data) {
                if ($row_data->error_flag == 'N') {
                    $parent_supplier = BranchAnnex::where('supplier_id', auth()->user()->id)->pluck('supplier_parent_id')->first();
                    $pdt_data = Product::where('sku', '=', $row_data->sku)->where('supplier_id', '=', $parent_supplier)->first();
                    if (empty($pdt_data)) {
                        $row_data->error_flag = 'Y';
                    } else {
                        if ($pdt_data->product_type != 'complex') {
                            $row_data->error_flag = 'Y';
                        }
                        if (!empty($row_data->min_stock) && !empty($row_data->max_stock)) {
                            if (($row_data->max_stock) < ($row_data->min_stock)) {
                                $row_data->error_flag = 'Y';
                            }
                        }
                        $varient_da = array();
                        $varient_da = ProductVariant::where('product_id', '=', $pdt_data->id)->where('supplier_id', '=', $parent_supplier)->get();
                        if (!empty($row_data->variant) && !empty($varient_da)) {
                            $val = "";
                            foreach ($varient_da as $varient_data) {
                                $varient = VariantLang::where('id', $varient_data->variant_lang_id)->where('attribute_id', $varient_data->attribute_id)->first();
                                if (!empty($varient)) {
                                    if ((strtolower($varient->name) === strtolower($row_data->variant))) {
                                        $var_id = $varient->id;
                                        $val = "exist";
                                        break;
                                    }
                                }
                            }

                            if ($val != "exist") {
                                $row_data->error_flag = 'Y';
                            }
                        } else {
                            $row_data->error_flag = 'Y';
                        }
                    }
                    if ($row_data->error_flag == 'N') {
                        $pdt_var = ProductVariant::where('product_id', $pdt_data->id)->where('supplier_id', '=', $parent_supplier)
                                        ->where('variant_lang_id', '=', $var_id)->get();
                        if (!empty($pdt_var)) {
                            foreach ($pdt_var as $pdt_var_val) {
                                $data_to_save = [
                                    'min_stock' => $row_data->min_stock,
                                    'max_stock' => $row_data->max_stock
                                ];
                                $saved_data = ProductBranchStock::where(['product_variant_stock_id' => $pdt_var_val->id, 'branch_id' => auth()->user()->id])
                                        ->update($data_to_save);
                            }
                        }
                    }
                }
                TempStock::where('id', $row_data->id)->update(['cron_flag' => 'S', 'error_flag' => $row_data->error_flag, 'duplicate_flag' => $row_data->duplicate_flag]);
            }
        }
        $check_temp_emp = TempStock::where('cron_flag', 'I')->first();
        if (!$check_temp_emp) {
            TempStock::where('cron_flag', 'S')->update(['cron_flag' => 'C']);
        }

        return response()->json(['message' => 'Thank you ! Successfully updated stock list', 'status' => '1']);
    }

    public function importedComplex(Request $req) {
        $search = $req->search;
        $error_count = $success_count = $duplicate_count = 0;
        $pagination_count = 10;
        $result_data = array();
        $query = TempStock::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->where('sku', 'like', "%" . $search . "%");
            });
        }
        $result_data = $query->where('cron_flag', 'C')
                        ->paginate($pagination_count)->appends(request()->query());

        if (count($result_data) > 0) {
            foreach ($result_data as $row_data) {
                if ($row_data->error_flag == 'Y') {
                    $error_count++;
                } else {
                    $success_count++;
                }
            }
        }
        $total_count = $error_count + $success_count;
        $data = [
            'result_data' => $result_data,
            'error_count' => $error_count,
            'success_count' => $success_count,
            'total_count' => $total_count,
            'search' => $search,
        ];

        return view('branch.product.stock_updates.imported_complex', $data);
    }

}
