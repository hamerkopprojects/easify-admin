<?php

namespace App\Http\Controllers\Branch;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\OrderItems;
use App\User;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\BestsellingProducts;

class HomeController extends Controller {

    protected $order;

    public function __construct(Order $order) {
        $this->order = $order;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $total_items_for_collection = $this->order
                        ->leftJoin('order_items as item', 'order.id', '=', 'item.order_id')
                        ->whereIn('order.order_status', [3, 4])
                        ->where('item.branch_id', auth()->user()->id)
                        ->select(DB::raw('count(item.id) AS total'))->first();

        $total_items_collected = $this->order
                        ->leftJoin('order_items as item', 'order.id', '=', 'item.order_id')
                        ->whereIn('order.order_status', [5])
                        ->where('item.branch_id', auth()->user()->id)
                        ->select(DB::raw('count(item.id) AS total'))->first();

        $total_sale = $this->order
                        ->leftJoin('order_items as item', 'order.id', '=', 'item.order_id')
                        ->whereIn('order.order_status', [9])
                        ->where('item.branch_id', auth()->user()->id)
                        ->select(DB::raw('sum(item.grant_total) AS sum'))->first();


        $todays_bookings = $this->order
                        ->leftJoin('order_items as item', 'order.id', '=', 'item.order_id')
                        ->leftJoin('customer as cust', 'order.customer_id', '=', 'cust.id')
                        ->whereIn('order.order_status', [1])
                        ->where('item.branch_id', auth()->user()->id)
                        ->select(
                                'order.id', 'order.order_id as order_id', 'order.delivery_schedule_date', 'order.time_slot', 'order.order_status', 'cust.cust_name', DB::raw('SUM(item.item_price *item.item_count ) AS item_sum')
                        )
                        ->whereDate('order.created_at', Carbon::today()->toDateString())->groupBy('order.id')->orderBy('order.id', 'desc')->paginate(10);

        $best_selling_pdts = BestsellingProducts::with('product')
        ->orderBy('product_count', 'desc')
        ->whereHas('product', function($q) {
            $q->where('supplier_id', auth()->user()->id);
        })
        ->groupBy('product_id')
        ->paginate(10);

        return view('branch.dashboard.index', compact('todays_bookings', 'best_selling_pdts', 'total_items_for_collection', 'total_items_collected', 'total_sale'));
    }

    public function getCount(Request $req) {

        $start = Carbon::parse($req->start_date)->format('Y-m-d');
        $end = Carbon::parse($req->end_date)->format('Y-m-d');
        $total_sale = $this->order
                        ->leftJoin('order_items as item', 'order.id', '=', 'item.order_id')
                        ->whereIn('order.order_status', [9])
                        ->where('item.branch_id', auth()->user()->id)
                        ->select(DB::raw('sum(item.grant_total) AS sum'))->whereBetween('order.created_at', [$start, $end])->first();
        
         $total_items_for_collection = $this->order
                        ->leftJoin('order_items as item', 'order.id', '=', 'item.order_id')
                        ->whereIn('order.order_status', [3, 4])
                        ->where('item.branch_id', auth()->user()->id)
                        ->select(DB::raw('count(item.id) AS total'))->whereBetween('order.created_at', [$start, $end])->first();

        $total_items_collected = $this->order
                        ->leftJoin('order_items as item', 'order.id', '=', 'item.order_id')
                        ->whereIn('order.order_status', [5])
                        ->where('item.branch_id', auth()->user()->id)
                        ->select(DB::raw('count(item.id) AS total'))->whereBetween('order.created_at', [$start, $end])->first();
        $sales_count = 'SAR '.number_format($total_sale->sum, 2, ".", "");
        return [
            'total_collection' => $total_items_for_collection->total,
            'total_collected' => $total_items_collected->total,
            'sales_count' => $sales_count,
        ];
    }

}
