<?php

namespace App\Http\Controllers\Branch;

use App\Models\UserRoles;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class BranchLoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:branch')->except('logout');
    }
    public function login()
    {

        return view('branch.login');
    }

    public function branchLogin(Request $request)
    {

        $roles = UserRoles::where('name', "Branch")->first();
        if (Auth::guard('branch')->attempt(['email' => $request->email, 'password' => $request->password, 'role_id' => $roles->id, 'status' => 'active'], $request->get('remember'))) {

            return redirect()->intended('/branch');
        } else {

            return redirect()->back()->withErrors(['email' => 'Email/Password Incorrect!']);
        }
    }
    public function logout()
    {
        if(Auth::guard('branch')->check()) 
        Auth::guard('branch')->logout();

        Session::flash('success', 'Logout successfully.');
        return redirect()->route('branch.login');
    }
}
