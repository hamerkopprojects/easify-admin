<?php

namespace App\Http\Controllers\Api\Driverapp;

use App\User;
use Exception;
use Carbon\Carbon;
use App\Services\OtpService;
use Illuminate\Http\Request;
use App\Models\OtherSettings;
use App\Services\OtpGenerator;
use App\Http\Controllers\Controller;
use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessResponse;
use App\Http\Responses\SuccessWithData;
use App\Http\Requests\Api\Driverapp\LoginRequest;
use App\Http\Requests\Api\Driverapp\OtpVerifyRequest;

class LoginController extends Controller
{
    protected $driver;

    const DRIVER = 3;

    public function __construct(User $driver)
    {
        $this->driver = $driver;
    }

    public function driverAppLogin(LoginRequest $req, OtpService $otpService)
    {
        if ($this->is_arabic($req->phone)) {
            $converted_phone = $this->convert($req->phone);
            $phone_trimmed = ltrim($converted_phone, '0');
            $phone = '0' . $phone_trimmed;
        } else {
            $phone_trimmed = ltrim($req->phone, '0');
            $phone = '0' . $phone_trimmed;
        }
        $driver = $this->driver
            ->whereIn('phone', array($phone_trimmed, $phone))
            ->where('role_id', static::DRIVER)
            ->where('status', 'active')
            ->first();

        $noUserFound = is_null($driver);

        if ($noUserFound) {
            throw new Exception(trans('driver.login.user_exist'));
        }

        $otp = $this->generateNewOtp($driver);
        $other_setting = OtherSettings::first();
        if($other_setting->sms == 'enable'){
            $message = trans('sms.otp.driver', ['otp' => $otp]);
            // dd($otp);
            $country_code = $driver->country_code ?? '+91' ;
            $otpService->send($message, $phone_trimmed,$country_code);
        }

        return new SuccessResponse(trans('driver.login.otp_success'));

        // return response()->json([
        //     'success' => true,
        //     'message' => 'otp send successfully',

        // ]);
    }

    /**
     * Generate new otp and save to db
     *
     * @param User $user
     * @return string
     */
    protected function generateNewOtp(User $driver)
    {
        $otpExpiry = Carbon::parse($driver->otp_generated_at)
            ->addMinutes(0);

        $now = now();

        $otp =  OtpGenerator::generateOtp($driver->phone);
        // $otp = 1234;
        // dd($driver);
        $driver->update([
            'otp' => $otp,
            'otp_generated_at' => $now,
        ]);

        return $otp;
    }


    protected function is_arabic($str)
    {
        if (mb_detect_encoding($str) !== 'UTF-8') {
            $str = mb_convert_encoding($str, mb_detect_encoding($str), 'UTF-8');
        }

        /*
        $str = str_split($str); <- this function is not mb safe, it splits by bytes, not characters. we cannot use it
        $str = preg_split('//u',$str); <- this function woulrd probably work fine but there was a bug reported in some php version so it pslits by bytes and not chars as well
        */
        preg_match_all('/.|\n/u', $str, $matches);
        $chars = $matches[0];
        $arabic_count = 0;
        $latin_count = 0;
        $total_count = 0;
        foreach ($chars as $char) {
            //$pos = ord($char); we cant use that, its not binary safe 
            $pos = $this->uniord($char);
            // echo $char ." --> ".$pos.PHP_EOL;

            if ($pos >= 1536 && $pos <= 1791) {
                $arabic_count++;
            } else if ($pos > 123 && $pos < 123) {
                $latin_count++;
            }
            $total_count++;
        }
        if (($arabic_count / $total_count) > 0.6) {
            // 60% arabic chars, its probably arabic
            return true;
        }
        return false;
    }

    protected  function convert($string)
    {
        return strtr($string, array('۰' => '0', '۱' => '1', '۲' => '2', '۳' => '3', '۴' => '4', '۵' => '5', '۶' => '6', '۷' => '7', '۸' => '8', '۹' => '9', '٠' => '0', '١' => '1', '٢' => '2', '٣' => '3', '٤' => '4', '٥' => '5', '٦' => '6', '٧' => '7', '٨' => '8', '٩' => '9'));
    }

    protected function uniord($u)
    {
        // i just copied this function fron the php.net comments, but it should work fine!
        $k = mb_convert_encoding($u, 'UCS-2LE', 'UTF-8');
        $k1 = ord(substr($k, 0, 1));
        $k2 = ord(substr($k, 1, 1));
        return $k2 * 256 + $k1;
    }
    public function otpVerify(OtpVerifyRequest $request)
    {
        if ($this->is_arabic($request->phone)) {
            $converted_phone = $this->convert($request->phone);
            $phone_trimmed = ltrim($converted_phone, '0');
            $phone = '0'.$phone_trimmed;
        } else {
            $phone_trimmed = ltrim($request->phone, '0');
            $phone = '0'.$phone_trimmed;
        }
        $user = $this->driver
            ->whereIn('phone', array($phone_trimmed,$phone))
            ->where('role_id', static::DRIVER)
            ->where('status', 'active')
            ->first();

        if (!$user) {
            return new ErrorResponse(trans('driver.login.user_exist'));
        }

        if ($this->isValidOtp($user, $request->otp)) {

            $user->update([
                // 'is_verified' => true,
                'fcm_token' => $request->fcm_token,
            ]);
            
            $token = $user->createToken('Api Token', ['user'])->accessToken;
            return new SuccessWithData([
                'user' => $user,
                'access_token' => $token,
            ]);
        }

        return new ErrorResponse(trans('driver.login.otp_wrong'));
    }
     /**
     * Check if the otp is valid
     *
     * @param User $user
     * @param string $otp
     * @return boolean
     */
    protected function isValidOtp(User $driver, $otp)
    {
        $otpExpiry = Carbon::parse($driver->otp_generated_at)
            ->addMinutes(60);

        return $driver->otp == $otp
            && $otpExpiry->gt(now());
    }
    
    /**
     * Undocumented function
     *
     * @param LoginRequest $request
     * @param OtpService $otpService
     * @return void
     */
    
   public function resend(LoginRequest $request,OtpService $otpService)
   {
    if ($this->is_arabic($request->phone)) {
        $converted_phone = $this->convert($request->phone);
        $phone_trimmed = ltrim($converted_phone, '0');
        $phone = '0'.$phone_trimmed;
    } else {
        $phone_trimmed = ltrim($request->phone, '0');
        $phone = '0'.$phone_trimmed;
    }

    $user = $this->driver
        ->whereIn('phone', array($phone_trimmed,$phone))
        ->where('role_id', static::DRIVER)
        ->where('status', 'active')
        ->first();
    
    $otp = $this->generateNewOtp($user);
    $phone_trimmed = ltrim($request->phone, '0');
    $other_setting = OtherSettings::first();
    if($other_setting->sms == 'enable'){
        $message = trans('sms.otp.driver', ['otp' => $otp]);
        $country_code = $user->country_code ?? '+91' ;
        $otpService->send($message, $phone_trimmed,$country_code);
    }
        

    return new SuccessResponse(trans('driver.login.otp_resend'));
   }

   public function logout()
   {
       $driver = auth()->user();
           // dd($driver);
       $driver->token()->revoke();
      
       // return new SuccessResponse('Logout successful');
       return response()->json([
           'success' => true,
           'message' => 'Logout successful',

       ]);
   }
  
}
