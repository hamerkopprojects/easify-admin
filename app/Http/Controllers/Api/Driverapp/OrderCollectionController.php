<?php

namespace App\Http\Controllers\Api\Driverapp;

use Carbon\Carbon;
use App\Models\Order;
use App\Models\OrderItems;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use App\Http\Responses\SuccessResponse;
use App\Http\Responses\SuccessWithData;
use App\Http\Requests\Api\Driverapp\UpdateStatusRequest;
use App\Http\Requests\Api\Driverapp\UpdateStatusWarehouseRequest;
use App\Models\OtherSettings;

class OrderCollectionController extends Controller
{
    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }
    public function orderList(Request $req)
    {
        $date = $req->date;
        $ord_status = Config::get('constants.order_status');
        $query = $this->order
            ->whereIn('order.order_status', [3, 4, 5])
            ->leftJoin('order_items as item', 'order.id', '=', 'item.order_id')
            ->where('item.driver_id',auth()->user()->id)
            ->select('order.id','order.order_status', 'item.collection_date', 'item.collection_time_slot','order.order_id');
        if ($date) {
            $query->whereDate('item.collection_date', Carbon::today()->toDateString());
        }
        $order = $query->groupBy('order.id')->paginate(5);
        $collection_status = Config::get('constants.driver_collection_status');
       
        $order->map(function ($orders) use ($ord_status ,$collection_status) {
            $orders->collection_date =  Carbon::parse($orders->collection_date)->formatLocalized('%d %b %Y');
            // Carbon::createFromFormat('Y-m-d H:i:s', $orders->collection_date)->format('d-F-Y');
            $orders->order_status = $ord_status[$orders->order_status];
            $order_items_accept = OrderItems::where('order_id',$orders->id)
            ->where('driver_id',auth()->user()->id)
            ->whereIn('status',[5,6])
            ->count();
            $order_items = OrderItems::where('order_id',$orders->id)
            ->where('driver_id',auth()->user()->id)
            ->count();
        if($order_items_accept == $order_items )
            {
                $stat = $collection_status[2];
            
                $collection_stat = trans("notification.collection_stat.{$stat}");
            }else if($order_items_accept > 0){
                $stat = $collection_status[3];
                $collection_stat = trans("notification.collection_stat.{$stat}");
            }else{
                $stat = $collection_status[1];
                $collection_stat = trans("notification.collection_stat.{$stat}");
            }
            $orders->collection_status = $collection_stat;
        });
        return new SuccessWithData($order);
    }
    public function collectionDetails($id)
    {
        $ord_status = Config::get('constants.order_status');
        $stat_colle= Config::get('constants.driver_collection_status');
        $product_status = Config::get('constants.product_status');
        
        $order = $this->order->where('id',$id)
            ->with(['items'=>function($query){
                $query->where('driver_id',auth()->user()->id)
                ->with('product:id,sku,product_type,status,cover_image')
                ->with(['branch'=>function($q){
                    $q->select('id','code','name','phone','region_id')
                    ->with(['supplier_region'=>function($q){
                        $q->with('lang:id,region_id,name,language')
                        ->select('id','coordinates');
                    }]);
                }]);
            }])
            // ->select('id,order_id','customer_id',)
            ->first();
            $order_items_accept = OrderItems::where('order_id',$id)
            ->where('driver_id',auth()->user()->id)
            ->whereIn('status',[5,6])
            ->count();
            $order_items = OrderItems::where('order_id',$id)
            ->where('driver_id',auth()->user()->id)
            ->count();
            if($order_items_accept == $order_items )
            {
                $stat = $stat_colle[2];
            
                $collection_stat = trans("notification.collection_stat.{$stat}");
                // $collection_stat = $collection_status[2];
            }else if($order_items_accept > 0){
                
                $stat = $stat_colle[3];
            
                $collection_stat = trans("notification.collection_stat.{$stat}");
                // $collection_stat = $collection_status[3];
            }else{
                
                $stat = $stat_colle[1];
            
                $collection_stat = trans("notification.collection_stat.{$stat}");
                // $collection_stat = $collection_status[1];
            }
            $order->order_status = $ord_status[$order->order_status];
            $order->collection_status = $collection_stat;
            $order->delivery_schedule_date=  $order->delivery_schedule_date ? Carbon::createFromFormat('Y-m-d H:i:s',$order->delivery_schedule_date)->format('d-F-Y') : null ;
            $order->items->each(function ($product) use($product_status) {
             $product->product->cover_image = $product->product->cover_image ? asset('uploads/'.$product->product->cover_image) : asset('assets/frontend/custom/images/no-image.png') ;
             $product->product_status = $product->status ;
             $product->status = $product_status[$product->status];
             $product->collection_date = Carbon::createFromFormat('Y-m-d H:i:s',$product->collection_date)->format('d-F-Y');
            });
            
          return new SuccessWithData($order);  
    }

    public function markasCollected(UpdateStatusRequest $request)
    {
        // $stat = $request->status == 'collected' ? 5
        $lang = auth()->user()->language ?? 'en';
        $orderItems= OrderItems::where('id',$request->id)
                ->update(['status'=> 5]);
        $item_supplier = OrderItems::where('id',$request->id)->first();
        $count_orderItems = DB::table('order_items')->where('order_id', $request->order_id)->count();
        $completed_count = DB::table('order_items')->where('order_id', $request->order_id)->whereIn('status', [5,6,3])->count();
        if($count_orderItems == $completed_count )
        {
            
            Order::where('id',$request->order_id)
                    ->update([
                        'order_status'=> 5
                    ]);
            OrderItems::where('order_id',$request->order_id)
            ->where('branch_id',$item_supplier->branch_id)
            ->update([
                'supplier_status'=>4
            ]);
        }else{
            Order::where('id',$request->order_id)
                    ->update([
                        'order_status'=> 4
                    ]);
            OrderItems::where('order_id',$request->order_id)
                    ->where('branch_id',$item_supplier->branch_id)
                    ->update([
                        'supplier_status'=>3
                    ]);
        }
        return new SuccessResponse(trans('notification.collection_stat.stat_success',[],$lang));
    }
    public function inWareHouse(UpdateStatusWarehouseRequest $req)
    {
        $lang = auth()->user()->language ?? 'en';
        $shelf_id = $req->shelf_id;
        $item_id  = $req->id;
        $order=$req->order_id ;
        $orderItems= OrderItems::where('id',$item_id)
                    ->where('order_id',$order)
                ->update([
                    'status'=>6,
                    'shelf_id'=>$shelf_id
                    ]);
        return new SuccessResponse(trans('notification.collection_stat.stat_success',[],$lang));
    }
    public function shelfList()
    {
        $shelf=Config::get('constants.warehouse_shelfs');
        return new SuccessWithData($shelf);
    }
    public function warehouse()
    {
        $warehouse = OtherSettings::select('warehouse_loc')->get();
        return new SuccessWithData($warehouse);
    }
}
