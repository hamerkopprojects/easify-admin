<?php

namespace App\Http\Controllers\Api\Driverapp;

use stdClass;
use App\Models\Faqs;
use App\Models\Pages;
use Illuminate\Http\Request;
use App\Traits\FormatLanguage;
use App\Http\Controllers\Controller;
use App\Http\Responses\SuccessWithData;
use App\Models\HowtoUse;

class AppDataController extends Controller
{
    use FormatLanguage ;
    protected $page , $faq;
    const PRIVACY_POLICY = 'privacy-policy';
    const TERMS_AND_CONDITION = 'terms-and-conditions';
    const DRIVERAPP = 2;
    public function __construct(Pages $page , Faqs $faq )
    {
        $this->page = $page;
        $this->faq = $faq ;
    }
    public function privacyPolicies()
    {
        $privacy = $this->getData(static:: PRIVACY_POLICY,'Driverapp');

        return new SuccessWithData($privacy);
    }
    public function termsandConditions()
    {
        $terms = $this->getData(static::TERMS_AND_CONDITION ,'Driverapp');

        return new SuccessWithData($terms);
    }

    public function getData($type, $appType)
    {
        $data = $this->page
            ->with('lang:page_id,title,content,language')
            ->where('slug', $type)
            ->where('available_for',$appType)
            ->select('id')
            ->first();

        if ($data) {
            $data->lang = $data->lang->map(function ($lang) {
                // $lang->content = html_entity_decode($lang->content);
                $lang->content = strip_tags($lang->content);
                return $lang;
            });
            
            $this->keyBy($data, 'lang', 'language');
        }

        return $data ?? new stdClass;
    }

    public function faq()
    {
        $faq = $this->faq
        ->with('lang:id,faq_id,question,answer,language')
        ->whereHas('appType', function ($query) {
            $query->where('app_id','=', static::DRIVERAPP);
        })
        ->with('appType:id')
        ->get(['id']);
        $this->keyByNested($faq, 'lang', 'language');

        return new SuccessWithData($faq);
    }

    public function getUseAppData()
    {
        $useapp = HowtoUse::
        with('lang:id,how_to_id,title,content,language,image')       

        ->get();
        $useapp->each(function($useapp){
            $useapp->lang[0]->image =  $useapp->lang[0]->image ? asset('/uploads/'.$useapp->lang[0]->image) : null ;
            $useapp->lang[1]->image =  $useapp->lang[1]->image ? asset('/uploads/'.$useapp->lang[1]->image) : null ;
            $useapp->lang[0]->content = $useapp->lang[0]->content ? strip_tags(htmlspecialchars_decode($useapp->lang[0]->content)):null;
            $useapp->lang[1]->content = $useapp->lang[1]->content ? strip_tags(htmlspecialchars_decode($useapp->lang[1]->content)):null;
            $this->keyBy($useapp,'lang', 'language');
        });
        

        return new SuccessWithData($useapp);
    }

}
