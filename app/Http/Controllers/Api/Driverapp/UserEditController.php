<?php

namespace App\Http\Controllers\Api\Driverapp;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\SuccessResponse;
use App\Http\Responses\SuccessWithData;
use App\Http\Requests\Api\Driverapp\UserUpdateRequest;

class UserEditController extends Controller
{
    public function editUser()
    {
        // $user = auth()->user()->only('name','phone','email','id','photo');
        $user = User::where('id',auth()->user()->id)
                ->select('name','phone','email','id','photo')
                ->firstOrFail();
       
        $user->photo = $user->photo ? asset('uploads/'.$user->photo) : null ;


        return new SuccessWithData ($user);

    }
    public function updateDriver(UserUpdateRequest $request)
    {
        $user = User::where('id',auth()->user()->id)
                ->firstOrFail();
        // $user->photo = $user->photo ? asset('uploads/'.$user->photo) : null ;
        $path = $user->photo ;
        if($request->photo)
        {
            $path = $request->file('photo')->store("userprofile", ['disk' => 'public_uploads']);
        }
        $user = User::where('id',auth()->user()->id)
           ->update([
            'name'=>$request->name,
            'email'=>$request->email,
            'photo'=>$path 
        ]);
        
        // $image      = $request->file('photo');
        // $_path = $image->getRealPath();
        
        // return new SuccessWithData( $_path );
        return new SuccessResponse('Profile updated succesfully');
    }
}
