<?php

namespace App\Http\Controllers\Api\Driverapp;

use Illuminate\Http\Request;
use App\Models\SupportRequests;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Responses\SuccessResponse;
use App\Http\Responses\SuccessWithData;
use App\Http\Requests\Api\Driverapp\SupportRequest;
use App\Http\Requests\Api\Driverapp\SupportChatRequest;

class SupportController extends Controller
{
    protected $support;

    public function __construct(SupportRequests $support)
    {
        $this->support = $support;
    }

    public function index(SupportRequest $request)
    {
        $support = DB::transaction(function() use ($request) {
            $support = $this->support->create([
                'subject' => $request->subject,
                'message' => $request->message,
                'app_type' => 'driver',
                'submitted_by' => auth()->id(),
            ]);

            $support->chats()->create([
                'message' => $request->message,
                'user_type' => 'driver',
            ]);

            return $support;
        });

       
        return new SuccessResponse(trans('notification.success_msg.support_success'));
    }
    public function supportChat($id)
    {
        $request = $this->support
            ->where('id', $id)
            ->where('submitted_by', auth()->id())
            ->firstOrFail();
            
        $chatHistory = $request->chats()
            ->whereIn('user_type', ['driver', 'admin'])
            ->select('id', 'message', 'user_type')
            ->orderBy('created_at', 'asc')
            ->get();

        return new SuccessWithData($chatHistory);
    }
    public function sendChat(SupportChatRequest $req)
    {
        $supportRequest = $this->support
            ->where('app_type', 'driver')
            ->where('submitted_by', auth()->id())
            ->where('id', $req->request_id)
            ->firstOrFail();

        DB::transaction(function () use ($req,$supportRequest ) {
            $supportRequest->chats()->create([
                'message' => $req->message,
                'user_type' => 'driver',
            ]);

            $supportRequest->touch();
        });

        return new SuccessResponse('New message sent successfully');

    }
}
