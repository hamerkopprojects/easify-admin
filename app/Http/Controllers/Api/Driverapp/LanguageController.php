<?php

namespace App\Http\Controllers\Api\Driverapp;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Driverapp\LanguageRequest;
use App\Http\Responses\SuccessResponse;

class LanguageController extends Controller
{
    
        public function update(LanguageRequest $request)
        {
            /** @var \App\user */
            $user = auth()->user();
            $user->update(['language' => $request->lang]);
    
            return new SuccessResponse('Language updated successfully');
        }
    
}
