<?php

namespace App\Http\Controllers\Api\Driverapp;

use App\Models\Order;
use App\Models\OrderItems;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Driverapp\QRCodeRequest;
use App\Http\Responses\ErrorResponse;
use Illuminate\Support\Facades\Config;
use App\Http\Responses\SuccessResponse;
use App\Http\Responses\SuccessWithData;

class QRCodeScanningController extends Controller
{
    public function generate(Request $req)
    {
        $qr = $req->code ;
        $shelf = $req->shelf_id ;
        $qr_explode_code = explode('-',$qr);
        $ord_status = Config::get('constants.order_status');
        $pro_status = Config::get('constants.product_status');
       if($qr_explode_code[0] != 0 && $qr_explode_code [1] != 0  && $qr_explode_code[2] != 0)
       {
            $item = OrderItems::where('order_id',$qr_explode_code[1])
            ->where('id',$qr_explode_code[0])
            ->where('branch_id',$qr_explode_code[2])
            ->first();
            if($item->status == 6){
                return new ErrorResponse(trans('notification.qr.pdt_collected'));
            }else{
                $item_status = $this->itemStatusUpdate($item);

                $item->update([
                    'status' => $item_status,
                    'shelf_id'=>$shelf

                    ]);
                $count_orderItems = DB::table('order_items')->where('order_id', $qr_explode_code[1])->count();
                $completed_count = DB::table('order_items')->where('order_id', $qr_explode_code[1])->whereIn('status', [5, 6])->count();
                $supp_order_count = DB::table('order_items')
                                    ->where('order_id', $qr_explode_code[1])
                                    ->where('branch_id', $qr_explode_code[2])
                                    ->count();
                $supp_order_accept_count = DB::table('order_items')
                                    ->where('order_id', $qr_explode_code[1])
                                    ->where('branch_id', $qr_explode_code[2])
                                    ->whereIn('status', [5, 6])
                                    ->count();
                if($supp_order_count ==$supp_order_accept_count )
                {
                    OrderItems::where('order_id',$qr_explode_code[1])
                                ->where('branch_id',$qr_explode_code[2])
                                ->update([
                                    'supplier_status'=>4
                                ]);
                  
                }else{
                    OrderItems::where('order_id',$qr_explode_code[1])
                                ->where('branch_id',$qr_explode_code[2])
                                ->update([
                                    'supplier_status'=>3
                                ]);
                }
                if ($count_orderItems == $completed_count) {

                    Order::where('id', $qr_explode_code[1])
                        ->update([
                            'order_status' => 5
                        ]);
                    $item->update([
                        'supplier_status' => 4
                    ]);
                } else {
                    Order::where('id', $qr_explode_code[1])
                        ->update([
                            'order_status' => 4
                        ]);
                }
                
                return new SuccessWithData([
                    'order_id' => $qr_explode_code[1],
                    'type' => 'collection'
                ]);
                
            }
           
       }else if($qr_explode_code[0] == 0 && $qr_explode_code[1] != 0 && $qr_explode_code[2] == 0 )
       {
            $order_current = Order::where('id',$qr_explode_code[1])
            ->first();
            if($order_current->order_status == 9)
            {
                return new ErrorResponse(trans('notification.qr.ord_delivered'));

            }else{
                $status = $this->orderStatusUpdate($order_current);
                $order_current->update([
                    'order_status'=>$status
                ]);
                return new SuccessWithData([
                    'order_id'=>$qr_explode_code[1],
                    'type'=>'delivery'
                ]);
            }
            
           
       }else if($qr_explode_code[0] == 0 && $qr_explode_code[1] != 0 && $qr_explode_code[2] != 0 )
       {
           if($qr_explode_code[3] == 'supplier')
           {
            $item_pro= OrderItems::where('order_id',$qr_explode_code[1])
            ->where('branch_id',$qr_explode_code[2])
            ->where('supplier_id',$qr_explode_code[2])
            ->get();
           }else{
            $item_pro = OrderItems::where('order_id',$qr_explode_code[1])
            ->where('branch_id',$qr_explode_code[2])
            ->get();
           }
           
            if($item_pro[0]->supplier_status == 4 ){
                return new ErrorResponse(trans('notification.qr.pdt_collected'));
            }else{
                foreach($item_pro as $it)
                {
                   
                    OrderItems::where('id',$it->id)
                    ->where('status',4)
                    ->update(['status'=>5]);
                }
                
            
             $count_orderItems = DB::table('order_items')->where('order_id',$qr_explode_code[1])->count();
             $completed_count = DB::table('order_items')->where('order_id',$qr_explode_code[1])->whereIn('status', [5,6,3])->count();
                if($count_orderItems == $completed_count )
                {
                    
                    Order::where('id',$qr_explode_code[1])
                            ->update([
                                'order_status'=> 5
                            ]);
                    
                }else{
                    Order::where('id',$qr_explode_code[1])
                            ->update([
                                'order_status'=> 4
                            ]);
                }
                $supp_order_count = DB::table('order_items')
                    ->where('order_id', $qr_explode_code[1])
                    ->where('branch_id', $qr_explode_code[2])
                    ->count();
                $supp_order_accept_count = DB::table('order_items')
                    ->where('order_id', $qr_explode_code[1])
                    ->where('branch_id', $qr_explode_code[2])
                    ->whereIn('status', [5, 6])
                    ->count();
                if($supp_order_count ==$supp_order_accept_count )
                    {
                        OrderItems::where('order_id',$qr_explode_code[1])
                                    ->where('branch_id',$qr_explode_code[2])
                                    ->update([
                                        'supplier_status'=>4
                                    ]);
                  
                    }else{
                        OrderItems::where('order_id',$qr_explode_code[1])
                                ->where('branch_id',$qr_explode_code[2])
                                ->update([
                                    'supplier_status'=>3
                                ]);
                    }
                
                return new SuccessWithData([
                    'order_id'=>$qr_explode_code[1],
                    'type'=>'collection'
                ]);
            }
           
       }
    }

    protected function orderStatusUpdate($order)
    {
        $order_status =$order->order_status ;
        $status = '';
        switch($order_status)
        {
            case '3':
                $status = 5;
                break;
            case '4':
                $status = 5;
                break;
            case '7':
                $status = 9;
                break;
            case '9':
                 $status = 9;
             break;
        }
        return $status ;
    }
    protected function itemStatusUpdate($item)
    {
        $status_item = '';
        switch($item->status)
        {
            case '4':
                $status_item = '5';
                break;
            case '5':
                $status_item = '6';
                break;
            case '6':
                    $status_item = '6';
                    break;
        }
        return $status_item ;
    }
    public function QrCodeStatus(QRCodeRequest $req)
    {
        $qr_code = $req->code ;

        $qr_explode_code = explode('-',$qr_code);
        if (count($qr_explode_code) !== 4) {
            return new ErrorResponse(trans('notification.qr.qr_invd'));
        }else{
       
            if($qr_explode_code[0] != 0 && $qr_explode_code [1] != 0  && $qr_explode_code[2] != 0){
                $pro_status = [
                    5 =>"Collect",
                    6 => "place_at_ware_house"

                ];
                $item = OrderItems::where('order_id',$qr_explode_code[1])
                    ->where('id',$qr_explode_code[0])
                    ->where('branch_id',$qr_explode_code[2])
                    ->first();
                if($item->status == 6){
                        return new ErrorResponse(trans('notification.qr.pdt_aldy_ware'));
                }else{
                    $item_stat = $this->itemStatusUpdate($item);
                    $my_st =$pro_status[$item_stat ];
                    $confirm = trans("notification.qr.{$my_st}");
                    $item_stat == 6 ? $atHouse=true :$atHouse = false;
                    
                    return response()->json([
                        'success' => true,
                        'msg' =>$confirm ,
                        "warehouse"=>$atHouse
                    ], 200);
                }
                
               
            }else if($qr_explode_code[0] == 0 && $qr_explode_code [1] != 0  && $qr_explode_code[2] != 0)
            {
                $item = OrderItems::where('order_id',$qr_explode_code[1])
                ->where('branch_id',$qr_explode_code[2])
                ->get();
           
                 if($item[0]->supplier_status == 4 ){
                    return new ErrorResponse(trans('notification.qr.pdt_collected'));
                }else{
                    $confirm = trans('notification.qr.supp_collected');
                    return new SuccessResponse($confirm);
                }
               
            }elseif($qr_explode_code[0] == 0 && $qr_explode_code [1] != 0  && $qr_explode_code[2] == 0)
            {
                $order_current = Order::where('id',$qr_explode_code[1])
                ->first();
                if($order_current->order_status == 9)
                 {
                        return new ErrorResponse(trans('notification.qr.ord_delivered'));
                    }else{
                    $confirm = trans('notification.qr.ord_del_confirm');
                     return new SuccessResponse($confirm);
                }
            }
        }
        
    }
}