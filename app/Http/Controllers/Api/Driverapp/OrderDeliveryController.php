<?php

namespace App\Http\Controllers\Api\Driverapp;

use Carbon\Carbon;
use App\Models\Order;
use App\Models\Customer;
use App\Models\OrderItems;
use App\Services\SendEmail;
use Illuminate\Http\Request;
use App\Traits\FormatLanguage;
use App\Models\CancellationReason;
use App\Models\BestsellingProducts;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use App\Http\Responses\SuccessResponse;
use App\Http\Responses\SuccessWithData;
use App\Http\Requests\Api\Driverapp\CancellationRequest;
use App\Traits\OrderActivityLog;

class OrderDeliveryController extends Controller
{
    protected $order;

    use FormatLanguage,OrderActivityLog;

    const DRIVERAPP = 2;
    const SINGLE = 1 ;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }
    public function activeOrders(Request $req)
    {
        $date= $req->date;
        $ord_status = Config::get('constants.order_status');
        $activeOrder = $this->order
            ->whereIn('order_status',[7,8])
            ->where('driver_id',auth()->user()->id)
            ->with('customer:id,cust_name,phone')
            ->when($req->has('date'), function ($query) use ($date) {
                
                $query->whereDate('delivery_schedule_date',Carbon::parse($date)->format('Y-m-d'));
            })
            ->select($this->orderSelect())
            ->paginate(100);
        $activeOrder->map(function ($orders) use ($ord_status) {
            $orders->delivery_schedule_date =  Carbon::parse($orders->delivery_schedule_date)->formatLocalized('%d %b %Y');
            // Carbon::createFromFormat('Y-m-d H:i:s', $orders->delivery_schedule_date)->format('d-F-Y');
            $orders->order_status = $ord_status[$orders->order_status];
        });
        return new SuccessWithData($activeOrder);
    }
    public function deliveredOrder()
    {
        $ord_status = Config::get('constants.order_status');
        $deliveredOrder = $this->order
            ->with('customer:id,cust_name,phone')
            ->where('order_status', 9)
            ->where('driver_id',auth()->user()->id)
            ->select($this->orderSelect())
            ->paginate(100);
           
        $deliveredOrder->map(function ($orders) use ($ord_status) {
            $orders->delivery_schedule_date =  Carbon::parse($orders->delivery_schedule_date)->formatLocalized('%d %b %Y');
            // Carbon::createFromFormat('Y-m-d H:i:s', $orders->delivery_schedule_date)->format('d-F-Y');
            $stat = $ord_status[$orders->order_status];
            $orders->order_status = trans("notification.order_status.{$stat}");
            
        });
        return new SuccessWithData($deliveredOrder);
    }
    
    public function orderDetails($id)
    {
        $ord_status = Config::get('constants.order_status');
        $orderDetails=$this->order
                    ->with(['product'=> function($q){
                        $q->with('lang:id,name,language,product_id')
                        ->with('category:id,status,category_unique_id')
                        ->withPivot(['id','order_id','product_id','item_count','grant_total', 'sub_total', 'product_details',
                        'variant_name', 'item_price','driver_id']);
                    }])
                    ->with('customer:cust_name,id,phone')
                    ->with(['cancellation'=>function($query){
                        $query->with('lang:name,language,cancellation_id')
                        ->select('id');
                    }])
                    ->where('id',$id)
                    ->firstOrFail();
        $orderDetails->order_status = $ord_status[$orderDetails->order_status];
        $orderDetails->delivery_schedule_date =Carbon::parse($orderDetails->delivery_schedule_date)->formatLocalized('%d %b %Y');
        $orderDetails->payment_method =$orderDetails->payment_method ? strtoupper($orderDetails->payment_method ): ''; 
        $orderDetails->product->each(function ($product) {
            $this->keyBy($product, 'lang', 'language');
            // $this->keyBy($product->category, 'lang', 'language');
         $product->cover_image = $product->cover_image ? asset('uploads/'.$product->cover_image) : asset('assets/frontend/custom/images/no-image.png') ;
        
        });
        if($orderDetails->cancellation)
        {
            $this->keyBy($orderDetails->cancellation, 'lang', 'language');
        }
            
       
        return new SuccessWithData($orderDetails);
    }

    protected function orderSelect()
    {
        return[
            'id',
            'customer_id',
            'order_id',
            'driver_id',
            'delivery_loc_coordinates',
            'delivery_loc',
            'delivery_schedule_date',
            'time_slot',
            'order_status'

        ];
    }
    public function cancelOrder(CancellationRequest $req)
    {
        $cancel = $this->order->where('id',$req->order_id)
                    ->update([
                        'order_status'=>8,
                        'cancel_reason_id'=>$req->reason,
                        'cancel_note'=>$req->message
                    ]);
        $order_details = Order::find($req->order_id);
        $ord_status = (Config::get('constants.order_status'));
        $log ='Order('.$order_details->order_id.') status changed to  <b>'.$ord_status[8].'</b> by ' . auth()->user()->name . '(Driver)' .' on ' . Carbon::now()->format('d/m/Y g:i A');
        $this->orderActivity($order_details->id,'driver',$log,'',$order_details->customer_id,auth()->user()->id);
        return new SuccessResponse('Order cancellation request send successfully');
    }
    public function markAsDelivered($id)
    {
        $cancel = $this->order->where('id',$id)
                    ->update([
                        'order_status'=>9,
                    ]);
        $order_products = OrderItems::where('order_id', $id)->get();
        foreach ($order_products as $pro) {
            $most_sold_product = BestsellingProducts::select('product_count')->where('product_id', $pro->id)->first();
            if (isset($most_sold_product) && $most_sold_product->product_count > 0) {
                $view_count = $most_sold_product->product_count + 1;
                BestsellingProducts::where('product_id', $pro->product_id)
                    ->update([
                        "product_count" => $view_count
                    ]);
            } else {
                BestsellingProducts::create([
                    'product_id' => $pro->product_id,
                    'product_count' => static::SINGLE,
                    'created_at' => Carbon::now(),
                    'updated_at' => now()
                ]);
            }
        }
        $this->sendCompletedMail($id);
        $order_details = Order::find($id);
        $ord_status = (Config::get('constants.order_status'));
        $log ='Order('.$order_details->order_id.') status changed to  <b>'.$ord_status[9].'</b> by ' . auth()->user()->name . '(Driver)' .' on ' . Carbon::now()->format('d/m/Y g:i A');
        $this->orderActivity($order_details->id,'driver',$log,'',$order_details->customer_id,auth()->user()->id);
        return new SuccessResponse('Order delivered successfully');
    }

    public function cancellationReasons()
    {
       $cancel = CancellationReason::with('lang:id,cancellation_id,name,language')
        ->whereHas('appType', function ($query) {
            $query->where('app_id','=', static::DRIVERAPP);
        })
        ->with('appType:id')
        ->get(['id']);
        $this->keyByNested($cancel, 'lang', 'language');

        return new SuccessWithData($cancel);

    }
    protected function sendCompletedMail($id)
    {
        $ord_status = (Config::get('constants.order_status'));
        $order = Order::find($id);
        $status = $ord_status[$order->order_status];
        $customer =Customer::find($order->customer_id);
        $subject = 'Your order is '. $status;
        SendEmail::generateOrderEmail($customer->email, $customer->cust_name, $subject, $order, 4);
    }
}
