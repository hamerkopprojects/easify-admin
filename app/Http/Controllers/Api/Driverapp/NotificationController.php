<?php

namespace App\Http\Controllers\Api\Driverapp;

use App\User;
use Illuminate\Http\Request;
use App\Models\AppNotifications;
use App\Http\Controllers\Controller;
use App\Http\Responses\SuccessResponse;
use App\Http\Responses\SuccessWithData;

class NotificationController extends Controller
{
    protected $notification;

    public function __construct(AppNotifications $notification)
    {
        $this->notification = $notification;
    }

    public function get()
    {
        $notifications = $this->notification
            ->where('to_type', 'driver')
            ->where('to_id', auth()->id())
            ->orderBy('created_at', 'desc')
            ->select('id', 'content', 'is_read', 'details', 'created_at')
            ->paginate(15);
        
        return new SuccessWithData($notifications);
    }

    public function markAsRead($notificationId)
    {
        $notification = $this->notification
            ->where('id', $notificationId)
            ->where('to_id', auth()->id())
            ->where('to_type', 'driver')
            ->firstOrFail();

        $notification->update(['is_read' => 1]);

        return new SuccessResponse('Notification changed to read status');
    }
    public function toggle(Request $request)
    {
        $request->validate([
            'enable' => 'required|boolean'
        ]);
        
        /** @var \App\Models\Customer */
        $user = auth()->user();
        $status = $request->enable ? 1 : 0;

        $user->update(['need_notification' => $status]);
        
        
        $message = $status?trans('notification.enabled'):trans('notification.disabled');

        return new SuccessResponse($message);
    }

    public function notificationCheck()
    {
        $need_notification = auth()->user()->only('id','name','need_notification');
        return new SuccessWithData( $need_notification);
    }
    public function unreadCount()
    {
        $noti_unread = $this->notification
            ->where('to_id', auth()->id())
            ->where('to_type', 'driver')
            ->where('is_read', '0')
            ->count();
        $badge = User::where('id',auth()->user()->id)->first();
        return new SuccessWithData([
            "count"=>$noti_unread,
            "badge"=>$badge->driverapp_badge_seen
            ]);
    }
    public function badgeUpdate()
    {
        $badge = User::where('id',auth()->user()->id)->update([
            'driverapp_badge_seen' =>'false'
        ]);
        return new SuccessResponse('badge updated');
    }

}
