<?php

namespace App\Http\Controllers;

use PDF;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class QRCodeGenerationController extends Controller
{
    public function productCodeGeneration(Request $req)
    {
        $product = $req->product;
        $order =  $req->order;
        $supplier =  $req->supplier;
        // dd($product,$order,$supplier);
    //    $product= Product::where('id',$id)->first();
    //     // $path=route('products.detail',$product->slug);
        $target_dir_path = "public/upload/qrcodes/";
       $target_orgin_dir_path = "/upload/qrcodes/";
        if (!File::isDirectory($target_dir_path)) {
            File::makeDirectory($target_dir_path, 0777, true, true);
        }
        $invEncode = $product."-".$order."-". $supplier."-pro";
         $createfilepath = $target_dir_path . '/qr_'.$invEncode;
        $filepath = $target_orgin_dir_path . '/qr_'.$invEncode;
       
        // str_pad($id, 5, '0', STR_PAD_LEFT);
        QrCode::backgroundColor(255, 255, 0)
        ->format('png')
        ->size(300)
        ->generate($invEncode, $createfilepath);
        // $qr_url = url($filepath);
            $pdf= PDF::loadView('admin.qr-code',compact('createfilepath'));
            return $pdf->stream('qrcode.pdf');
            
    }
    public function OrderCodeGeneration($order)
    {
        $target_dir_path = "public/upload/qrcodes/order";
       $target_orgin_dir_path = "/upload/qrcodes/order";
        if (!File::isDirectory($target_dir_path)) {
            File::makeDirectory($target_dir_path, 0777, true, true);
        }
        $invEncode = "0-".$order."-0-order";
         $createfilepath = $target_dir_path . '/qr_'.$invEncode;
        $filepath = $target_orgin_dir_path . '/qr_'.$invEncode;
       
        QrCode::backgroundColor(255, 255, 0)
        ->format('png')
        ->size(300)
        ->generate($invEncode, $createfilepath);
            $pdf= PDF::loadView('admin.qr-code',compact('createfilepath'));
            return $pdf->stream('order-qrcode.pdf');
    }

    public function supplierCodeGeneration(Request $req)
    {
        $order =  $req->order;
        $supplier =  $req->supplier;
        $type =$req->type ;
       
        $target_dir_path = "public/upload/qrcodes/";
       $target_orgin_dir_path = "/upload/qrcodes/";
        if (!File::isDirectory($target_dir_path)) {
            File::makeDirectory($target_dir_path, 0777, true, true);
        }
        $invEncode ="0-".$order."-". $supplier."-supplier";
         $createfilepath = $target_dir_path . '/qr_'.$invEncode;
        $filepath = $target_orgin_dir_path . '/qr_'.$invEncode;
        QrCode::backgroundColor(255, 255, 0)
        ->format('png')
        ->size(300)
        ->generate($invEncode, $createfilepath);
            $pdf= PDF::loadView('admin.qr-code',compact('createfilepath'));
            return $pdf->stream('qrcode.pdf');
    }
    public function branchCodeGeneration(Request $req)
    {
        $order =  $req->order;
        $supplier =  $req->supplier;
       
        $target_dir_path = "public/upload/qrcodes/";
       $target_orgin_dir_path = "/upload/qrcodes/";
        if (!File::isDirectory($target_dir_path)) {
            File::makeDirectory($target_dir_path, 0777, true, true);
        }
        $invEncode ="0-".$order."-". $supplier."-branch";
         $createfilepath = $target_dir_path . '/qr_'.$invEncode;
        $filepath = $target_orgin_dir_path . '/qr_'.$invEncode;
        QrCode::backgroundColor(255, 255, 0)
        ->format('png')
        ->size(300)
        ->generate($invEncode, $createfilepath);
            $pdf= PDF::loadView('admin.qr-code',compact('createfilepath'));
            return $pdf->stream('qrcode.pdf');
    }
    
}
