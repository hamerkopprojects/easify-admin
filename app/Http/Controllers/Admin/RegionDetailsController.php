<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Models\Region;
use App\Models\RegionLang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\CityLang;
use App\Models\City;
use App\Models\Supplier;
use Illuminate\Support\Facades\Validator;

class RegionDetailsController extends Controller
{
    protected const DRIVER = 3;
    protected const ZERO = 0;
    public function get(Request $req )
    {
        $search=$req->search_region ?? '';
        $search_value=$req->search_id;
        $drivers=User::where('role_id',static::DRIVER)->get();
        $city=City::with('lang')->get();
        if($req->search_region)
        {

            $data=RegionLang::select('region_id')->where('name', 'like', "%" . $search . "%")->get()->toArray();
            $da=[];
            foreach($data as  $d)
            {
                $da[]=$d['region_id'];
            }
            $region= Region::with('lang')->whereIn('id',$da)->paginate(10);
        //     $region = Region::with(['lang'=> function($q) use ($search){
        //         $q->where('name',$search);
        //        // dd($q);
        //    }])
        //    ->whereHas('lang', function($q) use($search)
        //        {
        //            $q->where('name',$search);
        //        })
        //     ->where('id',$search_value)
        //    ->paginate(20);

        }else{
            $region=Region::with('lang')->paginate(20);

        }
        return view('admin.settings.region.index',compact('region','search','drivers','city'));

    }

    public function store(Request $request)
    {

        if($request->id){
            $region= DB::transaction(function () use ($request) {
                $region = Region::where('id',$request->id)->update([
                    'delivary_charge'=>$request->charge,
                    'driver'=>$request->driver,
                    'coordinates'=>json_encode($request->coordinates),
                    'city_id'=>$request->city
                ]);
                RegionLang::where('language','en')
                ->where('region_id',$request->id)
                ->update([
                    'name' => $request->region_en,
                ]);
                RegionLang::where('language','ar')
                ->where('region_id',$request->id)
                ->update([
                    'name' => $request->region_ar,
                ]);
            });
            return [
                'msg'=>'success',
                'input'=>'Region updated successfully'
            ];
        }else{
            $rules=[
                'coordinates'=>'required'
            ];
            $messages =[
                'coordinates.required'=>'Location required'
            ];
            $validator = Validator::make($request->all(), $rules,$messages);
             if (!$validator->passes()) {

                return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
            }else{
                $region= DB::transaction(function () use ($request) {

                    $region = Region::create([
                        'delivary_charge'=>$request->charge,
                        'driver'=>$request->driver,
                        'coordinates'=>json_encode($request->coordinates),
                        'city_id'=>$request->city
                    ]);

                    $region->lang()->createMany([
                        [
                            'name' => $request->region_en,
                            'language' => 'en',
                        ],
                        [
                            'name' => $request->region_ar,
                            'language' => 'ar',
                        ],
                    ]);

                    return $region;
                });
            }


            if($region)
            {
                return[
                   'msg'=>'success',
                   'input'=>'Region added successfully'
                ];
            }else{
                return[
                    'msg'=>'error'
                ];
            }
        }

    }
    public function destroy(Request $request)
    {
        $region=Region::find($request->id);
        $list=$region->customer()->count();
        // dd($list);
        $supplier = Supplier::where(['region_id' => $request->id])->exists();
        if($list === static::ZERO && $supplier == false){
            $region->lang()->delete();
            $region->delete();
            return response()->json(['status' => 1, 'message' => 'Region deleted successfully']);
         }else{
                return response()->json(['status' => 0, 'message' => 'This city cannot be deleted because it is assigned to some of the existing customer / supplier.']);
            }

    }
    public function edit($id)
    {
        $region=Region::with('lang')->where('id',$id)->first();
        return[
            'region'=>$region
        ];
    }
    public function autoComplete(Request $req)
    {
        $lang=RegionLang::where('name', 'like', "%" . $req->search . "%")->get();
        $response = array();
        foreach($lang as $lan){
         $response[] = array("value"=>$lan->region_id,"label"=>$lan->name);
        }

      return response()->json($response);
    }


    public function getRegion(Request $req)
    {
        $region=Region::with('lang')->where('city_id', $req->city_id)->get();
        return response()->json($region);
    }
}
