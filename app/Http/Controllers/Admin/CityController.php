<?php

namespace App\Http\Controllers\Admin;

use App\Models\City;
use App\Models\CityLang;
use App\Models\Region;
use App\Models\Supplier;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CityController extends Controller
{
    public function get(Request $req)
    {
        $search = $req->search ?? '';
        $query = City::query();
        if (!empty($search)) {
            $query->whereHas('lang', function ($query) use ($search) {
                $query->where('name', 'like', "%" . $search . "%");
            });
        }
        $query->with('lang');

        $city = $query->paginate(20);

        return view('admin.city.index', compact('city', 'search'));

    }  
    
    public function store(Request $req)
    {

        $rules = [
            'city_en' => 'required',
            'city_ar' => 'required',
        ];
        $messages = [
            'city_en.required' => 'City name (EN) is required.',
            'city_ar.required' => 'City name (AR) is required.',
        ];
        $validator = Validator::make($req->all(), $rules,$messages);
        if (!$validator->passes()) {
            
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        }else{
           
               
            $city= DB::transaction(function () use ($req) {
            
                $city = City::create([
                    'status'=>'active',
                ]);
    
                $city->lang()->createMany([
                    [
                        'name' => $req->city_en,
                        'language' => 'en',
                    ],
                    [
                        'name' => $req->city_ar,
                        'language' => 'ar',
                    ],
                ]);
    
                return $city;
            });
                $msg="City added successfully";
                if ($city) {
                    return response()->json(['status' => 1, 'message' => $msg]);
                } else {
                    return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
                }
            }
            
    }

    public function edit($id)
    {
        $city=City::with('lang')->where('id',$id)->first();
        return[
            'city'=>$city
        ];
    }

    public function update(Request $req)
    {
       
        $rules = [
            'city_en' => 'required',
            'city_ar' => 'required',
        ];
        $messages = [
            'city_en.required' => 'City name (EN) is required.',
            'city_ar.required' => 'City name (AR) is required.',
           
        ];
        $validator = Validator::make($req->all(), $rules,$messages);
        if (!$validator->passes()) {
            
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        }else{

            $city = DB::transaction(function () use ($req) {
                $city = City::where('id',$req->city_unique)->update([
                    'status'=>'active',
                ]);
                CityLang::where('language','en')
                ->where('city_id',$req->city_unique)
                ->update([
                    'name' => $req->city_en,
                ]);
                CityLang::where('language','ar')
                ->where('city_id',$req->city_unique)
                ->update([
                    'name' => $req->city_ar,
                ]);
                return $city;
            });
            $msg="City updated successfully";
            return response()->json(['status' => 1, 'message' => $msg]);

            }
    }

    public function statusUpdate(Request $req)
    {
    
        $status=$req->status === 'Activate' ? 'active' : 'deactive';
     
        $statusUpdate=City::where('id',$req->id)
        ->update([
            'status'=>$status
        ]);
        return response()->json(['status' => 1, 'message' => "Status updated successfully"]);
    }

    public function destroy(Request $req)
    {
       
        $city=City::find($req->id);
        
       
        if(!empty($city))
        {
            $region = Region::select('id')->where('city_id', $req->id)->exists();
            $supplier = Supplier::where(['city_id' => $req->id])->exists();
            $customer = Customer::where(['city' => $req->id])->exists();
            if ($region == false && $supplier == false && $customer == false) {
            $city->delete();
            return response()->json(['status' => 1, 'message' => 'City deleted successfully']);
            }else{
                return response()->json(['status' => 0, 'message' => 'This city cannot be deleted because it is assigned to some of the existing region / supplier / customer.']);
            }
          

        }else{
            return response()->json(['status' => 0, 'message' => 'Cannot delete city have related records']);
           
        }

        
    }
}
