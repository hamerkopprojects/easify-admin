<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Carbon\Carbon;
use App\Models\Customer;
use App\Models\Supplier;
use App\Models\SupportChat;
use App\Services\SendEmail;
use Illuminate\Http\Request;
use App\Models\SupportRequests;
use App\Traits\CanSaveNotification;
use App\Traits\CanSendNotification;
use App\Http\Controllers\Controller;
use App\Http\Responses\SuccessResponse;
use App\Http\Responses\SuccessWithData;

class SupportRequestController extends Controller
{
    use CanSaveNotification,CanSendNotification ;
    public function list(Request $req)
    {
        $app_type = $req->search_app_type;
        $name = $req->search_name ?? '' ;
       $quary=SupportRequests::
                with('owner');
        if($app_type)
        {
            $quary->where('app_type',"like","%".$app_type."%");
        }
        if($name)
        {
            if($app_type == 'driver')
            {
                $quary->where('submitted_by',$name)
                ->where('app_type','driver');
            }else{
                $quary->where('submitted_by',$name)
                ->where('app_type','!=','driver');
            }
           
        }
        
       $support=$quary->orderBy('created_at','desc')->paginate(20);
        $users=Customer::where('deleted_at',null)->select('cust_name','id')->get();
        return view('admin.supportchat.index',compact('support','app_type','users','name'));
    }
    public function getChat($id)
    {
        $chat = SupportChat::where('request_id', $id)
            ->orderBy('created_at')
            ->get();
        $details=SupportRequests::
            with('owner')
            ->where('id',$id)
            ->first();
        $now = now();

        $chat->map(function ($msg) use ($now) {
            $msg->message = nl2br($msg->message);
            $msg->time = Carbon::parse($msg->created_at)->diffForHumans($now);
            return $msg;
        });

        return new SuccessWithData([
            'chat'=>$chat,
            'details'=>$details
        ]);
    }
    public function send(Request $request, $id)
    {
        
        SupportChat::create([
            'request_id' => $id,
            'message' => $request->message,
            'user_type' => 'admin',
        ]);
        $support = SupportRequests::find($id);
        if($support->app_type == 'driver')
        {
            $this->notifyDriver($id);
        }else if($support->app_type == 'website')
        {
            $this->sendNotificationMail($id);
        }
        
        return new SuccessResponse('Message send successfully');
    }
    public function usersList($type)
    {
        if($type=='driver')
        {
            $users=User::where('role_id',2)->select('name','id')->get();
        }else if($type == 'customer'){
            $users=Customer::where('deleted_at',null)->select('cust_name','id')->get();
        }else{
            $users=Supplier::where('deleted_at',null)->select('name','id')->get();
        }
        return $users;
    }
    public function notifyDriver($requestId)
    {
        $request = SupportRequests::find($requestId);
        
        $owner = $request->owner;
        $language = $owner->language;

        $name = $request->app_type == 'driver'
            ? $owner->name
            : $owner->cust_name?? '';
        
        $title = trans(
            'notification.support.title.replied', 
            ['user' => $name],
            $language
        );

        $content = [
            'en' => trans(
                'notification.support.content.replied', 
                ['subject' => $request->subject],
                'en'
            ),
            'ar' => trans(
                'notification.support.content.replied', 
                ['subject' => $request->subject],
                'ar'
            ),
        ];
        
       $data= $this->saveNotification(
            $content,
            'admin',
            $request->app_type,
            [
                'type' => 'support',
                'request_id' => $requestId,
            ],
            $owner->id
        );
        // $badge = $this->countNotification($owner->id,'user');
        if($owner->need_notification == 1 &&  $owner->fcm_token)
        {
            $this->sendNotification(
                $owner->fcm_token,
                $title,
                [
                    'title' => $title,
                    'body' => $content[$language],
                    'type' => 'support',
                    'request_id' => $requestId,
                    'time' => now()->format('Y-m-d H:i:s'),
                    'notification_id'=>$data->id ?? ''
                ],
                0
            );
            $badge = User::where('id',$owner->id)->update([
                'driverapp_badge_seen'=>'true'
                ]);
        }
      
    }
    protected function sendNotificationMail($requestId)
    {
        $request = SupportRequests::find($requestId);
        $user = Customer::where('id',$request->submitted_by)->first();
        $subject = 'Replay from Easify support team';
        $query = $request->subject;
        SendEmail::generateSupportEmail($user->email,$user->cust_name, $subject,$query);

    }
}
