<?php

namespace App\Http\Controllers\Admin;

use Config;
use App\User;
use App\Models\UserRoles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Mail\SetPasswordEmail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

use App\Services\SendEmail;
use URL;

class UserManagementController extends Controller
{
    protected const ZERO = 0;
    public function get(Request $req)
    {
        
        $role=UserRoles::whereNotIn('id', [5,6])->get();
        $search=$req->search ?? '';
        $role_id=$req->select_role ?? '';
        if($search || $role_id)
        {
            $query=User::query()->with('roles');
            if($search)
            {
                $query->where(function ($sub) use ($search) {
                    $sub->where('name', 'like', "%" . $search . "%");
                    $sub ->orWhere('email', 'like', "%" . $search . "%");
                    $sub->orWhere('phone', 'like', "%" . $search . "%");
                    $sub->orWhere('user_id', 'like', "%" . $search . "%");

                });
            }
            
            if($role_id)
            {
            
                $query->where('role_id',$role_id);
             
            }
            $user=$query->where('id','!=',Auth::user()->id)->paginate(20);
            //  dd($user);
            return view('admin.user.index',compact('user','role','search','role_id'));
        }
    
    else{
        $user=User::with('roles')->where('id','!=',Auth::user()->id)->paginate(20);
        // dd($user);
        return view('admin.user.index',compact('user','role','search','role_id'));
    }
        
        
        // $user_roles=UserRoles::where()
        // dd($user);
        // return view('admin.user.index',compact('user','role','search','role_id'));
    }
    public function store(Request $req)
    {

        $rules = [
            'name' => 'required',
            'email' => 'required|unique:users,email',
            'phone' => 'required|unique:users,phone',
            'role' => 'required',

        ];
        $messages = [
            'name.required' => 'Name is required.',
            'email.required' => 'Email is required.',
            'email.unique' => 'User with same email already exists',
            'phone.required' => 'Phone number is required.',
            'phone.unique' => 'User with same phone number already exists',
            'phone.regex' => 'Phone number should be valid',
            'role.required' => 'Role is required.'
        ];
        $validator = Validator::make($req->all(), $rules,$messages);
        if (!$validator->passes()) {
            
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        }else{
           
               $user=$this->generateUserId();
               $userData= User::
                    create([
                    'name'=>$req->name,
                    'email'=>$req->email,
                    'phone'=>(int) $req->phone,
                    'role_id'=>$req->role,
                    'user_id'=>$user,
                    'national_id'=>$req->nationalId,
                    'status'=>'active'
                ]);
                $msg="User added successfully";
                if ($userData) {
                    return response()->json(['status' => 1, 'message' => $msg]);
                } else {
                    return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
                }
            }
            
    }
    public function edit($id)
    {
        $users=User::where('id',$id)->first();
        return[
            'user'=>$users
        ];
    }
    public function generateUserId()
    {
        $lastUser= User::select('user_id')
                ->orderBy('id', 'desc')
                ->first();
        // dd($lastUser);
        $lastId = 0;
        
        if ($lastUser) {
            $lastId = (int) substr($lastUser->user_id, 8);
        }
        // dd($lastId);
        $lastId++;

        return 'EAS-USER-' . str_pad($lastId, 5, '0', STR_PAD_LEFT);
    }

    public function update(Request $req)
    {
        
        $rules = [
            'name' => 'required',
            'email' => 'required|unique:users,email,'.$req->user_unique,
            'phone' => 'required|unique:users,phone,'.$req->user_unique,
            'role' => 'required',

        ];
        $messages = [
            'name.required' => 'Name is required.',
            'email.required' => 'Email is required.',
            'email.unique' => 'User with same email already exists',
            'mobile.required' => 'Phone number is required.',
            'mobile.unique' => 'Phone number should be unique',
            'role.required' => 'Role is required.'
        ];
        $validator = Validator::make($req->all(), $rules,$messages);
        if (!$validator->passes()) {
            
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        }else{
            $userData= User::where('id',$req->user_unique)
            ->update([
                'name'=>$req->name,
                'email'=>$req->email,
                'phone'=>(int)$req->phone,
                'role_id'=>$req->role,
                'national_id'=>$req->nationalId,
                
            ]);
            $msg="User details updated successfully";
            return response()->json(['status' => 1, 'message' => $msg]);

            }
    }

    public function statusUpdate(Request $req)
    {
     
        $status=$req->status === 'Deactive' ? 'active':'deactive';
        $statusUpdate=User::where('id',$req->id)
        ->update([
            'status'=>$status
        ]);
        return response()->json(['status' => 1, 'message' => "Status updated successfully"]);
    }
     
    public function destroy(Request $req)
    {
        // dd($req);
        $user=User::find($req->id);
        $list=$user->driver()->count();
        // dd($user->status);
        if($list === static::ZERO)
        {
            $user->delete();
            return response()->json(['status' => 1, 'message' => 'User deleted successfully']);
          

        }else{
            return response()->json(['status' => 0, 'message' => 'Cannot delete user have related records']);
           
        }

        
    }

    public function passwordSend(Request $req)
    {
     
        $user=User::where('id',$req->id)->first();
        $password=mt_rand(100000,999999); 
        $data=[
            'password'=>$password,
            'user'=>$user
        ];
        DB::transaction(function () use ($data,$password,$user,$req) {
            User::where('id',$req->id)->update([
                'password'=>Hash::make($password)
            ]);
        $subject = 'Welcome to EASIFY!';
        $url = URL::to('/login');
        SendEmail::generateEmail($user->email, $password, $user->name, $subject, 'user', $url);
        });
       
         return response()->json(['status' => 1, 'message' => 'One time password send successfully']);
    }
}
