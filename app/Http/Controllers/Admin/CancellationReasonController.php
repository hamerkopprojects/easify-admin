<?php

namespace App\Http\Controllers\Admin;

use App\Models\AppType;
use Illuminate\Http\Request;
use App\Models\CancellationApps;
use App\Models\CancellationReason;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\CancellationReasonLang;
use App\DataTables\CancellationReasonDataTable;

class CancellationReasonController extends Controller
{
    // public function get(CancellationReasonDataTable $dataTable)
    // {
    //     $reason=CancellationReasonLang::where('language','en')->get();
    //     return $dataTable->render('admin.settings.cancel.index',compact('reason'));  
    // }
    public function get(Request $request)
    {
        
        $search_field=$request->cancellation_search ?? '';
        $search=$request->search_field ?? '';
        $app_type=AppType::get();
        $reason=CancellationReasonLang::where('language','en')->get();
       if($search) {
        $data=CancellationReasonLang::select('cancellation_id')->where('name', 'like', "%" . $search . "%")->get()->toArray();
        $da=[];
        foreach($data as  $d)
        {
            $da[]=$d['cancellation_id'];
        }
        $cancel= CancellationReason::with('lang')->with('appType')->whereIn('id',$da)->paginate(20);
    //     $cancel= CancellationReason::with(['lang'=> function($q) use ($search){
    //         $q->where('cancellation_id',$search);
    //        // dd($q);
    //    }])
    //    ->whereHas('lang', function($q) use($search) 
    //        {
    //            $q->where('cancellation_id',$search);
    //        })
    //     ->where('id',$search)
    //    ->paginate(20);
       }else{
        $cancel=CancellationReason::with('lang')->with('appType')->paginate(20);
       }
      
       
        return view('admin.settings.cancel.index',compact('reason','cancel','search','app_type'));
    }
    public function store(Request $request)
    {
     
        $cancel = DB::transaction(function () use ($request) {
            $cancel =new CancellationReason();  
            $cancel->save();
            $cancel->lang()->createMany([
                [
                    'name' => $request->cancel_en,
                    'language' => 'en',
                ],
                [
                    'name' => $request->cancel_ar,
                    'language' => 'ar',
                ],
            ]);
            if($request->app_type ){
                $cancel->appType()->attach($request->app_type,
                ['created_at' => now(), 'updated_at' => now()]);
            }
            return $cancel;
        });
        if($cancel)
        {
            return [
                "status"=>'200',
                "msg"=>'success'
            ];
        }
        
    }

    public function edit($id)
    {
        $cancelReason=CancellationReason::with('lang')
                    ->where('id',$id)
                    ->with('appType')
                    ->first();
        return [
            "cancel"=>$cancelReason
        ];
    }
    public function update(Request $request)
    {
        // dd($request);
        $cancel = DB::transaction(function () use ($request) {
           CancellationReasonLang::where('cancellation_id',$request->id)
                ->where('language','en')  
                ->update( [
                    'name' => $request->cancel_en,
                   
                ]);
            CancellationReasonLang::where('cancellation_id',$request->id)
                ->where('language','ar')  
                ->update( [
                        'name' => $request->cancel_ar,
                       
                    ]);

                    $cancel= CancellationApps::where('cancellation_id',$request->id);
               $cancel->delete();
               foreach($request->app_type  as $apps)
                {
                    CancellationApps::create([
                       'cancellation_id'=>$request->id,
                       'app_id'=>$apps
                   ]);
                }
            
         });
         return [
             "status"=>'200',
             "msg"=>''
         ];
    }
    public function destroy(Request $req)
    {
        $model = CancellationReason::find( $req->id );
        $model->lang()->delete();
        $model->appType()->detach();
        $model->delete();
        return response()->json(['status' => 1, 'message' => 'Cancellation reason deleted successfully']);
    }
    public function autoComplete(Request $req)
    {
        $lang=CancellationReasonLang::where('name', 'like', "%" . $req->search . "%")->get();
        $response = array();
      foreach($lang as $lan){
         $response[] = array("value"=>$lan->cancellation_id,"label"=>$lan->name);
      }

      return response()->json($response);

    }
} 
