<?php

namespace App\Http\Controllers\Admin;

use App\Models\Brand;
use App\Models\Product;
use App\Models\BrandLang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Elasticsearch\ClientBuilder;

class BrandController extends Controller
{
    public function index(Request $req)
    {

        $brand = Brand::with('lang')->get();
        $search = $req->search ?? '';
        $query = Brand::query();
        if (!empty($search)) {
            $query->whereHas('lang', function ($query) use ($search) {
                $query->where('name', 'like', "%" . $search . "%");
                $query->orwhere('brand_unique_id', 'like', "%" . $search . "%");
            });
        }
        $query->with('lang');

        $brand = $query->paginate(20);

        return view('admin.product.brand.index', compact('brand', 'search'));
    }


    public function store(Request $req)
    {

        $rules = [
            'brand_name_en' => 'required|unique:brand_i18n,name,NULL,id,deleted_at,NULL',
            'brand_name_ar' => 'required',

        ];
        $messages = [
            'brand_name_en.unique' => 'Brand already exist.',
            'brand_name_en.required' => 'brand_name_en is required.',
            'brand_name_ar.required' => 'brand_name_ar is required.',
            

        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
           

            $userData = DB::transaction(function () use ($req) {
                $userData = Brand::create([
                    'brand_unique_id' => $this->generateBrandId(),
                    'status' => "deactive",
                    'slug' => $this->createSlug($req->brand_name_en)
                ]);
                $userData->lang()->createMany([
                    [
                        'name' => $req->brand_name_en,
                        'language' => 'en',
                    ],
                    [
                        'name' => $req->brand_name_ar,
                        'language' => 'ar',
                    ],
                ]);
                return $userData;
            });
            $msg = "Brand added successfully";
            if ($userData) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }


    public function statusUpdate(Request $req)
    {
        $client = ClientBuilder::create()->build();
        $att = Brand::where('id', $req->id)->first();
        if ($att) {
            if ($att->status == 'deactive') {
                Brand::where('id', $req->id)
                    ->update([
                        'status' => 'active'
                    ]);
            } else {
                Brand::where('id', $req->id)
                    ->update([
                        'status' => 'deactive'
                    ]);
//************Delete this records from Elastic Search engine***********************/
                $langs = array('en','ar');
                foreach ($langs as $val) {
                    $params ['body'][] = array(  
                        'delete' => array(  
                            '_index' => 'suggestion_easify',  
                            '_type' => 'list',  
                            '_id' => $req->id.'-'.$val.'-C-0-P-0' 
                        )  
                    );  
                }
                $response = $client -> bulk($params);
//************Delete this records from Elastic Search engine***********************/
            }
            return response()->json(['status' => 1, 'message' => 'Status updated successfully']);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }

    public function destroy(Request $req)
    {
        $client = ClientBuilder::create()->build();

        $brand = Brand::find($req->id);
        if (!empty($brand)) {
                $brand->lang()->delete();
                $brand->delete();
//************Delete this records from Elastic Search engine***********************/
                $langs = array('en','ar');
                foreach ($langs as $val) {
                    $params ['body'][] = array(  
                        'delete' => array(  
                            '_index' => 'suggestion_easify',  
                            '_type' => 'list',  
                            '_id' => $req->id.'-'.$val.'-C-0-P-0' 
                        )  
                    );  
                }
                $response = $client -> bulk($params);
//************Delete this records from Elastic Search engine***********************/
                return response()->json(['status' => 1, 'message' => 'Brand deleted successfully']);
            
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong']);
        }
    }

    public function edit($id)
    {
        $brand = Brand::with('lang')->where('id', $id)->first();
        return [
            'brand' => $brand
        ];
    }

    public function update(Request $req)
    {
        if ($req->user_unique) {
            $unique = ',' . $req->user_unique;
        } else {
            $unique = ',NULL';
        }

        $rules = [
            'brand_name_en' => 'required|unique:brand_i18n,name' . $unique . ',brand_id,language,en',
            'brand_name_ar' => 'required',
        ];
        $messages = [
            'brand_name_en.unique' => 'Brand already exist.',
            'brand_name_en.required' => 'category_name_en is required.',
            'brand_name_ar.required' => 'category_name_ar is required.',
           
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
           
            $userData = DB::transaction(function () use ($req) {
                $userData = Brand::where('id', $req->user_unique)
                ->update([
                    'slug' => $this->createSlug($req->brand_name_en)
                ]);
                $userData = BrandLang::where('brand_id', $req->user_unique)->where('language', 'en')
                    ->update([
                        'name' => $req->brand_name_en,
                    ]);
                $userData = BrandLang::where('brand_id', $req->user_unique)->where('language', 'ar')
                    ->update([
                        'name' => $req->brand_name_ar,
                    ]);
                return $userData;
            });
            $msg = "Brand updated successfully";
            if ($userData) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }

    private function createSlug($string)
    {
        $slug = strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'), array('', '-', ''), trim($string)));
        return $slug;
    }



    public function brand_images(Request $req)
    {

        $brand_id = $req->brand_id;
       
        if ($req->upload_type == 'image_en') {
            $rules = [
                'photo' => 'required|image|mimes:png,jpg,jpeg|max:10240'
            ];
            $messages = [
                'photo.max' => 'The image must be less than 2Mb in size',
                'photo.mimes' => "The image must be of the format jpeg or png",
            ];
        } elseif ($req->upload_type == 'image_ar') {
            $rules = [
                'photo' => 'required|image|mimes:png,jpg,jpeg|max:10240'
            ];
            $messages = [
                'photo.max' => 'The image must be less than 2Mb in size',
                'photo.mimes' => "The image must be of the format jpeg or png",
            ];
        }  elseif ($req->upload_type == 'banner_en') {
            $rules = [
                'photo' => 'required|image|mimes:png,jpg,jpeg|max:10240'
            ];
            $messages = [
                'photo.max' => 'The image must be less than 2Mb in size',
                'photo.mimes' => "The image must be of the format jpeg or png",
            ];
        } else {
            $rules = [
                'photo' => 'required|image|mimes:png,jpg,jpeg|max:10240'
            ];
            $messages = [
                'photo.max' => 'The image must be less than 2Mb in size',
                'photo.mimes' => "The image must be of the format jpeg or png",
            ];
        }
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {

            if ($req->upload_type == 'image_en') {

                $file = request()->file('photo');
                $path = $file->store("brand/cover/en", ['disk' => 'public_uploads']);
                
                BrandLang::where('brand_id', $brand_id)->where('language', "en")
                    ->update([
                        "image_path" => $path
                    ]);
                $full_path = url('uploads/' . $path);

                return response()->json(['status' => 1, 'message' => 'Image added successfully', 'path' => $full_path, 'brand_id' => $brand_id]);
            } else if ($req->upload_type == 'image_ar') {

                $file = request()->file('photo');
                $path = $file->store("brand/cover/ar", ['disk' => 'public_uploads']);
               
                BrandLang::where('brand_id', $brand_id)->where('language', "ar")
                    ->update([
                        "image_path" => $path
                    ]);
                $full_path = url('uploads/' . $path);
              
                return response()->json(['status' => 1, 'message' => 'Image added successfully', 'path' => $full_path, 'brand_id' => $brand_id]);
            } else  if ($req->upload_type == 'banner_en') {

                $file = request()->file('photo');
                $path = $file->store("brand/banner/en", ['disk' => 'public_uploads']);
                
                BrandLang::where('brand_id', $brand_id)->where('language', "en")
                    ->update([
                        "banner_image" => $path
                    ]);
                $full_path = url('uploads/' . $path);

                return response()->json(['status' => 1, 'message' => 'Image added successfully', 'path' => $full_path, 'brand_id' => $brand_id]);
            }else  {

                $file = request()->file('photo');
                $path = $file->store("brand/banner/ar", ['disk' => 'public_uploads']);
             
                BrandLang::where('brand_id', $brand_id)->where('language', "ar")
                    ->update([
                        "banner_image" => $path
                    ]);
                $full_path = url('uploads/' . $path);

                return response()->json(['status' => 1, 'message' => 'Image added successfully', 'path' => $full_path, 'brand_id' => $brand_id]);
            }
        }
    }
    public function detete_img(Request $request)
    {

        if ($request->type == 'image_en') {
            $brand = BrandLang::where('brand_id', $request->id)->where('language', "en")->first();
            if (!empty($brand)) {
                $file_path = public_path() . '/uploads/' . $brand["image_path"];
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
            BrandLang::where('brand_id', $request->id)->where('language', "en")
                ->update([
                    "image_path" => Null
                ]);
            $msg = 'Image deleted successfully';
        } else if ($request->type == 'image_ar') {
            $brand = BrandLang::where('brand_id', $request->id)->where('language', "ar")->first();
            if (!empty($brand)) {
                $file_path = public_path() . '/uploads/' . $brand["image_path"];
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
            BrandLang::where('brand_id', $request->id)->where('language', "ar")
                ->update([
                    "image_path" => Null
                ]);
            $msg = 'Image deleted successfully';
        }  else if ($request->type == 'banner_en') {
            $brand = BrandLang::where('brand_id', $request->id)->where('language', "en")->first();
            if (!empty($brand)) {
                $file_path = public_path() . '/uploads/' . $brand["banner_image"];
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
            BrandLang::where('brand_id', $request->id)->where('language', "en")
                ->update([
                    "banner_image" => Null
                ]);
            $msg = 'Image deleted successfully';
        }
        else {
            $brand = BrandLang::where('brand_id', $request->id)->where('language', "ar")->first();
            if (!empty($brand)) {
                $file_path = public_path() . '/uploads/' . $brand["banner_image"];
                if (is_file($file_path)) {
                    unlink($file_path);
                }
            }
            BrandLang::where('brand_id', $request->id)->where('language', "ar")
                ->update([
                    "banner_image" => Null
                ]);
            $msg = 'Image deleted successfully';
        }
        return response()->json(['status' => 1, 'message' => $msg, 'id' => $request->id, 'type' => $request->type]);
    }
    
    public function generateBrandId() {
        $lastcategory = Brand::select('brand_unique_id')
                ->orderBy('id', 'desc')
                ->first();

        $lastId = 0;

        if ($lastcategory) {
            $lastId = (int) substr($lastcategory->brand_unique_id, 4);
        }

        $lastId++;

        return 'BRA-' . str_pad($lastId, 5, '0', STR_PAD_LEFT);
    }

}
