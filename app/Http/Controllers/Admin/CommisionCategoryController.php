<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\CommisionCategory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CommisionCategoryController extends Controller
{
    public function get(Request $req)
    {
        $search = $req->search ?? '';
        $query = CommisionCategory::query();
        if (!empty($search)) {
                $query->where('name', 'like', "%" . $search . "%");
        }
        $commision = $query->paginate(20);

        return view('admin.settings.commision.index', compact('commision', 'search'));

    }  

    public function store(Request $req)
    {

        $rules = [
            'commision' => 'required|unique:commision_category,name',
            'value' => 'required',
        ];
        $messages = [
            'commision.required' => 'Commision is required.',
            'commision.unique' => 'Commision category name already exists',
            'value.required' => 'Value is required.',
        ];
        $validator = Validator::make($req->all(), $rules,$messages);
        if (!$validator->passes()) {
            
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        }else{
            
                $commision = CommisionCategory::create([

                    'name'=>$req->commision,
                    'value'=>$req->value,
                ]);

                $msg="Commision category added successfully";
                if ($commision) {
                    return response()->json(['status' => 1, 'message' => $msg]);
                } else {
                    return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
                }
            }
            
    }

    public function edit($id)
    {
        $commision=CommisionCategory::where('id',$id)->first();
      
        return[
            'commision'=>$commision
        ];
    }

    public function update(Request $req)
    {
       
        $rules = [
            'commision' => 'required',
            'value' => 'required',
        ];
        $messages = [
            'commision.required' => 'Commision is required.',
            'value.required' => 'Value is required.',
        ];
        $validator = Validator::make($req->all(), $rules,$messages);
        if (!$validator->passes()) {
            
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);

        }else{
                $commision = CommisionCategory::where('id',$req->commision_unique)->update([
                    'name'=>$req->commision,
                    'value'=>$req->value,

                ]);
                
            $msg="Commision Category updated successfully";
            return response()->json(['status' => 1, 'message' => $msg]);

            }
    }
    public function destroy(Request $req)
    {
       
        $commision=CommisionCategory::find($req->id);
        
        if(!empty($commision))
        {
            $commision->delete();
            return response()->json(['status' => 1, 'message' => 'Commision category deleted successfully']);

        }else{
            return response()->json(['status' => 0, 'message' => 'Cannot delete Commision category have related records']);
           
        }

        
    }

}
