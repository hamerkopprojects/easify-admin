<?php

namespace App\Http\Controllers\Admin;

use App\Models\HowtoUse;
use App\Models\HowtoUseLang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class HowtoUseAppController extends Controller
{
    public function get()
    {
        $pages=HowtoUse::with('lang')->paginate(20);
        return view('admin.cms.useapp.index',compact('pages'));
    }
    public function edit($id)
    {
        $imgEn='';
        $imgAr= '';
        $lang=HowtoUseLang::where('how_to_id',$id)->get();
       
        if($lang[0]->image){
            $imgEn=url('uploads/' . $lang[0]->image);
            
        }
        if($lang[1]->image){
            $imgAr=url('uploads/' . $lang[1]->image);
        }
       
     

        $use_data=HowtoUse::with('lang')
                 ->where('id',$id)
                 ->first();
                 return[
                     'page'=>$use_data,
                     'imgEn'=>$imgEn,
                     'imgAr'=>$imgAr
                 ];
    }
    public function removeFile(Request $request)
    {
        // Storage::delete($salon->getOriginal($request->type));
        // $salon->update(["{$request->type}" => null]);

        if($request->type ==='screenEn')
        {
            
            HowtoUseLang::where('how_to_id',$request->id)
                        ->where('language','en')
                        ->update([
                                'image' =>NULL,
                            ]);
        }else{
            
                HowtoUseLang::where('how_to_id',$request->id)
                            ->where('language','ar')
                            ->update([
                                    'image' =>NULL,
                                ]);
        
        }
        return [
            'msg'=>'success'
        ];
    }
    public function uploadFile($id,Request $request)
    {  
        $file = request()->file('photo');
        $path = $file->store("screenshot", ['disk' => 'public_uploads']);
      
        if($request->type === 'screenEn')
        {
        
            HowtoUseLang::where('how_to_id',$id)
            ->where('language','en')
            ->update([
                "image" => $path,
            ]);
    
        }else{
            HowtoUseLang::where('how_to_id',$id)
            ->where('language','ar')
            ->update([
                "image" => $path,
            ]);
        }
        
        return ['image' => url('uploads/' . $path)];
    }
    public function update(Request $request)
    {
        $region= DB::transaction(function () use ($request) {
            
            HowtoUseLang::where('language','en')
            ->where('how_to_id',$request->id)
            -> update([
                'title'=>$request->title_en,
                'content'=>$request->content_en
            ]);
            HowtoUseLang::where('language','ar')
            ->where('how_to_id',$request->id)
            -> update([
                'title'=>$request->title_ar,
                'content'=>$request->content_ar
            ]);

           
        });
        return [
            'msg'=>'Success'
        ];
    }
}
