<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\RatingSegments;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\DataTables\RatingSegmentDataTable;
use App\Http\Requests\SegmentRequest;
use App\Models\RatingSegmentsLang;

class RatingSegmentsController extends Controller
{
    // public function get(RatingSegmentDataTable $dataTable)
    // {
    //     return $dataTable->render('admin.settings.rating.index');  
    // }
    public function get()
    {
        $rating=RatingSegments::with('lang')->get();
        return view('admin.settings.rating.index',compact('rating'));
    }

    public function edit($id)
    {
        $rating=RatingSegments::with('lang')->where('id',$id)->first();
        return[
            'rating' => $rating
        ];
    }
    public function update(Request $request,RatingSegments $segments )
    {
        // dd($request);
        // $segments->load('lang');
        DB::transaction(function () use ($request ,$segments) {
            RatingSegmentsLang::where('language','en')
            ->where('rating_segment_id',$request->id)
            ->update([
                'name'=>$request->segmentTitleEn
            ]);
            RatingSegmentsLang::where('language','ar')
            ->where('rating_segment_id',$request->id)
            ->update([
                'name'=>$request->segmentTitleAr
            ]);
            // $segments->lang
            //     ->firstWhere('language', 'en')
            //     ->where('rating_segment_id',$request->id)
            //     ->update([
            //         'name' => $request->segmentTitleEn,
                    
            //     ]);

            // $segments->lang
            //     ->firstWhere('language', 'ar')
            //     ->where('rating_segment_id',$request->id)
            //     ->update([
            //         'name' => $request->segmentTitleEn,
                    
            //     ]);
        
        });

        return [
            'status'=>'200',
            'msg'=>'Success'
        ];
    }
    public function statusUpdate(Request $request)
    {
       
        $status=$request->status === 'Deactivate' ? 'deactive':'active';
        RatingSegments::where('id',$request->id)->update([
            'status'=>$status
        ]);

        return response()->json(['status' => 1, 'message' => 'Status updated successfully']);
    }
}
