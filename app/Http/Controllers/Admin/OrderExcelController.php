<?php

namespace App\Http\Controllers\Admin;

use PDF;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Exports\AdminOrderExport;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Config;

class OrderExcelController extends Controller
{
    public function downloadExcel()
    {
        return Excel::download(new AdminOrderExport,'Orders.xlsx');
    }

    public function detailsPdfDownload($id)
    {
        $lang = 'en';
        $details = Order::with(['items' => function ($query) use ($lang) {
                $query
                    ->with('supplier')
                    ->with('branch')
                    ->with('driver')
                    ->with(['product' => function ($query) use ($lang) { }]);
                
            }])
            ->with('customer')
            ->where('id', $id)
            ->first();
        $supplier = DB::table('order_items')
            ->where('order_id', $id)
            ->distinct('branch_id')
            ->count('branch_id');
        $ord_status = (Config::get('constants.order_status'));
        if(in_array($details->order_status,[1,2,3]))
        {
            $pdf = PDF::loadView('admin.order.collection_pdf_view', compact('details','supplier','ord_status'));
                return $pdf->stream('order.pdf');
        }else{
            $pdf = PDF::loadView('admin.order.delivery_pdf_view', compact('details','supplier','ord_status'));
                return $pdf->stream('order.pdf');
        }
    }
}
