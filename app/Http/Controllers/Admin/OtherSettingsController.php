<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\OtherSettings;
use App\Http\Controllers\Controller;
use Validator;

class OtherSettingsController extends Controller
{
    public function get()
    {
        $other = OtherSettings::first();

        return view('admin.settings.other.index', compact('other'));
    }


    public function store(Request $request)
    {


        $other = OtherSettings::first();
        $data = [
            'currency' => json_encode([
                'currency_en' => $request->currency_en,
                'currency_ar' => $request->currency_ar
            ]),
            'driver_loc_fetch_interval' => json_encode([
                'fetch_time' => $request->fetch_time,
                'cancel_time' => $request->cancel_time
            ]),
            'payment_options' => json_encode([
                'cod' => $request->cod,
                'online' => $request->online, 'ofline' => $request->ofline
            ]),
            'warehouse_loc' => json_encode([
                'latitude' => $request->latitude,
                'longitude' => $request->longitude,
                'loc_name'=>$request->location
            ]),
            'cod_fee' => $request->codfee,
            'delivery_days' => $request->delivery_days,
            'auto_approval_for_cod_orders' => $request->order ? "enable" : "disable",
            'rating' => $request->rating ? "enable" : "disable",
            'vat_percentage' => $request->vat,
            'operational_area' => $request->latlng,
            'sms' => $request->sms ? "enable" : "disable",
            

        ];
        if (!empty($other)) {
            $setting_save = OtherSettings::where('id', $other->id)->update(
                $data
            );
            return redirect()->route('other.get')->with('success', 'Other Settings Updated Successfully');
        } else {
            $setting_save = OtherSettings::Create(
                $data
            );
            return redirect()->route('other.get')->with('success', 'Other setting Added Successfully');
        }
    }
}
