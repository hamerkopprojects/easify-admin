<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\Region;
use App\Models\City;
use App\Models\Customer;
use App\Models\Address;
use App\Models\CustomerBranch;
use App\Models\BusinessType;
use Validator;
use Config;
use DB;

class CustomerController extends Controller {

    //list customer
    public function index(Request $req) {
        $search = $req->search;
        $query = Customer::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->where('cust_name', 'like', "%" . $search . "%");
                $sub->orWhere('email', 'like', "%" . $search . "%");
                $sub->orWhere('phone', 'like', "%" . $search . "%");
                $sub->orWhere('cust_id', 'like', "%" . $search . "%");
            });
        }
        $customer = $query->where('deleted_at', null)
                ->orderBy('id', 'desc')
                ->paginate(20);

        $data = [
            'customer' => $customer,
            'search' => $search
        ];
        return view('admin.customer.index', $data);
    }

    public function create(Request $req) {
        $_customer_id = $req->id;
        $row_data = array();
        if ($_customer_id != '') {
            $row_data = Customer::where('id', '=', $_customer_id)->first();
        }
        $region = Region::with('lang')->get();
        
        $city = City::with('lang')->get();
        $business_type = BusinessType::with('lang')->get();
        $data = [
            'row_data' => $row_data,
            'region' => $region,
            'city' => $city,
            'business_type' => $business_type
        ];
        return view('admin.customer.create', $data);
    }

    public function save(Request $req) {
        if ($req->_customer_id) {
            $unique = ',' . $req->_customer_id;
            $rules = [
                'cust_name' => 'required',
                'business_name' => 'required',
                'email' => 'required|unique:customer,email' . $unique.',id,deleted_at,NULL',
                'phone' => 'required|unique:customer,phone' . $unique,
            ];
        } else {
            $rules = [
                'cust_name' => 'required',
                'business_name' => 'required',
                'email' => 'required|unique:customer,email,NULL,id,deleted_at,NULL',
                'phone' => 'required|unique:customer,phone,NULL,id,deleted_at,NULL',
            ];
        }

       
        $messages = [
            'cust_name.required' => 'Name is required.',
            'business_name.required' => 'Business name is required.',
            'email.required' => 'Email is required.',
            'email.unique' => 'Customer with same email already exists',
            'phone.required' => 'Phone number is required.',
            'phone.unique' => 'Customer with same Phone number already exists',
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $data_to_save = [
                'cust_name' => $req->cust_name,
                'business_type' => $req->business_type,
                'business_name' => $req->business_name,
                'email' => $req->email,
                'phone' => (int)$req->phone,
                'region' => $req->region,
                'city' => $req->city,
                'dob' => $req->dob,
                
            ];
            if ($req->_customer_id == '') {
                $cust_id = $this->generateCustomerId();
                $data_to_save['cust_id'] = $cust_id;
                $data_to_save['created_at'] = Carbon::now();
                $saved_data = Customer::insert($data_to_save);
                $msg = 'Customer added successfully.';
            } else {
                $saved_data = Customer::where('id', $req->_customer_id)
                        ->update($data_to_save);
                $msg = 'Customer updated successfully.';
            }
            if ($saved_data) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }

    // delete customer
    public function deleteCustomer(Request $req) {
        $cust = Customer::find($req->id);
        if ($req->id) {
            $cust_branch = CustomerBranch::select('id')->where('customer_id', $req->id)->exists();
            if($cust_branch == false){
            $address = Address::select('id')->where('cust_id', $req->id)->get();
            if(!empty($address)){
            $address->each->delete();
            }
            $cust->delete();
            return response()->json(['status' => 1, 'message' => 'Customer deleted successfully']);
            }else{
            return response()->json(['status' => 0, 'message' => 'This customer has branches so it cannot be deleted']);
           }
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }

    // activate/deactivate customer
    public function activate(Request $req) {
        $cust = Customer::where('id', $req->id)->first();
        if ($cust) {
            if ($cust->status == 'N') {
                Customer::where('id', $req->id)
                        ->update([
                            'status' => 'Y'
                ]);
            } else {
                Customer::where('id', $req->id)
                        ->update([
                            'status' => 'N'
                ]);
            }
            return response()->json(['status' => 1, 'message' => 'Status updated successfully']);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }

    public function generateCustomerId() {
        $lastcustomer = Customer::select('cust_id')
                ->orderBy('id', 'desc')
                ->first();

        $lastId = 0;

        if ($lastcustomer) {
            $lastId = (int) substr($lastcustomer->cust_id, 9);
        }

        $lastId++;

        return 'EAS-CUST-' . str_pad($lastId, 5, '0', STR_PAD_LEFT);
    }

    //view customer
    public function details(Request $req) {
        
        $cust = Customer::with('customer_region')->with('customer_city')->where('id', $req->id)->first();

        $delivery_address = Address::leftJoin('region_i18n', function ($join) {
                    $join->on('region_i18n.region_id', '=', 'address.region');
                    $join->where('region_i18n.language', '=', 'en');
                })
                ->select('address.*', 'region_i18n.name as reg_name')
                ->where(['address.cust_id' => $req->id, 'address.address_flag' => 'D'])
                ->orderBy('address.id', 'asc')
                ->get();
                
        $billing_address = Address::leftJoin('region_i18n', function ($join) {
                    $join->on('region_i18n.region_id', '=', 'address.region');
                    $join->where('region_i18n.language', '=', 'en');
                })
                ->select('address.*', 'region_i18n.name as reg_name')
                ->where(['address.cust_id' => $req->id, 'address.address_flag' => 'B'])
                ->orderBy('address.id', 'asc')
                ->get();


        $data = [
            'cust_data' => $cust,
            'cust_id' => $req->id,
            'delivery_address' => $delivery_address,
            'billing_address' => $billing_address,
        ];
        return view('admin.customer.details', $data);
    }

    public function create_address(Request $req) {
        $address_type = $req->type;
        $cust_id = $req->cust_id;
        $_address_id = $req->id;
        $row_data = array();
        $d_count = Address::where(['cust_id' => $cust_id, 'address_flag' => 'D'])->count();
        $b_count = Address::where(['cust_id' => $cust_id, 'address_flag' => 'B'])->count();
        if ($_address_id != '') {
            $row_data = Address::where(['id' => $_address_id, 'address_flag' => $address_type])->first();
        }

        $region = Region::leftJoin('region_i18n', 'region_i18n.region_id', '=', 'region.id')
                ->select('region_i18n.name as reg_name', 'region_i18n.region_id as id')
                ->where('region_i18n.language', '=', 'en')
                ->get(['id', 'reg_name']);

        $data = [
            'row_data' => $row_data,
            'address_type' => $address_type,
            'cust_id' => $cust_id,
            'd_count' => $d_count,
            'b_count' => $b_count,
            'region' => $region,
        ];
        return view('admin.customer.create_address', $data);
    }

    // Edit / save Address
    public function save_address(Request $req) {
//echo $req->is_default;exit;
        $rules = [
            'apartment_name' => 'required',
            'street_name' => 'required',
            'postal_code' => 'required',
            'region' => 'required',
        ];
        $messages = [
            'apartment_name.required' => 'Apartment Name is required.',
            'street_name.required' => 'Street Name is required.',
            'postal_code.required' => 'Postal Code is required.',
            'region.required' => 'Region is required.',
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $d_count = Address::where(['cust_id' => $req->cust_id, 'address_flag' => $req->address_type])->count();
            if ($d_count > 0 && $req->is_default == 'Y') {
                Address::where(['cust_id' => $req->cust_id, 'address_flag' => $req->address_type])
                        ->update([
                            'is_default' => 'N'
                ]);
            }
            if ($req->is_default == 'Y') {
                $is_default = 'Y';
            } else {
                $is_default = 'N';
            }

            $data_to_save = [
                'address_flag' => $req->address_type,
                'cust_id' => $req->cust_id,
                'apartment_name' => $req->apartment_name,
                'street_name' => $req->street_name,
                'postal_code' => $req->postal_code,
                'region' => $req->region,
                'is_default' => $is_default,
            ];
            if ($req->_address_id == '') {
                $data_to_save['created_at'] = Carbon::now();
                $saved_data = Address::insert($data_to_save);
                $msg = 'Address added successfully.';
            } else {
                $saved_data = Address::where('id', $req->_address_id)
                        ->update($data_to_save);
                $msg = 'Address updated successfully.';
            }
            if ($saved_data) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }

// set default address
    public function set_default(Request $req) {
        $_address_id = $req->id;
        $type = $req->type;
        if ($_address_id) {
            Address::where(['cust_id' => $req->cust_id, 'address_flag' => $req->type])
                    ->update([
                        'is_default' => 'N'
            ]);
            Address::where(['id' => $_address_id])
                    ->update([
                        'is_default' => 'Y'
            ]);

            return response()->json(['status' => 1, 'message' => 'Set default address successfully.']);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }

    public function delete_address(Request $req) {
        $address = Address::where('id', '=', $req->id)->firstOrFail();
        if ($address) {
            $address->delete();
            return response()->json(['status' => 1, 'message' => __('frontend.addr_delet')]);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }

}
