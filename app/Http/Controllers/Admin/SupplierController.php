<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\Supplier;
use App\Models\Region;
use App\Models\City;
use App\Models\UserRoles;
use App\Models\BranchAnnex;
use App\Models\ProductBranchStock;
use App\Mail\SetPasswordEmail;
use App\Models\CommisionCategory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Models\Product;
use App\Models\SupplierAnnex;
use Validator;
use Config;
use DB;

use URL;

use App\Services\SendEmail;
use App\Traits\LoginActivityLog;

class SupplierController extends Controller {

    use LoginActivityLog ;
    public function index(Request $req) {
        $query = Supplier::where('deleted_at', null);
        $search = $req->search;
        if ($search) {
            $query->where('name', 'like', "%" . $search . "%");
            $query->orWhere('email', 'like', "%" . $search . "%");
            $query->orWhere('phone', 'like', "%" . $search . "%");
            $query->orWhere('code', 'like', "%" . $search . "%");
        }
        $supplier_list = $query->where('role_id', 5)->paginate(20);

        return view('admin.supplier.index', compact('supplier_list', 'search'));
    }

    public function create(Request $req) {
        $_supplier_id = $req->id;
        $city = City::with('lang')->get();
        $row_data = $region = array();
        $commision = CommisionCategory::where('deleted_at',null)->get();
        if ($_supplier_id != '') {
            $row_data = Supplier::with('supplier_annex')->where('id', '=', $_supplier_id)->first();
            $region = Region::with('lang')->where(['city_id' => $row_data->city_id])->get();
        }
        $this->loginLog(auth()->user()->id);
        return view('admin.supplier.create', compact('row_data', 'region', 'city','commision'));
    }

    public function save(Request $req) {
        // dd($req->toArray());
        if ($req->_supplier_id) {
            $unique = ',' . $req->_supplier_id;
            $rules = [
                'name' => 'required',
                'contact_person_name' => 'required',
                'email' => 'required|unique:supplier,email' . $unique . ',id,deleted_at,NULL',
                'phone' => 'required|unique:supplier,phone' . $unique,
                'commision' => 'required',
                'product_image_en'=>'nullable|file|mimes:png,jpg,jpeg,pdf'
            ];
        } else {
            $rules = [
                'name' => 'required',
                'contact_person_name' => 'required',
                'email' => 'required|unique:supplier,email,NULL,id,deleted_at,NULL',
                'phone' => 'required|unique:supplier,phone,NULL,id,deleted_at,NULL',
                'commision' => 'required',
                'product_image_en'=>'nullable|file|mimes:png,jpg,jpeg,pdf'
            ];
        }


        $messages = [
            'name.required' => 'Name is required.',
            'contact_person_name.required' => 'Contact person name is required.',
            'email.required' => 'Email is required.',
            'email.unique' => 'Supplier with same email already exists',
            'phone.required' => 'Phone number is required.',
            'phone.unique' => 'Supplier with same Phone number already exists',
            'commision.required' => 'Commision category is required',
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $roles = UserRoles::where('name', "Supplier")->first();
            $data_to_save = [
                'role_id' => $roles->id,
                'name' => $req->name,
                'contact_person_name' => $req->contact_person_name,
                'email' => $req->email,
                'phone' => (int) $req->phone,
                'region_id' => $req->region_id,
                'city_id' => $req->city_id,
            ];
            if (!empty($req->file('product_image_en'))) {
                $current = Carbon::now()->format('YmdHs');
                $file = $req->file('product_image_en');
               
                $name_en = $current . $file->getClientOriginalName();
                
                $path_en = $file->move(public_path() . '/uploads/supplier/cr_copy/', $name_en);
                $path_file = 'supplier/cr_copy/' . $name_en;
                
            }
            if ($req->_supplier_id == '') {
                // $data_to_save['code'] = $this->generateSupplierId();
                $data_to_save['created_at'] = Carbon::now();
                // $saved_data = Supplier::create([$data_to_save]);
                $saved_data = Supplier::create(
                    [
                    'role_id' => $roles->id,
                    'name' => $req->name,
                    'contact_person_name' => $req->contact_person_name,
                    'email' => $req->email,
                    'phone' => (int) $req->phone,
                    'region_id' => $req->region_id,
                    'city_id' => $req->city_id,
                    'code'=> $this->generateSupplierId(),
                    'created_at'=>Carbon::now()

                ]
            );
                
                $saved_data->supplier_annex()->createMany([
                   [ 'cr_copy'=>$path_file ?? '',
                    'commision_category_id'=>$req->commision
                   ]
                ]);
                $msg = 'Supplier added successfully.';
            } else {
                $regions = array();
                $branch_data = BranchAnnex::with(['supplier' => function($query) {
                                $query->where('role_id', 6);
                            }])->where('supplier_parent_id', $req->_supplier_id)->get();
                foreach ($branch_data as $value) {
                    $regions[] = $value->supplier->region_id;
                }
                $regions = array_unique($regions);
                if (in_array($req->region_id, $regions)) {
                    return response()->json(['status' => 0, 'message' => 'Seems supplier / branch exist with region selected.']);
                }
                
                $supp_anx = SupplierAnnex::where('supplier_id',$req->_supplier_id)->first();

                 
                
                $saved_data = Supplier::where('id', $req->_supplier_id)
                        ->update($data_to_save);
                if($supp_anx){
                SupplierAnnex::where('supplier_id',$req->_supplier_id)
                ->update([
                    'cr_copy'=>$path_file ?? $supp_anx->cr_copy,
                    'commision_category_id'=>$req->commision
                ]);
                }else{
                    SupplierAnnex::create([
                    'supplier_id' => $req->_supplier_id,
                    'cr_copy'=>$path_file ?? '',
                    'commision_category_id'=>$req->commision
                   ]);
                }
                $msg = 'Supplier updated successfully.';
            }

            if ($saved_data) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }

    // delete supplier
    public function delete(Request $req) {
        $supplier = Supplier::find($req->id);
        if ($req->id) {
            $branch = BranchAnnex::where(['supplier_parent_id' => $req->id])->exists();
            $product = Product::where(['supplier_id' => $req->id])->exists();
            $productvar = ProductBranchStock::where(['branch_id' => $req->id])->exists();
            if ($branch == false && $product == false && $productvar == false) {
                if ($supplier->role_id == 6) {
                    BranchAnnex::where('supplier_id', $req->id)->delete();
                }
                $supplier->delete();
                return response()->json(['status' => 1, 'message' => 'Supplier deleted successfully']);
            } else {
                return response()->json(['status' => 0, 'message' => 'This supplier cannot be deleted because it is assigned to some of the existing product or branch.']);
            }
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }

    // activate/deactivate supplier
    public function activate(Request $req) {
        $supplier = Supplier::where('id', $req->id)->first();
        if ($supplier) {
            if ($supplier->status == 'deactive') {
                Supplier::where('id', $req->id)
                        ->update([
                            'status' => 'active'
                ]);
            } else {
                Supplier::where('id', $req->id)
                        ->update([
                            'status' => 'deactive'
                ]);
            }
            $this->loginLog(auth()->user()->id);
            return response()->json(['status' => 1, 'message' => 'Status updated successfully']);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }

    public function details(Request $req) {
        $supplier_id = $req->id;
        if ($supplier_id != '') {
            $supplier_data = Supplier::with('region')->with('city')->with('supplier_annex')->where('id', '=', $supplier_id)->first();
        }
        return view('admin.supplier.details', compact('supplier_id', 'supplier_data'));
    }

    public function branches(Request $req) {
        $supplier_id = $req->id;
        $branch_data = array();
        if ($supplier_id != '') {
            $branch_data = BranchAnnex::with(['supplier' => function($query) {
                            $query->where('role_id', 6)->with('region');
                        }])->where('supplier_parent_id', $supplier_id)->get();
        }
        return view('admin.supplier.branch', compact('supplier_id', 'branch_data'));
    }

    public function create_branch(Request $req) {
        $_branch_id = $req->id;
        $supplier_id = $req->supplier_id;
        $city = City::with('lang')->get();
        $row_data = $region = array();
        if ($_branch_id != '') {
            $row_data = Supplier::where('id', '=', $_branch_id)->first();
            $region = Region::with('lang')->where(['city_id' => $row_data->city_id])->get();
        }
        return view('admin.supplier.create_supplier', compact('row_data', 'region', 'city', 'supplier_id'));
    }

    public function save_branch(Request $req) {
        if ($req->_branch_id) {
            $unique = ',' . $req->_branch_id;
            $rules = [
                'name' => 'required',
                'contact_person_name' => 'required',
                'email' => 'required|unique:supplier,email' . $unique . ',id,deleted_at,NULL',
                'phone' => 'required|unique:supplier,phone' . $unique,
            ];
        } else {
            $rules = [
                'name' => 'required',
                'contact_person_name' => 'required',
                'email' => 'required|unique:supplier,email,NULL,id,deleted_at,NULL',
                'phone' => 'required|unique:supplier,phone,NULL,id,deleted_at,NULL',
            ];
        }


        $messages = [
            'name.required' => 'Name is required.',
            'contact_person_name.required' => 'Contact person name is required.',
            'email.required' => 'Email is required.',
            'email.unique' => 'Branch with same email already exists',
            'phone.required' => 'Phone number is required.',
            'phone.unique' => 'Branch with same Phone number already exists',
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $supplier_id = $req->supplier_id;
            $supplier_data = Supplier::where('id', '=', $supplier_id)->first();
            $regions[] = $supplier_data->region_id;
            if ($req->_branch_id == '') {
            $branch_data = BranchAnnex::with(['supplier' => function($query) {
                            $query->where('role_id', 6);
                        }])->where('supplier_parent_id', $supplier_id)->get();
            }else{
                $branch_data = BranchAnnex::with(['supplier' => function($query) {
                            $query->where('role_id', 6);
                        }])->where('supplier_parent_id', $supplier_id)->where('supplier_id', '!=', $req->_branch_id)->get();
            }
            foreach ($branch_data as $value) {
                $regions[] = $value->supplier->region_id;
            }
            $regions = array_unique($regions);
            if (in_array($req->region_id, $regions)) {
                return response()->json(['status' => 0, 'message' => 'Seems supplier / branch exist with region selected.']);
            }

            $roles = UserRoles::where('name', "Branch")->first();
            $data_to_save = [
                'role_id' => $roles->id,
                'name' => $req->name,
                'contact_person_name' => $req->contact_person_name,
                'email' => $req->email,
                'phone' => (int) $req->phone,
                'region_id' => $req->region_id,
                'city_id' => $req->city_id,
            ];
            if ($req->_branch_id == '') {
                $data_to_save['code'] = $this->generateBranchId();
                $data_to_save['created_at'] = Carbon::now();
                $saved_data = Supplier::create($data_to_save);
                BranchAnnex::create([
                    'supplier_id' => $saved_data->id,
                    'supplier_parent_id' => $supplier_id,
                ]);
                $msg = 'Branch added successfully.';
            } else {
                $saved_data = Supplier::where('id', $req->_branch_id)
                        ->update($data_to_save);
                BranchAnnex::where('supplier_id', $req->_branch_id)
                        ->update([
                            'supplier_parent_id' => $supplier_id,
                ]);
                $msg = 'Branch updated successfully.';
            }

            if ($saved_data) {
                $this->loginLog(auth()->user()->id);
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }

    public function generateSupplierId() {

        $roles = UserRoles::where('name', "Supplier")->first();

        $lastsupplier = Supplier::select('code')
                ->where('role_id', $roles->id)
                ->orderBy('id', 'desc')
                ->first();

        $lastId = 0;

        if ($lastsupplier) {
            $lastId = (int) substr($lastsupplier->code, 9);
        }

        $lastId++;

        return 'EAS-SUPP-' . str_pad($lastId, 5, '0', STR_PAD_LEFT);
    }

    public function generateBranchId() {

        $roles = UserRoles::where('name', "Branch")->first();

        $lastbranch = Supplier::select('code')
                ->where('role_id', $roles->id)
                ->orderBy('id', 'desc')
                ->first();

        $lastId = 0;

        if ($lastbranch) {
            $lastId = (int) substr($lastbranch->code, 9);
        }

        $lastId++;

        return 'EAS-BRAN-' . str_pad($lastId, 5, '0', STR_PAD_LEFT);
    }

    public function passwordSend(Request $req) {

        $supplier = Supplier::where('id', $req->id)->first();
//      $password=mt_rand(100000,999999);
        $password = '1234';
        $data = [
            'password' => $password,
            'user' => $supplier
        ];
        Supplier::where('id', $req->id)
                ->update([
                    'password' => Hash::make($password)
        ]);
//             Mail::to($user->email)->send(new SetPasswordEmail($data));
        if(isset($req->from) && $req->from == 'branch'){
         $fom =  'branch';  
         $url = URL::to('/branch/login');
        }else{
          $fom =  'supplier';  
          $url = URL::to('/supplier/login');
        }
        $subject = 'Welcome to EASIFY!';
        
        SendEmail::generateEmail($supplier->email, $password, $supplier->name, $subject, $fom, $url);
        return response()->json(['status' => 1, 'message' => 'One time password send successfully']);
    }

}
