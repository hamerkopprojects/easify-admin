<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\AdminNotification;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\AdminNotificationLang;

class NotificationController extends Controller
{
    public function get(Request $req)
    {
        $search=$req->search_note ?? '';
        if($search)
        {
            $data=AdminNotificationLang::select('notification_id')->where('title', 'like', "%" . $search . "%")->get()->toArray();
            $da=[];
            foreach($data as  $d)
            {
                $da[]=$d['notification_id'];
            }
            $notification= AdminNotification::with('lang')->whereIn('id',$da)->paginate(10);
            // dd($notification);
        //     $notification= AdminNotification::
        //     whereHas('lang', function($q) use($search) 
        //        {
        //            $q->where('title', 'like', "%" . $search . "%");
                  
        //        })
        //     ->with(['lang'=> function($q) use ($search){
        //         $q->where('title', 'like', "%" . $search . "%");;
        //        // dd($q);
        //    }])

        //    dd($note);
            //  ->paginate(20);
            return view('admin.notification.index',compact('notification','search'));
        }else{
            $notification =AdminNotification::with('lang')->paginate(20);
            return view('admin.notification.index',compact('notification','search'));
        }
       
    }
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'app_value'=>'required',
            'title_en'=>'required',
            'notification_en'=>'required',
            'title_ar'=>'required',
            'notification_ar'=>'required'
        ],[
            'app_value.required'=>'App type required',
            'title_en.required'=>'Title(EN) required',
            'title_ar.required'=>'Title (AR) required',
            'notification_en.required'=>'Notification (EN) required',
            'notification_ar.required'=>'Notification (AR) required'

        ]);
        $note= DB::transaction(function () use ($request) {
            
            $note = AdminNotification::create([
                'app_type'=>$request->app_value
            ]);

            $note->lang()->createMany([
                [
                    'title' => $request->title_en,
                    'content'=>$request->notification_en,
                    'language' => 'en',
                ],
                [
                    'title' => $request->title_ar,
                    'content'=>$request->notification_ar,
                    'language' => 'ar',
                ],
            ]);

            return $note;
        });
       if($note)
       {
            return redirect()->route('notification.get')->with('success','Notification added successfully');
       }else{
        return redirect()->route('notification.get')->with('error','Erroe !!');
       }
    }

    public function destroy(Request $request)
    {
        $del=AdminNotification::find($request->id);
        $del->lang()->delete();
        $del->delete();
        return response()->json(['status' => 1, 'message' => 'Notification deleted successfully']);
    }
    public function autoComplete(Request $req)
    {
        $lang=AdminNotificationLang::
        where('title', 'like', "%" . $req->search . "%")
        // ->orwhere('content','like', "%" . $req->search . "%")
        ->get();
        $response = array();
      foreach($lang as $lan){
         $response[] = array("value"=>$lan->notification_id,"label"=>$lan->title);
      }

      return response()->json($response);
    }
}
