<?php

namespace App\Http\Controllers\Admin;

use App\Models\Faqs;
use App\Models\AppType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\FaqApps;
use App\Models\FaqsLang;

class FaqsController extends Controller
{
    public function get(Request $request)
    {
        // $faq=Faqs::with('lang')->paginate(20);
        // $faq ='';
        $search=$request->faq_select;
        $search_field=$request->search_field ?? '';
        $app_type=AppType::get();
        // dd($app_type);
        $faqLang =FaqsLang::where('language','en')->get();
        // dd($faqLang);
        if($search_field)
        {
            $data=FaqsLang::select('faq_id')->where('question', 'like', "%" . $search_field . "%")
            ->orwhere('answer','like', "%" . $search_field . "%")->get()->toArray();
            $da=[];
            foreach($data as  $d)
            {
                $da[]=$d['faq_id'];
            }
            $faq= Faqs::with('lang')->with('appType')->whereIn('id',$da)->paginate(20);
        
        }else{
            $faq=Faqs::with('lang')->with('appType')->paginate(20);
            // dd($faq);
        }
        return view('admin.cms.faqs.index',compact('faq','app_type','faqLang','search_field'));
    }

    public function store(Request $request)
    {
        // dd(json_encode($request->app_type));
        
        $faq= DB::transaction(function () use ($request) {
             $faq=Faqs::create([
                 'status' => 'active',
             ]);
             $faq->lang()->createMany([
                [
                    'question' => $request->title_en,
                    'answer' => $request->content_en,
                    'language' => 'en',
                ],
                [
                    'question' => $request->title_ar,
                    'answer' => $request->content_ar,
                    'language' => 'ar',
                ],
             ]);
             if($request->app_type ){
                 $faq->appType()->attach($request->app_type,
                 ['created_at' => now(), 'updated_at' => now()]);
                // foreach($request->app_type  as $apps)
                // {
                //    $faq->appType()->createMany([
                //        [
                //           'app_id'=>$apps
                //        ]
                       
                //    ]);
                // }
             }
             
            

             return $faq;
         });
         if($faq)
         {
             return [
                 "msg"=>"success"
             ];
         }else{
            return [
                "msg"=>"error"
            ];
         }
    }
    public function statusUpdate(Request $request)
    {
      
        $status=$request->status === 'Deactivate' ? 'deactive' : 'active' ;
        Faqs::where('id',$request->id)
             ->update([
                'status'=>$status
        ]);
        return response()->json(['status' => 1, 'message' => "Status updated successfully"]);
    }
    public function edit($id)
    {
        $faq=Faqs::with('lang')
                 ->with('appType')
                    ->where('id',$id)
                    ->first();
        // dd($faq);
        return [
            'page'=>$faq,
            
        ];
    }
    public function update(Request $request)
    {
        $faq= DB::transaction(function () use ($request) {
           
            FaqsLang::where('faq_id',$request->id)
            ->where('language','en')
            ->update(
               [
                   'question' => $request->title_en,
                   'answer' => $request->content_en,
                  
               ]);
               FaqsLang::where('faq_id',$request->id)
               ->where('language','ar')
               ->update(
               [
                   'question' => $request->title_ar,
                   'answer' => $request->content_ar,
                   
               ]);
            
               $faq= FaqApps::where('faq_id',$request->id);
               $faq->delete();
               foreach($request->app_type  as $apps)
                {
                   FaqApps::create([
                       'faq_id'=>$request->id,
                       'app_id'=>$apps
                   ]);
                }
        });

            return [
                'msg'=>"success"
            ];
    }

    public function destroy(Request $request )
    {
       $faq=Faqs::find($request->id);
       $faq->lang()->delete();
         $faq->appType()->detach();
        $faq->delete();
        return response()->json(['status' => 1, 'message' => 'FAQ deleted successfully']);
    }
    public function search(Request $req)
    {
        $lang=FaqsLang::where('question', 'like', "%" . $req->search . "%")->get();
        $response = array();
        foreach($lang as $lan){
         $response[] = array("value"=>$lan->faq_id,"label"=>$lan->question);
        }

      return response()->json($response);
    }
}
