<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\Region;
use App\Models\Review;
use App\Models\Customer;
use App\Models\Supplier;
use App\Models\OrderItems;
use App\Models\RegionLang;
use App\Services\SendEmail;
use Illuminate\Http\Request;
use App\Models\OtherSettings;
use App\Models\ProductRating;
use App\Models\ProductVariant;
use App\Traits\FormatLanguage;
use App\Models\AppNotifications;
use App\Models\CancellationReason;
use Illuminate\Support\Facades\DB;
use App\Models\BestsellingProducts;
use App\Traits\CanSaveNotification;
use App\Traits\CanSendNotification;
use App\Http\Controllers\Controller;
use App\Models\CancellationReasonLang;
use App\Traits\LoginActivityLog;
use App\Traits\OrderActivityLog;
use Illuminate\Support\Facades\Config;

class OrderController extends Controller
{
    use FormatLanguage,CanSaveNotification,CanSendNotification,OrderActivityLog,LoginActivityLog;

    protected $order;

    const SINGLE = 1;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function activeCollectionOrder(Request $req)
    {
        $ord_status = (Config::get('constants.order_status'));

        $cust_id = $req->customer ?? '';
        $search = $req->search;
        $stat = $req->status;
        $selected_region = $req->region;
        $date = $req->date;
        $query = $this->order->with('product')
            ->with('customer')
            ->whereNotIn('order_status', [6, 8, 9, 7]);
        if ($req->search) {
            $query->where('order_id', 'like', "%" . $req->search . "%");
        }
        if ($req->date) {
            $date_new = Carbon::parse($req->date)->format('Y-m-d');
            $query->whereDate('created_at', $date_new);
        }
        if ($req->customer) {
            $query->where('customer_id', $req->customer);
        }
        if ($req->status) {
            $query->where('order_status', $req->status);
        }
        if ($req->region) {
            $query->where('delivery_loc', 'like', "%" . $req->region . "%");
        }
        $active = $query->orderBy('created_at','desc')->paginate(20);
        $customers = Customer::where('status', 'Y')->get();
        $regions = RegionLang::where('deleted_at', null)
            ->where('language', 'en')
            ->get();
        return view('admin.order.collection', compact('active', 'customers', 'cust_id', 'ord_status', 'search', 'date', 'stat', 'regions', 'selected_region'));
    }
    public function activeDeliveryOrder(Request $req)
    {
        $search_region = $req->region;
        $cust_id = $req->search_select;
        $search = $req->search;
        $ord_status = (Config::get('constants.order_status'));
        $query = $this->order->with('product')
            ->with('customer')
            ->with('driver')
            ->whereIn('order_status', [7,8]);
        if ($req->search) {
            $query->where('order_id', 'like', "%" . $req->search . "%");
        }
        if ($req->date) {
            $date_new = Carbon::parse($req->date)->format('Y-m-d');
            $query->whereDate('delivery_schedule_date', $date_new);
        }
        if ($req->search_select) {
            $query->where('customer_id', $req->search_select);
        }
        if ($req->region) {
            $query->where('delivery_loc', $req->region);
        }

        $active = $query->paginate(20);
        $customers = Customer::where('status', 'Y')->get();
        $regions = Region::with(['lang' => function ($q) {
            $q->where('language', 'en');
        }])
            ->get();
        return view('admin.order.delivery', compact('active', 'customers', 'regions', 'search_region', 'cust_id', 'search','ord_status'));
    }
    public function cancelledOrder(Request $req)
    {
        $search_region = $req->region;
        $cust_id = $req->search_select;
        $search = $req->search;
        $query = $this->order->with('product')
            ->with('customer')
            ->where('order_status', 6);
        if ($req->search) {
            $query->where('order_id', 'like', "%" . $req->search . "%");
        }
        if ($req->date) {
            $date_new = Carbon::parse($req->date)->format('Y-m-d');
            $query->whereDate('delivery_schedule_date', $date_new);
        }
        if ($req->search_select) {
            $query->where('customer_id', $req->search_select);
        }
        if ($req->region) {
            $query->where('delivery_loc', $req->region);
        }
        $active = $query->paginate(20);
        $customers = Customer::where('status', 'Y')->get();
        $regions = Region::with(['lang' => function ($q) {
            $q->where('language', 'en');
        }])
            ->get();
        
        return view('admin.order.cancel', compact('active', 'customers', 'regions', 'cust_id', 'search_region', 'search'));
    }
    public function CompletedOrder(Request $req)
    {
        $search_region = $req->region;
        $cust_id = $req->search_select;
        $search = $req->search;
        $query = $this->order->with('product')
            ->with('customer')
            ->where('order_status', 9);
        if ($req->search) {
            $query->where('order_id', 'like', "%" . $req->search . "%");
        }
        if ($req->date) {
            $date_new = Carbon::parse($req->date)->format('Y-m-d');
            $query->whereDate('delivery_schedule_date', $date_new);
        }
        if ($req->search_select) {
            $query->where('customer_id', $req->search_select);
        }
        if ($req->region) {
            $query->where('delivery_loc', $req->region);
        }
        $grant_sum = $query->sum('grant_total');
        $active = $query->paginate(20);
        $customers = Customer::where('status', 'Y')->get();
        $regions = Region::with(['lang' => function ($q) {
            $q->where('language', 'en');
        }])
            ->get();
        return view('admin.order.completed', compact('active', 'customers', 'regions', 'cust_id', 'search_region', 'search', 'grant_sum'));
    }
    public function orderDetails($id)
    {
        $lang = 'en';
        $ord_status = (Config::get('constants.order_status'));
        $pro_status = (Config::get('constants.product_status'));
        $details = $this->order
            ->with(['items' => function ($query) use ($lang) {
                $query
                    ->with('supplier')
                    ->with('branch')
                    ->with('driver')
                    ->with(['product' => function ($query) use ($lang) { }]);
            }])
            ->with('customer')
            ->with(['cancellation' => function ($query) use ($lang) {
                $query->with(['lang' => function ($query) use ($lang) {
                    $query->where('language', $lang);
                }]);
            }])
            ->where('id', $id)
            ->first();
        $order_cancel=Order::with('owner')->where('id',$id)->first();
        // $supplier_new = DB::table('order_items')
        //     ->where('order_id', $id)
        //     ->distinct('supplier_id')
        //     ->count('supplier_id');
        $supplier = DB::table('order_items')
            ->where('order_id', $id)
            ->distinct('branch_id')
            ->count('branch_id');
        $driver = User::where('role_id', 3)->where('status', 'active')->get();
        if (in_array($details->order_status, [1])) {
            return view('admin.order.details.supplier_action', compact('details', 'driver', 'supplier', 'ord_status','pro_status'));
        }
        else if (in_array($details->order_status, [3, 2, 4])) {
            return view('admin.order.details.collection', compact('details', 'driver', 'supplier', 'ord_status','pro_status'));
        } else if (in_array($details->order_status, [9, 6])) {
            return view('admin.order.details.completed', compact('details', 'ord_status','order_cancel'));
        } elseif ($details->order_status == 5) {
            return view('admin.order.details.collection_complete', compact('details', 'ord_status'));
        } elseif (in_array($details->order_status, [7,8])) {
            return view('admin.order.details.active_delivery', compact('details', 'supplier', 'ord_status'));
        }
    }
 
    public function scheduleFormGet(Request $req)
    {
        $pro_status = (Config::get('constants.product_status'));
        $order_id = $req->order_id;
        // $supplier_id = $req->supplier_id;
        $branch_id = $req->branch_id;
        $supplier_details = Supplier::where('deleted_at', null);
        $collection_stat = OrderItems::where('order_id',$req->order_id)
        ->where('branch_id',$req->branch_id)
        ->first();
        $order_details = Order::where('id', $order_id)
            ->with('items')
            ->where('deleted_at', null)
            ->first();
        $other_settings = OtherSettings::first();

        if ($branch_id) {
            $supplier_region = $supplier_details->where('id', $branch_id)->first();
            $list_drivers = Region::select('driver')->where('id', $supplier_region->region_id)->get()->toArray();
            $drivers = User::select('name', 'id')
                ->where('role_id', 3)
                // ->whereIn('id', $list_drivers)
                ->where('status', 'active')
                ->get();
            
            $select_driver =  $region = Region::select('driver','id')->where('id',$supplier_region->region_id)->first();
           
        } else {
            $drivers = User::select('name', 'id')
                ->where('role_id', 3)
                ->where('status', 'active')
                ->get();
        }
        if ($req->type == 'complete_schedule') {
            $status = "schedule for delivery";
            return view('admin.order.details.collection-completed-schedule-form', compact('order_id', 'drivers', 'other_settings', 'branch_id', 'status', 'order_details'));
        } else if ($req->type == 'active_delivery') {

            $status = "schedule for delivery";
            return view('admin.order.details.active_delivery_form', compact('order_id', 'drivers', 'other_settings', 'branch_id', 'status', 'order_details'));
        }
        $status = '';
        return view('admin.order.details.schedule-form', compact('order_id', 'drivers', 'other_settings', 'branch_id', 'status', 'order_details','collection_stat','pro_status','select_driver'));
    }
    public function getTimeSlot(Request $req)
    {
        $time_slot = Config::get('constants.schedule_time');
        return $time_slot;
    }
    public function storeSchedule(Request $req)
    {
        
        if ($req->date) {
            $parsed = Carbon::parse($req->date)->format('Y-m-d');
        }
        $order_items = OrderItems::where('order_id', $req->order)
            ->where('branch_id', $req->supplier)
            ->first();
        $current_order = Order::where('id', $req->order)->first();
        OrderItems::where('order_id', $req->order)
            ->where('branch_id', $req->supplier)
            ->whereIn('status',[4,2])
            ->update([
                'driver_id' => $req->driver,
                'collection_date' => $parsed ?? $order_items->collection_date,
                'collection_time_slot' => $req->slot ?? $order_items->collection_time_slot,
                'status'=> 4

            ]);
            if($order_items->status == 2)
            {
                $this->sendNotificatioTosuppliers($req->supplier,$req->order);
            }
            $item_count = DB::table('order_items')
            ->where('order_id',$req->order)
            ->count();
        $collected_count = DB::table('order_items')
            ->where('order_id',$req->order)
            ->whereIn('status',[4,3])
            ->count();
            if($item_count ==$collected_count )
            {
                $current_order->update([
                    'order_status'=> 3
                ]);
                $ord = OrderItems::where('order_id',$req->order)->groupBy('branch_id')->get();
                foreach ($ord as $my_or) {
                    // dd($my_or->driver_id);
                    $this->notifyDriver($req->order,'collection',$my_or->driver_id,$req->date, $req->slot,$req->supplier);
                }
               
            }
            
            // $this->toCollectionPending($req->order);
            // if($req->status == 'collected')
            // {
            //     $this->updateOrderStatus($req->order,$req->supplier);
            // }

        // Order::where('id', $req->order)
        //     ->update([
        //         'order_status' => $req->status ?? $current_order->order_status
        //     ]);
        // if($req->status == '3')
        // {
        //     $order_items = OrderItems::where('order_id', $req->order)
        //     ->where('branch_id', $req->supplier)
        //     ->update([
        //         'status'=>'accept'
        //     ]);

        // }
        $order_item_list = OrderItems::where('order_id', $req->order)
        ->where('branch_id', $req->supplier);
       $emial= $this->sendEmailCustomer($req->order,2);
       $dst = $order_item_list->first();
    //    $log = auth()->user()->name .'(Admin) change the order status';
    //    $this->orderActivity($req->order,'admin',$log,$req->supplier,$current_order->customer_id);
      
      
       
        return response()->json("Changes updated successfully");
    }
    protected function updateOrderStatus($order,$supplier)
    {
        $count_orderItems = DB::table('order_items')->where('order_id', $order)->count();
        $completed_count = DB::table('order_items')->where('order_id', $order)->whereIn('status', ['collected','at warehouse'])->count();
        if($count_orderItems == $completed_count )
        {
            
            Order::where('id',$order)
                    ->update([
                        'order_status'=> 5
                    ]);
            OrderItems::where('order_id',$order)
            ->where('branch_id',$supplier)
            ->update([
                'supplier_status'=>4
            ]);
        }else{
            Order::where('id',$order)
                    ->update([
                        'order_status'=> 4
                    ]);
            OrderItems::where('order_id',$order)
                    ->where('branch_id',$supplier)
                    ->update([
                        'supplier_status'=>3
                    ]);
        }
    }
    public function savecompleteSchedule(Request $req)
    {
         
         $order= Order::where('id', $req->order)->first();
        if ($req->date) {
            $parsed = Carbon::parse($req->date)->format('Y-m-d');
        }
        Order::where('id', $req->order)
            ->update([
                'driver_id' => $req->driver ?? $order->driver_id,
                'delivery_schedule_date' => $parsed ?? $order->delivery_schedule_date,
                'time_slot' => $req->slot ?? $order->time_slot,
                'order_status' => $req->status ?? $order->order_status
            ]);
        $my_order = Order::where('id', $req->order)->first();
        if($order->already_send == 0)
        {
            $this->notifyDriver($req->order,'schedule',$my_order->driver_id,$req->date,$req->slot,'');
            $this->sendEmailCustomer($req->order,3);
            $order= $order->update([
                'already_send'=>1
            ]);

        }
       
        return response()->json("Changes updated successfully");
    }

    public function orderReview($id)
    {
        $review = ProductRating::with(['product' => function ($q) {
            $q->with('lang');
        }])
            ->where('order_id', $id)
            ->get();
        $orderReview  = Review::where('order_id', $id)
            ->with('customer')
            ->with(['reviewSegments' => function ($query) {
                $query->with(['segment' => function ($query) {
                    $query->with('lang:name,language,rating_segment_id')
                        ->select('id');
                }])
                    ->select('review_id', 'segment_id', 'rating');
            }])
            ->select('id', 'rating_total', 'message', 'customer_id', 'order_id')
            ->first();
        $orderReview && $orderReview->reviewSegments->each(function ($review) {
            $this->keyBy($review->segment, 'lang', 'language');
        });
        $status = '';
        if($orderReview){
            $rv_val = $orderReview->rating_total;
            switch ($rv_val) {
                case $rv_val < 2:
                    $status = 'Poor';
                break;
                case $rv_val >= 2 && $rv_val < 3:
                    $status = 'Below average';
                break;
                case $rv_val >= 3 && $rv_val < 4:
                    $status = 'Average';
                break;
                case $rv_val >= 4 && $rv_val < 5:
                    $status = 'Good';
                break;
                case $rv_val >= 5:
                    $status = 'Excellent';
                break;
            }
        }
       

        return view('admin.order.details.review', compact('id', 'review', 'orderReview','status'));
    }

    public function cancelSingleOrder(Request $req)
    {
        $my_order = $this->order->where('id', $req->order);
        if ($req->type == 'accept') {
            $my_order->update([
                'order_status' => 6
            ]);

            $this->notifyDriver($req->order, 'cancel', $my_order->driver_id,'','','');
            $order_item = OrderItems::where('order_id', '=', $req->order)->get();

            foreach ($order_item as $item) {
                $product = Product::where('id', '=', $item->product_id)->first();
                $supplier = Supplier::where('id', $item->branch_id)->first();
                $region_id = $supplier->region_id;
                if ($product->product_type == 'complex') {
                    $product_stock = ProductVariant::where('variant_lang_id', '=', $item->variant_id)
                        ->with(['pdt_branch' => function ($query) use ($region_id) {
                            if ($region_id) {
                                $query->where('region_id', $region_id);
                            }
                        }])->where('product_id', '=', $item->product_id)
                        ->first();
                } else {
                    $product_stock = ProductVariant::where('variant_lang_id', '=', NULL)
                        ->with(['pdt_branch' => function ($query) use ($region_id) {
                            if ($region_id) {
                                $query->where('region_id', $region_id);
                            }
                        }])->where('product_id', '=', $item->product_id)
                        ->first();
                }
                $max_stock = $product_stock->pdt_branch->max_stock;
                $remaining_stock = $max_stock + $item->item_count;
                ProductBranchStock::where(['product_variant_stock_id' => $product_stock->id, 'region_id' => $region_id])
                    ->update([
                        'max_stock' => $remaining_stock,
                    ]);
            }

            return response()->json(['status' => 1, 'message' => "Order Cancelled successfully"]);
        } else {
            $my_order->update([
                'order_status' => 7
            ]);
            return response()->json(['status' => 1, 'message' => "Cancellation reject successfully"]);
        }
    }

    protected function notifyDriver($order,$status,$driver_id,$date,$slot,$supplier)
    {

        $order = Order::find($order);
        if($status == 'collection' || $status == 'cancel_collection')
        {
            $driver = User::find($driver_id);
            $supp=Supplier::where('id',$supplier)->first();
        }else{
        $driver = $order->driver;
        }
       
        $language = $driver->language ?? 'en';
        switch($status)
        {
            case 'schedule':
                 $title = trans("notification.order.title.schedule", [], $language);
        
                $body = [
                    'en' => $this->getContent('schedule', $order, 'en',$date,$slot,''),
                    'ar' => $this->getContent('schedule',$order, 'ar',$date,$slot,''),
                ];
            break;
            case 'cancel':
                $title = trans("notification.order.title.cancel", [], $language);
        
                $body = [
                    'en' => $this->getContent('cancel', $order, 'en','','',''),
                    'ar' => $this->getContent('cancel',$order, 'ar','','',''),
                ];
            break;
            case 'cancel_collection' :
                $title = trans("notification.order.title.cancel", [], $language);
        
                $body = [
                    'en' => $this->getContent('cancel_collection', $order, 'en','','',''),
                    'ar' => $this->getContent('cancel_collection',$order, 'ar','','',''),
                ];
                break;
            case 'collection':
                $title = trans("notification.order.title.collection", [], $language);
        
                $body = [
                    'en' => $this->getContent('collection', $order, 'en',$date,$slot,$supp),
                    'ar' => $this->getContent('collection',$order, 'ar',$date,$slot,$supp),
                ];
            break;
        } 
        
            $content = [
            'title' => $title,
            'body' => $body[$language],
            'type' => $status == 'collection' ? 'collection' : 'order',
            'order_id'=>$order->id,
            'order_uniq'=>$order->order_id,
            'time' => now()->format('Y-m-d H:i:s')
        ];
      
        $val = $this->saveNotification(
            $body,
            // 'salon',
            'admin',
            'driver',
            [
                'type' => $status == 'collection' ? 'collection' : 'order',
                'order_id'=>$order->id,
                
            ],
            $driver->id
        ); 
        if($driver->need_notification == 1 && $driver->fcm_token )
        {
            $this->sendNotification($driver->fcm_token, $title, $content,0);  
            $badge = User::where('id',$driver->id)->update([
                'driverapp_badge_seen'=>'true'
                ]);
        }

    }
    private function getContent($status,$order,$lang,$date,$slot,$supp)
    {
        switch($status)
        {
            case 'cancel':
                $reason = CancellationReasonLang::where('cancellation_id',$order->cancel_reason_id)
                ->where('language',$lang)
                ->first();
            return  trans(
                "notification.order.content.cancel",
                [
            
                    'order_id'=>$order->order_id,
                    'reason' => $reason->name
                ],
                $lang
            );
            break;
            case 'schedule':
                return  trans(
                    "notification.order.content.schedule",
                    [
                
                        'order_id'=>$order->order_id,
                        'date'=>$date ?? '',
                        'slot'=>$slot ?? '',
                    ],
                    $lang
                );

            break;
            case 'collection':
                return  trans(
                    "notification.order.content.collection",
                    [
                
                        'order_id'=>$order->order_id,
                        'date'=>$date ?? '',
                        'slot'=>$slot ?? '',
                        'supp'->$supp->code ?? ''
                    ],
                    $lang
                );
            break;
            case 'cancel_collection':
                return  trans(
                    "notification.order.content.cancel",
                    [
                
                        'order_id'=>$order->order_id
                    ],
                    $lang
                );
                break;
        }
        
    }

    public function productAcceptReject(Request $req)
    {
        $pro_status = (Config::get('constants.product_status'));
        $current_order = Order::find($req->order);
        OrderItems::where('order_id', $req->order)
            
            ->where('id',$req->id)
            ->update([
                'status' => $req->status
            ]);
            $log = 'Order '.$current_order->order_id.' - '.auth()->user()->name .' (Admin)'.$pro_status[$req->status] .' the item';
            $this->orderActivity($req->order,'admin',$log,'','',auth()->user()->id);
        $accepted_order_item_count = DB::table('order_items')->where('order_id', $req->order)->where('status', 2)->count();
        $order_item_count = $this->order->where('id', $req->order)->first();
        if ($accepted_order_item_count == $order_item_count->item_count) {
            $this->order->where('id', $req->order)
                ->update(['order_status' => 2]);
                
            $log = 'Order '.$current_order->order_id.' status <b> Waiting for supplier action  </b> changed to <b> Ready for collection </b> by ' . auth()->user()->name . '( admin)' .' on ' . Carbon::now()->format('d/m/Y g:i A');
            $this->orderActivity($req->order,'admin',$log,'',$current_order->customer_id, auth()->user()->id);
        } else {
            $pending = DB::table('order_items')->where('order_id', $req->order)->where('status', 1)->count();
            if ($pending) {
                $this->order->where('id', $req->order)
                    ->update(['order_status' => 1]);
            } else {
                $this->order->where('id', $req->order)
                    ->update(['order_status' => 2]);
                    $log = 'Order '.$current_order->order_id.' status <b> Waiting for supplier action  </b> changed to <b> Ready for collection </b> by ' . auth()->user()->name . '( admin)' .' on ' . Carbon::now()->format('d/m/Y g:i A');
                    $this->orderActivity($req->order_id,'admin',$log,'','', auth()->user()->id);
            }
        }
        $accepted_supplier_order_item_count = DB::table('order_items')
            ->where('order_id', $req->order)
            ->where('branch_id', $req->supplier)
            ->where('status', 'accept')->count();
        $order_item_count_supp = DB::table('order_items')
            ->where('order_id', $req->order)
            ->where('branch_id', $req->supplier)
            ->count();

        if ($accepted_supplier_order_item_count == $order_item_count_supp) {
            DB::table('order_items')
                ->where('order_id', $req->order)
                ->where('supplier_id', $req->supplier)
                ->update([
                    'supplier_status' => 2
                ]);
        }
        if($req->status == 3)
        {
            $cancelled_item = OrderItems::where('order_id', $req->order)
            ->where('id',$req->id)
            // ->where('supplier_id', $req->supplier)
            ->first();
            // dd($cancelled_item);
            $order_current = Order::where('id', $req->order)->first();
        $product_val =$cancelled_item->item_price *$cancelled_item->item_count ;
        
        $sub = $order_current->sub_total - $product_val;
        $grant = $order_current->grant_total -$product_val;
        $order_current->update([
            'sub_total' => $sub,
            'grant_total' => $grant
        ]);
        }
        
        $msg = 'Product '.$pro_status[$req->status]. ' successfully';
        return response()->json(['status' => 1, 'message' => $msg]);
    }

    public function acceptAllProducts(Request $req)
    {
        $order = $req->order;
        $supplier = $req->supplier;
        $rejected_product_count= DB::table('order_items')
            ->where('order_id', $order)
            ->where('branch_id', $supplier)
            ->where('status', 3)->count();
        // $this->driverSelection($order,$supplier,0);
        if($rejected_product_count)
        {
            $msg = 'Product already rejected';
            return response()->json(['status' => 2, 'message' => $msg]);

        }else{
            $accept = OrderItems::where('order_id', $order)
            ->where('branch_id', $supplier)
            ->update([
                'status'=>2
            ]);
            $accepted_order_item_count = DB::table('order_items')->where('order_id', $req->order)->where('status', 2)->count();
        $order_item_count = $this->order->where('id', $req->order)->first();
        if ($accepted_order_item_count == $order_item_count->item_count) {
            $this->order->where('id', $req->order)
                ->update(['order_status' => 2]);
        } else {
            $pending = DB::table('order_items')->where('order_id', $req->order)->where('status', 1)->count();
            if ($pending) {
                $this->order->where('id', $req->order)
                    ->update(['order_status' => 1]);
            } else {
                $this->order->where('id', $req->order)
                    ->update(['order_status' => 2]);
            }
        }
        DB::table('order_items')
                ->where('order_id', $req->order)
                ->where('supplier_id', $req->supplier)
                ->update([
                    'supplier_status' => 2
                ]);
            $my_order = Order::find($req->order);
            $ord_status = (Config::get('constants.order_status'));
            $log = 'Order '.$my_order->order_id.' status <b>'.$ord_status[1].'</b> changed to <b>'.$ord_status[2].'</b> by ' . auth()->user()->name . '( admin)' .' on ' . Carbon::now()->format('d/m/Y g:i A');
            $this->orderActivity($req->order,'admin',$log,'',$my_order->customer_id,'');
            return response()->json(['status' => 1, 'message' => "Accept all products"]);

        }
    }
    public function CollectionStatusUpdate(Request $req)
    {
        $order = $req->order_id;
        $item_id = $req->item_id;
        $branch_id = $req->branch_id;
        OrderItems::where('id',$item_id)
                    ->where('order_id',$order)
                    ->update([
                        'status'=>$req->collection_stat,
                        'shelf_id'=>$req->shelf
                    ]);
        $my_order = Order::find($req->order_id);
        $ord_status = (Config::get('constants.order_status'));
        $pro_status = (Config::get('constants.product_status'));
        $log = 'Order '.$my_order->order_id.' collection status  changed to <b>'.$pro_status[$req->collection_stat].'</b> by ' . auth()->user()->name . '( admin)' .' on ' . Carbon::now()->format('d/m/Y g:i A');
        $this->orderActivity($req->order_id,'admin',$log,'','',auth()->user()->id);
        $item_count = DB::table('order_items')
            ->where('order_id',$order)
            ->count();
        $collected_count = DB::table('order_items')
            ->where('order_id',$order)
            ->whereIn('status',[5,6,3])
            ->count();
        if($item_count == $collected_count )
        {
            Order::where('id',$order)
                    ->update([
                        'order_status'=>5
                    ]);
                    $log = 'Order '.$my_order->order_id.' order  status  changed to <b>'.$ord_status[5].'</b> by ' . auth()->user()->name . '( admin)' .' on ' . Carbon::now()->format('d/m/Y g:i A');
                    $this->orderActivity($req->order_id,'admin',$log,'','',auth()->user()->id);
        }else{
            Order::where('id',$order)
                    ->update([
                        'order_status'=> 4
                    ]);
        }
        $this->loginLog(auth()->user()->id);
        return response()->json(['status' => 2, 'message' => "Collection status updated successfully"]);
        // $driver=DB::table('order_items')
        // ->where('order_id',$order)
        // ->where('driver_id',null)
        // ->count();
        // $date=DB::table('order_items')
        // ->where('order_id',$order)
        // ->Where('collection_time_slot',null)
        // ->count();
        // $time=DB::table('order_items')
        // ->where('order_id',$order)
        // ->Where('collection_date',null)
        // ->count();
        // if($driver == 0 && $date == 0 && $time == 0  )
        // {
        //     Order::where('id',$order)
        //     ->update([
        //         'order_status'=>3
        //     ]);
        //     // $this->sendEmailCustomer($order,2);
        //     // return response()->json(['status' => 1, 'message' => "order  updated"]);
        // }else{
        //     return response()->json(['status' => 2, 'message' => "Schedule all values"]);
        // }


    }

   
    public function completeSingleOrder(Request $req)
    {
        $order = $req->order;
        
        Order::where('id',$order)
        ->update([
            'order_status'=>9
        ]);
        $order_products = OrderItems::where('order_id',$order)->get();
       
            foreach($order_products as $pro)
            {
                $most_sold_product = BestsellingProducts::select('product_count')->where('product_id', $pro->id)->first();
                if (isset( $most_sold_product ) && $most_sold_product->product_count > 0) {
                    $view_count = $most_sold_product->product_count + 1;
                    BestsellingProducts::where('product_id', $pro->product_id)
                        ->update([
                            "product_count" => $view_count
                    ]);
                } else {
                    BestsellingProducts::create([
                        'product_id' => $pro->product_id,
                        'product_count' => static::SINGLE,
                        'created_at' => Carbon::now(),
                        'updated_at'=>now()
                    ]);
                }
            }
            $emial= $this->sendEmailCustomer($req->order,4);
            $this->loginLog(auth()->user()->id);
            $current_order = Order::find($order);
            $log = 'Order '.$current_order->order_id.' status <b> Ready for delivery </b> changed to <b> Delivered </b> by ' . auth()->user()->name . '( admin)' .' on ' . Carbon::now()->format('d/m/Y g:i A');
            $this->orderActivity($order,'admin',$log,'',$current_order->customer_id,'');
            $this->loginLog(auth()->user()->id);
        return response()->json(['status' => 1, 'message' => "Order completed success successfully"]);
    }

    public function collectionStatusform(Request $req)
    {
        $item_id = $req->item_id;
        $branch_id = $req->branch_id;
        $order_id = $req->order_id ;
        $item_details = OrderItems::where('id',$item_id)->first();
        $shelfs = (Config::get('constants.warehouse_shelfs'));
        return view('admin.order.details.collection-status_form', compact('order_id','branch_id','item_id','item_details','shelfs'));
    }

    public function orderCancellationForm(Request $req)
    {
        $order_id = $req->id;
        $cancellationReason = CancellationReason::with(['lang' => function ($query){
            $query->where('language', 'en');
        }])
            ->get();
            return view('admin.order.details.cancel_form',compact('cancellationReason','order_id'));
    }

    public function cancelOrder(Request $req)
    {
        $current_order = Order::where('id',$req->order_id)->first(); 
        $order_items = OrderItems::where('order_id',$req->order_id)->groupBy('branch_id')->get();
        Order::where('id',$req->order_id)
        ->update([
            'cancel_reason_id'=>$req->reason,
            'cancel_note'=>$req->comment,
            'order_status'=>6,
            'updated_by'=>'admin',
            'updated_by_id'=>auth()->user()->id
        ]);
        if($current_order->driver_id && $current_order->order_status == 7)
        {
            $this->notifyDriver($req->order_id, 'cancel', 0,'','','');
        }
        else{
            foreach($order_items as $driver)
            {
                $this->notifyDriver($req->order_id, 'cancel_collection', $driver->driver_id,'','','');
            }
        }
        $ord_status = (Config::get('constants.order_status'));
        $log ='Order '.$current_order->order_id.' status <b>'.$ord_status[$current_order->order_status].'</b> changed to <b>'.$ord_status[6].'</b> by ' . auth()->user()->name . '( admin)' .' on ' . Carbon::now()->format('d/m/Y g:i A');
        $this->orderActivity($req->order_id,'admin',$log,'',$current_order->customer_id,'');
        $this->loginLog(auth()->user()->id);
        return response()->json(['status' => 1, 'message' => "Order Cancelled successfully"]);
    }
    protected function sendEmailCustomer($id,$stat)
    {
        $ord_status = (Config::get('constants.web_order_status'));
        $order = Order::find($id);
        $status = $ord_status[$stat];
        $customer =Customer::find($order->customer_id);
        if($stat == 2)
        {
            $subject = 'Your order is under preparation';
        }else{
            $subject = 'Your order is '.$status ;
        }
        
        SendEmail::generateOrderEmail($customer->email, $customer->cust_name, $subject, $order,$stat);
    }

    private function driverSelection($order_id,$supplier_id,$row)
    {
        $supplier = Supplier::where('id',$supplier_id)->first();
        $region = Region::select('driver','id')->where('id',$supplier->region_id)->first();
       
        if($row != 0)
        {

        }else{
           OrderItems::where('order_id',$order_id)
                        ->where('branch_id',$supplier_id)
                        ->update([
                            'driver_id'=>$region ? $region->driver : ''
                        ]);
            
            
        }
    }
    private function sendNotificatioTosuppliers($supplier_id,$order_id)
    {
        $supplier = Supplier::where('id',$supplier_id)->first();
        $order_details = Order::where('id',$order_id)->first();
        $notifications = new AppNotifications;
        $notifications->activity_id = $order_id;
        $notifications->is_from = 'order';
        $notifications->from_type = 'admin';
        $notifications->to_type = 'supplier';
        $notifications->to_id = $supplier_id;
        
        $content = json_encode([
                'en' => 'The operation team schedule collection of the order '. $order_details->order_id.' on'. Carbon::now()->format('d/m/Y g:i A'),
                'ar' =>'The operation team schedule collection of the order '.  $order_details->order_id .' on'. Carbon::now()->format('d/m/Y g:i A')
            ]);
        $detail=json_encode([
            'type'=>'order',
            'order_id'=>$order_id
        ]);
        $notifications->content = $content;
        $notifications->details = $detail;
        $notifications->save();
    }
}
