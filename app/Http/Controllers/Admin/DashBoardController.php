<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\OrderItems;
use App\Models\Customer;
use App\Models\OtherSettings;
use App\Models\Supplier;
use Illuminate\Support\Facades\DB;
use App\Models\BestsellingProducts;

class DashBoardController extends Controller {

    public function get() {
        $total_sale = DB::table('order')
                ->whereIn('order.order_status', [9])
                ->sum('order.grant_total');

        $todays_collection = Order::withCount(['items' => function ($query) {
                                $query->distinct('branch_id');
                            }])->whereIn('order_status', [4, 5])
                        ->with('customer')
                        ->whereDate('created_at', Carbon::today()->toDateString())->orderBy('id', 'desc')->paginate(10);
        $todays_delivery = Order::whereIn('order_status', [9])
                        ->with('driver')
                        ->with('customer')
                        ->whereDate('created_at', Carbon::today()->toDateString())->orderBy('id', 'desc')->paginate(10);
        
        $best_selling_pdts = BestsellingProducts::with('product')->orderBy('product_count', 'desc')->groupBy('product_id')->paginate(10);
        
        $total_customers = Customer::count();
        $total_suppliers = Supplier::where('role_id', 5)->count();
        $total_cancelled_order = Order::where('order_status', 6)->count();
        $total_order_for_collection = Order::whereIn('order_status', [3,4])->count();
        $total_items_in_ware_house = Order::whereIn('order_status', [5])->count();
        $total_orders_delivery = Order::whereIn('order_status', [7])->count();
        $total_orders_delivered = Order::whereIn('order_status', [9])->count();

        return view('admin.dashboard.index', compact('total_sale', 'todays_collection', 'todays_delivery', 'best_selling_pdts', 'total_customers',
                'total_suppliers', 'total_cancelled_order', 'total_order_for_collection', 'total_items_in_ware_house', 'total_orders_delivery', 
                'total_orders_delivered', 'total_orders_delivered'));
    }
    
    public function getCount(Request $req) {

                $start = Carbon::parse($req->start_date)->format('Y-m-d');
                $end = Carbon::parse($req->end_date)->format('Y-m-d');
                $total_sale =  Order::whereIn('order.order_status', [9])->whereBetween('created_at', [$start, $end])
                ->sum('order.grant_total');
                $total_cancelled_order = Order::where('order_status', 6)->whereBetween('created_at', [$start, $end])->count();
                $total_customers = Customer::where('status', 'Y')->whereBetween('created_at', [$start, $end])->count();
                $total_suppliers = Supplier::where('status', 'active')->whereBetween('created_at', [$start, $end])->count();
                $total_collection = Order::whereIn('order_status', [3,4])->whereBetween('created_at', [$start, $end])->count();
                $total_ware_house = Order::whereIn('order_status', [5])->whereBetween('created_at', [$start, $end])->count();
                $total_delivery = Order::whereIn('order_status', [7])->whereBetween('created_at', [$start, $end])->count();
                $total_delivered = Order::whereIn('order_status', [9])->whereBetween('created_at', [$start, $end])->count();
                $sales_count = 'SAR '.number_format($total_sale, 2, ".", "");
                return [
                    'total_cancelled' => $total_cancelled_order,
                    'sales_count' => $sales_count,
                    'total_customers' => $total_customers,
                    'total_suppliers' => $total_suppliers,
                    'total_collection' => $total_collection,
                    'total_ware_house' => $total_ware_house,
                    'total_delivery' => $total_delivery,
                    'total_delivered' => $total_delivered,
                ];
            }

}
