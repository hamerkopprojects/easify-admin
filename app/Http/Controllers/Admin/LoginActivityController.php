<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Models\UserRoles;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginActivityController extends Controller
{
    public function index(Request $req)
    {
        $role = UserRoles::get();
        $search = $req->search;
        $usr_id = $req->user ;
        $query = User::query()->with('roles');
        if ($search) {
            $from = date("Y-m-d 00:00:00", strtotime($search));
            $to = date("Y-m-d 23:59:59", strtotime($search));

            $query->whereBetween('last_login_date', [$from, $to]);
        }
        if($usr_id)
        {
            $query->where('id', $usr_id);

        }
        $user = $query->paginate(10);
        $users_list = User::get();
        $data = [
            'user' => $user,
            'search' => $search,
            'user_list'=>$users_list,
            'usr_id'=>$usr_id
        ];
       
        return view('admin.log.activity_log', $data);
    }
}
