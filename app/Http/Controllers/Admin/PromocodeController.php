<?php

namespace App\Http\Controllers\Admin;

use App\Models\Promocode;
use App\Models\ProductLang;
use App\Models\CategoryLang;
use Illuminate\Http\Request;
use App\Models\PromocodeProduct;
use App\Models\PromocodeCategory;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Validator;

class PromocodeController extends Controller
{

    public function index(Request $req)
    {
        $promo = Promocode::with('promocategory')->with('promocategory')->get();
        $search = $req->search ?? '';
        $query = Promocode::query();
        if (!empty($search)) {

            $query->where('code', 'like', "%" . $search . "%");
        }

        $promo = $query->paginate(20);

        return view('admin.product.promocodes.index', compact('promo', 'search'));
    }
    public function statusUpdate(Request $req)
    {
        $att = Promocode::where('id', $req->id)->first();
        if ($att) {
            if ($att->status == 'deactive') {
                Promocode::where('id', $req->id)
                    ->update([
                        'status' => 'active'
                    ]);
            } else {
                Promocode::where('id', $req->id)
                    ->update([
                        'status' => 'deactive'
                    ]);
            }
            return response()->json(['status' => 1, 'message' => 'Status updated successfully']);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }

    public function store(Request $req)
    {

        $rules = [
            'code' => 'unique:promocodes,code,NULL,id,deleted_at,NULL',
        ];
        $messages = [
            'code.unique' => 'Promocode already exist.',
        ];

        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {

            $data = DB::transaction(function () use ($req) {
                
                if($req->rule_based == 'category'){
                    $type = $req->category_type;
                }else{
                    $type = $req->product_type;
                }
                $data = Promocode::create([
                    'code' => preg_replace("/\s+/", "", $req->code),
                    'discount_type' => $req->discount_type,
                    'value' => $req->value,
                    'max_num_usage' => $req->max_num_usage,
                    'max_num_per_user' => $req->max_num_per_user,
                    'rule_based' => $req->rule_based,
                    'type' => $type,
                    'status' => 'active'
                ]);

                $category_ci_array = $req->category_ci_array;

                if (!empty($category_ci_array)) {
                    foreach ($category_ci_array as $category_ci) {

                        PromocodeCategory::create([
                            'promocodes_id' => $data->id,
                            'category_id' => $category_ci,
                            'type' => $req->category_type,
                        ]);
                    }
                }
                $category_ce_array = $req->category_ce_array;

                if (!empty($category_ce_array)) {
                    foreach ($category_ce_array as $category_ce) {

                        PromocodeCategory::create([
                            'promocodes_id' => $data->id,
                            'category_id' => $category_ce,
                            'type' => $req->category_type,
                        ]);
                    }
                }
                $product_pi_array = $req->product_pi_array;
                if (!empty($product_pi_array)) {
                    foreach ($product_pi_array as $product_pi) {

                        PromocodeProduct::create([
                            'promocodes_id' => $data->id,
                            'product_id' => $product_pi,
                            'type' => $req->product_type,
                        ]);
                    }
                }
                $product_pe_array = $req->product_pe_array;
                if (!empty($product_pe_array)) {
                    foreach ($product_pe_array as $product_pe) {

                        PromocodeProduct::create([
                            'promocodes_id' => $data->id,
                            'product_id' => $product_pe,
                            'type' => $req->product_type,
                        ]);
                    }
                }
                return $data;
            });
            $msg = "Promo code added successfully";
            if ($data) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }

    public function edit($id)
    {


        $promocode = Promocode::with(['promocategory' => function ($query) use ($id) {
            $query->with(['promotionalcategory' => function ($query) use ($id) { }]);
        }])->with(['promoproduct' => function ($query) use ($id) {
            $query->with(['promotionalproduct' => function ($query) use ($id) { }]);
        }])->where('id', $id)->first();
        return [
            'promocode' => $promocode
        ];
    }

    public function update(Request $req)
    {

       
        $data = DB::transaction(function () use ($req) {
        if($req->rule_based == 'category'){
            $type = $req->category_type;
        }else{
            $type = $req->product_type;
        }
        $data = Promocode::where('id', $req->promocode_unique)->update([

            'code' => preg_replace("/\s+/", "", $req->code),
            'discount_type' => $req->discount_type,
            'value' => $req->value,
            'max_num_usage' => $req->max_num_usage,
            'max_num_per_user' => $req->max_num_per_user,
            'rule_based' => $req->rule_based,
             'type' => $type,

        ]);
        
        if($req->rule_based == 'category'){
            if (PromocodeCategory::where('promocodes_id', $req->promocode_unique)->exists()) {
                PromocodeCategory::where('promocodes_id', $req->promocode_unique)->delete();
            }
            if($req->category_type == 'include'){
            $category_ci_array = $req->category_ci_array;
            if (!empty($category_ci_array)) {
            foreach ($category_ci_array as $category_ci) {
            PromocodeCategory::create([
                    'promocodes_id' => $req->promocode_unique,
                    'category_id' => $category_ci,
                    'type' => $req->category_type,
            ]);
            }
            }
            }else{
            $category_ce_array = $req->category_ce_array;
            if (!empty($category_ce_array)) {
            foreach ($category_ce_array as $category_ce) {

                PromocodeCategory::create([
                    'promocodes_id' => $req->promocode_unique,
                    'category_id' => $category_ce,
                    'type' => $req->category_type,
                ]);
            }
        }
        }
            }else{
            if (PromocodeProduct::where('promocodes_id', $req->promocode_unique)->exists()) {
                PromocodeProduct::where('promocodes_id', $req->promocode_unique)->delete();
            }   
            if($req->product_type == 'include'){
            $product_pi_array = $req->product_pi_array;
            if (!empty($product_pi_array)) {
            foreach ($product_pi_array as $product_pi) {

                PromocodeProduct::create([
                    'promocodes_id' => $req->promocode_unique,
                    'product_id' => $product_pi,
                    'type' => $req->product_type,
                ]);
            }
        } 
             }else{
        $product_pe_array = $req->product_pe_array;
        if (!empty($product_pe_array)) {
            foreach ($product_pe_array as $product_pe) {

                PromocodeProduct::create([
                    'promocodes_id' => $req->promocode_unique,
                    'product_id' => $product_pe,
                    'type' => $req->product_type,
                ]);
            }
        }
             }
             }


        return $data;
        });
        $msg = "Promo code updated successfully";
        if ($data) {
        return response()->json(['status' => 1, 'message' => $msg]);
        } else {
        return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
        
    }
    public function destroy(Request $req)
    {
        if ($req->id) {
            $prmocd = Promocode::find($req->id);
            if (!empty($prmocd)) {
                $prmocd->promocategory()->delete();
                $prmocd->promoproduct()->delete();
                $prmocd->delete();

                return response()->json(['status' => 1, 'message' => 'Promocode deleted successfully']);
            }
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }

    public function autocompleteCategory(Request $request)
    {

        $data = CategoryLang::select("name", "category_id")->where("name", "LIKE", "%{$request->input('query')}%")->where("language", "en")->get();

        return response()->json($data);
    }
}
