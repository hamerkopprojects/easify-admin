<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Carbon\Carbon;
use App\Models\Customer;
use App\Models\OrderLog;
use App\Models\Supplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderActivityLogController extends Controller
{
    public function index(Request $req)
    {
        $date = $req->date;
        $cust_id = $req->customer;
        $supp_id = $req->supplier;
        $usr_id= $req->user_id;
        $query = OrderLog::where('deleted_at',null);
            
        if ($req->date) {
            $date = Carbon::parse($req->date)->format('Y-m-d');
            $query->whereDate('created_at', $date);
        }
        if ($req->user_id) {
            $query->where('user_id', $usr_id);
        }
        if ($req->customer) {
            $query->where('customer_id', $cust_id);
        }
        if ($req->supplier) {
            $query->where('supplier_id', $supp_id);
        }

        $log = $query->orderBy('created_at','desc')->paginate(20);
        $customers = Customer::get();
        $suppliers = Supplier::where('status','active')->get();
        $users = User::get();
        return view('admin.log.order_activity', compact('log','customers','suppliers','users','supp_id','date','cust_id','usr_id'));
    }
}
