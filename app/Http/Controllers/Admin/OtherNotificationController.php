<?php

namespace App\Http\Controllers\Admin;

use App\Models\Customer;
use Carbon\Carbon;
use App\Models\AppNotifications;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OtherNotificationController extends Controller {
    const pagination = 20;
    public function get(Request $req) {
        $this->clearCount(static::pagination);
        $date = $req->date;
        $query = AppNotifications::query();
        $customer_id = $req->customer_id;
        if ($req->date) {
            $date = Carbon::parse($req->date)->format('Y-m-d');
            $query->whereDate('created_at', $date);
        }
        if ($req->customer_id) {
            $query->where('to_id', $req->customer_id);
        }
        $notifications = $query->whereIn('from_type' , ['website', 'supplier', 'branch'])->where('to_type', 'admin')->orderBy('id', 'desc')->paginate(static::pagination);
        $customer = Customer::where('deleted_at', null)->get();
        return view('admin.notification.other', compact('notifications', 'date', 'customer', 'customer_id'));
    }
    
    protected function clearCount($count)
    {
        AppNotifications::where('admin_read', 0)->whereIn('from_type' , ['website', 'supplier', 'branch'])->where('to_type', 'admin')->take($count)
            ->update(['admin_read' => 1]);
    }
    

}
