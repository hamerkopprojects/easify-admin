<?php

namespace App\Http\Controllers\Supplier;

use App\Models\Brand;
use Illuminate\Http\Request;
use App\Models\SupplierBrand;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class PreferredBrandController extends Controller
{
    public function get()
    {
        $brands=Brand::with('lang')
        ->where('deleted_at',null)
        ->get();
        $selected_brand=SupplierBrand::where('supplier_id',auth()->user()->id)
                        ->get();
        
        return view('supplier.product.brand.index',compact('brands','selected_brand'));
    }

    public function brandStore(Request $req)
    {
        // dd($req->toArray());
        $supplier = auth()->user();
        $supplier->brand()->detach();
        $supplier->brand()->attach(
            $req->brands ?? [],
            ['created_at' => now(), 'updated_at' => now()]
        );
        
        Session::flash('success', __('messages.admin.brand_update_msg'));
           
        // return view('supplier.product.brand.index');  
        return redirect()->route('supplier.brand_list');

    }
}
