<?php

namespace App\Http\Controllers\Supplier;

use App\Models\City;
use App\Models\Brand;
use App\Models\Region;
use App\Models\Category;
use App\Models\Supplier;
use App\Events\SendToken;
use App\Models\UserRoles;
use Illuminate\Http\Request;
use App\Models\SupplierAnnex;
use App\Models\SupplierBrand;
use Illuminate\Support\Carbon;
use App\Models\SupplierCategory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class RegistrationController extends Controller {

    public function register() {

        $region = Region::with('lang')->get();
        $city = City::with('lang')->get();
        $data = [
            'region' => $region,
            'city' => $city
        ];
        return view('supplier.auth.register', $data);
    }

    public function store(Request $req) {

        $this->validate(
                $req, [
            'supplier_name' => 'required',
            'contact_name' => 'required',
            'email' => 'required|unique:supplier,email',
            'phone' => 'required|unique:supplier,phone',
            'city' => 'required',
            'region' => 'required'
                ], [
            'email.required' => 'Email is required.',
            'email.unique' => 'Supplier with same email already exists',
            'phone.required' => 'Phone number is required.',
            'phone.unique' => 'Supplier with same Phone number already exists',
                ]
        );
        if (!empty($req->file('product_image_en'))) {
            $current = Carbon::now()->format('YmdHs');
            $file = $req->file('product_image_en');
            $name_en = $current . $file->getClientOriginalName();
            $path_en = $file->move(public_path() . '/uploads/supplier/cr_copy/', $name_en);
            $path = 'supplier/cr_copy/' . $name_en;
        } else {
            $path = NULL;
        }
        // $var=mt_rand(100000,999999);
        $var = "1234";
        $roles = UserRoles::where('name', "Supplier")->first();

        $supplierData = Supplier::
                create([
                    'name' => $req->supplier_name,
                    'contact_person_name' => $req->contact_name,
                    'email' => $req->email,
                    'phone' => $req->phone,
                    'city_id' => $req->city,
                    'region_id' => $req->region,
                    'role_id' => $roles->id,
                    'verification_code' => $var,
                    'code_generated_at' => Carbon::now(),
                    'password' => Hash::make($var),
                    'code' => $this->generateSupplierId(),
        ]);
        if (!empty($req->file('product_image_en'))) {
            $supplierData = SupplierAnnex::
                    create([
                        'supplier_id' => $supplierData['id'],
                        'cr_copy' => $path,
            ]);
        }
        $email = $req->email;
        // event(new SendToken($req->email, $var));

        return view('supplier.auth.verify_account', compact('email'));
    }

    public function verify(Request $request) {
        $email = $request->email;

        $user = Supplier::where('email', $email)->where('verification_code', $request->code)->first();
        $id = $user->id;
        if ($user) {
            $user = Supplier::where('email', $email)->update(
                    [
                        'status' => "active",
            ]);

            $main_cat = Category::with('lang')
                    ->where('parent_id', NULL)
                    ->where('status', "active")
                    ->get();


            $category = Category::with('lang')->get();

            Session::flash('success', 'Token Verified Successfully.');
            return view('supplier.auth.pref_category', compact('category', 'main_cat', 'id'));
        } else {
            Session::flash('error', 'Enter valid token');
            return view('supplier.auth.verify_account', compact('email'));
        }
    }

    public function storeCategory(Request $request) {
        $id = $request->supplier_id;
        $brand = Brand::with('lang')
                ->where('status', "active")
                ->get();
        if ($request->category) {

            $categories = $request->category;
            foreach ($categories as $category) {
                SupplierCategory::create([
                    'supplier_id' => $id,
                    'category_id' => $category,
                ]);
            }

            Session::flash('success', 'Category Inserted Successfully.');

            return view('supplier.auth.pref_brand', compact('brand', 'id'));
        } else {
            return view('supplier.auth.pref_brand', compact('brand', 'id'));
        }
    }

    public function prefBrand(Request $request) {

        if (!empty($request['supplier_id'])) {
            $id = $request['supplier_id'];
        }

        $brand = Brand::with('lang')
                ->where('status', "active")
                ->get();


        return view('supplier.auth.pref_brand', compact('brand', 'id'));
    }

    public function storeBrand(Request $request) {
        if ($request->brand) {
            $id = $request->supplier_id;
            $brands = $request->brand;

            foreach ($brands as $brand) {
                SupplierBrand::create([
                    'supplier_id' => $id,
                    'brand_id' => $brand,
                ]);
            }
            Session::flash('success', 'Brand Inserted Successfully.');

            return redirect()->route('supplier.login');
        } else {
            return redirect()->route('supplier.login');
        }
    }

    public function generateSupplierId() {

        $roles = UserRoles::where('name', "Supplier")->first();

        $lastsupplier = Supplier::select('code')
                ->where('role_id', $roles->id)
                ->orderBy('id', 'desc')
                ->first();

        $lastId = 0;

        if ($lastsupplier) {
            $lastId = (int) substr($lastsupplier->code, 9);
        }

        $lastId++;

        return 'EAS-SUPP-' . str_pad($lastId, 5, '0', STR_PAD_LEFT);
    }

    public function getRegion(Request $req) {
        $region = Region::with('lang')->where('city_id', $req->city_id)->get();
        return response()->json($region);
    }

}
