<?php

namespace App\Http\Controllers\Supplier;

use App\Models\Product;
use App\Models\Supplier;
use App\Models\Attribute;
use App\Models\BranchAnnex;
use App\Models\ProductPrice;
use Illuminate\Http\Request;
use App\Models\ProductImages;
use App\Models\ProductVariant;
use App\Models\ProductAttribute;
use App\Models\CategoryAttributes;
use App\Http\Controllers\Controller;

class ProductViewController extends Controller
{
    public function details(Request $req)
    {
        $product_id = $req->id;

        $product = Product::with('lang')
                    ->with('category')
                    ->with('brand')
                    ->where('id', $product_id)
                    ->get();
        return view('supplier.product.products.view.product_view', compact('product'));
    }
    public function getViewTabs(Request $req)
    {
        $product_id = $req->id;
        $product_type = Product::where('id', $product_id)->pluck('product_type');
        if ($req->activeTab == '#pdt_info') {
            $product_id = $req->id;
            $product = Product::with('lang')->with('category')->with('brand')->where('id', $product_id)->get();
            return view('supplier.product.products.view.basic_details', compact('product'))->render();
        }elseif ($req->activeTab == '#attributes') {
            $pdt_id = $req->id;
            $pro = Product::with('lang')->where('id', $pdt_id)
            ->get();
            
            $pdt_att = ProductAttribute::with('lang')->with('attribute')->with('product')
                ->whereIn('attribute_type', ['textbox', 'textarea'])
                ->where('product_id', $pdt_id)
                ->get();

            $pdt_var = ProductAttribute::with('lang')->with('attribute')->with('variant')
                ->where('attribute_type', 'dropdown')
                ->where('product_id', $pdt_id)
                ->get();

            $pdt_att_var = ProductVariant::with('attribute')->with('variant')
                ->where('product_id', $pdt_id)
                ->get();
            return view('supplier.product.products.view.product_attribute', compact('pdt_att','pdt_id', 'pdt_var', 'pdt_att_var', 'product_type', 'pro'))->render();
        }  elseif ($req->activeTab == '#price') {
            $pdt_id = $req->id;
            $pdt_data = Product::where(['id' => $req->id])->first();
            $product_type = $pdt_data->product_type ? $pdt_data->product_type : 'simple';
            $pdt_att_var = array();
            if ($product_type == 'complex') {

                $pdt_att_var = Attribute::with('lang')->leftJoin('category_attributes', 'category_attributes.attribute_id', '=', 'attribute.id')
                        ->select('attribute.*')
                        ->where('category_attributes.category_id', '=', $pdt_data->category_id)
                        ->where('attribute.is_variant', 'yes')
                        ->where('attribute.attribute_type', '=', 'dropdown')
                        ->groupBy('category_attributes.attribute_id')
                        ->first();
            }
            $product_price = ProductPrice::where('product_id', $req->id)->first();


            return view('supplier.product.products.view.product_price')->with(compact('product_price','pdt_data','product_type','pdt_att_var','pdt_id'))->render();
        } elseif ($req->activeTab == '#stock') {
            // dd($req->toArray());
            $pdt_var_count = ProductVariant::with('attribute')->with('variant')
                        ->where('product_id', $req->id)
                        ->get();
                        $pdt_id = $req->id;
                        $pdt_att_var = $pdt_att_row_data = array();
                        $pdt_data = Product::where(['id' => $req->id])->first();
                        $product_type = $pdt_data->product_type ? $pdt_data->product_type : 'simple';
                        $pdt_att_var = array();
                        if ($product_type == 'complex') {
                            $pdt_att_var = Attribute::with('lang')->leftJoin('category_attributes', 'category_attributes.attribute_id', '=', 'attribute.id')
                                    ->select('attribute.*')
                                    ->where('category_attributes.category_id', '=', $pdt_data->category_id)
                                    ->where('attribute.is_variant', 'yes')
                                    ->where('attribute.attribute_type', '=', 'dropdown')
                                    ->groupBy('category_attributes.attribute_id')
                                    ->first();
                        }
            
                        $supplier_data = Supplier::where('id', $pdt_data->supplier_id)->first();
                        $branch_data = BranchAnnex::where('supplier_parent_id', $pdt_data->supplier_id)->get();
                        return view('supplier.product.products.view.product_stock')->with(compact('pdt_id', 'pdt_att_var', 'product_type', 'supplier_data', 'branch_data'))->render();
        } else{
            
            $prdt = ProductImages::where('product_id', $product_id)->get();
           
            $cover = Product::where('id', $product_id)->get();
            return view('supplier.product.products.view.images_list')->with(compact('prdt', 'cover'))->render();
        }
    
    }
}
