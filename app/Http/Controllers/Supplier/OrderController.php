<?php

namespace App\Http\Controllers\Supplier;

use Carbon\Carbon;
use App\Models\Order;
use App\Models\Supplier;
use App\Models\OrderItems;
use Illuminate\Http\Request;
use App\Models\OtherSettings;
use App\Models\AppNotifications;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Traits\OrderActivityLog;
use Illuminate\Support\Facades\Config;

class OrderController extends Controller
{
    protected $order;

    use OrderActivityLog;
    
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function activeOrders(Request $req)
    {
        $stat  = $req->search_select;
        $search = $req->search;
        $date = $req->date;
        $supp_status = (Config::get('constants.supplier_status'));
        $query = $this->order
            ->leftJoin('order_items as item', 'order.id', '=', 'item.order_id')
            ->leftJoin('users as user', 'item.driver_id', '=', 'user.id')
            ->whereIn('item.supplier_status', [1, 2, 3, 4])
            ->whereNotIn('order.order_status', [6, 8, 9, 7])
            ->where('item.branch_id', auth()->user()->id)
            ->select(
                'order.id',
                'order.order_id as order_id',
                'order.created_at as order_date',
                'order.grant_total as amount',
                'order.order_status',
                'item.status as item_status',
                'item.supplier_status as supplier_status',
                'user.name as driver',
                'item.item_price',
                DB::raw('SUM(item.item_price *item.item_count ) AS item_sum'
                )
            )
            ->groupBy('order.id');
            
        if ($req->search_select) {
            $query->where('item.supplier_status', $req->search_select);
        }
        if ($req->search) {
            $query->where('order.order_id', 'like', "%" . $req->search . "%");
        }
        if ($req->date) {
            $date_new = Carbon::parse($req->date)->format('Y-m-d');
            $query->whereDate('order.created_at', $date_new);
        }
        $active = $query->paginate(20);
        return view('supplier.order.active', compact('search','active', 'stat', 'search', 'supp_status','date'));
    }
    public function completedOrders(Request $req)
    {
        $search = $req->search;
        $date = $req->date;
        $query = $this->order
            ->leftJoin('order_items as item', 'order.id', '=', 'item.order_id')
            ->where('order.order_status', 9)
            ->where('item.branch_id', auth()->user()->id)
            ->select('order.id','order.order_id as order_id', 
            'order.created_at as order_date', 'order.grant_total as amount',
             'order.order_status', 'item.status as item_status','item.item_price',
             'item.collection_date','item.collection_time_slot', DB::raw('sum(item.grant_total) as value'))
            ->groupBy('order.id');
        if ($req->search) {
            $query->where('order.order_id', $req->search);
        }
        if ($req->date) {
            $date_new = Carbon::parse($req->date)->format('Y-m-d');
            $query->whereDate('item.collection_date', $date_new);
        }
        $detail=$total_sale =OrderItems::leftJoin('order', 'order.id', '=', 'order_items.order_id')
        ->where('order.order_status', 9)
        ->where('order_items.branch_id', auth()->user()->id)
        ->sum('order_items.grant_total'); 
                    
                
        $active = $query->paginate(20);
        return view('supplier.order.complete', compact('active', 'search', 'date','detail'
    ));
    }
    public function cancelledOrders(Request $req)
    {
        $search = $req->search;
        $date = $req->date;
        $query = $this->order
            ->leftJoin('order_items as item', 'order.id', '=', 'item.order_id')
            ->where('order.order_status', 7)
            ->where('item.branch_id', auth()->user()->id)
            ->select('order.id', 'order.order_id as order_id', 'order.created_at as order_date', 'order.grant_total as amount', 'order.order_status', 'item.status as item_status')
            ->groupBy('order.id');
        if ($req->search) {
            $query->where('order.order_id', $req->search);
        }
        if ($req->date) {
            $date_new = Carbon::parse($req->date)->format('Y-m-d');
            $query->whereDate('order.delivery_schedule_date', $date_new);
        }
        $active = $query->paginate(20);
        return view('supplier.order.cancelled', compact('active', 'search', 'date'));
    }

    public function orderDetails($id)
    {
        $lang = "en";
        $pro_status = (Config::get('constants.product_status'));
        $details = $this->order
            ->with(['items' => function ($query) use ($lang) {
                $query->with(['product' => function ($query) use ($lang) { }])
                    ->with('driver')
                    ->where('branch_id', auth()->user()->id);
            }])
            ->with('cancellation')
            ->with('owner')
            ->where('id', $id)
            ->first();
        $supplier = Supplier::where('id', auth()->user()->id)
            ->with(['supplier_region' => function ($q) {
                $q->with('lang');
            }])
            ->first();
        $other = OtherSettings::first();
        $regi= $supplier->supplier_region->coordinates ;
        // dd($supplier->supplier_region->coordinates);
        $km = 0 ;
        if (!empty($regi) && $regi != "null") {
            $supp_loc = json_decode($supplier->supplier_region->coordinates);

            $wherehouse = json_decode($other->warehouse_loc);
            //    dd($supp_loc[0]);
            $theta = $wherehouse->longitude - $supp_loc[0]->lng;
            $distance = sin(deg2rad($wherehouse->latitude)) * sin(deg2rad($supp_loc[0]->lat)) +  cos(deg2rad($wherehouse->latitude)) * cos(deg2rad($supp_loc[0]->lat)) * cos(deg2rad($theta));
            $distance = acos($distance);
            $distance = rad2deg($distance);
            $miles = $distance * 60 * 1.1515;
            $km = $miles * 1.609344;
        }
        return view('supplier.order.details.active', compact('details', 'supplier', 'km','pro_status'));
    }
    public function acceptProduct(Request $req)
    {
        OrderItems::where('order_id', $req->order)
            ->where('product_id', $req->product)
            ->where('branch_id', $req->supplier)
            ->where('id',$req->variant)
            ->update([
                'status' => 2
            ]);
        $accepted_order_item_count = DB::table('order_items')->where('order_id', $req->order)->whereIn('status',[2,3])->count();
        $order_item_count = $this->order->where('id', $req->order)->first();
        if ($accepted_order_item_count == $order_item_count->item_count) {

            $this->order->where('id', $req->order)
                ->update(['order_status' => 3]);
                $this->notification($req->order);
        } else {
            $pending = DB::table('order_items')->where('order_id', $req->order)->where('status', 1)->count();
            if ($pending) {
                $this->order->where('id', $req->order)
                    ->update(['order_status' => 1]);
            } else {
                $this->order->where('id', $req->order)
                    ->update(['order_status' => 2]);
            }
        }
        $accepted_supplier_order_item_count = DB::table('order_items')
            ->where('order_id', $req->order)
            ->where('branch_id', $req->supplier)
            ->where('status', 1)->count();
        $order_item_count_supp = DB::table('order_items')
            ->where('order_id', $req->order)
            ->where('branch_id', $req->supplier)
            ->count();

        if ($accepted_supplier_order_item_count == $order_item_count_supp) {
            
            DB::table('order_items')
                ->where('order_id', $req->order)
                ->where('supplier_id', $req->supplier)
                ->update([
                    'supplier_status' => 2
                ]);
            
        $order_details = Order::find($req->order);
        $log ='Supplier <b>'. auth()->user()->name . '</b> accepted there items in the order '.$order_details->order_id.' on ' . Carbon::now()->format('d/m/Y g:i A');
        $this->orderActivity($order_details->id, 'admin', $log, auth()->user()->id, '','');
        }
       
        return response()->json(['status' => 1, 'message' => 'Product accept successfully']);
    }
    public function cancelProduct(Request $req)
    {
        
        OrderItems::where('order_id', $req->order)
            ->where('product_id', $req->product)
            ->where('branch_id', $req->supplier)
            ->where('id',$req->variant)
            ->update([
                'status' => 3
            ]);
        $cancelled_item = OrderItems::where('order_id', $req->order)
            ->where('product_id', $req->product)
            ->where('supplier_id', $req->supplier)
            ->first();
        $order = Order::where('id', $req->order)->first();
        
        $sub = $order->sub_total - $cancelled_item->item_price;
        $grant = $order->grant_total - $cancelled_item->item_price;
        $order->update([
            'sub_total' => $sub,
            'grant_total' => $grant
        ]);
        $accepted_order_item_count = DB::table('order_items')->where('order_id', $req->order)->whereIn('status',[2,3])->count();
        $order_item_count = $this->order->where('id', $req->order)->first();
        if ($accepted_order_item_count == $order_item_count->item_count) {
            $this->order->where('id', $req->order)
                ->update(['order_status' => 3]);
                
            $this->notification($req->order);
        } else {
            $pending = DB::table('order_items')->where('order_id', $req->order)->where('status', 1)->count();
            if ($pending) {
                $this->order->where('id', $req->order)
                    ->update(['order_status' => 1]);
            } else {
                $this->order->where('id', $req->order)
                    ->update(['order_status' => 2]);
            }
        }
        $accepted_supplier_order_item_count = DB::table('order_items')
            ->where('order_id', $req->order)
            ->where('supplier_id', $req->supplier)
            ->where('status', 2)->count();
        $order_item_count_supp = DB::table('order_items')
            ->where('order_id', $req->order)
            ->where('supplier_id', $req->supplier)
            ->count();

        if ($accepted_supplier_order_item_count == $order_item_count_supp) {
           
            DB::table('order_items')
                ->where('order_id', $req->order)
                ->where('supplier_id', $req->supplier)
                ->update([
                    'supplier_status' => 5
                ]);
            
        $order_details = Order::find($req->order);
        $log ='Supplier <b>'. auth()->user()->name . '</b> rejected there items in the order '.$order_details->order_id.' on ' . Carbon::now()->format('d/m/Y g:i A');
        $this->orderActivity($order_details->id, 'admin', $log, auth()->user()->id, '', '');
        }
      
        return response()->json(['status' => 1, 'message' => 'Product Cancelled successfully']);
    }

    private function notification($order_id)
    {
        $order_data = Order::where('id', '=', $order_id)->first();
        $supplier = auth()->user();
        $notifications = new AppNotifications;
        $notifications->activity_id = $order_id;
        $notifications->is_from = 'order';
        $notifications->from_type = 'supplier';
        $notifications->to_type = 'admin';
        $notifications->to_id = 1;
        
        $content = json_encode([
                'en' => 'Supplier ('.$supplier->code.') accept/reject the products on  ' . Carbon::now()->format('d/m/Y g:i A'),
                'ar' =>'Supplier ('.$supplier->code.') accept/reject the products on  ' . Carbon::now()->format('d/m/Y g:i A')
            ]);
        $detail=json_encode([
            'type'=>'order',
            'order_id'=>$order_id,
        ]);
        $notifications->content = $content;
        $notifications->details = $detail;
        $notifications->save();
    }
}
