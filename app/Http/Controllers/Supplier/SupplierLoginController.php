<?php

namespace App\Http\Controllers\Supplier;

use App\Models\UserRoles;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class SupplierLoginController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:supplier')->except('logout');
    }

    public function login()
    {
        return view('supplier.auth.login');
    }

    public function supplierLogin(Request $request)
    {

        $roles = UserRoles::where('name', "Supplier")->first();

        if (Auth::guard('supplier')->attempt(['email' => $request->email, 'password' => $request->password, 'role_id' => $roles->id, 'status' => 'active'], $request->get('remember'))) {

            return redirect()->intended('/supplier');
        } else {
            return redirect()->back()->withErrors(['email' => 'Email/Password Incorrect!']);
        }
    }
    public function logout()
    {
        
        if(Auth::guard('supplier')->check()) 
        Auth::guard('supplier')->logout();

        Session::flash('success', 'Logout successfully.');
        return redirect()->route('supplier.login');
    }
}
