<?php

namespace App\Http\Controllers\Supplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Validator;

use App\Models\Supplier;

class EditProfileController extends Controller {

    public function index() {
        $supplier = Supplier::where('id', Auth::user()->id)->first();
        //   dd($user->phone);
        return view('supplier.auth.edit_profile', compact('supplier'));
    }

    public function profile_update(Request $request) {
        $rules = [
            'auth_name' => 'required',
            'auth_email' => 'required|unique:supplier,email,'.Auth::user()->id,
            'phone' => 'required|unique:supplier,phone,'.Auth::user()->id,
        ];
        $messages = [
            'auth_name.required' => 'Name required',
            'auth_email.required' => 'Email is required.',
            'auth_email.unique' => 'Supplier with same email already exists',
            'mobile.required' => 'Phone number is required.',
            'mobile.unique' => 'Phone number should be unique',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            Supplier::where('id', Auth::user()->id)
                    ->update([
                        'name' => $request->auth_name,
                        'email' => $request->auth_email,
                        'phone' => (int)$request->phone,
            ]);
            return response()->json(['status' => 1, 'message' => __('messages.admin.profile_msg1')]);
        }
    }

    public function password_change(Request $request) {
        $rules = [
            'password' => 'min:6|required_with:confirm_password|same:confirm_password',
            'confirm_password' => 'min:6'
        ];
        $messages = [
            'password.required' => 'Password required',
            'confirm_password.required' => 'Confirm password required'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
        Supplier::where('id', Auth::user()->id)
                ->update([
                    'password' => Hash::make($request->password)
        ]);
        return response()->json(['status' => 1, 'message' => __('messages.admin.profile_msg2')]);
        }
    }

}
