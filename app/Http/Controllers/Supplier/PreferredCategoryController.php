<?php

namespace App\Http\Controllers\Supplier;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\SupplierCategory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class PreferredCategoryController extends Controller
{
    public function get()
    {
        $main_cat = Category::with('lang')
        ->where('parent_id', NULL)
        ->where('status', "active")
        ->get();
      
       
        $category = Category::with('lang')->get();

        $selected_category=SupplierCategory::where('supplier_id',auth()->user()->id)
                        ->get();

        return view('supplier.product.category.index',compact('main_cat','category','selected_category'));
    }

    public function categoryStore(Request $request)
    {
        // dd($request->toArray());
        $supplier=auth()->user();
        $supplier->category()->detach();
        $supplier->category()->attach(
            $request->category ?? [],
            ['created_at' => now(), 'updated_at' => now()]
        );
        Session::flash('success',  __('messages.admin.category_update_msg'));
        return redirect()->route('supplier.category_list');
    }
    
    public function getSubcategory(Request $req){
        $selected_category=SupplierCategory::where('supplier_id',auth()->user()->id)
                        ->get('category_id')->toArray();
        $cat_data = Category::with(['lang' => function ($query) {
                        $query->where('language', 'en');
                    }])->where('parent_id', $req->cat_id)->whereIn('id',$selected_category)->get();
        return $cat_data;
                    
    }
}
