<?php

namespace App\Http\Controllers\Supplier;

use DateTime;
use DatePeriod;
use DateInterval;
use Carbon\Carbon;
use App\Models\Brand;
use App\Models\Order;
use App\Models\Category;
use App\Models\Customer;
use App\Models\OrderItems;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

class ReportController extends Controller
{
    public function index()
    {
        $category = Category::with('lang')->where(['status' => 'active', 'level' => 1])->get();
        $brand = Brand::with('lang')->where(['status' => 'active'])->get();
        // $supplier = Supplier::where(['role_id' => 5, 'status' => 'active'])->get();
        return view('supplier.reports.index', compact('category', 'brand'));
    }
    public function salestab(Request $req){
        $lang = Config::get('app.locale') ?? 'en';
        $start = Carbon::parse($req->start_date)->format('Y-m-d');
        $end = Carbon::parse($req->end_date)->format('Y-m-d');
        $filter_brand = $req->filter_brand;
        $filter_category = $req->filter_category;
        $filter_supplier = $req->filter_supplier;
        $query = DB::table('order')
                    ->select('product.id as id', DB::raw('sum(distinct order_items.grant_total) as value'), 'product_i18n.name as label', DB::raw('count(distinct order.id) as sale_count'))
                    ->join('order_items', 'order.id', '=', 'order_items.order_id')
                    ->join('product_categories', 'order_items.product_id', '=', 'product_categories.product_id')
                    ->join('product', 'product.id', '=', 'product_categories.product_id')
                    ->join('product_i18n', function($join) use($lang){
                        $join->on('product_i18n.product_id','=','order_items.product_id')
                          ->where('product_i18n.language','=',$lang);
                      });
                    $query->where('order.order_status', 9)
                    ->where('order_items.branch_id',auth()->user()->id);
                   if(!empty($filter_category)){
                    $query->whereIn('product_categories.category_id', $filter_category);   
                   }
                   if(!empty($filter_brand)){
                    $query->whereIn('product.brand_id', $filter_brand);   
                   }
                   if(!empty($filter_supplier)){
                    $query->where('product.supplier_id', $filter_supplier);   
                   }
        $products = $query->whereDate('order.created_at', '>=', $start)
                    ->whereDate('order.created_at', '<=', $end)
                    ->orderBy('sale_count', 'desc')
                    ->groupBy('product_i18n.product_id')
                    ->limit(5)->get();
       
        $top_products = $products;
        
        
        $query = DB::table('order')
                    ->select('category.id as id', DB::raw('sum(distinct order_items.grant_total) as value'), 'category_i18n.name as label', DB::raw('count(distinct order_items.order_id) as sale_count'))
                    ->join('order_items', 'order.id', '=', 'order_items.order_id')
                    ->join('product_categories', 'order_items.product_id', '=', 'product_categories.product_id')
                    ->join('product', 'product.main_category', '=', 'product_categories.category_id')
                    ->join('category', 'category.id', '=', 'product_categories.category_id')
                    ->join('category_i18n', function($join) use($lang){
                        $join->on('category_i18n.category_id','=','category.id')
                          ->where('category_i18n.language','=',$lang);
                      })
                    ->where('order.order_status', 9)
                    ->where('order_items.branch_id',auth()->user()->id);
                   if(!empty($filter_category)){
                    $query->whereIn('product_categories.category_id', $filter_category);   
                   }
                   if(!empty($filter_brand)){
                    $query->whereIn('product.brand_id', $filter_brand);   
                   }
                   if(!empty($filter_supplier)){
                    $query->where('product.supplier_id', $filter_supplier);   
                   }
        $categories = $query->whereDate('order.created_at', '>=', $start)
                    ->whereDate('order.created_at', '<=', $end)
                    ->orderBy('sale_count', 'desc')
                    ->groupBy('category.id')
                    ->limit(5)->get();
        $topcategories = $categories;
        
        
        // Applying categories filter
        $cats = $req->filter_category;
        $all_cat = $topcategories;
          if (!empty($topcategories) && !empty($cats)) {
            $all_cat = array();
            foreach ($topcategories as $key => $value) {
                if (in_array($value->id, $cats)) {
                  $all_cat[] = $value;  
                }
            }
        }  
        $all_filt_cat_final = $all_cat;
        
        $query = DB::table('order')
                    ->select('brand_i18n.brand_id as id',DB::raw('sum(distinct order_items.grant_total) as value'), 'brand_i18n.name as label', DB::raw('count(distinct order_items.order_id) as sale_count'))
                    ->join('order_items', 'order.id', '=', 'order_items.order_id')
                    ->join('product_categories', 'order_items.product_id', '=', 'product_categories.product_id')
                    ->join('product', 'order_items.product_id', '=', 'product.id')
                    ->join('brand_i18n', function($join) use($lang){
                        $join->on('product.brand_id','=','brand_i18n.brand_id')
                          ->where('brand_i18n.language','=',$lang);
                      })
                    ->where('order.order_status', 9)
                    ->where('order_items.branch_id',auth()->user()->id);
                   if(!empty($filter_category)){
                    $query->whereIn('product_categories.category_id', $filter_category);   
                   }
                   if(!empty($filter_brand)){
                    $query->whereIn('product.brand_id', $filter_brand);   
                   }
                   if(!empty($filter_supplier)){
                    $query->where('product.supplier_id', $filter_supplier);   
                   }
       $brands = $query->whereDate('order.created_at', '>=', $start)
                    ->whereDate('order.created_at', '<=', $end)
                    ->orderBy('sale_count', 'desc')
                    ->groupBy('brand_i18n.brand_id')
                    ->limit(5)->get();
        $top_brands = $brands;
        
        // Applying brand filter
        $filter_brand = $req->filter_brand;
        $all_brands = $top_brands; 
        if (!empty($top_brands) && !empty($filter_brand)) {
            $all_brands = array();
            foreach ($top_brands as $key => $value) {
                if (in_array($value->id, $filter_brand)) {
                  $all_brands[] = $value;  
                }
            }
        } 
        $all_filt_brand_final =  $all_brands;
        
       $query = DB::table('order')->select(DB::raw('sum(order.grant_total) as sum'));
       if(!empty($filter_category) || !empty($filter_brand) || !empty($filter_supplier)){
            $query->join('order_items', 'order.id', '=', 'order_items.order_id')
              ->join('product_categories', 'order_items.product_id', '=', 'product_categories.product_id')
              ->join('product', 'product.main_category', '=', 'product_categories.category_id');
       }
        if(!empty($filter_category)){
            $query->whereIn('product_categories.category_id', $filter_category);   
        }
       if(!empty($filter_brand)){
        $query->whereIn('product.brand_id', $filter_brand);   
       }
       if(!empty($filter_supplier)){
        $query->where('product.supplier_id', $filter_supplier);   
       }
   
        $total_sale =OrderItems::leftJoin('order', 'order.id', '=', 'order_items.order_id')
        ->where('order.order_status', 9)
        ->where('order_items.branch_id', auth()->user()->id)
        ->sum('order_items.grant_total'); 

        $total_sale = $total_sale ? $total_sale : 0.00;
        
        
        $query = DB::table('order')->select(DB::raw('count(distinct order.id) as count'));
        
            if(!empty($filter_category) || !empty($filter_brand) || !empty($filter_supplier)){
                $query->join('order_items', 'order.id', '=', 'order_items.order_id')
                ->join('product_categories', 'order_items.product_id', '=', 'product_categories.product_id')
                ->join('product', 'order_items.product_id', '=', 'product.id');
        }
        if(!empty($filter_category)){
            $query->whereIn('product_categories.category_id', $filter_category);   
            }
        if(!empty($filter_brand)){
            $query->whereIn('product.brand_id', $filter_brand);   
        }
        if(!empty($filter_supplier)){
            $query->where('product.supplier_id', $filter_supplier);   
        }
        // $total_orders=$query->where('order.order_status', 9)->whereDate('order.created_at', '>=', $start)
        //                   ->whereDate('order.created_at', '<=', $end)->first();
        $total_orders = Order::leftJoin('order_items as item', 'order.id', '=', 'item.order_id')
        ->where('order.order_status', 9)
        ->where('item.branch_id', auth()->user()->id)
        ->groupBy('order.id')
        ->get()
        ->count();
        $total_orders = $total_orders ? $total_orders : 0;
        
        $total_customers = Customer::where('status', 'Y')->whereBetween('created_at', [$start, $end])->count();
        
        echo json_encode(['message'=> 'Sales tab data fetched', 'top_categories' =>$topcategories, 'all_categories' =>$all_filt_cat_final, 'total_sale' =>$total_sale, 
            'top_brands' => $top_brands, 'all_brands' =>$all_filt_brand_final, 'top_products' =>$top_products, 
            'total_orders' => $total_orders, 'total_customers' => $total_customers, 'status' => TRUE]);
        return;
    }

    public function linechart(Request $req) {
        
        $category = Category::with('lang')->where(['status' => 'active', 'level' => 1])->get();
        $brand = Brand::with('lang')->where(['status' => 'active'])->get();
        // $supplier = Supplier::where(['role_id' => 5, 'status' => 'active'])->get();
        return view('supplier.reports.linechart', compact('category', 'brand'));
    }
    
    public function linecharttab(Request $req){
        $lang = Config::get('app.locale') ?? 'en';
        $start = Carbon::parse($req->start_date)->format('Y-m-d');
        $end = Carbon::parse($req->end_date)->format('Y-m-d');
        $filter_brand = $req->filter_brand;
        $filter_category = $req->filter_category;
        $filter_supplier = $req->filter_supplier;
        
        $date1 = date_create($start);
        $date2 = date_create($end);
        $diff = date_diff($date1,$date2);
        $datecount = $diff->format("%a");
        
        $sql = DB::table('order');
        if ($datecount > 31) {
        $sql->select('category.id as id',DB::raw('sum(distinct order_items.grant_total) as value'), 'category_i18n.name as label',DB::raw('DATE_FORMAT(order.created_at,"%Y-%M") as date'),DB::raw('DATE_FORMAT(order.created_at,"%Y%m") as sortcol'));
        }else{
          $sql->select('category.id as id',DB::raw('sum(distinct order_items.grant_total) as value'), 'category_i18n.name as label',DB::raw('DATE_FORMAT(order.created_at,"%Y-%m-%d") as date'),DB::raw('DATE_FORMAT(order.created_at,"%Y%m%d") as sortcol'));  
        }         
        $sql->join('order_items', 'order.id', '=', 'order_items.order_id')
                    ->join('product_categories', 'order_items.product_id', '=', 'product_categories.product_id')
                    ->join('product', 'product.main_category', '=', 'product_categories.category_id')
                    ->join('category', 'category.id', '=', 'product_categories.category_id')
                    // ->join('category_i18n', 'category_i18n.category_id', '=', 'category.id')
                    ->join('category_i18n', function($join) use($lang){
                        $join->on('category_i18n.category_id','=','category.id')
                          ->where('category_i18n.language','=',$lang);
                      })
                    ->where('order.order_status', 9)
                    ->where('order_items.branch_id',auth()->user()->id);
        
        if(!empty($filter_category)){
           $sql->whereIn('product_categories.category_id', $filter_category);   
        }
        if(!empty($filter_brand)){
            $sql->whereIn('product.brand_id', $filter_brand);   
        }
        if(!empty($filter_supplier)){
            $sql->where('product.supplier_id', $filter_supplier);   
        }
        $categories = $sql->whereDate('order.created_at', '>=', $start)
            ->whereDate('order.created_at', '<=', $end)
            ->groupBy('category.id','order.created_at')
            ->get();
            $topcategories = $categories;
        
            // Applying categories filter
            $cats = $req->filter_category;
            $all_cat = $topcategories;
            if (!empty($topcategories) && !empty($cats)) {
                $all_cat = array();
                foreach ($topcategories as $key => $value) {
                    if (in_array($value->id, $cats)) {
                    $all_cat[] = $value;  
                    }
                }
            }  
            $all_filt_cat_final = $all_cat;
        
        $query = DB::table('order')->select(DB::raw('sum(distinct order.grant_total) as sum'));
        if(!empty($filter_category) || !empty($filter_brand) || !empty($filter_supplier)){
                $query->join('order_items', 'order.id', '=', 'order_items.order_id')
                ->join('product_categories', 'order_items.product_id', '=', 'product_categories.product_id')
                ->join('product', 'product.main_category', '=', 'product_categories.category_id');
        }
            if(!empty($filter_category)){
                $query->whereIn('product_categories.category_id', $filter_category);   
            }
        if(!empty($filter_brand)){
            $query->whereIn('product.brand_id', $filter_brand);   
        }
        if(!empty($filter_supplier)){
            $query->where('product.supplier_id', $filter_supplier);   
        }
        //    $total_sale=$query->where('order.order_status', 9)->whereDate('order.created_at', '>=', $start)
        //                       ->whereDate('order.created_at', '<=', $end)->first();
        //    $total_sale = $total_sale->sum ? $total_sale->sum : 0.00;
        $total_sale =OrderItems::leftJoin('order', 'order.id', '=', 'order_items.order_id')
        ->where('order.order_status', 9)
        ->where('order_items.branch_id', auth()->user()->id)
        ->sum('order_items.grant_total'); 
        $total_sale = $total_sale ? $total_sale : 0.00;
        $query = DB::table('order')->select(DB::raw('count(distinct order.id) as count'));
        
            if(!empty($filter_category) || !empty($filter_brand) || !empty($filter_supplier)){
                $query->join('order_items', 'order.id', '=', 'order_items.order_id')
                ->join('product_categories', 'order_items.product_id', '=', 'product_categories.product_id')
                ->join('product', 'order_items.product_id', '=', 'product.id');
        }
        if(!empty($filter_category)){
            $query->whereIn('product_categories.category_id', $filter_category);   
            }
        if(!empty($filter_brand)){
            $query->whereIn('product.brand_id', $filter_brand);   
        }
        if(!empty($filter_supplier)){
            $query->where('product.supplier_id', $filter_supplier);   
        }
            // $total_orders=$query->where('order.order_status', 9)->whereDate('order.created_at', '>=', $start)
            //                   ->whereDate('order.created_at', '<=', $end)->first();
            $total_orders = Order::leftJoin('order_items as item', 'order.id', '=', 'item.order_id')
            ->where('order.order_status', 9)
            ->where('item.branch_id', auth()->user()->id)
            ->groupBy('order.id')
            ->get()
            ->count();
            $total_orders = $total_orders ? $total_orders : 0;
            
            
            // Graph
            
            $linegraphdata = array();
            $linegraphdata_final = array();
            $yaxis = array();

            if(!empty($all_filt_cat_final)) {
                foreach ($all_filt_cat_final as $key => $value) {
                    $linegraphdata[$value->date]['day'] = $value->date;
                    $linegraphdata[$value->date]['sortcol'] = $value->sortcol;
                    if (!isset($linegraphdata[$value->date][$value->label])) {
                        $linegraphdata[$value->date][$value->label] = 0;
                    }
                    if (!isset($linegraphdata[$value->date]['value'])) {
                        $linegraphdata[$value->date]['value'] = 0;
                    }
                    $linegraphdata[$value->date][$value->label] += $value->value;
                    $linegraphdata[$value->date]['value'] += $value->value;
                    array_push($yaxis,$value->label);
                }
                foreach ($linegraphdata as $key => $value) {
      //                $total_sale += $value['value']; 
                    unset($value['value']);
                    $linegraphdata_final[] = $value;
                }
                $yaxis = array_unique($yaxis);
            }
            
            $y_axis = array();
            if (!empty($yaxis)) {
                foreach ($yaxis as $key => $value) {
                    array_push($y_axis,$value);
                }
            }
            
            $linegraph_data = array();
            if (!empty($linegraphdata_final)) {
                foreach ($linegraphdata_final as $key => $value) {
                    foreach ($yaxis as $ykey => $yvalue) {
                        if (!isset($value[$yvalue])) {
                            $value[$yvalue] = 0;
                        } else {
                            $temp = $value[$yvalue];
                            unset($value[$yvalue]);
                            $value[$yvalue] = $temp;
                        }
                    }
                    // array_push($linegraph_data,$value);
                    $linegraph_data[$value['day']] = $value;
                }
            }

            $lgraph_data = array();
            if (!empty($linegraph_data)) {
            if ($datecount > 31) {
                $temp_date_from = date("Y-F", strtotime($start));
                $temp_date_to = date("Y-F", strtotime($end));
                $month_array = array($temp_date_from);
                while($temp_date_from != $temp_date_to){
                    $temp_date_key = date("Ym", strtotime($temp_date_from. ' +1 months'));
                    $temp_date_from = date("Y-F", strtotime($temp_date_from. ' +1 months'));
                    $month_array[$temp_date_key] = $temp_date_from; 
                }
                foreach ($month_array as $key => $value) {
                    if (isset($linegraph_data[$value])) {
                        array_push($lgraph_data ,$linegraph_data[$value]);
                    } else {
                        $temp_array = array();
                        $temp_array['day'] = $value;
                        $sortcol = $key;
                        $temp_array['sortcol'] = $sortcol;
                        foreach ($y_axis as $ykey => $yvalue) {
                        $temp_array[$yvalue] = 0;
                        }
                        array_push($lgraph_data ,$temp_array);                        
                    }
                }

                usort($lgraph_data, function ($a, $b) { return $a['sortcol'] - $b['sortcol']; });
                foreach ($lgraph_data as $key => $value) {
                    unset($lgraph_data[$key]['sortcol']);
                }
            } else {
                if (!empty($linegraph_data)) {
                    $temp_date_to = date("Y-m-d", strtotime($end. ' +1 days'));
                    $period = new DatePeriod(
                        new DateTime($start),
                        new DateInterval('P1D'),
                        new DateTime($temp_date_to)
                    );
                    foreach ($period as $key => $value) {
                        $nowdate = $value->format('Y-m-d');
                        $sortcol = $value->format('Ymd');
                        if (isset($linegraph_data[$nowdate])) {
                            array_push($lgraph_data ,$linegraph_data[$nowdate]);
                        } else {
                            $temp_array = array();
                            $temp_array['day'] = $nowdate;
                            $temp_array['sortcol'] = $sortcol;
                            foreach ($y_axis as $ykey => $yvalue) {
                            $temp_array[$yvalue] = 0;
                            }
                            array_push($lgraph_data ,$temp_array);                        
                        }
                    }
                    
                    usort($lgraph_data, function ($a, $b) { return $a['sortcol'] - $b['sortcol']; });
                    foreach ($lgraph_data as $key => $value) {
                        unset($lgraph_data[$key]['sortcol']);
                    }
                }
            }
            }
            
        $total_customers = Customer::where('status', 'Y')->whereBetween('created_at', [$start, $end])->count();
        
        echo json_encode(['message'=> 'Sales line chart tab data fetched', 'total_sale' =>$total_sale, 'total_orders' => $total_orders, 'total_customers' => $total_customers, 
            'linegraph_ykeys' => $y_axis,'linegraph_data' => $lgraph_data, 'status' => TRUE]);
            return;
    }
    public function hourlychart(Request $req) {
        
        return view('supplier.reports.hourly');
    }
    
    public function hourlytab(Request $req){
        $start = Carbon::parse($req->start_date)->format('Y-m-d');
        $end = Carbon::parse($req->end_date)->format('Y-m-d');
        
        
        
        //    $total_sale = DB::table('order')->where('order.order_status', 9)->whereDate('order.created_at', '>=', $start)
        //                       ->whereDate('order.created_at', '<=', $end)->sum('order.grant_total');
            
        $total_sale =OrderItems::leftJoin('order', 'order.id', '=', 'order_items.order_id')
        ->where('order.order_status', 9)
        ->where('order_items.branch_id', auth()->user()->id)
        ->sum('order_items.grant_total'); 
        $total_orders = Order::leftJoin('order_items as item', 'order.id', '=', 'item.order_id')
        ->where('order.order_status', 9)
        ->where('item.branch_id', auth()->user()->id)
        ->groupBy('order.id')
        ->get()
        ->count();

        //    $total_orders = DB::table('order')->where('order.order_status', 9)->whereDate('order.created_at', '>=', $start)
        //           ->whereDate('order.created_at', '<=', $end)->count('order.id');
            
        $products = DB::table('order')
                    ->select(DB::raw('distinct sum(order_items.grant_total) as sales'), DB::raw('DATE_FORMAT(order.created_at,"%l %p") as time'), DB::raw('DATE_FORMAT(order.created_at,"%H") as date'))
                    ->join('order_items', 'order.id', '=', 'order_items.order_id')
                    ->where('order.order_status', 9)
                    ->where('order_items.branch_id',auth()->user()->id)
                    ->whereDate('order.created_at', '>=', $start)
                    ->whereDate('order.created_at', '<=', $end)
                    ->groupBy('date','time')
                    ->get();
        
        $all_products = $products;
        $hourly_data = array();

        if (!empty($all_products)) {
            foreach ($all_products as $key => $value) {
                $times = explode(' ', $value->time);
                $right_time = $times[0] + 1;
                if ($right_time == 13) {
                    if ($times[1] == 'AM') {
                        $right_time = '1 AM';
                    } else {
                        $right_time = '1 PM';
                    }
                } elseif ($right_time == 12) {
                    if ($times[1] == 'AM') {
                        $right_time = '12 PM';
                    } else {
                        $right_time = '12 AM';
                    }
                } else {
                    $right_time = $right_time .''.$times[1];
                }
                $newtime = str_replace(' ', '', $value->time) . ' - ' . str_replace(' ', '', $right_time);
                $value->time = $newtime;
                unset($value->date);
                $hourly_data[$newtime] = $value;
            }
        }
        
        $timespan_array = array('12AM - 1AM',
                                '1AM - 2AM',
                                '2AM - 3AM',
                                '3AM - 4AM',
                                '4AM - 5AM',
                                '5AM - 6AM',
                                '6AM - 7AM',
                                '7AM - 8AM',
                                '8AM - 9AM',
                                '9AM - 10AM',
                                '10AM - 11AM',
                                '11AM - 12PM',
                                '12PM - 1PM',
                                '1PM - 2PM',
                                '2PM - 3PM',
                                '3PM - 4PM',
                                '4PM - 5PM',
                                '5PM - 6PM',
                                '6PM - 7PM',
                                '7PM - 8PM',
                                '8PM - 9PM',
                                '9PM - 10PM',
                                '10PM - 11PM',
                                '11PM - 12AM');
        $hourly_data_final = array(); 
        foreach ($timespan_array as $key => $value) {
            if (isset($hourly_data[$value])) {
               array_push($hourly_data_final,$hourly_data[$value]); 
            } else {
               array_push($hourly_data_final,array('sales' => 0,'time' => $value));                
            }
        }
        
        $total_customers = Customer::where('status', 'Y')->whereBetween('created_at', [$start, $end])->count();
        
        echo json_encode(['message'=> 'Hourly tab data fetched','total_sale' =>$total_sale, 'total_orders' => $total_orders, 'total_customers' => $total_customers, 'hourly_data' => $hourly_data_final, 'status' => TRUE]);
        return;
    }
    


}
