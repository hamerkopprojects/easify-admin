<?php

namespace App\Http\Controllers\supplier;

use App\Models\City;
use App\Models\Region;
use App\Models\Supplier;
use App\Models\UserRoles;
use App\Models\ProductBranchStock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\BranchAnnex;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\Mail;
use App\Services\SendEmail;
use URL;

class BranchController extends Controller {

    public function index(Request $req) {
        $search = isset($req->search) ? $req->search : '';
        $roles = UserRoles::where('name', "Branch")->first();
        $branch = BranchAnnex::with(['supplier' => function($query) use($search){
                            $query->where('role_id', 6)->with('region');
                            if ($search) {
                                    $query->where('name', 'like', "%" . $search . "%")
                                    ->orWhere('email', 'like', "%" . $search . "%")
                                    ->orWhere('phone', 'like', "%" . $search . "%");
                           }
                        }])->where('supplier_parent_id', auth()->user()->id)->paginate(20);
        $region = Region::with('lang')->get();
        $city = City::with('lang')->get();
        return view('supplier.branch.index', compact('branch', 'search', 'region', 'city'));
    }

    public function store(Request $req) {

        $rules = [
            'name' => 'required',
            'contact_name' => 'required',
            'email' => 'required|unique:supplier,email',
            'phone' => 'required|unique:supplier,phone',
        ];
        $messages = [
            'name.required' => 'Name is required.',
            'contact_name.required' => 'Contact Name is required.',
            'email.required' => 'Email is required.',
            'email.unique' => 'Email already exist.',
            'phone.required' => 'Phone is required.',
            'phone.unique' => 'Phone already exist.',
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {

            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $roles = UserRoles::where('name', "Branch")->first();
            $supplier_id = auth()->guard('supplier')->user()->id;
            $supplier_data = Supplier::where('id', '=', $supplier_id)->first();
            $regions[] = $supplier_data->region_id;
            $branch_data = BranchAnnex::with(['supplier' => function($query) {
                            $query->where('role_id', 6);
                        }])->where('supplier_parent_id', $supplier_id)->get();
            if(!empty($branch_data)){
            foreach ($branch_data as $value) {
                $regions[] = $value->supplier->region_id;
            }
            }
            $regions = array_unique($regions);
            if (in_array($req->region, $regions)) {
                return response()->json(['status' => 0, 'message' => 'Seems supplier / branch exist with region selected.']);
            }
           
            $branch = DB::transaction(function () use ($req, $roles, $supplier_id) {

                        $branch = Supplier::create([
                                    'name' => $req->name,
                                    'contact_person_name' => $req->contact_name,
                                    'email' => $req->email,
                                    'phone' => (int)$req->phone,
                                    'name' => $req->name,
                                    'city_id' => $req->city,
                                    'region_id' => $req->region,
                                    'role_id' => $roles->id,
                                    'code' => $this->generateBranchId(),
                        ]);

                        $branch = BranchAnnex::create([
                                    'supplier_id' => $branch['id'],
                                    'supplier_parent_id' => $supplier_id,
                        ]);

                        return $branch;
                    });
            $msg = __('messages.admin.branch_added_msg');
            if ($branch) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }

    public function edit($id) {

        $branch = Supplier::where('id', $id)->first();

        return[
            'branch' => $branch
        ];
    }

    public function update(Request $req) {

        $rules = [
            'name' => 'required',
            'contact_name' => 'required',
        ];
        $messages = [
            'name.required' => 'Name is required.',
            'contact_name.required' => 'Contact Name is required.',
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {

            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $supplier_id = auth()->guard('supplier')->user()->id;
            $supplier_data = Supplier::where('id', '=', $supplier_id)->first();
            $regions[] = $supplier_data->region_id;
                $branch_data = BranchAnnex::with(['supplier' => function($query) {
                            $query->where('role_id', 6);
                        }])->where('supplier_parent_id', $supplier_id)->where('supplier_id', '!=', $req->branch_unique)->get();
            foreach ($branch_data as $value) {
                $regions[] = $value->supplier->region_id;
            }
            $regions = array_unique($regions);
            if (in_array($req->region, $regions)) {
                return response()->json(['status' => 0, 'message' => 'Seems supplier / branch exist with region selected.']);
            }

            $branch = DB::transaction(function () use ($req) {
                        $branch = Supplier::where('id', $req->branch_unique)->update([
                            'name' => $req->name,
                            'contact_person_name' => $req->contact_name,
                            'email' => $req->email,
                            'phone' => $req->phone,
                            'name' => $req->name,
                            'city_id' => $req->city,
                            'region_id' => $req->region,
                        ]);


                        return $branch;
                    });
            $msg = __('messages.admin.branch_updated_msg');
            return response()->json(['status' => 1, 'message' => $msg]);
        }
    }

    public function statusUpdate(Request $req) {

        $status = $req->status === 'Activate' ? 'active' : 'deactive';

        $statusUpdate = Supplier::where('id', $req->id)
                ->update([
            'status' => $status
        ]);
        return response()->json(['status' => 1, 'message' =>  __('messages.admin.branch_status_msg')]);
    }

    public function destroy(Request $req) {

        $branch = Supplier::find($req->id);
        $productvar = ProductBranchStock::where(['branch_id' => $req->id])->exists();

        if (!empty($branch) && $productvar == false) {
            BranchAnnex::where(['supplier_id' => $req->id])->delete();
            $branch->delete();
            return response()->json(['status' => 1, 'message' => __('messages.admin.onetime_pwd')]);
        } else {
            return response()->json(['status' => 0, 'message' => 'Cannot delete branch have related records']);
        }
    }

    public function sendPassword(Request $req) {

        $supplier = Supplier::where('id', $req->id)->first();
        // $password=mt_rand(100000,999999);
        $password = 1234;
        $data = [
            'password' => $password,
            'user' => $supplier
        ];

        DB::transaction(function () use ($data, $password, $supplier, $req) {
            Supplier::where('id', $req->id)->update([
                'password' => Hash::make($password)
            ]);
            //  Mail::to($supplier->email)->send(new SetPasswordEmail($data));
        });
        $subject = 'Welcome to EASIFY!';
        $url = URL::to('/branch/login');
        SendEmail::generateEmail($supplier->email, $password, $supplier->name, $subject, 'branch', $url);
        return response()->json(['status' => 1, 'message' =>  __('messages.admin.onetime_pwd_msg')]);
    }

    public function generateBranchId() {

        $roles = UserRoles::where('name', "Branch")->first();

        $lastbranch = Supplier::select('code')
                ->where('role_id', $roles->id)
                ->orderBy('id', 'desc')
                ->first();

        $lastId = 0;

        if ($lastbranch) {
            $lastId = (int) substr($lastbranch->code, 9);
        }

        $lastId++;

        return 'EAS-BRAN-' . str_pad($lastId, 5, '0', STR_PAD_LEFT);
    }

     public function getRegion(Request $req)
    {
        $region=Region::with('lang')->where('city_id', $req->city_id)->get();
        return response()->json($region);
    }
}
