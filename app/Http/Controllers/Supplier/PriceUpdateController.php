<?php

namespace App\Http\Controllers\Supplier;

use DB;
use Excel;
use Config;
use Validator;
use App\Models\Product;
use App\Models\VariantLang;
use App\Models\TempPrice;
use Illuminate\Http\Request;
use App\Imports\ProductImport;
use Illuminate\Support\Carbon;
use App\Models\ProductPrice;
use App\Http\Controllers\Controller;

class PriceUpdateController extends Controller {

    public function index(Request $request) {

        if (!empty($request['type'])) {
            $type = $request['type'];
        }
        if (!empty($request->action)) {
            $type = $request->action;
        }
        TempPrice::truncate();
        return view('supplier.product.price_updates.index', compact('type'));
    }

    public function pricesubmit(Request $req) {

        $type = $req->type;
        $rules = [
            'select_file' => 'required|mimes:xlsx|max:2048',
        ];
        $messages = [
            'select_file.required' => 'File is required.',
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $file = $req->file('select_file');
            $data = Excel::toArray(new ProductImport, $file);

            if ($type == "simple") {
                TempPrice::truncate();
                if (count($data) > 0) {
                    foreach ($data as $key => $sub) {
                        foreach ($sub as $key1 => $val) {
                            if ($key1 > 0) {
                                $errors = "";
                                $temp_price = new TempPrice;

                                $temp_price->sku = trim($val[0]);
                                if ($temp_price->sku == "") {
                                    $errors .= "SKU is required,";
                                }
                                $temp_price->price = $val[1];
                                if ($temp_price->price == "") {
                                    $errors .= "Price is required,";
                                }
                                $temp_price->discount_price = $val[2];
                                if ($errors) {
                                    $temp_price->error_flag = "Y";
                                    $temp_price->errors = $errors;
                                } else {
                                    $temp_price->error_flag = "N";
                                }
                                if (!empty($temp_price->sku)) {
                                    $pdt_data = TempPrice::where('sku', '=', $temp_price->sku)->first();

                                    if (!empty($pdt_data)) {
                                        $temp_price->duplicate_flag = 'Y';
                                    }
                                }
                                $temp_price->created_at = Carbon::now();

                                $temp_price->save();
                            }
                        }
                    }
                    return response()->json(['status' => '1', 'type' => $type]);
                } else {
                    return response()->json(['message' => $validator->errors()->first(), 'status' => '0']);
                }
            } else {
                TempPrice::truncate();
                if (count($data) > 0) {
                    foreach ($data as $key => $sub) {
                        foreach ($sub as $key1 => $val) {
                            if ($key1 > 0) {
                                $errors = "";
                                $temp_price = new TempPrice;

                                $temp_price->sku = trim($val[0]);
                                if ($temp_price->sku == "") {
                                    $errors .= "SKU is required,";
                                }
                                $temp_price->variant = $val[1];
                                if ($temp_price->variant == "") {
                                    $errors .= "Variant is required,";
                                }
                                $temp_price->price = $val[2];
                                if ($temp_price->price == "") {
                                    $errors .= "Price is required,";
                                }
                                $temp_price->discount_price = $val[3];

                                if ($errors) {
                                    $temp_price->error_flag = "Y";
                                    $temp_price->errors = $errors;
                                } else {
                                    $temp_price->error_flag = "N";
                                }
                                if (!empty($temp_price->sku)) {
                                    $pdt_data = TempPrice::where('sku', '=', $temp_price->sku)->where('variant', '=', $temp_price->variant)->first();

                                    if (!empty($pdt_data)) {
                                        $temp_price->duplicate_flag = 'Y';
                                    }
                                }
                                $temp_price->created_at = Carbon::now();

                                $temp_price->save();
                            }
                        }
                    }
                    return response()->json(['status' => '1', 'type' => $type]);
                } else {
                    return response()->json(['message' => $validator->errors()->first(), 'status' => '0']);
                }
            }
        }
    }
    public function priceSimple(Request $req)
    {
        $search = $req->search;
        $error_count  = $success_count = $duplicate_count = 0;
        $result_data = array();
        $pagination_count = 10;
        $query = TempPrice::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->where('sku', 'like', "%" . $search . "%");
            });
        }
        $temp_data = $query->where('cron_flag', 'I')->paginate($pagination_count)->appends(request()->query());

        if (count($temp_data) > 0) {
            foreach ($temp_data as $row_data) {
                $sku_data = Product::where('sku', '=', $row_data->sku)->where('supplier_id', '=', auth()->user()->id)->first();

                if (empty($sku_data)) {
                    $row_data->error_flag = 'Y';
                }
                if (!empty($sku_data)) {
                    if ($sku_data->product_type != 'simple') {
                        $row_data->error_flag = 'Y';
                    }
                }
                if (!empty($row_data->price) && !empty($row_data->discount_price)) {
                    if ($row_data->price <= $row_data->discount_price) {
                        $row_data->error_flag = 'Y';
                    }
                }


                if ($row_data->error_flag == 'Y') {
                    $error_count++;
                }
                if ($row_data->error_flag == 'N') {
                    $pdt_data = Product::where('sku', '=', $row_data->sku)->where('supplier_id', '=', auth()->user()->id)->first();
                    if (!empty($pdt_data)) {
                        $row_data->duplicate_flag = 'Y';
                        $duplicate_count++;
                    }
                }

                if ($row_data->error_flag == 'N' && $row_data->duplicate_flag == 'N') {
                    $success_count++;
                }
                $result[] = $row_data;
            }
            $result_data = $result;
        }

        $total_count = $error_count + $success_count + $duplicate_count;
        $data = [
            'temp_data' => $temp_data,
            'result_data' => $result_data,
            'error_count' => $error_count,
            'duplicate_count' => $duplicate_count,
            'success_count' => $success_count,
            'total_count' => $total_count,
            'search' => $search
        ];

        return view('supplier.product.price_updates.price_simple', $data);
    }
    
    public function SimpleErrors(Request $req)
    {

        $id = $req->id;
        $result = TempPrice::where('id', $id)->first();

        $sku_data = Product::where('sku', '=', $result->sku)->where('supplier_id', '=', auth()->user()->id)->first();

        $new_errors = '';
        if (!empty($result->sku)) {
            if (empty($sku_data)) {

                $new_errors .= "SKU not exist ,";
            }
        }
        if (!empty($sku_data)) {
            if ($sku_data->product_type != 'simple') {
                $new_errors .= "Imported product is not simple ,";
            }
        }
        if (!empty($result->price) && !empty($result->discount_price)) {
            if (($result->price) <= ($result->discount_price)) {
                $new_errors .= "Discount is greater than price,";
            }
        }

        $all_errors = $result->errors . $new_errors;
        $errors = [];
        $errors = explode(",", $all_errors);

        $data = ['errors' => $errors];

        return view('supplier.product.price_updates.errors', $data);
    }
    
     public function confirmSimple()
    {

        $result_data = array();

        $result_data = TempPrice::where('cron_flag', 'I')->get();

        if (count($result_data) > 0) {
            foreach ($result_data as $row_data) {
                if ($row_data->error_flag == 'N') {
                    $sku_data = Product::where('sku', '=', $row_data->sku)->where('supplier_id', '=', auth()->user()->id)->first();
                    if (empty($sku_data)) {
                        $row_data->error_flag = 'Y';
                    } else {
                        if ($sku_data->product_type != 'simple') {
                            $row_data->error_flag = 'Y';
                        } else {
                            $row_data->sku = $row_data->sku;
                        }
                    }
                    if (!empty($row_data->price) && !empty($row_data->discount_price)) {
                        if (($row_data->price) <= ($row_data->discount_price)) {
                            $row_data->error_flag = 'Y';
                        }
                    }

                    if ($row_data->error_flag == 'N') {
                        $pdt_data = Product::where('sku', '=', $row_data->sku)->where('supplier_id', '=', auth()->user()->id)->first();
                        $product_price = ProductPrice::where('product_id', $pdt_data->id)->first();
                        if ($row_data->discount_price) {
                                $discount_price = $row_data->price - ($row_data->price * $row_data->discount_price / 100);
                            } else {
                                $discount_price = NULL;
                        }
                        $data_to_save = [
                            'price' => $row_data->price,
                            'discount' => $row_data->discount_price,
                            'discount_price' => $discount_price,
                        ];
                        $saved_data = ProductPrice::where('product_id', $pdt_data->id)
                            ->update($data_to_save);
                    }
                }
                TempPrice::where('id', $row_data->id)->update(['cron_flag' => 'S', 'error_flag' => $row_data->error_flag, 'duplicate_flag' => $row_data->duplicate_flag]);
            }
        }
        $check_temp_emp = TempPrice::where('cron_flag', 'I')->first();
        if (!$check_temp_emp) {
            TempPrice::where('cron_flag', 'S')->update(['cron_flag' => 'C']);
        }

        return response()->json(['message' => 'Thank you! Successfully updated price list', 'status' => '1']);
    }
    
    public function importedSimple(Request $req)
    {
        $search = $req->search;
        $error_count = $success_count = $duplicate_count = 0;
        $pagination_count = 10;
        $result_data = array();
        $query = TempPrice::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->where('sku', 'like', "%" . $search . "%");
            });
        }
        $result_data = $query->where('cron_flag', 'C')
            ->paginate($pagination_count)->appends(request()->query());

        if (count($result_data) > 0) {
            foreach ($result_data as $row_data) {
                if ($row_data->error_flag == 'Y') {
                    $error_count++;
                } else {
                    $success_count++;
                }
            }
        }
        $total_count = $error_count + $success_count;
        $data = [
            'result_data' => $result_data,
            'error_count' => $error_count,
            'success_count' => $success_count,
            'total_count' => $total_count,
            'search' => $search,
        ];

        return view('supplier.product.price_updates.simple_imported', $data);
    }
    
     public function priceComplex(Request $req)
    {
        $search = $req->search;
        $error_count  = $success_count = $duplicate_count = 0;
        $result_data = array();
        $pagination_count = 10;
        $query = TempPrice::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->where('sku', 'like', "%" . $search . "%");
            });
        }
        $temp_data = $query->where('cron_flag', 'I')->paginate($pagination_count)->appends(request()->query());

        if (count($temp_data) > 0) {
            foreach ($temp_data as $row_data) {
                $sku_data = Product::where('sku', '=', $row_data->sku)->where('supplier_id', '=', auth()->user()->id)->first();

                if (empty($sku_data)) {
                    $row_data->error_flag = 'Y';
                }

                if (!empty($row_data->price) && !empty($row_data->discount_price)) {
                    if ($row_data->price <= $row_data->discount_price) {
                        $row_data->error_flag = 'Y';
                    }
                }
                if (!empty($sku_data)) {
                    if ($sku_data->product_type != 'complex') {
                        $row_data->error_flag = 'Y';
                    }
                    $skdata = Product::where('sku', '=', $row_data->sku)->where('supplier_id', '=', auth()->user()->id)->first();
                    if (!empty($skdata)) {
                        $varient_da = ProductPrice::where('product_id', '=', $skdata->id)->get();
                        if (!empty($varient_da)) {
                            $val = "";
                            foreach ($varient_da as $varient_data) {

                                $varient = VariantLang::where('id', $varient_data->variant_lang_id)->where('attribute_id', $varient_data->attribute_id)->first();
                                if (!empty($varient)) {

                                    if ((strtolower($varient->name) === strtolower($row_data->variant))) {
                                        $val = "exist";
                                        break;
                                    }
                                }
                            }

                            if ($val != "exist") {

                                $row_data->error_flag = 'Y';
                            }
                        }
                    } else {
                        $row_data->error_flag = 'Y';
                    }
                }

                if ($row_data->error_flag == 'Y') {
                    $error_count++;
                }
                if ($row_data->error_flag == 'N') {
                    $pdt_data = Product::where('sku', '=', $row_data->sku)->where('supplier_id', '=', auth()->user()->id)->first();
                    if (!empty($pdt_data)) {
                        $row_data->duplicate_flag = 'Y';
                        $duplicate_count++;
                    }
                }

                if ($row_data->error_flag == 'N' && $row_data->duplicate_flag == 'N') {
                    $success_count++;
                }
                $result[] = $row_data;
            }
            $result_data = $result;
        }

        $total_count = $error_count + $success_count + $duplicate_count;
        $data = [
            'temp_data' => $temp_data,
            'result_data' => $result_data,
            'error_count' => $error_count,
            'duplicate_count' =>$duplicate_count,
            'success_count' => $success_count,
            'total_count' => $total_count,
            'search' => $search
        ];

        return view('supplier.product.price_updates.price_complex', $data);
    }
    public function ComplexErrors(Request $req)
    {

        $id = $req->id;
        $result = TempPrice::where('id', $id)->first();

        $sku_data = Product::where('sku', '=', $result->sku)->where('supplier_id', '=', auth()->user()->id)->first();

        $new_errors = '';
        if (!empty($result->sku)) {
            if (empty($sku_data)) {

                $new_errors .= "SKU not exist ,";
            }
        }
        if (!empty($result->price) && !empty($result->discount_price)) {
            if (($result->price) <= ($result->discount_price)) {
                $new_errors .= "Discount is greater than price,";
            }
        }
        if (!empty($sku_data)) {
            if ($sku_data->product_type != 'complex') {
                $new_errors .= "Imported product is not complex ,";
            }
            $varient_da = ProductPrice::where('product_id', '=', $sku_data->id)->get();
            if (!empty($varient_da) && $sku_data->product_type == 'complex') {
                $val = "";
                foreach ($varient_da as $varient_data) {
                    $varient = VariantLang::where('id', $varient_data->variant_lang_id)->where('attribute_id', $varient_data->attribute_id)->first();
                                
                    if (!empty($varient)) {

                        if ((strtolower($varient->name) === strtolower($result->variant))) {
                            $val = "exist";
                            break;
                        }
                    }
                }

                if ($val != "exist") {

                    $new_errors .= "SKU and Variant mismatch,";
                }
            }
        }

        $all_errors = $result->errors . $new_errors;
        $errors = [];
        $errors = explode(",", $all_errors);

        $data = ['errors' => $errors];

        return view('supplier.product.price_updates.errors', $data);
    }
    
    public function confirmComplex()
    {

        $result_data = array();
        $result_data = TempPrice::where('cron_flag', 'I')->get();
        if (count($result_data) > 0) {
            foreach ($result_data as $row_data) {
                if ($row_data->error_flag == 'N') {
                    $sku_data = Product::where('sku', '=', $row_data->sku)->where('supplier_id', '=', auth()->user()->id)->first();
                    if (empty($sku_data)) {
                        $row_data->error_flag = 'Y';
                    } else {
                        $varient_da = ProductPrice::where('product_id', '=', $sku_data->id)->get();

                        if (!empty($varient_da)) {
                            $val = '';
                            foreach ($varient_da as $varient_data) {
                                $varient = VariantLang::where('id', $varient_data->variant_lang_id)->where('attribute_id', $varient_data->attribute_id)->first();
                                if (!empty($varient)) {

                                    if ((strtolower($varient->name) === strtolower($row_data->variant))) {
                                        $val = "exist";
                                        break;
                                    }
                                }
                            }

                            if ($val != "exist") {

                                $row_data->error_flag = 'Y';
                            } else {
                                $row_data->sku = $row_data->sku;
                            }
                        }
                    }
                    if (!empty($row_data->price) && !empty($row_data->discount_price)) {
                        if (($row_data->price) <= ($row_data->discount_price)) {
                            $row_data->error_flag = 'Y';
                        }
                    }

                    if ($row_data->error_flag == 'N') {
                        $product_price = ProductPrice::where('product_id', $sku_data->id)->where('variant_lang_id', $varient->id)->first();
                        if ($row_data->discount_price) {
                                $discount_price = $row_data->price - ($row_data->price * $row_data->discount_price / 100);
                            } else {
                                $discount_price = NULL;
                        }
                        $data_to_save = [
                            'price' => $row_data->price,
                            'discount' => $row_data->discount_price,
                            'discount_price' => $discount_price
                        ];
                        $saved_data = ProductPrice::where('product_id', $sku_data->id)->where('variant_lang_id', $varient->id)
                            ->update($data_to_save);
                    }
                }
                TempPrice::where('id', $row_data->id)->update(['cron_flag' => 'S', 'error_flag' => $row_data->error_flag, 'duplicate_flag' => $row_data->duplicate_flag]);
            }
        }
        $check_temp_emp = TempPrice::where('cron_flag', 'I')->first();
        if (!$check_temp_emp) {
            TempPrice::where('cron_flag', 'S')->update(['cron_flag' => 'C']);
        }

        return response()->json(['message' => 'Thank you! Successfully updated price list', 'status' => '1']);
    }
    
     public function importedComplex(Request $req)
    {
        $search = $req->search;
        $error_count = $success_count = $duplicate_count = 0;
        $pagination_count = 10;
        $result_data = array();
        $query = TempPrice::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->where('sku', 'like', "%" . $search . "%");
            });
        }
        $result_data = $query->where('cron_flag', 'C')
            ->paginate($pagination_count)->appends(request()->query());

        if (count($result_data) > 0) {
            foreach ($result_data as $row_data) {
                if ($row_data->error_flag == 'Y') {
                    $error_count++;
                } else {
                    $success_count++;
                }
            }
        }
        $total_count = $error_count + $success_count;
        $data = [
            'result_data' => $result_data,
            'error_count' => $error_count,
            'success_count' => $success_count,
            'total_count' => $total_count,
            'search' => $search,
        ];


        return view('supplier.product.price_updates.complex_imported', $data);
    }

}
