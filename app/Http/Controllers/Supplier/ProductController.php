<?php

namespace App\Http\Controllers\Supplier;

use App\Ads;
use App\Slider;
use Carbon\Carbon;
use App\Helpers\Helper;
use App\Models\Product;
use App\Models\Category;
use App\Models\Supplier;
use App\Models\SupplierAnnex;
use App\Models\Attribute;
use App\Models\BrandLang;
use App\Models\BranchAnnex;
use App\Models\ProductLang;
use App\Models\CategoryLang;
use App\Models\ProductPrice;
use Illuminate\Http\Request;
use App\Models\ProductImages;
use App\Models\SupplierBrand;
use App\Models\ProductVariant;
use App\Models\ProductAttribute;
use App\Models\SupplierCategory;
use App\Models\ProductCategories;
use App\Models\CategoryAttributes;
use App\Models\ProductBranchStock;
use App\Models\PromotionalProduct;
use App\Models\RecentlyViewedProducts;
use App\Models\ProductViewCount;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller {

    public function get(Request $req) {
        $search = $req->search;
        $category_id = $req->cat_id;
        // $product_type = $req->product_type;
        $query = Product::with('lang');
        if ($search) {
            $query->whereHas('lang', function ($query) use ($search) {
                $query->where('name', 'like', "%" . $search . "%");
                $query->orWhere('sku', 'like', "%" . $search . "%");
            });
        }
        if ($req->cat_id) {
            $query->where('category_id', '=', $req->cat_id);
        }
        if ($req->product_type) {
            $query->where('product_type', '=', $req->product_type);
        }
        $pdt_data = $query->where('deleted_at', null)
                ->where('supplier_id', auth()->user()->id)
                ->orderBy('id', 'desc')
                ->paginate(20);


        $cat_data = Category::with(['lang' => function ($query) {
                        $query->where('language', 'en');
                    }])
                ->where('parent_id', NULL)
                ->get();
        $supplier = auth()->user()->code;
        return view('supplier.product.products.index', compact('pdt_data', 'cat_data', 'search', 'category_id', 'supplier'));
    }

    public function create(Request $req) {
        $_product_id = $req->id;
        $cat_id = $req->cat_id ? $req->cat_id : NULL;
        $row_data = $lang_array = $product_data = array();
        $selected_category = SupplierCategory::where('supplier_id', auth()->user()->id)
                        ->get('category_id')->toArray();
        $cat_data = Category::with(['lang' => function ($query) {
                        $query->where('language', 'en');
                    }])->where('parent_id', NULL)->whereIn('id', $selected_category)->get();
        $selected_brand = SupplierBrand::where('supplier_id', auth()->user()->id)
                        ->get('brand_id')->toArray();
        $brand_data = BrandLang::where(['language' => 'en'])
                        ->whereIn('brand_id', $selected_brand)->get();
        $is_variant = Attribute::where(['is_variant' => 'yes'])->exists();
        $supplier = Supplier::where(['role_id' => 5])->get();
        if ($_product_id) {
            $product_data = Product::with('lang')->with('category')->with('brand')->where('id', $_product_id)->first();
            $sku = $product_data->sku;
            $selected_categories = str_replace('{{' . $product_data->main_category . '}}', '', $product_data->category_ids);
            $selected_categories = str_replace('{{' . $product_data->sub_category . '}}', '', $selected_categories);
            $selected_categories = str_replace('}}{{', ',', $selected_categories);
            $selected_categories = str_replace('{{', ' ', $selected_categories);
            $selected_categories = str_replace('}}', ' ', $selected_categories);
            $cat_ids = explode(",", $selected_categories);
            $sub_cat = Category::with('lang')->where(['parent_id' => $product_data->main_category])->whereIn('id', $selected_category)->get();
            
        } else {
            $supp_data = Supplier::where('id', '=', auth()->user()->id)->first();
            $gen_sku = $this->generateSku();
            $sku = $supp_data->code.'-'.$gen_sku;
            $cat_ids = '';
            $sub_cat = '';
        }
        $multiple_cat = Category::with('lang')->where(['status' => 'active'])->whereIn('id', $selected_category)->get();
        $data = [
            'row_data' => $row_data,
            'cat_data' => $cat_data,
            'sub_cat' => $sub_cat,
            'multiple_cat' => $multiple_cat,
            'brand_data' => $brand_data,
            'sku' => $sku,
            'row_data' => $product_data,
            'is_variant' => $is_variant,
            'cat_ids' => $cat_ids,
            'supplier' => $supplier,
        ];
        return view('supplier.product.products.create', $data);
    }

    public function storeProductInfo(Request $req) {
        if ($req->pdt_id) {
            $unique = ',' . $req->pdt_id;
        } else {
            $unique = ',NULL';
        }
        $rules = [
            'sku' => 'required|unique:product,sku' . $unique . ',id,deleted_at,NULL',
            'name_en' => 'required|unique:product_i18n,name' . $unique . ',product_id,language,en',
            'name_ar' => 'required',
            'category' => 'required',
            'brand' => 'required',
            'description_en' => 'required',
            'description_ar' => 'required',
        ];
        $messages = [
            'name_en.unique' => 'Product already exist.',
            'sku.required' => 'SKU is required.',
            'sku.unique' => 'Product with same sku already exists',
            'name_en.required' => 'Product Name(EN) is required.',
            'name_ar.required' => 'Product Name(AR) is required.',
            'category.required' => 'Category is required.',
            'brand.required' => 'Brand is required.',
            'supplier.required' => 'Brand is required.',
            'description_en.required' => 'Description(EN) is required.',
            'description_ar.required' => 'Description(AR) is required.',
        ];


        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            if ($req->product_type == 'complex') {
                $pdt_att_var = Attribute::with('lang')->leftJoin('category_attributes', 'category_attributes.attribute_id', '=', 'attribute.id')
                        ->select('attribute.*')
                        ->where('category_attributes.category_id', '=', $req->category)
                        ->where('attribute.is_variant', 'yes')
                        ->where('attribute.attribute_type', '=', 'dropdown')
                        ->groupBy('category_attributes.attribute_id')
                        ->first();
                if (empty($pdt_att_var)) {
                    return response()->json(['status' => 0, 'message' => 'System not allowed to add complex products under this category <br/>since no variant attributes found under this category']);
                }
            }

            $supp_commission = SupplierAnnex::where(['supplier_id' => auth()->user()->id])->pluck('commision_category_id')->first();
            if (empty($req->pdt_id)) {
                $product = DB::transaction(function () use ($req, $supp_commission) {
                            $product = Product::create([
                                        'category_id' => $req->category,
                                        'brand_id' => $req->brand,
                                        'supplier_id' => auth()->user()->id,
                                        'commision_percentage' => $supp_commission,
                                        'main_category' => $req->main_category,
                                        'sub_category' => $req->sub_category,
                                        'sku' => $req->sku,
                                        'status' => 'deactive',
                                        'supplier_id' => auth()->user()->id,
                                        'product_type' => $req->product_type,
                                        'created_at' => Carbon::now(),
                                        'slug' => $this->createSlug($req->name_en)
                            ]);

                            $product->lang()->createMany([
                                [
                                    'name' => $req->name_en,
                                    'description' => $req->description_en,
                                    'ingredients' => $req->ingredients_en,
                                    'how_to_use' => $req->how_to_use_en,
                                    'reasons_to_buy' => $req->reasons_to_buy_en,
                                    'language' => 'en',
                                ],
                                [
                                    'name' => $req->name_ar,
                                    'description' => $req->description_ar,
                                    'ingredients' => $req->ingredients_ar,
                                    'how_to_use' => $req->how_to_use_ar,
                                    'reasons_to_buy' => $req->reasons_to_buy_ar,
                                    'language' => 'ar',
                                ],
                            ]);

                            return $product;
                        });
                $msg = __('messages.admin.product_info_add_msg');
            } else {
                $product = DB::transaction(function () use ($req, $supp_commission) {
                            $product = Product::where('id', $req->pdt_id)
                                    ->update([
                                'category_id' => $req->category,
                                'brand_id' => $req->brand,
                                'supplier_id' => auth()->user()->id,
                                'commision_percentage' => $supp_commission,
                                'sku' => $req->sku,        
                                'main_category' => $req->main_category,
                                'sub_category' => $req->sub_category,
                                'product_type' => $req->product_type,
                                'slug' => $this->createSlug($req->name_en)
                            ]);
                            ProductLang::where('language', 'en')
                                    ->where('product_id', $req->pdt_id)
                                    ->update([
                                        'name' => $req->name_en,
                                        'description' => $req->description_en,
                                        'ingredients' => $req->ingredients_en,
                                        'how_to_use' => $req->how_to_use_en,
                                        'reasons_to_buy' => $req->reasons_to_buy_en,
                            ]);
                            ProductLang::where('language', 'ar')
                                    ->where('product_id', $req->pdt_id)
                                    ->update([
                                        'name' => $req->name_ar,
                                        'description' => $req->description_ar,
                                        'ingredients' => $req->ingredients_ar,
                                        'how_to_use' => $req->how_to_use_ar,
                                        'reasons_to_buy' => $req->reasons_to_buy_ar,
                            ]);

                            return $product;
                        });
                $msg = __('messages.admin.product_info_update_msg');
            }
            if ($product) {
                $pdt_id = !empty($product->id) ? $product->id : $req->pdt_id;
                //For multiple category
                $cat_ids = '';
                $cat_ids = '';
                $sec_category = $req->input('sub_sub_category');
                if (ProductCategories::where('product_id', $pdt_id)->exists()) {
                    ProductCategories::where('product_id', $pdt_id)->delete();
                }
                $main_category = $req->main_category;
                $sub_category = $req->sub_category;
                $sec_category[] = $main_category;
                $sec_category[] = $sub_category;
                if ($sec_category) {
                    foreach ($sec_category as $value) {
                        $cat_ids .= '{{' . $value . '}}';
                        $saved = ProductCategories::create([

                                    'product_id' => $pdt_id,
                                    'category_id' => $value
                        ]);
                    }
                    $product_categories = Product::where('id', $pdt_id)
                            ->update([
                        'category_ids' => $cat_ids,
                    ]);
                }
                
                if(auth()->user()->id){
                    $region_ids = '';
                    $supplier_data = Supplier::where('id', '=', auth()->user()->id)->first();
                    $regions[] = $supplier_data->region_id;
                    $branch_data = BranchAnnex::with(['supplier' => function($query){
                                $query->where('role_id', 6);
                            }])->where('supplier_parent_id', auth()->user()->id)->get();
                            foreach ($branch_data as $value) {
                               $regions[] = $value->supplier->region_id; 
                            }
                    $regions = array_unique($regions);
                    foreach ($regions as $reg_value) {
                        $region_ids .= '{{' . $reg_value . '}}';
                    }
                    
                    Product::where('id', $pdt_id)
                            ->update([
                        'supplier_branch_region' => $region_ids,
                    ]);
                    
                }
                //end multiple category
                $pdt_att = $pdt_att_var = array();
                $product_type = $req->product_type ? $req->product_type : 'simple';


                $pdt_att = CategoryAttributes::with(['cat_attributes' => function ($query) {
                                $query->where('is_variant', 'no')
                                        ->orderByRaw("FIELD(attribute_type, 'textbox', 'textarea', 'dropdown')");
                            }])->where('category_id', '=', $req->category)->get();
                if ($product_type == 'complex') {
                    $pdt_att_var = Attribute::with('lang')->leftJoin('category_attributes', 'category_attributes.attribute_id', '=', 'attribute.id')
                            ->select('attribute.*')
                            ->where('category_attributes.category_id', '=', $req->category)
                            ->where('attribute.is_variant', 'yes')
                            ->where('attribute.attribute_type', '=', 'dropdown')
                            ->groupBy('category_attributes.attribute_id')
                            ->first();
                }
            }
            $html = view('supplier.product.products.attribute_details')->with(compact('pdt_id', 'pdt_att', 'pdt_att_var', 'product_type'))->render();
            return response()->json(['status' => 1, 'message' => $msg, 'result' => $html]);
        }
    }

    public function generateSku() {
        $lastsku = Product::select('sku')
                ->orderBy('id', 'desc')
                ->first();

        $lastId = 0;

        if ($lastsku) {
            $lastId = strstr($lastsku->sku, 'SKU-');
            $lastId = str_replace('SKU-', '', $lastId);
        }
        $lastId++;
        return 'SKU-' . str_pad($lastId, 5, '0', STR_PAD_LEFT);
    }

    private function createSlug($string) {
        $slug = strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'), array('', '-', ''), trim($string)));
        return $slug;
    }

    public function storeProductAttribute(Request $req) {
        $pdt_data = Product::where(['id' => $req->pdt_id])->first();
        $rules = [];
        $messages = [];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            if ($req->pdt_id) {
                $flag = 0;
                $pdt_att = ProductAttribute::with('lang')->with('attribute')->with('product')
                        ->where('product_id', $req->pdt_id)
                        ->get();
                if (count($pdt_att) > 0) {
                    $flag = 1;
                    foreach ($pdt_att as $row_data_val) {
                        $pdt_att_value = ProductAttribute::find($row_data_val->id);

                        if (!empty($pdt_att_value)) {
                            $pdt_att_value->lang()->delete();
                            $pdt_att_value->delete();
                        }
                    }
                }
                $pdt_var_count = ProductVariant::with('attribute')->with('variant')
                        ->where('product_id', $req->pdt_id)
                        ->get();
                if (count($pdt_var_count) > 0) {
                    $flag = 1;
                }
                $attribute = DB::transaction(function () use ($req) {
                            $attribute = 1;
                            $attribute_id = $req->attribute_id;
                            $i = 0;
                            if (!empty($attribute_id)) {
                                foreach ($attribute_id as $att_id) {
                                    $att_value_type = $req->attribute_type[$i];
                                    $varinat_lang_id = 'varinat_lang_' . $att_id . '_id' ? 'varinat_lang_' . $att_id . '_id' : NULL;
                                    $variant_id = 'variant_id' . $att_id ? 'variant_id' . $att_id : NULL;
                                    $attribute = ProductAttribute::create([
                                                'product_id' => $req->pdt_id,
                                                'attribute_id' => $att_id,
                                                'attribute_type' => $att_value_type,
                                                'variant_id' => $req->$variant_id,
                                                'variant_lang_id' => $req->$varinat_lang_id,
                                                'created_at' => Carbon::now()
                                    ]);

                                    if ($att_value_type == 'textbox' || $att_value_type == 'textarea') {
                                        $name_en = 'att_name_' . $att_id . '_en';
                                        $name_ar = 'att_name_' . $att_id . '_ar';
                                        $attribute->lang()->createMany([
                                            [
                                                'name' => $req->$name_en,
                                                'language' => 'en',
                                            ],
                                            [
                                                'name' => $req->$name_ar,
                                                'language' => 'ar',
                                            ],
                                        ]);
                                    }
                                    $i++;
                                }
                            }
                            if ($req->product_type == 'complex' && !empty($req->var_attribute_id)) {
                                $var_ids = array();
                                $variant_lang_id = $req->variant_lang_id;
                                if (!empty($variant_lang_id)) {
                                    foreach ($variant_lang_id as $key => $lang_id) {
                                        if (!empty($lang_id)) {
                                            $pdt_var = ProductVariant::where('product_id', $req->pdt_id)->where('variant_id', $req->variant_id)->where('variant_lang_id', $lang_id)->first();
                                            if (empty($pdt_var)) {
                                                $attribute = $pdt_variant = ProductVariant::create([
                                                            'attribute_id' => $req->var_attribute_id,
                                                            'variant_id' => $req->variant_id,
                                                            'product_id' => $req->pdt_id,
                                                            'variant_lang_id' => $lang_id,
                                                            'created_at' => Carbon::now()
                                                ]);
                                                $var_ids[] = $pdt_variant->id;
                                            } else {
                                                $attribute = ProductVariant::where('product_id', $req->pdt_id)->where('variant_id', $req->variant_id)->where('variant_lang_id', $lang_id)
                                                        ->update([
                                                    'attribute_id' => $req->var_attribute_id,
                                                    'variant_id' => $req->variant_id,
                                                    'product_id' => $req->pdt_id,
                                                    'variant_lang_id' => $lang_id,
                                                ]);
                                                $var_ids[] = $pdt_var->id;
                                            }
                                        }
                                    }
                                    if (!empty($var_ids)) {
                                        ProductVariant::whereNotIn('id', $var_ids)->where(['product_id' => $req->pdt_id, 'variant_id' => $req->variant_id])->delete();
                                    }
                                }
                            }

                            return $attribute;
                        });
            }
            if ($flag == 1) {
                $msg = __('messages.admin.attribute_update_msg');
            } else {
                $msg = __('messages.admin.attribute_add_msg');
            }
        }
        if ($attribute) {
            $pdt_id = $req->pdt_id;
            $pdt_data = Product::where(['id' => $req->pdt_id])->first();
            $product_type = $pdt_data->product_type;
            $pdt_att_var = array();
            if ($product_type == 'complex') {
                $pdt_att_var = Attribute::with('lang')->leftJoin('category_attributes', 'category_attributes.attribute_id', '=', 'attribute.id')
                        ->select('attribute.*')
                        ->where('category_attributes.category_id', '=', $pdt_data->category_id)
                        ->where('attribute.is_variant', 'yes')
                        ->where('attribute.attribute_type', '=', 'dropdown')
                        ->groupBy('category_attributes.attribute_id')
                        ->first();
            }
            $supplier_data = Supplier::where('id', $pdt_data->supplier_id)->first();
            $branch_data = BranchAnnex::where('supplier_parent_id', $pdt_data->supplier_id)->get();

            $html = view('supplier.product.products.product_stock')->with(compact('pdt_id', 'pdt_att_var', 'product_type', 'supplier_data', 'branch_data'))->render();
            return response()->json(['status' => 1, 'message' => $msg, 'result' => $html]);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }

    public function get_tabs(Request $req) {
        $product_type = Product::where('id', $req->pdt_id)->pluck('product_type');
        $product_type = $product_type[0];
        $pdt_id = $req->pdt_id;
        if ($req->activeTab == '#pdt_info') {
            $selected_category = SupplierCategory::where('supplier_id', auth()->user()->id)
                            ->get('category_id')->toArray();
            $cat_data = Category::with(['lang' => function ($query) {
                            $query->where('language', 'en');
                        }])->where('parent_id', NULL)->whereIn('id', $selected_category)->get();
            $selected_brand = SupplierBrand::where('supplier_id', auth()->user()->id)
                            ->get('brand_id')->toArray();
            $brand_data = BrandLang::where(['language' => 'en'])
                            ->whereIn('brand_id', $selected_brand)->get();
            $pdt_id = $req->pdt_id;
            $sku = $cat_ids = '';
            if ($req->pdt_id) {
                $row_data = Product::with('lang')->with('category')->with('brand')->where('id', $req->pdt_id)->first();
                $selected_categories = str_replace('{{' . $row_data->main_category . '}}', '', $row_data->category_ids);
                $selected_categories = str_replace('{{' . $row_data->sub_category . '}}', '', $selected_categories);
                $selected_categories = str_replace('}}{{', ',', $selected_categories);
                $selected_categories = str_replace('{{', ' ', $selected_categories);
                $selected_categories = str_replace('}}', ' ', $selected_categories);
                $cat_ids = explode(",", $selected_categories);
                $sub_cat = Category::with('lang')->where(['parent_id' => $row_data->main_category])->whereIn('id', $selected_category)->get();
                $multiple_cat = Category::with('lang')->where(['status' => 'active'])->whereIn('id', $selected_category)->get();
            }
            $is_variant = Attribute::where(['is_variant' => 'yes'])->exists();
            return view('supplier.product.products.basic_details', compact('cat_data', 'multiple_cat', 'brand_data', 'pdt_id', 'row_data', 'sku', 'is_variant', 'cat_ids', 'sub_cat'));
        } elseif ($req->activeTab == '#attributes') {
            $pdt_id = $req->pdt_id;
            $pdt_att = $pdt_att_var = array();
            $pdt_data = Product::where(['id' => $req->pdt_id])->first();
            $product_type = $pdt_data->product_type ? $pdt_data->product_type : 'simple';
            $pdt_att = CategoryAttributes::with(['cat_attributes' => function ($query) {
                            $query->where('is_variant', 'no')
                                    ->orderByRaw("FIELD(attribute_type, 'textbox', 'textarea', 'dropdown')");
                        }])->where('category_id', '=', $pdt_data->category_id)->get();
            if ($product_type == 'complex') {
                $pdt_att_var = Attribute::with('lang')->leftJoin('category_attributes', 'category_attributes.attribute_id', '=', 'attribute.id')
                        ->select('attribute.*')
                        ->where('category_attributes.category_id', '=', $pdt_data->category_id)
                        ->where('attribute.is_variant', 'yes')
                        ->where('attribute.attribute_type', '=', 'dropdown')
                        ->groupBy('category_attributes.attribute_id')
                        ->first();
            }
            return view('supplier.product.products.attribute_details')->with(compact('pdt_id', 'pdt_att', 'pdt_att_var', 'product_type'))->render();
        } elseif ($req->activeTab == '#stock') {
            $pdt_id = $req->pdt_id;
            $pdt_att_var = $pdt_att_row_data = array();
            $pdt_data = Product::where(['id' => $req->pdt_id])->first();
            $product_type = $pdt_data->product_type ? $pdt_data->product_type : 'simple';
            $pdt_att_var = array();
            if ($product_type == 'complex') {
                $pdt_att_var = Attribute::with('lang')->leftJoin('category_attributes', 'category_attributes.attribute_id', '=', 'attribute.id')
                        ->select('attribute.*')
                        ->where('category_attributes.category_id', '=', $pdt_data->category_id)
                        ->where('attribute.is_variant', 'yes')
                        ->where('attribute.attribute_type', '=', 'dropdown')
                        ->groupBy('category_attributes.attribute_id')
                        ->first();
            }

            $supplier_data = Supplier::where('id', $pdt_data->supplier_id)->first();
            $branch_data = BranchAnnex::where('supplier_parent_id', $pdt_data->supplier_id)->get();

            return view('supplier.product.products.product_stock')->with(compact('pdt_id', 'pdt_att_var', 'product_type', 'supplier_data', 'branch_data'))->render();
        } elseif ($req->activeTab == '#price') {
            $pdt_id = $req->pdt_id;
            $pdt_data = Product::where(['id' => $req->pdt_id])->first();
            $product_type = $pdt_data->product_type ? $pdt_data->product_type : 'simple';
            $pdt_att_var = array();
            if ($product_type == 'complex') {

                $pdt_att_var = Attribute::with('lang')->leftJoin('category_attributes', 'category_attributes.attribute_id', '=', 'attribute.id')
                        ->select('attribute.*')
                        ->where('category_attributes.category_id', '=', $pdt_data->category_id)
                        ->where('attribute.is_variant', 'yes')
                        ->where('attribute.attribute_type', '=', 'dropdown')
                        ->groupBy('category_attributes.attribute_id')
                        ->first();
            }
            $product_price = ProductPrice::where('product_id', $req->pdt_id)->first();
            $sub_cat_markup = Category::where(['id' => $pdt_data->sub_category])->pluck('markup_percentage')->first();
            if($sub_cat_markup){
            $easy_markup = $sub_cat_markup;
            }else{
            $main_cat_markup = Category::where(['id' => $pdt_data->main_category])->pluck('markup_percentage')->first();
            $easy_markup = $main_cat_markup;  
            }
            $easy_markup = $easy_markup ? $easy_markup : '';
            return view('supplier.product.products.product_price')->with(compact('pdt_id', 'pdt_att_var', 'product_type', 'product_price', 'easy_markup'))->render();
        } elseif ($req->activeTab == '#photos') {
            $pdt_data = Product::where(['id' => $pdt_id])->first();
            $cover_image = $pdt_data->cover_image ? $pdt_data->cover_image : '';
            $pdt_images = ProductImages::where(['product_id' => $pdt_id])->get();
            return view('supplier.product.products.product_images')->with(compact('pdt_id', 'pdt_images', 'cover_image'))->render();
        }
    }

    public function productStock(Request $req) {
        $pdt_data = Product::where(['id' => $req->pdt_id])->first();
        if ($req->product_type == 'simple') {
            $rules = [
                'max_stock.*' => 'required',
                'min_stock.*' => 'nullable|lt:max_stock.*',
            ];
            $messages = [
                'max_stock.required' => 'Max stock is required.',
                'lt' => 'Min stock must be less than the max stock',
            ];
        } else {
            $rules = [
                'max_stock_var.*' => 'required',
                'min_stock_var.*' => 'nullable|lt:max_stock_var.*',
            ];
            $messages = [
                'max_stock_var.required' => 'Max stock is required.',
                'lt' => 'Min stock must be less than the max stock',
            ];
        }
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            if ($req->pdt_id) {
                $flag = 0;
                $pdt_var_count = ProductVariant::with('attribute')->with('variant')
                        ->where('product_id', $req->pdt_id)
                        ->get();
                if (count($pdt_var_count) > 0) {
                    $flag = 1;
                }if ($pdt_data->product_type == 'simple') {
                    $variant_id = $req->product_variant_id;
                    if (!empty($variant_id)) {
                        $pdt_branch = ProductBranchStock::where('product_variant_stock_id', $variant_id)->get();
                        if (!empty($pdt_branch)) {
                            $pdt_branch->each->delete();
                        }
                    }
                    $pdt_var = ProductVariant::where('product_id', $req->pdt_id)->first();
                    $pdt_variant = '';
                    if (empty($pdt_var)) {
                        $pdt_variant = ProductVariant::create([
                                    'product_id' => $req->pdt_id,
                                    'supplier_id' => $pdt_data->supplier_id,
                                    'created_at' => Carbon::now()
                        ]);
                    } else {
                        ProductVariant::where('product_id', $req->pdt_id)->where('id', $pdt_var->id)
                                ->update([
                                    'product_id' => $req->pdt_id,
                                    'supplier_id' => $pdt_data->supplier_id,
                        ]);
                    }
                    $branches = $req->branch_id;
                    $product_variant_id = isset($pdt_var->id) ? $pdt_var->id : $pdt_variant->id;
                    foreach ($branches as $key => $branch_val) {
                        $branch_data = Supplier::select('region_id')->where('id', '=', $branch_val)->first();
                        $branch_stock = ProductBranchStock::create([
                                    'product_variant_stock_id' => $product_variant_id,
                                    'branch_id' => $branch_val,
                                    'region_id' => $branch_data->region_id,
                                    'min_stock' => $req->min_stock[$key],
                                    'max_stock' => $req->max_stock[$key],
                                    'created_at' => Carbon::now()
                        ]);
                    }
                } else {
                    $variant_ids = $req->product_variant_id;
                    $pdt_branch = ProductBranchStock::whereIn('product_variant_stock_id', $variant_ids)->get();
                    if (!empty($pdt_branch)) {
                        $pdt_branch->each->delete();
                    }
                    $pdt_variant = ProductVariant::whereIn('id', $variant_ids)
                            ->update([
                        'supplier_id' => $pdt_data->supplier_id,
                    ]);

                    $branches = $req->branch_id;
                    foreach ($branches as $key => $branch_val) {
                        $branch_data = Supplier::select('region_id')->where('id', '=', $branch_val)->first();
                        ProductBranchStock::create([
                            'product_variant_stock_id' => $req->product_variant_id[$key],
                            'branch_id' => $branch_val,
                            'region_id' => $branch_data->region_id,
                            'min_stock' => $req->min_stock_var[$key],
                            'max_stock' => $req->max_stock_var[$key],
                            'created_at' => Carbon::now()
                        ]);
                    }
                }
                if ($flag == 1) {
                    $msg = __('messages.admin.stock_update_msg');
                } else {
                    $msg = __('messages.admin.stock_add_msg');
                }

                $pdt_id = $req->pdt_id;
                $pdt_data = Product::where(['id' => $req->pdt_id])->first();
                $product_type = $pdt_data->product_type;
                $pdt_att_var = array();
                if ($product_type == 'complex') {
                    $pdt_att_var = Attribute::with('lang')->leftJoin('category_attributes', 'category_attributes.attribute_id', '=', 'attribute.id')
                            ->select('attribute.*')
                            ->where('category_attributes.category_id', '=', $pdt_data->category_id)
                            ->where('attribute.is_variant', 'yes')
                            ->where('attribute.attribute_type', '=', 'dropdown')
                            ->groupBy('category_attributes.attribute_id')
                            ->first();
                }
                $product_price = ProductPrice::where('product_id', $req->pdt_id)->first();
                $sub_cat_markup = Category::where(['id' => $pdt_data->sub_category])->pluck('markup_percentage')->first();
                if($sub_cat_markup){
                $easy_markup = $sub_cat_markup;
                }else{
                $main_cat_markup = Category::where(['id' => $pdt_data->main_category])->pluck('markup_percentage')->first();
                $easy_markup = $main_cat_markup;  
                }
                $easy_markup = $easy_markup ? $easy_markup : '';
                $html = view('supplier.product.products.product_price')->with(compact('pdt_id', 'pdt_att_var', 'product_type', 'product_price', 'easy_markup'))->render();
                return response()->json(['status' => 1, 'message' => $msg, 'result' => $html]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }

    public function productPrice(Request $req) {
        if ($req->product_type == 'simple') {
            $rules = [
                'price' => 'required',
            ];
            $messages = [
                'price.required' => 'Price is required.',
            ];
        } else {
            $rules = [
                'price_var.*' => 'required',
            ];
            $messages = [
                'price_var.required' => 'Price is required.',
            ];
        }
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $flag = 0;
            $pdt_id = $req->pdt_id;
            $product_price = ProductPrice::where('product_id', $req->pdt_id)->get();
            if (count($product_price) > 0) {
                $flag = 1;
            }
            $pdt_data = Product::where(['id' => $req->pdt_id])->first();
            if (!empty($pdt_data)) {
                if ($pdt_data->product_type == 'simple') {
                    $product_price = ProductPrice::where('product_id', $req->pdt_id)->first();
                    if ($req->discount) {
                        $discount_price = $req->price - ($req->price * $req->discount / 100);
                    } else {
                        $discount_price = NULL;
                    }
                    if (!empty($product_price)) {
                        ProductPrice::where(['product_id' => $req->pdt_id])
                                ->update([
                                    'price' => $req->price,
                                    'discount' => $req->discount,
                                    'discount_price' => $discount_price,
                                    'easify_markup' => $req->easify_markup
                        ]);
                    } else {
                        ProductPrice::create([
                            'product_id' => $req->pdt_id,
                            'price' => $req->price,
                            'discount' => $req->discount,
                            'discount_price' => $discount_price,
                            'easify_markup' => $req->easify_markup,
                            'created_at' => Carbon::now()
                        ]);
                    }
                } else {
                    if (!empty($req->attribute_id)) {
                        $variant_lang_id = $req->variant_lang_id;
                        $i = 0;
                        foreach ($variant_lang_id as $key => $lang_id) {
                            if (!empty($lang_id)) {
                                $product_price = ProductPrice::where(['product_id' => $req->pdt_id, 'variant_id' => $req->variant_id, 'variant_lang_id' => $lang_id])->count();
                            }
                            if ($req->discount_var[$key]) {
                                $discount_price = $req->price_var[$key] - ($req->price_var[$key] * $req->discount_var[$key] / 100);
                            } else {
                                $discount_price = NULL;
                            }
                            if ($product_price > 0) {
                                ProductPrice::where(['product_id' => $req->pdt_id, 'variant_id' => $req->variant_id, 'variant_lang_id' => $lang_id])
                                        ->update([
                                            'attribute_id' => $req->attribute_id,
                                            'variant_id' => $req->variant_id,
                                            'price' => $req->price_var[$key],
                                            'discount' => $req->discount_var[$key],
                                            'discount_price' => $discount_price,
                                            'easify_markup' => $req->easify_markup_var[$key]
                                ]);
                            } else {
                                $attribute = ProductPrice::create([
                                            'attribute_id' => $req->attribute_id,
                                            'variant_id' => $req->variant_id,
                                            'product_id' => $req->pdt_id,
                                            'variant_lang_id' => $lang_id,
                                            'price' => $req->price_var[$key],
                                            'discount' => $req->discount_var[$key],
                                            'discount_price' => $discount_price,
                                            'easify_markup' => $req->easify_markup_var[$key],
                                            'created_at' => Carbon::now()
                                ]);
                            }

                            $i++;
                        }
                    }
                }

                $product_type = $pdt_data->product_type;
                $cover_image = $pdt_data->cover_image ? $pdt_data->cover_image : '';
                $pdt_images = ProductImages::where(['product_id' => $pdt_id])->get();
                if ($flag == 1) {
                    $msg = __('messages.admin.price_update_msg');
                } else {
                    $msg = __('messages.admin.price_add_msg');
                }
                $html = view('supplier.product.products.product_images')->with(compact('pdt_id', 'product_type', 'cover_image', 'pdt_images'))->render();
                return response()->json(['status' => 1, 'message' => $msg, 'result' => $html]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }

    public function productImages(Request $req) {
        $pdt_id = $req->pdt_id;
        if ($req->upload_type == 'single') {
            $rules = [
                'photo' => 'required|image|mimes:png,jpg,jpeg|max:1052672'
            ];
            $messages = [
                'photo.max' => 'The image must be less than 2Mb in size',
                'photo.mimes' => "The image must be of the format jpeg or png",
            ];
        } else {
            $rules = [
                'photos' => ['required', 'array'
                ],
                'photos.*' => 'image|mimes:png,jpg,jpeg|max:1052672',
            ];
            $messages = [
                'photos.max' => 'The image must be less than 2Mb in size',
                'photos.mimes' => "The image must be of the format jpeg or png",
            ];
        }
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            if ($req->upload_type == 'single') {
                $file = request()->file('photo');
                $path = $file->store("products/cover", ['disk' => 'public_uploads']);
                Product::where('id', $pdt_id)
                        ->update([
                            "cover_image" => $path
                ]);
                $full_path = url('uploads/' . $path);
                return response()->json(['status' => 1, 'message' => __('messages.admin.cover_image_msg'), 'path' => $full_path, 'pdt_id' => $pdt_id]);
            } else {
                $productImages = $req->file('photos') ? $req->file('photos') : [];

                foreach ($productImages as $image) {
                    $saved = ProductImages::create([
                                'product_id' => $pdt_id,
                                'path' => $image->store("products/product_images", ['disk' => 'public_uploads']),
                                'created_at' => Carbon::now()
                    ]);
                }
                $pdt_images = ProductImages::where(['id' => $saved['id']])->first();
                $full_path = url('uploads/' . $pdt_images->path);
                return response()->json(['status' => 1, 'message' => __('messages.admin.product_img_msg'), 'path' => $full_path, 'image_id' => $saved['id']]);
            }
        }
    }

    public function productStatusChange(Request $req) {
        $product_data = Product::where('id', $req->id)->first();
        $variant_count = Helper::get_variant($req->id, $product_data->product_type);
        $price_count = Helper::get_price($req->id);
        if ($product_data) {
            if ($product_data->product_type == 'complex') {
                if ($variant_count > 0 && $price_count > 0) {
                    if ($product_data->supplier_status == 'publish') {
                        Product::where('id', $req->id)
                                ->update([
                                    'supplier_status' => 'unpublish',
                                    'status' => 'deactive',
                        ]);
                        $status = 'Unpublish';
                    } else {
                        Product::where('id', $req->id)
                                ->update([
                                    'supplier_status' => 'publish'
                        ]);
                        $status = 'Publish';
                    }
                    return response()->json(['status' => 1, 'message' => $status . ' successfully']);
                } else {
                    return response()->json(['status' => 0, 'message' => 'Product cannot be published. Please fill all required data']);
                }
            } else {
                if ($price_count > 0) {
                    if ($product_data->supplier_status == 'unpublish') {
                        Product::where('id', $req->id)
                                ->update([
                                    'supplier_status' => 'publish'
                        ]);
                        $status = 'Publish';
                    } else {
                        Product::where('id', $req->id)
                                ->update([
                                    'supplier_status' => 'unpublish',
                                    'status' => 'deactive',
                        ]);
                        $status = 'Unpublish';
                    }
                    return response()->json(['status' => 1, 'message' => $status .' successfully']);
                } else {
                    return response()->json(['status' => 0, 'message' => 'Product cannot be published. Please fill all required data']);
                }
            }
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }

    public function deleteProduct(Request $req) {
        if ($req->id) {
            $pdt = Product::find($req->id);
            $product_variant = ProductVariant::where('product_id', $req->id)->get();
            $pdt_cat = ProductCategories::where('product_id', $req->id)->first();
            $product_price = ProductPrice::where('product_id', $req->id)->get();
            $promo_product = PromotionalProduct::where('product_id', $req->id)->first();
            $recently_viewed_products = RecentlyViewedProducts::where('product_id', $req->id)->get();
            $product_view_count = ProductViewCount::where('product_id', $req->id)->first();
            if (!empty($pdt))
                $ads = Ads::select('id')->where('web_product', $req->id)->exists();
            $slider = Slider::select('id')->where('product', $req->id)->exists();
            if ($ads == false && $slider == false) {
                if (!empty($pdt_cat)) {
                    $pdt_cat->delete();
                }
                if (!empty($product_variant)) {
                    $product_variant->each->delete();
                }
                if (!empty($product_price)) {
                    $product_price->each->delete();
                }
                if (!empty($recently_viewed_products)) {
                    $recently_viewed_products->each->delete();
                }
                if (!empty($product_view_count)) {
                    $product_view_count->delete();
                }
                $pdt->lang()->delete();
                $pdt->delete();
                return response()->json(['status' => 1, 'message' => 'Product deleted successfully']);
            } else {
                return response()->json(['status' => 0, 'message' => 'This product cannot be deleted because it is assigned to some of the existing orders.']);
            }
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }

}
