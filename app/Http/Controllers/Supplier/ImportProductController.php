<?php

namespace App\Http\Controllers\supplier;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
//use Illuminate\Support\Facades\DB;
use App\Imports\ProductImport;
use App\Models\Category;
use App\Models\CategoryLang;
use App\Models\Brand;
use App\Models\BrandLang;
use App\Models\Product;
use App\Models\ProductLang;
use App\Models\ProductAttribute;
use App\Models\ProductAttributeLang;
use App\Models\ProductVariant;
use App\Models\ProductPrice;
use App\Models\Supplier;
use App\Models\TempProducts;
use App\Models\ProductBranchStock;
use App\Models\ProductCategories;
use App\Models\SupplierAnnex;
use Validator;
use Config;
use DB;
use Excel;

class ImportProductController extends Controller {

    //Import products
    public function index(Request $req) {

        if (isset($req->action) && $req->action == "76GH34RT65") {
            TempProducts::truncate();
            return redirect('supplier/importproducts/import');
        }
        $temp_pdts = TempProducts::where('cron_flag', 'C')->first();
        if (!empty($temp_pdts)) {
            return redirect('supplier/importproducts/imported_data');
        } else {
            $temp_pdts = TempProducts::where('cron_flag', 'I')
                    ->orWhere('cron_flag', 'S')
                    ->first();
            if (!empty($temp_pdts)) {
                return redirect('supplier/importproducts/excel_view');
            }
        }

        return view('supplier.product.import.index');
    }

    public function importsubmit(Request $req) {

        $rules = [
            'select_file' => 'required|mimes:xlsx|max:2048',
        ];
        $messages = [
            'select_file.required' => 'File is required.',
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $file = $req->file('select_file');
            $data = Excel::toArray(new ProductImport, $file);
            if (count($data) > 0) {
                foreach ($data as $key => $sub) {
                    foreach ($sub as $key1 => $val) {
                        if ($key1 > 0) {
                            $errors = "";
                            $temp_products = new TempProducts;
//
                            $temp_products->sku = $val[0];
                            if ($temp_products->sku == "") {
                                $supp_data = Supplier::where('id', '=', auth()->user()->id)->first();
                                $gen_sku = $this->generateSku();
                                $sku = $supp_data->code . '-' . $gen_sku;
                                $temp_products->sku = $sku;
                            }
                            $temp_products->product_name_en = $val[1];
                            if ($temp_products->product_name_en == "") {
                                $errors .= "Product Name (EN) is required,";
                            }
                            $temp_products->product_name_ar = $val[2];
                            if ($temp_products->product_name_ar == "") {
                                $errors .= "Product Name (AR) is required,";
                            }
                            $temp_products->main_category = $val[3];
                            if ($temp_products->main_category == "") {
                                $errors .= "Main category is required,";
                            }
                            $temp_products->level2_category = $val[4];
                            if ($temp_products->level2_category == "") {
                                $errors .= "Lavel 2 category is required,";
                            }
                            $temp_products->level3_category = $val[5];
                            $temp_products->category = $val[6];
                            if ($temp_products->category == "") {
                                $errors .= "Category is required,";
                            }
                            $temp_products->brand = $val[7];
                            if ($temp_products->brand == "") {
                                $errors .= "Brand is required,";
                            }
                            $temp_products->description_en = $val[8];
                            if ($temp_products->description_en == "") {
                                $errors .= "Description (EN) is required,";
                            }
                            $temp_products->description_ar = $val[9];
                            if ($temp_products->description_ar == "") {
                                $errors .= "Description (AR) is required,";
                            }
                            $temp_products->min_stock = $val[10];
                            if ($temp_products->min_stock == "") {
                                $errors .= "Min Stock is required,";
                            }
                            $temp_products->max_stock = $val[11];
                            if ($temp_products->max_stock == "") {
                                $errors .= "Max Stock is required,";
                            }
                            $temp_products->price = $val[12];
                            if ($temp_products->price == "") {
                                $errors .= "Price is required,";
                            }
                            $temp_products->discount_price = $val[13];
                            if ($errors) {
                                $temp_products->error_flag = "Y";
                                $temp_products->errors = $errors;
                            } else {
                                $temp_products->error_flag = "N";
                            }
                            $temp_products->created_at = Carbon::now();
                            $temp_products->save();
                        }
                    }
                }
                return response()->json(['status' => '1']);
            } else {
                return response()->json(['message' => $validation->errors()->first(), 'status' => '0']);
            }
        }
    }

    public function excel_view(Request $req) {
        $search = $req->search;
        $error_count = $duplicate_count = $success_count = 0;
        $result_data = array();
        $pagination_count = 10;
        $query = TempProducts::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->where('sku', 'like', "%" . $search . "%");
                $sub->orWhere('product_name_en', 'like', "%" . $search . "%");
                $sub->orWhere('product_name_ar', 'like', "%" . $search . "%");
            });
        }
        $temp_data = $query->where('cron_flag', 'I')->paginate($pagination_count)->appends(request()->query());
        if (count($temp_data) > 0) {
            foreach ($temp_data as $row_data) {
                $main_category = CategoryLang::select('category_id')->where('name', $row_data->main_category)->first();
                if (!$main_category) {
                    $row_data->error_flag = 'Y';
                }
                $level2_category = CategoryLang::select('category_id')->where('name', $row_data->level2_category)->first();
                if (!$level2_category) {
                    $row_data->error_flag = 'Y';
                }
                if ($main_category) {
                    $is_parent = Category::select('id')->where('id', $main_category->category_id)->where('parent_id', NULL)->exists();
                    if ($is_parent == false) {
                        $row_data->error_flag = 'Y';
                    }
                }
                if ($main_category && $level2_category) {
                    $is_sub = Category::select('id')->where('id', $level2_category->category_id)->where('parent_id', $main_category->category_id)->exists();
                    if ($is_sub == false) {
                        $row_data->error_flag = 'Y';
                    }
                }
                if ($row_data->level3_category) {
                    $val = 0;
                    $level3_category = $row_data->level3_category;
                    $level3_cat_arr = explode(",", $level3_category);
                    foreach ($level3_cat_arr as $key => $value) {
                        $level3 = CategoryLang::select('id')->where('name', $value)->exists();
                        if ($level3 == false) {
                            $val = 1;
                        }
                    }
                    if ($val == 1) {
                        $row_data->error_flag = 'Y';
                    }
                }
                $cat_data = CategoryLang::select('id')->where('name', $row_data->category)->exists();
                if ($cat_data == false) {
                    $row_data->error_flag = 'Y';
                }
                $brand_data = BrandLang::select('id')->where('name', $row_data->brand)->exists();
                if ($brand_data == false) {
                    $row_data->error_flag = 'Y';
                }
                if (!empty($row_data->price) && !empty($row_data->discount_price)) {
                    if ($row_data->price <= $row_data->discount_price) {
                        $row_data->error_flag = 'Y';
                    }
                }
                if (!empty($row_data->min_stock) && !empty($row_data->max_stock)) {
                    if ($row_data->max_stock <= $row_data->min_stock) {
                        $row_data->error_flag = 'Y';
                    }
                }
                $sku_data = Product::where('sku', '=', $row_data->sku)->where('supplier_id', '=', auth()->user()->id)->first();
                if (!empty($sku_data)) {
                    if ($sku_data->product_type != 'simple') {
                        $row_data->error_flag = 'Y';
                    }
                } else {
                    $sku_exist = Product::where('sku', '=', $row_data->sku)->exists();
                    if ($sku_exist != false) {
                        $row_data->error_flag = 'Y';
                    }
                }
                if ($row_data->error_flag == 'Y') {
                    $error_count++;
                }
                if ($row_data->error_flag == 'N') {
                    $pdt_data = Product::where('sku', '=', $row_data->sku)->first();
                    if (!empty($pdt_data)) {
                        $row_data->duplicate_flag = 'Y';
                        $duplicate_count++;
                    }
                }
                if ($row_data->error_flag == 'N' && $row_data->duplicate_flag == 'N') {
                    $success_count++;
                }
                $result[] = $row_data;
            }
            $result_data = $result;
        }
        $total_count = $error_count + $duplicate_count + $success_count;
        $data = [
            'temp_data' => $temp_data,
            'result_data' => $result_data,
            'error_count' => $error_count,
            'duplicate_count' => $duplicate_count,
            'success_count' => $success_count,
            'total_count' => $total_count,
            'search' => $search
        ];
        return view('supplier.product.import.verify_import', $data);
    }

    public function get_errors(Request $req) {
        $id = $req->id;
        $result = TempProducts::where('id', $id)->first();
        $cat_data = CategoryLang::where('name', '=', $result->category)->first();
        $new_errors = '';
        $main_category = CategoryLang::select('category_id')->where('name', $result->main_category)->first();
        if (!empty($result->main_category) && !$main_category) {
            $new_errors .= "Main category not exist in the system,";
        }
        $level2_category = CategoryLang::select('category_id')->where('name', $result->level2_category)->first();
        if (!empty($result->level2_category) && !$level2_category) {
            $new_errors .= "Level2 category not exist in the system,";
        }
        if ($main_category) {
            $is_parent = Category::select('id')->where('id', $main_category->category_id)->where('parent_id', NULL)->exists();
            if ($is_parent == false) {
                $new_errors .= "Not a main category,";
            }
        }
        if ($main_category && $level2_category) {
            $is_sub = Category::select('id')->where('id', $level2_category->category_id)->where('parent_id', $main_category->category_id)->exists();
            if ($is_sub == false) {
                $new_errors .= "Not a level2 category,";
            }
        }
        if ($result->level3_category) {
            $val = 0;
            $level3_category = $result->level3_category;
            $level3_cat_arr = explode(",", $level3_category);
            foreach ($level3_cat_arr as $key => $value) {
                $level3 = CategoryLang::select('id')->where('name', $value)->exists();
                if ($level3 == false) {
                    $val = 1;
                }
            }
            if ($val == 1) {
                $new_errors .= "Level 3 category not exist in the system,";
            }
        }
        if (!empty($result->category) && empty($cat_data)) {
            $new_errors .= "Category not exist in the system,";
        }
        $brand_data = BrandLang::where('name', '=', $result->brand)->first();
        if (!empty($result->brand) && empty($brand_data)) {
            $new_errors .= "Brand not exist in the system,";
        }
        if (!empty($result->price) && !empty($result->discount_price)) {
            if (($result->price) <= ($result->discount_price)) {
                $new_errors .= "Discount is greater than price,";
            }
        }
        if (!empty($result->min_stock) && !empty($result->max_stock)) {
            if (($result->max_stock) <= ($result->min_stock)) {
                $new_errors .= "Min stock is greater than max stock,";
            }
        }
        $sku_data = Product::where('sku', '=', $result->sku)->where('supplier_id', '=', auth()->user()->id)->first();
        if (!empty($sku_data)) {
            if ($sku_data->product_type != 'simple') {
                $new_errors .= "Imported product is not simple ,";
            }
        } else {
            $sku_exist = Product::where('sku', '=', $result->sku)->exists();
            if ($sku_exist != false) {
                $new_errors .= "You are not allowed to alter this product because it is already used by another supplier ,";
            }
        }
        $all_errors = $result->errors . $new_errors;
        $errors = [];
        $errors = explode(",", $all_errors);

        $data = ['errors' => $errors];
        return view('supplier.product.import.errors', $data);
    }

    public function import_view(Request $req) {
        $_temp_id = $req->id;
        $row_data = array();
        if ($_temp_id != '') {
            $row_data = TempProducts::where('id', '=', $_temp_id)->first();
        }

        $data = [
            'row_data' => $row_data
        ];
        return view('supplier.product.import.view', $data);
    }

    public function confrim_import() {
        $result_data = array();

        $result_data = TempProducts::where('cron_flag', 'I')->get();
        if (count($result_data) > 0) {
            foreach ($result_data as $row_data) {
                if ($row_data->error_flag == 'N') {
                    $main_category = CategoryLang::select('category_id')->where('name', $row_data->main_category)->first();
                    if (!$main_category) {
                        $row_data->error_flag = 'Y';
                    }
                    $level2_category = CategoryLang::select('category_id')->where('name', $row_data->level2_category)->first();
                    if (!$level2_category) {
                        $row_data->error_flag = 'Y';
                    }
                    if ($main_category) {
                        $is_parent = Category::select('id')->where('id', $main_category->category_id)->where('parent_id', NULL)->exists();
                        if ($is_parent == false) {
                            $row_data->error_flag = 'Y';
                        } else {
                            $row_data->main_category = $main_category->category_id;
                        }
                    }
                    if ($main_category && $level2_category) {
                        $is_sub = Category::select('id')->where('id', $level2_category->category_id)->where('parent_id', $main_category->category_id)->exists();
                        if ($is_sub == false) {
                            $row_data->error_flag = 'Y';
                        } else {
                            $row_data->sub_category = $level2_category->category_id;
                        }
                    }
                    if ($row_data->level3_category) {
                        $val = 0;
                        $level3_category = $row_data->level3_category;
                        $level3_cat_arr = explode(",", $level3_category);
                        foreach ($level3_cat_arr as $key => $value) {
                            $level3 = CategoryLang::select('id')->where('name', $value)->exists();
                            if ($level3 == false) {
                                $val = 1;
                            }
                        }
                        if ($val == 1) {
                            $row_data->error_flag = 'Y';
                        }
                    }
                    $cat_data = CategoryLang::where('name', '=', $row_data->category)->first();
                    if (empty($cat_data)) {
                        $row_data->error_flag = 'Y';
                    } else {
                        $row_data->category_id = $cat_data['category_id'];
                    }
                    $brand_data = BrandLang::where('name', '=', $row_data->brand)->first();
                    if (empty($brand_data)) {
                        $row_data->error_flag = 'Y';
                    } else {
                        $row_data->brand_id = $brand_data['brand_id'];
                    }
                    if (!empty($row_data->price) && !empty($row_data->discount_price)) {
                        if (($row_data->price) <= ($row_data->discount_price)) {
                            $row_data->error_flag = 'Y';
                        }
                    }
                    if (!empty($row_data->min_stock) && !empty($row_data->max_stock)) {
                        if (($row_data->max_stock) <= ($row_data->min_stock)) {
                            $row_data->error_flag = 'Y';
                        }
                    }
                    $sku_data = Product::where('sku', '=', $row_data->sku)->where('supplier_id', '=', auth()->user()->id)->first();
                    if (!empty($sku_data)) {
                        if ($sku_data->product_type != 'simple') {
                            $row_data->error_flag = 'Y';
                        }
                    } else {
                        $sku_exist = Product::where('sku', '=', $row_data->sku)->exists();
                        if ($sku_exist != false) {
                            $row_data->error_flag = 'Y';
                        }
                    }
                    if ($row_data->error_flag == 'N') {
                        $pdt_data = Product::where('sku', '=', $row_data->sku)->where('supplier_id', '=', auth()->user()->id)->first();
                        $supp_commission = SupplierAnnex::where(['supplier_id' => auth()->user()->id])->pluck('commision_category_id')->first();
                        $data_to_save = [
                            'sku' => $row_data->sku,
                            'category_id' => $row_data->category_id,
                            'supplier_id' => auth()->user()->id,
                            'commision_percentage' => $supp_commission,
                            'brand_id' => $row_data->brand_id,
                            'main_category' => $row_data->main_category,
                            'sub_category' => $row_data->sub_category,
                            'slug' => $this->createSlug($row_data->product_name_en)
                        ];
                        if (!empty($pdt_data)) {
                            $row_data->duplicate_flag = 'Y';
                            $saved_data = Product::where('id', $pdt_data->id)
                                    ->update($data_to_save);
                            ProductLang::where('language', 'en')
                                    ->where('product_id', $pdt_data->id)
                                    ->update([
                                        'name' => $row_data->product_name_en,
                                        'description' => $row_data->description_en,
                            ]);
                            ProductLang::where('language', 'ar')
                                    ->where('product_id', $pdt_data->id)
                                    ->update([
                                        'name' => $row_data->product_name_ar,
                                        'description' => $row_data->description_ar,
                            ]);
                            $prdt_pdt_price = ProductPrice::where('product_id', $pdt_data->id)->first();
                            $easify_markup = isset($prdt_pdt_price->easify_markup) ? $prdt_pdt_price->easify_markup : NULL;
                            if ($row_data->discount_price) {
                                $discount_price = $row_data->price - ($row_data->price * $row_data->discount_price / 100);
                            } else {
                                $discount_price = NULL;
                            }
                            $data_to_save = [
                                'price' => $row_data->price,
                                'discount' => $row_data->discount_price,
                                'discount_price' => $discount_price,
                                'easify_markup' => $easify_markup,
                            ];
                            $saved_data = ProductPrice::where('product_id', $pdt_data->id)
                                    ->update($data_to_save);
                            $pdt_var = ProductVariant::where('product_id', $pdt_data->id)->where('supplier_id', '=', auth()->user()->id)->first();
                            $branch_data = Supplier::select('region_id')->where('id', '=', auth()->user()->id)->first();
                            $pdt_var_id = isset($pdt_var->id) ? $pdt_var->id : NULL;
                            $data_to_save = [
                                'region_id' => $branch_data->region_id,
                                'min_stock' => $row_data->min_stock,
                                'max_stock' => $row_data->max_stock
                            ];
                            $saved_data = ProductBranchStock::where(['product_variant_stock_id' => $pdt_var_id, 'branch_id' => auth()->user()->id])
                                    ->update($data_to_save);
                        } else {
                            $data_to_save['created_at'] = Carbon::now();
                            $saved_data = Product::create($data_to_save);
                            $saved_data->lang()->createMany([
                                [
                                    'name' => $row_data->product_name_en,
                                    'description' => $row_data->description_en,
                                    'language' => 'en',
                                ],
                                [
                                    'name' => $row_data->product_name_ar,
                                    'description' => $row_data->description_ar,
                                    'language' => 'ar',
                                ],
                            ]);
                            $sub_cat_markup = Category::where(['id' => $row_data->sub_category])->pluck('markup_percentage')->first();
                            if ($sub_cat_markup) {
                                $easy_markup = $sub_cat_markup;
                            } else {
                                $main_cat_markup = Category::where(['id' => $row_data->main_category])->pluck('markup_percentage')->first();
                                $easy_markup = $main_cat_markup;
                            }
                            $easy_markup = $easy_markup ? $easy_markup : '';
                            if ($row_data->discount_price) {
                                $discount_price = $row_data->price - ($row_data->price * $row_data->discount_price / 100);
                            } else {
                                $discount_price = NULL;
                            }
                            ProductPrice::create([
                                'product_id' => $saved_data->id,
                                'price' => $row_data->price,
                                'discount' => $row_data->discount_price,
                                'discount_price' => $discount_price,
                                'easify_markup' => $easy_markup,
                                'created_at' => Carbon::now()
                            ]);

                            $product_variant = ProductVariant::create([
                                        'product_id' => $saved_data->id,
                                        'supplier_id' => auth()->user()->id,
                                        'created_at' => Carbon::now()
                            ]);
                            $branch_data = Supplier::select('region_id')->where('id', '=', auth()->user()->id)->first();
                            ProductBranchStock::create([
                                'product_variant_stock_id' => $product_variant->id,
                                'branch_id' => auth()->user()->id,
                                'region_id' => $branch_data->region_id,
                                'min_stock' => $row_data->min_stock,
                                'max_stock' => $row_data->max_stock,
                                'created_at' => Carbon::now()
                            ]);
                        }

                        $pdt_id = !empty($saved_data->id) ? $saved_data->id : $pdt_data->id;
                        //For multiple category
                        $cat_ids = '';
                        $sec_category = [];
                        $level3_category = $row_data->level3_category;
                        if($level3_category){
                        $level3_cat_arr = explode(",", $level3_category);
                        foreach ($level3_cat_arr as $key => $value) {
                            $level3 = CategoryLang::select('category_id')->where('name', $value)->first();
                            $sec_category[] = $level3->category_id;
                        }
                        if (ProductCategories::where('product_id', $pdt_id)->exists()) {
                            ProductCategories::where('product_id', $pdt_id)->delete();
                        }
                        }
                        $main_category = $row_data->main_category;
                        $sub_category = $row_data->sub_category;
                        $sec_category[] = $main_category;
                        $sec_category[] = $sub_category;
                        if ($sec_category) {
                            foreach ($sec_category as $value) {
                                $cat_ids .= '{{' . $value . '}}';
                                $saved = ProductCategories::create([
                                            'product_id' => $pdt_id,
                                            'category_id' => $value
                                ]);
                            }
                            $product_categories = Product::where('id', $pdt_id)
                                    ->update([
                                'category_ids' => $cat_ids,
                            ]);
                        }
                    }
                }
                TempProducts::where('id', $row_data->id)->update(['cron_flag' => 'S', 'duplicate_flag' => $row_data->duplicate_flag, 'error_flag' => $row_data->error_flag]);
            }
        }
        $check_temp_emp = TempProducts::where('cron_flag', 'I')->first();
        if (!$check_temp_emp) {
            TempProducts::where('cron_flag', 'S')->update(['cron_flag' => 'C']);
        }

        return response()->json(['message' => 'Thank you! Successfully imported', 'status' => '1']);
    }

    public function imported_data(Request $req) {
        $search = $req->search;
        $error_count = $success_count = 0;
        $pagination_count = 10;
        $result_data = array();
        $query = TempProducts::query();
        if ($search) {
            $query->where(function ($sub) use ($search) {
                $sub->where('sku', 'like', "%" . $search . "%");
                $sub->orWhere('product_name_en', 'like', "%" . $search . "%");
                $sub->orWhere('product_name_ar', 'like', "%" . $search . "%");
            });
        }
        $result_data = $query->where('cron_flag', 'C')
                        ->paginate($pagination_count)->appends(request()->query());

        if (count($result_data) > 0) {
            foreach ($result_data as $row_data) {
                if ($row_data->error_flag == 'Y') {
                    $error_count++;
                } else {
                    $success_count++;
                }
            }
        }
        $total_count = $error_count + $success_count;
        $data = [
            'result_data' => $result_data,
            'error_count' => $error_count,
            'success_count' => $success_count,
            'total_count' => $total_count,
            'search' => $search,
        ];
        return view('supplier.product.import.product_imported', $data);
    }

    public function generateSku() {
        $lastsku = Product::select('sku')
                ->orderBy('id', 'desc')
                ->first();

        $lastId = 0;

        if ($lastsku) {
            $lastId = strstr($lastsku->sku, 'SKU-');
            $lastId = str_replace('SKU-', '', $lastId);
        }
        $lastId++;
        return 'SKU-' . str_pad($lastId, 5, '0', STR_PAD_LEFT);
    }

    private function createSlug($string) {
        $slug = strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'), array('', '-', ''), trim($string)));
        return $slug;
    }

}
