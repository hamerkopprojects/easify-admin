<?php

namespace App\Http\Middleware;

use Closure;

class CustomerSessionCheck
{

    public function handle($request, Closure $next)
    {
        if ( ! $request->session()->exists('customer_id')) {
            if( $request->ajax() ) {

                return response()->json(['success' => FALSE, 'message' => 'Not authorised to perfom this action', 'code' => 'no.login']);
            }
            return redirect('/');
        }

        return $next($request);
    }

}