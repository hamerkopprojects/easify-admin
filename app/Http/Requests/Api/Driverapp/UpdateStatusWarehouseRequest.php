<?php

namespace App\Http\Requests\Api\Driverapp;

use Illuminate\Foundation\Http\FormRequest;

class UpdateStatusWarehouseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'=>'required',
            'shelf_id'=>'required',
            'order_id'=>'required'

        ];
    }
}
