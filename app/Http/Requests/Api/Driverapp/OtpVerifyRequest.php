<?php

namespace App\Http\Requests\Api\Driverapp;

use Illuminate\Foundation\Http\FormRequest;

class OtpVerifyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|min:6|max:12',
            'otp' => 'required|digits:4',
            'fcm_token' => 'nullable|string',
        ];
    }
}
