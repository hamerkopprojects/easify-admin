<?php

namespace App\Http\Requests\Api\Driverapp;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'=>'required',
            'photo'=>'nullable|image|mimes:png,jpg,jpeg',
            'name'=>'required'
        ];
    }
}
