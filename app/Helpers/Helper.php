<?php

namespace App\Helpers;

use App\Models\ProductVariant;
use App\Models\ProductAttribute;
use App\Models\ProductPrice;
use App\Models\WishList;
use Illuminate\Support\Facades\Session;

class Helper {

    public static function get_variant($pdt_id, $product_type = '') {
        if ($product_type == 'simple') {
            $variants = ProductAttribute::with('attribute')->where(['product_id' => $pdt_id])->count();
        } else {
            $variants = ProductVariant::with('variant')->where(['product_id' => $pdt_id])->count();
        }
        return $variants;
    }

    public static function get_price($pdt_id) {
        $price_count = ProductPrice::where(['product_id' => $pdt_id])->count();
        return $price_count;
    }
    
    public static function wish_listed_products() {
        //for whish list
        $customer = Session::get('customer_id');
        $region_id = Session::get('region_id');
        $wishListAll = [];
        if ($customer) {
            $wishList = WishList::where('customer_id', $customer)->where('region_id', $region_id)->get();
            foreach ($wishList as $wish) {
                $wishListAll[] = $wish->product_id;
            }
        }
        return $wishListAll;
        //end
    }

}
