<?php

namespace App\DataTables;

use Carbon\Carbon;

use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class EmtyazDataTable extends DataTable
{
    public function builder()
    {
        // $pageLength = Config::get('app.page_length');
        // $lengthMenu = Config::get('app.length_menu');
        $lengthMenu = [20, 30, 50, 100];
        $pageLength = 20;
        return parent::builder()
            ->languageZeroRecords('No data available')
            ->lengthMenu($lengthMenu)
            ->pageLength($pageLength);
    }

    /**
     * Convert array to collection of Column class.
     *
     * @param array $columns
     * @return Collection
     */
    protected function toColumnsCollection(array $columns)
    {
        $collection = collect();

        foreach ($columns as $column => $title) {
            $data['data']  = $column;
            $data['title'] = $title;
            $collection->push(new Column($data));
        }

        return $collection;
    }

    /**
     * Get mapped columns versus final decorated output.
     *
     * @return array
     */
    protected function getDataForExport()
    {
        $columns = $this->exportColumns();

        return $this->mapResponseToColumns($columns, 'exportable');
    }

    /**
     * Get export columns definition.
     *
     * @return array|string
     */
    private function exportColumns()
    {
        return is_array($this->exportColumns) ? $this->toColumnsCollection($this->exportColumns) : $this->getExportColumnsFromBuilder();
    }
}
