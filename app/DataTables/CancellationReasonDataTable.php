<?php

namespace App\DataTables;



use App\Models\RatingSegments;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use App\DataTables\EmtyazDataTable;
use App\Models\CancellationReason;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class CancellationReasonDataTable extends EmtyazDataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->addColumn('cancel_en', function (CancellationReason $term) {
                return $term->lang->firstWhere('language', 'en')->name ?? '';
            })
            ->addColumn('cancel_ar', function (CancellationReason $term) {
                return $term->lang->firstWhere('language', 'ar')->name ?? '';
            })
            ->addColumn('actions', function (CancellationReason $term) {
                
                return '<i class="ti-pencil-alt  edit-style cancel_edit btn btn-sm btn-success text-white" 
                data-id="'.$term->id.'" data-toggle="tooltip" data-placement="top" title="Edit"></i>
                <i class="ti-trash deleteBtn cust_delete  edit-style btn btn-sm btn-danger text-white"  data-name="Jalboot" data-last_name="OM" data-id= "' . $term->id . '" data-toggle="tooltip" data-placement="top" title="Delete"></i>';
            })
            ->filterColumn('cancel_en', function ($query, $keyword) {
                // dd($keyword);
                $ids = explode(',', $keyword);
                return $query->whereIn('id', $ids);
            })

            ->rawColumns(['actions']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\faq $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(CancellationReason $model)
    {
        return $model->newQuery()
            ->with('lang');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('cancel-reason-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('rtip')
            ->ordering(false)

            ->buttons(
                Button::make('create'),
                Button::make('export'),
                Button::make('print'),
                Button::make('reset'),
                Button::make('reload')
            );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('DT_RowIndex')
                ->title('SNO'),
            Column::computed('cancel_en')
                ->title('CANCELLATION REASON(EN)')
                ->searchable(true),
            Column::computed('cancel_ar')
                ->className('right-align')
                ->title('CANCELLATION REASON(AR)'),

            Column::computed('actions')
                ->title(''),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Salons_' . date('YmdHis');
    }
}
