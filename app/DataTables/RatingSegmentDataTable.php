<?php

namespace App\DataTables;



use App\Models\RatingSegments;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use App\DataTables\EmtyazDataTable;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class RatingSegmentDataTable extends EmtyazDataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->addColumn('segment_en', function (RatingSegments $term) {
                return $term->lang->firstWhere('language', 'en')->name ?? '';
            })
            ->addColumn('segment_ar', function (RatingSegments $term) {
                return $term->lang->firstWhere('language', 'ar')->name ?? '';
            })
            ->addColumn('actions', function (RatingSegments $term) {
                
                return '<i class="ti-pencil-alt  edit-style" 
                data-id="'.$term->id.'" data-toggle="tooltip" data-placement="top" title="Edit"></i>';
            })

            ->rawColumns(['actions']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\faq $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(RatingSegments $model)
    {
        return $model->newQuery()
            ->with('lang');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('rating-segments-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('lrtip')
            ->ordering(false)

            ->buttons(
                Button::make('create'),
                Button::make('export'),
                Button::make('print'),
                Button::make('reset'),
                Button::make('reload')
            );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('DT_RowIndex')
                ->title('SNO'),
            Column::computed('segment_en')
                ->title('SEGMENT TITLE(EN)'),
            Column::computed('segment_ar')
                ->className('right-align')
                ->title('SEGMENT TITLE(AR)'),

            Column::computed('actions')
                ->title(''),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Salons_' . date('YmdHis');
    }
}
