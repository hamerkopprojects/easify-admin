<?php

namespace App\Providers;

use DB;
use App\Models\Category;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $lang = request('language','en');
            App::setLocale($lang);

         Schema::defaultStringLength(191);
         
         Relation::morphMap([
            'driver' => 'App\User',
            'admin' => 'App\User',
            'website' => 'App\Models\Customer',
            'supplier' =>'App\Models\Supplier',
            'branch' =>'App\Models\Supplier',
            'customer' =>'App\Models\Customer',

        ]);
         
        $settings = DB::table('other_settings')->first();
        View::share('settings', $settings);
        
        
        $currency = array(
        'en' => json_decode($settings->currency)->currency_en,
        'ar' => json_decode($settings->currency)->currency_ar
                );
        View::share('currency', $currency);

         view()->composer('frontend/frontend_layouts/nav', function($view) {
            $view->with('menu', Category::with(['lang' => function ($query){
            $query->where('language', app()->getLocale());
        }])->where(['parent_id' => NULL, 'status' => 'active'])->get());
        });
    }
}
