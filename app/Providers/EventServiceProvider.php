<?php

namespace App\Providers;

use App\Events\SendToken;
use App\Events\CustomerAuthenticated;
use App\Events\CartInitialize;

use App\Listeners\GenerateToken;
use App\Listeners\CustomerAuthListener;
use App\Listeners\CartInitializeListener;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        SendToken::class =>[
            GenerateToken::class,
        ],
        CustomerAuthenticated::class => [
            CustomerAuthListener::class
        ],
        CustomerAuthenticated::class => [
            CustomerAuthListener::class
        ],
        CartInitialize::class => [
            CartInitializeListener::class
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
