<?php

namespace App\View\Components;


use Illuminate\View\Component;

class OrderCompleteTab extends Component
{
    public $activeTab;

    public $tabs = [
        'order' => 'ORDER INFO',
        'review'=>'REVIEWS',
        
    ];

    public $url = [
        'order'=>"#",
        'review'=>'#'
    ];

    protected $salon;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($activeTab,$id)
    {
        $this->activeTab = $activeTab;
        if ($id) {
            $this->setEditTabUrl($id);
        }
       
    }
    public function setEditTabUrl( $id)
    {
        $this->url = [
            // 'active'=>'#'
            'order' => route('admin.order-details',[$id]),
            'review' =>route('admin.order-review',[$id]),
            // 'services' => route('salon.services.edit', [$salon]),
            // 'facilities' => route('salon.facilities.edit', [$salon]),
            // 'schedule' => route('salon.schedule.edit', [$salon]),
        ];
    }

  

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.order-complete-tab');
    }
}
