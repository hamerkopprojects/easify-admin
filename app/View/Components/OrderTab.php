<?php

namespace App\View\Components;


use Illuminate\View\Component;

class OrderTab extends Component
{
    public $activeTab;

    public $tabs = [
        'collection' => 'ACTIVE ORDERS(Collection)',
        'delivery' => 'ACTIVE ORDERS(Delivery)',
        'complete'=>'DELIVERED ORDERS',
        'cancel'=>'CANCELLED ORDERS'
        
    ];

    public $url = [
        'collection'=>"collection",
        'delivery'=>"delivery",
        'complete'=>'complete',
        'cancel'=>'cancel',
        
    ];

   

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($activeTab)
    {
        $this->activeTab = $activeTab;
       
    }

  

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.order-tab');
    }
}
