<?php

namespace App\View\Components;


use Illuminate\View\Component;

class SuppReportTab extends Component
{
    
    public $reportArray;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($reportTab)
    {
        
        $this->reportArray = [
            'sales' => [
                'tabs' =>"sales_tab",
                'url' =>route('supplier.reports'),
                'icon' => 'ti-money',
            ],
            'saleslinechart' => [
                'tabs' => "sales_line_tab",
                'url' => route('supplier.sales.linechart'),
                'icon' => 'ti-stats-up',
            ],
            'hourlysales' => [
                'tabs' => "hr_sales_tab",
                'url' => route('supplier.sales.hourly'),
               'icon' => 'ti-timer',
            ]
            
         ];
       
    }

  

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.supp-report-tab');
    }
}
