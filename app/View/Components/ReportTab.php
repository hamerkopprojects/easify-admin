<?php

namespace App\View\Components;


use Illuminate\View\Component;

class ReportTab extends Component
{
    
    public $reportArray;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($reportTab)
    {
        
        $this->reportArray = [
            'sales' => [
                'tabs' => 'Sales',
                'url' => route('reports'),
                'icon' => 'ti-money',
            ],
            'saleslinechart' => [
                'tabs' => 'Sales(Line chart)',
                'url' => route('sales.linechart'),
                'icon' => 'ti-stats-up',
            ],
            'hourlysales' => [
                'tabs' => 'Hourly Sales',
                'url' => route('sales.hourly'),
               'icon' => 'ti-timer',
            ],
            'commission' => [
                'tabs' => 'Commission Report',
                'url' => route('reports.commission'),
               'icon' => 'ti-bar-chart',
            ],
            
         ];
       
    }

  

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.report-tab');
    }
}
