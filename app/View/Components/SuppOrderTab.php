<?php

namespace App\View\Components;


use Illuminate\View\Component;

class SuppOrderTab extends Component
{
    public $activeTab;

    public $tabs = [
        'supp-active' => 'active_orders',
        'supp-complete'=>'completed_orders' ,
        'supp-cancel'=>'cancelled_orders'
        
    ];

    public $url = [
       
        'supp-active'=>"supp-active",
        'supp-complete'=>'supp-complete',
        'supp-cancel'=>'supp-cancel',
        
    ];

   

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($activeTab)
    {
        $this->activeTab = $activeTab;
       
    }

  

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.supp-order-tab');
    }
}
