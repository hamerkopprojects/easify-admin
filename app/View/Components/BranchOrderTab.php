<?php

namespace App\View\Components;


use Illuminate\View\Component;

class BranchOrderTab extends Component
{
    public $activeTab;

    public $tabs = [
        'bch-active' => 'active_orders',
        'bch-complete'=>'completed_orders' ,
        'bch-cancel'=>'cancelled_orders'
        
    ];

    public $url = [
       
        'bch-active'=>"active",
        'bch-complete'=>'complete',
        'bch-cancel'=>'cancel',
        
    ];

   

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($activeTab)
    {
        $this->activeTab = $activeTab;
       
    }

  

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.branch-order-tab');
    }
}
