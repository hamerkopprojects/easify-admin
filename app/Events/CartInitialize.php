<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CartInitialize
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $cartKey;

    /**
     * Create a new event instance.
     *
     * @param  $lang
     * @return void
     */
    public function __construct($type)
    {
        $n = 30;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_:+-.@!=,?'; 
        $randomString = '';
    
        for ($i = 0; $i < $n; $i++) { 
            $index = rand(0, strlen($characters) - 1); 
            $randomString .= $characters[$index]; 
        }
        
        $cartKey = $randomString;
        if( $type == 'create') {
            $this->cartKey = $cartKey;
        } else {
            $this->cartKey = '';
        }
    }
}