<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Driverapp\LoginController;
use App\Http\Controllers\Api\Driverapp\AppDataController;
use App\Http\Controllers\Api\Driverapp\LanguageController;
use App\Http\Controllers\Api\Driverapp\SupportController;
use App\Http\Controllers\Api\Driverapp\UserEditController;
use App\Http\Controllers\Api\Driverapp\NotificationController;
use App\Http\Controllers\Api\Driverapp\OrderDeliveryController;
use App\Http\Controllers\Api\Driverapp\QRCodeScanningController;
use App\Http\Controllers\Api\Driverapp\OrderCollectionController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('login', [LoginController::class, 'driverAppLogin']);
Route::post('verify', [LoginController::class, 'otpVerify']);
Route::post('resend', [LoginController::class, 'resend']);
Route::get('privacy-policies', [AppDataController::class, 'privacyPolicies']);
Route::get('terms-conditions', [AppDataController::class, 'termsandConditions']);
Route::get('faq', [AppDataController::class, 'faq']);
Route::get('useapp', [AppDataController::class, 'getUseAppData']);



Route::group(['middleware' => ['auth:api' ,'scope:user']], function () {
    Route::post('logout', [LoginController::class, 'logout']);
    Route::get('my-details', [UserEditController::class,'editUser']);
    Route::post('update-me', [UserEditController::class,'updateDriver']);
    Route::post('lang', [LanguageController::class, 'update']);

    // collection tab
    Route::get('order-collection', [OrderCollectionController::class, 'orderList']);
    Route::get('order-collection/details/{id}', [OrderCollectionController::class, 'collectionDetails']);
    Route::post('order-collection/status-update', [OrderCollectionController::class, 'markasCollected']);
    Route::get('order-collection/shelflist', [OrderCollectionController::class, 'shelfList']);
    Route::post('order-collection/at-warehouse', [OrderCollectionController::class, 'inWareHouse']);
    Route::get('order-collection/warehouse', [OrderCollectionController::class, 'warehouse']);
    
    
    
    
    // deliverey tab
    Route::get('order-delivery/active', [OrderDeliveryController::class, 'activeOrders']);
    Route::get('order-delivery/delivered', [OrderDeliveryController::class, 'deliveredOrder']);
    Route::get('order-delivery/details/{id}', [OrderDeliveryController::class, 'orderDetails']);
    Route::post('order-delivery/cancel', [OrderDeliveryController::class, 'cancelOrder']);
    Route::get('order-delivery/mark-as-delivered/{id}', [OrderDeliveryController::class, 'markAsDelivered']);
    Route::get('cancellation-reason', [OrderDeliveryController::class, 'cancellationReasons']);
    
    
    //  Support

    Route::post('support', [SupportController::class, 'index']);
    Route::get('support/{id}', [SupportController::class, 'supportChat']);
    Route::post('support/chat', [SupportController::class, 'sendChat']);

    // notification 
    Route::get('/notification/list', [NotificationController::class, 'get']);
    Route::get('/notification/read/{id}', [NotificationController::class, 'markAsRead']);
    Route::put('notifications', [NotificationController::class, 'toggle']);
    Route::get('/check/need-notification', [NotificationController::class, 'notificationCheck']);
    Route::get('driver/notification/count', [NotificationController::class, 'unreadCount']);
    Route::get('driver/notification/badge', [NotificationController::class, 'badgeUpdate']);
    // QR code
    
    Route::post('scan-confirm', [QRCodeScanningController::class, 'QrCodeStatus']);
    Route::post('scan-qr', [QRCodeScanningController::class, 'generate']);
    

});

