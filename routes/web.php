<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\EditProfile;
use App\Http\Controllers\Admin\CityController;
use App\Http\Controllers\Admin\FaqsController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Admin\PagesController;
use App\Http\Controllers\Supplier\AuthController;
use App\Http\Controllers\Supplier\HomeController;
use App\Http\Controllers\Admin\DashBoardController;
use App\Http\Controllers\Admin\OrderExcelController;
use App\Http\Controllers\UserVerificationController;
use App\Http\Controllers\Admin\HowtoUseAppController;
use App\Http\Controllers\Admin\NotificationController;
use App\Http\Controllers\Branch\BranchLoginController;
use App\Http\Controllers\Admin\OtherSettingsController;
use App\Http\Controllers\Admin\RegionDetailsController;
use App\Http\Controllers\Admin\RatingSegmentsController;
use App\Http\Controllers\Admin\SupportRequestController;
use App\Http\Controllers\Admin\UserManagementController;
use App\Http\Controllers\Branch\ForgotPasswordController;
use App\Http\Controllers\Supplier\SupplierLoginController;
use App\Http\Controllers\Admin\CommisionCategoryController;
use App\Http\Controllers\Supplier\PreferredBrandController;
use App\Http\Controllers\Admin\CancellationReasonController;
use App\Http\Controllers\Admin\LoginActivityController;
use App\Http\Controllers\Admin\OrderActivityLogController;
use App\Http\Controllers\Admin\PromotionalProductController;
use App\Http\Controllers\Supplier\PreferredCategoryController;
use App\Http\Controllers\Frontend\PromotionalProductsController;
use App\Http\Controllers\Frontend\ElasticSearchController;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

// Frontend

Route::get('setlocale/{locale}', function ($locale) {
    if (in_array($locale, \Config::get('app.locales'))) {
        session(['locale' => $locale]);
    }
    return redirect()->back();
});

// Route::get('/', 'HomeController@index')->name('home');
Route::get('/admin', 'HomeController@index')->name('home');
Auth::routes(['register' => false]);

Route::get('auth/google', 'Auth\GoogleController@redirectToGoogle')->name('google.redirect');
Route::get('auth/google/callback', 'Auth\GoogleController@handleGoogleCallback')->name('google.callback');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/pages/{slug}', 'Frontend\CmsController@index')->name('pages.show');
Route::group(['prefix' => 'admin', 'middleware' => 'auth:web'], function () {
    Route::get("/dashboard", [DashBoardController::class, 'get'])->name('dashboard.get');
    Route::post("/dashboard/find-count", [DashBoardController::class, 'getCount'])->name('dashboard.card_count');

    // Cusromer
    Route::get('/customer', 'Admin\CustomerController@index')->name('customer');
    Route::get('/customer/create', 'Admin\CustomerController@create')->name('create_customer');
    Route::post('/customer/save', 'Admin\CustomerController@save')->name('save_customer');
    Route::post('/customer/delete', 'Admin\CustomerController@deleteCustomer')->name('delete_customer');
    Route::post('/customer/activate', 'Admin\CustomerController@activate')->name('activate_customer');
    Route::get('/customer/details/{id}', 'Admin\CustomerController@details')->name('customer_details');
    Route::get('/customer/create_address', 'Admin\CustomerController@create_address')->name('create_address');
    Route::post('/customer/save_address', 'Admin\CustomerController@save_address')->name('save_address');
    Route::get('/customer/set_default', 'Admin\CustomerController@set_default')->name('set_default');
    Route::post('/customer/delete_address', 'Admin\CustomerController@delete_address')->name('delete_address');

    Route::get("/notification", [NotificationController::class, 'get'])->name('notification.get');
    Route::post("/notification/store", [NotificationController::class, 'store'])->name('notification.store');
    Route::post("/notification/delete", [NotificationController::class, 'destroy'])->name('notification.delete');
    Route::post("/autocomplete", [NotificationController::class, 'autoComplete'])->name('notification.auto');
    
    Route::get("/other/notification", 'Admin\OtherNotificationController@get')->name('notification.other');
    // Ads
    Route::get('/ads', 'Admin\AdsController@index')->name('ads');
    Route::post('ads/upload_image', 'Admin\AdsController@upload_image')->name('upload_image');
    Route::post('ads/detete_img', 'Admin\AdsController@detete_img')->name('detete_img');
    Route::get('/ads/link_category', 'Admin\AdsController@link_category')->name('link_category');
    Route::post('/ads/add_link', 'Admin\AdsController@add_link')->name('add_link');
    Route::get('/ads/tabs', 'Admin\AdsController@get_tabs')->name('ads_tab');
    Route::get('/ads/slider', 'Admin\AdsController@slider')->name('slider');
    Route::post('ads/slider_image', 'Admin\AdsController@upload_slider_image')->name('slider_image');
    Route::get('/ads/slider_category', 'Admin\AdsController@slider_category')->name('slider_category');
    Route::post('/ads/add_slider_category', 'Admin\AdsController@add_slider_category')->name('add_slider_category');
    Route::post('ads/detete_slider_img', 'Admin\AdsController@detete_slider_img')->name('detete_slider_img');
    Route::post('/ads/add_slider_data', 'Admin\AdsController@add_slider_data')->name('add_slider_data');
    Route::get('/ads/get_products', 'Admin\AdsController@getProducts')->name('ads.products');


    // Attribute

    Route::get("/attribute", 'Admin\AttributeController@index')->name('attribute');
    Route::get('/attribute/create', 'Admin\AttributeController@create')->name('create_attribute');
    Route::post('/attribute/save', 'Admin\AttributeController@save')->name('save_attribute');
    Route::post('/attribute/delete', 'Admin\AttributeController@deleteAttribute')->name('deleteAttribute');
    Route::post('/attribute/activate', 'Admin\AttributeController@activate')->name('activate_attribute');
    Route::get('/attribute/add_category_attribute', 'Admin\AttributeController@add_category_attribute')->name('add_category_attribute');
    Route::post('/attribute/save_category_attribute', 'Admin\AttributeController@save_cat_attribute')->name('save_cat_attribute');
    
    Route::get('/attribute/getCount', 'Admin\AttributeController@getCount')->name('attribute.getCount');


    // User

    Route::get("/user", [UserManagementController::class, 'get'])->name('user-list.get');
    Route::post("/user/store", [UserManagementController::class, 'store'])->name('user-list.store');
    Route::get("/user-list/edit/{id}", [UserManagementController::class, 'edit'])->name('user-list.edit');
    Route::post("/user-list/update", [UserManagementController::class, 'update'])->name('user-list.update');
    Route::post("/user-list/status/update", [UserManagementController::class, 'statusUpdate'])->name('user-list.status.update');
    Route::post("/user-list/delete", [UserManagementController::class, 'destroy'])->name('user-list.delete');
    Route::post("/user-list/sendPassword", [UserManagementController::class, 'passwordSend'])->name('user-list.setPassword');
//  Segment

    Route::get("/segments", [RatingSegmentsController::class, 'get'])->name('segments.get');
    Route::get("/segments/edit/{id}", [RatingSegmentsController::class, 'edit'])->name('segments.edit');
    Route::post("/segments/update", [RatingSegmentsController::class, 'update'])->name('segments.update');
    Route::post("/segments/status", [RatingSegmentsController::class, 'statusUpdate'])->name('segments.status');
// Cancellation Reason

    Route::get("/cancel-reasons", [CancellationReasonController::class, 'get'])->name('cancel.get');
    Route::post("/cancel-reasons/store", [CancellationReasonController::class, 'store'])->name('cancel.store');
    Route::get("/cancel-reasons/edit/{id}", [CancellationReasonController::class, 'edit'])->name('cancel.edit');
    Route::post("/cancel-reasons/update", [CancellationReasonController::class, 'update'])->name('cancel.update');
    Route::post("/cancel-reasons/delete", [CancellationReasonController::class, 'destroy'])->name('cancel.delete');
    Route::post("/cancel-reasons/auto-complete", [CancellationReasonController::class, 'autoComplete'])->name('cancel.auto');

// faq

    Route::get("/faqs", [FaqsController::class, 'get'])->name('faq.get');
    Route::get("/faq/edit/{id}", [FaqsController::class, 'edit'])->name('faq.edit');
    Route::post("/faqs/update", [FaqsController::class, 'update'])->name('faq.update');
    Route::post("/faqs/store", [FaqsController::class, 'store'])->name('faq.store');
    Route::post("/faqs/status-update", [FaqsController::class, 'statusUpdate'])->name('faq.status.update');
    Route::post("/faqs/delete", [FaqsController::class, 'destroy'])->name('faq.delete');
    Route::post("/faqs/search", [FaqsController::class, 'search'])->name('faq.search');

// City

    Route::get("/city", [CityController::class, 'get'])->name('city.get');
    Route::post("/city/store", [CityController::class, 'store'])->name('city.store');
    Route::get("/city/edit/{id}", [CityController::class, 'edit'])->name('city.edit');
    Route::post("/city/update", [CityController::class, 'update'])->name('city.update');
    Route::post("/city/status/update", [CityController::class, 'statusUpdate'])->name('city.status.update');
    Route::post("/city/delete", [CityController::class, 'destroy'])->name('city.delete');

// Route

    Route::get("/region", [RegionDetailsController::class, 'get'])->name('region.get');
    Route::get("/region/edit/{id}", [RegionDetailsController::class, 'edit'])->name('region.edit');
    Route::post("/region/store", [RegionDetailsController::class, 'store'])->name('region.store');
    Route::post("/region/delete", [RegionDetailsController::class, 'destroy'])->name('region.delete');
    Route::post("/region/auto", [RegionDetailsController::class, 'autoComplete'])->name('region.auto');
    Route::get("/region/get", [RegionDetailsController::class, 'getRegion'])->name('region.get_all');

// Pages
    Route::get("/pages", [PagesController::class, 'get'])->name('pages.get');
    Route::get("/pages/edit/{id}", [PagesController::class, 'edit'])->name('pages.edit');
    Route::post("/pages/update", [PagesController::class, 'update'])->name('pages.update');

// How to use app
    Route::get("/howto-use", [HowtoUseAppController::class, 'get'])->name('use.get');
    Route::get("/howto-use/edit/{id}", [HowtoUseAppController::class, 'edit'])->name('use.edit');
    Route::post("/howto-use/update", [HowtoUseAppController::class, 'update'])->name('use.update');
    Route::post("/howto-use/file/delete", [HowtoUseAppController::class, 'removeFile'])->name('use.image.delete');
    Route::post("/howto-use/file/delete", [HowtoUseAppController::class, 'removeFile'])->name('use.image.delete');
    Route::post("/howto-use/upload/file/{id}", [HowtoUseAppController::class, 'uploadFile'])->name('use.image.upload');

// Other settings
    Route::get("/other-settings", [OtherSettingsController::class, 'get'])->name('other.get');
    Route::post("/other-settings/store", [OtherSettingsController::class, 'store'])->name('other.store');


    Route::get("/edit-user", [EditProfile::class, 'editUser'])->name('user.edit');
    Route::post("/name-rest", [EditProfile::class, 'updateName'])->name('name.reset');
    Route::post("/password-change", [EditProfile::class, 'updatePassword'])->name('password.new');



// Promotional Products
    Route::get("/promoproduct", 'Admin\PromotionalProductController@index')->name('promoproduct');
    Route::get("/autocomplete", 'Admin\PromotionalProductController@autocomplete')->name('autocomplete');
    Route::post('/promoproduct/store', 'Admin\PromotionalProductController@store')->name('promoproduct.store');
    Route::get("/promoproducts", 'Admin\PromotionalProductController@promoproducts')->name('promoproducts');

//promocodes
    Route::get("/promocode", 'Admin\PromocodeController@index')->name('promocode');
    Route::post("/promocode/status/update", 'Admin\PromocodeController@statusUpdate')->name('promocode.status.update');
    Route::post("/promocode/delete", 'Admin\PromocodeController@destroy')->name('promocode.delete');
    Route::post('/promocode/store', 'Admin\PromocodeController@store')->name('promocode.store');
    Route::get("/autocomplete_category", 'Admin\PromocodeController@autocompleteCategory')->name('autocomplete_category');
    Route::get("/promocode/edit/{id}", 'Admin\PromocodeController@edit')->name('promocode.edit');
    Route::post("/promocode/update", 'Admin\PromocodeController@update')->name('promocode.update');

    Route::post('/brand/brand_images', 'Admin\BrandController@brand_images')->name('upload_brand_images');
    Route::post('brand/detete_img', 'Admin\BrandController@detete_img')->name('detete_brand_img');
    Route::get('/brand', 'Admin\BrandController@index')->name('brand');
    Route::post('/brand/store', 'Admin\BrandController@store')->name('brand.store');
    Route::post("/brand/update", 'Admin\BrandController@update')->name('brand.update');
    Route::post("/brand/status/update", 'Admin\BrandController@statusUpdate')->name('brand.status.update');
    Route::post("/brand/delete", 'Admin\BrandController@destroy')->name('brand.delete');
    Route::get("/brand/edit/{id}", 'Admin\BrandController@edit')->name('brand.edit');
    Route::post('/brand/brand_images', 'Admin\BrandController@brand_images')->name('upload_brand_images');
    Route::post('brand/detete_img', 'Admin\BrandController@detete_img')->name('detete_brand_img');

// Commision category

    Route::get("/commision-category", [CommisionCategoryController::class, 'get'])->name('commision.get');
    Route::post("/commision-category/store", [CommisionCategoryController::class, 'store'])->name('commision.store');
    Route::get("/commision-category/edit/{id}", [CommisionCategoryController::class, 'edit'])->name('commision.edit');
    Route::post("/commision-category/update", [CommisionCategoryController::class, 'update'])->name('commision.update');
    Route::post("/commision-category/delete", [CommisionCategoryController::class, 'destroy'])->name('commision.delete');
//category

    Route::get('/category', 'Admin\CategoryController@index')->name('category');
    Route::post('/category/store', 'Admin\CategoryController@store')->name('category.store');
    Route::get("/category/edit/{id}", 'Admin\CategoryController@edit')->name('category.edit');
    Route::post("/category/update", 'Admin\CategoryController@update')->name('category.update');
    Route::post("/category/status/update", 'Admin\CategoryController@statusUpdate')->name('category.status.update');
    Route::post("/category/featured/update", 'Admin\CategoryController@featuredUpdate')->name('category.featured.update');
    Route::post("/category/delete", 'Admin\CategoryController@destroy')->name('category.delete');
    Route::get('/category/category_attribute', 'Admin\CategoryController@add_category_attribute')->name('add_category_attribute');
    Route::post('/category/save_category_attribute', 'Admin\CategoryController@save_cat_attribute')->name('save_cat_attribute');
    Route::post("/category/delete_attributes", 'Admin\CategoryController@delete_attributes')->name('delete_cat_attributes');
    Route::get('/category/category_attribute', 'Admin\CategoryController@addProducttoCategory')->name('add_product_to_category');
    Route::post('/category/save_category_products', 'Admin\CategoryController@saveCategoryProducts')->name('save_category_products');
    Route::post("/category/delete_product", 'Admin\CategoryController@deleteCategoryProduct')->name('delete_category_products');
    Route::post('/category/category_images', 'Admin\CategoryController@category_images')->name('upload_category_images');
    Route::post('category/detete_img', 'Admin\CategoryController@detete_img')->name('detete_category_img');
    Route::get('/category/sub_category', 'Admin\CategoryController@getSubcategory')->name('category.sub');

// Products
    Route::get("/products", 'Admin\ProductController@index')->name('products');
    Route::get('/products/create', 'Admin\ProductController@create')->name('create_product');
    Route::get('/products/tabs', 'Admin\ProductController@get_tabs')->name('product_tabs');
    Route::post('/products/product_info', 'Admin\ProductController@product_info')->name('save_product_info');
    Route::post('/products/product_attributes', 'Admin\ProductController@product_attributes')->name('save_product_attributes');
    Route::post('/products/product_stock', 'Admin\ProductController@product_stock')->name('save_product_stock');
    Route::post('/products/product_price', 'Admin\ProductController@product_price')->name('save_product_price');
    Route::post('/products/product_images', 'Admin\ProductController@product_images')->name('upload_product_images');
    Route::post('products/detete_img', 'Admin\ProductController@detete_img')->name('detete_pdt_img');
    Route::post('/products/delete', 'Admin\ProductController@deleteProduct')->name('deleteProduct');
    Route::post('/products/activate', 'Admin\ProductController@activate')->name('activate_product');
    Route::get('/products/details/{id}', 'Admin\ProductViewController@details')->name('view_product');
    Route::get('/products/viewtabs', 'Admin\ProductViewController@getViewTabs')->name('productview_tabs');

    // Supplier
    Route::get('/suppliers', 'Admin\SupplierController@index')->name('admin.supplier');
    Route::get('/supplier/create', 'Admin\SupplierController@create')->name('admin.create_supplier');
    Route::post('/supplier/save', 'Admin\SupplierController@save')->name('admin.save_supplier');
    Route::post('/supplier/delete', 'Admin\SupplierController@delete')->name('admin.delete_supplier');
    Route::post('/supplier/activate', 'Admin\SupplierController@activate')->name('admin.activate_supplier');
    Route::get('/supplier/details/{id}', 'Admin\SupplierController@details')->name('admin.supplier_details');
    Route::get('/supplier/branches/{id}', 'Admin\SupplierController@branches')->name('admin.branch');
    Route::get('/branch/create', 'Admin\SupplierController@create_branch')->name('admin.create_branch');
    Route::post('/branch/save', 'Admin\SupplierController@save_branch')->name('admin.save_branch');
    Route::post("/supplier/sendPassword", 'Admin\SupplierController@passwordSend')->name('supplier.send_password');

    // Order

    Route::get('/collection', 'Admin\OrderController@activeCollectionOrder')->name('admin.order-collection');
    Route::get('/delivery', 'Admin\OrderController@activeDeliveryOrder')->name('admin.order-delivery');
    Route::get('/cancel', 'Admin\OrderController@cancelledOrder')->name('admin.order-cancelled');
    Route::get('/complete', 'Admin\OrderController@CompletedOrder')->name('admin.order-completed');
    Route::get('/order-details/{id}', 'Admin\OrderController@orderDetails')->name('admin.order-details');
    Route::get('/order-review/{id}', 'Admin\OrderController@orderReview')->name('admin.order-review');
    Route::post('/schedule', 'Admin\OrderController@scheduleFormGet')->name('admin.schedule-form');
    Route::post("order/time-slot", 'Admin\OrderController@getTimeSlot')->name('order.time-slot');
    Route::post("order/storeSchedule", 'Admin\OrderController@storeSchedule')->name('order.save-schedule');
    Route::post("order/schedule-complete", 'Admin\OrderController@savecompleteSchedule')->name('order.complete-collection.schedule');
    Route::get("/download-excel", [OrderExcelController::class, 'downloadExcel'])->name('admin.order.excel-download');
    Route::get("/download-pdf/{id}", [OrderExcelController::class, 'detailsPdfDownload'])->name('admin.order-details.pdf');
    Route::post('/cancel/single', 'Admin\OrderController@cancelSingleOrder')->name('admin.cancelsingle-order');
    Route::post('/accept-reject/single', 'Admin\OrderController@productAcceptReject')->name('admin.accept-reject');
    Route::post('/accept/all', 'Admin\OrderController@acceptAllProducts')->name('admin.accept-all-product');
    Route::post('/collect-status-update', 'Admin\OrderController@CollectionStatusUpdate')->name('admin.changeStatus');
    Route::post('/complete/single', 'Admin\OrderController@completeSingleOrder')->name('admin.orderComplete');
    Route::post('/collection/collection-status', 'Admin\OrderController@collectionStatusform')->name('admin.collection_pending');
    Route::get('/order-cancel/admin', 'Admin\OrderController@orderCancellationForm')->name('admin.order_cancel');
    Route::post('admin/order/cancel', 'Admin\OrderController@cancelOrder')->name('admin.cacel.my.order');

    
    

    //  Support 
    Route::get("/support_request", [SupportRequestController::class, 'list'])->name('support.list');
    Route::get("/support-request/chat/{id}", [SupportRequestController::class, 'getChat'])->name('support.chat.get');
    Route::POST("/support-request/chat/{id}", [SupportRequestController::class, 'send'])->name('support.chat.send');
    Route::get("/user/list-chat/{type_app}", [SupportRequestController::class, 'usersList'])->name('support.list_user');
    
    Route::post('ckeditor/image_upload', 'Admin\CKEditorController@upload')->name('editor.upload');
    
    
    // Reports

    Route::get('/reports', 'Admin\ReportsController@index')->name('reports');
    Route::post('/reports/salestab', 'Admin\ReportsController@salestab')->name('sales.reports');
    
    Route::get('/reports/saleslinechart', 'Admin\ReportsController@linechart')->name('sales.linechart');
    Route::post('/reports/linecharttab', 'Admin\ReportsController@linecharttab')->name('sales.linecharttab');
    
    Route::get('/reports/hourly', 'Admin\ReportsController@hourlychart')->name('sales.hourly');
    Route::post('/reports/hourlytab', 'Admin\ReportsController@hourlytab')->name('sales.hourlytab');
    
    Route::get('/reports/commission', 'Admin\ReportsController@commission')->name('reports.commission');
    Route::post('/reports/commissiontab', 'Admin\ReportsController@commissiontab')->name('reports.commissiontab');
    
    Route::get("/reports/download-excel/{id}/{start}/{end}", 'Admin\ReportsController@downloadExcel')->name('reports.excel-download');
    
    // OrderActivity
    
    Route::get("/order-activity", [OrderActivityLogController::class,'index'])->name('log.order');
    Route::get("/login-activity", [LoginActivityController::class,'index'])->name('log.login');
});


Route::post("token/verify", [UserVerificationController::class, 'resetUserPassword'])->name('user.verify');
Route::post("verify", [UserVerificationController::class, 'verify'])->name('token.verify');
Route::post("user/password/change", [UserVerificationController::class, 'passwordUpdating'])->name('user.reset');
Route::get("user/logout", [UserVerificationController::class, 'logout'])->name('user.logout');
Route::post("login", [LoginController::class, 'login'])->name('login');





Route::get('supplier/register', 'Supplier\RegistrationController@register')->name('supplier.register');
Route::post('supplier/store', 'Supplier\RegistrationController@store')->name('supplier.store');
Route::get('/supplier/region', 'Supplier\RegistrationController@getRegion')->name('supplier.get_region');
Route::post('supplier/verify', 'Supplier\RegistrationController@verify')->name('supplier.verify');

Route::post('supplier/category/store', 'Supplier\RegistrationController@storeCategory')->name('supplier.preferred_category');
Route::get('supplier/pref-brand', 'Supplier\RegistrationController@prefBrand')->name('supplier.pref-brand');
Route::post('supplier/brand/store', 'Supplier\RegistrationController@storeBrand')->name('supplier.pref_brand.store');


Route::get('supplier/login', 'Supplier\SupplierLoginController@login')->name('supplier.login');
Route::post('supplier/login', 'Supplier\SupplierLoginController@supplierLogin')->name('supplier.supplierlogin');
Route::get('supplier/password/request', 'Supplier\ForgotPasswordController@passwordRequest')->name('supplier.password.request');
Route::post('supplier/forgetpassword', 'Supplier\ForgotPasswordController@forgetpassword')->name('supplier.forgetpassword');
Route::post('supplier/token/verify', 'Supplier\ForgotPasswordController@verify')->name('supplier.token.verify');
Route::post("supplier/password/change", 'Supplier\ForgotPasswordController@passwordUpdating')->name('supplier.reset');

Route::group(['middleware' => 'auth:supplier'], function () {

    Route::get('supplier/logout', 'Supplier\SupplierLoginController@logout')->name('supplier.logout');

    Route::get('/supplier', 'Supplier\HomeController@index')->name('supplier');
    
    Route::post("/supplier/dashboard/find-count", 'Supplier\HomeController@getCount')->name('supplier.card_count');

    Route::get('/supplier/branch', 'Supplier\BranchController@index')->name('supplier.branch');
    Route::post('/supplier/branch', 'Supplier\BranchController@store')->name('branch.store');
    Route::post('/supplier/branch/store', 'Supplier\BranchController@update')->name('branch.update');
    Route::get("/supplier/branch/edit/{id}", 'Supplier\BranchController@edit')->name('branch.edit');
    Route::post("/supplier/branch/status/update", 'Supplier\BranchController@statusUpdate')->name('branch.status.update');
    Route::post("/supplier/branch/delete", 'Supplier\BranchController@destroy')->name('branch.delete');
    Route::post("/supplier/branch/send-password", 'Supplier\BranchController@sendPassword')->name('branch.setPassword');
    Route::get('/supplier/branch/get_region', 'Supplier\BranchController@getRegion')->name('branch.get_region');

    // products
    Route::get('/supplier/products', 'Supplier\ProductController@get')->name('supplier.product_list');
    Route::get('supplier/products/create', 'Supplier\ProductController@create')->name('supplier.create_product');
    Route::post('supplier/products/product_info', 'Supplier\ProductController@storeProductInfo')->name('supplier.save_product_info');
    Route::post('supplier/products/product_attributes', 'Supplier\ProductController@storeProductAttribute')->name('supplier.save_product_attributes');
    Route::post('supplier/products/activate_product', 'Supplier\ProductController@activateProducts')->name('supplier.activate_product');
    Route::get('supplier/products/tabs', 'Supplier\ProductController@get_tabs')->name('supplier.product_tabs');
    Route::post('supplier/products/product_stock', 'Supplier\ProductController@productStock')->name('supplier.save_product_stock');
    Route::post('supplier/products/product_price', 'Supplier\ProductController@productPrice')->name('supplier.save_product_price');
    Route::post('supplier/products/product_images', 'Supplier\ProductController@productImages')->name('supplier.upload_product_images');
    Route::post('supplier/products/activate', 'Supplier\ProductController@ProductStatusChange')->name('supplier.activate_product');
    Route::post('supplier/products/delete', 'Supplier\ProductController@deleteProduct')->name('supplier.deleteProduct');
    Route::get('supplier/products/viewtabs', 'Supplier\ProductViewController@getViewTabs')->name('supplier.productview_tabs');
    Route::get('supplier/products/details/{id}', 'Supplier\ProductViewController@details')->name('supplier.view_product');
    // Preferred Category
    Route::get('/supplier/category/list', [PreferredCategoryController::class, 'get'])->name('supplier.category_list');
    Route::post('/supplier/category/save', [PreferredCategoryController::class, 'categoryStore'])->name('supplier.category.store');
    Route::get('/supplier/category/sub_category', 'Supplier\PreferredCategoryController@getSubcategory')->name('supplier.sub_category');
    //Preferred Brand

    Route::get('/supplier/brands/list', [PreferredBrandController::class, 'get'])->name('supplier.brand_list');
    Route::post('/supplier/brands/save', [PreferredBrandController::class, 'brandStore'])->name('supplier.brand.store');

    //Import products
    Route::get('/supplier/importproducts/import', 'Supplier\ImportProductController@index')->name('import_products');
    Route::post('/supplier/importproducts/importsubmit', 'Supplier\ImportProductController@importsubmit')->name('import_submit');
    Route::get('/supplier/importproducts/excel_view', 'Supplier\ImportProductController@excel_view')->name('product_excel_view');
    Route::get('/supplier/importproducts/errors', 'Supplier\ImportProductController@get_errors')->name('import_errors');
    Route::get('/supplier/importproducts/import_view', 'Supplier\ImportProductController@import_view')->name('import_view');
    Route::get('/supplier/importproducts/confrim', 'Supplier\ImportProductController@confrim_import')->name('confrim_import');
    Route::get('/supplier/importproducts/imported_data', 'Supplier\ImportProductController@imported_data')->name('imported_data');
    
    // stock update
    Route::get('/supplier/stock/import', 'Supplier\StockUpdateController@index')->name('stock_update');
    Route::post('/supplier/stock/stocksubmit', 'Supplier\StockUpdateController@importsubmit')->name('stock_import_submit');
    Route::get('/supplier/stock/excel_view', 'Supplier\StockUpdateController@excel_view')->name('excel_stock');
    Route::get('/supplier/stock/errors', 'Supplier\StockUpdateController@get_errors')->name('stock.import_errors');
    Route::get('/supplier/stock/confrim', 'Supplier\StockUpdateController@confirm_import')->name('stock.confrim_import');
    Route::get('/supplier/stock/imported_data', 'Supplier\StockUpdateController@imported_data')->name('stock.imported_data');
    Route::get('/supplier/stock/excel_view_complex', 'Supplier\StockUpdateController@excel_view_complex')->name('excel_stock_complex');
    Route::get('/supplier/stock/errors_complex', 'Supplier\StockUpdateController@get_errors_complex')->name('stock.import_errors_complex');
    Route::get('/supplier/stock/confrim_complex', 'Supplier\StockUpdateController@confirm_import_complex')->name('stock.confrim_import_complex');
    Route::get('/supplier/stock/imported_data_complex', 'Supplier\StockUpdateController@imported_data_complex')->name('stock.imported_data_complex');
    
    
    // price update
    Route::get('/supplier/price/import', 'Supplier\PriceUpdateController@index')->name('price_update');
    Route::post('/supplier/price/priceimport', 'Supplier\PriceUpdateController@pricesubmit')->name('price_import_submit');
    Route::get('/supplier/price/pricesimple', 'Supplier\PriceUpdateController@priceSimple')->name('price_simple');
    Route::get('/supplier/price/simple_errors', 'Supplier\PriceUpdateController@SimpleErrors')->name('price.simple_errors');
    Route::get('/supplier/price/confirm_simple', 'Supplier\PriceUpdateController@confirmSimple')->name('price.confirm_import_simple');
    Route::get('/supplier/price/imported_simple', 'Supplier\PriceUpdateController@importedSimple')->name('price.imported_simple');
    
    Route::get('/price/pricecomplex', 'Supplier\PriceUpdateController@priceComplex')->name('price_complex');
    Route::get('/price/complex_errors', 'Supplier\PriceUpdateController@ComplexErrors')->name('price.complex_errors');
    Route::get('/price/confirm_complex', 'Supplier\PriceUpdateController@confirmComplex')->name('price.confirm_import_complex');
    Route::get('/price/imported_complex', 'Supplier\PriceUpdateController@importedComplex')->name('price.imported_complex');
    

    // order
    Route::get('supplier/supp-active', 'Supplier\OrderController@activeOrders')->name('supplier.active-order');
    Route::get('supplier/supp-complete', 'Supplier\OrderController@completedOrders')->name('supplier.complete-order');
    Route::get('supplier/supp-cancel', 'Supplier\OrderController@cancelledOrders')->name('supplier.cancel-order');

    Route::get('supplier/order-details/{id}', 'Supplier\OrderController@orderDetails')->name('supplier.order-details');
    Route::post('supplier/accept/product', 'Supplier\OrderController@acceptProduct')->name('supplier.accept-order');
    Route::post('supplier/cancel/product', 'Supplier\OrderController@cancelProduct')->name('supplier.cancel-product');
    Route::get("/supplier/download-excel", 'Supplier\OrderExcelController@downloadExcel')->name('supplier.order.excel-download');
    Route::post("/supplier/support", 'Supplier\OrderExcelController@supportChat')->name('supplier.support_chat');
    Route::post("/supplier/support/store", 'Supplier\OrderExcelController@supportChatStore')->name('supplier.support.store');
    Route::get("/supplier/order/pdf/{id}", 'Supplier\OrderExcelController@OrderPdf')->name('supplier.order.details.pdf');

    // Support
    Route::get("/supplier/support", 'Supplier\SupportRequestController@index')->name('supplier.support');
    Route::get("/supplier/support/create", 'Supplier\SupportRequestController@create')->name('supplier.create-support');
    Route::get("/supplier/support/store", 'Supplier\SupportRequestController@supportChatStore')->name('supplier.support-store');
    Route::get("/supplier/support-request/chat/{id}",'Supplier\SupportRequestController@getChat')->name('supplier.support.chat.get');
    Route::post("/supplier/support-request/chat/{id}", 'Supplier\SupportRequestController@send')->name('supplier.support.chat.send');
    
    
    Route::get("/supplier/edit-supplier", 'Supplier\EditProfileController@index')->name('supplier.edit');
    Route::post("/supplier/profile", 'Supplier\EditProfileController@profile_update')->name('supplier.profile');
    Route::post("/supplier/password-change", 'Supplier\EditProfileController@password_change')->name('supplier.password_new');
    // Supplier report
    Route::get("/supplier/report", 'Supplier\ReportController@index')->name('supplier.reports');
    Route::post('/supplier/reports/salestab', 'Supplier\ReportController@salestab')->name('supplier.sales.reports');
    Route::get('/supplier/reports/saleslinechart', 'Supplier\ReportController@linechart')->name('supplier.sales.linechart');
    Route::post('/supplier/reports/linecharttab', 'Supplier\ReportController@linecharttab')->name('supplier.sales.linecharttab');
    Route::get('supplier/reports/hourly', 'Supplier\ReportController@hourlychart')->name('supplier.sales.hourly');
    Route::post('supplier/reports/hourlytab', 'Supplier\ReportController@hourlytab')->name('supplier.sales.hourlytab');
    
    Route::get("/supplier/notification", 'Supplier\OtherNotificationController@get')->name('supplier.notification');
});

Route::get('branch/login', 'Branch\BranchLoginController@login')->name('branch.login');
Route::post('branch/login', 'Branch\BranchLoginController@branchLogin')->name('branch.branchlogin');

Route::group(['middleware' => 'auth:branch'], function () {

    Route::get('/branch', 'Branch\HomeController@index')->name('branch');
    Route::post("/branch/dashboard/find-count", 'Supplier\HomeController@getCount')->name('branch.card_count');
    
    Route::get("/dashboard", 'Branch\HomeController@index')->name('branch.dashboard');
    Route::get('branch/logout', 'Branch\BranchLoginController@logout')->name('branch.logout');

    // products

    Route::get('/branch/products', 'Branch\ProductController@get')->name('branch.product_list');
    Route::get('/branch/products/details/{id}', 'Branch\ProductController@details')->name('branch.view_product');
    Route::get('/branch/products/stock_update/{id}', 'Branch\ProductController@stockUpdates')->name('branch.stock_update');
    Route::post('branch/products/product_stock', 'Branch\ProductController@updateStock')->name('branch.save_product_stock');

    Route::get('branch/products/viewtabs', 'Branch\ProductViewController@getViewTabs')->name('branch.productview_tabs');
    Route::get('branch/products/details/{id}', 'Branch\ProductViewController@details')->name('branch.view_product');

    // order
    Route::get('branch/active', 'Branch\OrderController@activeOrders')->name('branch.active-order');
    Route::get('branch/complete', 'Branch\OrderController@completedOrders')->name('branch.complete-order');
    Route::get('branch/cancel', 'Branch\OrderController@cancelledOrders')->name('branch.cancel-order');
    Route::get('branch/order-details/{id}', 'Branch\OrderController@orderDetails')->name('branch.order-details');
    Route::post('branch/accept/product', 'Branch\OrderController@acceptProduct')->name('branch.accept-product');
    Route::post('branch/cancel/product', 'Branch\OrderController@cancelProduct')->name('branch.cancel-product');
    Route::get("/branch/download-excel", 'Branch\OrderExcelController@downloadExcel')->name('branch.order.excel-download');
    Route::post("/branch/support", 'Branch\OrderExcelController@supportChat')->name('branch.support_chat');
    Route::post("/branch/support/store", 'Branch\OrderExcelController@supportChatStore')->name('branch.support.store');
    Route::get("/branch/order/pdf/{id}", 'Branch\OrderExcelController@OrderPdf')->name('branch.order.details.pdf');


    //  Support 
    Route::get("/branch/support", 'Branch\SupportRequestController@index')->name('branch.support');
    Route::get("/branch/support/create", 'Branch\SupportRequestController@create')->name('branch.create-support');
    Route::get("/branch/support/store", 'Branch\SupportRequestController@supportChatStore')->name('branch.support-store');
    Route::get("/branch/support-request/chat/{id}",'Branch\SupportRequestController@getChat')->name('branch.support.chat.get');
    Route::post("/branch/support-request/chat/{id}", 'Branch\SupportRequestController@send')->name('branch.support.chat.send');
    
     // stock update
    Route::get('/branch/stock/import', 'Branch\StockUpdateController@index')->name('branch.stock_import');
    Route::post('/branch/stock/stocksubmit', 'Branch\StockUpdateController@importsubmit')->name('branch.stock_import_submit');
    Route::get('/branch/stock/stock_simple', 'Branch\StockUpdateController@StockSimple')->name('branch.stock_simple');
    Route::get('/branch/stock/errors', 'Branch\StockUpdateController@simple_errors')->name('branch.simple_errors');
    Route::get('/branch/stock/confrim_simple', 'Branch\StockUpdateController@confirmSimple')->name('branch.comfirm_simple');
    Route::get('/branch/stock/imported_simple', 'Branch\StockUpdateController@importedSimple')->name('branch.imported_simple');
    
    
    Route::get('/branch/stock/stock_complex', 'Branch\StockUpdateController@StockComplex')->name('branch.stock_complex');
    Route::get('/branch/stock/complex_errors', 'Branch\StockUpdateController@complex_errors')->name('branch.complex_errors');
    Route::get('/branch/stock/confrim_complex', 'Branch\StockUpdateController@confirmComplex')->name('branch.comfirm_complex');
    Route::get('/branch/stock/imported_complex', 'Branch\StockUpdateController@importedComplex')->name('branch.imported_complex');
    
    Route::get("/branch/edit_branch", 'Branch\EditProfileController@index')->name('branch.edit');
    Route::post("/branch/profile", 'Branch\EditProfileController@profile_update')->name('branch.profile');
    Route::post("/branch/password-change", 'Branch\EditProfileController@password_change')->name('branch.password_new');
    
    //  Branch report
    
    Route::get("/branch/report", 'Branch\ReportController@index')->name('branch.reports');
    Route::post('/branch/reports/salestab', 'Branch\ReportController@salestab')->name('branch.sales.reports');
    Route::get('/branch/reports/saleslinechart', 'Branch\ReportController@linechart')->name('branch.sales.linechart');
    Route::post('/branch/reports/linecharttab', 'Branch\ReportController@linecharttab')->name('branch.sales.linecharttab');
    Route::get('branch/reports/hourly', 'Branch\ReportController@hourlychart')->name('branch.sales.hourly');
    Route::post('branch/reports/hourlytab', 'Branch\ReportController@hourlytab')->name('branch.sales.hourlytab');
    
    Route::get("/branch/notification", 'Branch\OtherNotificationController@get')->name('branch.notification');
});

Route::get('branch/password/request', 'Branch\ForgotPasswordController@passwordRequest')->name('branch.password.request');
Route::post('branch/forgetpassword', 'Branch\ForgotPasswordController@forgetpassword')->name('branch.forgetpassword');
Route::post('branch/token/verify', 'Branch\ForgotPasswordController@verify')->name('branch.token.verify');
Route::post("branch/password/change", 'Branch\ForgotPasswordController@passwordUpdating')->name('branch.reset');


Route::get("/", 'Frontend\HomeController@home')->name('frontend');
Route::get('/home/search', 'Frontend\ElasticSearchController@index')->name('home.search');

Route::post('/more_category', 'Frontend\HomeController@more_category')->name('frontend.loadMore');

Route::post('/customer/login', 'Frontend\Common@login')->name('customer.login');
Route::get('/customer/logout', 'Frontend\Common@logout')->name('customer.logout');
Route::post('/customer/create', 'Frontend\Common@customer_create')->name('customer.create');
Route::post('/customer/verify', 'Frontend\Common@customer_verify')->name('customer.verify');
Route::post('/customer/phone', 'Frontend\Common@login_with_phone')->name('customer.phonelogin');
Route::post('/map/region/is_inside', 'Frontend\Common@is_inside_geo_region')->name('map.is_inside_reg');

Route::post('/cart/add', 'Frontend\HomeController@addToCart')->name('cart.add');
Route::get('/cart/show', 'Frontend\HomeController@cart')->name('cart.show');
Route::post('/product/remove', 'Frontend\HomeController@removeFromCart')->name('product.remove');
Route::get('/cart/refresh', 'Frontend\HomeController@cartRefresh')->name('cart.refresh');

Route::get('/cart/add_note', 'Frontend\HomeController@order_note')->name('cart.order_note');
Route::get('/cart/coupon', 'Frontend\HomeController@coupon_code')->name('cart.coupon_code');

Route::get('/category/{slug}', 'Frontend\CategoryController@index')->name('category.show');
Route::get('/products/{slug}', 'Frontend\ProductController@index')->name('products.detail');
Route::post('/category/search', 'Frontend\CategoryController@search')->name('category.search');
Route::post('/category/loadMore', 'Frontend\CategoryController@loadMore')->name('category.loadMore');


Route::get('/promotional-products', [PromotionalProductsController::class, 'get'])->name('promotional.all');
Route::post('/promotional-products/loadMore', [PromotionalProductsController::class, 'loadMore'])->name('promoproduct.loadmore');
Route::post('/promotional-products/search', [PromotionalProductsController::class, 'searchProduct'])->name('promoproduct.search');
Route::post('/promotional-products/quality', [PromotionalProductsController::class, 'qualityFind'])->name('promoproduct.quantity');

Route::get('/product-qr', 'QRCodeGenerationController@productCodeGeneration')->name('product.QRCode');
Route::get('/order-qr/{id}', 'QRCodeGenerationController@OrderCodeGeneration')->name('order-qrcode');
Route::get('/supplier-qr/', 'QRCodeGenerationController@supplierCodeGeneration')->name('supplier-qrcode');
Route::get('/branch-qr/', 'QRCodeGenerationController@branchCodeGeneration')->name('branch-qrcode');


Route::group(['middleware' => 'customersession'], function () {

    Route::post('/customer/branch/add', 'Frontend\Common@branch_create')->name('branch.add');
    Route::get('/customer/branch', 'Frontend\HomeController@branch')->name('customer.branch');
    Route::get('/customer/wish_list', 'Frontend\Common@wishlist')->name('customer.wishlist');
    Route::post('/customer/favourite', 'Frontend\Common@add_to_favourite')->name('product.favourite');
    Route::post('/customer/branch/delete', 'Frontend\Common@delete_branch')->name('frontend.branch.delete');
    
    Route::post('/customer/branch/login', 'Frontend\Common@branch_login_credentials')->name('frontend.branch.login');


    Route::get('/cart/checkout', 'Frontend\HomeController@checkout')->name('cart.checkout');
    Route::get('/customer/address/{id?}', 'Frontend\Common@addresses')->name('customer.adddresses');

    Route::post('/customer/address/add', 'Frontend\Common@save_address')->name('customer.add_address');
    Route::post('/customer/address/list', 'Frontend\Common@list_address')->name('customer.list_address');
    Route::post('customer/forgot_password', 'Frontend\Common@forgot_password')->name('customer.forgot_password');
    Route::post('customer/reset_password', 'Frontend\Common@reset_password')->name('customer.reset_password');
    Route::post('/customer/address/addbilling', 'Frontend\Common@make_billing')->name('customer.make_billing');
    Route::post('/customer/address/delete', 'Frontend\Common@delete_address')->name('customer.delete_address');
    Route::post('/customer/address/edit', 'Frontend\Common@edit_address')->name('customer.edit_address');
    Route::post('/customer/address/change', 'Frontend\Common@change_default')->name('customer.change_address');
    Route::get('/customer/get/savelater', 'Frontend\HomeController@saveLaterItems')->name('customer.savelater_items');
    Route::get('/customer/get/wishlist_items', 'Frontend\HomeController@wishlist_items')->name('customer.wishlist_items');
    Route::post('/customer/savelater', 'Frontend\Common@add_to_savelater')->name('customer.savelater');

    Route::post('billing/choose_address', 'Frontend\Common@choose_billing_address')->name('billing.set_address');
    Route::post('/cart/order', 'Frontend\HomeController@checkout_cart')->name('cart.place_order');
    Route::get('/cart/thankyou/{id}', 'Frontend\HomeController@thankyou')->name('cart.thanks');

    // order
    Route::get('/order/{id?}', 'Frontend\OrderController@orderList')->name('order.list');
    Route::get('/order-details/{id}', 'Frontend\OrderController@orderDetails')->name('order.details');
    Route::post('/order/support', 'Frontend\OrderController@supportChat')->name('order.support_request');
    Route::post('/order/cancel', 'Frontend\OrderController@cancelOrder')->name('order.cancel_order');
    Route::get('cart/check', 'Frontend\OrderController@cartCount')->name('cart.item_check');
    Route::get('order/modify/{id?}', 'Frontend\OrderController@modifyOrder')->name('order.modify');
    Route::get('order/filter/{id?}', 'Frontend\OrderController@filterOrder')->name('order.filter');

    Route::post('/order/product/rating', 'Frontend\OrderController@product_rating')->name('order.product_rating');
    Route::post('/order/order-rating', 'Frontend\OrderController@orderRating')->name('order.order_rating');




    Route::get('/profile/edit', 'Frontend\EditProfileController@index')->name('profile.edit');
    Route::post('profile/upload', 'Frontend\EditProfileController@upload_image')->name('profile_image');
    Route::post('profile/update', 'Frontend\EditProfileController@profile_update')->name('edit_profile');
    Route::get('/profile/region', 'Frontend\EditProfileController@getRegion')->name('profile.region');


    // supprt request
    Route::get('/support-list', 'Frontend\SupportFrontendController@getSupportList')->name('supprot.frndend.list');
    Route::get('/support-chat/{id}', 'Frontend\SupportFrontendController@getDetailsChat')->name('supprot.frndend.details');
    Route::post('frondend/support-request/chat/{id}', 'Frontend\SupportFrontendController@sendChat')->name('supprot.frndend.sendchat');

    
});

